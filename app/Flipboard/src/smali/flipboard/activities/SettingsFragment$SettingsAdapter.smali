.class Lflipboard/activities/SettingsFragment$SettingsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method private constructor <init>(Lflipboard/activities/SettingsFragment;)V
    .locals 0

    .prologue
    .line 1334
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/activities/SettingsFragment;B)V
    .locals 0

    .prologue
    .line 1334
    invoke-direct {p0, p1}, Lflipboard/activities/SettingsFragment$SettingsAdapter;-><init>(Lflipboard/activities/SettingsFragment;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lflipboard/activities/SettingsFragment$RowItem;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1353
    .line 1355
    const/4 v1, 0x0

    move v4, v0

    move v2, v0

    move v3, p1

    .line 1356
    :goto_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->h(Lflipboard/activities/SettingsFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    if-nez v2, :cond_1

    .line 1357
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->h(Lflipboard/activities/SettingsFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SettingsFragment$Category;

    .line 1358
    iget-object v5, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v3, :cond_0

    .line 1359
    iget-object v0, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SettingsFragment$RowItem;

    .line 1360
    const/4 v1, 0x1

    move v2, v3

    .line 1364
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    move-object v1, v0

    .line 1365
    goto :goto_0

    .line 1362
    :cond_0
    iget-object v0, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, v3, v0

    move-object v6, v1

    move v1, v2

    move v2, v0

    move-object v0, v6

    goto :goto_1

    .line 1366
    :cond_1
    return-object v1
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1460
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 1343
    const/4 v0, 0x0

    .line 1344
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->h(Lflipboard/activities/SettingsFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SettingsFragment$Category;

    .line 1345
    iget-object v0, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1346
    goto :goto_0

    .line 1347
    :cond_0
    return v1
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1334
    invoke-virtual {p0, p1}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a(I)Lflipboard/activities/SettingsFragment$RowItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1386
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 1371
    invoke-virtual {p0, p1}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a(I)Lflipboard/activities/SettingsFragment$RowItem;

    move-result-object v0

    .line 1372
    if-eqz v0, :cond_1

    .line 1373
    iget-boolean v0, v0, Lflipboard/activities/SettingsFragment$RowItem;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1376
    :goto_0
    return v0

    .line 1373
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1376
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1391
    invoke-virtual {p0, p1}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a(I)Lflipboard/activities/SettingsFragment$RowItem;

    move-result-object v3

    .line 1393
    iget-boolean v0, v3, Lflipboard/activities/SettingsFragment$RowItem;->k:Z

    if-eqz v0, :cond_3

    .line 1394
    if-nez p2, :cond_0

    .line 1395
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030119

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1397
    :cond_0
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 1398
    if-nez v0, :cond_2

    .line 1455
    :cond_1
    :goto_0
    return-object p2

    .line 1401
    :cond_2
    iget-object v1, v3, Lflipboard/activities/SettingsFragment$RowItem;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1403
    :cond_3
    if-nez p2, :cond_4

    .line 1404
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f030117

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1407
    :cond_4
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 1408
    if-eqz v0, :cond_1

    .line 1411
    iget-object v4, v3, Lflipboard/activities/SettingsFragment$RowItem;->d:Ljava/lang/String;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 1413
    const v0, 0x7f0a0309

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 1414
    if-eqz v0, :cond_5

    .line 1415
    invoke-virtual {v3}, Lflipboard/activities/SettingsFragment$RowItem;->b()Ljava/lang/String;

    move-result-object v4

    .line 1416
    if-eqz v4, :cond_6

    .line 1417
    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 1418
    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 1424
    :cond_5
    :goto_1
    const v0, 0x7f0a030a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1425
    iget-boolean v4, v3, Lflipboard/activities/SettingsFragment$RowItem;->h:Z

    if-eqz v4, :cond_7

    .line 1426
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1427
    iget-object v4, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v4}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v4

    iget-object v5, v3, Lflipboard/activities/SettingsFragment$RowItem;->i:Ljava/lang/String;

    iget-boolean v6, v3, Lflipboard/activities/SettingsFragment$RowItem;->j:Z

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1428
    new-instance v4, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;

    invoke-direct {v4, p0, v3, v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;-><init>(Lflipboard/activities/SettingsFragment$SettingsAdapter;Lflipboard/activities/SettingsFragment$RowItem;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1444
    :goto_2
    const v0, 0x7f0a0307

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 1445
    iget-object v4, v3, Lflipboard/activities/SettingsFragment$RowItem;->g:Ljava/lang/String;

    if-eqz v4, :cond_8

    .line 1446
    sget-object v4, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 1447
    iget-object v3, v3, Lflipboard/activities/SettingsFragment$RowItem;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 1448
    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 1453
    :goto_3
    const v0, 0x7f0a030c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1420
    :cond_6
    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_1

    .line 1440
    :cond_7
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1441
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_2

    .line 1450
    :cond_8
    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_3

    :cond_9
    move v0, v2

    .line 1453
    goto :goto_4
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 1381
    const/4 v0, 0x2

    return v0
.end method
