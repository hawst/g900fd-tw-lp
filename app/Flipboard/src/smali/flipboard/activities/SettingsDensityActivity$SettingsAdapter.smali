.class Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingsDensityActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsDensityActivity;


# direct methods
.method private constructor <init>(Lflipboard/activities/SettingsDensityActivity;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/activities/SettingsDensityActivity;B)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;-><init>(Lflipboard/activities/SettingsDensityActivity;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lflipboard/activities/SettingsDensityActivity$RowItem;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 188
    .line 190
    const/4 v1, 0x0

    move v4, v0

    move v2, v0

    move v3, p1

    .line 191
    :goto_0
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v0}, Lflipboard/activities/SettingsDensityActivity;->c(Lflipboard/activities/SettingsDensityActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    if-nez v2, :cond_1

    .line 192
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v0}, Lflipboard/activities/SettingsDensityActivity;->c(Lflipboard/activities/SettingsDensityActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SettingsDensityActivity$Category;

    .line 193
    iget-object v5, v0, Lflipboard/activities/SettingsDensityActivity$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v3, :cond_0

    .line 194
    iget-object v0, v0, Lflipboard/activities/SettingsDensityActivity$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SettingsDensityActivity$RowItem;

    .line 195
    const/4 v1, 0x1

    move v2, v3

    .line 199
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    move-object v1, v0

    .line 200
    goto :goto_0

    .line 197
    :cond_0
    iget-object v0, v0, Lflipboard/activities/SettingsDensityActivity$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, v3, v0

    move-object v6, v1

    move v1, v2

    move v2, v0

    move-object v0, v6

    goto :goto_1

    .line 201
    :cond_1
    return-object v1
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    iget-object v1, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v1}, Lflipboard/activities/SettingsDensityActivity;->c(Lflipboard/activities/SettingsDensityActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SettingsDensityActivity$Category;

    .line 181
    iget-object v0, v0, Lflipboard/activities/SettingsDensityActivity$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 182
    goto :goto_0

    .line 183
    :cond_0
    return v1
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a(I)Lflipboard/activities/SettingsDensityActivity$RowItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 216
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a(I)Lflipboard/activities/SettingsDensityActivity$RowItem;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/activities/SettingsDensityActivity$RowItem;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 221
    invoke-virtual {p0, p1}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a(I)Lflipboard/activities/SettingsDensityActivity$RowItem;

    move-result-object v1

    .line 223
    iget-boolean v0, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->k:Z

    if-eqz v0, :cond_3

    .line 224
    if-nez p2, :cond_0

    .line 225
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-virtual {v0}, Lflipboard/activities/SettingsDensityActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030119

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 227
    :cond_0
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 228
    if-nez v0, :cond_2

    .line 283
    :cond_1
    :goto_0
    return-object p2

    .line 231
    :cond_2
    iget-object v1, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 233
    :cond_3
    if-nez p2, :cond_4

    .line 234
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-virtual {v0}, Lflipboard/activities/SettingsDensityActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030117

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 237
    :cond_4
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 238
    if-eqz v0, :cond_1

    .line 241
    iget-object v2, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 243
    const v0, 0x7f0a0309

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 244
    if-eqz v0, :cond_5

    .line 245
    invoke-virtual {v1}, Lflipboard/activities/SettingsDensityActivity$RowItem;->a()Ljava/lang/String;

    move-result-object v2

    .line 246
    if-eqz v2, :cond_6

    .line 247
    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 248
    invoke-interface {v0, v5}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 254
    :cond_5
    :goto_1
    const v0, 0x7f0a030a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 255
    iget-boolean v2, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->h:Z

    if-eqz v2, :cond_7

    .line 256
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 257
    iget-object v2, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v2}, Lflipboard/activities/SettingsDensityActivity;->d(Lflipboard/activities/SettingsDensityActivity;)Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->i:Ljava/lang/String;

    iget-boolean v4, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->j:Z

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 258
    new-instance v2, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;

    invoke-direct {v2, p0, v1, v0}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;-><init>(Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;Lflipboard/activities/SettingsDensityActivity$RowItem;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    :goto_2
    const v0, 0x7f0a0307

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 275
    iget-object v2, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->g:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 276
    sget-object v2, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 277
    iget-object v1, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 278
    invoke-static {v0, v5}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 250
    :cond_6
    invoke-interface {v0, v6}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_1

    .line 270
    :cond_7
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_2

    .line 280
    :cond_8
    invoke-static {v0, v6}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x2

    return v0
.end method
