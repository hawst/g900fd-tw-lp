.class public Lflipboard/activities/SocialCardActivity;
.super Lflipboard/activities/FeedActivity;
.source "SocialCardActivity.java"


# instance fields
.field n:Lflipboard/activities/SocialCardFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lflipboard/activities/FeedActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 25
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f030074

    invoke-virtual {p0, v0}, Lflipboard/activities/SocialCardActivity;->setContentView(I)V

    .line 27
    iget-object v0, p0, Lflipboard/activities/SocialCardActivity;->D:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_0

    .line 28
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 29
    invoke-virtual {p0}, Lflipboard/activities/SocialCardActivity;->finish()V

    .line 50
    :goto_0
    return-void

    .line 32
    :cond_0
    if-nez p1, :cond_4

    .line 33
    new-instance v0, Lflipboard/activities/SocialCardFragment;

    invoke-direct {v0}, Lflipboard/activities/SocialCardFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    .line 34
    iget-object v0, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    iget-object v1, p0, Lflipboard/activities/SocialCardActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/activities/SocialCardFragment;->a(Lflipboard/objs/FeedItem;)V

    .line 35
    iget-object v0, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    iget-object v1, p0, Lflipboard/activities/SocialCardActivity;->C:Lflipboard/service/Section;

    iput-object v1, v0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    .line 36
    invoke-virtual {p0}, Lflipboard/activities/SocialCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extraNavFrom"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    iget-object v1, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    invoke-static {v0}, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    move-result-object v0

    iput-object v0, v1, Lflipboard/activities/SocialCardFragment;->z:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    .line 40
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/SocialCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launched_by_sstream"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 41
    invoke-virtual {p0}, Lflipboard/activities/SocialCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_origin_section_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lflipboard/activities/SocialCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_content_discovery_from_source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 43
    invoke-virtual {p0}, Lflipboard/activities/SocialCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_flipboard_button_title"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    iget-object v3, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    iget-boolean v4, p0, Lflipboard/activities/SocialCardActivity;->Z:Z

    iput-boolean v4, v3, Lflipboard/activities/SocialCardFragment;->v:Z

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, v3, Lflipboard/activities/SocialCardFragment;->w:Lflipboard/service/Section;

    iput-object v1, v3, Lflipboard/activities/SocialCardFragment;->x:Landroid/os/Bundle;

    iput-object v2, v3, Lflipboard/activities/SocialCardFragment;->y:Ljava/lang/String;

    .line 46
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    const-string v3, "social_card"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto :goto_0

    .line 44
    :cond_3
    iget-object v4, v3, Lflipboard/activities/SocialCardFragment;->n:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v4, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    goto :goto_1

    .line 48
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "social_card"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SocialCardFragment;

    iput-object v0, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    goto/16 :goto_0
.end method

.method public send(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lflipboard/activities/SocialCardActivity;->n:Lflipboard/activities/SocialCardFragment;

    invoke-virtual {v0, p1}, Lflipboard/activities/SocialCardFragment;->send(Landroid/view/View;)V

    .line 57
    :cond_0
    return-void
.end method
