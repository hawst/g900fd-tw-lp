.class public Lflipboard/activities/ShareFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "ShareFragment.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field final b:Lflipboard/service/FlipboardManager;

.field c:Lflipboard/gui/MagazineThumbView;

.field d:Lflipboard/objs/Magazine;

.field e:Lflipboard/gui/FLButton;

.field f:Lflipboard/gui/FLEditText;

.field g:Landroid/view/ViewGroup;

.field h:Landroid/support/v4/view/ViewPager;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation
.end field

.field m:Lflipboard/objs/Magazine;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/view/View;

.field private p:Lflipboard/gui/FLImageView;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroid/support/v4/view/PagerTabStrip;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Lflipboard/activities/ShareFragment$MagazineAdapter;

.field private v:Landroid/os/Bundle;

.field private w:Ljava/lang/String;

.field private x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:Z

.field private z:Lflipboard/service/Section;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lflipboard/activities/ShareActivity;->n:Lflipboard/util/Log;

    sput-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 90
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->i:Ljava/util/List;

    .line 1021
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ShareFragment;Lflipboard/objs/Magazine;)Lflipboard/objs/Magazine;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ShareFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ShareFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lflipboard/activities/ShareFragment;->B:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ShareFragment;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lflipboard/activities/ShareFragment;->x:Ljava/util/Map;

    return-object p1
.end method

.method private a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 313
    iput-object p1, p0, Lflipboard/activities/ShareFragment;->n:Ljava/util/List;

    .line 314
    iget-boolean v0, p0, Lflipboard/activities/ShareFragment;->A:Z

    if-eqz v0, :cond_6

    .line 315
    iput-boolean v7, p0, Lflipboard/activities/ShareFragment;->A:Z

    .line 316
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0246

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 317
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0261

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 318
    new-instance v2, Lflipboard/objs/Link;

    invoke-direct {v2}, Lflipboard/objs/Link;-><init>()V

    .line 319
    const-string v3, "experiment"

    iput-object v3, v2, Lflipboard/objs/Link;->a:Ljava/lang/String;

    .line 320
    const-string v3, "38"

    iput-object v3, v2, Lflipboard/objs/Link;->b:Ljava/lang/String;

    .line 323
    iget-object v3, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v4, "created.picks.default.magazine"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 324
    iget-object v4, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v5, "created.read.later.default.magazine"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 326
    iget-object v5, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v6, "had.no.magazines"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 327
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz v5, :cond_a

    .line 329
    :cond_0
    if-nez v3, :cond_1

    .line 330
    const-string v6, "default_"

    invoke-static {v6, v0}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lflipboard/objs/Link;->c:Ljava/lang/String;

    .line 331
    new-instance v6, Lflipboard/objs/Magazine;

    const-string v7, "public"

    const v8, 0x7f0200d6

    invoke-direct {v6, v0, v7, v8, v2}, Lflipboard/objs/Magazine;-><init>(Ljava/lang/String;Ljava/lang/String;ILflipboard/objs/Link;)V

    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    :cond_1
    if-nez v4, :cond_2

    .line 335
    const-string v0, "default_"

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Link;->c:Ljava/lang/String;

    .line 336
    new-instance v0, Lflipboard/objs/Magazine;

    const-string v6, "private"

    const v7, 0x7f0200d7

    invoke-direct {v0, v1, v6, v7, v2}, Lflipboard/objs/Magazine;-><init>(Ljava/lang/String;Ljava/lang/String;ILflipboard/objs/Link;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    :cond_2
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->j:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->e:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 340
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 341
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 342
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "flipboard.extra.reference.item.partner.id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 344
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 346
    if-eqz v4, :cond_3

    if-nez v3, :cond_8

    .line 347
    :cond_3
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->a:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 358
    :cond_4
    iget-boolean v0, p0, Lflipboard/activities/ShareFragment;->y:Z

    if-nez v0, :cond_5

    .line 359
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 360
    iput-boolean v9, p0, Lflipboard/activities/ShareFragment;->y:Z

    .line 363
    :cond_5
    if-nez v5, :cond_6

    .line 364
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "had.no.magazines"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 372
    :cond_6
    :goto_0
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->n:Ljava/util/List;

    new-instance v1, Lflipboard/activities/ShareFragment$CreateMagazine;

    invoke-direct {v1}, Lflipboard/activities/ShareFragment$CreateMagazine;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    if-eqz v0, :cond_7

    .line 375
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ShareFragment$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ShareFragment$2;-><init>(Lflipboard/activities/ShareFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 384
    :cond_7
    return-void

    .line 349
    :cond_8
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 350
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    .line 351
    iget-boolean v0, v0, Lflipboard/objs/Magazine;->m:Z

    if-eqz v0, :cond_9

    .line 352
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->a:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    invoke-virtual {v1, v0, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_1

    .line 368
    :cond_a
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created.read.later.default.magazine"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 369
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created.picks.default.magazine"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private a()Z
    .locals 9

    .prologue
    const/16 v8, 0x2710

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 239
    .line 242
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    .line 243
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 246
    :goto_0
    if-eqz v0, :cond_7

    .line 247
    sget-object v4, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v1

    .line 249
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/net/Uri;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_1

    const-string v4, "http://"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "https://"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 254
    :cond_0
    iput-object v0, p0, Lflipboard/activities/ShareFragment;->w:Ljava/lang/String;

    move v0, v1

    .line 308
    :goto_1
    return v0

    .line 256
    :cond_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 257
    invoke-static {v4}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v5

    .line 258
    if-eqz v5, :cond_5

    const-string v6, "text/plain"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 260
    :try_start_0
    sget-object v2, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v2, v5

    .line 262
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 263
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 265
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 268
    :cond_2
    :goto_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-ge v6, v8, :cond_4

    if-eqz v0, :cond_4

    .line 269
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    if-le v6, v8, :cond_3

    .line 270
    const/4 v6, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    rsub-int v7, v7, 0x270d

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string v0, "..."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :goto_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 276
    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 284
    :catch_0
    move-exception v0

    .line 285
    :try_start_2
    const-class v5, Lflipboard/activities/ComposeActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v5

    sget-object v6, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 287
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    .line 288
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move v0, v1

    .line 289
    goto :goto_1

    .line 273
    :cond_3
    :try_start_4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 287
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    .line 288
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 293
    :catch_1
    move-exception v0

    .line 292
    const-class v2, Lflipboard/activities/ComposeActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v2, v4, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 293
    goto/16 :goto_1

    .line 279
    :cond_4
    :try_start_6
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    .line 282
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->w:Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 287
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    .line 288
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    move v0, v1

    .line 289
    goto/16 :goto_1

    .line 295
    :cond_5
    sget-object v3, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    new-array v3, v2, [Ljava/lang/Object;

    aput-object v0, v3, v1

    .line 296
    if-eqz v0, :cond_6

    .line 297
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lflipboard/util/ShareHelper;->a(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/activities/ShareFragment$6;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/ShareFragment$6;-><init>(Lflipboard/activities/ShareFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/Flap$JSONResultObserver;)V

    move v0, v2

    .line 298
    goto/16 :goto_1

    .line 300
    :cond_6
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 301
    if-eqz v0, :cond_7

    .line 302
    const v2, 0x7f0d033d

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v3

    goto/16 :goto_0
.end method

.method static synthetic b(Lflipboard/activities/ShareFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/ShareFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lflipboard/activities/ShareFragment;)Lflipboard/gui/FLImageView;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->p:Lflipboard/gui/FLImageView;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/ShareFragment;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/ShareFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/ShareFragment;)Lflipboard/activities/ShareFragment$MagazineAdapter;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/ShareFragment;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/ShareFragment;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->x:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i(Lflipboard/activities/ShareFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lflipboard/activities/ShareFragment;)Lflipboard/objs/Magazine;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    return-object v0
.end method

.method static synthetic k(Lflipboard/activities/ShareFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lflipboard/activities/ShareFragment;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 83
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ShareFragment;->A:Z

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/ShareFragment;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public addToMagazine()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 556
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-boolean v0, v0, Lflipboard/objs/Magazine;->n:Z

    if-eqz v0, :cond_1

    .line 557
    new-instance v0, Lflipboard/activities/ShareFragment$4;

    invoke-direct {v0, p0}, Lflipboard/activities/ShareFragment$4;-><init>(Lflipboard/activities/ShareFragment;)V

    .line 629
    iget-object v1, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v1, v1, Lflipboard/objs/Magazine;->p:Lflipboard/objs/Link;

    invoke-virtual {v1}, Lflipboard/objs/Link;->toString()Ljava/lang/String;

    move-result-object v1

    .line 631
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v3, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v4, v4, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iget-object v5, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v5, v5, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    new-instance v6, Lflipboard/service/Flap$CreateMagazineRequest;

    invoke-direct {v6, v2, v3}, Lflipboard/service/Flap$CreateMagazineRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v6, v4, v5, v1, v0}, Lflipboard/service/Flap$CreateMagazineRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$CreateMagazineRequest;

    .line 775
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->f:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 640
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 641
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 644
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 645
    iget-object v1, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v1, v1, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    if-eqz v1, :cond_2

    .line 646
    iget-object v1, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v1, v1, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 647
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 648
    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 653
    :cond_3
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v5}, Lflipboard/service/User;->c(Ljava/util/List;)V

    .line 657
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 658
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_magazine_share_target"

    iget-object v5, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v5, v5, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 661
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    const-string v1, "auth/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 662
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 667
    :goto_2
    new-instance v7, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->p:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v5, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v7, v1, v5}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 672
    iget-object v1, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    if-eqz v1, :cond_b

    .line 673
    iget-object v1, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    .line 674
    sget-object v5, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v7, v5, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 675
    sget-object v5, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v6, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    iget-object v6, v6, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v6, v6, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v7, v5, v6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 676
    iget-object v5, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    iget-object v6, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v5

    .line 680
    :goto_3
    iget-object v6, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    if-eqz v6, :cond_4

    .line 681
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v6, "flipboard.extra.navigating.from"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 685
    :cond_4
    iget-object v6, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v8, "flipboard.extra.reference.item.partner.id"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 686
    sget-object v9, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v6, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    invoke-virtual {v6}, Lflipboard/objs/Magazine;->a()Z

    move-result v6

    if-eqz v6, :cond_8

    sget-object v6, Lflipboard/objs/UsageEventV2$EventDataType;->c:Lflipboard/objs/UsageEventV2$EventDataType;

    :goto_4
    invoke-virtual {v7, v9, v6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 687
    sget-object v6, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v9, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    invoke-virtual {v7, v6, v9}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 688
    sget-object v6, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v9, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    invoke-virtual {v7, v6, v9}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 689
    sget-object v6, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v7, v6, v8}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 690
    sget-object v6, Lflipboard/objs/UsageEventV2$CommonEventData;->g:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v8, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v8, v8, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v7, v6, v8}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 691
    sget-object v8, Lflipboard/objs/UsageEventV2$CommonEventData;->h:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v6, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v6, v6, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    if-nez v6, :cond_9

    const-string v6, ""

    :goto_5
    invoke-virtual {v7, v8, v6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 692
    sget-object v6, Lflipboard/objs/UsageEventV2$CommonEventData;->e:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v7, v6, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 693
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v7, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 694
    if-eqz v5, :cond_5

    .line 695
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v6, v5, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v7, v0, v6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 696
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->j:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v6, v5, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    invoke-virtual {v7, v0, v6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 698
    if-eqz v2, :cond_5

    sget-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->b:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 699
    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->al()Ljava/lang/String;

    move-result-object v0

    .line 700
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v7, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 704
    :cond_5
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-boolean v0, v0, Lflipboard/objs/Magazine;->m:Z

    if-eqz v0, :cond_6

    .line 705
    const-string v0, "default_"

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v2, v2, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 706
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->f:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v7, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 710
    :cond_6
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    invoke-static {v7, v0, v1, v2}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/UsageEventV2;Lflipboard/objs/Magazine;Ljava/lang/String;Ljava/lang/String;)Lflipboard/service/Flap$CancellableJSONResultObserver;

    move-result-object v1

    .line 712
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->x:Ljava/util/Map;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 714
    new-instance v0, Lflipboard/activities/ShareFragment$5;

    invoke-direct {v0, p0, v1, v3, v4}, Lflipboard/activities/ShareFragment$5;-><init>(Lflipboard/activities/ShareFragment;Lflipboard/service/Flap$JSONResultObserver;Ljava/util/List;Ljava/lang/String;)V

    .line 765
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->x:Ljava/util/Map;

    iget-object v5, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    iget-object v6, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v6, v6, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    iget-object v7, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    iget-object v8, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    check-cast v9, Lflipboard/activities/FlipboardActivity;

    invoke-static/range {v0 .. v9}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Lflipboard/service/Flap$JSONResultObserver;Ljava/util/Map;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    .line 770
    :goto_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hasFlippedIntoMagazine"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 772
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 773
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 664
    :cond_7
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    goto/16 :goto_2

    .line 686
    :cond_8
    sget-object v6, Lflipboard/objs/UsageEventV2$EventDataType;->d:Lflipboard/objs/UsageEventV2$EventDataType;

    goto/16 :goto_4

    .line 691
    :cond_9
    iget-object v6, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v6, v6, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    goto/16 :goto_5

    .line 767
    :cond_a
    iget-object v5, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v6, v0, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    iget-object v7, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    iget-object v8, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    move-object v2, v1

    invoke-static/range {v2 .. v8}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V

    goto :goto_6

    :cond_b
    move-object v1, v2

    move-object v5, v2

    goto/16 :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 126
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    .line 129
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.reference"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    .line 131
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.reference.link"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v1, "extra_section_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_0

    .line 134
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->z:Lflipboard/service/Section;

    .line 138
    :cond_0
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->s:Ljava/lang/String;

    aput-object v1, v0, v2

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 140
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->w:Ljava/lang/String;

    .line 142
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "User wants to share this text: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    aput-object v1, v0, v2

    .line 146
    :cond_1
    invoke-direct {p0}, Lflipboard/activities/ShareFragment;->a()Z

    move-result v1

    .line 148
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->w:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->w:Ljava/lang/String;

    invoke-static {v0, v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;I)Ljava/util/Set;

    move-result-object v0

    .line 150
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 151
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    .line 155
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    if-nez v0, :cond_3

    if-eqz v1, :cond_6

    .line 157
    :cond_3
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->t:Ljava/lang/String;

    new-instance v3, Lflipboard/activities/ShareFragment$1;

    invoke-direct {v3, p0}, Lflipboard/activities/ShareFragment$1;-><init>(Lflipboard/activities/ShareFragment;)V

    new-instance v4, Lflipboard/service/Flap$FlipusRequst;

    invoke-direct {v4, v0, v1}, Lflipboard/service/Flap$FlipusRequst;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v4, v2, v3}, Lflipboard/service/Flap$FlipusRequst;->a(Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$FlipusRequst;

    .line 211
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 213
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/ShareFragment;->a(Ljava/util/List;)V

    .line 215
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->r()V

    .line 226
    :cond_4
    :goto_0
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->q:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->e:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 227
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228
    const/4 v0, 0x0

    .line 229
    iget-object v3, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    if-eqz v3, :cond_5

    .line 230
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.navigating.from"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 232
    :cond_5
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 233
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 234
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 235
    return-void

    .line 217
    :cond_6
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 218
    if-eqz v0, :cond_4

    .line 219
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/ComposeActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->v:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 221
    invoke-virtual {p0, v1}, Lflipboard/activities/ShareFragment;->startActivity(Landroid/content/Intent;)V

    .line 222
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x11

    const/4 v3, -0x2

    .line 417
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 418
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 433
    :goto_0
    return-object v0

    .line 421
    :cond_1
    const v0, 0x7f030122

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLinearLayout;

    .line 422
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_2

    .line 423
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090109

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 424
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 425
    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 426
    invoke-virtual {v0, v2}, Lflipboard/gui/FLLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 428
    :cond_2
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 429
    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 430
    invoke-virtual {v0, v1}, Lflipboard/gui/FLLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 389
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 390
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 438
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 440
    const v0, 0x7f0a031f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->h:Landroid/support/v4/view/ViewPager;

    .line 441
    const v0, 0x7f0a031d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->e:Lflipboard/gui/FLButton;

    .line 442
    const v0, 0x7f0a0320

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLEditText;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->f:Lflipboard/gui/FLEditText;

    .line 443
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->f:Lflipboard/gui/FLEditText;

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lflipboard/gui/FLEditText;->setVisibility(I)V

    .line 444
    const v0, 0x7f0a0321

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->o:Landroid/view/View;

    .line 445
    const v0, 0x7f0a0322

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->p:Lflipboard/gui/FLImageView;

    .line 446
    const v0, 0x7f0a0316

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->g:Landroid/view/ViewGroup;

    .line 447
    const v0, 0x7f0a023a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTabStrip;

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->r:Landroid/support/v4/view/PagerTabStrip;

    move v2, v1

    .line 450
    :goto_1
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->r:Landroid/support/v4/view/PagerTabStrip;

    invoke-virtual {v0}, Landroid/support/v4/view/PagerTabStrip;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 451
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->r:Landroid/support/v4/view/PagerTabStrip;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/PagerTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 452
    instance-of v5, v0, Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 453
    check-cast v0, Landroid/widget/TextView;

    .line 454
    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-static {v5}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 455
    iget-object v5, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 450
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 443
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 459
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->p:Lflipboard/gui/FLImageView;

    sget-object v2, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 461
    new-instance v0, Lflipboard/activities/ShareFragment$MagazineAdapter;

    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lflipboard/activities/ShareFragment$MagazineAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    .line 462
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->n:Ljava/util/List;

    iput-object v2, v0, Lflipboard/activities/ShareFragment$MagazineAdapter;->b:Ljava/util/List;

    .line 463
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->h:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 464
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->h:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 466
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->y()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lflipboard/activities/ShareFragment;->q:Ljava/util/List;

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->x()Ljava/util/List;

    move-result-object v5

    iget-object v0, p0, Lflipboard/activities/ShareFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v7, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v7, v7, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v7

    if-eqz v5, :cond_9

    iget-object v2, v7, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-interface {v5, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    :goto_3
    new-instance v8, Lflipboard/activities/ShareFragment$ServiceToggleButton;

    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v8, v9, v7, v2}, Lflipboard/activities/ShareFragment$ServiceToggleButton;-><init>(Landroid/content/Context;Lflipboard/objs/ConfigService;Z)V

    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f090102

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v10, 0x7f09007d

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v9, v1, v1, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v8, v9}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v10, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_3
    :goto_4
    packed-switch v2, :pswitch_data_0

    :goto_5
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->g:Landroid/view/ViewGroup;

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, v0, Lflipboard/service/Account;->d:Lflipboard/io/Download;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lflipboard/service/Account;->e:Lflipboard/io/Download;

    if-nez v2, :cond_5

    :cond_4
    iget-object v2, p0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    const-string v8, "ShareFragment:initServiceseBar"

    new-instance v9, Lflipboard/activities/ShareFragment$3;

    invoke-direct {v9, p0, v7, v0}, Lflipboard/activities/ShareFragment$3;-><init>(Lflipboard/activities/ShareFragment;Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V

    invoke-virtual {v2, v8, v9}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_5
    invoke-static {v7, v0}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V

    goto/16 :goto_2

    :sswitch_0
    const-string v11, "twitter"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v2, v1

    goto :goto_4

    :sswitch_1
    const-string v11, "facebook"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v2, v3

    goto :goto_4

    :sswitch_2
    const-string v11, "googleplus"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v2, v4

    goto :goto_4

    :pswitch_0
    const v2, 0x7f0a0010

    invoke-virtual {v8, v2}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setId(I)V

    goto :goto_5

    :pswitch_1
    const v2, 0x7f0a000e

    invoke-virtual {v8, v2}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setId(I)V

    goto :goto_5

    :pswitch_2
    const v2, 0x7f0a000f

    invoke-virtual {v8, v2}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setId(I)V

    goto :goto_5

    .line 468
    :cond_6
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->m:Lflipboard/objs/Magazine;

    if-eqz v0, :cond_7

    .line 469
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    iget-object v2, p0, Lflipboard/activities/ShareFragment;->m:Lflipboard/objs/Magazine;

    iput-object v2, v0, Lflipboard/activities/ShareFragment$MagazineAdapter;->c:Lflipboard/objs/Magazine;

    .line 470
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->u:Lflipboard/activities/ShareFragment$MagazineAdapter;

    iget-object v0, v0, Landroid/support/v4/view/PagerAdapter;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 473
    :cond_7
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->B:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 474
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->o:Landroid/view/View;

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 475
    iget-object v0, p0, Lflipboard/activities/ShareFragment;->p:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/activities/ShareFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 477
    :cond_8
    return-void

    :cond_9
    move v2, v1

    goto/16 :goto_3

    .line 466
    nop

    :sswitch_data_0
    .sparse-switch
        -0x5b73d8ad -> :sswitch_2
        -0x369e558d -> :sswitch_0
        0x1da19ac6 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
