.class Lflipboard/activities/SettingsFragment$9;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$9;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 518
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$9;->a:Lflipboard/activities/SettingsFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$9;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v2}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/SwitchContentGuideActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v2, 0x3039

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/SettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 520
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$9;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$9;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    iget-object v0, v0, Lflipboard/objs/ConfigEdition;->a:Ljava/lang/String;

    .line 513
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$9;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
