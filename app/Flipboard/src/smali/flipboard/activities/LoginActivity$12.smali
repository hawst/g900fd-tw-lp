.class Lflipboard/activities/LoginActivity$12;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/UserInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/activities/LoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 645
    iput-object p1, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iput-object p2, p0, Lflipboard/activities/LoginActivity$12;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/LoginActivity$12;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 645
    check-cast p1, Lflipboard/objs/UserInfo;

    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->a:Ljava/lang/String;

    const-string v1, "googleplus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->e(Lflipboard/activities/LoginActivity;)Lflipboard/util/GooglePlusSignInClient;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity$12;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/util/GooglePlusSignInClient;->a(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p1, Lflipboard/objs/UserInfo;->b:Z

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/LoginActivity$12$1;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/LoginActivity$12$1;-><init>(Lflipboard/activities/LoginActivity$12;Lflipboard/objs/UserInfo;)V

    invoke-virtual {v0, p1, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/objs/UserInfo;Lflipboard/util/Observer;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "GoogleSSOLogin failed"

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-static {v0, v1, v2}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    if-eqz v1, :cond_1

    iget-boolean v0, v1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    const v0, 0x7f0d0164

    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v2, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v2}, Lflipboard/activities/LoginActivity;->k(Lflipboard/activities/LoginActivity;)Lflipboard/util/Log;

    move-result-object v2

    const-string v3, "Login to flipboard failed after obtaining Google plus sign-in token: %s"

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    const v3, 0x7f0d011e

    invoke-virtual {v1, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0, v6}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 712
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    .line 714
    const-string v0, "GoogleSSOLogin failed"

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-static {v0, v1, v2}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 716
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->a:Ljava/lang/String;

    const-string v1, "googleplus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->e(Lflipboard/activities/LoginActivity;)Lflipboard/util/GooglePlusSignInClient;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity$12;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/util/GooglePlusSignInClient;->a(Ljava/lang/String;)V

    .line 719
    :cond_0
    const-string v0, "418"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;)V

    .line 729
    :cond_1
    :goto_0
    return-void

    .line 722
    :cond_2
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    .line 723
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_1

    .line 724
    const v1, 0x7f0d0164

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 725
    const v2, 0x7f0d0165

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 726
    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method
