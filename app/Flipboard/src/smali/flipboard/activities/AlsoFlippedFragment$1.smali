.class Lflipboard/activities/AlsoFlippedFragment$1;
.super Ljava/lang/Object;
.source "AlsoFlippedFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/AlsoFlippedFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/AlsoFlippedFragment;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lflipboard/activities/AlsoFlippedFragment$1;->a:Lflipboard/activities/AlsoFlippedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflipboard/objs/FeedItem;

    .line 131
    const/4 v0, 0x0

    .line 132
    iget-object v1, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 133
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 136
    :cond_0
    if-nez v0, :cond_4

    .line 137
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v4, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    iget-object v2, v4, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    iget-object v3, v4, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v1, v0

    .line 139
    :goto_0
    iget-object v2, p0, Lflipboard/activities/AlsoFlippedFragment$1;->a:Lflipboard/activities/AlsoFlippedFragment;

    invoke-virtual {v2}, Lflipboard/gui/dialog/FLDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_1

    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->a()Z

    move-result v3

    if-nez v3, :cond_2

    const v3, 0x7f0d0218

    invoke-virtual {v2, v3}, Lflipboard/activities/AlsoFlippedFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 141
    :cond_1
    :goto_1
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->g:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->d:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v2, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 142
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 143
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/AlsoFlippedFragment$1;->a:Lflipboard/activities/AlsoFlippedFragment;

    invoke-static {v2}, Lflipboard/activities/AlsoFlippedFragment;->a(Lflipboard/activities/AlsoFlippedFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 144
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 146
    iget-object v0, p0, Lflipboard/activities/AlsoFlippedFragment$1;->a:Lflipboard/activities/AlsoFlippedFragment;

    invoke-virtual {v0}, Lflipboard/activities/AlsoFlippedFragment;->a()V

    .line 147
    return-void

    .line 139
    :cond_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "source"

    const-string v5, "alsoFlippedDialog"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "originSectionIdentifier"

    iget-object v5, v2, Lflipboard/activities/AlsoFlippedFragment;->k:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "linkType"

    const-string v5, "magazine"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v3}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v3

    if-nez v3, :cond_3

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    :cond_3
    invoke-virtual {v2, v0}, Lflipboard/activities/AlsoFlippedFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method
