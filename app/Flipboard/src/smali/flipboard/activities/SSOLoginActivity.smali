.class public Lflipboard/activities/SSOLoginActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "SSOLoginActivity.java"


# static fields
.field public static n:Lflipboard/util/Log;


# instance fields
.field private final o:Ljava/lang/String;

.field private p:Lflipboard/objs/ConfigService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "servicelogin"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 36
    const-string v0, "facebook"

    iput-object v0, p0, Lflipboard/activities/SSOLoginActivity;->o:Ljava/lang/String;

    return-void
.end method

.method private j()V
    .locals 7

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x1

    .line 171
    invoke-virtual {p0}, Lflipboard/activities/SSOLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 177
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bz:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bA:I

    if-lez v0, :cond_1

    .line 178
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/SSOLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_2

    .line 179
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    iget v1, v0, Lflipboard/objs/ConfigService;->bz:I

    .line 180
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bA:I

    move v2, v1

    move v1, v0

    .line 186
    :goto_0
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 187
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 188
    if-nez v2, :cond_3

    .line 189
    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 193
    :goto_1
    if-nez v1, :cond_4

    .line 194
    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 199
    :cond_1
    :goto_2
    return-void

    .line 182
    :cond_2
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    iget v1, v0, Lflipboard/objs/ConfigService;->bB:I

    .line 183
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bC:I

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 191
    :cond_3
    int-to-float v2, v2

    invoke-static {v5, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_1

    .line 196
    :cond_4
    int-to-float v1, v1

    invoke-static {v5, v1, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2
.end method


# virtual methods
.method protected final f()V
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SSOLoginActivity;->overridePendingTransition(II)V

    .line 61
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    const-string v0, "login"

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 205
    invoke-direct {p0}, Lflipboard/activities/SSOLoginActivity;->j()V

    .line 206
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 42
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lflipboard/activities/SSOLoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v1, "facebook"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    .line 48
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    if-nez v0, :cond_1

    .line 49
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "No service config in LoginActvity for service=%s"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "facebook"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-virtual {p0}, Lflipboard/activities/SSOLoginActivity;->finish()V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v1, p0, Lflipboard/activities/SSOLoginActivity;->p:Lflipboard/objs/ConfigService;

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_2

    const v0, 0x7f030113

    invoke-virtual {p0, v0}, Lflipboard/activities/SSOLoginActivity;->setContentView(I)V

    invoke-virtual {p0}, Lflipboard/activities/SSOLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setLayout(II)V

    :goto_1
    iput-boolean v6, p0, Lflipboard/activities/SSOLoginActivity;->W:Z

    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v3, "/v1/flipboard/loginWithSSO"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "service"

    aput-object v5, v4, v6

    const-string v5, "facebook"

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v0, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    new-array v0, v8, [Ljava/lang/Object;

    iget-object v3, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v3, v0, v6

    aput-object v2, v0, v7

    const v0, 0x7f0a02fb

    invoke-virtual {p0, v0}, Lflipboard/activities/SSOLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v3, v8}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    invoke-virtual {v3, v6}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/CookieManager;->removeAllCookie()V

    new-instance v3, Lflipboard/activities/SSOLoginActivity$1;

    invoke-direct {v3, p0, p0, v1}, Lflipboard/activities/SSOLoginActivity$1;-><init>(Lflipboard/activities/SSOLoginActivity;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    const-string v1, "facebook"

    const-string v3, "web,"

    const-string v4, "FlipSSOLogin Cancelled"

    invoke-static {p0, v1, v3, v4}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const v0, 0x7f030114

    invoke-virtual {p0, v0}, Lflipboard/activities/SSOLoginActivity;->setContentView(I)V

    invoke-virtual {p0}, Lflipboard/activities/SSOLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setLayout(II)V

    invoke-direct {p0}, Lflipboard/activities/SSOLoginActivity;->j()V

    goto :goto_1
.end method
