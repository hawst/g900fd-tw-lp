.class Lflipboard/activities/ReportIssueActivity$7$3;
.super Ljava/lang/Object;
.source "ReportIssueActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/activities/ReportIssueActivity$7;


# direct methods
.method constructor <init>(Lflipboard/activities/ReportIssueActivity$7;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lflipboard/activities/ReportIssueActivity$7$3;->b:Lflipboard/activities/ReportIssueActivity$7;

    iput-object p2, p0, Lflipboard/activities/ReportIssueActivity$7$3;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 414
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7$3;->b:Lflipboard/activities/ReportIssueActivity$7;

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    invoke-static {v0}, Lflipboard/activities/ReportIssueActivity;->g(Lflipboard/activities/ReportIssueActivity;)Lflipboard/gui/dialog/FLAlertDialog$Builder;

    move-result-object v0

    const-string v1, "Created issue with id %s as user %s. No screenshot uploaded because %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity$7$3;->b:Lflipboard/activities/ReportIssueActivity$7;

    invoke-static {v4}, Lflipboard/activities/ReportIssueActivity$7;->a(Lflipboard/activities/ReportIssueActivity$7;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity$7$3;->b:Lflipboard/activities/ReportIssueActivity$7;

    invoke-static {v4}, Lflipboard/activities/ReportIssueActivity$7;->b(Lflipboard/activities/ReportIssueActivity$7;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity$7$3;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 415
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7$3;->b:Lflipboard/activities/ReportIssueActivity$7;

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity$7$3;->b:Lflipboard/activities/ReportIssueActivity$7;

    invoke-static {v1}, Lflipboard/activities/ReportIssueActivity$7;->a(Lflipboard/activities/ReportIssueActivity$7;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/ReportIssueActivity;->b(Ljava/lang/String;)V

    .line 416
    return-void
.end method
