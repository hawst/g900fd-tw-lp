.class Lflipboard/activities/VideoActivity$3;
.super Landroid/widget/MediaController;
.source "VideoActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/VideoActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/VideoActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lflipboard/activities/VideoActivity$3;->a:Lflipboard/activities/VideoActivity;

    invoke-direct {p0, p2}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public show()V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lflipboard/activities/VideoActivity$3;->a:Lflipboard/activities/VideoActivity;

    .line 460
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    :try_start_0
    invoke-super {p0}, Landroid/widget/MediaController;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 467
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public show(I)V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lflipboard/activities/VideoActivity$3;->a:Lflipboard/activities/VideoActivity;

    .line 443
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/MediaController;->show(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 446
    :catch_0
    move-exception v0

    .line 450
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
