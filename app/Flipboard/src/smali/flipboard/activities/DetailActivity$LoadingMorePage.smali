.class Lflipboard/activities/DetailActivity$LoadingMorePage;
.super Lflipboard/gui/ContainerView;
.source "DetailActivity.java"


# instance fields
.field a:Lflipboard/gui/NoContentView;

.field final synthetic b:Lflipboard/activities/DetailActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/DetailActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1749
    iput-object p1, p0, Lflipboard/activities/DetailActivity$LoadingMorePage;->b:Lflipboard/activities/DetailActivity;

    .line 1750
    invoke-direct {p0, p2}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 1751
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity$LoadingMorePage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity$LoadingMorePage;->setBackgroundColor(I)V

    .line 1754
    const v0, 0x7f0300e0

    invoke-static {p2, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1755
    const v0, 0x7f0a0287

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity$LoadingMorePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/NoContentView;

    iput-object v0, p0, Lflipboard/activities/DetailActivity$LoadingMorePage;->a:Lflipboard/gui/NoContentView;

    .line 1756
    iget-object v0, p0, Lflipboard/activities/DetailActivity$LoadingMorePage;->a:Lflipboard/gui/NoContentView;

    iget-object v1, p1, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setSection(Lflipboard/service/Section;)V

    .line 1757
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 1765
    iget-object v0, p0, Lflipboard/activities/DetailActivity$LoadingMorePage;->a:Lflipboard/gui/NoContentView;

    const v1, 0x7f0a016a

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1766
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1786
    iget-object v0, p0, Lflipboard/activities/DetailActivity$LoadingMorePage;->a:Lflipboard/gui/NoContentView;

    sub-int v1, p4, p2

    invoke-virtual {v0, v2, v2, v1, p5}, Lflipboard/gui/NoContentView;->layout(IIII)V

    .line 1787
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 1776
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1777
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1778
    invoke-virtual {p0, v0, v1}, Lflipboard/activities/DetailActivity$LoadingMorePage;->setMeasuredDimension(II)V

    .line 1781
    iget-object v0, p0, Lflipboard/activities/DetailActivity$LoadingMorePage;->a:Lflipboard/gui/NoContentView;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lflipboard/gui/NoContentView;->measure(II)V

    .line 1782
    return-void
.end method
