.class Lflipboard/activities/CreateMagazineFragment$2;
.super Ljava/lang/Object;
.source "CreateMagazineFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/CreateMagazineFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateMagazineFragment;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lflipboard/activities/CreateMagazineFragment$2;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 156
    iget-object v7, p0, Lflipboard/activities/CreateMagazineFragment$2;->a:Lflipboard/activities/CreateMagazineFragment;

    iget-boolean v0, v7, Lflipboard/activities/CreateMagazineFragment;->g:Z

    if-eqz v0, :cond_2

    new-instance v6, Lflipboard/activities/CreateMagazineFragment$5;

    invoke-direct {v6, v7}, Lflipboard/activities/CreateMagazineFragment$5;-><init>(Lflipboard/activities/CreateMagazineFragment;)V

    iget-object v5, v7, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->c:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->d:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "public"

    :goto_0
    iput-object v0, v5, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    if-eqz v0, :cond_1

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    iget-object v0, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    const-string v1, "no_category_selected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    iget-object v0, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    iput-object v0, v5, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    :goto_1
    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v5}, Lflipboard/service/User;->a(Lflipboard/objs/Magazine;)V

    iget-object v2, v1, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    new-instance v0, Lflipboard/service/Flap$EditMagazineRequest;

    invoke-direct {v0, v2, v1}, Lflipboard/service/Flap$EditMagazineRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iget-object v1, v5, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    iget-object v2, v5, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    iget-object v3, v5, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iget-object v4, v5, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    iget-object v5, v5, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap$EditMagazineRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$EditMagazineRequest;

    new-instance v0, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    const v1, 0x7f0d00d2

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    new-instance v1, Lflipboard/activities/CreateMagazineFragment$6;

    invoke-direct {v1, v7, v6}, Lflipboard/activities/CreateMagazineFragment$6;-><init>(Lflipboard/activities/CreateMagazineFragment;Lflipboard/service/Flap$CancellableJSONResultObserver;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {v7}, Lflipboard/activities/CreateMagazineFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "editing_magazine"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 158
    :goto_2
    return-void

    .line 156
    :cond_0
    const-string v0, "private"

    goto :goto_0

    :cond_1
    iput-object v4, v5, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->c:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lflipboard/activities/CreateMagazineFragment$7;

    invoke-direct {v5, v7}, Lflipboard/activities/CreateMagazineFragment$7;-><init>(Lflipboard/activities/CreateMagazineFragment;)V

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v3, "public"

    :goto_3
    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    if-eqz v0, :cond_3

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    iget-object v0, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    const-string v2, "no_category_selected"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    iget-object v4, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    :cond_3
    iget-object v6, v7, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v8, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v7, Lflipboard/activities/CreateMagazineFragment;->d:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lflipboard/service/Flap$CreateMagazineRequest;

    invoke-direct {v0, v6, v8}, Lflipboard/service/Flap$CreateMagazineRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$CreateMagazineRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$CreateMagazineRequest;

    new-instance v0, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    new-instance v1, Lflipboard/activities/CreateMagazineFragment$8;

    invoke-direct {v1, v7, v5}, Lflipboard/activities/CreateMagazineFragment$8;-><init>(Lflipboard/activities/CreateMagazineFragment;Lflipboard/service/Flap$CancellableJSONResultObserver;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {v7}, Lflipboard/activities/CreateMagazineFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "creating_magazine"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const-string v3, "private"

    goto :goto_3
.end method
