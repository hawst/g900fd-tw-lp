.class Lflipboard/activities/DetailActivity$6;
.super Ljava/lang/Object;
.source "DetailActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:Lflipboard/activities/DetailActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/DetailActivity;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 522
    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/activities/DetailActivity$17;->a:[I

    invoke-virtual {p2}, Lflipboard/service/Section$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lflipboard/activities/DetailActivity$6;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity$6;->a:I

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    invoke-static {v0, v3}, Lflipboard/activities/DetailActivity;->a(Lflipboard/activities/DetailActivity;Z)Z

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v1, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v1, v1, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v2, v2, Lflipboard/activities/DetailActivity;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, v0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    invoke-virtual {v0}, Lflipboard/activities/DetailActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/DetailActivity$6$1;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$6$1;-><init>(Lflipboard/activities/DetailActivity$6;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lflipboard/activities/DetailActivity$6;->a:I

    const/16 v1, 0x28

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflipboard/activities/DetailActivity;->a(Lflipboard/activities/DetailActivity;Z)Z

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x32

    new-instance v2, Lflipboard/activities/DetailActivity$6$2;

    invoke-direct {v2, p0}, Lflipboard/activities/DetailActivity$6$2;-><init>(Lflipboard/activities/DetailActivity$6;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Section has no items in detailview"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "DetailView is null after section load, showing no-content view instead"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    invoke-virtual {v0}, Lflipboard/activities/DetailActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/DetailActivity$6$3;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$6$3;-><init>(Lflipboard/activities/DetailActivity$6;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity$6;->b:Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/DetailActivity$6$4;

    invoke-direct {v1, p0, p3}, Lflipboard/activities/DetailActivity$6$4;-><init>(Lflipboard/activities/DetailActivity$6;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
