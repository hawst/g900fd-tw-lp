.class Lflipboard/activities/ReportIssueActivity$7;
.super Ljava/lang/Object;
.source "ReportIssueActivity.java"

# interfaces
.implements Lflipboard/service/FlipboardManager$CreateIssueObserver;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/activities/ReportIssueActivity;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 361
    iput-object p1, p0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    iput-object p2, p0, Lflipboard/activities/ReportIssueActivity$7;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363
    const-string v0, "jira-bot"

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ReportIssueActivity$7;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/ReportIssueActivity$7;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 373
    invoke-static {}, Lflipboard/activities/ReportIssueActivity;->j()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/activities/ReportIssueActivity$7;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 374
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 367
    invoke-static {}, Lflipboard/activities/ReportIssueActivity;->j()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 368
    iput-object p1, p0, Lflipboard/activities/ReportIssueActivity$7;->d:Ljava/lang/String;

    .line 369
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ReportIssueActivity$7$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ReportIssueActivity$7$2;-><init>(Lflipboard/activities/ReportIssueActivity$7;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 406
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 378
    iput-object p1, p0, Lflipboard/activities/ReportIssueActivity$7;->c:Ljava/lang/String;

    .line 379
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    invoke-static {v0}, Lflipboard/activities/ReportIssueActivity;->d(Lflipboard/activities/ReportIssueActivity;)Z

    .line 380
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 384
    invoke-static {}, Lflipboard/activities/ReportIssueActivity;->j()Lflipboard/util/Log;

    .line 385
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ReportIssueActivity$7$1;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/ReportIssueActivity$7$1;-><init>(Lflipboard/activities/ReportIssueActivity$7;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 394
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 410
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity$7;->b:Lflipboard/activities/ReportIssueActivity;

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ReportIssueActivity$7$3;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/ReportIssueActivity$7$3;-><init>(Lflipboard/activities/ReportIssueActivity$7;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 418
    return-void
.end method
