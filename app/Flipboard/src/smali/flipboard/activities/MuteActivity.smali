.class public Lflipboard/activities/MuteActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "MuteActivity.java"


# instance fields
.field n:Lflipboard/service/User;

.field o:Lflipboard/objs/UserState$State;

.field p:Lflipboard/activities/MuteActivity$MuteListAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 66
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lflipboard/activities/MuteActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lflipboard/activities/MuteActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v0, p0, Lflipboard/activities/MuteActivity;->n:Lflipboard/service/User;

    .line 44
    iget-object v0, p0, Lflipboard/activities/MuteActivity;->n:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->k()Lflipboard/objs/UserState;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iput-object v0, p0, Lflipboard/activities/MuteActivity;->o:Lflipboard/objs/UserState$State;

    .line 46
    new-instance v0, Lflipboard/activities/MuteActivity$MuteListAdapter;

    invoke-direct {v0, p0}, Lflipboard/activities/MuteActivity$MuteListAdapter;-><init>(Lflipboard/activities/MuteActivity;)V

    iput-object v0, p0, Lflipboard/activities/MuteActivity;->p:Lflipboard/activities/MuteActivity$MuteListAdapter;

    .line 47
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/MuteActivity;->setContentView(I)V

    .line 49
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/MuteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 50
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 52
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/MuteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 53
    const v1, 0x7f0d02cd

    invoke-virtual {p0, v1}, Lflipboard/activities/MuteActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 55
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/MuteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 57
    const v1, 0x7f0300db

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 58
    const v1, 0x7f0a0142

    invoke-virtual {p0, v1}, Lflipboard/activities/MuteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 59
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 61
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 63
    iget-object v1, p0, Lflipboard/activities/MuteActivity;->p:Lflipboard/activities/MuteActivity$MuteListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 249
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 252
    iget-object v4, p0, Lflipboard/activities/MuteActivity;->p:Lflipboard/activities/MuteActivity$MuteListAdapter;

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move-object v3, v2

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    iget-boolean v6, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->c:Z

    if-eqz v6, :cond_0

    iput-boolean v7, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->c:Z

    iget-boolean v6, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->b:Z

    if-eqz v6, :cond_2

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    iget-object v0, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->a:Lflipboard/objs/UserState$MutedAuthor;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_3
    iget-object v0, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->a:Lflipboard/objs/UserState$MutedAuthor;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_5

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    iget-object v0, v0, Lflipboard/activities/MuteActivity;->n:Lflipboard/service/User;

    invoke-virtual {v0, v3, v2}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/lang/String;)V

    :cond_5
    if-eqz v1, :cond_6

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    iget-object v0, v0, Lflipboard/activities/MuteActivity;->n:Lflipboard/service/User;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->b(Ljava/util/List;Ljava/lang/String;)V

    :cond_6
    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->b:Ljava/util/List;

    if-eqz v0, :cond_d

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v2

    :cond_7
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    iget-boolean v5, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->c:Z

    if-eqz v5, :cond_7

    iput-boolean v7, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->c:Z

    iget-boolean v5, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->b:Z

    if-eqz v5, :cond_9

    if-nez v2, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_8
    iget-object v0, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->a:Lflipboard/objs/UserState$MutedAuthor;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_9
    if-nez v1, :cond_a

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_a
    iget-object v0, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->a:Lflipboard/objs/UserState$MutedAuthor;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_b
    if-eqz v2, :cond_c

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    iget-object v0, v0, Lflipboard/activities/MuteActivity;->n:Lflipboard/service/User;

    sget-object v3, Lflipboard/service/Section;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/lang/String;)V

    :cond_c
    if-eqz v1, :cond_d

    iget-object v0, v4, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    iget-object v0, v0, Lflipboard/activities/MuteActivity;->n:Lflipboard/service/User;

    sget-object v2, Lflipboard/service/Section;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->b(Ljava/util/List;Ljava/lang/String;)V

    .line 253
    :cond_d
    return-void
.end method
