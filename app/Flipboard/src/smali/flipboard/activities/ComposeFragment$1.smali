.class Lflipboard/activities/ComposeFragment$1;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lflipboard/activities/ComposeFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    const v5, 0x7f0a00e6

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 357
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "twitter"

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Account;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 358
    :goto_0
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$1;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 359
    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->b(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->b(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v4}, Lflipboard/activities/ComposeFragment;->b(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 360
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->b(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 361
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$1;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 362
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 363
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 368
    :goto_1
    return-void

    :cond_0
    move v1, v3

    .line 357
    goto :goto_0

    .line 365
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;)V

    .line 366
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$1;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v0, v2}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;Z)Z

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 344
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 349
    return-void
.end method
