.class public Lflipboard/activities/ComposeFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "ComposeFragment.java"


# static fields
.field static a:Lflipboard/util/Log;

.field static final synthetic e:Z


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lflipboard/service/Section;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Lflipboard/service/FlipboardManager;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lflipboard/service/Account;

.field private h:Lflipboard/json/FLObject;

.field private i:Lflipboard/service/Flap$ShareListRequest;

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Lflipboard/gui/dialog/FLProgressDialogFragment;

.field private final p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:Landroid/os/Bundle;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/activities/ComposeFragment;->e:Z

    .line 63
    const-string v0, "compose"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 82
    const/16 v0, 0x14

    iput v0, p0, Lflipboard/activities/ComposeFragment;->p:I

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment;->u:Z

    .line 93
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    .line 103
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/activities/ComposeFragment;->setRetainInstance(Z)V

    .line 104
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;)I
    .locals 6

    .prologue
    .line 1355
    const-wide/16 v2, 0x0

    .line 1356
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1357
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 1358
    if-lez v1, :cond_0

    const/16 v4, 0x7f

    if-ge v1, v4, :cond_0

    .line 1359
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    .line 1356
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1361
    :cond_0
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    goto :goto_1

    .line 1364
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment;Lflipboard/gui/dialog/FLProgressDialogFragment;)Lflipboard/gui/dialog/FLProgressDialogFragment;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lflipboard/activities/ComposeFragment;->o:Lflipboard/gui/dialog/FLProgressDialogFragment;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Account;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lflipboard/activities/ComposeFragment;->x:Ljava/util/Map;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0a00ea

    .line 1247
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1248
    if-eqz v0, :cond_0

    .line 1249
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1252
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lflipboard/util/ShareHelper;->a(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1253
    if-nez v1, :cond_2

    .line 1254
    if-eqz v0, :cond_1

    .line 1255
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1316
    :cond_1
    :goto_0
    return-void

    .line 1260
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/activities/ComposeFragment$12;

    invoke-direct {v2, p0, v1}, Lflipboard/activities/ComposeFragment$12;-><init>(Lflipboard/activities/ComposeFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/Flap$JSONResultObserver;)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment;Lflipboard/objs/ConfigService;)Z
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/objs/ConfigService;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment;Z)Z
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lflipboard/activities/ComposeFragment;->u:Z

    return p1
.end method

.method private a(Lflipboard/objs/ConfigService;)Z
    .locals 1

    .prologue
    .line 766
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->av:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1337
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1338
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1339
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget v1, v1, Lflipboard/model/ConfigSetting;->ShortenedURLCharacterCount:I

    add-int/2addr v0, v1

    .line 1341
    :cond_0
    return v0
.end method

.method static synthetic b(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/ComposeFragment;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method static synthetic c()Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v7, 0x8c

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1029
    const v0, 0x7f0a00e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1030
    const v1, 0x7f0a00e6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1035
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 1036
    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v5, :cond_0

    const-string v5, "twitter"

    iget-object v6, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v6, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v6, v6, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1037
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1038
    invoke-direct {p0, v2}, Lflipboard/activities/ComposeFragment;->b(Ljava/lang/String;)I

    move-result v2

    .line 1043
    :cond_0
    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v5, v5, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    if-eqz v5, :cond_9

    .line 1044
    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v5, v5, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const-string v6, "twitter"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v5, v5, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const-string v6, "tcweibo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v5, v5, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const-string v6, "qzone"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1045
    :cond_1
    if-lez v2, :cond_2

    if-gt v2, v7, :cond_2

    move v1, v3

    .line 1058
    :goto_0
    if-eqz v1, :cond_8

    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    if-nez v1, :cond_8

    .line 1060
    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1061
    return-void

    :cond_2
    move v1, v4

    .line 1045
    goto :goto_0

    .line 1046
    :cond_3
    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v5, v5, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const-string v6, "weibo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1047
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->a(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1048
    if-lez v1, :cond_4

    if-gt v1, v7, :cond_4

    move v1, v3

    goto :goto_0

    :cond_4
    move v1, v4

    goto :goto_0

    .line 1050
    :cond_5
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    if-lez v2, :cond_7

    :cond_6
    move v1, v3

    goto :goto_0

    :cond_7
    move v1, v4

    goto :goto_0

    :cond_8
    move v3, v4

    .line 1058
    goto :goto_1

    :cond_9
    move v1, v4

    goto :goto_0
.end method

.method static synthetic d(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/ComposeFragment;)Lflipboard/gui/dialog/FLProgressDialogFragment;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->o:Lflipboard/gui/dialog/FLProgressDialogFragment;

    return-object v0
.end method

.method private e()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 567
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->x()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 568
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    .line 569
    if-eqz v3, :cond_0

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->g:Z

    if-eqz v0, :cond_0

    .line 570
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 573
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 574
    return-object v1
.end method

.method private f()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1002
    sget-boolean v0, Lflipboard/activities/ComposeFragment;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1003
    :cond_0
    const-string v0, "twitter"

    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1006
    const-string v0, "RT @%s: %s"

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->s:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->r:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1021
    :goto_0
    return-object v0

    .line 1007
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1008
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->r:Ljava/lang/String;

    .line 1009
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1011
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1012
    const-string v0, "%s%s"

    .line 1016
    :goto_1
    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v3

    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1014
    :cond_2
    const-string v0, "%s %s"

    goto :goto_1

    .line 1019
    :cond_3
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic f(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/ComposeFragment;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->x:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->B:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic j(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Flap$ShareListRequest;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    return-object v0
.end method

.method static synthetic k(Lflipboard/activities/ComposeFragment;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment;->m:Z

    return v0
.end method

.method static synthetic l(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 554
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/ChooseAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 555
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v1, :cond_0

    .line 556
    const-string v1, "flipboard.extra.selectedAccount"

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    invoke-virtual {v2}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 558
    :cond_0
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    if-eqz v1, :cond_1

    .line 559
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-static {v0, v1}, Lflipboard/util/ShareHelper;->a(Landroid/content/Intent;Lflipboard/json/FLObject;)V

    .line 561
    :cond_1
    const/16 v1, 0x1e79

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ComposeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 562
    return-void
.end method

.method final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1068
    const v0, 0x7f0a00e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1069
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1070
    const v0, 0x7f0a00eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1076
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v3, :cond_0

    const-string v3, "twitter"

    iget-object v4, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v4, v4, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1077
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1078
    invoke-direct {p0, v1}, Lflipboard/activities/ComposeFragment;->b(Ljava/lang/String;)I

    move-result v1

    .line 1082
    :cond_0
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v3, :cond_1

    const-string v3, "weibo"

    iget-object v4, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v4, v4, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1083
    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->a(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1085
    :cond_1
    rsub-int v2, v1, 0x8c

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1086
    const/16 v2, 0x8c

    if-le v1, v2, :cond_2

    .line 1087
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1091
    :goto_0
    invoke-direct {p0, p1}, Lflipboard/activities/ComposeFragment;->c(Landroid/view/View;)V

    .line 1092
    return-void

    .line 1089
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method final a(Landroid/view/View;Lflipboard/service/Account;Lflipboard/json/FLObject;)V
    .locals 11

    .prologue
    const/16 v4, 0x8

    const/4 v10, 0x2

    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 866
    sget-boolean v0, Lflipboard/activities/ComposeFragment;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 868
    :cond_0
    if-nez p2, :cond_1

    .line 869
    const v0, 0x7f0a00e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    .line 870
    const v0, 0x7f0a00e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0094

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 998
    :goto_0
    return-void

    .line 874
    :cond_1
    iget-object v0, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->n:Ljava/lang/String;

    .line 875
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->n:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v5

    .line 878
    iget-boolean v0, v5, Lflipboard/objs/ConfigService;->av:Z

    if-nez v0, :cond_2

    move-object p3, v1

    .line 884
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eq v0, p2, :cond_4

    .line 885
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    if-eqz v0, :cond_3

    .line 886
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    invoke-virtual {v0}, Lflipboard/service/Flap$ShareListRequest;->cancel()Z

    .line 887
    iput-object v1, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    .line 889
    :cond_3
    iput-boolean v3, p0, Lflipboard/activities/ComposeFragment;->m:Z

    .line 891
    :cond_4
    iput-object p2, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    .line 892
    iput-object p3, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    .line 896
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {v0, v2}, Lflipboard/service/Account;->a(Lflipboard/json/FLObject;)V

    .line 899
    sget-boolean v0, Lflipboard/activities/ComposeFragment;->e:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    invoke-direct {p0, v5}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment;->m:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v6, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v7, Lflipboard/activities/ComposeFragment$9;

    invoke-direct {v7, p0, v0, v5}, Lflipboard/activities/ComposeFragment$9;-><init>(Lflipboard/activities/ComposeFragment;Lflipboard/service/Account;Lflipboard/objs/ConfigService;)V

    invoke-virtual {v2, v6, v5, v7}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$ShareListRequest;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    .line 901
    :cond_6
    const v0, 0x7f0a00e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iget-object v2, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v2}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 902
    const v0, 0x7f0a00e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iget-object v2, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 903
    const v0, 0x7f0a00e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iget-object v2, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 906
    invoke-virtual {v5}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    .line 907
    iget-boolean v0, v5, Lflipboard/objs/ConfigService;->av:Z

    if-eqz v0, :cond_f

    .line 908
    invoke-direct {p0, v5}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 911
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->size()I

    move-result v0

    if-ne v0, v9, :cond_7

    .line 912
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_19

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 913
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 919
    :goto_1
    sget-boolean v5, Lflipboard/activities/ComposeFragment;->e:Z

    if-nez v5, :cond_8

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 917
    :cond_7
    const-string v0, "%d %s"

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {v7}, Lflipboard/json/FLObject;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v5}, Lflipboard/objs/ConfigService;->i()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v9

    invoke-static {v0, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 920
    :cond_8
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%s%s"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d01e3

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    aput-object v0, v6, v9

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 928
    :cond_9
    :goto_2
    const v0, 0x7f0a00e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 931
    const v0, 0x7f0a00e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    if-eqz v0, :cond_10

    move v0, v3

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 933
    const v0, 0x7f0a00eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 934
    const-string v2, "twitter"

    iget-object v5, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "weibo"

    iget-object v5, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "tcweibo"

    iget-object v5, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "qzone"

    iget-object v5, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 935
    :cond_a
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-ne v2, v4, :cond_b

    .line 936
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 937
    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 938
    invoke-virtual {v2, v9}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 939
    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 940
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 953
    :cond_b
    :goto_4
    const v0, 0x7f0a00e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 954
    iget-boolean v2, p0, Lflipboard/activities/ComposeFragment;->u:Z

    if-nez v2, :cond_14

    .line 955
    const-string v2, "twitter"

    iget-object v4, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 956
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->q:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 957
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 967
    :cond_c
    :goto_5
    iput-boolean v3, p0, Lflipboard/activities/ComposeFragment;->u:Z

    .line 993
    :cond_d
    :goto_6
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v3, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->W:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 994
    if-eqz v2, :cond_e

    move-object v1, v2

    :cond_e
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 996
    invoke-virtual {p0, p1}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;)V

    .line 997
    invoke-virtual {p0, p1}, Lflipboard/activities/ComposeFragment;->b(Landroid/view/View;)V

    goto/16 :goto_0

    .line 924
    :cond_f
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 925
    iget-object v6, v5, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    iget-object v5, v5, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v6, v5}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    goto/16 :goto_2

    :cond_10
    move v0, v4

    .line 931
    goto/16 :goto_3

    .line 943
    :cond_11
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_b

    .line 944
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {v2, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 945
    const-wide/16 v6, 0x2bc

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 946
    invoke-virtual {v2, v9}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 947
    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 948
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 958
    :cond_12
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 959
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 962
    :cond_13
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 970
    :cond_14
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 971
    iget-object v4, p0, Lflipboard/activities/ComposeFragment;->q:Ljava/lang/String;

    if-eqz v4, :cond_17

    .line 973
    const-string v4, "twitter"

    iget-object v5, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 974
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_15

    .line 975
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 976
    :cond_15
    iget-object v4, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    if-eqz v4, :cond_d

    const-string v4, "http://"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const-string v4, "https://"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 978
    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 979
    const-string v2, "%s%s"

    .line 983
    :goto_7
    new-array v4, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v3

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    aput-object v3, v4, v9

    invoke-static {v2, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 981
    :cond_16
    const-string v2, "%s %s"

    goto :goto_7

    .line 986
    :cond_17
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    if-eqz v3, :cond_d

    const-string v3, "twitter"

    iget-object v4, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 987
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 988
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 990
    :cond_18
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_19
    move-object v0, v1

    goto/16 :goto_1
.end method

.method final b(Landroid/view/View;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x2bc

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1099
    const v0, 0x7f0a00e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 1100
    const v1, 0x7f0a00e8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1101
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v2, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1102
    :cond_0
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1103
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1104
    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1105
    invoke-virtual {v2, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1106
    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1107
    invoke-virtual {v0, v7}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 1108
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1109
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1127
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lflipboard/activities/ComposeFragment;->c(Landroid/view/View;)V

    .line 1128
    return-void

    .line 1112
    :cond_2
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-ne v2, v7, :cond_3

    .line 1113
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1114
    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1115
    invoke-virtual {v2, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1116
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1117
    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1118
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 1119
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1120
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1122
    :cond_3
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1123
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    const v0, 0x7f0a00e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v2, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    new-instance v3, Lflipboard/activities/ComposeFragment$10;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/activities/ComposeFragment$10;-><init>(Lflipboard/activities/ComposeFragment;Lflipboard/gui/FLTextView;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1321
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1322
    if-nez v0, :cond_0

    move v0, v1

    .line 1327
    :goto_0
    return v0

    .line 1325
    :cond_0
    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1326
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1327
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment;->u:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1199
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 1200
    const/4 v0, 0x0

    .line 1201
    const/16 v1, 0x1e7a

    if-ne p1, v1, :cond_4

    .line 1202
    sget-object v0, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 1203
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    .line 1221
    :cond_0
    if-eqz v0, :cond_1

    .line 1222
    invoke-direct {p0, v0}, Lflipboard/activities/ComposeFragment;->a(Ljava/lang/String;)V

    .line 1225
    :cond_1
    const/16 v0, 0x1e79

    if-ne p1, v0, :cond_3

    .line 1226
    if-eqz p3, :cond_3

    .line 1227
    const-string v0, "flipboard.extra.selectedAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1228
    invoke-static {p3}, Lflipboard/util/ShareHelper;->a(Landroid/content/Intent;)Lflipboard/json/FLObject;

    move-result-object v2

    .line 1229
    if-eqz v1, :cond_3

    .line 1231
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment;->e()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    .line 1232
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 1233
    invoke-virtual {v0}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1235
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v2}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;Lflipboard/service/Account;Lflipboard/json/FLObject;)V

    .line 1243
    :cond_3
    :goto_0
    return-void

    .line 1204
    :cond_4
    const/16 v1, 0x1e7b

    if-ne p1, v1, :cond_0

    .line 1206
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1208
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/net/Uri;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 1209
    sget-object v1, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    .line 1211
    if-nez v0, :cond_0

    .line 1212
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1213
    if-eqz v0, :cond_3

    .line 1214
    const v1, 0x7f0d033d

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    .line 110
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    .line 113
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "launched_by_flipboard_activity"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment;->z:Z

    .line 114
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "extra_section_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_1

    .line 116
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->B:Lflipboard/service/Section;

    .line 119
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-nez v0, :cond_6

    .line 120
    :cond_2
    sget-object v0, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    const-string v1, "User id not exist because user has not built TOC yet, no way to compose anything. Cancelling compose"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 122
    if-nez v0, :cond_5

    .line 202
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v0, v2

    .line 113
    goto :goto_0

    .line 126
    :cond_5
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->m()V

    .line 127
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 128
    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 129
    invoke-virtual {p0, v1}, Lflipboard/activities/ComposeFragment;->startActivity(Landroid/content/Intent;)V

    .line 130
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    goto :goto_1

    .line 135
    :cond_6
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment;->e()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    .line 136
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 138
    :cond_7
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 139
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->a()V

    .line 143
    :cond_8
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_a

    .line 145
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    .line 149
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "sharePhoto.jpg"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    .line 153
    :cond_a
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-nez v0, :cond_e

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 155
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 156
    const-string v3, "pref_compose_account_id"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_b

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v3, :cond_b

    .line 158
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    .line 161
    :cond_b
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-nez v0, :cond_d

    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_d

    .line 163
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_c
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 164
    iget-object v4, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    if-eqz v4, :cond_c

    const-string v4, "weibo"

    iget-object v5, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v5, v5, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 165
    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    goto :goto_2

    .line 169
    :cond_d
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-nez v0, :cond_e

    .line 171
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    .line 175
    :cond_e
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    if-eqz v0, :cond_f

    .line 176
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.reference"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->q:Ljava/lang/String;

    .line 177
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.reference.title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->r:Ljava/lang/String;

    .line 178
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.reference.author"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->s:Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.reference.link"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.reference.service"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->v:Ljava/lang/String;

    .line 181
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "flipboard.extra.post.service.id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->w:Ljava/lang/String;

    .line 183
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->t:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    .line 186
    sget-object v0, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "User wants to share this text: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_f
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    invoke-static {v0, v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;I)Ljava/util/Set;

    move-result-object v0

    .line 191
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_10

    .line 192
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    const-string v3, "twitter"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 195
    const-string v0, ""

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    .line 198
    :cond_10
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 199
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment;->z:Z

    if-nez v0, :cond_11

    :goto_3
    iput-boolean v1, p0, Lflipboard/activities/ComposeFragment;->u:Z

    goto/16 :goto_1

    :cond_11
    move v1, v2

    goto :goto_3
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    .line 1167
    const v0, 0x7f0a004c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 1168
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getOverflowItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 1169
    invoke-interface {v0}, Landroid/view/MenuItem;->getGroupId()I

    move-result v2

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-interface {v0}, Landroid/view/MenuItem;->getOrder()I

    move-result v4

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {p1, v2, v3, v4, v5}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0

    .line 1171
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1172
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const v10, 0x7f0d033d

    const/16 v9, 0x2710

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 211
    const v0, 0x7f030041

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 212
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 213
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 214
    const v3, 0x7f0f0001

    invoke-virtual {v0, v3, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 215
    const v0, 0x7f0a039a

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 216
    const v0, 0x7f0a039b

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 217
    const v0, 0x7f0a039c

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 218
    const v0, 0x7f0a039d

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 219
    const v0, 0x7f0a039e

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 221
    const v0, 0x7f0a004c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 222
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 223
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 224
    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 228
    const v0, 0x7f0a00e6

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 229
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 231
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {p0, v4, v1, v3}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;Lflipboard/service/Account;Lflipboard/json/FLObject;)V

    .line 237
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 238
    invoke-virtual {p0, v4}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;)V

    .line 239
    invoke-virtual {p0, v4}, Lflipboard/activities/ComposeFragment;->b(Landroid/view/View;)V

    .line 242
    :cond_0
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    if-eqz v1, :cond_e

    .line 243
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->y:Landroid/os/Bundle;

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 246
    :goto_0
    if-eqz v1, :cond_1

    .line 247
    sget-object v3, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v7

    .line 249
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v1, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/net/Uri;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 250
    if-nez v1, :cond_5

    .line 251
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    .line 252
    invoke-virtual {v1, v10}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 309
    :cond_1
    :goto_1
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->w:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 310
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->w:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 311
    iget-object v3, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 318
    :cond_2
    :goto_2
    if-nez v2, :cond_c

    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->v:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 319
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_3
    add-int/lit8 v3, v1, -0x1

    if-lez v1, :cond_c

    .line 320
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->f:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/Account;

    .line 321
    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->v:Ljava/lang/String;

    iget-object v6, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v6, v6, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 327
    :goto_4
    if-eqz v1, :cond_3

    .line 328
    iget-object v2, v1, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v2, v2, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    invoke-virtual {p0, v4, v1, v2}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;Lflipboard/service/Account;Lflipboard/json/FLObject;)V

    .line 331
    :cond_3
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 332
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 334
    invoke-virtual {p0, v4}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;)V

    .line 339
    :cond_4
    new-instance v1, Lflipboard/activities/ComposeFragment$1;

    invoke-direct {v1, p0, v4}, Lflipboard/activities/ComposeFragment$1;-><init>(Lflipboard/activities/ComposeFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 372
    const v0, 0x7f0a00e0

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 373
    new-instance v1, Lflipboard/activities/ComposeFragment$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$2;-><init>(Lflipboard/activities/ComposeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    return-object v4

    .line 254
    :cond_5
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-static {v3}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v5

    .line 256
    if-eqz v5, :cond_9

    const-string v6, "text/plain"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 258
    :try_start_0
    sget-object v5, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    .line 260
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 261
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 264
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 267
    :cond_6
    :goto_5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-ge v7, v9, :cond_8

    if-eqz v1, :cond_8

    .line 268
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    if-le v7, v9, :cond_7

    .line 269
    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    rsub-int v8, v8, 0x270d

    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v1, "..."

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :goto_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 275
    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 283
    :catch_0
    move-exception v1

    .line 284
    :try_start_2
    const-class v6, Lflipboard/activities/ComposeActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v6

    sget-object v7, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 286
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 287
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 291
    :catch_1
    move-exception v1

    .line 290
    const-class v3, Lflipboard/activities/ComposeActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v3

    sget-object v5, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v3, v5, v2, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 272
    :cond_7
    :try_start_4
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_6

    .line 286
    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 287
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 278
    :cond_8
    :try_start_6
    sget-object v1, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    .line 281
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/ComposeFragment;->A:Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 286
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 287
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_1

    .line 294
    :cond_9
    sget-object v3, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v7

    .line 295
    if-eqz v1, :cond_a

    .line 296
    invoke-direct {p0, v1}, Lflipboard/activities/ComposeFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 298
    :cond_a
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    .line 299
    if-eqz v1, :cond_1

    .line 300
    invoke-virtual {v1, v10}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    move v1, v3

    .line 325
    goto/16 :goto_3

    :cond_c
    move-object v1, v2

    goto/16 :goto_4

    :cond_d
    move-object v2, v1

    goto/16 :goto_2

    :cond_e
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    invoke-virtual {v0}, Lflipboard/service/Flap$ShareListRequest;->cancel()Z

    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ComposeFragment;->i:Lflipboard/service/Flap$ShareListRequest;

    .line 390
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 391
    return-void
.end method

.method public send()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 579
    sget-object v0, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    .line 580
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 581
    if-eqz v0, :cond_0

    .line 582
    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 583
    if-eqz v1, :cond_0

    .line 584
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    .line 585
    if-eqz v3, :cond_0

    .line 586
    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 591
    :cond_0
    iget-object v1, p0, Lflipboard/activities/ComposeFragment;->g:Lflipboard/service/Account;

    if-nez v1, :cond_2

    .line 593
    sget-object v1, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    const-string v2, "No services logged in, no way to compose anything. Cancelling compose"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 594
    if-eqz v0, :cond_1

    .line 595
    const v1, 0x7f0d01e7

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 762
    :cond_1
    :goto_0
    return-void

    .line 601
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 602
    if-eqz v0, :cond_5

    .line 603
    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 604
    if-eqz v0, :cond_5

    .line 605
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 609
    :goto_1
    if-eqz v4, :cond_3

    .line 611
    :goto_2
    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    if-eqz v0, :cond_4

    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/activities/ComposeFragment;->h:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 614
    :goto_3
    new-instance v1, Lflipboard/activities/ComposeFragment$6;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$6;-><init>(Lflipboard/activities/ComposeFragment;)V

    .line 687
    new-instance v0, Lflipboard/activities/ComposeFragment$7;

    invoke-direct {v0, p0, v1, v3, v4}, Lflipboard/activities/ComposeFragment$7;-><init>(Lflipboard/activities/ComposeFragment;Lflipboard/service/Flap$CancellableJSONResultObserver;Ljava/util/List;Ljava/lang/String;)V

    .line 747
    new-instance v2, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    .line 748
    const v5, 0x7f0d029d

    invoke-virtual {v2, v5}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    .line 749
    new-instance v5, Lflipboard/activities/ComposeFragment$8;

    invoke-direct {v5, p0, v0, v1}, Lflipboard/activities/ComposeFragment$8;-><init>(Lflipboard/activities/ComposeFragment;Lflipboard/service/Flap$CancellableJSONResultObserver;Lflipboard/service/Flap$CancellableJSONResultObserver;)V

    iput-object v5, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 760
    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "uploading"

    invoke-virtual {v2, v5, v6}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 761
    iget-object v2, p0, Lflipboard/activities/ComposeFragment;->x:Ljava/util/Map;

    iget-object v5, p0, Lflipboard/activities/ComposeFragment;->q:Ljava/lang/String;

    iget-object v6, p0, Lflipboard/activities/ComposeFragment;->n:Ljava/lang/String;

    iget-object v7, p0, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    iget-object v8, p0, Lflipboard/activities/ComposeFragment;->B:Lflipboard/service/Section;

    invoke-virtual {p0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    check-cast v9, Lflipboard/activities/FlipboardActivity;

    invoke-static/range {v0 .. v9}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Lflipboard/service/Flap$JSONResultObserver;Ljava/util/Map;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    goto :goto_0

    :cond_3
    move-object v4, v2

    .line 609
    goto :goto_2

    :cond_4
    move-object v3, v2

    .line 611
    goto :goto_3

    :cond_5
    move-object v4, v2

    goto :goto_1
.end method
