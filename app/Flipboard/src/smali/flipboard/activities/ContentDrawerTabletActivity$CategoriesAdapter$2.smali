.class Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;
.super Ljava/lang/Object;
.source "ContentDrawerTabletActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;


# direct methods
.method constructor <init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;->a:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 562
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    invoke-virtual {p1}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v1

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Notifications status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "end message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;->a:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;->a:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0, p1, v1}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;Lflipboard/service/Section;Ljava/util/List;)V

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {p1, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Ljava/util/List;)V

    :goto_1
    invoke-virtual {p1}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;->a:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;)Lflipboard/util/Observer;

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2$1;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_4
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_1
.end method
