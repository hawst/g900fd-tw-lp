.class Lflipboard/activities/ShareFragment$5$1;
.super Ljava/lang/Object;
.source "ShareFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/json/FLObject;

.field final synthetic b:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic c:Lflipboard/activities/ShareFragment$5;


# direct methods
.method constructor <init>(Lflipboard/activities/ShareFragment$5;Lflipboard/json/FLObject;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 0

    .prologue
    .line 722
    iput-object p1, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iput-object p2, p0, Lflipboard/activities/ShareFragment$5$1;->a:Lflipboard/json/FLObject;

    iput-object p3, p0, Lflipboard/activities/ShareFragment$5$1;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 726
    iget-object v0, p0, Lflipboard/activities/ShareFragment$5$1;->a:Lflipboard/json/FLObject;

    const-string v1, "result"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 727
    iget-object v1, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v1, v1, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v1}, Lflipboard/activities/ShareFragment;->h(Lflipboard/activities/ShareFragment;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v1, v1, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v1}, Lflipboard/activities/ShareFragment;->h(Lflipboard/activities/ShareFragment;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 729
    iget-object v0, p0, Lflipboard/activities/ShareFragment$5$1;->b:Lflipboard/service/Flap$JSONResultObserver;

    iget-object v1, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v1, v1, Lflipboard/activities/ShareFragment$5;->a:Lflipboard/service/Flap$JSONResultObserver;

    iget-object v2, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v2, v2, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v2}, Lflipboard/activities/ShareFragment;->h(Lflipboard/activities/ShareFragment;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v3, v3, Lflipboard/activities/ShareFragment$5;->b:Ljava/util/List;

    iget-object v4, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v4, v4, Lflipboard/activities/ShareFragment$5;->c:Ljava/lang/String;

    iget-object v5, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v5, v5, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v5}, Lflipboard/activities/ShareFragment;->i(Lflipboard/activities/ShareFragment;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v6, v6, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v6}, Lflipboard/activities/ShareFragment;->j(Lflipboard/activities/ShareFragment;)Lflipboard/objs/Magazine;

    move-result-object v6

    iget-object v6, v6, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    iget-object v7, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v7, v7, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v7}, Lflipboard/activities/ShareFragment;->k(Lflipboard/activities/ShareFragment;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v8, v8, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-static {v8}, Lflipboard/activities/ShareFragment;->l(Lflipboard/activities/ShareFragment;)Lflipboard/service/Section;

    move-result-object v8

    iget-object v9, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    iget-object v9, v9, Lflipboard/activities/ShareFragment$5;->d:Lflipboard/activities/ShareFragment;

    invoke-virtual {v9}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    check-cast v9, Lflipboard/activities/FlipboardActivity;

    invoke-static/range {v0 .. v9}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Lflipboard/service/Flap$JSONResultObserver;Ljava/util/Map;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    .line 735
    :goto_0
    return-void

    .line 733
    :cond_0
    iget-object v1, p0, Lflipboard/activities/ShareFragment$5$1;->c:Lflipboard/activities/ShareFragment$5;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "got unexpected short url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/activities/ShareFragment$5;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
