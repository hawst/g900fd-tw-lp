.class Lflipboard/activities/TOCActivity$2;
.super Ljava/lang/Object;
.source "TOCActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/TOCActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/TOCActivity;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lflipboard/activities/TOCActivity$2;->a:Lflipboard/activities/TOCActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 190
    .line 193
    iget-object v0, p0, Lflipboard/activities/TOCActivity$2;->a:Lflipboard/activities/TOCActivity;

    invoke-static {v0}, Lflipboard/activities/TOCActivity;->a(Lflipboard/activities/TOCActivity;)Lflipboard/gui/toc/TOCView;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getPages()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v2

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getPages()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v2

    if-ltz v2, :cond_4

    .line 195
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getPages()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 196
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getSections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 197
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getSections()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/Section;

    iget v1, v1, Lflipboard/service/Section;->g:I

    .line 198
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getSections()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getSections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget v0, v0, Lflipboard/service/Section;->g:I

    move v2, v1

    move v1, v0

    .line 202
    :goto_0
    iget-object v0, p0, Lflipboard/activities/TOCActivity$2;->a:Lflipboard/activities/TOCActivity;

    iget-object v0, v0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 203
    invoke-virtual {v0}, Lflipboard/service/Section;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 204
    invoke-virtual {v0}, Lflipboard/service/Section;->o()V

    goto :goto_1

    .line 206
    :cond_1
    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v4, v4, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v4, :cond_2

    .line 208
    iget v4, v0, Lflipboard/service/Section;->g:I

    if-lt v4, v2, :cond_2

    iget v4, v0, Lflipboard/service/Section;->g:I

    if-le v4, v1, :cond_0

    .line 214
    :cond_2
    invoke-virtual {v0}, Lflipboard/service/Section;->n()V

    goto :goto_1

    .line 219
    :cond_3
    return-void

    :cond_4
    move v2, v1

    goto :goto_0
.end method
