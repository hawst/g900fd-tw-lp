.class Lflipboard/activities/ServiceLoginActivity$2;
.super Landroid/webkit/WebViewClient;
.source "ServiceLoginActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/service/Account;

.field final synthetic c:J

.field final synthetic d:Lflipboard/service/User;

.field final synthetic e:Lflipboard/activities/ServiceLoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ServiceLoginActivity;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Account;JLflipboard/service/User;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iput-object p2, p0, Lflipboard/activities/ServiceLoginActivity$2;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p3, p0, Lflipboard/activities/ServiceLoginActivity$2;->b:Lflipboard/service/Account;

    iput-wide p4, p0, Lflipboard/activities/ServiceLoginActivity$2;->c:J

    iput-object p6, p0, Lflipboard/activities/ServiceLoginActivity$2;->d:Lflipboard/service/User;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 376
    sget-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 380
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-boolean v0, v0, Lflipboard/activities/ServiceLoginActivity;->P:Z

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$2;->a:Lflipboard/activities/FlipboardActivity;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 383
    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 369
    sget-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 370
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-boolean v0, v0, Lflipboard/activities/ServiceLoginActivity;->P:Z

    if-eqz v0, :cond_0

    .line 371
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, p2}, Lflipboard/io/NetworkManager;->b(Ljava/lang/String;)V

    .line 373
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 292
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-boolean v2, v2, Lflipboard/activities/ServiceLoginActivity;->P:Z

    if-nez v2, :cond_1

    .line 365
    :cond_0
    :goto_0
    return v0

    .line 296
    :cond_1
    sget-object v2, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    new-array v2, v0, [Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 297
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v3, v3, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v4, "web"

    const-string v5, "Login Cancelled"

    invoke-static {v2, v3, v4, v5}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string v2, "flipboard://yoyo?json="

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 301
    :try_start_0
    new-instance v1, Lflipboard/json/JSONParser;

    const/16 v2, 0x16

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/HttpUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->y()Lflipboard/objs/UserInfo;

    move-result-object v1

    .line 302
    iget-boolean v2, v1, Lflipboard/objs/UserInfo;->b:Z

    if-eqz v2, :cond_5

    .line 303
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 304
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-boolean v2, v2, Lflipboard/activities/ServiceLoginActivity;->p:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v2, v2, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v2, v2, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v3, "nytimes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v2, v2, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v3, "ft"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 305
    :cond_2
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->b:Lflipboard/service/Account;

    if-eqz v2, :cond_4

    .line 306
    const-string v2, "originalSubscriptionLevel"

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$2;->b:Lflipboard/service/Account;

    iget-object v3, v3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v2, "additionalAction"

    const-string v3, "upgrade"

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_3
    :goto_1
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v1}, Lflipboard/objs/UserInfo;->a()Lflipboard/objs/UserInfo;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflipboard/activities/ServiceLoginActivity;->a(Lflipboard/objs/UserInfo;)V

    .line 318
    const-string v1, "from"

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v2}, Lflipboard/activities/ServiceLoginActivity;->a(Lflipboard/activities/ServiceLoginActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v1, "login"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/activities/ServiceLoginActivity$2;->c:J

    sub-long/2addr v2, v4

    iget-object v4, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v4, v4, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v5, "web"

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 334
    :catch_0
    move-exception v1

    .line 335
    sget-object v2, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 309
    :cond_4
    :try_start_1
    const-string v2, "additionalAction"

    const-string v3, "purchase"

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 323
    :cond_5
    iget v2, v1, Lflipboard/objs/UserInfo;->f:I

    const/16 v3, 0x44f

    if-ne v2, v3, :cond_6

    .line 324
    sget-object v2, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v4, v4, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    .line 325
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v1}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    goto/16 :goto_0

    .line 330
    :cond_6
    iget-object v2, v1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v1, v1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    .line 331
    :goto_2
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    const v3, 0x7f0d0165

    invoke-virtual {v2, v3}, Lflipboard/activities/ServiceLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 332
    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$2;->a:Lflipboard/activities/FlipboardActivity;

    const/4 v4, 0x1

    invoke-static {v3, v2, v1, v4}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 330
    :cond_7
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v1}, Lflipboard/activities/ServiceLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0164

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_2

    .line 339
    :cond_8
    const-string v2, "flipboard://openUrl?url="

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 340
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 341
    const-string v2, "url"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 342
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 343
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v1, v2}, Lflipboard/activities/ServiceLoginActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 345
    :cond_9
    const-string v2, "nytimes"

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v3, v3, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 346
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$2;->d:Lflipboard/service/User;

    invoke-virtual {v2, p2, v3}, Lflipboard/activities/ServiceLoginActivity;->a(Ljava/lang/String;Lflipboard/service/User;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_a
    move v0, v1

    .line 365
    goto/16 :goto_0

    .line 349
    :cond_b
    const-string v2, "market:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 351
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 352
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v2, v1}, Lflipboard/activities/ServiceLoginActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 354
    :cond_c
    const-string v2, "mailto:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 355
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SENDTO"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 356
    const-string v3, "text/html"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 358
    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v3, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 359
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v1}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    .line 360
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$2;->e:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v1, v2}, Lflipboard/activities/ServiceLoginActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
