.class public Lflipboard/activities/SwitchContentGuideActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "SwitchContentGuideActivity.java"


# instance fields
.field n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

.field private o:Lflipboard/io/UsageEvent;

.field private p:Z

.field private q:Lflipboard/service/RemoteWatchedFile;

.field private r:Lflipboard/service/RemoteWatchedFile$Observer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 66
    return-void
.end method

.method static synthetic a(Lflipboard/activities/SwitchContentGuideActivity;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/activities/SwitchContentGuideActivity;->b(Z)V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/SwitchContentGuideActivity;Ljava/util/List;Z)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/SwitchContentGuideActivity$2;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/activities/SwitchContentGuideActivity$2;-><init>(Lflipboard/activities/SwitchContentGuideActivity;Ljava/util/List;Z)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->q:Lflipboard/service/RemoteWatchedFile;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->q:Lflipboard/service/RemoteWatchedFile;

    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity;->r:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 232
    :cond_0
    new-instance v0, Lflipboard/activities/SwitchContentGuideActivity$1;

    invoke-direct {v0, p0, p1}, Lflipboard/activities/SwitchContentGuideActivity$1;-><init>(Lflipboard/activities/SwitchContentGuideActivity;Z)V

    iput-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->r:Lflipboard/service/RemoteWatchedFile$Observer;

    .line 268
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "contentGuide.json"

    iget-object v2, p0, Lflipboard/activities/SwitchContentGuideActivity;->r:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->q:Lflipboard/service/RemoteWatchedFile;

    .line 269
    return-void
.end method

.method static synthetic b(Lflipboard/activities/SwitchContentGuideActivity;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->p:Z

    return v0
.end method

.method static synthetic c(Lflipboard/activities/SwitchContentGuideActivity;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->p:Z

    return v0
.end method

.method static synthetic d(Lflipboard/activities/SwitchContentGuideActivity;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/activities/SwitchContentGuideActivity;->b(Z)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lflipboard/activities/SwitchContentGuideActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 44
    :cond_0
    new-instance v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-direct {v0, p0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;-><init>(Lflipboard/activities/SwitchContentGuideActivity;)V

    iput-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    .line 46
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/SwitchContentGuideActivity;->setContentView(I)V

    .line 47
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/SwitchContentGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 48
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 50
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/SwitchContentGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 51
    const v1, 0x7f0d00aa

    invoke-virtual {p0, v1}, Lflipboard/activities/SwitchContentGuideActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/SwitchContentGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 53
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lflipboard/activities/SwitchContentGuideActivity;->b(Z)V

    .line 54
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 298
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 299
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->q:Lflipboard/service/RemoteWatchedFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->r:Lflipboard/service/RemoteWatchedFile$Observer;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->q:Lflipboard/service/RemoteWatchedFile;

    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity;->r:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 301
    iput-object v2, p0, Lflipboard/activities/SwitchContentGuideActivity;->q:Lflipboard/service/RemoteWatchedFile;

    .line 302
    iput-object v2, p0, Lflipboard/activities/SwitchContentGuideActivity;->r:Lflipboard/service/RemoteWatchedFile$Observer;

    .line 304
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 199
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 202
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-static {v0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    const-string v3, "name"

    iget-object v4, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-static {v4}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    move-result-object v4

    invoke-virtual {v4}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 205
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    const-string v3, "locale"

    iget-object v4, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-static {v4}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    move-result-object v4

    iget-object v4, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-object v4, v4, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    const-string v3, "language"

    iget-object v4, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-static {v4}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    move-result-object v4

    iget-object v4, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-object v4, v4, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    iget-object v3, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    const-string v4, "wasModified"

    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-static {v0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    move-result-object v0

    iget-object v0, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-boolean v0, v0, Lflipboard/objs/ConfigEdition;->d:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 209
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    iget-wide v6, v3, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v4, v6

    iput-wide v4, v0, Lflipboard/io/UsageEvent;->g:J

    .line 210
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 213
    :cond_1
    iget-object v4, p0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    iget-object v0, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    iget-boolean v0, v0, Lflipboard/activities/SwitchContentGuideActivity;->p:Z

    if-nez v0, :cond_4

    .line 214
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 207
    goto :goto_0

    :cond_4
    move v3, v2

    .line 213
    :goto_1
    iget-object v0, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    iget-object v0, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    iget-boolean v5, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->a:Z

    if-eqz v5, :cond_5

    iget-object v5, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iput-boolean v1, v5, Lflipboard/objs/ConfigEdition;->d:Z

    iget-object v5, v4, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v5, v5, Lflipboard/activities/SwitchContentGuideActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v6, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-object v6, v6, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-object v0, v0, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    iget-object v0, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iput-boolean v2, v0, Lflipboard/objs/ConfigEdition;->d:Z

    goto :goto_2
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 62
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "configure"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    .line 63
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity;->o:Lflipboard/io/UsageEvent;

    const-string v1, "type"

    const-string v2, "contentGuide"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    return-void
.end method
