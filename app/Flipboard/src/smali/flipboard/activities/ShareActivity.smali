.class public Lflipboard/activities/ShareActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ShareActivity.java"

# interfaces
.implements Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;


# static fields
.field public static final n:Lflipboard/util/Log;


# instance fields
.field o:Lflipboard/activities/FlipboardFragment;

.field private p:Z

.field private q:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "share"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/ShareActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 410
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/Magazine;Lflipboard/service/Section;)V
    .locals 3

    .prologue
    .line 347
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ShareActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 348
    const-string v1, "edit_magazine_object"

    invoke-virtual {p1}, Lflipboard/objs/Magazine;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    const-string v1, "edit_magazine_sectionid"

    invoke-virtual {p2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const/16 v1, 0x8e

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FlipboardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 351
    const v0, 0x7f040001

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 352
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ShareActivity;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lflipboard/activities/ShareActivity;->p:Z

    return v0
.end method

.method static synthetic b(Lflipboard/activities/ShareActivity;)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/ShareActivity;->p:Z

    return v0
.end method

.method static synthetic c(Lflipboard/activities/ShareActivity;)V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    return-void
.end method

.method static synthetic d(Lflipboard/activities/ShareActivity;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->q:Lflipboard/util/Observer;

    return-object v0
.end method

.method private j()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 270
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v0, v0, Lflipboard/activities/ShareFragment;

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    invoke-virtual {v0}, Lflipboard/activities/ShareFragment;->getView()Landroid/view/View;

    .line 278
    :cond_0
    :goto_0
    return v1

    .line 273
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v0, v0, Lflipboard/activities/CreateMagazineFragment;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v0}, Lflipboard/activities/CreateMagazineFragment;->getView()Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/objs/Magazine;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v0, v0, Lflipboard/activities/CreateMagazineFragment;

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "share"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardFragment;

    .line 206
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lflipboard/activities/ShareFragment;

    if-eqz v1, :cond_0

    .line 207
    check-cast v0, Lflipboard/activities/ShareFragment;

    iput-object p1, v0, Lflipboard/activities/ShareFragment;->m:Lflipboard/objs/Magazine;

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ShareActivity;->p:Z

    .line 210
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->d()I

    move-result v0

    if-lez v0, :cond_2

    .line 211
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->c()V

    .line 222
    :cond_1
    :goto_0
    return-void

    .line 213
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 214
    new-instance v0, Lflipboard/activities/ShareFragment;

    invoke-direct {v0}, Lflipboard/activities/ShareFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    .line 215
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    .line 216
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    iput-object p1, v0, Lflipboard/activities/ShareFragment;->m:Lflipboard/objs/Magazine;

    .line 217
    const v0, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    const-string v3, "share"

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 218
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto :goto_0
.end method

.method public addToMagazine(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v0, v0, Lflipboard/activities/ShareFragment;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    invoke-virtual {v0}, Lflipboard/activities/ShareFragment;->addToMagazine()V

    .line 168
    :cond_0
    return-void
.end method

.method public cancel(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 194
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->onBackPressed()V

    .line 195
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lflipboard/activities/ShareActivity$3;

    invoke-direct {v0, p0}, Lflipboard/activities/ShareActivity$3;-><init>(Lflipboard/activities/ShareActivity;)V

    .line 261
    invoke-direct {p0}, Lflipboard/activities/ShareActivity;->j()Z

    .line 262
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 266
    return-void
.end method

.method public finish()V
    .locals 8

    .prologue
    .line 309
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-nez v0, :cond_1

    .line 310
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 316
    :goto_0
    iget-wide v2, p0, Lflipboard/activities/ShareActivity;->V:J

    .line 317
    iget-wide v4, p0, Lflipboard/activities/ShareActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/ShareActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 320
    :cond_0
    const-string v1, "extra_result_active_time"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 322
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lflipboard/activities/ShareActivity;->setResult(ILandroid/content/Intent;)V

    .line 323
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 324
    return-void

    .line 312
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    iget-object v1, v0, Lflipboard/activities/FlipboardFragment;->k:Landroid/content/Intent;

    if-nez v1, :cond_2

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iput-object v1, v0, Lflipboard/activities/FlipboardFragment;->k:Landroid/content/Intent;

    :cond_2
    iget-object v0, v0, Lflipboard/activities/FlipboardFragment;->k:Landroid/content/Intent;

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    const-string v0, "share"

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 303
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 304
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v0, :cond_0

    .line 227
    invoke-direct {p0}, Lflipboard/activities/ShareActivity;->j()Z

    .line 231
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    .line 243
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const v6, 0x7f0a004e

    .line 56
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SEND"

    if-ne v1, v2, :cond_3

    .line 65
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "flipboard.extra.navigating.from"

    sget-object v4, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->d:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ComposeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/ShareActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->finish()V

    goto :goto_0

    .line 74
    :cond_2
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 75
    invoke-virtual {v1}, Lflipboard/service/User;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 77
    new-instance v1, Lflipboard/gui/hints/LightBoxFragment;

    invoke-direct {v1}, Lflipboard/gui/hints/LightBoxFragment;-><init>()V

    sget-object v2, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->a:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-virtual {v1, v2}, Lflipboard/gui/hints/LightBoxFragment;->a(Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;)V

    new-instance v2, Lflipboard/activities/ShareActivity$5;

    invoke-direct {v2, p0}, Lflipboard/activities/ShareActivity$5;-><init>(Lflipboard/activities/ShareActivity;)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {v1, v0}, Lflipboard/gui/hints/LightBoxFragment;->a(Z)V

    new-instance v0, Lflipboard/activities/ShareActivity$6;

    invoke-direct {v0, p0}, Lflipboard/activities/ShareActivity$6;-><init>(Lflipboard/activities/ShareActivity;)V

    iput-object v0, v1, Lflipboard/gui/hints/LightBoxFragment;->l:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "light_box"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/hints/LightBoxFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ShareActivity$7;

    invoke-direct {v1, p0}, Lflipboard/activities/ShareActivity$7;-><init>(Lflipboard/activities/ShareActivity;)V

    iput-object v1, p0, Lflipboard/activities/ShareActivity;->q:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    goto :goto_0

    .line 84
    :cond_3
    const v1, 0x7f03011b

    invoke-virtual {p0, v1}, Lflipboard/activities/ShareActivity;->setContentView(I)V

    .line 86
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 87
    const-string v2, "create_magazine_object"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 88
    const-string v4, "edit_magazine_object"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    .line 90
    if-nez p1, :cond_9

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_6

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    :goto_1
    if-eqz v0, :cond_7

    new-instance v0, Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {v0}, Lflipboard/activities/CreateMagazineFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v2, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    const-string v4, "create"

    invoke-virtual {v0, v6, v2, v4}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    :goto_2
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    :goto_3
    iput-boolean v3, p0, Lflipboard/activities/ShareActivity;->W:Z

    const-string v0, "edit"

    iget-object v1, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "create"

    iget-object v1, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/CreateMagazineFragment;

    iput-object p0, v0, Lflipboard/activities/CreateMagazineFragment;->i:Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

    .line 92
    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    new-instance v1, Lflipboard/activities/ShareActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ShareActivity$1;-><init>(Lflipboard/activities/ShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 104
    iget-boolean v0, p0, Lflipboard/activities/ShareActivity;->Z:Z

    if-nez v0, :cond_0

    .line 105
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->e:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    goto/16 :goto_0

    :cond_6
    move v0, v3

    .line 90
    goto :goto_1

    :cond_7
    if-eqz v4, :cond_8

    new-instance v0, Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {v0}, Lflipboard/activities/CreateMagazineFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v2, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    const-string v4, "edit"

    invoke-virtual {v0, v6, v2, v4}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto :goto_2

    :cond_8
    new-instance v0, Lflipboard/activities/ShareFragment;

    invoke-direct {v0}, Lflipboard/activities/ShareFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v2, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    const-string v4, "share"

    invoke-virtual {v0, v6, v2, v4}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto :goto_2

    :cond_9
    const-string v0, "last_show_fragment_tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v0, "last_show_fragment_tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, p0

    :goto_4
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardFragment;

    iput-object v0, v2, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    goto/16 :goto_3

    :cond_a
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    if-eqz v4, :cond_b

    const-string v0, "edit"

    move-object v2, p0

    goto :goto_4

    :cond_b
    const-string v0, "share"

    move-object v2, p0

    goto :goto_4
.end method

.method public onCreateMagazineCanceled(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 199
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->onBackPressed()V

    .line 200
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 151
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "shareUIShown"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "last_show_fragment_tag"

    iget-object v1, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_0
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 160
    return-void
.end method

.method public selectMagazine(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 172
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v0, v0, Lflipboard/activities/ShareFragment;

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    instance-of v1, p1, Lflipboard/gui/MagazineThumbView;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/activities/ShareFragment;->c:Lflipboard/gui/MagazineThumbView;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/activities/ShareFragment;->c:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v3}, Lflipboard/gui/MagazineThumbView;->setSelected(Z)V

    :cond_0
    move-object v1, p1

    check-cast v1, Lflipboard/gui/MagazineThumbView;

    iput-object v1, v0, Lflipboard/activities/ShareFragment;->c:Lflipboard/gui/MagazineThumbView;

    iget-object v1, v0, Lflipboard/activities/ShareFragment;->c:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v2}, Lflipboard/gui/MagazineThumbView;->setSelected(Z)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/Magazine;

    iput-object v1, v0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    iget-object v5, v0, Lflipboard/activities/ShareFragment;->e:Lflipboard/gui/FLButton;

    iget-object v1, v0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    iget-object v2, v0, Lflipboard/activities/ShareFragment;->f:Lflipboard/gui/FLEditText;

    iget-object v1, v0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    if-eqz v1, :cond_3

    move v1, v3

    :goto_1
    invoke-virtual {v2, v1}, Lflipboard/gui/FLEditText;->setVisibility(I)V

    iget-object v1, v0, Lflipboard/activities/ShareFragment;->d:Lflipboard/objs/Magazine;

    invoke-virtual {v1}, Lflipboard/objs/Magazine;->a()Z

    move-result v1

    iget-object v2, v0, Lflipboard/activities/ShareFragment;->g:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lflipboard/activities/ShareFragment;->g:Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    :goto_2
    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 175
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 173
    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2
.end method

.method public showCreateMagazine(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 328
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 329
    const v0, 0x7f040016

    const v2, 0x7f040018

    const v3, 0x7f040015

    const v4, 0x7f040017

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->a(IIII)Landroid/support/v4/app/FragmentTransaction;

    .line 332
    invoke-virtual {p0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 333
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v0, v0, Lflipboard/activities/ShareFragment;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    iget-object v0, v0, Lflipboard/activities/ShareFragment;->f:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "flipboard.extra.share.comment"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_0
    new-instance v0, Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {v0}, Lflipboard/activities/CreateMagazineFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    .line 338
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/CreateMagazineFragment;

    iput-object p0, v0, Lflipboard/activities/CreateMagazineFragment;->i:Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

    .line 339
    iget-object v0, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    .line 340
    const v0, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    const-string v3, "create"

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 341
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->a(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 342
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 343
    return-void
.end method
