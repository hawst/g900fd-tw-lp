.class Lflipboard/activities/CreateAccountActivity$9$2$1;
.super Ljava/lang/Object;
.source "CreateAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

.field final synthetic b:Lflipboard/service/FlipboardManager;

.field final synthetic c:Ljava/lang/Object;

.field final synthetic d:Lflipboard/activities/CreateAccountActivity$9$2;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateAccountActivity$9$2;Lflipboard/service/FlipboardManager$CreateAccountMessage;Lflipboard/service/FlipboardManager;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iput-object p2, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    iput-object p3, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->b:Lflipboard/service/FlipboardManager;

    iput-object p4, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->c:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const v6, 0x7f0201cf

    const/4 v3, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 320
    sget-object v0, Lflipboard/activities/CreateAccountActivity$18;->a:[I

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager$CreateAccountMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 370
    :goto_0
    return-void

    .line 322
    :pswitch_0
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "progress"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 323
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 326
    :cond_0
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v0

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->a:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_2

    .line 327
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "extra_invite"

    iget-object v3, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v3, v3, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v3, v3, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v3, v3, Lflipboard/activities/CreateAccountActivity;->D:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lflipboard/activities/CreateAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 347
    :cond_1
    :goto_1
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->a:Lflipboard/activities/LoginActivity$SignInMethod;

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v1}, Lflipboard/activities/CreateAccountActivity;->d(Lflipboard/activities/CreateAccountActivity;)Lflipboard/objs/UsageEventV2$EventCategory;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v2}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v2

    invoke-static {v4, v0, v1, v2}, Lflipboard/activities/CreateAccountActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 348
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0, v4}, Lflipboard/activities/CreateAccountActivity;->a(Lflipboard/activities/CreateAccountActivity;Z)Z

    .line 349
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/CreateAccountActivity;->finish()V

    goto :goto_0

    .line 328
    :cond_2
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v0

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_1

    .line 329
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 330
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 332
    :try_start_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->A()Lflipboard/model/ConfigFirstLaunch;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "createBriefingAccount"

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/User;->a(Lflipboard/model/ConfigFirstLaunch;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :goto_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 337
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 338
    const/4 v0, 0x0

    .line 339
    iget-object v3, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    if-eqz v3, :cond_3

    .line 340
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    .line 342
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 343
    const-string v3, "name"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v2, "avatar"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v0, v5, v1}, Lflipboard/activities/CreateAccountActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 333
    :catch_0
    move-exception v0

    .line 334
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 354
    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 356
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "progress"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 357
    if-eqz v1, :cond_4

    .line 358
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 362
    :cond_4
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v1}, Lflipboard/activities/CreateAccountActivity;->e(Lflipboard/activities/CreateAccountActivity;)Lflipboard/util/Log;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v0, v1, v3

    .line 363
    if-eqz v0, :cond_5

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    sget-object v2, Lflipboard/service/FlipboardManager$CreateAccountMessage;->c:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    if-ne v1, v2, :cond_5

    .line 365
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v1}, Lflipboard/activities/CreateAccountActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    invoke-virtual {v1, v6, v0}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 368
    :cond_5
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/CreateAccountActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$9$2$1;->d:Lflipboard/activities/CreateAccountActivity$9$2;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9$2;->a:Lflipboard/activities/CreateAccountActivity$9;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v1}, Lflipboard/activities/CreateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
