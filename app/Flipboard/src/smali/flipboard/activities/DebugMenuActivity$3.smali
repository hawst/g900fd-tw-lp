.class Lflipboard/activities/DebugMenuActivity$3;
.super Ljava/lang/Object;
.source "DebugMenuActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/DebugMenuActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/DebugMenuActivity;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lflipboard/activities/DebugMenuActivity$3;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 142
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$3;->a:Lflipboard/activities/DebugMenuActivity;

    const-class v2, Lflipboard/activities/DebugMenuActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    const-string v0, "sectionId"

    iget-object v2, p0, Lflipboard/activities/DebugMenuActivity$3;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v2}, Lflipboard/activities/DebugMenuActivity;->b(Lflipboard/activities/DebugMenuActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 145
    iget-object v2, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 146
    const-string v3, "feedItemId"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string v2, "activityType"

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 149
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$3;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-virtual {v0, v1}, Lflipboard/activities/DebugMenuActivity;->startActivity(Landroid/content/Intent;)V

    .line 150
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
