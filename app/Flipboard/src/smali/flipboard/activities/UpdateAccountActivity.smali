.class public Lflipboard/activities/UpdateAccountActivity;
.super Lflipboard/activities/ChooseAvatarActivity;
.source "UpdateAccountActivity.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field o:Z

.field private p:Lflipboard/util/Log;

.field private q:Lflipboard/io/UsageEvent;

.field private r:Landroid/widget/EditText;

.field private s:Landroid/widget/EditText;

.field private t:Landroid/widget/EditText;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/Button;

.field private y:Landroid/os/Handler;

.field private z:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lflipboard/activities/ChooseAvatarActivity;-><init>()V

    .line 36
    const-string v0, "update account"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->p:Lflipboard/util/Log;

    return-void
.end method

.method static synthetic a(Lflipboard/activities/UpdateAccountActivity;)V
    .locals 7

    .prologue
    const v3, 0x7f0d0125

    const/4 v6, 0x4

    const v5, 0x7f080084

    const/high16 v4, -0x10000

    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lflipboard/activities/UpdateAccountActivity;->o:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v0, :cond_1

    const-string v2, "flipboard"

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "flipboard"

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    if-nez v2, :cond_5

    const-string v0, ""

    :goto_2
    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f0d012c

    invoke-virtual {p0, v1}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    :goto_3
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    rsub-int v0, v0, 0x96

    if-gez v0, :cond_e

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    const/16 v2, 0x40

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    const v2, 0x7f0d0127

    invoke-virtual {p0, v2}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    goto/16 :goto_2

    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    :cond_8
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0xf

    if-le v0, v3, :cond_9

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f0d0127

    invoke-virtual {p0, v1}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    :cond_9
    const-string v0, "[[a-z][A-Z][0-9][_]]*"

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f0d0123

    invoke-virtual {p0, v1}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->A:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-boolean v0, p0, Lflipboard/activities/UpdateAccountActivity;->B:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f0d012a

    invoke-virtual {p0, v1}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    :cond_b
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f0d0126

    invoke-virtual {p0, v1}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    :cond_c
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f0d012b

    invoke-virtual {p0, v1}, Lflipboard/activities/UpdateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    :cond_d
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_e
    const/16 v1, 0xa

    if-gt v0, v1, :cond_f

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    :cond_f
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lflipboard/activities/UpdateAccountActivity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/UpdateAccountActivity$7;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/UpdateAccountActivity$7;-><init>(Lflipboard/activities/UpdateAccountActivity;Ljava/lang/String;)V

    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v3, Lflipboard/service/Flap$CheckUsernameRequest;

    invoke-direct {v3, v0, v2}, Lflipboard/service/Flap$CheckUsernameRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, p1, v1}, Lflipboard/service/Flap$CheckUsernameRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$CheckUsernameRequest;

    return-void
.end method

.method static synthetic a(Lflipboard/activities/UpdateAccountActivity;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lflipboard/activities/UpdateAccountActivity;->B:Z

    return p1
.end method

.method static synthetic b(Lflipboard/activities/UpdateAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lflipboard/activities/UpdateAccountActivity;->A:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lflipboard/activities/UpdateAccountActivity;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_7

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v3, 0x40

    if-ge v0, v3, :cond_7

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    const-string v0, "[[a-z][A-Z][0-9][_]]{1,15}"

    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v4, p0, Lflipboard/activities/UpdateAccountActivity;->A:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lflipboard/activities/UpdateAccountActivity;->A:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lflipboard/activities/UpdateAccountActivity;->B:Z

    and-int/2addr v0, v3

    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v3, 0x96

    if-gt v0, v3, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_1

    move v0, v1

    :goto_4
    iget-object v3, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    if-nez v3, :cond_2

    :goto_5
    and-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->x:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->x:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    :goto_6
    return-void

    :cond_1
    move v0, v2

    goto :goto_4

    :cond_2
    move v1, v2

    goto :goto_5

    :cond_3
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->x:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_6

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto/16 :goto_0
.end method

.method static synthetic c(Lflipboard/activities/UpdateAccountActivity;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v0, ""

    :goto_0
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->y:Landroid/os/Handler;

    if-nez v2, :cond_0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->y:Landroid/os/Handler;

    :cond_0
    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    const-string v0, "[[a-z][A-Z][0-9][_]]{1,15}"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->y:Landroid/os/Handler;

    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_1
    new-instance v0, Lflipboard/activities/UpdateAccountActivity$6;

    invoke-direct {v0, p0, v1}, Lflipboard/activities/UpdateAccountActivity$6;-><init>(Lflipboard/activities/UpdateAccountActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->y:Landroid/os/Handler;

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void

    :cond_3
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic d(Lflipboard/activities/UpdateAccountActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/UpdateAccountActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/UpdateAccountActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/UpdateAccountActivity;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->p:Lflipboard/util/Log;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/UpdateAccountActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->z:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public more(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->AccountHelpURLString:Ljava/lang/String;

    .line 459
    if-eqz v0, :cond_0

    .line 460
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 67
    invoke-super {p0, p1}, Lflipboard/activities/ChooseAvatarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    :goto_0
    return-void

    .line 71
    :cond_0
    const v0, 0x7f03014d

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->setContentView(I)V

    .line 73
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 74
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 76
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "configure"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->q:Lflipboard/io/UsageEvent;

    .line 77
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "serviceAction"

    const-string v2, "update"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 81
    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 82
    if-nez v1, :cond_1

    .line 83
    invoke-virtual {p0}, Lflipboard/activities/UpdateAccountActivity;->finish()V

    .line 84
    const-string v0, "flipboard"

    const/4 v1, 0x1

    invoke-static {v3, v3, v0, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 88
    :cond_1
    const v0, 0x7f0a037c

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    .line 89
    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    const v0, 0x7f0a037d

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->u:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    new-instance v2, Lflipboard/activities/UpdateAccountActivity$1;

    invoke-direct {v2, p0}, Lflipboard/activities/UpdateAccountActivity$1;-><init>(Lflipboard/activities/UpdateAccountActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 99
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->r:Landroid/widget/EditText;

    new-instance v2, Lflipboard/activities/UpdateAccountActivity$2;

    invoke-direct {v2, p0}, Lflipboard/activities/UpdateAccountActivity$2;-><init>(Lflipboard/activities/UpdateAccountActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    const v0, 0x7f0a037f

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    .line 110
    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v0, 0x7f0a0380

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->v:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->s:Landroid/widget/EditText;

    new-instance v2, Lflipboard/activities/UpdateAccountActivity$3;

    invoke-direct {v2, p0}, Lflipboard/activities/UpdateAccountActivity$3;-><init>(Lflipboard/activities/UpdateAccountActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 123
    const v0, 0x7f0a0382

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    .line 124
    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const v0, 0x7f0a0383

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->w:Landroid/widget/TextView;

    .line 126
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->t:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/UpdateAccountActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/UpdateAccountActivity$4;-><init>(Lflipboard/activities/UpdateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 136
    const v0, 0x7f0a0378

    invoke-virtual {p0, v0}, Lflipboard/activities/UpdateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->x:Landroid/widget/Button;

    .line 139
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->x:Landroid/widget/Button;

    new-instance v1, Lflipboard/activities/UpdateAccountActivity$5;

    invoke-direct {v1, p0}, Lflipboard/activities/UpdateAccountActivity$5;-><init>(Lflipboard/activities/UpdateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 89
    :cond_2
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    goto/16 :goto_1

    .line 110
    :cond_3
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    goto :goto_2

    .line 124
    :cond_4
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 466
    invoke-super {p0}, Lflipboard/activities/ChooseAvatarActivity;->onDestroy()V

    .line 467
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->q:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->q:Lflipboard/io/UsageEvent;

    iget-wide v2, p0, Lflipboard/activities/UpdateAccountActivity;->V:J

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 469
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity;->q:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 471
    :cond_0
    return-void
.end method
