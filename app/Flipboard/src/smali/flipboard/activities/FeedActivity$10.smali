.class Lflipboard/activities/FeedActivity$10;
.super Ljava/lang/Object;
.source "FeedActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/objs/FeedItem;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/activities/FlipboardActivity;

.field final synthetic d:Lflipboard/activities/FeedActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lflipboard/activities/FeedActivity$10;->d:Lflipboard/activities/FeedActivity;

    iput-object p2, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iput-object p3, p0, Lflipboard/activities/FeedActivity$10;->b:Lflipboard/service/Section;

    iput-object p4, p0, Lflipboard/activities/FeedActivity$10;->c:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 404
    iget-object v0, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    :cond_0
    iget-object v0, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 412
    :goto_0
    iget-object v1, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 416
    iget-object v1, p0, Lflipboard/activities/FeedActivity$10;->d:Lflipboard/activities/FeedActivity;

    iget-object v1, v1, Lflipboard/activities/FeedActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 417
    iget-object v2, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 418
    if-eqz v1, :cond_4

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->o:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 420
    iget-object v1, p0, Lflipboard/activities/FeedActivity$10;->b:Lflipboard/service/Section;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/activities/FeedActivity$10;->b:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    .line 421
    :goto_1
    iget-object v2, p0, Lflipboard/activities/FeedActivity$10;->d:Lflipboard/activities/FeedActivity;

    invoke-static {v2, v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_1
    :goto_2
    return v3

    .line 409
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FeedActivity$10;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 420
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 426
    :cond_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 427
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 428
    const-string v0, "com.android.browser.application_id"

    const-string v2, "flipboard"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    iget-object v0, p0, Lflipboard/activities/FeedActivity$10;->c:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method
