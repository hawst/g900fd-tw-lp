.class public Lflipboard/activities/ReportIssueActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ReportIssueActivity.java"


# static fields
.field public static n:Ljava/lang/String;

.field private static final p:Lflipboard/util/Log;


# instance fields
.field private A:Ljava/io/File;

.field private B:Landroid/widget/Spinner;

.field private C:Landroid/widget/Spinner;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private ad:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

.field private af:Z

.field private ag:I

.field private ah:Lflipboard/service/User;

.field private ai:Ljava/lang/String;

.field private aj:Ljava/lang/Runnable;

.field public final o:Ljava/lang/String;

.field private q:Landroid/widget/Spinner;

.field private r:Landroid/widget/Spinner;

.field private s:Landroid/widget/Spinner;

.field private t:Landroid/widget/EditText;

.field private u:Landroid/widget/EditText;

.field private v:Landroid/widget/AutoCompleteTextView;

.field private w:Lflipboard/gui/dialog/FLProgressDialog;

.field private x:Landroid/app/AlertDialog;

.field private y:Landroid/app/AlertDialog;

.field private z:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "report issue"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/ReportIssueActivity;->p:Lflipboard/util/Log;

    .line 52
    const-string v0, "screenshot_file"

    sput-object v0, Lflipboard/activities/ReportIssueActivity;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 68
    const-string v0, "https://jira.flipboard.com/browse/"

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->o:Ljava/lang/String;

    .line 80
    const v0, 0x392ff

    iput v0, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    .line 477
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ReportIssueActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ReportIssueActivity;Lflipboard/gui/dialog/FLAlertDialog$Builder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lflipboard/activities/ReportIssueActivity;->a(Lflipboard/gui/dialog/FLAlertDialog$Builder;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lflipboard/gui/dialog/FLAlertDialog$Builder;Ljava/lang/String;)V
    .locals 8

    .prologue
    const v7, 0x7f0800aa

    const v6, 0x7f080033

    const/4 v5, 0x1

    const/high16 v4, 0x41a00000    # 20.0f

    .line 509
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 512
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 513
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 514
    iget-object v0, p1, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v2, 0x7f0a0051

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 515
    if-eqz v0, :cond_0

    .line 516
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 518
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 519
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 520
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 521
    new-instance v0, Lflipboard/gui/FLButton;

    invoke-direct {v0, p0}, Lflipboard/gui/FLButton;-><init>(Landroid/content/Context;)V

    .line 522
    invoke-virtual {v0, v4}, Lflipboard/gui/FLButton;->setTextSize(F)V

    .line 523
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setTextColor(I)V

    .line 524
    const-string v2, "Copy Link"

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 525
    new-instance v2, Lflipboard/gui/FLButton;

    invoke-direct {v2, p0}, Lflipboard/gui/FLButton;-><init>(Landroid/content/Context;)V

    .line 526
    invoke-virtual {v2, v4}, Lflipboard/gui/FLButton;->setTextSize(F)V

    .line 527
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLButton;->setTextColor(I)V

    .line 528
    const-string v3, "Email"

    invoke-virtual {v2, v3}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 529
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 530
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 531
    invoke-virtual {p1, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 533
    invoke-virtual {p1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 534
    new-instance v3, Lflipboard/activities/ReportIssueActivity$9;

    invoke-direct {v3, p0}, Lflipboard/activities/ReportIssueActivity$9;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 543
    new-instance v3, Lflipboard/activities/ReportIssueActivity$10;

    invoke-direct {v3, p0, p2}, Lflipboard/activities/ReportIssueActivity$10;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 552
    new-instance v0, Lflipboard/activities/ReportIssueActivity$11;

    invoke-direct {v0, p0, p2, v1}, Lflipboard/activities/ReportIssueActivity$11;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 575
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_1

    .line 578
    :try_start_0
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 583
    :cond_1
    :goto_0
    return-void

    .line 579
    :catch_0
    move-exception v0

    .line 580
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/ReportIssueActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    return-void
.end method

.method static synthetic c(Lflipboard/activities/ReportIssueActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    return-void
.end method

.method static synthetic d(Lflipboard/activities/ReportIssueActivity;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ReportIssueActivity;->af:Z

    return v0
.end method

.method static synthetic e(Lflipboard/activities/ReportIssueActivity;)Lflipboard/gui/dialog/FLProgressDialog;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->w:Lflipboard/gui/dialog/FLProgressDialog;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/ReportIssueActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->x:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/ReportIssueActivity;)Lflipboard/gui/dialog/FLAlertDialog$Builder;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/ReportIssueActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    return-void
.end method

.method static synthetic i(Lflipboard/activities/ReportIssueActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->t:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic j()Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lflipboard/activities/ReportIssueActivity;->p:Lflipboard/util/Log;

    return-object v0
.end method

.method static synthetic j(Lflipboard/activities/ReportIssueActivity;)Ljava/io/File;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->A:Ljava/io/File;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 454
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->a()V

    .line 455
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->aj:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->aj:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 457
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->aj:Ljava/lang/Runnable;

    .line 459
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 588
    return-void
.end method

.method protected final a(Lflipboard/service/Section;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 598
    return-void
.end method

.method public final a(Lflipboard/service/Section;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Section;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 593
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->Q:Z

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->w:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->a(Landroid/content/DialogInterface;)V

    .line 440
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {p0, v0, p1}, Lflipboard/activities/ReportIssueActivity;->a(Lflipboard/gui/dialog/FLAlertDialog$Builder;Ljava/lang/String;)V

    .line 450
    :goto_0
    return-void

    .line 442
    :cond_0
    new-instance v0, Lflipboard/activities/ReportIssueActivity$8;

    invoke-direct {v0, p0, p1}, Lflipboard/activities/ReportIssueActivity$8;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->aj:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method protected final i()V
    .locals 1

    .prologue
    .line 504
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->setRequestedOrientation(I)V

    .line 505
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 471
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onAttachedToWindow()V

    .line 472
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 473
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    .line 475
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->a(Landroid/app/Dialog;)V

    .line 466
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const v12, 0x1090009

    const/4 v11, 0x2

    const v10, 0x7f0a02ce

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 88
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 92
    :cond_0
    const v0, 0x7f0300f8

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->setContentView(I)V

    .line 94
    iput-boolean v3, p0, Lflipboard/activities/ReportIssueActivity;->W:Z

    .line 96
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 98
    if-eqz v1, :cond_9

    .line 99
    sget-object v0, Lflipboard/activities/ReportIssueActivity;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->z:Ljava/io/File;

    .line 100
    const-string v0, "AML"

    const-string v2, "project_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 101
    const-string v2, "flipmag_partner_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/activities/ReportIssueActivity;->D:Ljava/lang/String;

    .line 102
    const-string v2, "aml_url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/activities/ReportIssueActivity;->ac:Ljava/lang/String;

    .line 103
    const-string v2, "feed_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/activities/ReportIssueActivity;->E:Ljava/lang/String;

    .line 104
    const-string v2, "feed_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/activities/ReportIssueActivity;->F:Ljava/lang/String;

    .line 105
    const-string v2, "item_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/activities/ReportIssueActivity;->G:Ljava/lang/String;

    .line 106
    const-string v2, "source_url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ai:Ljava/lang/String;

    move v2, v0

    .line 110
    :goto_1
    const v0, 0x7f0a02c5

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 111
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v9, v1, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(ZLflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;Z)Landroid/view/View;

    .line 113
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-direct {v1, p0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 114
    const v4, 0x7f0d01d9

    invoke-virtual {v1, v4}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v4

    const v5, 0x1080055

    invoke-virtual {v4, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v4

    .line 115
    invoke-virtual {v4, v11}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 116
    new-instance v5, Lflipboard/activities/ReportIssueActivity$1;

    invoke-direct {v5, p0}, Lflipboard/activities/ReportIssueActivity$1;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    iput-object v5, v4, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 124
    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 126
    const v0, 0x7f0a02c6

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 127
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 128
    const v4, 0x7f0a02c4

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    .line 130
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    const v4, 0x7f0d0279

    invoke-virtual {v1, v4}, Landroid/widget/AutoCompleteTextView;->setHint(I)V

    .line 131
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ah:Lflipboard/service/User;

    .line 132
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ah:Lflipboard/service/User;

    iget-object v4, v1, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v4, :cond_5

    iget-object v1, v1, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v1, v1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v1, v1, Lflipboard/objs/UserState$Data;->h:Ljava/util/List;

    :goto_2
    iput-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    .line 133
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    if-nez v1, :cond_1

    .line 134
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity;->N:Landroid/content/SharedPreferences;

    const-string v5, "report_issue_previously_email"

    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    .line 137
    :cond_1
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 138
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v4, "flipboard"

    invoke-virtual {v1, v4}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_2

    .line 140
    iget-object v4, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    .line 141
    if-eqz v4, :cond_2

    const-string v5, "flipboard"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 142
    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_2
    :goto_3
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 152
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v4, 0x7f0300f7

    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, p0, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 153
    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 154
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v3}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 155
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    iget v4, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v1, v4}, Landroid/widget/AutoCompleteTextView;->setId(I)V

    .line 156
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    new-instance v4, Lflipboard/activities/ReportIssueActivity$2;

    invoke-direct {v4, p0}, Lflipboard/activities/ReportIssueActivity$2;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    const v1, 0x7f0d01d5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 170
    const v0, 0x7f0a02cb

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 171
    const v1, 0x7f0a02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->t:Landroid/widget/EditText;

    .line 172
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->t:Landroid/widget/EditText;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setId(I)V

    .line 174
    const v0, 0x7f0a0268

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 175
    const v1, 0x7f0a02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->u:Landroid/widget/EditText;

    .line 176
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->u:Landroid/widget/EditText;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setId(I)V

    .line 179
    const v0, 0x7f0a02c9

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 180
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 181
    const v4, 0x7f0d01d1

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 182
    const v0, 0x7f0a02cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->s:Landroid/widget/Spinner;

    .line 183
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->s:Landroid/widget/Spinner;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setId(I)V

    .line 184
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030125

    const/4 v4, 0x6

    new-array v4, v4, [Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Android"

    const-string v7, "AND"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v3

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Home Feed content"

    const-string v7, "HF"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v9

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Article Markup (AML)"

    const-string v7, "AML"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v11

    const/4 v5, 0x3

    new-instance v6, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v7, "Flipmag templates (FLM)"

    const-string v8, "FLM"

    invoke-direct {v6, p0, v7, v8}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x4

    new-instance v6, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v7, "Localization (LOC)"

    const-string v8, "LOC"

    invoke-direct {v6, p0, v7, v8}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x5

    new-instance v6, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v7, "Platform (PLAT)"

    const-string v8, "PLAT"

    invoke-direct {v6, p0, v7, v8}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    invoke-direct {v0, p0, v1, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 193
    invoke-virtual {v0, v12}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 194
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->s:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 195
    if-eqz v2, :cond_3

    .line 196
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->s:Landroid/widget/Spinner;

    invoke-virtual {v0, v9}, Landroid/widget/Spinner;->setSelection(I)V

    .line 199
    :cond_3
    const v0, 0x7f0a02c7

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 200
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 201
    const v2, 0x7f0d01d8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 202
    const v0, 0x7f0a02cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->q:Landroid/widget/Spinner;

    .line 203
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->q:Landroid/widget/Spinner;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setId(I)V

    .line 204
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030125

    new-array v2, v11, [Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Bug"

    const-string v6, "1"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Improvement"

    const-string v6, "4"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v9

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 209
    invoke-virtual {v0, v12}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 210
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->q:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 212
    const v0, 0x7f0a02c8

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 213
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 214
    const-string v2, "Priority"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    const v0, 0x7f0a02cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->r:Landroid/widget/Spinner;

    .line 216
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->r:Landroid/widget/Spinner;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setId(I)V

    .line 217
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030125

    const/4 v2, 0x6

    new-array v2, v2, [Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Blocker"

    const-string v6, "1"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Critical"

    const-string v6, "2"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v9

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Major"

    const-string v6, "3"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v11

    const/4 v4, 0x3

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Normal"

    const-string v7, "6"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v4

    const/4 v4, 0x4

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Minor"

    const-string v7, "4"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v4

    const/4 v4, 0x5

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Trivial"

    const-string v7, "5"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v4

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 226
    invoke-virtual {v0, v12}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 227
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->r:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 228
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->r:Landroid/widget/Spinner;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 230
    const v0, 0x7f0a02ca

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 231
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 232
    const v2, 0x7f0d01d6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 233
    const v0, 0x7f0a02cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->B:Landroid/widget/Spinner;

    .line 234
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->B:Landroid/widget/Spinner;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setId(I)V

    .line 236
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030125

    const/4 v2, 0x4

    new-array v2, v2, [Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "I didn\'t try"

    const-string v6, "10920"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Always"

    const-string v6, "10921"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v9

    new-instance v4, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v5, "Sometimes"

    const-string v6, "10922"

    invoke-direct {v4, p0, v5, v6}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v11

    const/4 v4, 0x3

    new-instance v5, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    const-string v6, "Rarely"

    const-string v7, "10923"

    invoke-direct {v5, p0, v6, v7}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v4

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 242
    invoke-virtual {v0, v12}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 243
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->B:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 245
    const v0, 0x7f0a0016

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 246
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 247
    const v2, 0x7f0d01d3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 248
    const v0, 0x7f0a02cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->C:Landroid/widget/Spinner;

    .line 249
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->C:Landroid/widget/Spinner;

    iget v1, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/activities/ReportIssueActivity;->ag:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setId(I)V

    .line 252
    invoke-virtual {p0}, Lflipboard/activities/ReportIssueActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 254
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 255
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v5

    array-length v6, v5

    move v1, v3

    move v0, v3

    :goto_4
    if-ge v1, v6, :cond_7

    aget-object v7, v5, v1

    .line 256
    new-instance v8, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    invoke-virtual {v7}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, p0, v9, v10}, Lflipboard/activities/ReportIssueActivity$SpinnerItem;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 258
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 255
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 132
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 146
    :cond_6
    iget-object v4, p0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v4, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 261
    :cond_7
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f030125

    invoke-direct {v1, p0, v2, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 263
    invoke-virtual {v1, v12}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 264
    iget-object v2, p0, Lflipboard/activities/ReportIssueActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 265
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 269
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->z:Ljava/io/File;

    if-eqz v0, :cond_8

    .line 270
    const v0, 0x7f0a02cc

    invoke-virtual {p0, v0}, Lflipboard/activities/ReportIssueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 271
    iget-object v1, p0, Lflipboard/activities/ReportIssueActivity;->z:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 275
    :cond_8
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialog;

    const v1, 0x7f0d033e

    invoke-direct {v0, p0, v1}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->w:Lflipboard/gui/dialog/FLProgressDialog;

    .line 276
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->w:Lflipboard/gui/dialog/FLProgressDialog;

    new-instance v1, Lflipboard/activities/ReportIssueActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/ReportIssueActivity$3;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 283
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->w:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 285
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v0, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 286
    const v1, 0x7f0d033f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 287
    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 288
    const v1, 0x7f0d0280

    new-instance v2, Lflipboard/activities/ReportIssueActivity$4;

    invoke-direct {v2, p0}, Lflipboard/activities/ReportIssueActivity$4;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 295
    const v1, 0x7f0d0278

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 296
    invoke-virtual {v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->x:Landroid/app/AlertDialog;

    .line 298
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v0, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 299
    const v1, 0x7f0d027c

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 300
    const v1, 0x7f0d027b

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 301
    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 302
    const v1, 0x7f0d0277

    new-instance v2, Lflipboard/activities/ReportIssueActivity$5;

    invoke-direct {v2, p0}, Lflipboard/activities/ReportIssueActivity$5;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 309
    const v1, 0x7f0d027e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 310
    invoke-virtual {v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->y:Landroid/app/AlertDialog;

    .line 312
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v0, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/ReportIssueActivity;->ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    .line 313
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    const v1, 0x7f0d01da

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 314
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 315
    iget-object v0, p0, Lflipboard/activities/ReportIssueActivity;->ae:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    const v1, 0x7f0d023f

    new-instance v2, Lflipboard/activities/ReportIssueActivity$6;

    invoke-direct {v2, p0}, Lflipboard/activities/ReportIssueActivity$6;-><init>(Lflipboard/activities/ReportIssueActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :cond_9
    move v2, v3

    goto/16 :goto_1
.end method

.method public sendIssue(Landroid/view/View;)V
    .locals 20

    .prologue
    .line 327
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 328
    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->z:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 330
    invoke-virtual/range {p0 .. p0}, Lflipboard/activities/ReportIssueActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    .line 332
    :try_start_0
    const-string v2, "flip_screenshot"

    const-string v3, ".jpg"

    invoke-static {v2, v3, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lflipboard/activities/ReportIssueActivity;->A:Ljava/io/File;

    .line 333
    new-instance v1, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/activities/ReportIssueActivity;->z:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 334
    new-instance v2, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/activities/ReportIssueActivity;->A:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 335
    const/16 v3, 0x400

    new-array v3, v3, [B

    .line 337
    :goto_0
    invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_3

    .line 338
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 347
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->q:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    iget-object v6, v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;->a:Ljava/lang/String;

    .line 348
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->r:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    iget-object v7, v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;->a:Ljava/lang/String;

    .line 349
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->v:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 350
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 351
    :cond_1
    const v1, 0x7f0d027a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/activities/ReportIssueActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 435
    :cond_2
    :goto_2
    return-void

    .line 340
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 341
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 354
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->w:Lflipboard/gui/dialog/FLProgressDialog;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/activities/ReportIssueActivity;->a(Landroid/app/Dialog;)V

    .line 355
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 356
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 357
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->s:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    iget-object v4, v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;->a:Ljava/lang/String;

    .line 358
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->B:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    iget-object v11, v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;->a:Ljava/lang/String;

    .line 359
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;

    iget-object v10, v1, Lflipboard/activities/ReportIssueActivity$SpinnerItem;->a:Ljava/lang/String;

    .line 361
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/activities/ReportIssueActivity;->M:Lflipboard/service/FlipboardManager;

    move-object/from16 v0, p0

    iget-object v12, v0, Lflipboard/activities/ReportIssueActivity;->D:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lflipboard/activities/ReportIssueActivity;->ac:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lflipboard/activities/ReportIssueActivity;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lflipboard/activities/ReportIssueActivity;->F:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity;->G:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity;->ai:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/activities/ReportIssueActivity;->z:Ljava/io/File;

    move-object/from16 v18, v0

    new-instance v3, Lflipboard/activities/ReportIssueActivity$7;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v8}, Lflipboard/activities/ReportIssueActivity$7;-><init>(Lflipboard/activities/ReportIssueActivity;Ljava/lang/String;)V

    new-instance v19, Lflipboard/service/FlipboardManager$SearchUserRequest;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lflipboard/service/FlipboardManager$SearchUserRequest;-><init>(Lflipboard/service/FlipboardManager;)V

    new-instance v1, Lflipboard/service/FlipboardManager$41;

    invoke-direct/range {v1 .. v18}, Lflipboard/service/FlipboardManager$41;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/FlipboardManager$CreateIssueObserver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    move-object/from16 v0, v19

    iput-object v8, v0, Lflipboard/service/FlipboardManager$SearchUserRequest;->a:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v1, v0, Lflipboard/service/FlipboardManager$SearchUserRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual/range {v19 .. v19}, Lflipboard/service/FlipboardManager$SearchUserRequest;->c()V

    .line 423
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_5

    .line 424
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 425
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 429
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "report_issue_previously_email"

    new-instance v3, Ljava/util/TreeSet;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 430
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/activities/ReportIssueActivity;->ah:Lflipboard/service/User;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/activities/ReportIssueActivity;->ad:Ljava/util/List;

    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Lflipboard/service/User$36;

    invoke-direct {v3, v1, v2}, Lflipboard/service/User$36;-><init>(Lflipboard/service/User;Ljava/util/List;)V

    invoke-virtual {v1, v3}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 432
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lflipboard/activities/ReportIssueActivity;->af:Z

    if-eqz v1, :cond_2

    .line 433
    invoke-virtual/range {p0 .. p0}, Lflipboard/activities/ReportIssueActivity;->finish()V

    goto/16 :goto_2
.end method
