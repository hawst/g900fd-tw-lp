.class public Lflipboard/activities/ChooseAccountActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ChooseAccountActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private n:Lflipboard/service/Account;

.field private o:Lflipboard/json/FLObject;

.field private p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/ChooseAccountActivity$AccountItem;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lflipboard/activities/ChooseAccountActivity$Adapter;

.field private s:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 637
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/objs/ConfigService;Ljava/lang/String;)Landroid/view/View;
    .locals 9

    .prologue
    .line 39
    const v0, 0x7f030030

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v0, 0x7f0a004f

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, p2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a004c

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    new-instance v2, Lflipboard/activities/ChooseAccountActivity$Adapter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, p0, p0, v0}, Lflipboard/activities/ChooseAccountActivity$Adapter;-><init>(Lflipboard/activities/ChooseAccountActivity;Landroid/content/Context;Ljava/util/List;)V

    const v0, 0x7f0a00c5

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v1, 0x7f0a00ba

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00bb

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v3, 0x7f0a00bc

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lflipboard/gui/FLTextIntf;

    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0d00e8

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    iget-object v7, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v8, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v0, Lflipboard/activities/ChooseAccountActivity$2;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/ChooseAccountActivity$2;-><init>(Lflipboard/activities/ChooseAccountActivity;Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/objs/ConfigService;Landroid/view/View;Lflipboard/gui/FLTextIntf;)V

    invoke-virtual {v7, v8, p1, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$ShareListRequest;

    return-object v6
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/json/FLObject;)Lflipboard/json/FLObject;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/service/Account;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Ljava/util/List;Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/objs/ConfigService;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/util/List;Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/objs/ConfigService;Z)V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Ljava/util/List;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/util/List;Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/List;Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/objs/ConfigService;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionListItem;",
            ">;",
            "Lflipboard/activities/ChooseAccountActivity$Adapter;",
            "Lflipboard/objs/ConfigService;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 284
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    .line 285
    iget-object v2, v0, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v3, "folder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 287
    iget-object v2, v0, Lflipboard/objs/SectionListItem;->bP:Ljava/lang/String;

    invoke-static {v2}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a(Ljava/lang/String;)Lflipboard/activities/ChooseAccountActivity$AccountItem;

    move-result-object v2

    invoke-virtual {p2, v2}, Lflipboard/activities/ChooseAccountActivity$Adapter;->add(Ljava/lang/Object;)V

    .line 289
    iget-object v0, v0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    invoke-direct {p0, v0, p2, p3, p4}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/util/List;Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/objs/ConfigService;Z)V

    goto :goto_0

    .line 291
    :cond_1
    iget-object v2, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 294
    if-eqz p4, :cond_2

    iget-boolean v2, v0, Lflipboard/objs/SectionListItem;->i:Z

    if-eqz v2, :cond_2

    .line 295
    invoke-direct {p0, v0, p3}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)Z

    .line 297
    :cond_2
    new-instance v2, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    invoke-direct {v2, v0, p3}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)V

    invoke-virtual {p2, v2}, Lflipboard/activities/ChooseAccountActivity$Adapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 300
    :cond_3
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionListItem;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    .line 274
    iget-object v2, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 275
    iget-object v0, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    :cond_1
    iget-object v2, v0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 277
    iget-object v0, v0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    invoke-direct {p0, v0, p2}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/util/List;Ljava/util/Set;)V

    goto :goto_0

    .line 280
    :cond_2
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/service/Account;)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/service/Account;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v0, p1}, Lflipboard/json/FLObject;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/ChooseAccountActivity;Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;)Z

    move-result v0

    return v0
.end method

.method private a(Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)Z
    .locals 2

    .prologue
    .line 316
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;)Z

    move-result v0

    return v0
.end method

.method private a(Lflipboard/service/Account;)Z
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    if-eq p1, v0, :cond_0

    .line 308
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    .line 309
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    .line 310
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 320
    if-eqz p1, :cond_3

    .line 321
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    if-nez v1, :cond_0

    .line 322
    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    iput-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    .line 324
    :cond_0
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v1, p1}, Lflipboard/json/FLObject;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 326
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->size()I

    move-result v1

    if-le v1, v0, :cond_3

    .line 327
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v1, p1}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    :goto_0
    return v0

    .line 332
    :cond_1
    iget-boolean v1, p3, Lflipboard/objs/ConfigService;->aw:Z

    if-nez v1, :cond_2

    .line 334
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->clear()V

    .line 336
    :cond_2
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v1, p1, p2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 340
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/ChooseAccountActivity;)Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/json/FLObject;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAccountActivity;->setResult(I)V

    .line 145
    :goto_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 146
    return-void

    .line 140
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 141
    const-string v1, "flipboard.extra.selectedAccount"

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    invoke-virtual {v2}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->o:Lflipboard/json/FLObject;

    invoke-static {v0, v1}, Lflipboard/util/ShareHelper;->a(Landroid/content/Intent;Lflipboard/json/FLObject;)V

    .line 143
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lflipboard/activities/ChooseAccountActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 353
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "ChooseAccountActivity:animateBack"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget-object v0, v1, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a:Ljava/util/List;

    iget-object v2, v1, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->notifyDataSetChanged()V

    iget-object v0, v1, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->b:Lflipboard/activities/ChooseAccountActivity;

    const v2, 0x7f04001a

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    iget-object v0, v1, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->b:Lflipboard/activities/ChooseAccountActivity;

    const v2, 0x7f04001b

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    invoke-virtual {v1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->showPrevious()V

    invoke-virtual {v1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->getChildCount()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->removeViews(II)V

    .line 359
    :goto_0
    return-void

    .line 358
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 51
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "flipboard.extra.selectedAccount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/ShareHelper;->a(Landroid/content/Intent;)Lflipboard/json/FLObject;

    move-result-object v5

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    .line 63
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->x()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 64
    iget-object v7, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v8, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v7

    .line 65
    if-eqz v7, :cond_a

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->g:Z

    if-eqz v0, :cond_a

    .line 66
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v8, v7, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v8, v8, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v8

    .line 67
    new-instance v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    invoke-direct {v0, v7, v8}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/service/Account;Lflipboard/objs/ConfigService;)V

    .line 68
    iget-object v8, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    if-nez v1, :cond_a

    invoke-virtual {v7}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    :goto_2
    move-object v1, v0

    .line 75
    goto :goto_1

    .line 76
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 77
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0d035a

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a(Ljava/lang/String;)Lflipboard/activities/ChooseAccountActivity$AccountItem;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 83
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 84
    iget-boolean v7, v0, Lflipboard/objs/ConfigService;->g:Z

    if-eqz v7, :cond_4

    iget-object v7, p0, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v7}, Lflipboard/service/FlipboardManager;->x()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 85
    if-nez v3, :cond_3

    .line 86
    iget-object v3, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d001e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a(Ljava/lang/String;)Lflipboard/activities/ChooseAccountActivity$AccountItem;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    const/4 v3, 0x1

    .line 89
    :cond_3
    new-instance v7, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    invoke-direct {v7, v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/objs/ConfigService;)V

    .line 90
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move v0, v3

    move v3, v0

    .line 92
    goto :goto_3

    .line 94
    :cond_5
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 95
    sget-object v0, Lflipboard/activities/ChooseAccountActivity;->K:Lflipboard/util/Log;

    const-string v1, "No services logged in"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->finish()V

    goto/16 :goto_0

    .line 101
    :cond_6
    if-nez v1, :cond_9

    .line 102
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    move-object v3, v0

    .line 106
    :goto_4
    iget-object v0, v3, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    invoke-direct {p0, v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/service/Account;)Z

    .line 107
    if-nez v5, :cond_8

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    if-eqz v0, :cond_8

    .line 110
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->n:Lflipboard/service/Account;

    iget-object v0, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v0, v0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    .line 112
    :goto_5
    if-eqz v0, :cond_7

    .line 113
    invoke-virtual {v0}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 114
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v5, v3, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-direct {p0, v1, v0, v5}, Lflipboard/activities/ChooseAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;)Z

    goto :goto_6

    .line 119
    :cond_7
    new-instance v0, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;-><init>(Lflipboard/activities/ChooseAccountActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    .line 120
    const v0, 0x7f030030

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 121
    const v0, 0x7f0a004f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const v0, 0x7f0a004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 123
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 126
    new-instance v0, Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity;->q:Ljava/util/List;

    invoke-direct {v0, p0, p0, v2}, Lflipboard/activities/ChooseAccountActivity$Adapter;-><init>(Lflipboard/activities/ChooseAccountActivity;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->r:Lflipboard/activities/ChooseAccountActivity$Adapter;

    .line 127
    const v0, 0x7f0a00c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->s:Landroid/widget/ListView;

    .line 128
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->s:Landroid/widget/ListView;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity;->r:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity;->r:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a(Landroid/view/View;Lflipboard/activities/ChooseAccountActivity$Adapter;)V

    .line 132
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity;->p:Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAccountActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_8
    move-object v0, v5

    goto/16 :goto_5

    :cond_9
    move-object v3, v1

    goto/16 :goto_4

    :cond_a
    move-object v0, v1

    goto/16 :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_2

    instance-of v0, v1, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    sget-object v2, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->d:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    if-ne v0, v2, :cond_2

    .line 157
    check-cast v1, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v0, v1, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    new-instance v1, Lflipboard/activities/ChooseAccountActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ChooseAccountActivity$1;-><init>(Lflipboard/activities/ChooseAccountActivity;)V

    invoke-static {p0, v0, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;Lflipboard/util/SocialHelper$ServiceLoginObserver;)V

    goto :goto_0

    .line 177
    :cond_2
    const v0, 0x7f0a0056

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 178
    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    goto :goto_0
.end method
