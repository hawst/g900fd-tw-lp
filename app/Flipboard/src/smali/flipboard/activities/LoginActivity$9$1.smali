.class Lflipboard/activities/LoginActivity$9$1;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field final synthetic b:Lflipboard/activities/LoginActivity$9;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity$9;Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iput-object p2, p0, Lflipboard/activities/LoginActivity$9$1;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 357
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 359
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0, v6}, Lflipboard/activities/LoginActivity;->setResult(I)V

    .line 360
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->a:Lflipboard/activities/LoginActivity$SignInMethod;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->g(Lflipboard/activities/LoginActivity;)Lflipboard/objs/UsageEventV2$EventCategory;

    move-result-object v1

    iget-object v3, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v3, v3, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v3}, Lflipboard/activities/LoginActivity;->h(Lflipboard/activities/LoginActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v3

    invoke-static {v4, v0, v1, v3}, Lflipboard/activities/LoginActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 361
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iput-boolean v4, v0, Lflipboard/activities/LoginActivity;->o:Z

    .line 364
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0}, Lflipboard/activities/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 365
    if-eqz v0, :cond_3

    const-string v1, "extra_open_section_after_login_from_samsung"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 366
    const-string v1, "extra_open_section_after_login_from_samsung"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 367
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 368
    if-nez v0, :cond_0

    .line 369
    new-instance v0, Lflipboard/service/Section;

    const-string v3, "flipboard"

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 370
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 372
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 373
    const-string v2, "source"

    const-string v3, "samsungWidget"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v2, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0, v2, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    .line 375
    invoke-static {}, Lflipboard/util/AndroidUtil;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 402
    :cond_1
    :goto_0
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0}, Lflipboard/activities/LoginActivity;->finish()V

    .line 403
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    .line 404
    if-eqz v2, :cond_2

    .line 405
    const/high16 v0, 0x4000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 406
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0, v2}, Lflipboard/activities/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 408
    :cond_2
    const-string v0, "FlipLogin Succeeded"

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-static {v0, v1, v2}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 409
    return-void

    .line 381
    :cond_3
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->h(Lflipboard/activities/LoginActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v0

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_4

    .line 384
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 385
    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 387
    iget-object v3, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    if-eqz v3, :cond_5

    .line 388
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    .line 390
    :goto_1
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 391
    const-string v4, "name"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    const-string v1, "profile"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0, v6, v3}, Lflipboard/activities/LoginActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 396
    :cond_4
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$1;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 397
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    .line 399
    const-string v0, "page"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method
