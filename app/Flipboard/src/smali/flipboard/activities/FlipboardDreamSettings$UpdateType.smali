.class public final enum Lflipboard/activities/FlipboardDreamSettings$UpdateType;
.super Ljava/lang/Enum;
.source "FlipboardDreamSettings.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/activities/FlipboardDreamSettings$UpdateType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

.field public static final enum b:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

.field public static final enum c:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

.field private static final synthetic d:[Lflipboard/activities/FlipboardDreamSettings$UpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-instance v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    const-string v1, "WIFI_ONLY"

    invoke-direct {v0, v1, v2}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->a:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    new-instance v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v3}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->b:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    new-instance v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v4}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->c:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    sget-object v1, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->a:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->b:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->c:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->d:[Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/activities/FlipboardDreamSettings$UpdateType;
    .locals 1

    .prologue
    .line 101
    const-class v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    return-object v0
.end method

.method public static values()[Lflipboard/activities/FlipboardDreamSettings$UpdateType;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->d:[Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    invoke-virtual {v0}, [Lflipboard/activities/FlipboardDreamSettings$UpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    return-object v0
.end method
