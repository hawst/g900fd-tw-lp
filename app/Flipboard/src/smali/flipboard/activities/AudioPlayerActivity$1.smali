.class Lflipboard/activities/AudioPlayerActivity$1;
.super Ljava/lang/Object;
.source "AudioPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/AudioPlayerActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/AudioPlayerActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLBusyView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLBusyView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->b(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v1, "inAppBufferButton"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->b(Ljava/lang/String;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->c(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLToggleImageButton;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLToggleImageButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->b(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v1, "inAppPauseButton"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->c(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLToggleImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    .line 126
    const-string v0, "pause"

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_2
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->c(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLToggleImageButton;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLToggleImageButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->b(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v1, "inAppPlayButton"

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    if-eqz v2, :cond_4

    iget-object v1, v0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    const-string v2, "play"

    iput-object v2, v1, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/service/audio/FLAudioManager;->a(Z)V

    .line 132
    :cond_3
    :goto_1
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$1;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->c(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLToggleImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    .line 134
    const-string v0, "play"

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_4
    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
