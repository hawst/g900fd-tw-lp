.class Lflipboard/activities/ChooseAccountActivity$Adapter$1;
.super Ljava/lang/Object;
.source "ChooseAccountActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

.field final synthetic b:Lflipboard/activities/ChooseAccountActivity$Adapter;


# direct methods
.method constructor <init>(Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/activities/ChooseAccountActivity$AccountItem;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iput-object p2, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 554
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    if-nez v1, :cond_6

    .line 558
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->d(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 560
    if-nez p2, :cond_1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 616
    :cond_0
    :goto_1
    return-void

    .line 560
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 564
    :cond_2
    if-nez p2, :cond_4

    .line 568
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 586
    :cond_3
    :goto_2
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->av:Z

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v2, v2, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/objs/ConfigService;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 590
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->d(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a(Landroid/view/View;Lflipboard/activities/ChooseAccountActivity$Adapter;)V

    goto :goto_1

    .line 571
    :cond_4
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    invoke-static {v0, v1}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/service/Account;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/service/Account;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 573
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/service/Account;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v0, v0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    .line 574
    if-eqz v0, :cond_5

    .line 575
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v2, v2, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v2}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/service/Account;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 576
    invoke-virtual {v0}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 577
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v4, v0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1, v2}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;)Z

    goto :goto_3

    .line 581
    :cond_5
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->notifyDataSetChanged()V

    goto/16 :goto_2

    .line 593
    :cond_6
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v0, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v0, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 601
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1, v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Ljava/lang/String;)Z

    move-result v1

    if-eq p2, v1, :cond_0

    .line 602
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v2, v2, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v3, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    iget-object v3, v3, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-static {v1, v2, v3}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)Z

    .line 607
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1, v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Ljava/lang/String;)Z

    move-result v0

    .line 608
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eq v0, v1, :cond_7

    .line 609
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 613
    :cond_7
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$Adapter$1;->b:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->notifyDataSetChanged()V

    goto/16 :goto_1
.end method
