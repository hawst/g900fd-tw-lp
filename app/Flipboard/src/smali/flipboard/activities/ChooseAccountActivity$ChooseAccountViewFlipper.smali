.class Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;
.super Landroid/widget/ViewFlipper;
.source "ChooseAccountActivity.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/ChooseAccountActivity$Adapter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/ChooseAccountActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ChooseAccountActivity;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 643
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->b:Lflipboard/activities/ChooseAccountActivity;

    .line 644
    invoke-direct {p0, p2}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;)V

    .line 640
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a:Ljava/util/List;

    .line 645
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lflipboard/activities/ChooseAccountActivity$Adapter;)V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655
    invoke-virtual {p0, p1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->addView(Landroid/view/View;)V

    .line 658
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 659
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->b:Lflipboard/activities/ChooseAccountActivity;

    const v1, 0x7f040019

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 660
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->b:Lflipboard/activities/ChooseAccountActivity;

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 661
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->showNext()V

    .line 663
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 649
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAccountActivity$ChooseAccountViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
