.class public Lflipboard/activities/SettingsActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "SettingsActivity.java"


# instance fields
.field n:Lflipboard/activities/SettingsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "settings"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 28
    invoke-virtual {p0}, Lflipboard/activities/SettingsActivity;->finish()V

    .line 29
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 17
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const v0, 0x7f030074

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsActivity;->setContentView(I)V

    .line 19
    invoke-static {}, Lflipboard/activities/SettingsFragment;->a()Lflipboard/activities/SettingsFragment;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SettingsActivity;->n:Lflipboard/activities/SettingsFragment;

    .line 20
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 21
    const v1, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/SettingsActivity;->n:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 22
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 23
    return-void
.end method
