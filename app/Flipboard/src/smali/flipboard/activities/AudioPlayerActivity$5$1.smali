.class Lflipboard/activities/AudioPlayerActivity$5$1;
.super Ljava/lang/Object;
.source "AudioPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic b:J

.field final synthetic c:Lflipboard/activities/AudioPlayerActivity$5;


# direct methods
.method constructor <init>(Lflipboard/activities/AudioPlayerActivity$5;JJ)V
    .locals 0

    .prologue
    .line 429
    iput-object p1, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->c:Lflipboard/activities/AudioPlayerActivity$5;

    iput-wide p2, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->a:J

    iput-wide p4, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 434
    iget-wide v0, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->a:J

    iget-wide v4, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->b:J

    sub-long/2addr v0, v4

    .line 436
    iget-object v4, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->c:Lflipboard/activities/AudioPlayerActivity$5;

    iget-object v4, v4, Lflipboard/activities/AudioPlayerActivity$5;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v4}, Lflipboard/activities/AudioPlayerActivity;->h(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLLabelTextView;

    move-result-object v4

    const-string v5, "-%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    cmp-long v8, v0, v2

    if-lez v8, :cond_0

    :goto_0
    invoke-static {v0, v1}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->c:Lflipboard/activities/AudioPlayerActivity$5;

    iget-object v0, v0, Lflipboard/activities/AudioPlayerActivity$5;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v0}, Lflipboard/activities/AudioPlayerActivity;->i(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLLabelTextView;

    move-result-object v0

    iget-wide v2, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->b:J

    invoke-static {v2, v3}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-wide v0, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->b:J

    iget-wide v2, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->a:J

    invoke-static {v0, v1, v2, v3}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(JJ)I

    move-result v0

    .line 442
    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity$5$1;->c:Lflipboard/activities/AudioPlayerActivity$5;

    iget-object v1, v1, Lflipboard/activities/AudioPlayerActivity$5;->a:Lflipboard/activities/AudioPlayerActivity;

    invoke-static {v1}, Lflipboard/activities/AudioPlayerActivity;->j(Lflipboard/activities/AudioPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 443
    return-void

    :cond_0
    move-wide v0, v2

    .line 436
    goto :goto_0
.end method
