.class public Lflipboard/activities/FacebookAuthenticateFragment;
.super Landroid/support/v4/app/Fragment;
.source "FacebookAuthenticateFragment.java"


# static fields
.field public static final b:Lflipboard/util/Log;


# instance fields
.field public final a:Lflipboard/service/FlipboardManager;

.field c:Lflipboard/activities/FacebookAuthenticateFragment$CancelListener;

.field public d:Lflipboard/activities/LoginActivity$LoginInitFrom;

.field e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

.field public f:Lflipboard/service/ServiceReloginObserver;

.field public g:Ljava/lang/String;

.field public h:Lflipboard/objs/UsageEventV2$EventCategory;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private t:Lflipboard/service/Section;

.field private u:Ljava/lang/String;

.field private v:Lcom/facebook/Session$StatusCallback;

.field private w:Lcom/facebook/UiLifecycleHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "servicelogin"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 34
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    .line 56
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->FacebookSingleSignOnReadPermissions:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->i:Ljava/util/List;

    .line 57
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->FacebookSingleSignOnPublishPermissions:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->j:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 79
    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$1;

    invoke-direct {v0, p0}, Lflipboard/activities/FacebookAuthenticateFragment$1;-><init>(Lflipboard/activities/FacebookAuthenticateFragment;)V

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->v:Lcom/facebook/Session$StatusCallback;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->f:Lflipboard/service/ServiceReloginObserver;

    .line 92
    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 568
    return-void
.end method

.method static synthetic a(Lflipboard/activities/FacebookAuthenticateFragment;Lflipboard/service/Section;)Lflipboard/service/Section;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lflipboard/activities/FacebookAuthenticateFragment;->t:Lflipboard/service/Section;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/FacebookAuthenticateFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 306
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 307
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "facebook"

    const-string v2, "native"

    const-string v4, "FlipSSOWithToken Cancelled"

    invoke-static {v0, v1, v2, v4}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment$6;->a:[I

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v1}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 329
    :goto_0
    return-void

    .line 310
    :pswitch_0
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "facebook"

    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->d()Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    goto :goto_0

    .line 313
    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "facebook"

    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->d()Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    goto :goto_0

    .line 316
    :pswitch_2
    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "facebook"

    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->d()Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v5

    new-instance v0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    iget-object v4, v2, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, v2, v4}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const-string v4, "/v1/flipboard/loginWithSSOWithToken"

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    goto :goto_0

    .line 320
    :pswitch_3
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 321
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 322
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->d:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-virtual {v2}, Lflipboard/activities/LoginActivity$LoginInitFrom;->name()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflipboard/activities/FacebookAuthenticateFragment$3;

    invoke-direct {v3, p0}, Lflipboard/activities/FacebookAuthenticateFragment$3;-><init>(Lflipboard/activities/FacebookAuthenticateFragment;)V

    invoke-virtual {v1, v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Flap$Request;

    goto :goto_0

    .line 325
    :cond_1
    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "facebook"

    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->d()Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v5

    new-instance v0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    iget-object v4, v2, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, v2, v4}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const-string v4, "/v1/flipboard/createWithSSOWithToken"

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    goto/16 :goto_0

    .line 308
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lflipboard/activities/FacebookAuthenticateFragment;)Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 160
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/facebook/Session;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    invoke-virtual {v0}, Lcom/facebook/Session;->closeAndClearTokenInformation()V

    .line 165
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00de

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    new-instance v1, Lcom/facebook/Session$Builder;

    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/Session$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/facebook/Session$Builder;->setApplicationId(Ljava/lang/String;)Lcom/facebook/Session$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/Session$Builder;->build()Lcom/facebook/Session;

    move-result-object v0

    .line 167
    invoke-static {v0}, Lcom/facebook/Session;->setActiveSession(Lcom/facebook/Session;)V

    .line 168
    iget-boolean v1, p0, Lflipboard/activities/FacebookAuthenticateFragment;->o:Z

    if-eqz v1, :cond_1

    .line 169
    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 170
    new-instance v1, Lcom/facebook/Session$OpenRequest;

    invoke-direct {v1, p0}, Lcom/facebook/Session$OpenRequest;-><init>(Landroid/support/v4/app/Fragment;)V

    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->v:Lcom/facebook/Session$StatusCallback;

    invoke-virtual {v1, v2}, Lcom/facebook/Session$OpenRequest;->setCallback(Lcom/facebook/Session$StatusCallback;)Lcom/facebook/Session$OpenRequest;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->j:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/facebook/Session$OpenRequest;->setPermissions(Ljava/util/List;)Lcom/facebook/Session$OpenRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/Session;->openForPublish(Lcom/facebook/Session$OpenRequest;)V

    .line 175
    :goto_0
    return-void

    .line 172
    :cond_1
    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 173
    new-instance v1, Lcom/facebook/Session$OpenRequest;

    invoke-direct {v1, p0}, Lcom/facebook/Session$OpenRequest;-><init>(Landroid/support/v4/app/Fragment;)V

    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->v:Lcom/facebook/Session$StatusCallback;

    invoke-virtual {v1, v2}, Lcom/facebook/Session$OpenRequest;->setCallback(Lcom/facebook/Session$StatusCallback;)Lcom/facebook/Session$OpenRequest;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->i:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/facebook/Session$OpenRequest;->setPermissions(Ljava/util/List;)Lcom/facebook/Session$OpenRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/Session;->openForRead(Lcom/facebook/Session$OpenRequest;)V

    goto :goto_0
.end method

.method static synthetic c(Lflipboard/activities/FacebookAuthenticateFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 264
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 268
    :cond_0
    return-void
.end method

.method private d()Lflipboard/service/Flap$TypedResultObserver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$2;

    invoke-direct {v0, p0}, Lflipboard/activities/FacebookAuthenticateFragment$2;-><init>(Lflipboard/activities/FacebookAuthenticateFragment;)V

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/FacebookAuthenticateFragment;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->l:Z

    return v0
.end method

.method static synthetic e(Lflipboard/activities/FacebookAuthenticateFragment;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->n:Z

    return v0
.end method

.method static synthetic f(Lflipboard/activities/FacebookAuthenticateFragment;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->t:Lflipboard/service/Section;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 149
    const-string v0, "com.facebook.katana"

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->b()V

    .line 155
    :goto_0
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->r:Z

    .line 153
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/SSOLoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x1e3a

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040001

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method protected final a(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 179
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Facebook Login Session Changed to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_11

    .line 183
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/Session;->getPermissions()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    .line 184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/Session;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/facebook/SessionState;->OPENED:Lcom/facebook/SessionState;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/facebook/SessionState;->OPENED_TOKEN_UPDATED:Lcom/facebook/SessionState;

    if-ne p2, v0, :cond_7

    .line 186
    :cond_1
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->o:Z

    if-eqz v0, :cond_c

    .line 188
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->p:Z

    if-eqz v0, :cond_a

    .line 191
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->q:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-ne v0, v1, :cond_6

    .line 192
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->f:Lflipboard/service/ServiceReloginObserver;

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->f:Lflipboard/service/ServiceReloginObserver;

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V

    .line 261
    :cond_2
    :goto_1
    return-void

    .line 183
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/Session;->getPermissions()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->o:Z

    :goto_2
    invoke-virtual {p1}, Lcom/facebook/Session;->getPermissions()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->p:Z

    goto :goto_0

    :cond_4
    iput-boolean v3, p0, Lflipboard/activities/FacebookAuthenticateFragment;->o:Z

    goto :goto_2

    :cond_5
    iput-boolean v3, p0, Lflipboard/activities/FacebookAuthenticateFragment;->p:Z

    goto :goto_0

    .line 198
    :cond_6
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 199
    invoke-virtual {p1}, Lcom/facebook/Session;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/FacebookAuthenticateFragment;->a(Ljava/lang/String;)V

    .line 222
    :cond_7
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/Session;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/facebook/SessionState;->CLOSED_LOGIN_FAILED:Lcom/facebook/SessionState;

    if-ne p2, v0, :cond_2

    .line 223
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 224
    if-eqz p3, :cond_9

    .line 225
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_10

    .line 228
    const-string v1, "User canceled operation."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "User canceled log in."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 229
    :cond_8
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 230
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-ne v0, v1, :cond_9

    .line 231
    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->c()V

    .line 253
    :cond_9
    :goto_4
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->c:Lflipboard/activities/FacebookAuthenticateFragment$CancelListener;

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->c:Lflipboard/activities/FacebookAuthenticateFragment$CancelListener;

    invoke-interface {v0}, Lflipboard/activities/FacebookAuthenticateFragment$CancelListener;->a()V

    goto :goto_1

    .line 202
    :cond_a
    iput-boolean v3, p0, Lflipboard/activities/FacebookAuthenticateFragment;->q:Z

    .line 204
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->k:Z

    if-eqz v0, :cond_b

    .line 206
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 207
    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->b()V

    goto :goto_3

    .line 210
    :cond_b
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 211
    invoke-virtual {p1}, Lcom/facebook/Session;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/FacebookAuthenticateFragment;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 215
    :cond_c
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->p:Z

    if-eqz v0, :cond_d

    .line 216
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    goto :goto_3

    .line 218
    :cond_d
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    goto :goto_3

    .line 234
    :cond_e
    const-string v1, "The user denied the app"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 235
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 236
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-ne v0, v1, :cond_9

    .line 237
    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->c()V

    goto :goto_4

    .line 240
    :cond_f
    const-string v1, "called from onResume()"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 241
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    .line 242
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-ne v0, v1, :cond_9

    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->r:Z

    if-eqz v0, :cond_9

    .line 244
    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->c()V

    goto :goto_4

    .line 249
    :cond_10
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookAuthenticateFragment - failed with no error message, exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    invoke-direct {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->c()V

    goto :goto_4

    .line 259
    :cond_11
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment;->b:Lflipboard/util/Log;

    goto/16 :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 533
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 534
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/UiLifecycleHelper;->onActivityResult(IILandroid/content/Intent;)V

    .line 535
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 98
    new-instance v0, Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->v:Lcom/facebook/Session$StatusCallback;

    invoke-direct {v0, v1, v2}, Lcom/facebook/UiLifecycleHelper;-><init>(Landroid/app/Activity;Lcom/facebook/Session$StatusCallback;)V

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    .line 99
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v0, p1}, Lcom/facebook/UiLifecycleHelper;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {v0}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v1

    .line 105
    sget-object v2, Lcom/facebook/SessionState;->CLOSED:Lcom/facebook/SessionState;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/SessionState;->CLOSED_LOGIN_FAILED:Lcom/facebook/SessionState;

    if-eq v1, v2, :cond_0

    .line 106
    invoke-virtual {v0}, Lcom/facebook/Session;->closeAndClearTokenInformation()V

    .line 109
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 114
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 115
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "fragmentAction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    .line 116
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "startSectionAfterSuccess"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->n:Z

    .line 117
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finishActivityOnComplete"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->l:Z

    .line 118
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "openNewSessionOnCreate"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->m:Z

    .line 119
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "getAllPermissions"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->k:Z

    .line 120
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "invteString"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->u:Ljava/lang/String;

    .line 121
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 122
    const-string v0, ""

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->u:Ljava/lang/String;

    .line 124
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "errorMessage"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->g:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->g:Ljava/lang/String;

    .line 133
    :cond_1
    :goto_0
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-ne v0, v1, :cond_2

    .line 134
    iput-boolean v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->k:Z

    .line 135
    iput-boolean v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->m:Z

    .line 136
    iput-boolean v3, p0, Lflipboard/activities/FacebookAuthenticateFragment;->l:Z

    .line 137
    iput-boolean v3, p0, Lflipboard/activities/FacebookAuthenticateFragment;->n:Z

    .line 138
    iput-boolean v2, p0, Lflipboard/activities/FacebookAuthenticateFragment;->q:Z

    .line 142
    :cond_2
    iget-boolean v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->m:Z

    if-eqz v0, :cond_3

    .line 143
    invoke-virtual {p0}, Lflipboard/activities/FacebookAuthenticateFragment;->a()V

    .line 145
    :cond_3
    const/4 v0, 0x0

    return-object v0

    .line 129
    :cond_4
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->b:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    iput-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 547
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 548
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v0}, Lcom/facebook/UiLifecycleHelper;->onDestroy()V

    .line 549
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 540
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 541
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v0}, Lcom/facebook/UiLifecycleHelper;->onPause()V

    .line 542
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 292
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 294
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_1

    .line 296
    invoke-virtual {v0}, Lcom/facebook/Session;->isOpened()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/Session;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 298
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "called from onResume()"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/activities/FacebookAuthenticateFragment;->a(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V

    .line 301
    :cond_1
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v0}, Lcom/facebook/UiLifecycleHelper;->onResume()V

    .line 302
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 554
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 556
    :try_start_0
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment;->w:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v0, p1}, Lcom/facebook/UiLifecycleHelper;->onSaveInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    :goto_0
    return-void

    .line 557
    :catch_0
    move-exception v0

    .line 559
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
