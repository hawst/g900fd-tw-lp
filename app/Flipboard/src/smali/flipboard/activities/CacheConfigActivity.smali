.class public Lflipboard/activities/CacheConfigActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "CacheConfigActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 191
    return-void
.end method

.method static synthetic a(Lflipboard/activities/CacheConfigActivity;)Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity;->q:Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/CacheConfigActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lflipboard/activities/CacheConfigActivity;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/CacheConfigActivity;Z)Z
    .locals 0

    .prologue
    .line 30
    iput-boolean p1, p0, Lflipboard/activities/CacheConfigActivity;->p:Z

    return p1
.end method

.method static synthetic b(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/CacheConfigActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lflipboard/activities/CacheConfigActivity;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/CacheConfigActivity;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lflipboard/activities/CacheConfigActivity;->p:Z

    return v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 133
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 134
    const v1, 0x7f0d02a7

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 135
    const v1, 0x7f0d02a9

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 136
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 137
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 138
    new-instance v1, Lflipboard/activities/CacheConfigActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/CacheConfigActivity$3;-><init>(Lflipboard/activities/CacheConfigActivity;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 174
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "cache_confirmation"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 175
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 124
    iget-boolean v1, p0, Lflipboard/activities/CacheConfigActivity;->p:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 125
    invoke-direct {p0}, Lflipboard/activities/CacheConfigActivity;->j()V

    .line 129
    :goto_1
    return-void

    .line 124
    :cond_1
    iget-object v1, p0, Lflipboard/activities/CacheConfigActivity;->n:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    const-string v3, "cache_location"

    const-string v4, "external"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/activities/CacheConfigActivity;->o:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    const-string v3, "cache_size"

    const-string v4, "128MB"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :cond_2
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    const-string v1, "cache_location"

    const-string v2, "external"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CacheConfigActivity;->n:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    const-string v1, "cache_size"

    const-string v2, "128MB"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CacheConfigActivity;->o:Ljava/lang/String;

    .line 45
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/CacheConfigActivity;->setContentView(I)V

    .line 47
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/CacheConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 48
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 50
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/CacheConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 51
    const v1, 0x7f0d02a7

    invoke-virtual {p0, v1}, Lflipboard/activities/CacheConfigActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 52
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/CacheConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 54
    new-instance v1, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;

    invoke-direct {v1, p0}, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;-><init>(Lflipboard/activities/CacheConfigActivity;)V

    iput-object v1, p0, Lflipboard/activities/CacheConfigActivity;->q:Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;

    .line 55
    iget-object v1, p0, Lflipboard/activities/CacheConfigActivity;->q:Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 56
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 57
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 61
    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    :goto_0
    return-void

    .line 65
    :cond_0
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 67
    :pswitch_0
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 68
    const v3, 0x7f0d02ad

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 69
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 71
    invoke-virtual {p0}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02aa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 73
    iget-object v4, p0, Lflipboard/activities/CacheConfigActivity;->n:Ljava/lang/String;

    const-string v5, "internal"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 74
    :goto_1
    invoke-virtual {v2, v3, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 75
    new-instance v0, Lflipboard/activities/CacheConfigActivity$1;

    invoke-direct {v0, p0}, Lflipboard/activities/CacheConfigActivity$1;-><init>(Lflipboard/activities/CacheConfigActivity;)V

    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 87
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "cache_location"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 73
    goto :goto_1

    .line 90
    :pswitch_1
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 91
    const v1, 0x7f0d02ae

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 92
    sget-object v3, Lflipboard/activities/SettingsFragment;->b:[Ljava/lang/String;

    .line 93
    const/4 v1, -0x1

    .line 94
    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_3

    .line 95
    aget-object v4, v3, v0

    iget-object v5, p0, Lflipboard/activities/CacheConfigActivity;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 100
    :goto_3
    invoke-virtual {v2, v3, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 101
    new-instance v0, Lflipboard/activities/CacheConfigActivity$2;

    invoke-direct {v0, p0, v3}, Lflipboard/activities/CacheConfigActivity$2;-><init>(Lflipboard/activities/CacheConfigActivity;[Ljava/lang/String;)V

    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 113
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "cache_size"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 116
    :pswitch_2
    iput-boolean v1, p0, Lflipboard/activities/CacheConfigActivity;->p:Z

    .line 117
    invoke-direct {p0}, Lflipboard/activities/CacheConfigActivity;->j()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_3

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
