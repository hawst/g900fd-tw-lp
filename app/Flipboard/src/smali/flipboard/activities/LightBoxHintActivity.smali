.class public Lflipboard/activities/LightBoxHintActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "LightBoxHintActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final f()V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x0

    const v1, 0x7f040008

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/LightBoxHintActivity;->overridePendingTransition(II)V

    .line 65
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 26
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/LightBoxHintActivity;->W:Z

    .line 28
    const v0, 0x7f0300c8

    invoke-virtual {p0, v0}, Lflipboard/activities/LightBoxHintActivity;->setContentView(I)V

    .line 30
    invoke-virtual {p0}, Lflipboard/activities/LightBoxHintActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 34
    invoke-virtual {p0}, Lflipboard/activities/LightBoxHintActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 35
    const-string v2, "extra_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    const-string v2, "extra_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    const v3, 0x7f0a0254

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 41
    :cond_0
    const-string v0, "extra_url"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    const v0, 0x7f0a0255

    invoke-virtual {p0, v0}, Lflipboard/activities/LightBoxHintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLWebView;

    .line 43
    new-instance v2, Lflipboard/activities/LightBoxHintActivity$1;

    invoke-direct {v2, p0, p0}, Lflipboard/activities/LightBoxHintActivity$1;-><init>(Lflipboard/activities/LightBoxHintActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 56
    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 57
    return-void
.end method
