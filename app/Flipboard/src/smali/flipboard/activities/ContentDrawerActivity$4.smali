.class Lflipboard/activities/ContentDrawerActivity$4;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "ContentDrawerActivity.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/objs/ContentDrawerListItem;

.field final synthetic c:Lflipboard/activities/ContentDrawerActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ContentDrawerActivity;Ljava/lang/String;Lflipboard/objs/ContentDrawerListItem;)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lflipboard/activities/ContentDrawerActivity$4;->c:Lflipboard/activities/ContentDrawerActivity;

    iput-object p2, p0, Lflipboard/activities/ContentDrawerActivity$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/ContentDrawerActivity$4;->b:Lflipboard/objs/ContentDrawerListItem;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 464
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->a(Landroid/support/v4/app/DialogFragment;)V

    .line 465
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity$4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->h(Ljava/lang/String;)Lflipboard/objs/Magazine;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v0, Lflipboard/service/User;->n:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_4

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v2, "failed to find magazine for deleting: %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v1, v1, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    aput-object v1, v3, v7

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 467
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity$4;->c:Lflipboard/activities/ContentDrawerActivity;

    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity$4;->a:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/ShareHelper;->a(Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity$4;->c:Lflipboard/activities/ContentDrawerActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 469
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity$4;->b:Lflipboard/objs/ContentDrawerListItem;

    invoke-virtual {v1, v2}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ContentDrawerListItem;)I

    .line 470
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    const v1, 0x7f0a0102

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    .line 472
    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity$4;->c:Lflipboard/activities/ContentDrawerActivity;

    const v3, 0x7f0d0023

    invoke-virtual {v2, v3}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 473
    iput-boolean v7, v0, Lflipboard/gui/ContentDrawerView;->f:Z

    .line 474
    const v1, 0x7f0a0101

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 475
    const v1, 0x7f0a00ba

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 477
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity$4;->b:Lflipboard/objs/ContentDrawerListItem;

    invoke-static {v0}, Lflipboard/service/Section;->b(Lflipboard/objs/ContentDrawerListItem;)Ljava/lang/String;

    move-result-object v0

    .line 478
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity$4;->c:Lflipboard/activities/ContentDrawerActivity;

    if-eqz v0, :cond_3

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->g(Ljava/lang/String;)Z

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    const v4, 0x7f0d0322

    invoke-virtual {v1, v4}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    const v4, 0x7f0d0321

    invoke-virtual {v1, v4}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v6, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    const v2, 0x7f0d031f

    invoke-virtual {v3, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    const v2, 0x7f0d0320

    invoke-virtual {v3, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    new-instance v2, Lflipboard/activities/ContentDrawerActivity$5;

    invoke-direct {v2, v1, v0}, Lflipboard/activities/ContentDrawerActivity$5;-><init>(Lflipboard/activities/ContentDrawerActivity;Ljava/lang/String;)V

    iput-object v2, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    iget-object v0, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "remove_account"

    invoke-virtual {v3, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_2
    sget-object v0, Lflipboard/activities/ContentDrawerActivity;->o:Lflipboard/util/Log;

    .line 479
    :cond_3
    return-void

    .line 465
    :cond_4
    iget-object v1, v0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v1, v0, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    invoke-interface {v1}, Lflipboard/service/User$MagazineChangeListener;->a()V

    :cond_5
    sget-object v1, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
