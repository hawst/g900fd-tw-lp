.class public Lflipboard/activities/SectionTabletActivity;
.super Lflipboard/activities/FeedActivity;
.source "SectionTabletActivity.java"


# static fields
.field public static n:I


# instance fields
.field private A:Lcom/amazon/motiongestures/GestureListener;

.field public o:Lflipboard/gui/section/SectionFragment;

.field private p:Lflipboard/gui/section/SectionFragment;

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Lflipboard/gui/personal/FLDrawerLayout;

.field private w:I

.field private x:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcom/amazon/motiongestures/GestureManager;

.field private z:Lcom/amazon/motiongestures/GestureListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const v0, 0xad72

    sput v0, Lflipboard/activities/SectionTabletActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lflipboard/activities/FeedActivity;-><init>()V

    .line 51
    const-string v0, "state_section_id"

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->r:Ljava/lang/String;

    .line 52
    const-string v0, "state_fragment_state"

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->s:Ljava/lang/String;

    .line 53
    const-string v0, "state_time_saved"

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->t:Ljava/lang/String;

    .line 54
    const-string v0, "state_section_last_changed"

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->u:Ljava/lang/String;

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/activities/SectionTabletActivity;->w:I

    .line 61
    new-instance v0, Lflipboard/activities/SectionTabletActivity$1;

    invoke-direct {v0, p0}, Lflipboard/activities/SectionTabletActivity$1;-><init>(Lflipboard/activities/SectionTabletActivity;)V

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->z:Lcom/amazon/motiongestures/GestureListener;

    .line 71
    new-instance v0, Lflipboard/activities/SectionTabletActivity$2;

    invoke-direct {v0, p0}, Lflipboard/activities/SectionTabletActivity$2;-><init>(Lflipboard/activities/SectionTabletActivity;)V

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->A:Lcom/amazon/motiongestures/GestureListener;

    .line 691
    return-void
.end method


# virtual methods
.method public final A()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->o()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 419
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 420
    const-string v1, "Section debug options"

    iput-object v1, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 421
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Show possible layouts"

    aput-object v3, v1, v2

    iput-object v1, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 422
    new-instance v1, Lflipboard/activities/SectionTabletActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/SectionTabletActivity$4;-><init>(Lflipboard/activities/SectionTabletActivity;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 429
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "section_debug"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 430
    return v4
.end method

.method protected final F()V
    .locals 2

    .prologue
    .line 504
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->F()V

    .line 505
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->l()V

    .line 508
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 3

    .prologue
    .line 307
    invoke-super {p0, p1, p2}, Lflipboard/activities/FeedActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 308
    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    const v2, 0x7f0d0177

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/section/SectionFragment;->a(Ljava/lang/String;I)V

    .line 311
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 226
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 229
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment;->D:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 230
    :goto_0
    new-instance v3, Lflipboard/gui/section/SectionFragment;

    invoke-direct {v3}, Lflipboard/gui/section/SectionFragment;-><init>()V

    .line 231
    iget-object v4, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    iput-object v4, v3, Lflipboard/gui/section/SectionFragment;->F:Landroid/support/v4/widget/DrawerLayout;

    .line 232
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 233
    const-string v5, "sid"

    invoke-virtual {v4, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 235
    invoke-static {p2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 236
    iget-object v6, p0, Lflipboard/activities/SectionTabletActivity;->B:Lflipboard/util/Log;

    const-string v7, "\"from\" source not provided for section view event with identifier %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-virtual {v6, v7, v1}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    :goto_1
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 241
    invoke-virtual {v3, v4}, Lflipboard/gui/section/SectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 242
    const-string v1, "extra_is_top_level"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 243
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    invoke-static {v0}, Lflipboard/gui/personal/FLDrawerLayout;->b(Landroid/support/v4/widget/DrawerLayout;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 246
    iput-object v3, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    .line 247
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    const-string v3, "section"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 252
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 229
    goto :goto_0

    .line 238
    :cond_2
    const-string v1, "source"

    invoke-virtual {v5, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 250
    :cond_3
    iput-object v3, p0, Lflipboard/activities/SectionTabletActivity;->p:Lflipboard/gui/section/SectionFragment;

    goto :goto_2
.end method

.method public final b(Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    .line 299
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->b(Lflipboard/objs/FeedItem;)V

    .line 300
    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    const v2, 0x7f0d0176

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/section/SectionFragment;->a(Ljava/lang/String;I)V

    .line 303
    :cond_0
    return-void
.end method

.method public final c(Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 315
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->c(Lflipboard/objs/FeedItem;)V

    .line 316
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->i()V

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    check-cast v0, Lflipboard/gui/section/SectionTabletView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionTabletView;->g()V

    .line 317
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 196
    iget-boolean v2, p0, Lflipboard/activities/SectionTabletActivity;->q:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x18

    if-eq v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x19

    if-ne v2, v3, :cond_5

    :cond_1
    move v2, v1

    :goto_0
    if-nez v2, :cond_3

    :cond_2
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    return v0

    :pswitch_0
    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->j()V

    move v2, v1

    goto :goto_0

    :pswitch_1
    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->i()V

    move v2, v1

    goto :goto_0

    :cond_5
    move v2, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    const-string v1, "close"

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->a(Ljava/lang/String;)V

    .line 185
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 186
    return-void
.end method

.method public final g()Lflipboard/gui/flipping/FlippingBitmap;
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_1

    .line 437
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->D()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(I)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v0

    .line 439
    :goto_0
    return-object v0

    .line 437
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 439
    :cond_1
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->g()Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 461
    const-string v0, "section"

    return-object v0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->u:Lflipboard/service/audio/FLAudioManager;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->v:Lflipboard/util/Observer;

    if-nez v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->u:Lflipboard/service/audio/FLAudioManager;

    new-instance v2, Lflipboard/gui/section/SectionFragment$29;

    invoke-direct {v2, v0}, Lflipboard/gui/section/SectionFragment$29;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v2, v0, Lflipboard/gui/section/SectionFragment;->v:Lflipboard/util/Observer;

    invoke-virtual {v1, v2}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/util/Observer;)V

    .line 348
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->j()V

    .line 350
    :cond_1
    return-void
.end method

.method public final l()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->d()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_0

    .line 451
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 381
    const/16 v0, 0x4e42

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1e43

    if-ne p1, v0, :cond_2

    .line 382
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1, p2, p3}, Lflipboard/gui/section/SectionFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 388
    :cond_1
    :goto_0
    return-void

    .line 386
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FeedActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAudioControlClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 294
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/AudioPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/SectionTabletActivity;->startActivity(Landroid/content/Intent;)V

    .line 295
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    const-string v0, "auth/flipboard/coverstories"

    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->f:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SectionTabletActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->b()Z

    .line 564
    :goto_0
    return-void

    .line 562
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBackToTopClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 211
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onBackToTopClicked(Landroid/view/View;)V

    .line 213
    :cond_0
    return-void
.end method

.method public onBrickClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 527
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v1

    .line 529
    const/4 v0, 0x0

    .line 530
    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    if-eqz v2, :cond_0

    .line 531
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lflipboard/gui/personal/FLDrawerLayout;->b(I)Z

    move-result v0

    .line 533
    :cond_0
    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->e:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v0

    .line 534
    :goto_0
    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "synthetic/fullContentGuide"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 535
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ContentDrawerPhoneActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/SectionTabletActivity;->startActivity(Landroid/content/Intent;)V

    .line 540
    :goto_1
    return-void

    .line 533
    :cond_1
    const-string v0, "sectionItem"

    goto :goto_0

    .line 537
    :cond_2
    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    :cond_3
    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lflipboard/activities/SectionTabletActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onCommentClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 398
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 399
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 400
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "post"

    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 404
    :cond_0
    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    sget-object v2, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, v1, p0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    .line 407
    :cond_1
    return-void
.end method

.method public onComposeClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 285
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ComposeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 286
    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    if-eqz v1, :cond_0

    .line 287
    const-string v1, "extra_section_id"

    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    :cond_0
    invoke-virtual {p0, v0}, Lflipboard/activities/SectionTabletActivity;->startActivity(Landroid/content/Intent;)V

    .line 290
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/high16 v3, 0x1000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/SectionTabletActivity;->S:Z

    if-eqz v0, :cond_2

    .line 85
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 88
    :cond_2
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v0, :cond_3

    .line 95
    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 98
    :cond_3
    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "sid"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    const v3, 0x7f030074

    invoke-virtual {p0, v3}, Lflipboard/activities/SectionTabletActivity;->setContentView(I)V

    .line 100
    if-nez p1, :cond_8

    .line 101
    new-instance v3, Lflipboard/gui/section/SectionFragment;

    invoke-direct {v3}, Lflipboard/gui/section/SectionFragment;-><init>()V

    iput-object v3, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    .line 102
    iget-object v3, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflipboard/gui/section/SectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 103
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->ai:Landroid/os/Bundle;

    .line 104
    if-eqz v3, :cond_4

    .line 105
    iget-object v4, p0, Lflipboard/activities/SectionTabletActivity;->r:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 106
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 108
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 109
    iget-object v4, p0, Lflipboard/activities/SectionTabletActivity;->t:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    iget v0, v0, Lflipboard/model/ConfigSetting;->SavedSectionStateValidTime:I

    int-to-long v6, v0

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    move v0, v1

    .line 111
    :goto_1
    iget-object v4, p0, Lflipboard/activities/SectionTabletActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 112
    if-nez v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    iget-wide v6, v0, Lflipboard/service/Section;->j:J

    cmp-long v0, v6, v4

    if-nez v0, :cond_7

    .line 113
    :goto_2
    if-eqz v1, :cond_4

    .line 114
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->s:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iput-object v0, v1, Lflipboard/gui/section/SectionFragment;->B:Landroid/os/Bundle;

    .line 119
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    const-string v3, "section"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 124
    :goto_3
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    if-nez v0, :cond_5

    .line 125
    const v0, 0x7f0a01a6

    invoke-virtual {p0, v0}, Lflipboard/activities/SectionTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/personal/FLDrawerLayout;

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    .line 127
    :cond_5
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->v:Lflipboard/gui/personal/FLDrawerLayout;

    iput-object v1, v0, Lflipboard/gui/section/SectionFragment;->F:Landroid/support/v4/widget/DrawerLayout;

    .line 129
    invoke-static {p0}, Lflipboard/service/FlipboardManager;->a(Landroid/app/Activity;)Lcom/amazon/motiongestures/GestureManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    .line 131
    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "search_requested"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->q()V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 110
    goto :goto_1

    :cond_7
    move v1, v2

    .line 112
    goto :goto_2

    .line 121
    :cond_8
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "section"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionFragment;

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 470
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 471
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment;->A:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    :goto_0
    if-lez v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment;->p:Z

    if-nez v0, :cond_3

    .line 473
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->p()Z

    move-result v0

    if-nez v0, :cond_2

    .line 474
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iput-object v0, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    .line 475
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 476
    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->t:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 479
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->u:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    iget-wide v2, v2, Lflipboard/service/Section;->j:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 480
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 481
    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/SectionFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 482
    iget-object v2, p0, Lflipboard/activities/SectionTabletActivity;->s:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 483
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->ai:Landroid/os/Bundle;

    .line 490
    :goto_1
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->x:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 491
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->x:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 492
    iput-object v4, p0, Lflipboard/activities/SectionTabletActivity;->x:Lflipboard/util/Observer;

    .line 494
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onDestroy()V

    .line 495
    return-void

    .line 471
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 485
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v4, v0, Lflipboard/service/FlipboardManager;->ai:Landroid/os/Bundle;

    goto :goto_1

    .line 488
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v4, v0, Lflipboard/service/FlipboardManager;->ai:Landroid/os/Bundle;

    goto :goto_1
.end method

.method public onFlipItClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 271
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 273
    const/4 v0, 0x0

    .line 274
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 275
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 280
    :cond_0
    :goto_0
    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    sget-object v2, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {p0, v1, v0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V

    .line 281
    return-void

    .line 276
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 277
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    goto :goto_0
.end method

.method public onLeftScrubberLabelClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onLeftScrubberLabelClicked(Landroid/view/View;)V

    .line 362
    :cond_0
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 12

    .prologue
    .line 217
    iget-object v8, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    check-cast v0, Lflipboard/gui/section/SectionTabletView;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a03a9

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lflipboard/gui/section/SectionTabletView;->g()V

    iget-object v1, v8, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/gui/section/SectionTabletView;->getCurrentViewIndex()I

    move-result v0

    iget-object v1, v8, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v2, "SectionFragment:onMenuItemSelected:R.id.section_to_top"

    new-instance v3, Lflipboard/gui/section/SectionFragment$23;

    invoke-direct {v3, v8, v0}, Lflipboard/gui/section/SectionFragment$23;-><init>(Lflipboard/gui/section/SectionFragment;I)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 218
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lflipboard/activities/FeedActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    .line 221
    const/4 v0, 0x0

    return v0

    .line 217
    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03aa

    if-ne v0, v1, :cond_2

    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "nanoSearchButtonTapped"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->q()V

    goto :goto_0

    :cond_2
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03a8

    if-ne v0, v1, :cond_3

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawer(Landroid/app/Activity;)V

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03af

    if-ne v0, v1, :cond_4

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/ComposeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "extra_section_id"

    iget-object v2, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v8, v0}, Lflipboard/gui/section/SectionFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03b1

    if-ne v0, v1, :cond_6

    iget v0, v8, Lflipboard/gui/section/SectionFragment;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v8, Lflipboard/gui/section/SectionFragment;->e:I

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;Ljava/lang/String;)V

    :cond_5
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->t:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    goto/16 :goto_0

    :cond_6
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03b5

    if-ne v0, v1, :cond_7

    new-instance v1, Lflipboard/service/SyncJob;

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v2, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lflipboard/service/SyncJob;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/util/List;)V

    invoke-virtual {v1}, Lflipboard/service/SyncJob;->a()V

    goto/16 :goto_0

    :cond_7
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03b2

    if-eq v0, v1, :cond_8

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03b3

    if-ne v0, v1, :cond_b

    :cond_8
    new-instance v9, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v9}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    const v0, 0x7f0d01e4

    invoke-virtual {v9, v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    new-instance v0, Lflipboard/gui/section/SectionFragment$24;

    invoke-direct {v0, v8}, Lflipboard/gui/section/SectionFragment$24;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v0, v9, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "loading"

    invoke-virtual {v9, v0, v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03b3

    if-ne v0, v1, :cond_a

    const/4 v6, 0x1

    :goto_1
    if-eqz v6, :cond_9

    const-string v0, "inviteContributorButtonTapped"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->c(Ljava/lang/String;)V

    :cond_9
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v3}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    new-instance v7, Lflipboard/gui/section/SectionFragment$25;

    invoke-direct {v7, v8, v9, v6}, Lflipboard/gui/section/SectionFragment$25;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/dialog/FLProgressDialogFragment;Z)V

    invoke-virtual/range {v0 .. v7}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLflipboard/service/Flap$JSONResultObserver;)V

    goto/16 :goto_0

    :cond_a
    const/4 v6, 0x0

    goto :goto_1

    :cond_b
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03ac

    if-ne v0, v1, :cond_c

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->showContributors()V

    goto/16 :goto_0

    :cond_c
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03ab

    if-ne v0, v1, :cond_d

    iget-object v1, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-static {v1, v0}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    goto/16 :goto_0

    :cond_d
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03ad

    if-ne v0, v1, :cond_e

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v1, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {v0, v1}, Lflipboard/util/ShareHelper;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    goto/16 :goto_0

    :cond_e
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03ae

    if-ne v0, v1, :cond_f

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    new-instance v2, Lflipboard/gui/section/SectionFragment$26;

    invoke-direct {v2, v8}, Lflipboard/gui/section/SectionFragment$26;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-static {v0, v1, v2}, Lflipboard/util/ShareHelper;->b(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)V

    goto/16 :goto_0

    :cond_f
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x85ba

    if-ne v0, v1, :cond_10

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/SettingsDensityActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "sid"

    iget-object v2, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x4e43

    invoke-virtual {v8, v0, v1}, Lflipboard/gui/section/SectionFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_10
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x85bb

    if-ne v0, v1, :cond_0

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    if-nez v0, :cond_11

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v8, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    :cond_11
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "E HH:mm:ss"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_2
    const/16 v3, 0xa

    if-ge v0, v3, :cond_12

    const-string v3, "%s item %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v5, 0x1

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lflipboard/objs/FeedItem;

    invoke-direct {v4}, Lflipboard/objs/FeedItem;-><init>()V

    iput-object v3, v4, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iput-object v3, v4, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v10, 0x3e8

    div-long/2addr v6, v10

    iput-wide v6, v4, Lflipboard/objs/FeedItem;->Z:J

    const-string v3, "post"

    iput-object v3, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "fake-synthetic"

    iput-object v3, v4, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iget-object v3, v8, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_12
    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, v8, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget v0, v8, Lflipboard/gui/section/SectionFragment;->q:I

    if-nez v0, :cond_14

    invoke-virtual {v8}, Lflipboard/gui/section/SectionFragment;->m()V

    const/4 v0, 0x1

    iput-boolean v0, v8, Lflipboard/gui/section/SectionFragment;->x:Z

    const/4 v0, 0x1

    iput-boolean v0, v8, Lflipboard/gui/section/SectionFragment;->y:Z

    :cond_13
    :goto_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x7d0

    new-instance v4, Lflipboard/gui/section/SectionFragment$27;

    invoke-direct {v4, v8, v1}, Lflipboard/gui/section/SectionFragment$27;-><init>(Lflipboard/gui/section/SectionFragment;Ljava/util/List;)V

    invoke-virtual {v0, v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {v8}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_13

    const-string v2, "Refreshing"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lflipboard/gui/FLToast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3

    :cond_15
    invoke-virtual {v8}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    const-string v1, "Section is already fetching"

    const v2, 0x7f0201cf

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onMyFlipboardItemClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 517
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 518
    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "contentGuide"

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SectionTabletActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    return-void
.end method

.method public onPageboxClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 264
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 265
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onPageboxClick(Landroid/view/View;)V

    .line 267
    :cond_0
    return-void
.end method

.method public onPageboxMoreClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->onSubsectionClicked(Landroid/view/View;)V

    .line 207
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onPause()V

    .line 150
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->z:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->c()V

    .line 152
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->A:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->c()V

    .line 154
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onResume()V

    .line 139
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->N:Landroid/content/SharedPreferences;

    const-string v1, "volume_button_flip"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/SectionTabletActivity;->q:Z

    .line 140
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    if-eqz v0, :cond_0

    .line 141
    invoke-static {}, Lcom/amazon/motiongestures/Gesture;->a()Lcom/amazon/motiongestures/Gesture;

    .line 142
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->z:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->b()V

    .line 143
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->y:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->A:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->b()V

    .line 145
    :cond_0
    return-void
.end method

.method public onRightScrubberLabelClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onRightScrubberLabelClicked(Landroid/view/View;)V

    .line 374
    :cond_0
    return-void
.end method

.method public onSectionButton(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 336
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 337
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 338
    const-string v2, "source"

    const-string v3, "item-album"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v2, "originSectionIdentifier"

    iget-object v3, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-static {v0, p0, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Landroid/os/Bundle;)V

    .line 342
    return-void
.end method

.method public onShareClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 411
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 413
    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 415
    :cond_0
    return-void
.end method

.method public onShareItem(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 328
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 329
    if-eqz v0, :cond_0

    .line 330
    iget-object v1, p0, Lflipboard/activities/SectionTabletActivity;->C:Lflipboard/service/Section;

    sget-object v2, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {p0, v1, v0, v2}, Lflipboard/util/SocialHelper;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V

    .line 332
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 159
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onStart()V

    .line 160
    invoke-virtual {p0}, Lflipboard/activities/SectionTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "should_finish_other_section_activites"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x12c

    new-instance v1, Lflipboard/activities/SectionTabletActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/SectionTabletActivity$3;-><init>(Lflipboard/activities/SectionTabletActivity;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 178
    :cond_0
    return-void
.end method

.method public onSubsectionClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onSubsectionClicked(Landroid/view/View;)V

    .line 202
    return-void
.end method

.method protected final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 259
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 321
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->r()V

    .line 322
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->i()V

    .line 323
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    const v1, 0x7f0a016c

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 324
    :cond_1
    return-void
.end method

.method public showContributors(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->showContributors()V

    .line 457
    return-void
.end method

.method protected final w()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recreate"

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SectionTabletActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void
.end method
