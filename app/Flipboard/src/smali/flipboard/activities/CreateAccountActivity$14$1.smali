.class Lflipboard/activities/CreateAccountActivity$14$1;
.super Ljava/lang/Object;
.source "CreateAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lflipboard/activities/CreateAccountActivity$14;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateAccountActivity$14;Lflipboard/service/FlipboardManager$CreateAccountMessage;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 775
    iput-object p1, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iput-object p2, p0, Lflipboard/activities/CreateAccountActivity$14$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    iput-object p3, p0, Lflipboard/activities/CreateAccountActivity$14$1;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 779
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    sget-object v1, Lflipboard/service/FlipboardManager$CreateAccountMessage;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    if-ne v0, v1, :cond_0

    .line 780
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "extra_invite"

    iget-object v4, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v4, v4, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    iget-object v4, v4, Lflipboard/activities/CreateAccountActivity;->D:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/CreateAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 781
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$14;->a:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->b(Ljava/lang/String;)Lflipboard/activities/LoginActivity$SignInMethod;

    move-result-object v0

    .line 782
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v1}, Lflipboard/activities/CreateAccountActivity;->d(Lflipboard/activities/CreateAccountActivity;)Lflipboard/objs/UsageEventV2$EventCategory;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v2}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v2

    invoke-static {v5, v0, v1, v2}, Lflipboard/activities/CreateAccountActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 783
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0, v5}, Lflipboard/activities/CreateAccountActivity;->a(Lflipboard/activities/CreateAccountActivity;Z)Z

    .line 784
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/CreateAccountActivity;->finish()V

    .line 793
    :goto_0
    return-void

    .line 786
    :cond_0
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 787
    if-nez v0, :cond_1

    .line 788
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    const v1, 0x7f0d0104

    invoke-virtual {v0, v1}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 790
    :cond_1
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    const v2, 0x7f0d00be

    invoke-virtual {v1, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 791
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$14$1;->c:Lflipboard/activities/CreateAccountActivity$14;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$14;->b:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v2, v1, v0, v5}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method
