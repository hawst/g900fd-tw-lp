.class public Lflipboard/activities/CreateAccountActivity;
.super Lflipboard/activities/ChooseAvatarActivity;
.source "CreateAccountActivity.java"

# interfaces
.implements Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;


# static fields
.field private static G:I

.field public static o:Z


# instance fields
.field A:Z

.field B:Z

.field C:Lflipboard/gui/dialog/FLProgressDialogFragment;

.field D:Ljava/lang/String;

.field E:Landroid/view/View;

.field private F:Lflipboard/util/Log;

.field private ac:Lflipboard/util/GooglePlusSignInClient;

.field private ad:Z

.field private ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

.field private af:Lflipboard/activities/FacebookAuthenticateFragment;

.field private ag:Z

.field private ah:Z

.field private ai:Lflipboard/objs/UsageEventV2$EventCategory;

.field p:Landroid/widget/Button;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/EditText;

.field s:Landroid/widget/EditText;

.field t:Landroid/widget/EditText;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/widget/TextView;

.field x:Landroid/widget/TextView;

.field y:Landroid/widget/Button;

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x6

    sput v0, Lflipboard/activities/CreateAccountActivity;->G:I

    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/activities/CreateAccountActivity;->o:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lflipboard/activities/ChooseAvatarActivity;-><init>()V

    .line 65
    const-string v0, "create account"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->F:Lflipboard/util/Log;

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->ad:Z

    .line 111
    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ai:Lflipboard/objs/UsageEventV2$EventCategory;

    return-void
.end method

.method private I()Z
    .locals 3

    .prologue
    .line 555
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 556
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 557
    if-lez v1, :cond_0

    .line 558
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 559
    add-int/lit8 v1, v1, 0x1

    if-le v2, v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ge v2, v0, :cond_0

    .line 560
    const/4 v0, 0x1

    .line 563
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static J()Ljava/lang/String;
    .locals 4

    .prologue
    .line 924
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 925
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 926
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x7

    if-ge v0, v3, :cond_0

    .line 927
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 926
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 929
    :cond_0
    return-object v1
.end method

.method static synthetic a(Lflipboard/activities/CreateAccountActivity;Lflipboard/util/GooglePlusSignInClient;)Lflipboard/util/GooglePlusSignInClient;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lflipboard/activities/CreateAccountActivity;->ac:Lflipboard/util/GooglePlusSignInClient;

    return-object p1
.end method

.method public static a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V
    .locals 3

    .prologue
    .line 976
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->K:Lflipboard/objs/UsageEventV2$EventAction;

    invoke-direct {v0, v1, p2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 977
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 978
    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne p3, v1, :cond_0

    .line 979
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 981
    :cond_0
    if-eqz p1, :cond_1

    .line 982
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 984
    :cond_1
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 985
    return-void
.end method

.method static synthetic a(Lflipboard/activities/CreateAccountActivity;)V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const v5, 0x7f0d0125

    const/high16 v4, -0x10000

    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 50
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->z:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->A:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    :goto_2
    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_3
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v0, v6, :cond_6

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    const v1, 0x7f0d0245

    invoke-virtual {p0, v1}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_4
    return-void

    :cond_0
    const/16 v2, 0x40

    if-le v0, v2, :cond_1

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    const v2, 0x7f0d0127

    invoke-virtual {p0, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    :cond_1
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_2
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_3
    invoke-direct {p0}, Lflipboard/activities/CreateAccountActivity;->I()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    const v2, 0x7f0d0124

    invoke-virtual {p0, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_5
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_7
    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->B:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    :cond_9
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v0, v6, :cond_a

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    const v1, 0x7f0d0245

    invoke-virtual {p0, v1}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    :cond_a
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_c
    move v0, v1

    goto/16 :goto_2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lflipboard/activities/CreateAccountActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$17;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/activities/CreateAccountActivity$17;-><init>(Lflipboard/activities/CreateAccountActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 821
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$15;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$15;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 833
    new-instance v5, Lflipboard/activities/CreateAccountActivity$16;

    invoke-direct {v5, p0, p1, p2}, Lflipboard/activities/CreateAccountActivity$16;-><init>(Lflipboard/activities/CreateAccountActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-virtual {v1}, Lflipboard/activities/LoginActivity$LoginInitFrom;->name()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)V

    .line 891
    return-void
.end method

.method static synthetic a(Lflipboard/activities/CreateAccountActivity;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lflipboard/activities/CreateAccountActivity;->ag:Z

    return p1
.end method

.method static b(Ljava/lang/String;)Lflipboard/activities/LoginActivity$SignInMethod;
    .locals 1

    .prologue
    .line 806
    const-string v0, "facebook"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->c:Lflipboard/activities/LoginActivity$SignInMethod;

    .line 809
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->b:Lflipboard/activities/LoginActivity$SignInMethod;

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/CreateAccountActivity;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lflipboard/activities/CreateAccountActivity;->r()V

    return-void
.end method

.method static synthetic c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/CreateAccountActivity;)Lflipboard/objs/UsageEventV2$EventCategory;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ai:Lflipboard/objs/UsageEventV2$EventCategory;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/CreateAccountActivity;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->F:Lflipboard/util/Log;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/CreateAccountActivity;)Lflipboard/util/GooglePlusSignInClient;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ac:Lflipboard/util/GooglePlusSignInClient;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/FacebookAuthenticateFragment;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    return-object v0
.end method

.method private r()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 513
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sget v3, Lflipboard/activities/CreateAccountActivity;->G:I

    if-lt v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lflipboard/activities/CreateAccountActivity;->I()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const/16 v3, 0x40

    if-ge v2, v3, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 514
    :goto_2
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 515
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 517
    if-eqz v0, :cond_2

    .line 518
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    const v1, 0x7f0201ab

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 519
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    const v1, 0x7f0e000d

    invoke-virtual {v0, p0, v1}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 520
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 522
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 530
    :goto_3
    return-void

    :cond_0
    move v2, v1

    .line 513
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    .line 524
    :cond_2
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    const v1, 0x7f0201a8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 525
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    const v1, 0x7f0e000c

    invoke-virtual {v0, p0, v1}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 526
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 528
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_3

    :cond_3
    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 895
    const-string v0, "googleplus"

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lflipboard/activities/CreateAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 909
    .line 910
    const v0, 0x7f0d00be

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 911
    if-eqz p0, :cond_0

    iget-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 912
    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 914
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->ag:Z

    .line 915
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 919
    const-string v0, "connecting_google"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 920
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v5, 0x2328

    const/16 v4, 0x1e39

    const/4 v3, 0x1

    const/4 v1, -0x1

    .line 691
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/ChooseAvatarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 692
    const/16 v0, 0x1e3a

    if-ne p1, v0, :cond_4

    .line 693
    if-ne p2, v1, :cond_3

    .line 694
    const-string v1, "facebook"

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->finish()V

    .line 739
    :cond_0
    :goto_0
    return-void

    .line 694
    :cond_1
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    new-instance v5, Lflipboard/activities/CreateAccountActivity$14;

    invoke-direct {v5, p0, v1}, Lflipboard/activities/CreateAccountActivity$14;-><init>(Lflipboard/activities/CreateAccountActivity;Ljava/lang/String;)V

    const-string v3, "facebook"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-virtual {v2}, Lflipboard/activities/LoginActivity$LoginInitFrom;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Flap$Request;

    goto :goto_0

    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-virtual {v3}, Lflipboard/activities/LoginActivity$LoginInitFrom;->name()Ljava/lang/String;

    move-result-object v4

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)V

    goto :goto_0

    .line 696
    :cond_3
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ah:Z

    goto :goto_0

    .line 698
    :cond_4
    if-ne p1, v4, :cond_5

    if-ne p2, v1, :cond_5

    .line 699
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ac:Lflipboard/util/GooglePlusSignInClient;

    if-eqz v0, :cond_0

    .line 700
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "CreateAccountActivity:onActivityResult"

    new-instance v2, Lflipboard/activities/CreateAccountActivity$13;

    invoke-direct {v2, p0}, Lflipboard/activities/CreateAccountActivity$13;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 707
    :cond_5
    if-ne p1, v5, :cond_6

    if-ne p2, v1, :cond_6

    .line 708
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ac:Lflipboard/util/GooglePlusSignInClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ac:Lflipboard/util/GooglePlusSignInClient;

    invoke-virtual {v0}, Lflipboard/util/GooglePlusSignInClient;->d()V

    goto :goto_0

    .line 709
    :cond_6
    if-ne p1, v5, :cond_7

    if-nez p2, :cond_7

    .line 710
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ad:Z

    goto :goto_0

    .line 711
    :cond_7
    if-ne p1, v4, :cond_8

    if-nez p2, :cond_8

    .line 712
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ad:Z

    goto :goto_0

    .line 713
    :cond_8
    const/16 v0, 0x64

    if-ne p1, v0, :cond_9

    .line 714
    if-ne p2, v1, :cond_0

    .line 715
    if-eqz p3, :cond_0

    const-string v0, "is_agree_to_disclaimer"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->F:Lflipboard/util/Log;

    .line 718
    invoke-static {p0}, Lflipboard/service/SamsungHelper;->a(Landroid/app/Activity;)V

    goto :goto_0

    .line 721
    :cond_9
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 722
    if-ne p2, v1, :cond_0

    .line 723
    const-string v0, "access_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 725
    const-string v0, "api_server_url"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 726
    if-eqz v0, :cond_a

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 728
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 730
    :cond_a
    const-string v2, "auth_server_url"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 732
    const-string v2, "login_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 733
    const-string v2, "login_id_type"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 735
    const-string v2, "samsung"

    invoke-direct {p0, v2, v1, v0}, Lflipboard/activities/CreateAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/16 v9, 0x8

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 115
    invoke-super {p0, p1}, Lflipboard/activities/ChooseAvatarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    const v0, 0x7f03005a

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->setContentView(I)V

    .line 120
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/app/Activity;)V

    .line 122
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 123
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 125
    sget-object v0, Lflipboard/activities/LoginActivity$LoginInitFrom;->a:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    .line 126
    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 127
    if-eqz v1, :cond_2

    .line 128
    const-string v0, "extra_invite"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->D:Ljava/lang/String;

    .line 129
    const-string v0, "in_first_launch"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 130
    if-eqz v0, :cond_f

    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    :goto_1
    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ai:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 132
    const-string v0, "extra_initialized_from_briefing"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 133
    sget-object v0, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    .line 139
    :cond_2
    :goto_2
    const-string v0, "extra_flipboard_signup_screen_title"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_3

    .line 141
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :cond_3
    const-string v0, "extra_flipboard_signup_screen_button_text"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_4

    .line 145
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->y:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 148
    :cond_4
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_6

    .line 150
    :cond_5
    const v0, 0x7f0a0150

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :cond_6
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_7

    .line 154
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->E:Landroid/view/View;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$1;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    sget-boolean v0, Lflipboard/service/SamsungHelper;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "com.osp.app.signin"

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 166
    const v0, 0x7f0a0151

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 167
    new-instance v1, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0d030f

    invoke-virtual {p0, v3}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 168
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f020154

    invoke-direct {v2, v3, v4, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    invoke-interface {v1, v2, v6, v7, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 169
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 171
    new-instance v1, Lflipboard/activities/CreateAccountActivity$2;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$2;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    :cond_7
    const v0, 0x7f0a00f1

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    .line 181
    const v0, 0x7f0a014d

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    .line 182
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 183
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 184
    const/4 v1, 0x3

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 185
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 190
    :cond_8
    const v0, 0x7f0a0157

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 191
    const v1, 0x7f0a0158

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    .line 192
    const v0, 0x7f0a015a

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 193
    const v1, 0x7f0a015b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    .line 194
    const v0, 0x7f0a0154

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 195
    const v1, 0x7f0a0155

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    .line 197
    const v0, 0x7f0a0156

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    .line 198
    const v0, 0x7f0a0159

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    .line 199
    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    .line 201
    const v0, 0x7f0a014e

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 203
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_11

    .line 204
    const v1, 0x7f0d013e

    invoke-virtual {p0, v1}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    const v1, 0x7f0d0115

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 212
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$3;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 221
    invoke-static {}, Lflipboard/util/AndroidUtil;->l()Ljava/lang/String;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_9

    .line 223
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 225
    :cond_9
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$4;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 236
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    const v1, 0x7f0d011f

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 237
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$5;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$5;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 246
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$6;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$6;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 257
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    const v1, 0x7f0d0116

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 258
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$7;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$7;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 266
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$8;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$8;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 276
    new-instance v0, Lflipboard/activities/CreateAccountActivity$9;

    invoke-direct {v0, p0}, Lflipboard/activities/CreateAccountActivity$9;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    .line 384
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->p:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->q:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    const v0, 0x7f0a014f

    invoke-virtual {p0, v0}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    .line 388
    invoke-virtual {v0, v7}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    .line 391
    const v1, 0x7f0d0168

    invoke-virtual {p0, v1}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 393
    const v2, 0x7f0d030c

    invoke-virtual {p0, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 394
    new-instance v2, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 395
    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f020153

    invoke-direct {v1, v3, v4, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    invoke-interface {v2, v1, v6, v7, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 396
    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 402
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v1, :cond_a

    .line 403
    invoke-virtual {v0, v9}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 406
    :cond_a
    const v1, 0x7f0a0150

    invoke-virtual {p0, v1}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLButton;

    .line 407
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 409
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_12

    .line 410
    :cond_b
    const-string v2, "Facebook"

    .line 416
    :goto_4
    const v3, 0x7f0d030c

    invoke-virtual {p0, v3}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 417
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "   "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 418
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f020152

    invoke-direct {v2, v4, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    invoke-interface {v3, v2, v6, v7, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 419
    invoke-virtual {v1, v3}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 421
    invoke-virtual {v1}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    if-ne v2, v9, :cond_c

    invoke-virtual {v0}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    if-ne v2, v9, :cond_c

    .line 422
    const v2, 0x7f0a0152

    invoke-virtual {p0, v2}, Lflipboard/activities/CreateAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 425
    :cond_c
    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(Landroid/content/Context;)I

    move-result v2

    .line 426
    if-nez v2, :cond_13

    .line 429
    new-instance v2, Lflipboard/activities/CreateAccountActivity$10;

    invoke-direct {v2, p0}, Lflipboard/activities/CreateAccountActivity$10;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447
    :goto_5
    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 450
    invoke-direct {p0}, Lflipboard/activities/CreateAccountActivity;->r()V

    .line 452
    if-nez p1, :cond_14

    .line 453
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 454
    const-string v2, "fragmentAction"

    sget-object v3, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->a:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 455
    const-string v2, "invteString"

    iget-object v3, p0, Lflipboard/activities/CreateAccountActivity;->D:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    new-instance v2, Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-direct {v2}, Lflipboard/activities/FacebookAuthenticateFragment;-><init>()V

    iput-object v2, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    .line 457
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v2, v0}, Lflipboard/activities/FacebookAuthenticateFragment;->setArguments(Landroid/os/Bundle;)V

    .line 458
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->ai:Lflipboard/objs/UsageEventV2$EventCategory;

    iput-object v2, v0, Lflipboard/activities/FacebookAuthenticateFragment;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 459
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v2, v0, Lflipboard/activities/FacebookAuthenticateFragment;->d:Lflipboard/activities/LoginActivity$LoginInitFrom;

    .line 460
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x1020002

    iget-object v3, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 464
    :goto_6
    new-instance v0, Lflipboard/activities/CreateAccountActivity$11;

    invoke-direct {v0, p0}, Lflipboard/activities/CreateAccountActivity$11;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 480
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$12;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$12;-><init>(Lflipboard/activities/CreateAccountActivity;)V

    iput-object v1, v0, Lflipboard/activities/FacebookAuthenticateFragment;->c:Lflipboard/activities/FacebookAuthenticateFragment$CancelListener;

    .line 487
    sget-boolean v0, Lflipboard/activities/CreateAccountActivity;->o:Z

    if-eqz v0, :cond_d

    .line 488
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "null+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lflipboard/activities/CreateAccountActivity;->J()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@flipboard.com"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 489
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    const-string v1, "password"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 490
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    const-string v1, "Test Account"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 493
    :cond_d
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 494
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setGravity(I)V

    .line 495
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setGravity(I)V

    .line 496
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setGravity(I)V

    .line 498
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 499
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 500
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 504
    :cond_e
    invoke-static {}, Lflipboard/util/AndroidUtil;->m()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {p0}, Lflipboard/activities/CreateAccountActivity;->y()V

    goto/16 :goto_0

    .line 130
    :cond_f
    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    goto/16 :goto_1

    .line 135
    :cond_10
    sget-object v0, Lflipboard/activities/LoginActivity$LoginInitFrom;->a:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    goto/16 :goto_2

    .line 206
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0d013c

    invoke-virtual {p0, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0d013d

    invoke-virtual {p0, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 412
    :cond_12
    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 443
    :cond_13
    invoke-virtual {v0, v9}, Lflipboard/gui/FLButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 462
    :cond_14
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const v2, 0x1020002

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FacebookAuthenticateFragment;

    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->af:Lflipboard/activities/FacebookAuthenticateFragment;

    goto/16 :goto_6
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 682
    invoke-super {p0}, Lflipboard/activities/ChooseAvatarActivity;->onPause()V

    .line 683
    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->ag:Z

    if-nez v0, :cond_0

    .line 684
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->ai:Lflipboard/objs/UsageEventV2$EventCategory;

    iget-object v3, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-static {v0, v1, v2, v3}, Lflipboard/activities/CreateAccountActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 686
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 655
    invoke-super {p0}, Lflipboard/activities/ChooseAvatarActivity;->onResume()V

    .line 657
    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->ag:Z

    if-nez v0, :cond_1

    .line 658
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->L:Lflipboard/objs/UsageEventV2$EventAction;

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->ai:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 659
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    sget-object v2, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v1, v2, :cond_0

    .line 660
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity;->ae:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 662
    :cond_0
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 664
    :cond_1
    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->ah:Z

    if-eqz v0, :cond_2

    .line 665
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ag:Z

    .line 666
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ah:Z

    .line 673
    :cond_2
    iget-boolean v0, p0, Lflipboard/activities/CreateAccountActivity;->ad:Z

    if-eqz v0, :cond_3

    .line 674
    const-string v0, "connecting_google"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 675
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ad:Z

    .line 676
    iput-boolean v3, p0, Lflipboard/activities/CreateAccountActivity;->ag:Z

    .line 678
    :cond_3
    return-void
.end method
