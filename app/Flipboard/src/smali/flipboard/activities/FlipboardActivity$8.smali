.class Lflipboard/activities/FlipboardActivity$8;
.super Ljava/lang/Object;
.source "FlipboardActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:[Ljava/lang/CharSequence;

.field final synthetic b:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;[Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1058
    iput-object p1, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/activities/FlipboardActivity$8;->a:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 1061
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$8;->a:[Ljava/lang/CharSequence;

    aget-object v0, v0, p2

    .line 1062
    const-string v2, "Dump Views"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1063
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    const v2, 0x1020002

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1064
    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;)V

    .line 1173
    :cond_0
    :goto_0
    return-void

    .line 1065
    :cond_1
    const-string v2, "Fake New TOC Items"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1066
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v2, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v10, :cond_2

    iget-object v4, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v4, v2, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide v8, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    double-to-int v5, v6

    add-int/lit16 v5, v5, 0x3e8

    new-instance v6, Lflipboard/service/User$37;

    invoke-direct {v6, v2, v0}, Lflipboard/service/User$37;-><init>(Lflipboard/service/User;Lflipboard/service/Section;)V

    invoke-virtual {v4, v5, v6}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    goto :goto_1

    .line 1067
    :cond_3
    const-string v2, "Sync Down User State"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1068
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v5}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    goto :goto_0

    .line 1069
    :cond_4
    const-string v2, "Clear Watched Files"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1070
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v2, Lflipboard/service/FlipboardManager;->Q:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    sget-object v6, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v5, v6, v1

    new-instance v7, Ljava/io/File;

    iget-object v8, v2, Lflipboard/service/FlipboardManager;->Q:Ljava/io/File;

    invoke-direct {v7, v8, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v6, v9

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1071
    :cond_5
    const-string v2, "Dump SharedPrefs"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1072
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1073
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v3, v10, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v9

    goto :goto_3

    .line 1075
    :cond_6
    const-string v2, "Delete All Section Items"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1076
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->n()V

    goto/16 :goto_0

    .line 1077
    :cond_7
    const-string v2, "Refresh One Section"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1078
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->o()V

    goto/16 :goto_0

    .line 1079
    :cond_8
    const-string v2, "Debugger Breakpoint"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1080
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->G()V

    goto/16 :goto_0

    .line 1081
    :cond_9
    const-string v2, "Dump Bitmaps"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1082
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager;->c()V

    goto/16 :goto_0

    .line 1083
    :cond_a
    const-string v2, "Garbage Collect"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1084
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager;->b()V

    goto/16 :goto_0

    .line 1085
    :cond_b
    const-string v2, "Dump Downloads"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1086
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v0}, Lflipboard/io/DownloadManager;->d()V

    goto/16 :goto_0

    .line 1087
    :cond_c
    const-string v2, "Dump Network"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1088
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    const-string v1, "network info"

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1089
    :cond_d
    const-string v2, "Purge Bitmaps"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1090
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0, v1, v9}, Lflipboard/io/BitmapManager;->b(IZ)V

    invoke-virtual {v0, v1, v9}, Lflipboard/io/BitmapManager;->a(IZ)V

    goto/16 :goto_0

    .line 1091
    :cond_e
    const-string v1, "Purge Downloads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1092
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/io/DownloadManager;->a(J)V

    goto/16 :goto_0

    .line 1093
    :cond_f
    const-string v1, "Test Search"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1094
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v2, "cars"

    sget-object v3, Lflipboard/service/Flap$SearchType;->a:Lflipboard/service/Flap$SearchType;

    new-instance v4, Lflipboard/activities/FlipboardActivity$8$1;

    invoke-direct {v4, p0}, Lflipboard/activities/FlipboardActivity$8$1;-><init>(Lflipboard/activities/FlipboardActivity$8;)V

    const/4 v6, -0x1

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)Lflipboard/service/Flap$SearchRequest;

    goto/16 :goto_0

    .line 1115
    :cond_10
    const-string v1, "Dump Sections"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1116
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->v()V

    goto/16 :goto_0

    .line 1117
    :cond_11
    const-string v1, "Dump Usage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1118
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Lflipboard/io/UsageManager;->d()V

    goto/16 :goto_0

    .line 1119
    :cond_12
    const-string v1, "Purge Usage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1120
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Lflipboard/io/UsageManager;->c()V

    goto/16 :goto_0

    .line 1121
    :cond_13
    const-string v1, "Upload Usage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1122
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v0, v9}, Lflipboard/io/UsageManager;->a(Z)V

    goto/16 :goto_0

    .line 1123
    :cond_14
    const-string v1, "Dump memory to sd"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1124
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1125
    new-instance v1, Lflipboard/gui/dialog/FLProgressDialog;

    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dumping memory to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/dump.hprof"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1126
    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2, v1}, Lflipboard/activities/FlipboardActivity;->a(Landroid/app/Dialog;)V

    .line 1129
    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v2, v2, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v3, Lflipboard/activities/FlipboardActivity$8$2;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/activities/FlipboardActivity$8$2;-><init>(Lflipboard/activities/FlipboardActivity$8;Ljava/lang/String;Lflipboard/gui/dialog/FLProgressDialog;)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 1156
    :cond_15
    const-string v1, "Dump gl"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1157
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 1158
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$8;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v1, Lflipboard/activities/FlipboardActivity$8$3;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$8$3;-><init>(Lflipboard/activities/FlipboardActivity$8;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0
.end method
