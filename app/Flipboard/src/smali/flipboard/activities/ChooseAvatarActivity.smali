.class public abstract Lflipboard/activities/ChooseAvatarActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ChooseAvatarActivity.java"


# instance fields
.field protected n:[Ljava/lang/String;

.field private o:Lflipboard/util/Log;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 30
    const-string v0, "avatar chooser"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ChooseAvatarActivity;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    return-object v0
.end method

.method private a(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    .line 123
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 124
    const/4 v1, 0x0

    .line 125
    const/4 v2, 0x0

    .line 126
    const/16 v0, 0x2059

    if-ne p1, v0, :cond_2

    .line 127
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    .line 197
    :cond_0
    :goto_0
    if-nez v1, :cond_b

    .line 198
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    const-string v1, "No valid bitmap, can\'t update avatar"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    :cond_1
    :goto_1
    return-void

    .line 128
    :cond_2
    const/16 v0, 0x205a

    if-ne p1, v0, :cond_0

    .line 129
    if-eqz p3, :cond_1

    .line 134
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 136
    invoke-static {v0, p0}, Lflipboard/util/AndroidUtil;->a(Landroid/net/Uri;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v4

    .line 138
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    .line 139
    if-nez v4, :cond_3

    .line 140
    const v0, 0x7f0d033d

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_1

    .line 144
    :cond_3
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 145
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 146
    invoke-static {v4, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 147
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/lit16 v1, v1, 0x100

    .line 148
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/lit16 v0, v0, 0x100

    .line 149
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 150
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 151
    const/4 v3, 0x1

    if-le v0, v3, :cond_4

    .line 152
    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 154
    :cond_4
    invoke-static {v4, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 156
    if-nez v3, :cond_6

    const-string v0, "http:"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "https:"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 157
    :cond_5
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/activities/ChooseAvatarActivity;->n_()V

    new-instance v1, Lflipboard/activities/ChooseAvatarActivity$1;

    invoke-direct {v1, p0, p3, p1, p2}, Lflipboard/activities/ChooseAvatarActivity$1;-><init>(Lflipboard/activities/ChooseAvatarActivity;Landroid/content/Intent;II)V

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V

    goto :goto_1

    .line 161
    :cond_6
    if-nez v3, :cond_7

    .line 162
    const v0, 0x7f0d033d

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_1

    .line 166
    :cond_7
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    const/16 v1, 0x100

    if-gt v0, v1, :cond_8

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    const/16 v1, 0x100

    if-le v0, v1, :cond_a

    .line 169
    :cond_8
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_9

    .line 170
    const/16 v1, 0x100

    .line 171
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-double v6, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-double v8, v0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4070000000000000L    # 256.0

    mul-double/2addr v6, v8

    double-to-int v0, v6

    .line 176
    :goto_2
    iget-object v5, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 177
    const/4 v5, 0x1

    invoke-static {v3, v1, v0, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 180
    :goto_3
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, v4}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 185
    const-string v3, "Orientation"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    :goto_4
    move v2, v0

    .line 192
    goto/16 :goto_0

    .line 173
    :cond_9
    const/16 v0, 0x100

    .line 174
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-double v6, v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-double v8, v1

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4070000000000000L    # 256.0

    mul-double/2addr v6, v8

    double-to-int v1, v6

    goto :goto_2

    .line 179
    :cond_a
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    move-object v1, v3

    goto :goto_3

    .line 186
    :pswitch_1
    const/16 v2, 0x5a

    goto/16 :goto_0

    .line 187
    :pswitch_2
    const/16 v2, 0xb4

    goto/16 :goto_0

    .line 188
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_4

    .line 190
    :catch_0
    move-exception v0

    .line 191
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 203
    :cond_b
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 204
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-le v0, v3, :cond_d

    .line 205
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v0, v4, v3, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 211
    :cond_c
    :goto_5
    if-eqz v2, :cond_e

    .line 212
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v4

    .line 213
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {v3, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 214
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 215
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 216
    int-to-float v2, v2

    int-to-float v6, v3

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v3, v3

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v3, v7

    invoke-virtual {v5, v2, v6, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 217
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v4, v1, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 218
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 222
    :goto_6
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 223
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 224
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 225
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 227
    new-instance v0, Lflipboard/activities/ChooseAvatarActivity$2;

    invoke-direct {v0, p0}, Lflipboard/activities/ChooseAvatarActivity$2;-><init>(Lflipboard/activities/ChooseAvatarActivity;)V

    .line 256
    iget-object v2, p0, Lflipboard/activities/ChooseAvatarActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/activities/ChooseAvatarActivity$3;

    invoke-direct {v3, p0}, Lflipboard/activities/ChooseAvatarActivity$3;-><init>(Lflipboard/activities/ChooseAvatarActivity;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 263
    iget-object v2, p0, Lflipboard/activities/ChooseAvatarActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v3, "image/jpeg"

    iget-object v4, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v5, Lflipboard/service/Flap$UploadAvatarRequest;

    invoke-direct {v5, v2, v4}, Lflipboard/service/Flap$UploadAvatarRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v5, v1, v3, v0}, Lflipboard/service/Flap$UploadAvatarRequest;->a([BLjava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$UploadAvatarRequest;

    goto/16 :goto_1

    .line 206
    :cond_d
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ge v0, v3, :cond_c

    .line 207
    const/4 v0, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v3

    div-int/lit8 v4, v4, 0x2

    invoke-static {v1, v0, v4, v3, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_5

    :cond_e
    move-object v0, v1

    goto :goto_6

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lflipboard/activities/ChooseAvatarActivity;IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lflipboard/activities/ChooseAvatarActivity;->a(IILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    return-object v0
.end method

.method public choosePicture(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 63
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const v0, 0x7f0d031e

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const v0, 0x7f020071

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const v0, 0x7f0d004d

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const v0, 0x7f0201c2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->x()Ljava/util/List;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    iget-object v6, p0, Lflipboard/activities/ChooseAvatarActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v7, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v6

    iget-object v6, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v6}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    const-string v6, "flipboard"

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    iget-object v6, p0, Lflipboard/activities/ChooseAvatarActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v7, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v6

    iget-object v7, p0, Lflipboard/activities/ChooseAvatarActivity;->o:Lflipboard/util/Log;

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v8}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    const-string v7, "%s (%s)"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v9

    iget-object v0, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    aput-object v0, v8, v10

    invoke-static {v7, v8}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const v0, 0x7f0d026d

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const v0, 0x7f020168

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    const v5, 0x7f0d0093

    invoke-virtual {v0, v5}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    new-instance v5, Lflipboard/activities/ChooseAvatarActivity$4;

    invoke-direct {v5, p0, v1, v2}, Lflipboard/activities/ChooseAvatarActivity$4;-><init>(Lflipboard/activities/ChooseAvatarActivity;Ljava/util/List;Ljava/util/List;)V

    iput-object v5, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->p:Landroid/widget/ListAdapter;

    new-instance v2, Lflipboard/activities/ChooseAvatarActivity$5;

    invoke-direct {v2, p0, v1, v4, v3}, Lflipboard/activities/ChooseAvatarActivity$5;-><init>(Lflipboard/activities/ChooseAvatarActivity;Ljava/util/List;Ljava/util/List;I)V

    iput-object v2, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "source"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method protected final n_()V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    .line 391
    iget-boolean v1, p0, Lflipboard/activities/ChooseAvatarActivity;->P:Z

    if-eqz v1, :cond_0

    const-string v1, "progress"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 392
    new-instance v1, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    .line 393
    const v2, 0x7f0d0122

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    .line 394
    new-instance v2, Lflipboard/activities/ChooseAvatarActivity$6;

    invoke-direct {v2, p0}, Lflipboard/activities/ChooseAvatarActivity$6;-><init>(Lflipboard/activities/ChooseAvatarActivity;)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 414
    const-string v2, "progress"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 416
    :cond_0
    return-void
.end method

.method protected final o_()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "progress"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 421
    if-eqz v0, :cond_0

    .line 422
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    .line 423
    iget-boolean v1, v1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 424
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 427
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 118
    invoke-direct {p0, p1, p2, p3}, Lflipboard/activities/ChooseAvatarActivity;->a(IILandroid/content/Intent;)V

    .line 119
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lflipboard/activities/ChooseAvatarActivity;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 46
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    .line 47
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    iget-object v1, p0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 52
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 432
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    .line 433
    invoke-virtual {p0}, Lflipboard/activities/ChooseAvatarActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 434
    const v1, 0x7f0f0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 436
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onStart()V

    .line 58
    invoke-virtual {p0}, Lflipboard/activities/ChooseAvatarActivity;->p_()V

    .line 59
    return-void
.end method

.method final p_()V
    .locals 3

    .prologue
    .line 441
    const v0, 0x7f0a037a

    invoke-virtual {p0, v0}, Lflipboard/activities/ChooseAvatarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 442
    if-eqz v0, :cond_0

    .line 443
    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 444
    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 445
    iget-object v1, p0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 447
    :cond_0
    return-void
.end method
