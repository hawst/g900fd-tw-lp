.class Lflipboard/activities/CreateAccountActivity$16$2;
.super Ljava/lang/Object;
.source "CreateAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lflipboard/activities/CreateAccountActivity$16;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateAccountActivity$16;Lflipboard/service/FlipboardManager$CreateAccountMessage;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 850
    iput-object p1, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iput-object p2, p0, Lflipboard/activities/CreateAccountActivity$16$2;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    iput-object p3, p0, Lflipboard/activities/CreateAccountActivity$16$2;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 854
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    sget-object v1, Lflipboard/service/FlipboardManager$CreateAccountMessage;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    if-ne v0, v1, :cond_3

    .line 855
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v0

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->a:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_1

    .line 856
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "extra_invite"

    iget-object v3, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v3, v3, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iget-object v3, v3, Lflipboard/activities/CreateAccountActivity;->D:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lflipboard/activities/CreateAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 869
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->a:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->b(Ljava/lang/String;)Lflipboard/activities/LoginActivity$SignInMethod;

    move-result-object v0

    .line 870
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v1}, Lflipboard/activities/CreateAccountActivity;->d(Lflipboard/activities/CreateAccountActivity;)Lflipboard/objs/UsageEventV2$EventCategory;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v2}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v2

    invoke-static {v4, v0, v1, v2}, Lflipboard/activities/CreateAccountActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 871
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0, v4}, Lflipboard/activities/CreateAccountActivity;->a(Lflipboard/activities/CreateAccountActivity;Z)Z

    .line 872
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/CreateAccountActivity;->finish()V

    .line 886
    :goto_1
    return-void

    .line 857
    :cond_1
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v0

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_0

    .line 858
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 859
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 860
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 861
    const/4 v0, 0x0

    .line 862
    iget-object v3, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    if-eqz v3, :cond_2

    .line 863
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    .line 865
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 866
    const-string v3, "name"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 867
    const-string v2, "avatar"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 868
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-virtual {v0, v5, v1}, Lflipboard/activities/CreateAccountActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 874
    :cond_3
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->b:Ljava/lang/Object;

    check-cast v0, Lflipboard/service/Flap$ErrorData;

    .line 876
    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v1, v1, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    const v2, 0x7f0d00be

    invoke-virtual {v1, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 877
    if-eqz v0, :cond_4

    iget-object v2, v0, Lflipboard/service/Flap$ErrorData;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 878
    :cond_4
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    const v2, 0x7f0d0104

    invoke-virtual {v0, v2}, Lflipboard/activities/CreateAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 879
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v2, v1, v0, v4}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 880
    :cond_5
    iget v2, v0, Lflipboard/service/Flap$ErrorData;->a:I

    const/16 v3, 0x44d

    if-ne v2, v3, :cond_6

    .line 881
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/service/Flap$ErrorData;->b:Ljava/lang/String;

    invoke-static {v2, v1, v0}, Lflipboard/activities/CreateAccountActivity;->a(Lflipboard/activities/CreateAccountActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 883
    :cond_6
    iget-object v2, p0, Lflipboard/activities/CreateAccountActivity$16$2;->c:Lflipboard/activities/CreateAccountActivity$16;

    iget-object v2, v2, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/service/Flap$ErrorData;->b:Ljava/lang/String;

    invoke-static {v2, v1, v0, v4}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method
