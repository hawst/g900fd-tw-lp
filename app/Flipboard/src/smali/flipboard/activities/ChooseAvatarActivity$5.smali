.class Lflipboard/activities/ChooseAvatarActivity$5;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "ChooseAvatarActivity.java"


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:I

.field final synthetic d:Lflipboard/activities/ChooseAvatarActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ChooseAvatarActivity;Ljava/util/List;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    iput-object p2, p0, Lflipboard/activities/ChooseAvatarActivity$5;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/activities/ChooseAvatarActivity$5;->b:Ljava/util/List;

    iput p4, p0, Lflipboard/activities/ChooseAvatarActivity$5;->c:I

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 6

    .prologue
    const v5, 0x7f0a037a

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 348
    if-nez p2, :cond_0

    .line 350
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 351
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x2059

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 371
    :goto_0
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    invoke-static {v0}, Lflipboard/activities/ChooseAvatarActivity;->a(Lflipboard/activities/ChooseAvatarActivity;)Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 372
    return-void

    .line 352
    :cond_0
    if-ne p2, v4, :cond_1

    .line 354
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 355
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 356
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x205a

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_2

    .line 361
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    iget-object v0, v0, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v3

    .line 362
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    invoke-virtual {v0, v5}, Lflipboard/activities/ChooseAvatarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 363
    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_0

    .line 366
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    iget-object v0, v0, Lflipboard/activities/ChooseAvatarActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->b:Ljava/util/List;

    iget v2, p0, Lflipboard/activities/ChooseAvatarActivity$5;->c:I

    sub-int v2, p2, v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 367
    iget-object v1, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    iget-object v1, v1, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    .line 368
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    invoke-virtual {v0, v5}, Lflipboard/activities/ChooseAvatarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 369
    iget-object v1, p0, Lflipboard/activities/ChooseAvatarActivity$5;->d:Lflipboard/activities/ChooseAvatarActivity;

    iget-object v1, v1, Lflipboard/activities/ChooseAvatarActivity;->n:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
