.class Lflipboard/activities/ChooseAvatarActivity$4;
.super Landroid/widget/BaseAdapter;
.source "ChooseAvatarActivity.java"


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lflipboard/activities/ChooseAvatarActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ChooseAvatarActivity;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lflipboard/activities/ChooseAvatarActivity$4;->c:Lflipboard/activities/ChooseAvatarActivity;

    iput-object p2, p0, Lflipboard/activities/ChooseAvatarActivity$4;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/activities/ChooseAvatarActivity$4;->b:Ljava/util/List;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$4;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$4;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 318
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f0a017d

    const v5, 0x7f0a017c

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 323
    iget-object v0, p0, Lflipboard/activities/ChooseAvatarActivity$4;->c:Lflipboard/activities/ChooseAvatarActivity;

    const v1, 0x7f030067

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 324
    iget-object v1, p0, Lflipboard/activities/ChooseAvatarActivity$4;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 326
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 327
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 328
    iget-object v2, p0, Lflipboard/activities/ChooseAvatarActivity$4;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 329
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 338
    :cond_0
    :goto_0
    const v1, 0x7f0a017e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 339
    iget-object v2, p0, Lflipboard/activities/ChooseAvatarActivity$4;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    return-object v0

    .line 330
    :cond_1
    iget-object v1, p0, Lflipboard/activities/ChooseAvatarActivity$4;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 332
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    .line 333
    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 334
    iget-object v2, p0, Lflipboard/activities/ChooseAvatarActivity$4;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 335
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    .line 336
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
