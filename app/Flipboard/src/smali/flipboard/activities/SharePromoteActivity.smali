.class public Lflipboard/activities/SharePromoteActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "SharePromoteActivity.java"


# instance fields
.field private n:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->a()V

    .line 23
    iget-object v0, p0, Lflipboard/activities/SharePromoteActivity;->n:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.show.share.magazine"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    new-instance v0, Lflipboard/activities/SharePromoteFragment;

    iget-object v1, p0, Lflipboard/activities/SharePromoteActivity;->n:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lflipboard/activities/SharePromoteFragment;-><init>(Landroid/os/Bundle;)V

    .line 25
    const-string v1, "share_promote"

    invoke-virtual {v0, p0, v1}, Lflipboard/gui/dialog/FLDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    iget-object v0, p0, Lflipboard/activities/SharePromoteActivity;->n:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.show.also.flipped"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-static {}, Lflipboard/activities/AlsoFlippedFragment;->d()Lflipboard/activities/AlsoFlippedFragment;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lflipboard/activities/SharePromoteActivity;->n:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    const-string v1, "also_flipped"

    invoke-virtual {v0, p0, v1}, Lflipboard/gui/dialog/FLDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    const v0, 0x7f030120

    invoke-virtual {p0, v0}, Lflipboard/activities/SharePromoteActivity;->setContentView(I)V

    .line 17
    invoke-virtual {p0}, Lflipboard/activities/SharePromoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SharePromoteActivity;->n:Landroid/os/Bundle;

    .line 18
    return-void
.end method
