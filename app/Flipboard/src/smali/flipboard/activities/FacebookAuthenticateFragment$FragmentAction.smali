.class public final enum Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;
.super Ljava/lang/Enum;
.source "FacebookAuthenticateFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

.field public static final enum b:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

.field public static final enum c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

.field public static final enum d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

.field private static final synthetic e:[Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    const-string v1, "CREATE_FLIPBOARD_ACCOUNT"

    invoke-direct {v0, v1, v2}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->a:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    const-string v1, "LOGIN_TO_FLIPBOARD"

    invoke-direct {v0, v1, v3}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->b:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    const-string v1, "ADD_TO_EXISTING_ACCOUNT"

    invoke-direct {v0, v1, v4}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    const-string v1, "RELOGIN_TO_FACEBOOK_SERVICE"

    invoke-direct {v0, v1, v5}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->a:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->b:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->e:[Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    return-object v0
.end method

.method public static values()[Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->e:[Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v0}, [Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    return-object v0
.end method
