.class public Lflipboard/activities/LogOutActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "LogOutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 19
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-virtual {p0, v4}, Lflipboard/activities/LogOutActivity;->setResult(I)V

    .line 21
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 22
    const v1, 0x7f0d00a5

    invoke-virtual {p0, v1}, Lflipboard/activities/LogOutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Flipboard"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 23
    const v1, 0x7f0d02f4

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 24
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 26
    const v1, 0x7f0d00a3

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 27
    new-instance v1, Lflipboard/activities/LogOutActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/LogOutActivity$1;-><init>(Lflipboard/activities/LogOutActivity;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 42
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "sign_out"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 43
    return-void
.end method
