.class public Lflipboard/activities/CategoryPickerFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "CategoryPickerFragment.java"

# interfaces
.implements Lflipboard/gui/FLSearchBar$SearchResultObserver;


# instance fields
.field a:Lflipboard/activities/CategoryPickerFragment$CategoryPickerListener;

.field b:Z

.field c:Landroid/view/View$OnClickListener;

.field private d:Lflipboard/gui/firstrun/CategoryPickerCloud;

.field private e:Lflipboard/gui/FLSearchBar;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 121
    new-instance v0, Lflipboard/activities/CategoryPickerFragment$4;

    invoke-direct {v0, p0}, Lflipboard/activities/CategoryPickerFragment$4;-><init>(Lflipboard/activities/CategoryPickerFragment;)V

    iput-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->c:Landroid/view/View$OnClickListener;

    .line 303
    return-void
.end method

.method public static a()Lflipboard/activities/CategoryPickerFragment;
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lflipboard/activities/CategoryPickerFragment;

    invoke-direct {v0}, Lflipboard/activities/CategoryPickerFragment;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/FLSearchBar;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    return-object v0
.end method

.method private static a(JLjava/lang/String;IZIZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309
    new-instance v3, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->y:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v3, v0, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 310
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 311
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v4, "initial_search"

    invoke-virtual {v3, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 312
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v4, "topic_picker"

    invoke-virtual {v3, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 313
    sget-object v4, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    if-eqz p6, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 314
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 315
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->G:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v3, v0, p2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 316
    const-string v0, "top_result_offered"

    if-eqz p4, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 317
    const-string v0, "number_categories"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 319
    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2;->c()V

    .line 321
    return-void

    :cond_0
    move v0, v2

    .line 313
    goto :goto_0

    :cond_1
    move v1, v2

    .line 316
    goto :goto_1
.end method

.method static synthetic b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/CategoryPickerFragment;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/CategoryPickerFragment;->f:Z

    return v0
.end method

.method static synthetic d(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/activities/CategoryPickerFragment$CategoryPickerListener;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->a:Lflipboard/activities/CategoryPickerFragment$CategoryPickerListener;

    return-object v0
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    invoke-virtual {v2}, Lflipboard/gui/FLSearchBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-static/range {v0 .. v6}, Lflipboard/activities/CategoryPickerFragment;->a(JLjava/lang/String;IZIZ)V

    .line 231
    return-void
.end method

.method public final a(Ljava/util/List;ZJ)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;ZJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 185
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p3

    .line 187
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 189
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 193
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v4

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SearchResultCategory;

    .line 194
    iget-object v6, v2, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    const-string v9, "topic"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 195
    iget-object v6, v2, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v6, v3

    .line 196
    iget-object v2, v2, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SearchResultItem;

    .line 197
    new-instance v10, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    invoke-direct {v10}, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;-><init>()V

    .line 198
    const-string v3, "topic"

    iput-object v3, v10, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->feedType:Ljava/lang/String;

    .line 199
    iget-object v3, v2, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iput-object v3, v10, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    .line 200
    iget-object v2, v2, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    iput-object v2, v10, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    .line 201
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 206
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 207
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SearchResultCategory;

    iget-object v2, v2, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    sget-object v4, Lflipboard/objs/SearchResultItem;->h:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 209
    :cond_2
    if-nez p2, :cond_3

    .line 211
    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    invoke-virtual {v2}, Lflipboard/gui/FLSearchBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Lflipboard/activities/CategoryPickerFragment;->a(JLjava/lang/String;IZIZ)V

    .line 214
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/CategoryPickerFragment$5;

    invoke-direct {v1, p0, v7, p2}, Lflipboard/activities/CategoryPickerFragment$5;-><init>(Lflipboard/activities/CategoryPickerFragment;Ljava/util/ArrayList;Z)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 225
    return-void

    :cond_4
    move v3, v6

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 53
    const v0, 0x7f03002b

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 54
    const v0, 0x7f0a00b8

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/firstrun/CategoryPickerCloud;

    iput-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    .line 57
    const v0, 0x7f0a00b5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 58
    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const v1, 0x7f0a004d

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    .line 60
    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iput-object v0, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->f:Lflipboard/gui/FLTextView;

    iput-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->g:Lflipboard/gui/FLTextView;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v4}, Lflipboard/service/User;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0152

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0201bf

    invoke-virtual {v0, v4}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d00c9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080033

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    iput v7, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    .line 62
    :goto_0
    const v2, 0x7f0a00b6

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLSearchBar;

    iput-object v2, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    .line 63
    const v2, 0x7f0a00b7

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextView;

    .line 64
    iget-object v4, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    invoke-virtual {v4, p0}, Lflipboard/gui/FLSearchBar;->setSearchResultObserver(Lflipboard/gui/FLSearchBar$SearchResultObserver;)V

    .line 65
    iget-object v4, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    new-instance v5, Lflipboard/activities/CategoryPickerFragment$1;

    invoke-direct {v5, p0, v2, v0, v1}, Lflipboard/activities/CategoryPickerFragment$1;-><init>(Lflipboard/activities/CategoryPickerFragment;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;)V

    invoke-virtual {v4, v5}, Lflipboard/gui/FLSearchBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 80
    iget-object v4, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    new-instance v5, Lflipboard/activities/CategoryPickerFragment$2;

    invoke-direct {v5, p0, v2, v0, v1}, Lflipboard/activities/CategoryPickerFragment$2;-><init>(Lflipboard/activities/CategoryPickerFragment;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;)V

    invoke-virtual {v4, v5}, Lflipboard/gui/firstrun/CategoryPickerCloud;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 100
    iget-object v4, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    new-instance v5, Lflipboard/activities/CategoryPickerFragment$3;

    invoke-direct {v5, p0, v2, v0, v1}, Lflipboard/activities/CategoryPickerFragment$3;-><init>(Lflipboard/activities/CategoryPickerFragment;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;)V

    invoke-virtual {v4, v5}, Lflipboard/gui/FLSearchBar;->setKeyBoardCloseListener(Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;)V

    .line 112
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    iput-object v2, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->i:Lflipboard/gui/FLTextView;

    iget-object v2, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->i:Lflipboard/gui/FLTextView;

    new-instance v4, Lflipboard/gui/firstrun/CategoryPickerCloud$1;

    invoke-direct {v4, v0, v1}, Lflipboard/gui/firstrun/CategoryPickerCloud$1;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud;Lflipboard/gui/FLSearchBar;)V

    invoke-virtual {v2, v4}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->requestFocus()Z

    .line 114
    return-object v3

    .line 60
    :cond_0
    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0106

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 265
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onPause()V

    .line 266
    iget-boolean v0, p0, Lflipboard/activities/CategoryPickerFragment;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lflipboard/activities/CategoryPickerFragment;->h:Z

    if-nez v0, :cond_1

    .line 268
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 269
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 271
    iget-boolean v1, p0, Lflipboard/activities/CategoryPickerFragment;->b:Z

    if-eqz v1, :cond_2

    .line 272
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$MethodEventData;->b:Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 277
    :goto_0
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->x:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getSelectedTopics()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 278
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getShownItemsCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 280
    iget-boolean v1, p0, Lflipboard/activities/CategoryPickerFragment;->f:Z

    if-eqz v1, :cond_3

    .line 281
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 288
    :goto_1
    iget-boolean v1, p0, Lflipboard/activities/CategoryPickerFragment;->f:Z

    if-eqz v1, :cond_0

    .line 289
    iput-boolean v4, p0, Lflipboard/activities/CategoryPickerFragment;->h:Z

    .line 291
    :cond_0
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 293
    :cond_1
    iput-boolean v3, p0, Lflipboard/activities/CategoryPickerFragment;->f:Z

    .line 294
    return-void

    .line 274
    :cond_2
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$MethodEventData;->a:Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_0

    .line 283
    :cond_3
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_1
.end method

.method public setUserVisibleHint(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 234
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->setUserVisibleHint(Z)V

    .line 235
    if-eqz p1, :cond_2

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/CategoryPickerFragment;->g:Z

    .line 239
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 240
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v2, "category_selector"

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 241
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$MethodEventData;->b:Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 246
    :goto_0
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 261
    :cond_0
    :goto_1
    return-void

    .line 244
    :cond_1
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$MethodEventData;->a:Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_0

    .line 248
    :cond_2
    iput-boolean v2, p0, Lflipboard/activities/CategoryPickerFragment;->g:Z

    .line 250
    invoke-virtual {p0}, Lflipboard/activities/CategoryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p0}, Lflipboard/activities/CategoryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 253
    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    invoke-virtual {v1}, Lflipboard/gui/FLSearchBar;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 256
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->e:Lflipboard/gui/FLSearchBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLSearchBar;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment;->d:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getSelectedTopics()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/Map;)V

    goto :goto_1
.end method
