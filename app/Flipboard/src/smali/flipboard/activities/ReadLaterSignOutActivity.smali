.class public Lflipboard/activities/ReadLaterSignOutActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ReadLaterSignOutActivity.java"


# instance fields
.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ReadLaterSignOutActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/activities/ReadLaterSignOutActivity;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 31
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f0300f4

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterSignOutActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lflipboard/activities/ReadLaterSignOutActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 35
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 37
    invoke-virtual {p0}, Lflipboard/activities/ReadLaterSignOutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 38
    const v0, 0x7f0a02bf

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterSignOutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 39
    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    const v0, 0x7f0a02c0

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterSignOutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 42
    const-string v2, "account_username"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    const-string v0, "account_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ReadLaterSignOutActivity;->n:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public signOut(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 49
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ReadLaterSignOutActivity;->n:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v0

    .line 52
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v1, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0d00a5

    .line 53
    invoke-virtual {p0, v2}, Lflipboard/activities/ReadLaterSignOutActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d00a4

    .line 54
    invoke-virtual {p0, v2}, Lflipboard/activities/ReadLaterSignOutActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 56
    const v1, 0x7f0d02f4

    new-instance v2, Lflipboard/activities/ReadLaterSignOutActivity$1;

    invoke-direct {v2, p0}, Lflipboard/activities/ReadLaterSignOutActivity$1;-><init>(Lflipboard/activities/ReadLaterSignOutActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    const v1, 0x7f0d004a

    new-instance v2, Lflipboard/activities/ReadLaterSignOutActivity$2;

    invoke-direct {v2, p0}, Lflipboard/activities/ReadLaterSignOutActivity$2;-><init>(Lflipboard/activities/ReadLaterSignOutActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterSignOutActivity;->a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;

    .line 74
    return-void
.end method
