.class public Lflipboard/activities/VideoAdActivity;
.super Lflipboard/activities/FeedActivity;
.source "VideoAdActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final n:Lflipboard/util/Log;


# instance fields
.field o:Z

.field private p:Ljava/lang/String;

.field private q:Lflipboard/objs/Ad$MetricValues;

.field private r:Landroid/widget/MediaController;

.field private s:Lflipboard/gui/FLVideoView;

.field private t:Landroid/widget/RelativeLayout;

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final z:[Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "video"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/VideoAdActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lflipboard/activities/FeedActivity;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->x:Z

    .line 50
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object v0, p0, Lflipboard/activities/VideoAdActivity;->z:[Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private I()I
    .locals 3

    .prologue
    .line 175
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 178
    const/4 v0, 0x3

    .line 186
    :goto_0
    return v0

    .line 180
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 181
    const/4 v0, 0x1

    goto :goto_0

    .line 183
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/VideoAdActivity;)Landroid/widget/MediaController;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/VideoAdActivity;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lflipboard/activities/VideoAdActivity;->b(I)V

    return-void
.end method

.method static synthetic b(Lflipboard/activities/VideoAdActivity;)Lflipboard/gui/FLVideoView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4039000000000000L    # 25.0

    .line 357
    int-to-double v0, p1

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 364
    div-int/lit8 v0, v0, 0x19

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 365
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->z:[Ljava/util/concurrent/atomic/AtomicBoolean;

    aget-object v0, v0, v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->q:Lflipboard/objs/Ad$MetricValues;

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported playback_percent"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget-object v0, v0, Lflipboard/objs/Ad$MetricValues;->a:Ljava/lang/String;

    :goto_1
    invoke-static {v0}, Lflipboard/service/FLAdManager;->b(Ljava/lang/String;)V

    .line 364
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 366
    :sswitch_1
    iget-object v0, v0, Lflipboard/objs/Ad$MetricValues;->b:Ljava/lang/String;

    goto :goto_1

    :sswitch_2
    iget-object v0, v0, Lflipboard/objs/Ad$MetricValues;->c:Ljava/lang/String;

    goto :goto_1

    :sswitch_3
    iget-object v0, v0, Lflipboard/objs/Ad$MetricValues;->d:Ljava/lang/String;

    goto :goto_1

    :sswitch_4
    iget-object v0, v0, Lflipboard/objs/Ad$MetricValues;->e:Ljava/lang/String;

    goto :goto_1

    .line 371
    :cond_0
    return-void

    .line 366
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x19 -> :sswitch_1
        0x32 -> :sswitch_2
        0x4b -> :sswitch_3
        0x64 -> :sswitch_4
    .end sparse-switch
.end method

.method private j()V
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lflipboard/activities/VideoAdActivity;->J:Lflipboard/util/Log;

    .line 131
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->finish()V

    .line 132
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 8

    .prologue
    .line 136
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 138
    iget-wide v0, p0, Lflipboard/activities/VideoAdActivity;->V:J

    .line 139
    iget-wide v4, p0, Lflipboard/activities/VideoAdActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/VideoAdActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 142
    :cond_0
    iget-object v3, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v3}, Lflipboard/gui/FLVideoView;->getTotalWatchedTime()J

    move-result-wide v4

    .line 143
    iget-object v3, p0, Lflipboard/activities/VideoAdActivity;->q:Lflipboard/objs/Ad$MetricValues;

    iget-object v3, v3, Lflipboard/objs/Ad$MetricValues;->f:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;J)V

    .line 144
    const-string v3, "extra_result_active_time"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 145
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/VideoAdActivity;->setResult(ILandroid/content/Intent;)V

    .line 146
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 147
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lflipboard/activities/VideoAdActivity;->J:Lflipboard/util/Log;

    .line 337
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onBackPressed()V

    .line 338
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->P:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->show(I)V

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 87
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 88
    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 93
    :cond_0
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lflipboard/activities/VideoAdActivity;->b(I)V

    .line 94
    invoke-direct {p0}, Lflipboard/activities/VideoAdActivity;->j()V

    .line 95
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 208
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 209
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 212
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 215
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    iget v1, p0, Lflipboard/activities/VideoAdActivity;->u:I

    iget v2, p0, Lflipboard/activities/VideoAdActivity;->v:I

    invoke-direct {p0}, Lflipboard/activities/VideoAdActivity;->I()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/gui/FLVideoView;->a(III)V

    .line 217
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 54
    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->requestWindowFeature(I)Z

    .line 55
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    iput-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->o:Z

    .line 57
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/VideoAdActivity;->p:Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "impressionValues"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$MetricValues;

    iput-object v0, p0, Lflipboard/activities/VideoAdActivity;->q:Lflipboard/objs/Ad$MetricValues;

    .line 64
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->z:[Ljava/util/concurrent/atomic/AtomicBoolean;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 65
    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->z:[Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    aput-object v2, v1, v0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 191
    packed-switch p1, :pswitch_data_0

    .line 202
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 193
    :pswitch_0
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialog;

    const v1, 0x7f0d01e6

    invoke-direct {v0, p0, v1}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;I)V

    .line 194
    new-instance v1, Lflipboard/activities/VideoAdActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/VideoAdActivity$1;-><init>(Lflipboard/activities/VideoAdActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    .line 345
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->o:Z

    .line 346
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onDestroy()V

    .line 347
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 99
    sget-object v0, Lflipboard/activities/VideoAdActivity;->n:Lflipboard/util/Log;

    const-string v1, "Error playing video ad: %d, %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    invoke-direct {p0}, Lflipboard/activities/VideoAdActivity;->j()V

    .line 101
    return v5
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 316
    sget-object v0, Lflipboard/activities/VideoAdActivity;->J:Lflipboard/util/Log;

    .line 318
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 319
    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 323
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v1, "VideoAdActivity:onPause"

    new-instance v2, Lflipboard/activities/VideoAdActivity$6;

    invoke-direct {v2, p0}, Lflipboard/activities/VideoAdActivity$6;-><init>(Lflipboard/activities/VideoAdActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 331
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onPause()V

    .line 332
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 106
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    iput v0, p0, Lflipboard/activities/VideoAdActivity;->v:I

    .line 107
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    iput v0, p0, Lflipboard/activities/VideoAdActivity;->u:I

    .line 108
    iget v0, p0, Lflipboard/activities/VideoAdActivity;->v:I

    if-lez v0, :cond_0

    iget v0, p0, Lflipboard/activities/VideoAdActivity;->u:I

    if-lez v0, :cond_0

    .line 109
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->removeDialog(I)V

    .line 111
    :cond_0
    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 112
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setEnabled(Z)V

    .line 113
    invoke-virtual {p1, v1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 114
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    iget v1, p0, Lflipboard/activities/VideoAdActivity;->u:I

    iget v2, p0, Lflipboard/activities/VideoAdActivity;->v:I

    invoke-direct {p0}, Lflipboard/activities/VideoAdActivity;->I()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/gui/FLVideoView;->a(III)V

    .line 116
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0}, Lflipboard/gui/FLVideoView;->start()V

    .line 118
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 158
    sget-object v0, Lflipboard/activities/VideoAdActivity;->J:Lflipboard/util/Log;

    .line 159
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onResume()V

    .line 161
    iget-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->w:Z

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->finish()V

    .line 164
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->x:Z

    .line 165
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 166
    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 167
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 151
    sget-object v0, Lflipboard/activities/VideoAdActivity;->J:Lflipboard/util/Log;

    .line 152
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onStart()V

    .line 153
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->setRequestedOrientation(I)V

    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->showDialog(I)V

    const v0, 0x7f030154

    :try_start_0
    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->setContentView(I)V

    const v0, 0x7f0a0393

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lflipboard/activities/VideoAdActivity;->t:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lflipboard/activities/VideoAdActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08009b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v2, 0x30

    const/16 v3, 0x50

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xf

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    new-instance v2, Lflipboard/gui/FLVideoView;

    new-instance v3, Lflipboard/activities/VideoAdActivity$2;

    invoke-direct {v3, p0}, Lflipboard/activities/VideoAdActivity$2;-><init>(Lflipboard/activities/VideoAdActivity;)V

    new-instance v4, Lflipboard/activities/VideoAdActivity$3;

    invoke-direct {v4, p0}, Lflipboard/activities/VideoAdActivity$3;-><init>(Lflipboard/activities/VideoAdActivity;)V

    invoke-direct {v2, p0, v3, v4}, Lflipboard/gui/FLVideoView;-><init>(Landroid/content/Context;Lflipboard/gui/FLVideoView$TouchEventCallback;Lflipboard/gui/FLVideoView$DurationCallback;)V

    iput-object v2, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    iget-object v2, p0, Lflipboard/activities/VideoAdActivity;->t:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lflipboard/gui/FLVideoView;->setZOrderOnTop(Z)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lflipboard/activities/VideoAdActivity$4;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/VideoAdActivity$4;-><init>(Lflipboard/activities/VideoAdActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    iget-object v2, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, v2}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    iget-object v2, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, v2}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    iget-object v2, p0, Lflipboard/activities/VideoAdActivity;->r:Landroid/widget/MediaController;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLVideoView;->setMediaController(Landroid/widget/MediaController;)V

    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLVideoView;->setVideoURI(Landroid/net/Uri;)V

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    new-instance v1, Lflipboard/activities/VideoAdActivity$5;

    invoke-direct {v1, p0}, Lflipboard/activities/VideoAdActivity$5;-><init>(Lflipboard/activities/VideoAdActivity;)V

    iput-object v1, p0, Lflipboard/activities/VideoAdActivity;->y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iget-object v1, p0, Lflipboard/activities/VideoAdActivity;->y:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/VideoAdActivity;->w:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/activities/VideoAdActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 2

    .prologue
    .line 122
    iput p3, p0, Lflipboard/activities/VideoAdActivity;->v:I

    .line 123
    iput p2, p0, Lflipboard/activities/VideoAdActivity;->u:I

    .line 124
    iget v0, p0, Lflipboard/activities/VideoAdActivity;->v:I

    if-lez v0, :cond_0

    iget v0, p0, Lflipboard/activities/VideoAdActivity;->u:I

    if-lez v0, :cond_0

    .line 125
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoAdActivity;->removeDialog(I)V

    .line 126
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity;->s:Lflipboard/gui/FLVideoView;

    invoke-direct {p0}, Lflipboard/activities/VideoAdActivity;->I()I

    move-result v1

    invoke-virtual {v0, p2, p3, v1}, Lflipboard/gui/FLVideoView;->a(III)V

    .line 127
    return-void
.end method
