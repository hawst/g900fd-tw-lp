.class Lflipboard/activities/WidgetConfigActivity$1;
.super Ljava/lang/Object;
.source "WidgetConfigActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/widget/FlipboardWidgetManager;

.field final synthetic b:Lflipboard/activities/WidgetConfigActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/WidgetConfigActivity;Lflipboard/widget/FlipboardWidgetManager;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lflipboard/activities/WidgetConfigActivity$1;->b:Lflipboard/activities/WidgetConfigActivity;

    iput-object p2, p0, Lflipboard/activities/WidgetConfigActivity$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 62
    .line 63
    packed-switch p2, :pswitch_data_0

    .line 70
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lflipboard/activities/WidgetConfigActivity$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v1, v3}, Lflipboard/widget/FlipboardWidgetManager;->a(JZ)V

    .line 73
    iget-object v0, p0, Lflipboard/activities/WidgetConfigActivity$1;->b:Lflipboard/activities/WidgetConfigActivity;

    invoke-virtual {v0}, Lflipboard/activities/WidgetConfigActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    .line 76
    const-string v1, "appWidgetId"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 78
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 79
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    iget-object v0, p0, Lflipboard/activities/WidgetConfigActivity$1;->b:Lflipboard/activities/WidgetConfigActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lflipboard/activities/WidgetConfigActivity;->setResult(ILandroid/content/Intent;)V

    .line 83
    :cond_0
    iget-object v0, p0, Lflipboard/activities/WidgetConfigActivity$1;->b:Lflipboard/activities/WidgetConfigActivity;

    invoke-virtual {v0, p1}, Lflipboard/activities/WidgetConfigActivity;->a(Landroid/content/DialogInterface;)V

    .line 84
    return-void

    .line 65
    :pswitch_1
    const-wide/32 v0, 0x36ee80

    goto :goto_0

    .line 66
    :pswitch_2
    const-wide/32 v0, 0x2932e00

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
