.class public Lflipboard/activities/CreateAccountActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "CreateAccountActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/activities/CreateAccountActivity;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a014c

    const-string v1, "field \'title\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lflipboard/activities/CreateAccountActivity;->x:Landroid/widget/TextView;

    .line 12
    const v0, 0x7f0a00f1

    const-string v1, "field \'createButton\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lflipboard/activities/CreateAccountActivity;->y:Landroid/widget/Button;

    .line 14
    const v0, 0x7f0a015d

    const-string v1, "field \'signInView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    iput-object v0, p1, Lflipboard/activities/CreateAccountActivity;->E:Landroid/view/View;

    .line 16
    return-void
.end method

.method public static reset(Lflipboard/activities/CreateAccountActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->x:Landroid/widget/TextView;

    .line 20
    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->y:Landroid/widget/Button;

    .line 21
    iput-object v0, p0, Lflipboard/activities/CreateAccountActivity;->E:Landroid/view/View;

    .line 22
    return-void
.end method
