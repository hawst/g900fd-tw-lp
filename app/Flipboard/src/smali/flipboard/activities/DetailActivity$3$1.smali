.class Lflipboard/activities/DetailActivity$3$1;
.super Ljava/lang/Object;
.source "DetailActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field final synthetic b:Lflipboard/activities/DetailActivity$3;


# direct methods
.method constructor <init>(Lflipboard/activities/DetailActivity$3;Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lflipboard/activities/DetailActivity$3$1;->b:Lflipboard/activities/DetailActivity$3;

    iput-object p2, p0, Lflipboard/activities/DetailActivity$3$1;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 370
    iget-object v0, p0, Lflipboard/activities/DetailActivity$3$1;->b:Lflipboard/activities/DetailActivity$3;

    iget-object v6, v0, Lflipboard/activities/DetailActivity$3;->a:Lflipboard/activities/DetailActivity;

    iget-object v0, p0, Lflipboard/activities/DetailActivity$3$1;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    iget v1, v6, Lflipboard/activities/DetailActivity;->z:I

    iget v2, v6, Lflipboard/activities/DetailActivity;->p:I

    if-le v1, v2, :cond_0

    sget-object v1, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, v1, :cond_2

    iget-object v1, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v0, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v4}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V

    iget v0, v6, Lflipboard/activities/DetailActivity;->z:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v6, Lflipboard/activities/DetailActivity;->z:I

    :cond_0
    :goto_0
    iget-object v0, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v7

    sget-object v0, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    iget-boolean v0, v6, Lflipboard/activities/DetailActivity;->y:Z

    if-eqz v0, :cond_5

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_4

    iget-object v0, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v5}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lflipboard/gui/Interstitial;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lflipboard/gui/Interstitial;

    invoke-virtual {v0}, Lflipboard/gui/Interstitial;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v9, "nytimes"

    invoke-static {v2, v9}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    iget v9, v0, Lflipboard/gui/Interstitial;->d:I

    if-ne v2, v9, :cond_3

    move v2, v3

    :goto_2
    if-nez v2, :cond_1

    sget-object v2, Lflipboard/util/MeteringHelper$ExitPath;->c:Lflipboard/util/MeteringHelper$ExitPath;

    invoke-virtual {v0, v2}, Lflipboard/gui/Interstitial;->setExitPath(Lflipboard/util/MeteringHelper$ExitPath;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_2
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, v1, :cond_0

    iget-object v0, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v0

    iget-object v1, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, v6, Lflipboard/activities/DetailActivity;->o:I

    if-le v0, v1, :cond_0

    iget-object v0, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V

    iget v0, v6, Lflipboard/activities/DetailActivity;->z:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v6, Lflipboard/activities/DetailActivity;->z:I

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_2

    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v2, v6, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->c(Landroid/view/View;)V

    goto :goto_3

    .line 371
    :cond_5
    return-void
.end method
