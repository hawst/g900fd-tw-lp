.class Lflipboard/activities/SettingsFragment$41;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1301
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1304
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1305
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 6

    .prologue
    const v5, 0x7f0d02a5

    const v4, 0x7f0d02a4

    .line 1309
    const-string v0, "Unknown"

    .line 1310
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "application_mode"

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1311
    packed-switch v1, :pswitch_data_0

    .line 1324
    :goto_0
    return-object v0

    .line 1313
    :pswitch_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0, v4}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1316
    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0, v5}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319
    :pswitch_2
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 1320
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0, v5}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1322
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$41;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0, v4}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1311
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
