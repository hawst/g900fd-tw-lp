.class Lflipboard/activities/FlipboardActivity$6;
.super Ljava/lang/Object;
.source "FlipboardActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lflipboard/activities/FlipboardActivity$6;->a:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 615
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$6;->a:Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-nez v0, :cond_1

    .line 657
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v0, v0, Lflipboard/model/ConfigSetting;->forceUpgradeHiddenLauncherIconBuildAfterViewSectionOrItemCount:I

    .line 620
    if-nez v0, :cond_2

    .line 621
    const/16 v0, 0xa

    .line 625
    :cond_2
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 626
    const-string v2, "hiddenVersionOpenedCount"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 627
    if-ge v2, v0, :cond_3

    .line 629
    add-int/lit8 v0, v2, 0x1

    .line 630
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hiddenVersionOpenedCount"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 633
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->AppDownloadURL:Ljava/lang/String;

    .line 634
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 637
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 639
    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$6;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 640
    iget-object v3, p0, Lflipboard/activities/FlipboardActivity$6;->a:Lflipboard/activities/FlipboardActivity;

    const v4, 0x7f0d033a

    invoke-virtual {v3, v4}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 642
    new-instance v3, Lflipboard/activities/FlipboardActivity$6$1;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/FlipboardActivity$6$1;-><init>(Lflipboard/activities/FlipboardActivity$6;Ljava/lang/String;)V

    iput-object v3, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 650
    const v0, 0x7f0d004a

    invoke-virtual {v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 652
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$6;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d0051

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 653
    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 654
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$6;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "china_mobile_upgrade"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
