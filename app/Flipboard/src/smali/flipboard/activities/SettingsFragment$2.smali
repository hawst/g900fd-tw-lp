.class Lflipboard/activities/SettingsFragment$2;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 407
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v2}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/AboutActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 409
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 393
    const-string v2, ""

    .line 396
    :try_start_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v3}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 397
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 398
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    :goto_0
    const-string v3, "%s %s, %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    const v6, 0x7f0d013a

    invoke-virtual {v5, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    aput-object v2, v4, v1

    const/4 v1, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 399
    :catch_0
    move-exception v0

    move-object v7, v0

    move-object v0, v2

    move-object v2, v7

    .line 400
    iget-object v3, p0, Lflipboard/activities/SettingsFragment$2;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    sget-object v3, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    invoke-virtual {v3, v2}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    move-object v2, v0

    move v0, v1

    goto :goto_0
.end method
