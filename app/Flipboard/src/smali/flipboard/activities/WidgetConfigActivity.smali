.class public Lflipboard/activities/WidgetConfigActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "WidgetConfigActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method private j()V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    invoke-virtual {p0, v2}, Lflipboard/activities/WidgetConfigActivity;->setResult(I)V

    .line 36
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v4

    .line 39
    invoke-virtual {v4}, Lflipboard/widget/FlipboardWidgetManager;->g()J

    move-result-wide v6

    .line 40
    cmp-long v0, v6, v8

    if-gez v0, :cond_0

    move v0, v1

    .line 52
    :goto_0
    new-instance v5, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v5, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {p0}, Lflipboard/activities/WidgetConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0356

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 54
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Lflipboard/activities/WidgetConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d0355

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 56
    invoke-virtual {p0}, Lflipboard/activities/WidgetConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f0d0354

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    .line 57
    invoke-virtual {p0}, Lflipboard/activities/WidgetConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0353

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v3

    new-instance v1, Lflipboard/activities/WidgetConfigActivity$1;

    invoke-direct {v1, p0, v4}, Lflipboard/activities/WidgetConfigActivity$1;-><init>(Lflipboard/activities/WidgetConfigActivity;Lflipboard/widget/FlipboardWidgetManager;)V

    .line 54
    invoke-virtual {v5, v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 87
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 88
    new-instance v1, Lflipboard/activities/WidgetConfigActivity$2;

    invoke-direct {v1, p0}, Lflipboard/activities/WidgetConfigActivity$2;-><init>(Lflipboard/activities/WidgetConfigActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 95
    invoke-virtual {p0, v0}, Lflipboard/activities/WidgetConfigActivity;->a(Landroid/app/Dialog;)V

    .line 96
    return-void

    .line 43
    :cond_0
    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    move v0, v2

    .line 44
    goto :goto_0

    .line 45
    :cond_1
    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    const-wide/32 v8, 0x6ddd00

    cmp-long v0, v6, v8

    if-gez v0, :cond_2

    move v0, v1

    .line 46
    goto :goto_0

    :cond_2
    move v0, v3

    .line 48
    goto :goto_0
.end method


# virtual methods
.method protected final F()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->F()V

    .line 101
    invoke-direct {p0}, Lflipboard/activities/WidgetConfigActivity;->j()V

    .line 102
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 26
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v0, :cond_0

    .line 27
    invoke-direct {p0}, Lflipboard/activities/WidgetConfigActivity;->j()V

    .line 29
    :cond_0
    return-void
.end method
