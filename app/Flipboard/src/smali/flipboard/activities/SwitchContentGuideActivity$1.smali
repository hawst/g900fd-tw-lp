.class Lflipboard/activities/SwitchContentGuideActivity$1;
.super Ljava/lang/Object;
.source "SwitchContentGuideActivity.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lflipboard/activities/SwitchContentGuideActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/SwitchContentGuideActivity;Z)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lflipboard/activities/SwitchContentGuideActivity$1;->b:Lflipboard/activities/SwitchContentGuideActivity;

    iput-boolean p2, p0, Lflipboard/activities/SwitchContentGuideActivity$1;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 257
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-boolean v3, v0, Lflipboard/service/FlipboardManager;->ad:Z

    .line 258
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const-string v1, "fail loading sections.json: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 237
    :try_start_0
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->g()Lflipboard/objs/ConfigContentGuide;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_0

    iget-object v1, v0, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    if-nez v1, :cond_1

    .line 239
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Failed to load valid content guide, it or its sections is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    :goto_0
    return-void

    .line 243
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lflipboard/service/FlipboardManager;->ad:Z

    .line 244
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$1;->b:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v0, v0, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    iget-boolean v2, p0, Lflipboard/activities/SwitchContentGuideActivity$1;->a:Z

    invoke-static {v1, v0, v2}, Lflipboard/activities/SwitchContentGuideActivity;->a(Lflipboard/activities/SwitchContentGuideActivity;Ljava/util/List;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 245
    :catch_0
    move-exception v0

    .line 246
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "failed to parse sections.json: %-E"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-boolean v4, v1, Lflipboard/service/FlipboardManager;->ad:Z

    .line 250
    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 264
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-boolean v3, v0, Lflipboard/service/FlipboardManager;->ad:Z

    .line 265
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const-string v1, "fail loading sections.json, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    return-void
.end method
