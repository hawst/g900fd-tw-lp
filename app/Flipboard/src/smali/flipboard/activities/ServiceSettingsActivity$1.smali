.class Lflipboard/activities/ServiceSettingsActivity$1;
.super Ljava/lang/Object;
.source "ServiceSettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Ljava/lang/reflect/Field;

.field final synthetic d:Lflipboard/activities/ServiceSettingsActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/String;Landroid/widget/CheckBox;Ljava/lang/reflect/Field;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lflipboard/activities/ServiceSettingsActivity$1;->d:Lflipboard/activities/ServiceSettingsActivity;

    iput-object p2, p0, Lflipboard/activities/ServiceSettingsActivity$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/ServiceSettingsActivity$1;->b:Landroid/widget/CheckBox;

    iput-object p4, p0, Lflipboard/activities/ServiceSettingsActivity$1;->c:Ljava/lang/reflect/Field;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity$1;->d:Lflipboard/activities/ServiceSettingsActivity;

    iget-object v0, v0, Lflipboard/activities/ServiceSettingsActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity$1;->d:Lflipboard/activities/ServiceSettingsActivity;

    invoke-static {v0}, Lflipboard/activities/ServiceSettingsActivity;->a(Lflipboard/activities/ServiceSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/ServiceSettingsActivity$1;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 128
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity$1;->d:Lflipboard/activities/ServiceSettingsActivity;

    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$1;->c:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lflipboard/activities/ServiceSettingsActivity$1;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lflipboard/activities/ServiceSettingsActivity;->a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/reflect/Field;Z)V

    goto :goto_0
.end method
