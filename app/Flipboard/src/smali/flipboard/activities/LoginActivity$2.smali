.class Lflipboard/activities/LoginActivity$2;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lflipboard/activities/LoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    iput-object p2, p0, Lflipboard/activities/LoginActivity$2;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v1

    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->b(Lflipboard/activities/LoginActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 139
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0201ab

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 141
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity$2;->a:Landroid/content/Context;

    const v2, 0x7f0e000d

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 142
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 148
    :goto_1
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 144
    :cond_1
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0201a8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 145
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity$2;->a:Landroid/content/Context;

    const v2, 0x7f0e000c

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 146
    iget-object v0, p0, Lflipboard/activities/LoginActivity$2;->b:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 154
    return-void
.end method
