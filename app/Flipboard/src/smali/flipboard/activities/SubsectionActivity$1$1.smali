.class Lflipboard/activities/SubsectionActivity$1$1;
.super Ljava/lang/Object;
.source "SubsectionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/objs/SectionListResult;

.field final synthetic b:Lflipboard/activities/SubsectionActivity$1;


# direct methods
.method constructor <init>(Lflipboard/activities/SubsectionActivity$1;Lflipboard/objs/SectionListResult;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iput-object p2, p0, Lflipboard/activities/SubsectionActivity$1$1;->a:Lflipboard/objs/SectionListResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$1$1;->a:Lflipboard/objs/SectionListResult;

    invoke-virtual {v0}, Lflipboard/objs/SectionListResult;->a()Ljava/util/List;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iget-object v0, v0, Lflipboard/activities/SubsectionActivity$1;->a:Landroid/view/View;

    const v1, 0x7f0a00bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 113
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iget-object v1, v1, Lflipboard/activities/SubsectionActivity$1;->c:Lflipboard/activities/SubsectionActivity;

    const v2, 0x7f0d0223

    invoke-virtual {v1, v2}, Lflipboard/activities/SubsectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iget-object v0, v0, Lflipboard/activities/SubsectionActivity$1;->a:Landroid/view/View;

    const v1, 0x7f0a00bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iget-object v1, v1, Lflipboard/activities/SubsectionActivity$1;->c:Lflipboard/activities/SubsectionActivity;

    invoke-static {v1}, Lflipboard/activities/SubsectionActivity;->a(Lflipboard/activities/SubsectionActivity;)Lflipboard/objs/ConfigService;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iget-object v2, p0, Lflipboard/activities/SubsectionActivity$1$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v2, v2, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v4, "flipboard"

    invoke-virtual {v3, v4}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/SubsectionActivity$1;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$1$1;->b:Lflipboard/activities/SubsectionActivity$1;

    iget-object v2, p0, Lflipboard/activities/SubsectionActivity$1$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v2, v2, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/SubsectionActivity$1;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method
