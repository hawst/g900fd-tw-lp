.class public Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CreateMagazineFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lflipboard/model/ConfigSetting$MagazineCategory;",
        ">;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field b:I

.field c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/ConfigSetting$MagazineCategory;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic e:Lflipboard/activities/CreateMagazineFragment;


# direct methods
.method public constructor <init>(Lflipboard/activities/CreateMagazineFragment;Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v1, 0x7f0a0348

    const v0, 0x7f030134

    .line 265
    iput-object p1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    .line 266
    invoke-direct {p0, p2, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 267
    iput-object p2, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->a:Landroid/content/Context;

    .line 268
    iput v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->b:I

    .line 269
    iput v1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->c:I

    .line 270
    iput-object p3, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->d:Ljava/util/List;

    .line 272
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 297
    .line 298
    if-nez p2, :cond_1

    .line 301
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 302
    iget v1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->b:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 304
    new-instance v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;

    invoke-direct {v1}, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;-><init>()V

    .line 305
    iget v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->c:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;->a:Landroid/widget/TextView;

    .line 306
    const v0, 0x7f0a027f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;->b:Landroid/view/View;

    .line 308
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 313
    :goto_0
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigSetting$MagazineCategory;

    .line 314
    iget-object v4, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    const-string v5, "no_category_selected"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 315
    iget-object v4, v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;->a:Landroid/widget/TextView;

    iget-object v5, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->displayNameKey:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v1, v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;->b:Landroid/view/View;

    iget-object v4, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v4}, Lflipboard/activities/CreateMagazineFragment;->d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v4

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v0}, Lflipboard/activities/CreateMagazineFragment;->d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 322
    :goto_2
    return-object p2

    .line 310
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;

    move-object v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 316
    goto :goto_1

    .line 318
    :cond_3
    iget-object v4, v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;->a:Landroid/widget/TextView;

    iget-object v5, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v5}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v5

    iget-object v6, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->displayNameKey:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v1, v1, Lflipboard/activities/CreateMagazineFragment$CategoryHolder;->b:Landroid/view/View;

    iget-object v4, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v4}, Lflipboard/activities/CreateMagazineFragment;->d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v4

    if-ne v0, v4, :cond_4

    :goto_3
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v3, v2

    goto :goto_3
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigSetting$MagazineCategory;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 276
    .line 278
    if-nez p2, :cond_0

    .line 279
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 280
    const v1, 0x7f03012e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 283
    :cond_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 285
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v1

    iget-object v1, v1, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    const-string v2, "no_category_selected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 286
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v2}, Lflipboard/activities/CreateMagazineFragment;->d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v2

    iget-object v2, v2, Lflipboard/model/ConfigSetting$MagazineCategory;->displayNameKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :goto_0
    return-object p2

    .line 288
    :cond_1
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;->e:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v1}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
