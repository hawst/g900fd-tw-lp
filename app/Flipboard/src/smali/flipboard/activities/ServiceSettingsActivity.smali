.class public Lflipboard/activities/ServiceSettingsActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ServiceSettingsActivity.java"


# instance fields
.field private n:Ljava/lang/String;

.field private o:Landroid/content/SharedPreferences;

.field private p:Landroid/view/ViewGroup;

.field private q:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ServiceSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->o:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/reflect/Field;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-static {p0}, Lflipboard/activities/ServiceSettingsActivity;->b(Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lflipboard/activities/ServiceSettingsActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lflipboard/activities/ServiceSettingsActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 199
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 200
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/16 v2, 0x24

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 201
    const-string v2, "%s_%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p2, v3, v0

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 202
    invoke-direct {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 30
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->q:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/settings/Settings;->onChange(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Failed to set setting %s to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/reflect/Field;Z)V
    .locals 5

    .prologue
    .line 30
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->q:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/settings/Settings;->onChange(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Failed to set setting %s to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/reflect/Field;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 228
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 231
    :goto_0
    return-object v0

    .line 230
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Failed to get setting value for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/ServiceSettingsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 188
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 189
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->q:Ljava/lang/Class;

    const-string v1, "STRINGS"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 190
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    .line 192
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Error looking up label for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "label: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final e()V
    .locals 1

    .prologue
    .line 238
    const-class v0, Lflipboard/activities/SettingsActivity;

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->a(Ljava/lang/Class;)V

    .line 239
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lflipboard/activities/ServiceSettingsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/ServiceSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 51
    const v0, 0x7f030116

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->setContentView(I)V

    .line 52
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_2

    .line 53
    invoke-virtual {p0}, Lflipboard/activities/ServiceSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/activities/ServiceSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v4, -0x2

    invoke-virtual {v0, v3, v4}, Landroid/view/Window;->setLayout(II)V

    .line 56
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/ServiceSettingsActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 57
    sget-object v3, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 59
    const v0, 0x7f0a0303

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 60
    const-string v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    const v0, 0x7f0a0304

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 63
    const-string v3, "account_image"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 65
    const v0, 0x7f0a0305

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 66
    const-string v3, "account_username"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    const-string v0, "account_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->n:Ljava/lang/String;

    .line 70
    const v0, 0x7f0a0306

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->p:Landroid/view/ViewGroup;

    .line 71
    const-string v0, "flipboard.settings.%s%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/activities/ServiceSettingsActivity;->n:Ljava/lang/String;

    invoke-virtual {v3, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    iget-object v3, p0, Lflipboard/activities/ServiceSettingsActivity;->n:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->q:Ljava/lang/Class;

    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->q:Ljava/lang/Class;

    invoke-static {v0}, Lflipboard/settings/Settings;->getPrefsFor(Ljava/lang/Class;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->o:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->q:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    array-length v8, v7

    move v6, v1

    :goto_1
    if-ge v6, v8, :cond_0

    aget-object v2, v7, v6

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    const-string v0, "STRINGS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lflipboard/activities/ServiceSettingsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030117

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    const v0, 0x7f0a0308

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    const v1, 0x7f0a030b

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lflipboard/gui/FLTextIntf;

    const v1, 0x7f0a030a

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const v10, 0x7f0a0307

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lflipboard/activities/ServiceSettingsActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v10}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v3, v0, :cond_5

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v0, Lflipboard/activities/ServiceSettingsActivity$1;

    invoke-direct {v0, p0, v4, v1, v2}, Lflipboard/activities/ServiceSettingsActivity$1;-><init>(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/String;Landroid/widget/CheckBox;Ljava/lang/reflect/Field;)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    :goto_2
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_5
    invoke-virtual {v3}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lflipboard/activities/ServiceSettingsActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-interface {v5, v0}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    new-instance v0, Lflipboard/activities/ServiceSettingsActivity$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/ServiceSettingsActivity$2;-><init>(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/reflect/Field;Ljava/lang/Class;Ljava/lang/String;Lflipboard/gui/FLTextIntf;)V

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public onSignOutClicked(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 242
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v0

    .line 245
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 246
    const v2, 0x7f0d004a

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 248
    const v2, 0x7f0d00a5

    invoke-virtual {p0, v2}, Lflipboard/activities/ServiceSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 249
    const v2, 0x7f0d02f4

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 250
    const v2, 0x7f0d00a4

    invoke-virtual {p0, v2}, Lflipboard/activities/ServiceSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 251
    new-instance v0, Lflipboard/activities/ServiceSettingsActivity$3;

    invoke-direct {v0, p0}, Lflipboard/activities/ServiceSettingsActivity$3;-><init>(Lflipboard/activities/ServiceSettingsActivity;)V

    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 260
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "sign_out"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 261
    return-void
.end method
