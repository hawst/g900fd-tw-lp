.class Lflipboard/activities/ServiceLoginActivity$4;
.super Ljava/lang/Object;
.source "ServiceLoginActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/Section;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ServiceLoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ServiceLoginActivity;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lflipboard/activities/ServiceLoginActivity$4;->a:Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ServiceLoginActivity$4;)V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$4;->a:Lflipboard/activities/ServiceLoginActivity;

    iget-boolean v0, v0, Lflipboard/activities/ServiceLoginActivity;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$4;->a:Lflipboard/activities/ServiceLoginActivity;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "source"

    const-string v3, "serviceSignIn"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "serviceIdentifier"

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity$4;->a:Lflipboard/activities/ServiceLoginActivity;

    iget-object v3, v3, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$4;->a:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v2}, Lflipboard/activities/ServiceLoginActivity;->c(Lflipboard/activities/ServiceLoginActivity;)Lflipboard/service/Section;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$4;->a:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v0}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 497
    check-cast p1, Lflipboard/service/FlipboardManager;

    check-cast p2, Lflipboard/service/Section;

    new-instance v0, Lflipboard/activities/ServiceLoginActivity$4$1;

    invoke-direct {v0, p0, p2, p1}, Lflipboard/activities/ServiceLoginActivity$4$1;-><init>(Lflipboard/activities/ServiceLoginActivity$4;Lflipboard/service/Section;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method
