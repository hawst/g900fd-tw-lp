.class Lflipboard/activities/LoginActivity$6;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/LoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lflipboard/activities/LoginActivity$6;->a:Lflipboard/activities/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 290
    iget-object v0, p0, Lflipboard/activities/LoginActivity$6;->a:Lflipboard/activities/LoginActivity;

    iget-object v0, v0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "do_first_launch"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "FirstRun ClickedFacebookSSO"

    invoke-static {v0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;)V

    .line 293
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 295
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 296
    const v1, 0x7f0d00bf

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 297
    iget-object v1, p0, Lflipboard/activities/LoginActivity$6;->a:Lflipboard/activities/LoginActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "date"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 302
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lflipboard/activities/LoginActivity$6;->a:Lflipboard/activities/LoginActivity;

    iput-boolean v2, v0, Lflipboard/activities/LoginActivity;->o:Z

    .line 300
    iget-object v0, p0, Lflipboard/activities/LoginActivity$6;->a:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->f(Lflipboard/activities/LoginActivity;)Lflipboard/activities/FacebookAuthenticateFragment;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->a()V

    goto :goto_0
.end method
