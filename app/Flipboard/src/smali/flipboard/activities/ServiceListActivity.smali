.class public Lflipboard/activities/ServiceListActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ServiceListActivity.java"


# instance fields
.field n:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 26
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceListActivity;->setContentView(I)V

    .line 29
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/app/Activity;)V

    .line 31
    invoke-virtual {p0}, Lflipboard/activities/ServiceListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 32
    const-string v1, "key_account_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    const-string v2, "key_pagekey"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    const-string v3, "key_title"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    invoke-static {v1, v2, v0}, Lflipboard/gui/personal/ServiceListFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lflipboard/gui/personal/ServiceListFragment;

    move-result-object v0

    .line 37
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    .line 38
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/ServiceListActivity;->n:Landroid/view/View;

    .line 39
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    const-string v3, "SERVICE_LIST"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 41
    return-void
.end method
