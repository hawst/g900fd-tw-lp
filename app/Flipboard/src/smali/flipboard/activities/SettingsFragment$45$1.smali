.class Lflipboard/activities/SettingsFragment$45$1;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lflipboard/activities/SettingsFragment$45;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment$45;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1692
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$45$1;->b:Lflipboard/activities/SettingsFragment$45;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$45$1;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1695
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45$1;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1696
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 1698
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$45$1;->b:Lflipboard/activities/SettingsFragment$45;

    iget-object v1, v1, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "locale_override"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1702
    :goto_0
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$45$1;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1703
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45$1;->b:Lflipboard/activities/SettingsFragment$45;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, p1}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/DialogInterface;)V

    .line 1704
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45$1;->b:Lflipboard/activities/SettingsFragment$45;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1705
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45$1;->b:Lflipboard/activities/SettingsFragment$45;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SettingsFragment;->o:Z

    .line 1706
    return-void

    .line 1700
    :cond_0
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$45$1;->b:Lflipboard/activities/SettingsFragment$45;

    iget-object v1, v1, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "locale_override"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
