.class Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DebugMenuActivity.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/DebugMenuActivity;


# direct methods
.method public constructor <init>(Lflipboard/activities/DebugMenuActivity;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 271
    iput-object p1, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->b:Lflipboard/activities/DebugMenuActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 272
    iput-object p2, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    .line 273
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 280
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 293
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 298
    .line 299
    if-nez p2, :cond_0

    .line 300
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->b:Lflipboard/activities/DebugMenuActivity;

    invoke-virtual {v0}, Lflipboard/activities/DebugMenuActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 303
    :cond_0
    const v0, 0x7f0a015e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 304
    if-eqz v0, :cond_1

    .line 305
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 308
    :cond_1
    const v0, 0x7f0a0160

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 309
    if-eqz v0, :cond_2

    .line 310
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 313
    :cond_2
    const v0, 0x7f0a0161

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 314
    if-eqz v0, :cond_3

    .line 316
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-wide v2, v1, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 317
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->b:Lflipboard/activities/DebugMenuActivity;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    .line 318
    iget-object v4, p0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;->b:Lflipboard/activities/DebugMenuActivity;

    const/4 v5, 0x1

    invoke-static {v4, v2, v3, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 319
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " at "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 322
    :cond_3
    return-object p2
.end method
