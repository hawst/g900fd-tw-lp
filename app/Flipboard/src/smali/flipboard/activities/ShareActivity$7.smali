.class Lflipboard/activities/ShareActivity$7;
.super Ljava/lang/Object;
.source "ShareActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ShareActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ShareActivity;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lflipboard/activities/ShareActivity$7;->a:Lflipboard/activities/ShareActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/high16 v3, 0x400000

    .line 387
    check-cast p2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v0, Lflipboard/activities/ShareActivity$8;->a:[I

    invoke-virtual {p2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ShareActivity$7;->a:Lflipboard/activities/ShareActivity;

    invoke-static {v1}, Lflipboard/activities/ShareActivity;->d(Lflipboard/activities/ShareActivity;)Lflipboard/util/Observer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/activities/ShareActivity$7;->a:Lflipboard/activities/ShareActivity;

    invoke-virtual {v0}, Lflipboard/activities/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/activities/ShareActivity$7;->a:Lflipboard/activities/ShareActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/ShareActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/activities/ShareActivity$7;->a:Lflipboard/activities/ShareActivity;

    const-class v2, Lflipboard/activities/CreateAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/activities/ShareActivity$7;->a:Lflipboard/activities/ShareActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/ShareActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
