.class Lflipboard/activities/ContentDrawerTabletActivity$1;
.super Ljava/lang/Object;
.source "ContentDrawerTabletActivity.java"

# interfaces
.implements Lflipboard/gui/EditableListView$DragListener;


# instance fields
.field final synthetic a:Lflipboard/activities/ContentDrawerTabletActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ContentDrawerTabletActivity;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 112
    if-eq p1, p2, :cond_0

    .line 114
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->getCount()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ContentDrawerListItem;)I

    .line 119
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(ILflipboard/objs/ContentDrawerListItem;)V

    .line 124
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 125
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lflipboard/service/User;->a(IIZ)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 100
    check-cast p1, Lflipboard/objs/ContentDrawerListItem;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lflipboard/objs/ContentDrawerListItem;->c(Z)V

    .line 101
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/EditableListView;->invalidateViews()V

    .line 102
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 106
    check-cast p1, Lflipboard/objs/ContentDrawerListItem;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lflipboard/objs/ContentDrawerListItem;->c(Z)V

    .line 107
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/EditableListView;->invalidateViews()V

    .line 108
    return-void
.end method
