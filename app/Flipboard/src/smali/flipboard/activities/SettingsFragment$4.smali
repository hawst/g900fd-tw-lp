.class Lflipboard/activities/SettingsFragment$4;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$4;->b:Lflipboard/activities/SettingsFragment;

    iput-object p3, p0, Lflipboard/activities/SettingsFragment$4;->a:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 428
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 429
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$4;->b:Lflipboard/activities/SettingsFragment;

    const v2, 0x7f0d00a5

    invoke-virtual {v0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Flipboard"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 430
    const v0, 0x7f0d02f4

    invoke-virtual {v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 431
    const v0, 0x7f0d004a

    invoke-virtual {v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 433
    const v0, 0x7f0d00a3

    invoke-virtual {v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 434
    new-instance v0, Lflipboard/activities/SettingsFragment$4$1;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$4$1;-><init>(Lflipboard/activities/SettingsFragment$4;)V

    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 444
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$4;->b:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "sign_out"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 445
    return-void
.end method
