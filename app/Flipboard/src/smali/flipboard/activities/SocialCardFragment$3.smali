.class Lflipboard/activities/SocialCardFragment$3;
.super Ljava/lang/Object;
.source "SocialCardFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lflipboard/activities/SocialCardFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SocialCardFragment;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 198
    .line 200
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-boolean v0, v0, Lflipboard/activities/SocialCardFragment;->q:Z

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v0, v0, Lflipboard/activities/SocialCardFragment;->r:Lflipboard/gui/FLTextIntf;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 202
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v0, v0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    rsub-int v3, v0, 0x8c

    .line 203
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v0, v0, Lflipboard/activities/SocialCardFragment;->r:Lflipboard/gui/FLTextIntf;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 204
    if-ltz v3, :cond_0

    move v0, v1

    .line 205
    :goto_0
    iget-object v4, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v4, v4, Lflipboard/activities/SocialCardFragment;->r:Lflipboard/gui/FLTextIntf;

    iget-object v5, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    invoke-virtual {v5}, Lflipboard/activities/SocialCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-ltz v3, :cond_1

    const v3, 0x7f080084

    :goto_1
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v4, v3}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    .line 208
    :goto_2
    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v0, v0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 209
    :goto_3
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v0, v0, Lflipboard/activities/SocialCardFragment;->t:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 210
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment$3;->a:Lflipboard/activities/SocialCardFragment;

    iget-object v0, v0, Lflipboard/activities/SocialCardFragment;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 211
    return-void

    :cond_0
    move v0, v2

    .line 204
    goto :goto_0

    .line 205
    :cond_1
    const v3, 0x7f080070

    goto :goto_1

    :cond_2
    move v1, v2

    .line 208
    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method
