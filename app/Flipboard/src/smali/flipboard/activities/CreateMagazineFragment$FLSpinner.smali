.class public Lflipboard/activities/CreateMagazineFragment$FLSpinner;
.super Landroid/widget/Spinner;
.source "CreateMagazineFragment.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 598
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 602
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 603
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 620
    invoke-virtual {p0, p2}, Lflipboard/activities/CreateMagazineFragment$FLSpinner;->setSelection(I)V

    .line 621
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 622
    return-void
.end method

.method public performClick()Z
    .locals 3

    .prologue
    .line 607
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment$FLSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 611
    const v1, 0x7f0d029b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 612
    new-instance v1, Lflipboard/activities/CreateMagazineFragment$FLSpinner$DropDownAdapter;

    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment$FLSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/CreateMagazineFragment$FLSpinner$DropDownAdapter;-><init>(Lflipboard/activities/CreateMagazineFragment$FLSpinner;Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment$FLSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 615
    const/4 v0, 0x0

    return v0
.end method
