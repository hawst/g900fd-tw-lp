.class public Lflipboard/activities/LaunchActivity;
.super Landroid/support/v4/app/FlipboardFragmentActivity;
.source "LaunchActivity.java"


# instance fields
.field private n:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;-><init>()V

    .line 24
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/LaunchActivity;->n:Lflipboard/service/FlipboardManager;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 54
    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_0

    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    :goto_0
    return-object v0

    .line 57
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/TOCActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public static e()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 64
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "do_first_launch"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 66
    if-nez v0, :cond_0

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "do_first_launch_category"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 69
    :cond_0
    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 28
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/LaunchActivity;->n:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "always_allow_rotation"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lflipboard/activities/LaunchActivity;->setRequestedOrientation(I)V

    .line 31
    :goto_0
    invoke-virtual {p0}, Lflipboard/activities/LaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launch_from"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    const-string v1, "widget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {p0}, Lflipboard/activities/LaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, v3, v3}, Lflipboard/widget/FlipboardWidgetManager;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :cond_1
    invoke-static {}, Lflipboard/activities/LaunchActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 38
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 40
    invoke-virtual {p0, v0}, Lflipboard/activities/LaunchActivity;->startActivity(Landroid/content/Intent;)V

    .line 44
    :goto_1
    invoke-virtual {p0, v2, v2}, Lflipboard/activities/LaunchActivity;->overridePendingTransition(II)V

    .line 47
    invoke-virtual {p0}, Lflipboard/activities/LaunchActivity;->finish()V

    .line 48
    return-void

    .line 29
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/activities/LaunchActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 42
    :cond_3
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    invoke-static {p0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/LaunchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
