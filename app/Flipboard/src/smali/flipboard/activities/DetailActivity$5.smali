.class Lflipboard/activities/DetailActivity$5;
.super Ljava/lang/Object;
.source "DetailActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Ljava/lang/Object;",
        "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
        "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/DetailActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/DetailActivity;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 406
    check-cast p2, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-ne p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    invoke-static {v0}, Lflipboard/activities/DetailActivity;->a(Lflipboard/activities/DetailActivity;)Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentView()Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v1, :cond_1

    check-cast v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v1, p0, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    iget-object v1, v1, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v2, "DetailActivity:onCreate:flipObserver:notify"

    new-instance v3, Lflipboard/activities/DetailActivity$5$1;

    invoke-direct {v3, p0, p2, v0}, Lflipboard/activities/DetailActivity$5$1;-><init>(Lflipboard/activities/DetailActivity$5;Lflipboard/gui/flipping/FlipTransitionViews$Message;Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method
