.class Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DebugMenuActivity.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/DebugMenuActivity;


# direct methods
.method public constructor <init>(Lflipboard/activities/DebugMenuActivity;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 332
    iput-object p1, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->b:Lflipboard/activities/DebugMenuActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 333
    iput-object p2, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    .line 334
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 341
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 354
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 359
    .line 360
    if-nez p2, :cond_0

    .line 361
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->b:Lflipboard/activities/DebugMenuActivity;

    invoke-virtual {v0}, Lflipboard/activities/DebugMenuActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 363
    :cond_0
    const v0, 0x7f0a015e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 365
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 367
    const v0, 0x7f0a0160

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 368
    if-eqz v0, :cond_1

    .line 369
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 372
    :cond_1
    return-object p2
.end method
