.class Lflipboard/activities/SSOLoginActivity$1;
.super Landroid/webkit/WebViewClient;
.source "SSOLoginActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/activities/SSOLoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/SSOLoginActivity;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    iput-object p2, p0, Lflipboard/activities/SSOLoginActivity$1;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p3, p0, Lflipboard/activities/SSOLoginActivity$1;->b:Lflipboard/objs/ConfigService;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 160
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    iget-boolean v0, v0, Lflipboard/activities/SSOLoginActivity;->P:Z

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity$1;->a:Lflipboard/activities/FlipboardActivity;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 163
    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 147
    sget-object v0, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 148
    iget-object v0, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    iget-boolean v0, v0, Lflipboard/activities/SSOLoginActivity;->P:Z

    if-eqz v0, :cond_0

    .line 149
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, p2}, Lflipboard/io/NetworkManager;->b(Ljava/lang/String;)V

    .line 151
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 99
    iget-object v2, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    iget-boolean v2, v2, Lflipboard/activities/SSOLoginActivity;->P:Z

    if-nez v2, :cond_0

    .line 141
    :goto_0
    return v0

    .line 103
    :cond_0
    sget-object v2, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    new-array v2, v0, [Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 104
    iget-object v2, p0, Lflipboard/activities/SSOLoginActivity$1;->a:Lflipboard/activities/FlipboardActivity;

    const-string v3, "facebook"

    const-string v4, "web,"

    const-string v5, "FlipSSOLogin Cancelled"

    invoke-static {v2, v3, v4, v5}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v2, "flipboard://yoyo?json="

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 108
    :try_start_0
    new-instance v1, Lflipboard/json/JSONParser;

    const/16 v2, 0x16

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/HttpUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->y()Lflipboard/objs/UserInfo;

    move-result-object v1

    .line 109
    iget-boolean v2, v1, Lflipboard/objs/UserInfo;->b:Z

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    invoke-virtual {v1}, Lflipboard/objs/UserInfo;->a()Lflipboard/objs/UserInfo;

    move-result-object v1

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v4, Lflipboard/activities/SSOLoginActivity$2;

    invoke-direct {v4, v2}, Lflipboard/activities/SSOLoginActivity$2;-><init>(Lflipboard/activities/SSOLoginActivity;)V

    invoke-virtual {v3, v1, v4}, Lflipboard/service/FlipboardManager;->a(Lflipboard/objs/UserInfo;Lflipboard/util/Observer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 129
    :catch_0
    move-exception v1

    .line 130
    sget-object v2, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 112
    :cond_1
    :try_start_1
    iget v2, v1, Lflipboard/objs/UserInfo;->f:I

    const/16 v3, 0x44f

    if-ne v2, v3, :cond_2

    .line 113
    sget-object v2, Lflipboard/activities/SSOLoginActivity;->n:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/activities/SSOLoginActivity$1;->b:Lflipboard/objs/ConfigService;

    iget-object v4, v4, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    .line 114
    iget-object v1, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    invoke-virtual {v1}, Lflipboard/activities/SSOLoginActivity;->finish()V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v2, v1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 120
    iget-object v1, v1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    .line 126
    :goto_1
    iget-object v2, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    const v3, 0x7f0d0165

    invoke-virtual {v2, v3}, Lflipboard/activities/SSOLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 127
    iget-object v3, p0, Lflipboard/activities/SSOLoginActivity$1;->a:Lflipboard/activities/FlipboardActivity;

    const/4 v4, 0x1

    invoke-static {v3, v2, v1, v4}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 121
    :cond_3
    iget v1, v1, Lflipboard/objs/UserInfo;->f:I

    const/16 v2, 0x44e

    if-ne v1, v2, :cond_4

    .line 122
    iget-object v1, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    invoke-virtual {v1}, Lflipboard/activities/SSOLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0310

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 124
    :cond_4
    iget-object v1, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    invoke-virtual {v1}, Lflipboard/activities/SSOLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0164

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    .line 134
    :cond_5
    const-string v2, "market:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 136
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 137
    iget-object v2, p0, Lflipboard/activities/SSOLoginActivity$1;->c:Lflipboard/activities/SSOLoginActivity;

    invoke-virtual {v2, v1}, Lflipboard/activities/SSOLoginActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 141
    goto/16 :goto_0
.end method
