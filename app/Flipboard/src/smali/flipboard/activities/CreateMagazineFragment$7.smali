.class Lflipboard/activities/CreateMagazineFragment$7;
.super Ljava/lang/Object;
.source "CreateMagazineFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/activities/CreateMagazineFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateMagazineFragment;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lflipboard/activities/CreateMagazineFragment$7;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 475
    sget-object v0, Lflipboard/activities/CreateMagazineFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 478
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/activities/CreateMagazineFragment$7$1;

    invoke-direct {v2, p0}, Lflipboard/activities/CreateMagazineFragment$7$1;-><init>(Lflipboard/activities/CreateMagazineFragment$7;)V

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 496
    :try_start_0
    const-string v0, "magazine"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 497
    if-eqz v0, :cond_2

    .line 499
    new-instance v2, Lflipboard/json/JSONParser;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    .line 500
    invoke-virtual {v2}, Lflipboard/json/JSONParser;->p()Lflipboard/objs/Magazine;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 502
    :try_start_1
    iget-object v2, p0, Lflipboard/activities/CreateMagazineFragment$7;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v2}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->b(Lflipboard/objs/Magazine;)V

    .line 503
    iget-object v2, p0, Lflipboard/activities/CreateMagazineFragment$7;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v2}, Lflipboard/activities/CreateMagazineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 505
    if-eqz v2, :cond_0

    .line 506
    const-string v1, "flipboard.extra.navigating.from"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 508
    :cond_0
    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/objs/Magazine;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 515
    :goto_0
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$7;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->a(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 516
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/activities/CreateMagazineFragment$7$2;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/CreateMagazineFragment$7$2;-><init>(Lflipboard/activities/CreateMagazineFragment$7;Lflipboard/objs/Magazine;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 523
    :cond_1
    return-void

    .line 510
    :catch_0
    move-exception v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 511
    :goto_1
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 510
    :catch_1
    move-exception v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 527
    sget-object v0, Lflipboard/activities/CreateMagazineFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 529
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$7;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 530
    if-eqz v0, :cond_0

    .line 531
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$7;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    new-instance v2, Lflipboard/activities/CreateMagazineFragment$7$3;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/CreateMagazineFragment$7$3;-><init>(Lflipboard/activities/CreateMagazineFragment$7;Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 548
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 553
    sget-object v0, Lflipboard/activities/CreateMagazineFragment;->a:Lflipboard/util/Log;

    .line 554
    return-void
.end method
