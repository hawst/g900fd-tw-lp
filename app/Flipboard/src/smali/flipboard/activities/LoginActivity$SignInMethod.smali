.class public final enum Lflipboard/activities/LoginActivity$SignInMethod;
.super Ljava/lang/Enum;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/activities/LoginActivity$SignInMethod;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/activities/LoginActivity$SignInMethod;

.field public static final enum b:Lflipboard/activities/LoginActivity$SignInMethod;

.field public static final enum c:Lflipboard/activities/LoginActivity$SignInMethod;

.field private static final synthetic d:[Lflipboard/activities/LoginActivity$SignInMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lflipboard/activities/LoginActivity$SignInMethod;

    const-string v1, "flipboard"

    invoke-direct {v0, v1, v2}, Lflipboard/activities/LoginActivity$SignInMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->a:Lflipboard/activities/LoginActivity$SignInMethod;

    new-instance v0, Lflipboard/activities/LoginActivity$SignInMethod;

    const-string v1, "google"

    invoke-direct {v0, v1, v3}, Lflipboard/activities/LoginActivity$SignInMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->b:Lflipboard/activities/LoginActivity$SignInMethod;

    new-instance v0, Lflipboard/activities/LoginActivity$SignInMethod;

    const-string v1, "facebook"

    invoke-direct {v0, v1, v4}, Lflipboard/activities/LoginActivity$SignInMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->c:Lflipboard/activities/LoginActivity$SignInMethod;

    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/activities/LoginActivity$SignInMethod;

    sget-object v1, Lflipboard/activities/LoginActivity$SignInMethod;->a:Lflipboard/activities/LoginActivity$SignInMethod;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/activities/LoginActivity$SignInMethod;->b:Lflipboard/activities/LoginActivity$SignInMethod;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/activities/LoginActivity$SignInMethod;->c:Lflipboard/activities/LoginActivity$SignInMethod;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->d:[Lflipboard/activities/LoginActivity$SignInMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/activities/LoginActivity$SignInMethod;
    .locals 1

    .prologue
    .line 79
    const-class v0, Lflipboard/activities/LoginActivity$SignInMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/activities/LoginActivity$SignInMethod;

    return-object v0
.end method

.method public static values()[Lflipboard/activities/LoginActivity$SignInMethod;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->d:[Lflipboard/activities/LoginActivity$SignInMethod;

    invoke-virtual {v0}, [Lflipboard/activities/LoginActivity$SignInMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/activities/LoginActivity$SignInMethod;

    return-object v0
.end method
