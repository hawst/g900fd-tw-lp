.class Lflipboard/activities/MainActivity$1;
.super Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/MainActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/MainActivity;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lflipboard/activities/MainActivity$1;->a:Lflipboard/activities/MainActivity;

    invoke-direct {p0}, Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 97
    iget v0, p0, Lflipboard/activities/MainActivity$1;->b:I

    if-eq p1, v0, :cond_1

    iget v0, p0, Lflipboard/activities/MainActivity$1;->b:I

    if-ne v0, v1, :cond_1

    .line 99
    iget-object v0, p0, Lflipboard/activities/MainActivity$1;->a:Lflipboard/activities/MainActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lflipboard/activities/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 100
    iget-object v1, p0, Lflipboard/activities/MainActivity$1;->a:Lflipboard/activities/MainActivity;

    iget-object v1, v1, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 101
    iget-object v0, p0, Lflipboard/activities/MainActivity$1;->a:Lflipboard/activities/MainActivity;

    iget-object v0, v0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    iget v1, p0, Lflipboard/activities/MainActivity$1;->b:I

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/discovery/DiscoveryFragment;

    .line 102
    invoke-virtual {v0}, Lflipboard/gui/discovery/DiscoveryFragment;->b()V

    .line 109
    :cond_0
    :goto_0
    iput p1, p0, Lflipboard/activities/MainActivity$1;->b:I

    .line 110
    return-void

    .line 103
    :cond_1
    if-nez p1, :cond_2

    .line 104
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "homeFeedTabShown"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_2
    if-ne p1, v1, :cond_0

    .line 106
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "searchTabShown"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 107
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->a()V

    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-super {p0, p1}, Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;->a(I)V

    .line 84
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "enable_scrolling"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 85
    :goto_0
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lflipboard/activities/MainActivity$1;->a:Lflipboard/activities/MainActivity;

    iget-object v0, v0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionFragment;

    .line 87
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->onBackToTopClicked(Landroid/view/View;)V

    .line 89
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 84
    goto :goto_0
.end method
