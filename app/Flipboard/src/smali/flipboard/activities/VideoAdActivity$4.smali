.class Lflipboard/activities/VideoAdActivity$4;
.super Landroid/widget/MediaController;
.source "VideoAdActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/VideoAdActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/VideoAdActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lflipboard/activities/VideoAdActivity$4;->a:Lflipboard/activities/VideoAdActivity;

    invoke-direct {p0, p2}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public show()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity$4;->a:Lflipboard/activities/VideoAdActivity;

    .line 278
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    :try_start_0
    invoke-super {p0}, Landroid/widget/MediaController;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 285
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public show(I)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lflipboard/activities/VideoAdActivity$4;->a:Lflipboard/activities/VideoAdActivity;

    .line 262
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/MediaController;->show(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 269
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
