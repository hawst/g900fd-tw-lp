.class public Lflipboard/activities/FlipboardDreamSettings;
.super Lflipboard/activities/FlipboardActivity;
.source "FlipboardDreamSettings.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private n:Lflipboard/activities/FlipboardDreamSettings$Adapter;

.field private o:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

.field private final p:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 23
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 27
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    :goto_0
    iput-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->p:[I

    .line 103
    return-void

    .line 27
    :cond_0
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    goto :goto_0

    :array_0
    .array-data 4
        0x7f0d02b7
        0x7f0d02b4
        0x7f0d02b5
    .end array-data

    :array_1
    .array-data 4
        0x7f0d02b6
        0x7f0d02b4
        0x7f0d02b5
    .end array-data
.end method

.method static synthetic a(Lflipboard/activities/FlipboardDreamSettings;)Lflipboard/activities/FlipboardDreamSettings$UpdateType;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->o:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/FlipboardDreamSettings;Lflipboard/activities/FlipboardDreamSettings$UpdateType;)Lflipboard/activities/FlipboardDreamSettings$UpdateType;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lflipboard/activities/FlipboardDreamSettings;->o:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    return-object p1
.end method

.method static synthetic b(Lflipboard/activities/FlipboardDreamSettings;)Lflipboard/activities/FlipboardDreamSettings$Adapter;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->n:Lflipboard/activities/FlipboardDreamSettings$Adapter;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/FlipboardDreamSettings;)[I
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->p:[I

    return-object v0
.end method

.method public static j()Lflipboard/activities/FlipboardDreamSettings$UpdateType;
    .locals 3

    .prologue
    .line 33
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 34
    const-string v1, "daydream_fetch_items"

    sget-object v2, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->a:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->valueOf(Ljava/lang/String;)Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-static {}, Lflipboard/activities/FlipboardDreamSettings;->j()Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->o:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    .line 50
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardDreamSettings;->setContentView(I)V

    .line 52
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardDreamSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 53
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 55
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardDreamSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 56
    const v1, 0x7f0d02b2

    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardDreamSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 57
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardDreamSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 59
    new-instance v1, Lflipboard/activities/FlipboardDreamSettings$Adapter;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardDreamSettings$Adapter;-><init>(Lflipboard/activities/FlipboardDreamSettings;)V

    iput-object v1, p0, Lflipboard/activities/FlipboardDreamSettings;->n:Lflipboard/activities/FlipboardDreamSettings$Adapter;

    .line 60
    iget-object v1, p0, Lflipboard/activities/FlipboardDreamSettings;->n:Lflipboard/activities/FlipboardDreamSettings$Adapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 63
    iget-boolean v0, p0, Lflipboard/activities/FlipboardDreamSettings;->Z:Z

    if-nez v0, :cond_0

    .line 64
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->f:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 66
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->n:Lflipboard/activities/FlipboardDreamSettings$Adapter;

    invoke-virtual {v0, p3}, Lflipboard/activities/FlipboardDreamSettings$Adapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 80
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->p:[I

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    .line 81
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 82
    invoke-virtual {p0}, Lflipboard/activities/FlipboardDreamSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lflipboard/activities/FlipboardDreamSettings;->p:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 84
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings;->o:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->ordinal()I

    move-result v0

    .line 85
    invoke-virtual {v1, v2, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 86
    new-instance v0, Lflipboard/activities/FlipboardDreamSettings$1;

    invoke-direct {v0, p0}, Lflipboard/activities/FlipboardDreamSettings$1;-><init>(Lflipboard/activities/FlipboardDreamSettings;)V

    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 98
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "daydream_update"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
