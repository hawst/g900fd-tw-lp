.class Lflipboard/activities/SettingsDensityActivity$3;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsDensityActivity.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Lflipboard/activities/SettingsDensityActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsDensityActivity;ILflipboard/service/Section;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lflipboard/activities/SettingsDensityActivity$3;->d:Lflipboard/activities/SettingsDensityActivity;

    iput p2, p0, Lflipboard/activities/SettingsDensityActivity$3;->a:I

    iput-object p3, p0, Lflipboard/activities/SettingsDensityActivity$3;->b:Lflipboard/service/Section;

    iput-object p4, p0, Lflipboard/activities/SettingsDensityActivity$3;->c:Ljava/util/List;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 3

    .prologue
    .line 148
    invoke-super {p0, p1, p2}, Lflipboard/gui/dialog/FLDialogAdapter;->a(Landroid/support/v4/app/DialogFragment;I)V

    .line 149
    iget v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->a:I

    if-eq p2, v0, :cond_0

    .line 150
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->b:Lflipboard/service/Section;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->b:Lflipboard/service/Section;

    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lflipboard/objs/TOCSection;->O:Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->b:Lflipboard/service/Section;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->e(Z)V

    .line 153
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->b:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->u()V

    .line 158
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->d:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v0}, Lflipboard/activities/SettingsDensityActivity;->a(Lflipboard/activities/SettingsDensityActivity;)V

    .line 159
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->d:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v0}, Lflipboard/activities/SettingsDensityActivity;->b(Lflipboard/activities/SettingsDensityActivity;)Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->notifyDataSetChanged()V

    .line 160
    return-void

    .line 155
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$3;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lflipboard/service/User$44;

    invoke-direct {v2, v1, v0}, Lflipboard/service/User$44;-><init>(Lflipboard/service/User;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    goto :goto_0
.end method
