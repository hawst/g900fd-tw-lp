.class public Lflipboard/activities/ShareFragment$ServiceToggleButton;
.super Lflipboard/gui/FLImageView;
.source "ShareFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private f:Z

.field private g:Lflipboard/objs/ConfigService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/objs/ConfigService;Z)V
    .locals 1

    .prologue
    .line 1028
    invoke-direct {p0, p1}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;)V

    .line 1029
    invoke-virtual {p0, p0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1030
    iput-object p2, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->g:Lflipboard/objs/ConfigService;

    .line 1031
    if-eqz p3, :cond_0

    iget-object v0, p2, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setImage(Ljava/lang/String;)V

    .line 1032
    iput-boolean p3, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->f:Z

    .line 1033
    return-void

    .line 1031
    :cond_0
    iget-object v0, p2, Lflipboard/objs/ConfigService;->A:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1037
    iget-boolean v0, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->f:Z

    .line 1038
    iget-boolean v0, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->f:Z

    if-eqz v0, :cond_2

    .line 1039
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->a()V

    .line 1040
    iget-object v0, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->g:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setImage(Ljava/lang/String;)V

    .line 1041
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ShareActivity;

    iget-object v1, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->g:Lflipboard/objs/ConfigService;

    iget-object v2, v0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v2, v2, Lflipboard/activities/ShareFragment;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    iget-object v2, v0, Lflipboard/activities/ShareFragment;->i:Ljava/util/List;

    iget-object v0, v0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1048
    :cond_0
    :goto_1
    return-void

    .line 1037
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1043
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->a()V

    .line 1044
    iget-object v0, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->g:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->A:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->setImage(Ljava/lang/String;)V

    .line 1045
    invoke-virtual {p0}, Lflipboard/activities/ShareFragment$ServiceToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ShareActivity;

    iget-object v1, p0, Lflipboard/activities/ShareFragment$ServiceToggleButton;->g:Lflipboard/objs/ConfigService;

    iget-object v2, v0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    instance-of v2, v2, Lflipboard/activities/ShareFragment;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/activities/ShareActivity;->o:Lflipboard/activities/FlipboardFragment;

    check-cast v0, Lflipboard/activities/ShareFragment;

    iget-object v2, v0, Lflipboard/activities/ShareFragment;->i:Ljava/util/List;

    iget-object v0, v0, Lflipboard/activities/ShareFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method
