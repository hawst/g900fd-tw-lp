.class public Lflipboard/activities/MainActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "MainActivity.java"


# instance fields
.field n:Landroid/support/v4/view/ViewPager;

.field o:Lflipboard/gui/tabs/SlidingTabLayout;

.field public p:Lflipboard/gui/personal/TabPagerAdapter;

.field q:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 270
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 142
    const-string v0, "key_start_tab"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    .line 143
    if-eqz v0, :cond_0

    .line 144
    const-string v0, "key_start_tab"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 145
    if-eq v0, v1, :cond_0

    .line 146
    iget-object v1, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 150
    :cond_0
    const-string v0, "key_start_tab"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 151
    return-void
.end method


# virtual methods
.method public final A()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    .line 305
    instance-of v1, v0, Lflipboard/gui/section/SectionFragment;

    if-eqz v1, :cond_0

    .line 306
    check-cast v0, Lflipboard/gui/section/SectionFragment;

    .line 307
    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->o()Ljava/util/List;

    move-result-object v0

    .line 309
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->A()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 248
    :try_start_0
    const-string v0, "key_top_offset"

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/MainActivity;->q:Ljava/lang/Integer;

    .line 252
    :goto_0
    return-object v0

    .line 248
    :cond_0
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 250
    :catch_0
    move-exception v0

    move-object v0, p2

    goto :goto_0
.end method

.method public flipUiChanged(Lflipboard/gui/FlipFragment$FlipUiEvent;)V
    .locals 2
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 229
    iget-boolean v0, p1, Lflipboard/gui/FlipFragment$FlipUiEvent;->a:Z

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/MainActivity;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/MainActivity;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public final l()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBackToTopClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    .line 336
    instance-of v1, v0, Lflipboard/gui/section/SectionFragment;

    if-eqz v1, :cond_0

    .line 337
    check-cast v0, Lflipboard/gui/section/SectionFragment;

    .line 338
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->onBackToTopClicked(Landroid/view/View;)V

    .line 340
    :cond_0
    return-void
.end method

.method public onBrickClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v0

    .line 212
    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->e:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "synthetic/fullContentGuide"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ContentDrawerPhoneActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    const-string v1, "opened_from_discovery"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 216
    invoke-virtual {p0, v0}, Lflipboard/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    invoke-static {p0, v0, v1}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "do_first_launch"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0, v0}, Lflipboard/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 68
    invoke-virtual {p0}, Lflipboard/activities/MainActivity;->finish()V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const v0, 0x7f0300d9

    invoke-virtual {p0, v0}, Lflipboard/activities/MainActivity;->setContentView(I)V

    .line 73
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/app/Activity;)V

    .line 74
    invoke-virtual {p0}, Lflipboard/activities/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090122

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/MainActivity;->q:Ljava/lang/Integer;

    .line 76
    new-instance v0, Lflipboard/gui/personal/TabPagerAdapter;

    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-direct {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    .line 77
    iget-object v0, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 78
    iget-object v0, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 79
    iget-object v0, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    invoke-virtual {v0, v3, v1, v2}, Lflipboard/gui/tabs/SlidingTabLayout;->a(ILandroid/support/v4/view/ViewPager;Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;)V

    .line 80
    iget-object v0, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    new-instance v1, Lflipboard/activities/MainActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/MainActivity$1;-><init>(Lflipboard/activities/MainActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/tabs/SlidingTabLayout;->setOnPageChangeListener(Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;)V

    .line 116
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-virtual {v1, v3}, Lflipboard/gui/tabs/SlidingTabLayout;->a(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "homeTabHint"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 117
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lflipboard/gui/tabs/SlidingTabLayout;->a(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "searchTabHint"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 119
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "homeFeedTabShown"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 121
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/Flap$TypedResultObserver;)V

    .line 124
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "need_to_fetch_meta_data_for_sections"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "need_to_fetch_meta_data_for_sections"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 166
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const-string v1, "homeTabHint"

    invoke-virtual {v0, v1}, Lflipboard/service/HintManager;->a(Ljava/lang/String;)V

    .line 167
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const-string v1, "searchTabHint"

    invoke-virtual {v0, v1}, Lflipboard/service/HintManager;->a(Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    iget-object v1, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    .line 241
    invoke-virtual {v0, p2}, Lflipboard/activities/FlipboardFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 129
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 130
    invoke-direct {p0, p1}, Lflipboard/activities/MainActivity;->b(Landroid/content/Intent;)V

    .line 132
    const-string v0, "key_search_text"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const-string v0, "key_search_text"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    iget-object v0, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/discovery/DiscoveryFragment;

    invoke-virtual {v0, v1}, Lflipboard/gui/discovery/DiscoveryFragment;->a(Ljava/lang/String;)V

    .line 137
    const-string v0, "key_search_text"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 139
    :cond_0
    return-void
.end method

.method public onPageboxClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 282
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 283
    iget-object v0, p0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionFragment;->onPageboxClick(Landroid/view/View;)V

    .line 285
    :cond_0
    return-void
.end method

.method public onPageboxMoreClicked(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public onPriceTagButtonClicked(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 317
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 318
    if-eqz v0, :cond_0

    .line 319
    invoke-static {v0}, Lflipboard/usage/UsageTracker;->a(Lflipboard/objs/FeedItem;)V

    .line 320
    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v3

    .line 321
    iget-object v4, v0, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v4, :cond_1

    sget-object v4, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-eq v3, v4, :cond_1

    move v3, v1

    .line 324
    :goto_0
    if-nez v3, :cond_2

    .line 325
    :goto_1
    const-string v2, "auth/flipboard/coverstories"

    .line 326
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v3

    .line 327
    new-instance v4, Landroid/content/Intent;

    if-eqz v1, :cond_3

    const-class v0, Lflipboard/activities/DetailActivityStayOnRotation;

    :goto_2
    invoke-direct {v4, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    const-string v0, "sid"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string v0, "extra_current_item"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const/16 v0, 0x65

    invoke-virtual {p0, v4, v0}, Lflipboard/activities/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 332
    :cond_0
    return-void

    :cond_1
    move v3, v2

    .line 321
    goto :goto_0

    :cond_2
    move v1, v2

    .line 324
    goto :goto_1

    .line 327
    :cond_3
    const-class v0, Lflipboard/activities/DetailActivity;

    goto :goto_2
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 172
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 173
    iget-object v0, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 174
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->a()V

    .line 178
    :cond_0
    iget-object v0, p0, Lflipboard/activities/MainActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v1, "MainActivity:onResume"

    new-instance v2, Lflipboard/activities/MainActivity$2;

    invoke-direct {v2, p0}, Lflipboard/activities/MainActivity$2;-><init>(Lflipboard/activities/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 192
    iget-object v0, p0, Lflipboard/activities/MainActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->b(Landroid/app/Activity;)V

    .line 193
    invoke-virtual {p0}, Lflipboard/activities/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/MainActivity;->b(Landroid/content/Intent;)V

    .line 194
    return-void
.end method

.method protected final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    return-object v0
.end method

.method public toggleTabVisibility(Lflipboard/activities/MainActivity$TranslateContentViewYEvent;)V
    .locals 4
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const v1, 0x1020002

    .line 257
    iget-boolean v0, p1, Lflipboard/activities/MainActivity$TranslateContentViewYEvent;->a:Z

    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_0

    .line 260
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p1, Lflipboard/activities/MainActivity$TranslateContentViewYEvent;->b:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 265
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p1, Lflipboard/activities/MainActivity$TranslateContentViewYEvent;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public final u_()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method
