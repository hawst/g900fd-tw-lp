.class Lflipboard/activities/SettingsFragment$47;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Ljava/util/HashSet;

.field final synthetic c:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;[Ljava/lang/String;Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 1770
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$47;->a:[Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/SettingsFragment$47;->b:Ljava/util/HashSet;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 3

    .prologue
    .line 1774
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "server_baseurl"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$47;->a:[Ljava/lang/String;

    aget-object v2, v2, p2

    .line 1775
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "server_base_urls"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$47;->b:Ljava/util/HashSet;

    .line 1776
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "server_base_urls_size"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$47;->b:Ljava/util/HashSet;

    .line 1778
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1779
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1780
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SettingsFragment;->n:Z

    .line 1781
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1782
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 1786
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 1787
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x75

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1788
    return-void
.end method

.method public final e(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 1792
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "server_baseurl"

    sget-object v2, Lflipboard/activities/SettingsFragment;->c:Ljava/lang/String;

    .line 1793
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "server_base_urls"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1794
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "server_base_urls_size"

    const/4 v2, 0x0

    .line 1796
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1797
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1798
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SettingsFragment;->n:Z

    .line 1799
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1800
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 1801
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$47;->c:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1802
    return-void
.end method
