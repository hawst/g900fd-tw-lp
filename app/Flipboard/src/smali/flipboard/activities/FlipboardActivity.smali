.class public abstract Lflipboard/activities/FlipboardActivity;
.super Landroid/support/v4/app/FlipboardFragmentActivity;
.source "FlipboardActivity.java"


# static fields
.field static H:J

.field public static final J:Lflipboard/util/Log;

.field public static final K:Lflipboard/util/Log;

.field public static final L:Lflipboard/util/Log;

.field static final synthetic ab:Z

.field private static n:Z

.field private static o:Z

.field private static w:Z

.field private static final x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lflipboard/activities/FlipboardActivity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Lflipboard/util/TouchInfo;

.field protected I:Z

.field public final M:Lflipboard/service/FlipboardManager;

.field public final N:Landroid/content/SharedPreferences;

.field public O:Lflipboard/objs/FeedItem;

.field public P:Z

.field public Q:Z

.field R:J

.field S:Z

.field T:J

.field U:J

.field protected V:J

.field protected W:Z

.field public X:Lflipboard/gui/flipping/FlipTransitionBase;

.field Y:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected Z:Z

.field public final aa:Lcom/squareup/otto/Bus;

.field private final p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/BroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lflipboard/activities/FlipboardActivity$ActivityResultListener;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lflipboard/gui/FLToast;

.field private y:F

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/FlipboardActivity$OnBackPressedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    const-class v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/activities/FlipboardActivity;->ab:Z

    .line 108
    const-string v0, "usage"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/FlipboardActivity;->J:Lflipboard/util/Log;

    .line 109
    const-string v0, "activities"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    .line 112
    sput-boolean v1, Lflipboard/activities/FlipboardActivity;->n:Z

    const/4 v0, 0x0

    sput-object v0, Lflipboard/activities/FlipboardActivity;->L:Lflipboard/util/Log;

    .line 114
    sput-boolean v1, Lflipboard/activities/FlipboardActivity;->o:Z

    .line 153
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lflipboard/activities/FlipboardActivity;->x:Ljava/util/Set;

    return-void

    :cond_0
    move v0, v1

    .line 85
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;-><init>()V

    .line 116
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    .line 117
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    .line 140
    iput v1, p0, Lflipboard/activities/FlipboardActivity;->p:I

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    .line 144
    iput-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->s:Z

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->t:Ljava/util/List;

    .line 159
    new-instance v0, Lcom/squareup/otto/Bus;

    invoke-direct {v0}, Lcom/squareup/otto/Bus;-><init>()V

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->z:Ljava/util/List;

    return-void
.end method

.method public static E()V
    .locals 0

    .prologue
    .line 1586
    return-void
.end method

.method static synthetic G()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/activities/FlipboardActivity;->w:Z

    return v0
.end method

.method static synthetic H()Z
    .locals 1

    .prologue
    .line 85
    sget-boolean v0, Lflipboard/activities/FlipboardActivity;->o:Z

    return v0
.end method

.method public static a(II)Lflipboard/gui/dialog/FLAlertDialogFragment;
    .locals 2

    .prologue
    .line 1577
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 1578
    invoke-virtual {v0, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1579
    invoke-virtual {v0, p1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 1580
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 1581
    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lflipboard/util/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lflipboard/activities/FlipboardActivity;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lflipboard/util/Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 217
    sget-object v1, Lflipboard/activities/FlipboardActivity;->x:Ljava/util/Set;

    monitor-enter v1

    .line 218
    :try_start_0
    sget-object v0, Lflipboard/activities/FlipboardActivity;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 219
    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220
    invoke-interface {p1, v0}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static u()I
    .locals 3

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    sget-object v1, Lflipboard/activities/FlipboardActivity;->x:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 230
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 233
    goto :goto_0

    .line 234
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public A()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1002
    const/4 v0, 0x0

    return-object v0
.end method

.method public final B()Lflipboard/gui/FLToast;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1435
    sget-boolean v0, Lflipboard/activities/FlipboardActivity;->ab:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1436
    :cond_0
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->v:Lflipboard/gui/FLToast;

    if-eqz v0, :cond_1

    .line 1438
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->v:Lflipboard/gui/FLToast;

    iget-object v1, v0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {v0, v3}, Lflipboard/gui/FLToast;->a(I)V

    invoke-virtual {v0, v3}, Lflipboard/gui/FLToast;->setText(I)V

    .line 1442
    :goto_0
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->v:Lflipboard/gui/FLToast;

    return-object v0

    .line 1440
    :cond_1
    new-instance v0, Lflipboard/gui/FLToast;

    invoke-direct {v0, p0}, Lflipboard/gui/FLToast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->v:Lflipboard/gui/FLToast;

    goto :goto_0
.end method

.method protected C()V
    .locals 5

    .prologue
    .line 1491
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "onNetworkReconnect is not handled by %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1492
    return-void
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 1567
    const/4 v0, 0x0

    return v0
.end method

.method protected F()V
    .locals 2

    .prologue
    .line 1661
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->O:Lflipboard/service/PushServiceManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/PushServiceManager;->a(Lflipboard/service/User;)V

    .line 1662
    return-void
.end method

.method public final a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 1448
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 1451
    :try_start_0
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1452
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1458
    :goto_0
    return-object v0

    .line 1454
    :catch_0
    move-exception v0

    .line 1455
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 1458
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1677
    return-object p2
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 459
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->a()V

    .line 460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->Q:Z

    .line 461
    return-void
.end method

.method protected final a(I)V
    .locals 4

    .prologue
    .line 512
    sget-boolean v0, Lflipboard/activities/FlipboardActivity;->w:Z

    if-eqz v0, :cond_0

    .line 597
    :goto_0
    return-void

    .line 515
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lflipboard/activities/FlipboardActivity;->w:Z

    .line 520
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    int-to-long v2, p1

    new-instance v1, Lflipboard/activities/FlipboardActivity$5;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$5;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 1463
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 1466
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1471
    :cond_0
    :goto_0
    return-void

    .line 1467
    :catch_0
    move-exception v0

    .line 1468
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1476
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 1479
    :try_start_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1484
    :cond_0
    :goto_0
    return-void

    .line 1480
    :catch_0
    move-exception v0

    .line 1481
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1509
    invoke-virtual {p0, p1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 1510
    const v0, 0x7f040011

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 1511
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 1512
    return-void
.end method

.method public final a(Landroid/content/Intent;ILflipboard/activities/FlipboardActivity$ActivityResultListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 893
    invoke-static {p0, p1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 894
    const v0, 0x7f0d001d

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 917
    :goto_0
    return-void

    .line 898
    :cond_0
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 899
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 902
    :cond_1
    if-eqz p3, :cond_4

    .line 904
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->u:Ljava/util/Map;

    if-nez v0, :cond_3

    .line 905
    monitor-enter p0

    .line 906
    :try_start_0
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->u:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 907
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->u:Ljava/util/Map;

    .line 909
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 912
    :cond_3
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->u:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    :cond_4
    iput-boolean v2, p0, Lflipboard/activities/FlipboardActivity;->q:Z

    .line 916
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FlipboardFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 909
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardActivity:registerOnBackPressedListener"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    return-void
.end method

.method public a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1339
    .line 1341
    if-eqz p2, :cond_0

    .line 1342
    invoke-static {p2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1343
    iget-object v0, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 1345
    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/service/Section;Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method protected a(Lflipboard/service/Section;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1351
    :try_start_0
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 1353
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1354
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v2, 0x2f

    if-eq v0, v2, :cond_0

    .line 1355
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1357
    :cond_0
    const-string v0, "report_bug_image.jpg"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1358
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1359
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1360
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 1363
    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    if-nez v1, :cond_1

    .line 1364
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v1

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v2

    invoke-static {v1, v2}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(II)V

    .line 1366
    :cond_1
    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    const v2, 0x1020002

    invoke-virtual {p0, v2}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v1

    .line 1367
    if-eqz v1, :cond_2

    .line 1368
    iget-object v2, v1, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-static {v2, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 1369
    sget-object v2, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v2, v1}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 1372
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lflipboard/activities/ReportIssueActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1373
    if-eqz v1, :cond_3

    .line 1374
    sget-object v1, Lflipboard/activities/ReportIssueActivity;->n:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1377
    :cond_3
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->s_()Ljava/lang/String;

    move-result-object v0

    .line 1383
    if-eqz v0, :cond_4

    .line 1384
    const-string v1, "flipmag_partner_id"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1387
    :cond_4
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->p()Ljava/lang/String;

    move-result-object v0

    .line 1388
    if-eqz v0, :cond_5

    .line 1389
    const-string v1, "feed_id"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1392
    :cond_5
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->t_()Ljava/lang/String;

    move-result-object v0

    .line 1393
    if-eqz v0, :cond_6

    .line 1394
    const-string v1, "aml_url"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1397
    :cond_6
    if-eqz p1, :cond_7

    .line 1398
    const-string v0, "feed_data"

    invoke-virtual {p1}, Lflipboard/service/Section;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1401
    :cond_7
    if-eqz p2, :cond_8

    .line 1402
    const-string v0, "item_data"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1405
    :cond_8
    if-eqz p3, :cond_9

    .line 1406
    const-string v0, "source_url"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1409
    :cond_9
    invoke-virtual {p0, v2}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1413
    :goto_0
    return-void

    .line 1410
    :catch_0
    move-exception v0

    .line 1411
    sget-object v1, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Lflipboard/service/Section;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Section;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1328
    if-nez p2, :cond_0

    .line 1329
    invoke-virtual {p0, p1, v2, v2}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/service/Section;Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    :goto_0
    return-void

    .line 1330
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1331
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {p0, p1, v0}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto :goto_0

    .line 1333
    :cond_1
    invoke-static {p2}, Lflipboard/json/JSONSerializer;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v2}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/service/Section;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1502
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1503
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1504
    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/Intent;)V

    .line 1505
    return-void
.end method

.method public final b(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardActivity:unregisterOnBackPressedItem"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 213
    return-void
.end method

.method final d(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    .line 669
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 670
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "warned_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 671
    cmp-long v1, v4, v8

    if-lez v1, :cond_1

    const-wide/32 v6, 0x240c8400

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 679
    :cond_0
    :goto_0
    return v0

    .line 674
    :cond_1
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    const-string v4, "warned_last"

    invoke-interface {v1, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 675
    cmp-long v1, v4, v8

    if-lez v1, :cond_2

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 678
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "warned_last"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "warned_"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 679
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1276
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 1277
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_7

    .line 1278
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    if-eqz v1, :cond_7

    .line 1279
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    iget-boolean v3, v1, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    .line 1280
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionBase;->getLastFlipTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 1281
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-lez v1, :cond_3

    const-wide/16 v6, 0x320

    cmp-long v1, v4, v6

    if-gez v1, :cond_3

    move v1, v0

    .line 1282
    :goto_0
    const/16 v4, 0x16

    if-ne v2, v4, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    const/16 v4, 0x14

    if-ne v2, v4, :cond_4

    if-eqz v3, :cond_4

    .line 1283
    :cond_1
    if-nez v1, :cond_2

    .line 1284
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionBase;->i()V

    .line 1297
    :cond_2
    :goto_1
    return v0

    .line 1281
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1287
    :cond_4
    const/16 v4, 0x15

    if-ne v2, v4, :cond_5

    if-eqz v3, :cond_6

    :cond_5
    const/16 v4, 0x13

    if-ne v2, v4, :cond_7

    if-eqz v3, :cond_7

    .line 1288
    :cond_6
    if-nez v1, :cond_2

    .line 1289
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionBase;->j()V

    goto :goto_1

    .line 1297
    :cond_7
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1191
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    if-nez v0, :cond_1

    .line 1193
    iput-object v6, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    .line 1194
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v6, v0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    .line 1195
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 1259
    :cond_0
    :goto_0
    return v1

    .line 1198
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v3, v0, 0xff

    .line 1199
    if-nez v3, :cond_4

    .line 1200
    new-instance v0, Lflipboard/util/TouchInfo;

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v4}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v4

    iget-boolean v5, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    invoke-direct {v0, p1, v4, v5}, Lflipboard/util/TouchInfo;-><init>(Landroid/view/MotionEvent;ZZ)V

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    .line 1207
    :cond_2
    :goto_1
    if-ne v3, v1, :cond_3

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    iget v0, v0, Lflipboard/util/TouchInfo;->o:I

    if-lt v0, v10, :cond_3

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->D()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212
    :cond_3
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->q:Z

    if-eqz v0, :cond_5

    move v1, v2

    .line 1213
    goto :goto_0

    .line 1201
    :cond_4
    const/4 v0, 0x2

    if-ne v3, v0, :cond_2

    .line 1202
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    if-eqz v0, :cond_2

    .line 1203
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    iput v4, v0, Lflipboard/util/TouchInfo;->o:I

    goto :goto_1

    .line 1216
    :cond_5
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-eq v0, v1, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    if-gt v0, v1, :cond_8

    :cond_6
    move v0, v1

    .line 1221
    :goto_2
    if-eqz v0, :cond_7

    .line 1223
    packed-switch v3, :pswitch_data_0

    .line 1259
    :cond_7
    :goto_3
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v2

    .line 1216
    goto :goto_2

    .line 1225
    :pswitch_0
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    if-eqz v0, :cond_7

    .line 1226
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    iget-boolean v0, v0, Lflipboard/util/TouchInfo;->i:Z

    if-nez v0, :cond_0

    .line 1230
    iget-object v3, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    iget-boolean v0, v3, Lflipboard/util/TouchInfo;->h:Z

    if-nez v0, :cond_9

    iget v4, v3, Lflipboard/util/TouchInfo;->m:F

    iget v5, v3, Lflipboard/util/TouchInfo;->n:F

    iget-boolean v6, v3, Lflipboard/util/TouchInfo;->j:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v1, :cond_a

    move v0, v1

    :goto_4
    or-int/2addr v0, v6

    iput-boolean v0, v3, Lflipboard/util/TouchInfo;->j:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, v3, Lflipboard/util/TouchInfo;->m:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    iput v6, v3, Lflipboard/util/TouchInfo;->n:F

    iget v7, v3, Lflipboard/util/TouchInfo;->l:F

    sub-float v5, v6, v5

    add-float/2addr v5, v7

    iput v5, v3, Lflipboard/util/TouchInfo;->l:F

    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v5

    if-eqz v5, :cond_b

    iget v5, v3, Lflipboard/util/TouchInfo;->k:F

    sub-float v0, v4, v0

    add-float/2addr v0, v5

    iput v0, v3, Lflipboard/util/TouchInfo;->k:F

    :goto_5
    iget-boolean v0, v3, Lflipboard/util/TouchInfo;->f:Z

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-gt v0, v1, :cond_9

    iget v0, v3, Lflipboard/util/TouchInfo;->l:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v4, Lflipboard/util/TouchInfo;->a:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_c

    iput-boolean v1, v3, Lflipboard/util/TouchInfo;->h:Z

    :cond_9
    :goto_6
    move v0, v2

    :goto_7
    if-eqz v0, :cond_7

    .line 1232
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    if-eqz v0, :cond_7

    .line 1233
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->u_()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1237
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1238
    invoke-virtual {v0, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1239
    invoke-super {p0, v0}, Landroid/support/v4/app/FlipboardFragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 1230
    goto :goto_4

    :cond_b
    iget v5, v3, Lflipboard/util/TouchInfo;->k:F

    sub-float/2addr v0, v4

    add-float/2addr v0, v5

    iput v0, v3, Lflipboard/util/TouchInfo;->k:F

    goto :goto_5

    :cond_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, v3, Lflipboard/util/TouchInfo;->d:J

    sub-long/2addr v4, v6

    iget v0, v3, Lflipboard/util/TouchInfo;->k:F

    sget v6, Lflipboard/util/TouchInfo;->b:I

    int-to-float v6, v6

    cmpg-float v0, v0, v6

    if-ltz v0, :cond_9

    sget v0, Lflipboard/util/TouchInfo;->c:F

    iget v6, v3, Lflipboard/util/TouchInfo;->k:F

    mul-float/2addr v0, v6

    float-to-double v6, v0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v6, v8

    double-to-int v0, v6

    const/high16 v6, 0x447a0000    # 1000.0f

    int-to-float v0, v0

    long-to-float v7, v4

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v6

    float-to-int v0, v0

    iget-boolean v6, v3, Lflipboard/util/TouchInfo;->g:Z

    if-eqz v6, :cond_d

    const/16 v6, 0x352

    if-lt v0, v6, :cond_d

    iput-boolean v1, v3, Lflipboard/util/TouchInfo;->i:Z

    move v0, v1

    goto :goto_7

    :cond_d
    const-wide/16 v6, 0x1f4

    cmp-long v0, v4, v6

    if-ltz v0, :cond_9

    iput-boolean v1, v3, Lflipboard/util/TouchInfo;->h:Z

    goto :goto_6

    .line 1243
    :cond_e
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    iput-boolean v2, v0, Lflipboard/util/TouchInfo;->i:Z

    goto/16 :goto_3

    .line 1250
    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    iget-boolean v0, v0, Lflipboard/util/TouchInfo;->i:Z

    if-nez v0, :cond_0

    .line 1254
    :cond_f
    iput-object v6, p0, Lflipboard/activities/FlipboardActivity;->A:Lflipboard/util/TouchInfo;

    goto/16 :goto_3

    .line 1223
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public e()V
    .locals 2

    .prologue
    .line 1549
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->Z:Z

    if-nez v0, :cond_0

    .line 1550
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 1551
    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-eq v0, v1, :cond_0

    .line 1552
    const-class v0, Lflipboard/activities/TOCActivity;

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->a(Ljava/lang/Class;)V

    .line 1559
    :goto_0
    return-void

    .line 1557
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected f()V
    .locals 0

    .prologue
    .line 979
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 802
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->S:Z

    .line 803
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v0, "finish"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 804
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->finish()V

    .line 805
    return-void
.end method

.method public g()Lflipboard/gui/flipping/FlippingBitmap;
    .locals 1

    .prologue
    .line 1270
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1610
    const/4 v0, 0x0

    return-object v0
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 699
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "always_allow_rotation"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 702
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->setRequestedOrientation(I)V

    .line 706
    :goto_0
    return-void

    .line 704
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public l()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 992
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    return-object v0
.end method

.method protected n()V
    .locals 0

    .prologue
    .line 503
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 930
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1

    .line 931
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FlipboardFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 933
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->u:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->u:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity$ActivityResultListener;

    .line 935
    if-eqz v0, :cond_0

    .line 936
    invoke-interface {v0, p1, p2}, Lflipboard/activities/FlipboardActivity$ActivityResultListener;->a(II)V

    .line 939
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 831
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-nez v0, :cond_1

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 834
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 835
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 840
    :cond_2
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->d(Landroid/app/Activity;)V

    .line 842
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 843
    const/4 v0, 0x0

    .line 844
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez v0, :cond_3

    .line 845
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity$OnBackPressedListener;

    invoke-interface {v0}, Lflipboard/activities/FlipboardActivity$OnBackPressedListener;->c()Z

    move-result v0

    .line 846
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 848
    :cond_3
    if-nez v0, :cond_0

    .line 849
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onBackPressed()V

    .line 850
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->f()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 239
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory2()Landroid/view/LayoutInflater$Factory2;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v2, v1, Lflipboard/app/FlipboardApplication;->n:Lflipboard/util/FixSamsungCrashFactory;

    if-nez v2, :cond_0

    new-instance v2, Lflipboard/util/FixSamsungCrashFactory;

    invoke-direct {v2}, Lflipboard/util/FixSamsungCrashFactory;-><init>()V

    iput-object v2, v1, Lflipboard/app/FlipboardApplication;->n:Lflipboard/util/FixSamsungCrashFactory;

    const-string v2, "unwanted.workaround_for_samsung_411_crash"

    invoke-static {v2}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    :cond_0
    iget-object v1, v1, Lflipboard/app/FlipboardApplication;->n:Lflipboard/util/FixSamsungCrashFactory;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->setFactory2(Landroid/view/LayoutInflater$Factory2;)V

    .line 240
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 242
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 243
    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v0, p0, Lflipboard/activities/FlipboardActivity;->y:F

    .line 245
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launched_by_flipboard_activity"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->Z:Z

    .line 246
    sget-object v0, Lflipboard/activities/FlipboardActivity;->x:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 248
    const-string v0, "flipboard.app.QUIT"

    new-instance v1, Lflipboard/activities/FlipboardActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$1;-><init>(Lflipboard/activities/FlipboardActivity;)V

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lflipboard/activities/FlipboardActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->t:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 258
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    const v1, 0x7f0d00c1

    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 261
    sget-boolean v0, Lflipboard/activities/FlipboardActivity;->n:Z

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/activities/FlipboardActivity;->L:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_2

    .line 262
    invoke-static {p0}, Lcom/android/debug/hv/ViewServer;->a(Landroid/content/Context;)Lcom/android/debug/hv/ViewServer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/debug/hv/ViewServer;->a(Landroid/app/Activity;)V

    .line 265
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "data_use"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "data_use"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    const v1, 0x7f0d00c8

    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    const v1, 0x7f030066

    iput v1, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->r:I

    iput-boolean v3, v0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    new-instance v1, Lflipboard/activities/FlipboardActivity$9;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$9;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 266
    :cond_3
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->a(Ljava/lang/Object;)V

    .line 269
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_4

    .line 270
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 271
    const-string v0, "c197906b3ad1a9906b5182ba0a2c3851"

    invoke-static {p0, v0}, Lnet/hockeyapp/android/UpdateManager;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 273
    :cond_4
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1044
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1045
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lflipboard/activities/MediaButtonReceiver;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 1048
    if-nez p1, :cond_1

    .line 1049
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-nez v0, :cond_0

    .line 1181
    :goto_0
    return-object v1

    .line 1053
    :cond_0
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const-string v2, "Dump memory to sd"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Dump Views"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Dump Bitmaps"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Garbage Collect"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Dump Downloads"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Dump Network"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Purge Bitmaps"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Purge Downloads"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Test Search"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Dump Sections"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Dump Usage"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Purge Usage"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Upload Usage"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Dump gl"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Fake New TOC Items"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Clear Watched Files"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Dump SharedPrefs"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Sync Down User State"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Delete All Section Items"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Debugger Breakpoint"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Refresh One Section"

    aput-object v2, v0, v1

    .line 1055
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v1, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1056
    const-string v2, "Select debug option"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1057
    new-instance v2, Lflipboard/activities/FlipboardActivity$8;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/FlipboardActivity$8;-><init>(Lflipboard/activities/FlipboardActivity;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1175
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 1181
    goto/16 :goto_0

    .line 1176
    :cond_1
    const v0, 0x7f0a0007

    if-ne p1, v0, :cond_2

    .line 1177
    const-string v0, "android.intent.extra.TITLE"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1178
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-direct {v2, p0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d0236

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d023f

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1179
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 771
    sget-object v0, Lflipboard/activities/FlipboardActivity;->x:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 773
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->q()V

    .line 775
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    .line 779
    :cond_0
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onDestroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    :goto_0
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lflipboard/activities/FlipboardActivity;->V:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 790
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_1

    .line 780
    :catch_0
    move-exception v0

    .line 781
    sget-object v1, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 783
    :try_start_1
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 784
    :catch_1
    move-exception v0

    .line 785
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 790
    :cond_1
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 793
    sget-boolean v0, Lflipboard/activities/FlipboardActivity;->n:Z

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/activities/FlipboardActivity;->L:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_2

    .line 794
    invoke-static {p0}, Lcom/android/debug/hv/ViewServer;->a(Landroid/content/Context;)Lcom/android/debug/hv/ViewServer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/debug/hv/ViewServer;->b(Landroid/app/Activity;)V

    .line 796
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->b(Ljava/lang/Object;)V

    .line 797
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 809
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onDetachedFromWindow()V

    .line 810
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->setContentView(Landroid/view/View;)V

    .line 811
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v0, 0x1

    .line 1303
    const/16 v1, 0x19

    if-ne p1, v1, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 1305
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1306
    iget-wide v4, p0, Lflipboard/activities/FlipboardActivity;->T:J

    sub-long v4, v2, v4

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v1, :cond_0

    .line 1307
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->q()Lflipboard/service/Section;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->A()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/service/Section;Ljava/util/List;)V

    .line 1323
    :goto_0
    return v0

    .line 1310
    :cond_0
    iput-wide v2, p0, Lflipboard/activities/FlipboardActivity;->T:J

    .line 1323
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FlipboardFragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1313
    :cond_2
    const/16 v1, 0x18

    if-ne p1, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 1315
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1316
    iget-wide v4, p0, Lflipboard/activities/FlipboardActivity;->U:J

    sub-long v4, v2, v4

    cmp-long v1, v4, v6

    if-gez v1, :cond_3

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_3

    instance-of v1, p0, Lflipboard/activities/SettingsActivity;

    if-nez v1, :cond_3

    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    const-string v4, "enable_volume_up_to_settings"

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1317
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/SettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1320
    :cond_3
    iput-wide v2, p0, Lflipboard/activities/FlipboardActivity;->U:J

    goto :goto_1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1516
    const v0, 0x102002c

    if-ne p1, v0, :cond_0

    .line 1517
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->e()V

    move v0, v1

    .line 1536
    :goto_0
    return v0

    .line 1520
    :cond_0
    instance-of v0, p2, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 1521
    check-cast v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v3, v0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    move v0, v1

    .line 1522
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1521
    goto :goto_1

    .line 1525
    :cond_2
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03a0

    if-ne v0, v1, :cond_4

    .line 1526
    invoke-virtual {p0, v2}, Lflipboard/activities/FlipboardActivity;->showDialog(I)V

    .line 1536
    :cond_3
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FlipboardFragmentActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 1527
    :cond_4
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a039f

    if-ne v0, v1, :cond_5

    .line 1528
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->q()Lflipboard/service/Section;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->A()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/service/Section;Ljava/util/List;)V

    goto :goto_2

    .line 1529
    :cond_5
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a03a1

    if-ne v0, v1, :cond_6

    .line 1530
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 1531
    :cond_6
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x6fca3

    if-ne v0, v1, :cond_7

    .line 1532
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->b(Landroid/app/Activity;)V

    goto :goto_2

    .line 1533
    :cond_7
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a000d

    if-ne v0, v1, :cond_3

    .line 1534
    invoke-static {p0}, Lflipboard/gui/personal/NotificationsFragment;->a(Landroid/app/Activity;)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1541
    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 726
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lflipboard/activities/FlipboardActivity;->H:J

    .line 728
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->e()V

    .line 732
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onPause()V

    .line 733
    iput-boolean v4, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    .line 734
    iput-boolean v4, p0, Lflipboard/activities/FlipboardActivity;->Q:Z

    .line 736
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x3e8

    new-instance v1, Lflipboard/activities/FlipboardActivity$7;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$7;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 744
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/activities/FlipboardActivity;->R:J

    sub-long/2addr v0, v2

    .line 745
    sget-object v2, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 746
    iget-wide v2, p0, Lflipboard/activities/FlipboardActivity;->V:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/activities/FlipboardActivity;->V:J

    .line 748
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->Y:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 749
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->Y:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->b(Lflipboard/util/Observer;)V

    .line 751
    :cond_1
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 983
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 984
    if-eqz v0, :cond_0

    .line 985
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->a()V

    .line 987
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 494
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 495
    iget-wide v0, p0, Lflipboard/activities/FlipboardActivity;->V:J

    const-string v2, "state_active_time"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/activities/FlipboardActivity;->V:J

    .line 496
    return-void
.end method

.method protected onResume()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 372
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->b(Lflipboard/activities/FlipboardActivity;)V

    .line 374
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onResume()V

    .line 376
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->aa:Lnet/hockeyapp/android/CrashManagerListener;

    if-nez v1, :cond_0

    new-instance v1, Lflipboard/service/FlCrashListener;

    invoke-direct {v1}, Lflipboard/service/FlCrashListener;-><init>()V

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->aa:Lnet/hockeyapp/android/CrashManagerListener;

    :cond_0
    iget-object v1, v0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const-string v2, "c197906b3ad1a9906b5182ba0a2c3851"

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aa:Lnet/hockeyapp/android/CrashManagerListener;

    invoke-static {v1, v2, v0}, Lnet/hockeyapp/android/CrashManager;->a(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 379
    sget-wide v0, Lflipboard/activities/FlipboardActivity;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lflipboard/activities/FlipboardActivity;->H:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget v1, v1, Lflipboard/model/ConfigSetting;->UsageSessionRefreshInterval:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 380
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->v()V

    .line 381
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "lock"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "action"

    const-string v2, "inactive"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lflipboard/activities/FlipboardActivity;->H:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 382
    sget-object v0, Lflipboard/activities/FlipboardActivity;->J:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_1

    .line 383
    sget-object v0, Lflipboard/activities/FlipboardActivity;->J:Lflipboard/util/Log;

    .line 385
    :cond_1
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Lflipboard/io/UsageManager;->b()V

    .line 387
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lflipboard/activities/FlipboardActivity;->H:J

    .line 389
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->Y:Lflipboard/util/Observer;

    if-eqz v0, :cond_3

    .line 390
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->Y:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->a(Lflipboard/util/Observer;)V

    .line 394
    :cond_3
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    if-eqz v0, :cond_4

    .line 395
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->o()V

    .line 397
    :cond_4
    iput-boolean v7, p0, Lflipboard/activities/FlipboardActivity;->q:Z

    .line 399
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    .line 400
    iput-boolean v6, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    .line 401
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/FlipboardActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$3;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v8, v9, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 411
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->i()V

    .line 413
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/activities/FlipboardActivity;->R:J

    .line 415
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->z()V

    .line 418
    sget-boolean v0, Lflipboard/activities/FlipboardActivity;->n:Z

    if-eqz v0, :cond_5

    sget-object v0, Lflipboard/activities/FlipboardActivity;->L:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_5

    .line 419
    invoke-static {p0}, Lcom/android/debug/hv/ViewServer;->a(Landroid/content/Context;)Lcom/android/debug/hv/ViewServer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/debug/hv/ViewServer;->c(Landroid/app/Activity;)V

    .line 422
    :cond_5
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->n()V

    .line 424
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 425
    iget v1, p0, Lflipboard/activities/FlipboardActivity;->y:F

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_6

    .line 428
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/FlipboardActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$4;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v8, v9, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 441
    :cond_6
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_7

    .line 442
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v0}, Lflipboard/service/audio/MediaPlayerService;->c()V

    .line 443
    :goto_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 444
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->setVolumeControlStream(I)V

    .line 448
    :cond_7
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v2, "event"

    invoke-direct {v1, v2}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "type"

    const-string v3, "enter"

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "id"

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz v0, :cond_9

    const-string v2, "section"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "item"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const-string v0, "deprecated"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_9
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->b()V

    .line 450
    :cond_a
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->h()Ljava/lang/String;

    move-result-object v0

    .line 451
    if-nez v0, :cond_b

    .line 452
    const-string v0, "unnamed"

    .line 454
    :cond_b
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->ab:Lflipboard/objs/CrashInfo;

    iput-object v0, v1, Lflipboard/objs/CrashInfo;->e:Ljava/lang/String;

    .line 455
    return-void

    .line 442
    :cond_c
    invoke-virtual {v0, v6}, Lflipboard/service/audio/FLAudioManager;->a(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 485
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :goto_0
    const-string v0, "state_active_time"

    iget-wide v2, p0, Lflipboard/activities/FlipboardActivity;->V:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 490
    return-void

    .line 487
    :catch_0
    move-exception v0

    const-string v0, "unwanted.exception_in_onSaveInstanceState"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 856
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawer(Landroid/app/Activity;)V

    .line 857
    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 292
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 293
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_0

    const-string v1, "extra_restarted"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 297
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_restarted"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 299
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onStart()V

    .line 300
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 755
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->c(Lflipboard/activities/FlipboardActivity;)V

    .line 757
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onStop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 766
    :goto_0
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 767
    return-void

    .line 758
    :catch_0
    move-exception v0

    .line 759
    sget-object v1, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 761
    :try_start_1
    invoke-super {p0}, Landroid/support/v4/app/FlipboardFragmentActivity;->onStop()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 762
    :catch_1
    move-exception v0

    .line 763
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1038
    const/4 v0, 0x0

    return-object v0
.end method

.method public q()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 997
    const/4 v0, 0x0

    return-object v0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->s:Z

    if-nez v0, :cond_0

    .line 184
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->r:Z

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->s:Z

    .line 187
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    .line 188
    return-void
.end method

.method protected s_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1020
    const/4 v0, 0x0

    return-object v0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 862
    invoke-static {p0, p1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863
    iput-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->q:Z

    .line 864
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 865
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 867
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FlipboardFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 872
    :goto_0
    return-void

    .line 870
    :cond_1
    const v0, 0x7f0d001d

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 876
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 877
    const-string v0, "launched_by_flipboard_activity"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 879
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FlipboardFragmentActivity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 880
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 888
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/Intent;ILflipboard/activities/FlipboardActivity$ActivityResultListener;)V

    .line 889
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 922
    const-string v0, "launched_by_flipboard_activity"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 923
    const-string v0, "launched_by_flipboard_activity"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 925
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FlipboardFragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 926
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->s:Z

    if-eqz v0, :cond_0

    .line 196
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->r:Z

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->W:Z

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->s:Z

    .line 199
    :cond_0
    return-void
.end method

.method protected t_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1029
    const/4 v0, 0x0

    return-object v0
.end method

.method public u_()Z
    .locals 1

    .prologue
    .line 1264
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    .line 1265
    const/4 v0, 0x1

    return v0
.end method

.method final v()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 326
    new-instance v2, Lflipboard/io/UsageEvent;

    const-string v0, "settings"

    invoke-direct {v2, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 327
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 328
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    iget-object v0, v0, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    .line 329
    :goto_0
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    iget-object v1, v1, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    .line 330
    :goto_1
    iget-object v3, p0, Lflipboard/activities/FlipboardActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v3, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 331
    sget-object v6, Lflipboard/activities/FlipboardActivity;->J:Lflipboard/util/Log;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v8

    aput-object v1, v6, v9

    .line 332
    sget-object v6, Lflipboard/activities/FlipboardActivity;->J:Lflipboard/util/Log;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    .line 333
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 334
    const-string v6, "cgEdition"

    invoke-virtual {v2, v6, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 336
    :cond_0
    if-lez v3, :cond_4

    .line 337
    const-string v0, "beginSessionFavoritesCount"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 341
    :goto_2
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 342
    const-string v0, "cgLang"

    invoke-virtual {v2, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 344
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v4

    iput-wide v0, v2, Lflipboard/io/UsageEvent;->g:J

    .line 347
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    .line 349
    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 350
    const-string v0, "fullUse"

    .line 356
    :goto_3
    const-string v1, "reduceDataUsage"

    invoke-virtual {v2, v1, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 358
    invoke-virtual {v2}, Lflipboard/io/UsageEvent;->a()V

    .line 359
    return-void

    .line 328
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 329
    :cond_3
    const-string v1, ""

    goto :goto_1

    .line 339
    :cond_4
    sget-object v0, Lflipboard/activities/FlipboardActivity;->J:Lflipboard/util/Log;

    const-string v6, "Favorites count: %s "

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    invoke-virtual {v0, v6, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 351
    :cond_5
    const-string v1, "ondemand"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 352
    const-string v0, "onDemand"

    goto :goto_3

    .line 354
    :cond_6
    const-string v0, "wifiOnly"

    goto :goto_3
.end method

.method protected w()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 468
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 469
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 470
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 471
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 472
    const-string v2, "extra_restarted"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 473
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 474
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 475
    invoke-virtual {p0, v4, v4}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 477
    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 478
    invoke-virtual {p0, v4, v4}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 480
    return-void
.end method

.method protected final x()V
    .locals 1

    .prologue
    .line 507
    const/16 v0, 0x5dc

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->a(I)V

    .line 508
    return-void
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 601
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v0, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->forceUpgradeHiddenLauncherIconBuild:Z

    .line 607
    if-eqz v0, :cond_0

    .line 611
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x64

    new-instance v1, Lflipboard/activities/FlipboardActivity$6;

    invoke-direct {v1, p0}, Lflipboard/activities/FlipboardActivity$6;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected final z()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x400

    .line 711
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    const-string v1, "fullscreen"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->I:Z

    if-nez v0, :cond_0

    .line 713
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->I:Z

    .line 714
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 722
    :cond_0
    :goto_0
    return-void

    .line 717
    :cond_1
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->I:Z

    if-eqz v0, :cond_0

    .line 718
    iput-boolean v3, p0, Lflipboard/activities/FlipboardActivity;->I:Z

    .line 719
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method
