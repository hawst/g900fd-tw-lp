.class Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SwitchContentGuideActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/SwitchContentGuideActivity;

.field private c:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;


# direct methods
.method constructor <init>(Lflipboard/activities/SwitchContentGuideActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 88
    iput-object p1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    .line 89
    invoke-direct {p0, p1, v0, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 90
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    :cond_0
    invoke-static {p1}, Lflipboard/activities/SwitchContentGuideActivity;->a(Lflipboard/activities/SwitchContentGuideActivity;)V

    .line 93
    :cond_1
    return-void
.end method

.method static synthetic a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->c:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;)Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->c:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    return-object p1
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 120
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lflipboard/activities/SwitchContentGuideActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 123
    invoke-virtual {p0, p1}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 124
    instance-of v1, v2, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 125
    check-cast v1, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    .line 126
    const v4, 0x7f030134

    invoke-virtual {v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 129
    const v0, 0x7f0a0348

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 130
    invoke-virtual {v1}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    const v0, 0x7f0a027f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 134
    check-cast v2, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    iget-boolean v0, v2, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v3

    .line 137
    :goto_1
    return-object v0

    .line 134
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    move-object v0, v3

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 145
    instance-of v0, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->getCount()I

    move-result v0

    if-lt p3, v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-virtual {p0, p3}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    .line 157
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->c:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    if-eqz v1, :cond_1

    .line 158
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->c:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->a:Z

    .line 161
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->a:Z

    .line 162
    iput-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->c:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    .line 164
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    invoke-static {v1}, Lflipboard/activities/SwitchContentGuideActivity;->b(Lflipboard/activities/SwitchContentGuideActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 165
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    invoke-static {v1}, Lflipboard/activities/SwitchContentGuideActivity;->c(Lflipboard/activities/SwitchContentGuideActivity;)Z

    .line 168
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->notifyDataSetChanged()V

    .line 170
    new-instance v1, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    .line 171
    const v2, 0x7f0d01e4

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    .line 172
    iget-object v2, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    const-string v3, "progress"

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 173
    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v1, v1, Lflipboard/activities/SwitchContentGuideActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-object v2, v2, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;->b:Lflipboard/objs/ConfigEdition;

    iget-object v0, v0, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    invoke-static {v0}, Lflipboard/activities/SwitchContentGuideActivity;->d(Lflipboard/activities/SwitchContentGuideActivity;)V

    goto :goto_0
.end method
