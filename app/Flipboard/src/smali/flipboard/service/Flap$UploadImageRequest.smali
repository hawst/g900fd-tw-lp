.class public Lflipboard/service/Flap$UploadImageRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Ljava/io/File;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 3487
    iput-object p1, p0, Lflipboard/service/Flap$UploadImageRequest;->b:Lflipboard/service/Flap;

    .line 3488
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3489
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;IILflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$UploadImageRequest;
    .locals 2

    .prologue
    .line 3493
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    .line 3494
    iput-object p1, p0, Lflipboard/service/Flap$UploadImageRequest;->c:Ljava/lang/String;

    .line 3495
    iput-object p2, p0, Lflipboard/service/Flap$UploadImageRequest;->d:Ljava/io/File;

    .line 3496
    iput-object p3, p0, Lflipboard/service/Flap$UploadImageRequest;->e:Ljava/lang/String;

    .line 3497
    iput-object p6, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 3498
    iput p4, p0, Lflipboard/service/Flap$UploadImageRequest;->f:I

    .line 3499
    iput p5, p0, Lflipboard/service/Flap$UploadImageRequest;->g:I

    .line 3500
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3501
    return-object p0
.end method

.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3507
    iget-object v0, p0, Lflipboard/service/Flap$UploadImageRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/social/imageURL"

    iget-object v2, p0, Lflipboard/service/Flap$UploadImageRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "width"

    aput-object v4, v3, v8

    iget v4, p0, Lflipboard/service/Flap$UploadImageRequest;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    const-string v5, "height"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p0, Lflipboard/service/Flap$UploadImageRequest;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "reserved"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$UploadImageRequest;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3508
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v8

    .line 3510
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 3512
    new-instance v0, Lorg/apache/http/entity/FileEntity;

    iget-object v2, p0, Lflipboard/service/Flap$UploadImageRequest;->d:Ljava/io/File;

    iget-object v3, p0, Lflipboard/service/Flap$UploadImageRequest;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/entity/FileEntity;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3513
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/http/entity/FileEntity;->setChunked(Z)V

    .line 3514
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 3516
    iget-object v0, p0, Lflipboard/service/Flap$UploadImageRequest;->b:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3517
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3518
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$UploadImageRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3519
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3520
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 3521
    iget-object v0, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3541
    iput-object v6, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 3542
    :goto_0
    return-void

    .line 3524
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3525
    if-eqz v0, :cond_2

    .line 3526
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3527
    iget-object v1, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3541
    :goto_1
    iput-object v6, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3530
    :cond_1
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3535
    :catch_0
    move-exception v0

    .line 3536
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3541
    iput-object v6, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3533
    :cond_2
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 3537
    :catch_1
    move-exception v0

    .line 3538
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3539
    iget-object v1, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3541
    iput-object v6, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lflipboard/service/Flap$UploadImageRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method
