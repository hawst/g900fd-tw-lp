.class public abstract Lflipboard/service/PushServiceManager;
.super Ljava/lang/Object;
.source "PushServiceManager.java"


# static fields
.field public static a:Lflipboard/util/Log;

.field protected static b:Z


# instance fields
.field public c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "push notification"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/PushServiceManager;->a:Lflipboard/util/Log;

    .line 19
    const/4 v0, 0x1

    sput-boolean v0, Lflipboard/service/PushServiceManager;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lflipboard/service/PushServiceManager;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lflipboard/app/BuildConfig;->a:Lflipboard/service/PushServiceManager;

    iput-object p0, v0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    return-object v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->NotificationsEnabled:Z

    return v0
.end method

.method public static c(Lflipboard/service/User;)Z
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v0, :cond_1

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lflipboard/service/PushServiceManager;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Lflipboard/service/User;)V
.end method

.method public abstract b(Lflipboard/service/User;)V
.end method
