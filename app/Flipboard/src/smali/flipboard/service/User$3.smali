.class Lflipboard/service/User$3;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lflipboard/service/User$3;->a:Lflipboard/service/User;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 5

    .prologue
    .line 347
    const-string v0, "unreadcounts"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v1

    .line 348
    invoke-virtual {v1}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 349
    iget-object v3, p0, Lflipboard/service/User$3;->a:Lflipboard/service/User;

    invoke-static {v3}, Lflipboard/service/User;->b(Lflipboard/service/User;)Lflipboard/service/Section;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 350
    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->d(Ljava/lang/String;)I

    move-result v0

    .line 351
    iget-object v3, p0, Lflipboard/service/User$3;->a:Lflipboard/service/User;

    iget v3, v3, Lflipboard/service/User;->o:I

    if-eq v0, v3, :cond_0

    .line 352
    iget-object v3, p0, Lflipboard/service/User$3;->a:Lflipboard/service/User;

    iput v0, v3, Lflipboard/service/User;->o:I

    .line 353
    iget-object v0, p0, Lflipboard/service/User$3;->a:Lflipboard/service/User;

    sget-object v3, Lflipboard/service/User$Message;->k:Lflipboard/service/User$Message;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 358
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 361
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 362
    return-void
.end method
