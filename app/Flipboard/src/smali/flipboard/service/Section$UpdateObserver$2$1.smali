.class Lflipboard/service/Section$UpdateObserver$2$1;
.super Lflipboard/service/FlCrashListener;
.source "Section.java"


# instance fields
.field final synthetic a:Lflipboard/service/Section$UpdateObserver$2;


# direct methods
.method constructor <init>(Lflipboard/service/Section$UpdateObserver$2;)V
    .locals 0

    .prologue
    .line 1502
    iput-object p1, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    invoke-direct {p0}, Lflipboard/service/FlCrashListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1506
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lflipboard/service/FlCrashListener;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1507
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    iget-object v0, v0, Lflipboard/service/Section$UpdateObserver$2;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 1508
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 1509
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    iget-object v0, v0, Lflipboard/service/Section$UpdateObserver$2;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 1510
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    iget-object v0, v0, Lflipboard/service/Section$UpdateObserver$2;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1511
    const-string v5, "\n\nDuplicate index "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1512
    if-eqz v0, :cond_0

    .line 1513
    const-string v4, " title: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1508
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1516
    :cond_1
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    iget-object v0, v0, Lflipboard/service/Section$UpdateObserver$2;->c:Lflipboard/io/RequestLogEntry;

    if-eqz v0, :cond_2

    .line 1517
    const-string v0, "\n\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    iget-object v1, v1, Lflipboard/service/Section$UpdateObserver$2;->c:Lflipboard/io/RequestLogEntry;

    invoke-virtual {v1}, Lflipboard/io/RequestLogEntry;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1519
    :cond_2
    const-string v0, "\n\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1521
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver$2$1;->a:Lflipboard/service/Section$UpdateObserver$2;

    iget-object v0, v0, Lflipboard/service/Section$UpdateObserver$2;->d:Lflipboard/service/Section$UpdateObserver;

    iget-object v0, v0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1522
    const-string v0, "\n\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1523
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
