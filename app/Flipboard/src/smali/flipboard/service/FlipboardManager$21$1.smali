.class Lflipboard/service/FlipboardManager$21$1;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/activities/FlipboardActivity$ActivityResultListener;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lflipboard/service/User;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflipboard/service/Account;

.field final synthetic e:Lflipboard/service/FlipboardManager$21;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1886
    const-class v0, Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/service/FlipboardManager$21$1;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lflipboard/service/FlipboardManager$21;Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Account;)V
    .locals 0

    .prologue
    .line 1886
    iput-object p1, p0, Lflipboard/service/FlipboardManager$21$1;->e:Lflipboard/service/FlipboardManager$21;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$21$1;->b:Lflipboard/service/User;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$21$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/service/FlipboardManager$21$1;->d:Lflipboard/service/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1889
    sget-boolean v0, Lflipboard/service/FlipboardManager$21$1;->a:Z

    if-nez v0, :cond_0

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1890
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$21$1;->e:Lflipboard/service/FlipboardManager$21;

    iget-object v0, v0, Lflipboard/service/FlipboardManager$21;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->g(Lflipboard/service/FlipboardManager;)Z

    .line 1892
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 1901
    iget-object v0, p0, Lflipboard/service/FlipboardManager$21$1;->b:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$21$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 1902
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$21$1;->d:Lflipboard/service/Account;

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    .line 1903
    iget-object v1, p0, Lflipboard/service/FlipboardManager$21$1;->d:Lflipboard/service/Account;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lflipboard/service/FlipboardManager$21$1;->d:Lflipboard/service/Account;

    invoke-virtual {v1}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1905
    iget-object v0, p0, Lflipboard/service/FlipboardManager$21$1;->b:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$21$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->f(Ljava/lang/String;)V

    .line 1913
    :goto_0
    const-class v0, Lflipboard/activities/SectionTabletActivity;

    new-instance v1, Lflipboard/service/FlipboardManager$21$1$1;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$21$1$1;-><init>(Lflipboard/service/FlipboardManager$21$1;)V

    invoke-static {v0, v1}, Lflipboard/activities/FlipboardActivity;->a(Ljava/lang/Class;Lflipboard/util/Callback;)V

    .line 1921
    :goto_1
    return-void

    .line 1907
    :cond_1
    iget-object v1, p0, Lflipboard/service/FlipboardManager$21$1;->b:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Account;)V

    goto :goto_0

    .line 1919
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$21$1;->c:Ljava/lang/String;

    aput-object v1, v0, v3

    goto :goto_1
.end method
