.class Lflipboard/service/Flap$1;
.super Ljava/lang/Object;
.source "Flap.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lflipboard/service/Flap$1;->a:Lflipboard/service/Flap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 142
    const-string v0, "server_baseurl"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lflipboard/service/Flap$1;->a:Lflipboard/service/Flap;

    const-string v1, "server_baseurl"

    sget-object v2, Lflipboard/activities/SettingsFragment;->c:Ljava/lang/String;

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Flap;->d:Ljava/lang/String;

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    const-string v0, "adserver_baseurl"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lflipboard/service/Flap$1;->a:Lflipboard/service/Flap;

    const-string v1, "adserver_baseurl"

    invoke-static {}, Lflipboard/activities/SettingsFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Flap;->e:Ljava/lang/String;

    goto :goto_0
.end method
