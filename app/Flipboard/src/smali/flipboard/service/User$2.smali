.class Lflipboard/service/User$2;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lflipboard/service/User$2;->b:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$2;->a:Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 276
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 277
    const-string v0, "uid"

    iget-object v2, p0, Lflipboard/service/User$2;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lflipboard/service/User$2;->b:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 279
    const-string v3, "id = ?"

    new-array v4, v7, [Ljava/lang/String;

    iget v5, v0, Lflipboard/service/Section;->f:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v1, v3, v4}, Lflipboard/service/User$2;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 280
    sget-object v3, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    goto :goto_0

    .line 282
    :cond_0
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v3, "failed to update section uid: %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/service/User$2;->a:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 285
    :cond_1
    return-void
.end method
