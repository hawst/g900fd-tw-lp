.class public Lflipboard/service/Flap;
.super Ljava/lang/Object;
.source "Flap.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/concurrent/Executor;

.field private static final l:Ljava/io/ByteArrayInputStream;


# instance fields
.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field final g:Lorg/apache/http/client/CookieStore;

.field final h:Landroid/content/Context;

.field public final i:Lflipboard/service/FlipboardManager;

.field public j:Ljava/lang/String;

.field protected k:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Flap$UpdateRequest;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/String;

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    const-string v0, "flap"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    .line 68
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contentGuide.json"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "services.json"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "config.json"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "dynamicStrings.json"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "popularSearches.json"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "hints.json"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "externalLibraryFeeds.json"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "apiClients.json"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/service/Flap;->b:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/io/ByteArrayInputStream;

    new-array v1, v3, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sput-object v0, Lflipboard/service/Flap;->l:Ljava/io/ByteArrayInputStream;

    .line 107
    invoke-static {}, Lflipboard/util/AndroidUtil;->h()Ljava/util/concurrent/Executor;

    move-result-object v0

    sput-object v0, Lflipboard/service/Flap;->c:Ljava/util/concurrent/Executor;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lorg/apache/http/client/CookieStore;)V
    .locals 3

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    .line 117
    check-cast v0, Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    .line 128
    iput-object p2, p0, Lflipboard/service/Flap;->g:Lorg/apache/http/client/CookieStore;

    .line 129
    iput-object p1, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    .line 130
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "apad"

    :goto_0
    iput-object v0, p0, Lflipboard/service/Flap;->f:Ljava/lang/String;

    .line 133
    const-string v0, "flipboard_settings"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 134
    const-string v1, "server_baseurl"

    sget-object v2, Lflipboard/activities/SettingsFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/Flap;->d:Ljava/lang/String;

    .line 136
    const-string v1, "enable_audio"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/service/Flap;->k:Z

    .line 138
    new-instance v1, Lflipboard/service/Flap$1;

    invoke-direct {v1, p0}, Lflipboard/service/Flap$1;-><init>(Lflipboard/service/Flap;)V

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 149
    return-void

    .line 130
    :cond_0
    const-string v0, "aphone"

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/Flap;Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 55
    sget-object v0, Lflipboard/service/Flap;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "flap static files must be one of Flap.{SECTIONS,SERVICES,CONFIG,POPULAR_SEARCHES}_JSON"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v0, p0, Lflipboard/service/Flap;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/v1/static/"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p2, :cond_3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v5, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lflipboard/service/Flap;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "content_guide_locale"

    invoke-interface {v5, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "contentGuide.json"

    if-ne p1, v6, :cond_1

    const-string v1, "content_guide_language"

    invoke-interface {v5, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    :cond_1
    const-string v5, "?ver=%s&userid=%s&udid=%s&tuuid=%s&device=%s&lang=%s&locale=%s&screensize=%.1f&locale_cg=%s"

    const/16 v6, 0x9

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lflipboard/service/Flap;->c()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    aput-object v4, v6, v7

    const/4 v4, 0x2

    iget-object v7, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    aput-object v7, v6, v4

    const/4 v4, 0x3

    iget-object v7, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    aput-object v7, v6, v4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lflipboard/service/Flap;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x5

    aput-object v0, v6, v4

    const/4 v0, 0x6

    aput-object v1, v6, v0

    const/4 v0, 0x7

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->h()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v6, v0

    const/16 v0, 0x8

    aput-object v2, v6, v0

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_2

    const-string v0, "&bundled=true"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-direct {p0, v3, p3}, Lflipboard/service/Flap;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v0, p2, Lflipboard/service/User;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3949
    const-string v0, "zh"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3950
    const-string v0, "zh_TW"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "zh_HK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "zh-Hant"

    .line 3952
    :goto_0
    return-object v0

    .line 3950
    :cond_1
    const-string v0, "zh-Hans"

    goto :goto_0

    :cond_2
    move-object v0, p0

    goto :goto_0
.end method

.method private varargs a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x3d

    .line 4053
    move v0, v1

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_6

    .line 4054
    add-int/lit8 v3, v0, 0x1

    aget-object v0, p2, v0

    .line 4055
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 4056
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4057
    add-int/lit8 v0, v3, 0x3

    .line 4058
    goto :goto_0

    .line 4060
    :cond_0
    add-int/lit8 v2, v3, 0x1

    aget-object v0, p2, v3

    move-object v3, v0

    move v0, v2

    .line 4062
    :goto_1
    add-int/lit8 v2, v0, 0x1

    aget-object v0, p2, v0

    .line 4063
    if-eqz v0, :cond_5

    .line 4065
    instance-of v4, v0, [Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 4066
    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    array-length v5, v0

    move v4, v1

    :goto_2
    if-ge v4, v5, :cond_1

    aget-object v6, v0, v4

    .line 4067
    const-string v7, "&"

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4066
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_0

    .line 4069
    :cond_2
    instance-of v4, v0, Ljava/util/List;

    if-eqz v4, :cond_4

    .line 4070
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 4071
    const-string v5, "&"

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    move v0, v2

    .line 4072
    goto :goto_0

    .line 4074
    :cond_4
    const-string v4, "&"

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    move v0, v2

    .line 4076
    goto/16 :goto_0

    .line 4077
    :cond_6
    iget-object v0, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_7

    .line 4078
    const-string v0, "&flipster=1"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4080
    :cond_7
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_8

    .line 4081
    const-string v0, "&variant=china"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4083
    :cond_8
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_9
    move-object v9, v0

    move v0, v3

    move-object v3, v9

    goto/16 :goto_1
.end method

.method static synthetic a(Lflipboard/service/Flap;)Ljava/util/List;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    return-object v0
.end method

.method private a(Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;Z)V
    .locals 4

    .prologue
    .line 4874
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 4875
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4876
    invoke-virtual {p0, v0, p4}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;Z)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 4877
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 4878
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p3, v0, v1}, Lflipboard/service/Flap$Request;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4881
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 4882
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 4883
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected response from flap: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4897
    :try_start_2
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4907
    :goto_0
    return-void

    .line 4886
    :cond_0
    :try_start_3
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 4887
    if-eqz v0, :cond_2

    .line 4888
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4889
    invoke-interface {p1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4897
    :goto_1
    :try_start_4
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 4907
    :catch_0
    move-exception v0

    .line 4902
    :try_start_5
    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 4907
    :catchall_0
    move-exception v0

    throw v0

    .line 4891
    :cond_1
    :try_start_6
    const-string v1, "errormessage"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 4897
    :catchall_1
    move-exception v0

    :try_start_7
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V

    .line 4898
    throw v0
    :try_end_7
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 4903
    :catch_1
    move-exception v0

    .line 4904
    :try_start_8
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 4905
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    .line 4894
    :cond_2
    :try_start_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected null response for: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1
.end method

.method static synthetic a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;Z)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3960
    sget-object v0, Lflipboard/service/Flap;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;Z)V

    return-void
.end method

.method static synthetic f()Ljava/io/ByteArrayInputStream;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lflipboard/service/Flap;->l:Ljava/io/ByteArrayInputStream;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/User;Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")",
            "Lflipboard/service/Flap$CommentaryRequest;"
        }
    .end annotation

    .prologue
    .line 522
    new-instance v0, Lflipboard/service/Flap$CommentaryRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$CommentaryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const-string v1, "/v1/social/commentary"

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2, p3}, Lflipboard/service/Flap$CommentaryRequest;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")",
            "Lflipboard/service/Flap$CommentaryRequest;"
        }
    .end annotation

    .prologue
    .line 527
    new-instance v0, Lflipboard/service/Flap$CommentaryRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$CommentaryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const-string v1, "/v1/social/comments"

    invoke-virtual {v0, p2, v1, p3, p4}, Lflipboard/service/Flap$CommentaryRequest;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lflipboard/service/User;Ljava/util/List;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ImageRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")",
            "Lflipboard/service/Flap$ImageRequest;"
        }
    .end annotation

    .prologue
    .line 579
    new-instance v0, Lflipboard/service/Flap$ImageRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$ImageRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v0, p2, p3}, Lflipboard/service/Flap$ImageRequest;->a(Ljava/util/List;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ImageRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;)",
            "Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;"
        }
    .end annotation

    .prologue
    .line 447
    new-instance v0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    iget-object v1, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, p0, v1}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const/4 v3, 0x0

    const-string v4, "/v1/flipboard/loginWithToken"

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)Lflipboard/service/Flap$SearchRequest;
    .locals 4

    .prologue
    .line 493
    new-instance v0, Lflipboard/service/Flap$SearchRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$SearchRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object p2, v0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    iput-object p3, v0, Lflipboard/service/Flap$SearchRequest;->b:Lflipboard/service/Flap$SearchType;

    iput-object p4, v0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    if-eqz p5, :cond_0

    iput-object p5, v0, Lflipboard/service/Flap$SearchRequest;->d:Ljava/lang/String;

    iput p6, v0, Lflipboard/service/Flap$SearchRequest;->e:I

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/service/Flap$SearchRequest;->f:J

    invoke-virtual {v0}, Lflipboard/service/Flap$SearchRequest;->c()V

    return-object v0
.end method

.method public final a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$ShareListRequest;
    .locals 1

    .prologue
    .line 638
    new-instance v0, Lflipboard/service/Flap$ShareListRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$ShareListRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v0, p2, p3}, Lflipboard/service/Flap$ShareListRequest;->a(Lflipboard/objs/ConfigService;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$ShareListRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;
    .locals 2

    .prologue
    .line 389
    if-nez p1, :cond_0

    .line 390
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null user is not supported!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_0
    new-instance v0, Lflipboard/service/Flap$UpdateRequest;

    invoke-direct {v0, p0, p1, p2, p3}, Lflipboard/service/Flap$UpdateRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;ZZ)V

    return-object v0
.end method

.method public final varargs a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 4028
    iget-object v0, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 4029
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 4030
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 4031
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4032
    const-string v3, "content_guide_locale"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4033
    invoke-static {v2, v1}, Lflipboard/service/Flap;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4035
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 4036
    iget-object v0, p0, Lflipboard/service/Flap;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4037
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4038
    const-string v0, "/"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4039
    if-nez p2, :cond_0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4040
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4041
    const-string v5, "?userid=%s&udid=%s&tuuid=%s&ver=%s&device=%s&lang=%s&locale=%s&screensize=%.1f&locale_cg=%s"

    const/16 v6, 0x9

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    const/4 v0, 0x1

    iget-object v7, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    aput-object v7, v6, v0

    const/4 v0, 0x2

    iget-object v7, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    aput-object v7, v6, v0

    const/4 v0, 0x3

    invoke-virtual {p0}, Lflipboard/service/Flap;->c()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x4

    invoke-virtual {p0}, Lflipboard/service/Flap;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v2, v6, v0

    const/4 v0, 0x6

    aput-object v1, v6, v0

    const/4 v0, 0x7

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->h()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v6, v0

    const/16 v0, 0x8

    aput-object v3, v6, v0

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4043
    invoke-direct {p0, v4, p3}, Lflipboard/service/Flap;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 4039
    :cond_0
    iget-object v0, p2, Lflipboard/service/User;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    .line 751
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {p0}, Lflipboard/service/Flap;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;ZZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lorg/apache/http/HttpRequest;Z)Lorg/apache/http/HttpResponse;
    .locals 3

    .prologue
    .line 756
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {p0}, Lflipboard/service/Flap;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2, p2}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;ZZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lorg/apache/http/protocol/HttpContext;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 156
    iget-object v1, p0, Lflipboard/service/Flap;->g:Lorg/apache/http/client/CookieStore;

    if-eqz v1, :cond_0

    .line 157
    const-string v1, "http.cookie-store"

    iget-object v2, p0, Lflipboard/service/Flap;->g:Lorg/apache/http/client/CookieStore;

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/protocol/BasicHttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    :cond_0
    return-object v0
.end method

.method public final a(Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)V
    .locals 6

    .prologue
    .line 503
    new-instance v0, Lflipboard/service/Flap$ReplyRequest;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Flap$ReplyRequest;-><init>(Lflipboard/service/Flap;Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v0, p5, p6}, Lflipboard/service/Flap$ReplyRequest;->a(Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)Lflipboard/service/Flap$ReplyRequest;

    .line 504
    return-void
.end method

.method public final a(Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)V
    .locals 7

    .prologue
    .line 507
    new-instance v4, Lflipboard/objs/FeedItem;

    invoke-direct {v4}, Lflipboard/objs/FeedItem;-><init>()V

    .line 510
    iput-object p3, v4, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    .line 512
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap;->a(Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)V

    .line 513
    return-void
.end method

.method public final a(Lflipboard/objs/FeedItem;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 2

    .prologue
    .line 517
    new-instance v0, Lflipboard/service/Flap$ReplyRemoveRequest;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, p0, v1, p1, p2}, Lflipboard/service/Flap$ReplyRemoveRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/objs/FeedItem;Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lflipboard/service/Flap$ReplyRemoveRequest;->a(Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ReplyRemoveRequest;

    .line 518
    return-void
.end method

.method public final a(Lflipboard/service/User;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 862
    iget-object v0, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 863
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    iget-object v1, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 864
    iget-object v2, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    monitor-enter v2

    .line 865
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 866
    iget-object v0, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Flap$UpdateRequest;

    .line 867
    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    if-ne v3, p1, :cond_0

    .line 868
    iget-object v3, p0, Lflipboard/service/Flap;->q:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 869
    sget-object v3, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 870
    invoke-virtual {v0}, Lflipboard/service/Flap$UpdateRequest;->cancel()Z

    :cond_0
    move v0, v1

    .line 872
    goto :goto_0

    .line 873
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 875
    :cond_2
    return-void

    .line 873
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)V
    .locals 2

    .prologue
    .line 628
    new-instance v0, Lflipboard/service/Flap$SectionListRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$SectionListRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iget-object v1, p2, Lflipboard/objs/ConfigService;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, p3, p4}, Lflipboard/service/Flap$SectionListRequest;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$SectionListRequest;

    .line 629
    return-void
.end method

.method public final a(Lflipboard/service/User;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 6

    .prologue
    .line 551
    new-instance v0, Lflipboard/service/Flap$FollowRequest;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Flap$FollowRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)V

    invoke-static {v0, p4}, Lflipboard/service/Flap$FollowRequest;->a(Lflipboard/service/Flap$FollowRequest;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$FollowRequest;

    .line 552
    return-void
.end method

.method public final a(Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/CommentaryResult$Item$Commentary;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 667
    new-instance v0, Lflipboard/service/Flap$FlagRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$FlagRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {p2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lflipboard/objs/FeedItem;->m()Ljava/lang/String;

    move-result-object v2

    if-nez p4, :cond_0

    move-object v3, v4

    :goto_0
    iget-object v5, p3, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-nez v5, :cond_1

    :goto_1
    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap$FlagRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 668
    return-void

    .line 667
    :cond_0
    iget-object v3, p4, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v4, p3, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lflipboard/service/User;Ljava/lang/String;Ljava/util/Collection;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 655
    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 656
    new-instance v0, Lflipboard/service/Flap$ReadRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$ReadRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v0, p2, p3, p4}, Lflipboard/service/Flap$ReadRequest;->a(Ljava/lang/String;Ljava/util/Collection;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 658
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 555
    new-instance v0, Lflipboard/service/Flap$FollowRequest;

    invoke-direct {v0, p0, p1, p2, p3}, Lflipboard/service/Flap$FollowRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;)V

    invoke-static {v0, p4}, Lflipboard/service/Flap$FollowRequest;->a(Lflipboard/service/Flap$FollowRequest;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$FollowRequest;

    .line 556
    return-void
.end method

.method public final a(ZLflipboard/service/Flap$TypedResultObserver;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 478
    new-instance v0, Lflipboard/service/Flap$UserMagazinesRequest;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, p0, v1}, Lflipboard/service/Flap$UserMagazinesRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v0, p1, p2}, Lflipboard/service/Flap$UserMagazinesRequest;->a(ZLflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$UserMagazinesRequest;

    .line 479
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lflipboard/service/Flap;->m:Ljava/lang/String;

    .line 165
    if-nez v0, :cond_0

    .line 166
    const-string v0, "%s-%s-%s-%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/Flap;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 167
    iput-object v0, p0, Lflipboard/service/Flap;->m:Ljava/lang/String;

    .line 169
    :cond_0
    return-object v0
.end method

.method public final varargs b(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5097
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 5098
    iget-object v0, p0, Lflipboard/service/Flap;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    const-string v2, "flipboard_settings"

    invoke-virtual {v0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "adserver_baseurl"

    invoke-static {}, Lflipboard/activities/SettingsFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap;->e:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5099
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5100
    const-string v2, "?device=%s&user_id=%s&model=%s&ver=%s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    .line 5101
    invoke-virtual {p0}, Lflipboard/service/Flap;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    iget-object v0, p2, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v0, v3, v7

    .line 5102
    iget-object v0, p0, Lflipboard/service/Flap;->n:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "%s-%s"

    new-array v4, v8, [Ljava/lang/Object;

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v5, v4, v6

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap;->n:Ljava/lang/String;

    :cond_1
    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    .line 5103
    invoke-virtual {p0}, Lflipboard/service/Flap;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    .line 5100
    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5107
    iget-object v0, p0, Lflipboard/service/Flap;->r:Ljava/lang/String;

    if-nez v0, :cond_2

    iget v0, p0, Lflipboard/service/Flap;->s:I

    if-ge v0, v9, :cond_2

    .line 5109
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v0

    .line 5110
    const-string v2, "&advertising_id=%s&limit_ad_tracking=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-boolean v0, v0, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap;->r:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5117
    :cond_2
    :goto_0
    iget-object v0, p0, Lflipboard/service/Flap;->r:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5118
    iget-object v0, p0, Lflipboard/service/Flap;->r:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5120
    :cond_3
    invoke-direct {p0, v1, p3}, Lflipboard/service/Flap;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 5111
    :catch_0
    move-exception v0

    .line 5113
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 5114
    iget v0, p0, Lflipboard/service/Flap;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/service/Flap;->s:I

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 184
    iget-object v0, p0, Lflipboard/service/Flap;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 185
    const/4 v1, 0x0

    .line 187
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 192
    :goto_0
    if-eqz v1, :cond_2

    .line 194
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 195
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 196
    if-lez v2, :cond_0

    .line 197
    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 199
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap;->o:Ljava/lang/String;

    .line 207
    :cond_1
    iget-object v0, p0, Lflipboard/service/Flap;->o:Ljava/lang/String;

    :goto_1
    return-object v0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 202
    :cond_2
    const-string v0, "1.8.4.0"

    .line 203
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "unable to build version number. defaulting to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public compose(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 574
    new-instance v0, Lflipboard/service/Flap$ComposeRequest;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, p0, v1, p1, p6}, Lflipboard/service/Flap$ComposeRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Section;)V

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$ComposeRequest;->compose(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ComposeRequest;

    .line 575
    return-void
.end method

.method public createAccount(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$AccountRequestObserver;)Lflipboard/service/Flap$CreateAccountRequest;
    .locals 1

    .prologue
    .line 400
    new-instance v0, Lflipboard/service/Flap$CreateAccountRequest;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$CreateAccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object p8, v0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iput-object p2, v0, Lflipboard/service/Flap$CreateAccountRequest;->g:Ljava/lang/String;

    iput-object p3, v0, Lflipboard/service/Flap$CreateAccountRequest;->h:Ljava/lang/String;

    iput-object p4, v0, Lflipboard/service/Flap$CreateAccountRequest;->i:Ljava/lang/String;

    iput-object p5, v0, Lflipboard/service/Flap$CreateAccountRequest;->j:Ljava/lang/String;

    iput-object p6, v0, Lflipboard/service/Flap$CreateAccountRequest;->k:Ljava/lang/String;

    if-nez p7, :cond_0

    const-string p7, "flipboard"

    :cond_0
    iput-object p7, v0, Lflipboard/service/Flap$CreateAccountRequest;->l:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/service/Flap$CreateAccountRequest;->c()V

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 212
    iget-object v0, p0, Lflipboard/service/Flap;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 213
    const/4 v1, 0x0

    .line 215
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 220
    :goto_0
    if-eqz v0, :cond_2

    .line 222
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 223
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 224
    if-lez v1, :cond_0

    .line 225
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 227
    :cond_0
    iput-object v0, p0, Lflipboard/service/Flap;->p:Ljava/lang/String;

    .line 235
    :cond_1
    iget-object v0, p0, Lflipboard/service/Flap;->p:Ljava/lang/String;

    :goto_1
    return-object v0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 230
    :cond_2
    const-string v0, "1.8.4.0"

    .line 231
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "unable to build version number. defaulting to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 338
    iget-object v0, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "usage_redirect_monitor_baseurl"

    const-string v3, "http://jing.eng.live.flipboard.com:8080"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 339
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    const-string v0, "/usagemonitor"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    iget-object v0, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 342
    if-nez v0, :cond_0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 343
    const-string v2, "?userid=%s&deviceid=%s&device=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    const/4 v0, 0x1

    iget-object v4, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lflipboard/service/Flap;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    new-array v0, v5, [Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lflipboard/service/Flap;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 342
    :cond_0
    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    goto :goto_0
.end method
