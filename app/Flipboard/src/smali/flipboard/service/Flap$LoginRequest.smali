.class Lflipboard/service/Flap$LoginRequest;
.super Lflipboard/service/Flap$AccountRequest;
.source "Flap.java"


# instance fields
.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Z

.field j:Z

.field final synthetic k:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1864
    iput-object p1, p0, Lflipboard/service/Flap$LoginRequest;->k:Lflipboard/service/Flap;

    .line 1865
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$AccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1866
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1887
    iget-object v0, p0, Lflipboard/service/Flap$LoginRequest;->k:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/login"

    iget-object v2, p0, Lflipboard/service/Flap$LoginRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "username"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/service/Flap$LoginRequest;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "password"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$LoginRequest;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1888
    iget-boolean v1, p0, Lflipboard/service/Flap$LoginRequest;->i:Z

    if-eqz v1, :cond_0

    .line 1889
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&forgetCurrentAccount=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1891
    :cond_0
    iget-boolean v1, p0, Lflipboard/service/Flap$LoginRequest;->j:Z

    if-eqz v1, :cond_1

    .line 1892
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&migrateCurrentAccount=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1894
    :cond_1
    return-object v0
.end method

.method login(Ljava/lang/String;Ljava/lang/String;ZZLflipboard/service/Flap$AccountRequestObserver;)Lflipboard/service/Flap$LoginRequest;
    .locals 0

    .prologue
    .line 1870
    iput-object p5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    .line 1871
    iput-object p1, p0, Lflipboard/service/Flap$LoginRequest;->g:Ljava/lang/String;

    .line 1872
    iput-object p2, p0, Lflipboard/service/Flap$LoginRequest;->h:Ljava/lang/String;

    .line 1873
    iput-boolean p3, p0, Lflipboard/service/Flap$LoginRequest;->i:Z

    .line 1874
    iput-boolean p4, p0, Lflipboard/service/Flap$LoginRequest;->j:Z

    .line 1875
    invoke-virtual {p0}, Lflipboard/service/Flap$LoginRequest;->c()V

    .line 1876
    return-object p0
.end method
