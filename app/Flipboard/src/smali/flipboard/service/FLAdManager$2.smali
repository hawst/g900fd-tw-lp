.class Lflipboard/service/FLAdManager$2;
.super Ljava/lang/Object;
.source "FLAdManager.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/Ad;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/FLAdManager;


# direct methods
.method constructor <init>(Lflipboard/service/FLAdManager;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 328
    check-cast p1, Lflipboard/objs/Ad;

    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-static {v1}, Lflipboard/service/FLAdManager;->a(Lflipboard/service/FLAdManager;)Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v1

    if-ne p0, v1, :cond_4

    iget-object v1, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-static {}, Lflipboard/service/FLAdManager;->a()Landroid/graphics/Point;

    move-result-object v1

    iget-object v3, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-static {v4}, Lflipboard/service/FLAdManager;->a(Lflipboard/service/FLAdManager;)Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v4

    if-ne p0, v4, :cond_3

    iget-object v4, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-static {v4}, Lflipboard/service/FLAdManager;->b(Lflipboard/service/FLAdManager;)Lflipboard/service/Flap$TypedResultObserver;

    if-eqz p1, :cond_3

    iget-object v4, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-static {v4}, Lflipboard/service/FLAdManager;->c(Lflipboard/service/FLAdManager;)Lflipboard/service/FLAdManager$AdAsset;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p1, Lflipboard/objs/Ad;->j:J

    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v1, v4}, Lflipboard/objs/Ad;->a(IIZ)Lflipboard/objs/Ad$Asset;

    move-result-object v0

    new-instance v1, Lflipboard/service/FLAdManager$AdAsset;

    invoke-direct {v1, p1, v0}, Lflipboard/service/FLAdManager$AdAsset;-><init>(Lflipboard/objs/Ad;Lflipboard/objs/Ad$Asset;)V

    iget-object v0, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Lflipboard/service/FLAdManager;Lflipboard/service/FLAdManager$AdAsset;)Lflipboard/service/FLAdManager$AdAsset;

    iget-object v0, v1, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    const/4 v1, 0x1

    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/service/FLAdManager$2;->a:Lflipboard/service/FLAdManager;

    sget-object v3, Lflipboard/service/FLAdManager$Message;->a:Lflipboard/service/FLAdManager$Message;

    invoke-virtual {v1, v3, p1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    sget-object v1, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/objs/Ad$Asset;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v2}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    new-instance v1, Lflipboard/service/FLAdManager$2$1;

    invoke-direct {v1, p0}, Lflipboard/service/FLAdManager$2$1;-><init>(Lflipboard/service/FLAdManager$2;)V

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    if-eqz p1, :cond_0

    iget-object v1, p1, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    sget-object v3, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    iget-object v4, p1, Lflipboard/objs/Ad;->d:Ljava/util/List;

    invoke-static {v1, v3, v4}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 380
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const-string v1, "QUERY FAIL %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    return-void
.end method
