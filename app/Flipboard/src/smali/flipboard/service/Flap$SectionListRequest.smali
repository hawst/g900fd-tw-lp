.class public Lflipboard/service/Flap$SectionListRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Lflipboard/service/Flap$SectionListObserver;

.field final synthetic d:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 3792
    iput-object p1, p0, Lflipboard/service/Flap$SectionListRequest;->d:Lflipboard/service/Flap;

    .line 3793
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3794
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$SectionListRequest;
    .locals 1

    .prologue
    .line 3807
    const-string v0, "/v1/social/sectionList"

    iput-object v0, p0, Lflipboard/service/Flap$SectionListRequest;->a:Ljava/lang/String;

    .line 3808
    const-string v0, "friends"

    iput-object v0, p0, Lflipboard/service/Flap$SectionListRequest;->b:Ljava/lang/String;

    .line 3809
    iput-object p1, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    .line 3810
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3811
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$SectionListRequest;
    .locals 0

    .prologue
    .line 3798
    iput-object p1, p0, Lflipboard/service/Flap$SectionListRequest;->a:Ljava/lang/String;

    .line 3799
    iput-object p2, p0, Lflipboard/service/Flap$SectionListRequest;->b:Ljava/lang/String;

    .line 3800
    iput-object p3, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    .line 3801
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3802
    return-object p0
.end method

.method protected final a()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 3817
    iget-object v0, p0, Lflipboard/service/Flap$SectionListRequest;->d:Lflipboard/service/Flap;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/Flap$SectionListRequest;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/Flap$SectionListRequest;->n:Lflipboard/service/User;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3818
    iget-object v1, p0, Lflipboard/service/Flap$SectionListRequest;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3819
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pageKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Flap$SectionListRequest;->b:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3821
    :cond_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v4

    .line 3823
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3824
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3825
    iget-object v0, p0, Lflipboard/service/Flap$SectionListRequest;->d:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3826
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3827
    new-instance v3, Lflipboard/service/Flap$SectionListInputStream;

    iget-object v4, p0, Lflipboard/service/Flap$SectionListRequest;->d:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$SectionListRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lflipboard/service/Flap$SectionListInputStream;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    .line 3828
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3829
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    .line 3830
    iget-object v0, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3841
    iput-object v5, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    .line 3842
    :goto_0
    return-void

    .line 3833
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$SectionListInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListResult;

    .line 3834
    iget-object v1, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3841
    iput-object v5, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    .line 3835
    :catch_0
    move-exception v0

    .line 3836
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3841
    iput-object v5, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    .line 3837
    :catch_1
    move-exception v0

    .line 3838
    :try_start_3
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3839
    iget-object v1, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3841
    iput-object v5, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$SectionListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    throw v0
.end method
