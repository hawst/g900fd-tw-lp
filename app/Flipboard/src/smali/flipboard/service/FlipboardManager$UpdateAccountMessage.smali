.class public final enum Lflipboard/service/FlipboardManager$UpdateAccountMessage;
.super Ljava/lang/Enum;
.source "FlipboardManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/FlipboardManager$UpdateAccountMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

.field public static final enum b:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

.field public static final enum c:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

.field private static final synthetic d:[Lflipboard/service/FlipboardManager$UpdateAccountMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 418
    new-instance v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$UpdateAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3}, Lflipboard/service/FlipboardManager$UpdateAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->b:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    const-string v1, "FAILED_WITH_MESSAGE"

    invoke-direct {v0, v1, v4}, Lflipboard/service/FlipboardManager$UpdateAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->c:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    sget-object v1, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->b:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->c:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->d:[Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 418
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/FlipboardManager$UpdateAccountMessage;
    .locals 1

    .prologue
    .line 418
    const-class v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/service/FlipboardManager$UpdateAccountMessage;
    .locals 1

    .prologue
    .line 418
    sget-object v0, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->d:[Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    invoke-virtual {v0}, [Lflipboard/service/FlipboardManager$UpdateAccountMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    return-object v0
.end method
