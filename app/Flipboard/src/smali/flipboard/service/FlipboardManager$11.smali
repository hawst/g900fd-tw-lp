.class Lflipboard/service/FlipboardManager$11;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/Flap$CommentaryObserver;


# instance fields
.field final synthetic a:Lflipboard/service/Flap$CommentaryObserver;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:J

.field final synthetic d:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/service/Flap$CommentaryObserver;Ljava/util/Map;J)V
    .locals 0

    .prologue
    .line 1152
    iput-object p1, p0, Lflipboard/service/FlipboardManager$11;->d:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$11;->a:Lflipboard/service/Flap$CommentaryObserver;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$11;->b:Ljava/util/Map;

    iput-wide p4, p0, Lflipboard/service/FlipboardManager$11;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1152
    check-cast p1, Lflipboard/objs/CommentaryResult;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$11;->a:Lflipboard/service/Flap$CommentaryObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager$11;->a:Lflipboard/service/Flap$CommentaryObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/Object;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-nez v0, :cond_3

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    :cond_2
    return-void

    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v11, [Ljava/lang/Object;

    iget-object v1, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    aput-object v1, v0, v10

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$11;->b:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/HasCommentaryItem;

    if-nez v1, :cond_4

    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v3, "Null item for %s"

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    aput-object v0, v4, v10

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    iget-wide v4, p0, Lflipboard/service/FlipboardManager$11;->c:J

    iget-wide v6, v0, Lflipboard/objs/CommentaryResult$Item;->j:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-virtual {v1, v0, v4, v5}, Lflipboard/objs/HasCommentaryItem;->a(Lflipboard/objs/CommentaryResult$Item;J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1180
    iget-object v0, p0, Lflipboard/service/FlipboardManager$11;->a:Lflipboard/service/Flap$CommentaryObserver;

    if-eqz v0, :cond_0

    .line 1181
    iget-object v0, p0, Lflipboard/service/FlipboardManager$11;->a:Lflipboard/service/Flap$CommentaryObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V

    .line 1183
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "Failed to get commentary: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1184
    return-void
.end method
