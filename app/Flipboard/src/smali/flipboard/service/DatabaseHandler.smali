.class public abstract Lflipboard/service/DatabaseHandler;
.super Ljava/lang/Object;
.source "DatabaseHandler.java"


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Ljava/lang/String;

.field protected g:Landroid/database/sqlite/SQLiteDatabase;

.field public h:Landroid/database/Cursor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 149
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DatabaseHandler with existing cursor will have cursor overwritten"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "DatabaseHandler with existing cursor will have cursor overwritten"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    :cond_1
    iput-object p1, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    .line 155
    return-void
.end method

.method private f(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v2}, Ljava/util/HashMap;-><init>(IF)V

    iput-object v0, p0, Lflipboard/service/DatabaseHandler;->a:Ljava/util/HashMap;

    .line 58
    :cond_0
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 59
    if-nez v0, :cond_1

    .line 60
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 61
    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->a:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 24
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 25
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 26
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "FAILED TO INSERT ROW: table=%s, values=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    aput-object v5, v4, v6

    aput-object p1, v4, v7

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    :cond_0
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 29
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "row id too larger for int: %d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 31
    :cond_1
    long-to-int v0, v0

    return v0
.end method

.method public abstract a()V
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/service/DatabaseHandler;->a(Landroid/database/Cursor;)V

    .line 37
    return-void
.end method

.method protected final a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 116
    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 119
    :cond_0
    return v0
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 47
    iget-object v2, p0, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "SELECT name FROM sqlite_master WHERE type=\'table\' AND name=?"

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 48
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 49
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 50
    return v0

    :cond_0
    move v0, v1

    .line 48
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lflipboard/service/DatabaseHandler;->f(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lflipboard/service/DatabaseHandler;->f(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 76
    :cond_0
    const-string v1, "unwanted.database_get_string_returned_null_value"

    invoke-static {v1}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final b()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/DatabaseHandler;->a(Landroid/database/Cursor;)V

    .line 145
    :cond_0
    return-void
.end method

.method protected final b(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 125
    iget-object v2, p0, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v3, p0, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 126
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "FAILED TO DELETE ROW: table=%s, where=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    aput-object v5, v4, v0

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lflipboard/service/DatabaseHandler;->f(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lflipboard/service/DatabaseHandler;->f(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 92
    :goto_0
    return v0

    .line 90
    :cond_0
    const-string v1, "unwanted.database_get_int_returned_null_value"

    invoke-static {v1}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final c(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 101
    iget-object v1, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lflipboard/service/DatabaseHandler;->f(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    iget-object v0, p0, Lflipboard/service/DatabaseHandler;->h:Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lflipboard/service/DatabaseHandler;->f(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    .line 104
    :cond_0
    const-string v1, "unwanted.database_get_blob_returned_null_value"

    invoke-static {v1}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 111
    invoke-virtual {p0, p1}, Lflipboard/service/DatabaseHandler;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
