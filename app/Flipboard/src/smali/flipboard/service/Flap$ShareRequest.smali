.class public Lflipboard/service/Flap$ShareRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field final a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/service/Flap$JSONResultObserver;

.field final c:Lflipboard/service/Section;

.field final synthetic d:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 3617
    iput-object p1, p0, Lflipboard/service/Flap$ShareRequest;->d:Lflipboard/service/Flap;

    .line 3618
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3619
    iput-object p4, p0, Lflipboard/service/Flap$ShareRequest;->a:Lflipboard/objs/FeedItem;

    .line 3620
    iput-object p3, p0, Lflipboard/service/Flap$ShareRequest;->c:Lflipboard/service/Section;

    .line 3621
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ShareRequest;
    .locals 0

    .prologue
    .line 3625
    iput-object p1, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    .line 3626
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3627
    return-object p0
.end method

.method protected final a()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 3633
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 3634
    iget-object v0, p0, Lflipboard/service/Flap$ShareRequest;->d:Lflipboard/service/Flap;

    const-string v1, "/v1/social/share"

    iget-object v4, p0, Lflipboard/service/Flap$ShareRequest;->n:Lflipboard/service/User;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "oid"

    aput-object v6, v5, v9

    iget-object v6, p0, Lflipboard/service/Flap$ShareRequest;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v6}, Lflipboard/objs/FeedItem;->m()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    const/4 v6, 0x2

    const-string v7, "service"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lflipboard/service/Flap$ShareRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v7, v7, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v1, v4, v5}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3635
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v10, [Ljava/lang/Object;

    aput-object v0, v1, v9

    .line 3637
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3638
    const-string v0, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3639
    iget-object v0, p0, Lflipboard/service/Flap$ShareRequest;->d:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3640
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 3641
    new-instance v5, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$ShareRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v5, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3642
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3643
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 3644
    iget-object v0, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response from flap: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3664
    iput-object v8, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    .line 3665
    :goto_0
    return-void

    .line 3647
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3648
    if-eqz v0, :cond_2

    .line 3649
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3650
    const-string v1, "shared"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, Lflipboard/service/Flap$ShareRequest;->c:Lflipboard/service/Section;

    iget-object v5, p0, Lflipboard/service/Flap$ShareRequest;->a:Lflipboard/objs/FeedItem;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V

    .line 3651
    iget-object v1, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3664
    :goto_1
    iput-object v8, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3653
    :cond_1
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3658
    :catch_0
    move-exception v0

    .line 3659
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3664
    iput-object v8, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3656
    :cond_2
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 3660
    :catch_1
    move-exception v0

    .line 3661
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3662
    iget-object v1, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3664
    iput-object v8, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v8, p0, Lflipboard/service/Flap$ShareRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method
