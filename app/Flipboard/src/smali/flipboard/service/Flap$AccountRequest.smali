.class public abstract Lflipboard/service/Flap$AccountRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# static fields
.field static final synthetic e:Z


# instance fields
.field public a:Lflipboard/service/Flap$AccountRequestObserver;

.field b:Lflipboard/service/Flap$StreamingJSONInputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$StreamingJSONInputStream",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:J

.field final synthetic f:Lflipboard/service/Flap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1456
    const-class v0, Lflipboard/service/Flap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/service/Flap$AccountRequest;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1464
    iput-object p1, p0, Lflipboard/service/Flap$AccountRequest;->f:Lflipboard/service/Flap;

    .line 1465
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1466
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1498
    sget-boolean v0, Lflipboard/service/Flap$AccountRequest;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1499
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/service/Flap$AccountRequest;->d:J

    .line 1500
    invoke-virtual {p0}, Lflipboard/service/Flap$AccountRequest;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->f:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    new-instance v4, Lflipboard/service/Flap$AccountRequest$1;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$AccountRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v4, p0, v0}, Lflipboard/service/Flap$AccountRequest$1;-><init>(Lflipboard/service/Flap$AccountRequest;Ljava/io/InputStream;)V

    iput-object v4, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    sparse-switch v3, :sswitch_data_0

    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/Flap$AccountRequestObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iput-object v5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    :goto_0
    return-void

    :sswitch_0
    :try_start_1
    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    invoke-virtual {v0}, Lflipboard/service/Flap$AccountRequestObserver;->a()V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iput-object v5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    goto :goto_0

    :sswitch_1
    :try_start_2
    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    invoke-virtual {v0}, Lflipboard/service/Flap$StreamingJSONInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserInfo;

    iget-object v1, v0, Lflipboard/objs/UserInfo;->p:Lflipboard/json/FLObject;

    invoke-static {v1}, Lflipboard/abtest/Experiments;->a(Lflipboard/json/FLObject;)V

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-virtual {v0, v1}, Lflipboard/service/Flap$AccountRequestObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget-boolean v0, p0, Lflipboard/service/Flap$AccountRequest;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    invoke-virtual {v0}, Lflipboard/service/Flap$StreamingJSONInputStream;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;
    :try_end_3
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_1
    iput-object v5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    goto :goto_0

    :cond_2
    :try_start_4
    iget-boolean v1, p0, Lflipboard/service/Flap$AccountRequest;->c:Z

    if-eqz v1, :cond_4

    iget-object v0, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    const-string v1, "request was canceled"

    invoke-virtual {v0, v1}, Lflipboard/service/Flap$AccountRequestObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_5
    iget-boolean v1, p0, Lflipboard/service/Flap$AccountRequest;->c:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    invoke-virtual {v1}, Lflipboard/service/Flap$StreamingJSONInputStream;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    :cond_3
    throw v0
    :try_end_5
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/Flap$AccountRequestObserver;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    iput-object v5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    goto :goto_0

    :cond_4
    :try_start_7
    iget-boolean v1, v0, Lflipboard/objs/UserInfo;->b:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iget v2, v0, Lflipboard/objs/UserInfo;->f:I

    iget-object v3, v0, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v0, v0, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v2, v0}, Lflipboard/service/Flap$AccountRequestObserver;->a(ILjava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iget-object v2, v0, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    if-eqz v2, :cond_7

    iget-object v0, v0, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    :cond_7
    invoke-virtual {v1, v0}, Lflipboard/service/Flap$AccountRequestObserver;->a(Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_8
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/Flap$AccountRequestObserver;->a(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    iput-object v5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x1a2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public cancel()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1566
    iget-boolean v1, p0, Lflipboard/service/Flap$AccountRequest;->c:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    if-eqz v1, :cond_0

    .line 1567
    iput-boolean v0, p0, Lflipboard/service/Flap$AccountRequest;->c:Z

    .line 1568
    iget-object v1, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    invoke-virtual {v1}, Lflipboard/service/Flap$StreamingJSONInputStream;->b()V

    .line 1569
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/Flap$AccountRequest;->b:Lflipboard/service/Flap$StreamingJSONInputStream;

    .line 1572
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
