.class public abstract Lflipboard/service/Flap$StreamingJSONInputStream;
.super Ljava/io/FilterInputStream;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/io/FilterInputStream;"
    }
.end annotation


# instance fields
.field b:[B

.field c:I

.field d:I

.field e:Lflipboard/json/JSONParser;

.field f:I

.field protected g:Z


# direct methods
.method constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 4375
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/service/Flap$StreamingJSONInputStream;-><init>(Ljava/io/InputStream;Z)V

    .line 4376
    return-void
.end method

.method constructor <init>(Ljava/io/InputStream;Z)V
    .locals 1

    .prologue
    .line 4380
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4366
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->b:[B

    .line 4381
    iput-boolean p2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->g:Z

    .line 4382
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected final a([BII)V
    .locals 1

    .prologue
    .line 4386
    iget-object v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->e:Lflipboard/json/JSONParser;

    if-nez v0, :cond_0

    .line 4387
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p1, p2, p3}, Lflipboard/json/JSONParser;-><init>([BII)V

    iput-object v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->e:Lflipboard/json/JSONParser;

    .line 4391
    :goto_0
    return-void

    .line 4389
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->e:Lflipboard/json/JSONParser;

    invoke-virtual {v0, p1, p2, p3}, Lflipboard/json/JSONParser;->a([BII)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 4395
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "Flap:closeAsynchronously"

    new-instance v2, Lflipboard/service/Flap$StreamingJSONInputStream$1;

    invoke-direct {v2, p0}, Lflipboard/service/Flap$StreamingJSONInputStream$1;-><init>(Lflipboard/service/Flap$StreamingJSONInputStream;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 4406
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 4412
    iget-object v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->b:[B

    .line 4416
    :goto_0
    :try_start_0
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    :goto_1
    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    if-ge v1, v2, :cond_2

    .line 4417
    aget-byte v2, v0, v1

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    .line 4418
    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    sub-int v2, v1, v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 4420
    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    .line 4416
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4423
    :cond_1
    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    iget v3, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    sub-int v3, v1, v3

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/service/Flap$StreamingJSONInputStream;->a([BII)V

    .line 4424
    invoke-virtual {p0}, Lflipboard/service/Flap$StreamingJSONInputStream;->a()Ljava/lang/Object;

    move-result-object v0

    .line 4425
    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    .line 4456
    :goto_2
    return-object v0

    .line 4431
    :cond_2
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    if-lez v1, :cond_5

    .line 4433
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    if-le v1, v2, :cond_3

    .line 4434
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    iget v4, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4436
    :cond_3
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    sub-int/2addr v1, v2

    iput v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    .line 4437
    const/4 v1, 0x0

    iput v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    .line 4445
    :cond_4
    :goto_3
    iget-object v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->in:Ljava/io/InputStream;

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    array-length v3, v0

    iget v4, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 4446
    if-gtz v1, :cond_8

    .line 4447
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    if-ge v1, v2, :cond_7

    .line 4448
    iget-boolean v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->g:Z

    if-eqz v1, :cond_6

    .line 4449
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    iget v3, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/service/Flap$StreamingJSONInputStream;->a([BII)V

    .line 4450
    iget v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    iput v0, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    .line 4451
    invoke-virtual {p0}, Lflipboard/service/Flap$StreamingJSONInputStream;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 4438
    :cond_5
    iget v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    array-length v2, v0

    if-ne v1, v2, :cond_4

    .line 4440
    const/4 v2, 0x0

    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [B

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4441
    iput-object v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->b:[B

    move-object v0, v1

    goto :goto_3

    .line 4453
    :cond_6
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected EOF while "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    iget v3, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->c:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes to spare"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4461
    :catch_0
    move-exception v0

    .line 4463
    new-instance v1, Ljava/io/IOException;

    const-string v2, "canceled zip stream error"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 4455
    :cond_7
    :try_start_1
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 4456
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 4458
    :cond_8
    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    add-int/2addr v2, v1

    iput v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->d:I

    .line 4459
    iget v2, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->f:I

    add-int/2addr v1, v2

    iput v1, p0, Lflipboard/service/Flap$StreamingJSONInputStream;->f:I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
