.class public Lflipboard/service/Flap$PromoteToCoverRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Lflipboard/objs/FeedItem;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4684
    iput-object p1, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->b:Lflipboard/service/Flap;

    .line 4685
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4686
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$PromoteToCoverRequest;
    .locals 3

    .prologue
    .line 4690
    if-eqz p2, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4691
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 4693
    :cond_0
    iput-object p1, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->c:Ljava/lang/String;

    .line 4694
    iput-object p2, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->d:Lflipboard/objs/FeedItem;

    .line 4695
    iput-object p3, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4696
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4697
    return-object p0
.end method

.method protected final a()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 4702
    iget-object v0, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->d:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v0

    .line 4703
    if-eqz v0, :cond_0

    .line 4705
    iget-object v1, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->b:Lflipboard/service/Flap;

    const-string v2, "/v1/curator/editMagazine"

    iget-object v3, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->n:Lflipboard/service/User;

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "target"

    aput-object v5, v4, v7

    iget-object v5, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->c:Ljava/lang/String;

    aput-object v5, v4, v8

    const-string v5, "key"

    aput-object v5, v4, v6

    const-string v5, "coverItemId"

    aput-object v5, v4, v9

    const-string v5, "key"

    aput-object v5, v4, v10

    const/4 v5, 0x5

    const-string v6, "imageURL"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "value"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    iget-object v6, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->d:Lflipboard/objs/FeedItem;

    iget-object v6, v6, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "value"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4709
    :goto_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 4710
    iget-object v1, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->b:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4711
    return-void

    .line 4707
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/editMagazine"

    iget-object v2, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "target"

    aput-object v4, v3, v7

    iget-object v4, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->c:Ljava/lang/String;

    aput-object v4, v3, v8

    const-string v4, "key"

    aput-object v4, v3, v6

    const-string v4, "coverItemId"

    aput-object v4, v3, v9

    const-string v4, "value"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$PromoteToCoverRequest;->d:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
