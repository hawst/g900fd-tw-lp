.class Lflipboard/service/User$31;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/User$StateChanger;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Z

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;ILflipboard/service/Section;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2181
    iput-object p1, p0, Lflipboard/service/User$31;->e:Lflipboard/service/User;

    iput p2, p0, Lflipboard/service/User$31;->a:I

    iput-object p3, p0, Lflipboard/service/User$31;->b:Lflipboard/service/Section;

    iput-boolean p4, p0, Lflipboard/service/User$31;->c:Z

    iput-object p5, p0, Lflipboard/service/User$31;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2185
    iget-object v0, p0, Lflipboard/service/User$31;->e:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    iget v1, p0, Lflipboard/service/User$31;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2186
    iget-object v0, p0, Lflipboard/service/User$31;->b:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/service/User$31;->e:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->o(Lflipboard/service/User;)Lflipboard/util/Observer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 2187
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/User$31;->b:Lflipboard/service/Section;

    aput-object v2, v0, v1

    .line 2188
    iget-boolean v0, p0, Lflipboard/service/User$31;->c:Z

    if-eqz v0, :cond_1

    .line 2189
    const/4 v0, 0x0

    .line 2190
    iget-object v1, p0, Lflipboard/service/User$31;->d:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2191
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2192
    const-string v1, "from"

    iget-object v2, p0, Lflipboard/service/User$31;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2194
    :cond_0
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->m:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v2, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/service/User$31;->b:Lflipboard/service/Section;

    .line 2195
    invoke-virtual {v3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/service/User$31;->b:Lflipboard/service/Section;

    .line 2196
    iget-object v3, v3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 2197
    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/service/User$31;->b:Lflipboard/service/Section;

    .line 2198
    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    .line 2199
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 2201
    :cond_1
    return v4
.end method
