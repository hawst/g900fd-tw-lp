.class Lflipboard/service/FlipboardManager$9;
.super Landroid/content/BroadcastReceiver;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lflipboard/service/FlipboardManager$9;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 830
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 831
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    .line 832
    iget-object v0, p0, Lflipboard/service/FlipboardManager$9;->a:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Z)Z

    .line 842
    :cond_0
    :goto_0
    return-void

    .line 833
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 834
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    goto :goto_0

    .line 835
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 836
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    .line 837
    iget-object v0, p0, Lflipboard/service/FlipboardManager$9;->a:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Z)Z

    .line 838
    iget-object v0, p0, Lflipboard/service/FlipboardManager$9;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->c(Lflipboard/service/FlipboardManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager$9;->a:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v0, :cond_0

    .line 839
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->g()V

    goto :goto_0
.end method
