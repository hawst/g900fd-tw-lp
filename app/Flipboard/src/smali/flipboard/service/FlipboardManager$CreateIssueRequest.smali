.class Lflipboard/service/FlipboardManager$CreateIssueRequest;
.super Lflipboard/service/Flap$Request;
.source "FlipboardManager.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Lflipboard/service/Flap$JSONResultObserver;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;

.field q:Ljava/lang/String;

.field r:Ljava/lang/String;

.field s:Ljava/lang/String;

.field final synthetic t:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 1

    .prologue
    .line 3630
    iput-object p1, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->t:Lflipboard/service/FlipboardManager;

    .line 3631
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3632
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3657
    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    .line 3659
    new-instance v2, Lflipboard/json/FLObject;

    invoke-direct {v2}, Lflipboard/json/FLObject;-><init>()V

    .line 3660
    const-string v0, "fields"

    invoke-virtual {v1, v0, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3662
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 3663
    const-string v3, "key"

    iget-object v4, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3664
    const-string v3, "project"

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3666
    const-string v0, "summary"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3668
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 3669
    const-string v3, "id"

    iget-object v4, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->f:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3670
    const-string v3, "issuetype"

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3672
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 3673
    const-string v3, "id"

    iget-object v4, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->g:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3674
    const-string v3, "priority"

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3676
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 3677
    const-string v0, ""

    iput-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    .line 3679
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->l:Ljava/lang/String;

    const/16 v4, 0xc8

    invoke-static {v3, v4}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    .line 3682
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->m:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->m:Ljava/lang/String;

    const/16 v4, 0x7d0

    invoke-static {v3, v4}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    .line 3686
    :cond_2
    const-string v0, "description"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3689
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 3690
    const-string v3, "id"

    iget-object v4, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->i:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3691
    const-string v3, "customfield_10537"

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3694
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_8

    const-string v0, "tablet,"

    .line 3695
    :goto_0
    const-string v3, "customfield_10531"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Android,"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3698
    const-string v0, "customfield_10533"

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3701
    const-string v0, "customfield_10530"

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3704
    const-string v0, "customfield_10630"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v4}, Lflipboard/app/FlipboardApplication;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v4}, Lflipboard/app/FlipboardApplication;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3711
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 3712
    if-eqz v0, :cond_3

    .line 3713
    const-string v3, "customfield_10532"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3717
    :cond_3
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3718
    const-string v0, "customfield_10534"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3722
    :cond_4
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3723
    const-string v0, "customfield_10538"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->k:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3727
    :cond_5
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->q:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3728
    const-string v0, "customfield_10539"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->q:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3732
    :cond_6
    const-string v0, "customfield_10535"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->h:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3735
    const-string v0, "customfield_10532"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->r:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3737
    const-string v0, "customfield_11030"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->s:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3739
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 3740
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 3741
    const-string v3, "name"

    iget-object v4, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3742
    const-string v3, "reporter"

    invoke-virtual {v2, v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 3748
    :cond_7
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "https://jira.flipboard.com/rest/api/2/issue"

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 3749
    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3750
    const-string v2, "Authorization"

    const-string v3, "Basic amlyYWJvdDpGIWlwcWF0M3N0"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3751
    const-string v2, "Content-Type"

    const-string v3, "application/json;charset=UTF-8"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3752
    new-instance v2, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "utf-8"

    invoke-direct {v2, v1, v3}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3753
    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 3755
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->t:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->a()Lorg/apache/http/protocol/HttpContext;

    invoke-virtual {v1, v0}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 3756
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3757
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 3758
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    sget-object v4, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v4, v0, v1}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 3760
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3762
    if-eqz v0, :cond_c

    .line 3763
    div-int/lit8 v1, v2, 0x64

    const/4 v2, 0x2

    if-eq v1, v2, :cond_b

    .line 3764
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3765
    const-string v1, "errorMessages"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 3766
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/json/FLObject;

    .line 3767
    invoke-virtual {v1}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3768
    const-string v1, "\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3786
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V

    throw v0
    :try_end_2
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 3788
    :catch_0
    move-exception v0

    .line 3789
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$DNSTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3796
    :goto_2
    return-void

    .line 3694
    :cond_8
    const-string v0, "phone,"

    goto/16 :goto_0

    .line 3770
    :cond_9
    :try_start_3
    const-string v1, "errors"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v1

    .line 3771
    invoke-virtual {v1}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3772
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3773
    const-string v5, " : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3774
    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3775
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 3777
    :cond_a
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->e:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3786
    :goto_4
    :try_start_4
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 3790
    :catch_1
    move-exception v0

    .line 3791
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 3779
    :cond_b
    :try_start_5
    iget-object v1, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->e:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    goto :goto_4

    .line 3782
    :cond_c
    iget-object v0, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->e:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from jira"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 3792
    :catch_2
    move-exception v0

    .line 3793
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 3794
    iget-object v1, p0, Lflipboard/service/FlipboardManager$CreateIssueRequest;->e:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    goto :goto_2
.end method
