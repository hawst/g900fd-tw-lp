.class public Lflipboard/service/Flap$SearchRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Lflipboard/service/Flap$SearchType;

.field c:Lflipboard/service/Flap$SearchObserver;

.field d:Ljava/lang/String;

.field e:I

.field f:J

.field final synthetic g:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2329
    iput-object p1, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    .line 2330
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2331
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2350
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->b:Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->b:Lflipboard/service/Flap$SearchType;

    if-ne v0, v1, :cond_0

    .line 2352
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    const-string v1, "/v1/social/sectionSearch"

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "q"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "categories"

    aput-object v4, v3, v5

    const-string v4, "medium"

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2362
    :goto_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 2365
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2366
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2368
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2369
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 2370
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_3

    .line 2371
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected response from flap: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    iget-wide v4, p0, Lflipboard/service/Flap$SearchRequest;->f:J

    invoke-interface {v0, v1, v2, v4, v5}, Lflipboard/service/Flap$SearchObserver;->a(Ljava/lang/Throwable;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2397
    iput-object v8, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    .line 2398
    :goto_1
    return-void

    .line 2353
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->b:Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    if-ne v0, v1, :cond_1

    .line 2355
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    const-string v1, "/v1/social/sectionSearch"

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "q"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "categories"

    aput-object v4, v3, v5

    const-string v4, "medium2"

    aput-object v4, v3, v9

    const/4 v4, 0x4

    const-string v5, "nostream"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "true"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2356
    :cond_1
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->b:Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->d:Lflipboard/service/Flap$SearchType;

    if-ne v0, v1, :cond_2

    .line 2357
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    const-string v1, "/v1/social/sectionSearch"

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->n:Lflipboard/service/User;

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "q"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "categories"

    aput-object v4, v3, v5

    const-string v4, "medium2"

    aput-object v4, v3, v9

    const/4 v4, 0x4

    const-string v5, "nostream"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "true"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "see_more"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Lflipboard/service/Flap$SearchRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2360
    :cond_2
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    const-string v1, "/v1/social/sectionSearch"

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->n:Lflipboard/service/User;

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "q"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2374
    :cond_3
    :try_start_1
    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->b:Lflipboard/service/Flap$SearchType;

    sget-object v3, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    if-ne v2, v3, :cond_5

    .line 2375
    new-instance v2, Lflipboard/service/Flap$SearchResultStreamer;

    iget-object v3, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$SearchRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lflipboard/service/Flap$SearchResultStreamer;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    .line 2376
    invoke-virtual {v2}, Lflipboard/service/Flap$SearchResultStreamer;->c()Ljava/util/List;

    move-result-object v0

    .line 2377
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "search category results size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2378
    iget-object v1, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    iget-wide v4, p0, Lflipboard/service/Flap$SearchRequest;->f:J

    invoke-interface {v1, v2, v0, v4, v5}, Lflipboard/service/Flap$SearchObserver;->a(Ljava/lang/String;Ljava/util/List;J)V

    .line 2393
    :cond_4
    :goto_2
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    iget-object v1, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    iget-wide v2, p0, Lflipboard/service/Flap$SearchRequest;->f:J

    invoke-interface {v0, v1, v2, v3}, Lflipboard/service/Flap$SearchObserver;->a(Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2397
    iput-object v8, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    goto/16 :goto_1

    .line 2379
    :cond_5
    :try_start_2
    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->b:Lflipboard/service/Flap$SearchType;

    sget-object v3, Lflipboard/service/Flap$SearchType;->d:Lflipboard/service/Flap$SearchType;

    if-ne v2, v3, :cond_6

    .line 2380
    new-instance v2, Lflipboard/service/Flap$SearchResultStreamer;

    iget-object v3, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$SearchRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lflipboard/service/Flap$SearchResultStreamer;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    .line 2381
    iget-object v0, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    iget-object v1, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lflipboard/service/Flap$SearchResultStreamer;->c()Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lflipboard/service/Flap$SearchRequest;->e:I

    iget-wide v4, p0, Lflipboard/service/Flap$SearchRequest;->f:J

    invoke-interface/range {v0 .. v5}, Lflipboard/service/Flap$SearchObserver;->a(Ljava/lang/String;Ljava/util/List;IJ)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2394
    :catch_0
    move-exception v0

    .line 2395
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    iget-object v2, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    iget-wide v4, p0, Lflipboard/service/Flap$SearchRequest;->f:J

    invoke-interface {v1, v0, v2, v4, v5}, Lflipboard/service/Flap$SearchObserver;->a(Ljava/lang/Throwable;Ljava/lang/String;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2397
    iput-object v8, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    goto/16 :goto_1

    .line 2383
    :cond_6
    :try_start_4
    new-instance v2, Lflipboard/service/Flap$SearchResultStreamer;

    iget-object v3, p0, Lflipboard/service/Flap$SearchRequest;->g:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$SearchRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lflipboard/service/Flap$SearchResultStreamer;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    .line 2385
    :goto_3
    invoke-virtual {v2}, Lflipboard/service/Flap$SearchResultStreamer;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_4

    .line 2387
    :try_start_5
    iget-object v1, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    iget-object v3, p0, Lflipboard/service/Flap$SearchRequest;->a:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Lflipboard/service/Flap$SearchObserver;->a(Ljava/lang/String;Lflipboard/objs/SearchResultItem;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 2388
    :catch_1
    move-exception v0

    .line 2389
    :try_start_6
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "error in flap notify: %3E"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 2397
    :catchall_0
    move-exception v0

    iput-object v8, p0, Lflipboard/service/Flap$SearchRequest;->c:Lflipboard/service/Flap$SearchObserver;

    throw v0
.end method
