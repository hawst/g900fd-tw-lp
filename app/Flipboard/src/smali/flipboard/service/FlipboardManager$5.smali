.class Lflipboard/service/FlipboardManager$5;
.super Landroid/os/AsyncTask;
.source "FlipboardManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/res/AssetManager;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Landroid/content/res/AssetManager;)V
    .locals 0

    .prologue
    .line 758
    iput-object p1, p0, Lflipboard/service/FlipboardManager$5;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$5;->a:Landroid/content/res/AssetManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 758
    iget-object v0, p0, Lflipboard/service/FlipboardManager$5;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$5;->a:Landroid/content/res/AssetManager;

    const-string v2, "fonts/FaktFlipboard-SemiBold.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->z:Landroid/graphics/Typeface;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$5;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$5;->a:Landroid/content/res/AssetManager;

    const-string v2, "fonts/FaktFlipboard-Light.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->y:Landroid/graphics/Typeface;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$5;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$5;->a:Landroid/content/res/AssetManager;

    const-string v2, "fonts/TiemposText-Regular.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->A:Landroid/graphics/Typeface;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$5;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$5;->a:Landroid/content/res/AssetManager;

    const-string v2, "fonts/TiemposText-RegularItalic.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->C:Landroid/graphics/Typeface;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$5;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$5;->a:Landroid/content/res/AssetManager;

    const-string v2, "fonts/TiemposText-Semibold.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->B:Landroid/graphics/Typeface;

    const/4 v0, 0x0

    return-object v0
.end method
