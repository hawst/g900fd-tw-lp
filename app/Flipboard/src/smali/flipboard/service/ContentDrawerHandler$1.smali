.class Lflipboard/service/ContentDrawerHandler$1;
.super Ljava/lang/Object;
.source "ContentDrawerHandler.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/ContentDrawerHandler;


# direct methods
.method constructor <init>(Lflipboard/service/ContentDrawerHandler;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 74
    check-cast p2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v0, Lflipboard/service/ContentDrawerHandler$7;->a:[I

    invoke-virtual {p2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V

    :cond_0
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->h:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_1
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V

    :cond_1
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->g:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v3}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_2
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iput-boolean v2, v0, Lflipboard/service/ContentDrawerHandler;->b:Z

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->f:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v3}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_3
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->f:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v3}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_4
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    monitor-exit v0

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->i:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_5
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->i:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_6
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iput-boolean v2, v0, Lflipboard/service/ContentDrawerHandler;->b:Z

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->j:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_4
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V

    :cond_4
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->g:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler$1;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v3}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
