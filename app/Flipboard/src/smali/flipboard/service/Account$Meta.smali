.class public Lflipboard/service/Account$Meta;
.super Lflipboard/objs/Base;
.source "Account.java"


# instance fields
.field transient a:Z

.field public b:Z

.field public c:Lflipboard/json/FLObject;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 52
    instance-of v1, p1, Lflipboard/service/Account$Meta;

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Lflipboard/service/Account$Meta;

    .line 54
    iget-boolean v1, p1, Lflipboard/service/Account$Meta;->b:Z

    iget-boolean v2, p0, Lflipboard/service/Account$Meta;->b:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    iget-object v2, p0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 56
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-boolean v0, p0, Lflipboard/service/Account$Meta;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/lit16 v0, v0, 0x229

    .line 47
    mul-int/lit8 v0, v0, 0x4f

    iget-object v2, p0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 48
    return v0

    :cond_1
    move v0, v1

    .line 45
    goto :goto_0
.end method
