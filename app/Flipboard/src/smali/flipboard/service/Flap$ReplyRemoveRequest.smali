.class Lflipboard/service/Flap$ReplyRemoveRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field final a:Lflipboard/objs/FeedItem;

.field final b:Ljava/lang/String;

.field c:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic d:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/objs/FeedItem;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2571
    iput-object p1, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->d:Lflipboard/service/Flap;

    .line 2572
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2573
    iput-object p3, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->a:Lflipboard/objs/FeedItem;

    .line 2574
    iput-object p4, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->b:Ljava/lang/String;

    .line 2575
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ReplyRemoveRequest;
    .locals 0

    .prologue
    .line 2579
    iput-object p1, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    .line 2580
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2581
    return-object p0
.end method

.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2587
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->d:Lflipboard/service/Flap;

    const-string v1, "/v1/social/replyRemove"

    iget-object v2, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "oid"

    aput-object v4, v3, v7

    iget-object v4, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v4}, Lflipboard/objs/FeedItem;->m()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    const/4 v4, 0x2

    const-string v5, "target"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2588
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 2590
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2591
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2592
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->d:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2593
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2594
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$ReplyRemoveRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2595
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2596
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2597
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2617
    iput-object v6, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    .line 2618
    :goto_0
    return-void

    .line 2600
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 2601
    if-eqz v0, :cond_2

    .line 2602
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2603
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2617
    :goto_1
    iput-object v6, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 2605
    :cond_1
    :try_start_2
    const-string v1, "errormessage"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2606
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2611
    :catch_0
    move-exception v0

    .line 2612
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2617
    iput-object v6, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 2609
    :cond_2
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 2613
    :catch_1
    move-exception v0

    .line 2614
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2615
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2617
    iput-object v6, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lflipboard/service/Flap$ReplyRemoveRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method
