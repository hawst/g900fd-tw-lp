.class Lflipboard/service/HintManager$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "HintManager.java"


# instance fields
.field final synthetic a:Lflipboard/objs/ConfigHints$Hint;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Lflipboard/service/HintManager;


# direct methods
.method constructor <init>(Lflipboard/service/HintManager;Lflipboard/objs/ConfigHints$Hint;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lflipboard/service/HintManager$1;->c:Lflipboard/service/HintManager;

    iput-object p2, p0, Lflipboard/service/HintManager$1;->a:Lflipboard/objs/ConfigHints$Hint;

    iput-object p3, p0, Lflipboard/service/HintManager$1;->b:Landroid/view/View;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 415
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->b(Landroid/support/v4/app/DialogFragment;)V

    .line 417
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 418
    const-string v1, "source"

    const-string v2, "hintDialog"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v1, p0, Lflipboard/service/HintManager$1;->a:Lflipboard/objs/ConfigHints$Hint;

    iget-object v1, v1, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/service/HintManager$1;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/HintManager$1;->a:Lflipboard/objs/ConfigHints$Hint;

    iget-object v2, v2, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lflipboard/service/FlipboardUrlHandler;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/os/Bundle;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    iget-object v0, p0, Lflipboard/service/HintManager$1;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lflipboard/service/HintManager$1;->a:Lflipboard/objs/ConfigHints$Hint;

    iget-object v3, v3, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 422
    :cond_0
    return-void
.end method

.method public final c(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 426
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->c(Landroid/support/v4/app/DialogFragment;)V

    .line 428
    iget-object v0, p0, Lflipboard/service/HintManager$1;->c:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/service/HintManager$1;->a:Lflipboard/objs/ConfigHints$Hint;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->a(Lflipboard/objs/ConfigHints$Hint;Z)V

    .line 429
    return-void
.end method
