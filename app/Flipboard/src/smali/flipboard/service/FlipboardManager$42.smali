.class Lflipboard/service/FlipboardManager$42;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Landroid/location/LocationManager;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Landroid/location/LocationManager;)V
    .locals 0

    .prologue
    .line 4066
    iput-object p1, p0, Lflipboard/service/FlipboardManager$42;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$42;->a:Landroid/location/LocationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 4069
    sget-object v0, Lflipboard/service/FlipboardManager;->m:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 4070
    iget-object v0, p0, Lflipboard/service/FlipboardManager$42;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$42;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->ak:Landroid/location/Location;

    invoke-static {p1, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/location/Location;Landroid/location/Location;)Landroid/location/Location;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->ak:Landroid/location/Location;

    .line 4071
    iget-object v0, p0, Lflipboard/service/FlipboardManager$42;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->s(Lflipboard/service/FlipboardManager;)I

    .line 4072
    iget-object v0, p0, Lflipboard/service/FlipboardManager$42;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->t(Lflipboard/service/FlipboardManager;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 4073
    iget-object v0, p0, Lflipboard/service/FlipboardManager$42;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 4075
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4086
    sget-object v0, Lflipboard/service/FlipboardManager;->m:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 4087
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4082
    sget-object v0, Lflipboard/service/FlipboardManager;->m:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 4083
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 4078
    sget-object v0, Lflipboard/service/FlipboardManager;->m:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 4079
    return-void
.end method
