.class Lflipboard/service/Flap$UserStateRequest;
.super Lflipboard/service/Flap$StateRequest;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/service/Flap$StateRequest",
        "<",
        "Lflipboard/objs/UserState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic f:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2071
    iput-object p1, p0, Lflipboard/service/Flap$UserStateRequest;->f:Lflipboard/service/Flap;

    .line 2072
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$StateRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2073
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/io/InputStream;)Lflipboard/objs/FlapObjectResult;
    .locals 1

    .prologue
    .line 2068
    new-instance v0, Lflipboard/service/Flap$UserStateRequest$1;

    invoke-direct {v0, p0, p1}, Lflipboard/service/Flap$UserStateRequest$1;-><init>(Lflipboard/service/Flap$UserStateRequest;Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lflipboard/service/Flap$StreamingJSONInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState;

    return-object v0
.end method

.method public final a(ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserState;",
            ">;)",
            "Lflipboard/service/Flap$StateRequest",
            "<",
            "Lflipboard/objs/UserState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2077
    const-string v0, "user"

    invoke-super {p0, v0, p1, p2}, Lflipboard/service/Flap$StateRequest;->a(Ljava/lang/String;ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lflipboard/objs/UserState;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/objs/UserState;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserState;",
            ">;)",
            "Lflipboard/service/Flap$StateRequest",
            "<",
            "Lflipboard/objs/UserState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2082
    const-string v0, "user"

    iget-object v1, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    invoke-virtual {v1}, Lflipboard/objs/UserState$Data;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lflipboard/objs/UserState;->a()I

    move-result v2

    invoke-super {p0, v0, v1, v2, p2}, Lflipboard/service/Flap$StateRequest;->a(Ljava/lang/String;Ljava/lang/String;ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;

    move-result-object v0

    return-object v0
.end method
