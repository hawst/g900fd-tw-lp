.class Lflipboard/service/FlipboardManager$33;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2807
    iput-object p1, p0, Lflipboard/service/FlipboardManager$33;->a:Lflipboard/service/FlipboardManager;

    const/4 v0, 0x0

    const/16 v1, 0x8

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2810
    const-string v0, "CREATE TABLE magazines(id INTEGER PRIMARY KEY, uid TEXT, contributor INTEGER, descriptor BLOB)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2812
    const-string v0, "CREATE TABLE accounts(id INTEGER PRIMARY KEY, uid TEXT, service TEXT, serviceId TEXT, name TEXT, screenName TEXT, email TEXT, image TEXT, profile TEXT, metaData BLOB, descriptor BLOB)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2814
    const-string v0, "CREATE TABLE sections(id INTEGER PRIMARY KEY, uid TEXT, service TEXT, sectionId TEXT, remoteId TEXT, title TEXT, image TEXT, private BOOL, unreadRemoteId TEXT, items BLOB, tocItem BLOB, pos INTEGER DEFAULT 0, metaData BLOB, descriptor BLOB)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2816
    const-string v0, "CREATE TABLE userstate(id INTEGER PRIMARY KEY, uid TEXT, state BLOB, syncNeeded INTEGER DEFAULT 0, lastRefresh INTEGER DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2817
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 2820
    packed-switch p2, :pswitch_data_0

    .line 2852
    :goto_0
    return-void

    .line 2823
    :pswitch_0
    const-string v0, "ALTER TABLE sections ADD COLUMN metaData BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2824
    const-string v0, "ALTER TABLE accounts ADD COLUMN metaData BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2825
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 2830
    :pswitch_1
    const-string v0, "ALTER TABLE sections ADD COLUMN descriptor BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2831
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 2836
    :pswitch_2
    const-string v0, "ALTER TABLE accounts ADD COLUMN descriptor BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2837
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 2844
    :pswitch_3
    :try_start_0
    const-string v0, "CREATE TABLE IF NOT EXISTS magazines(id INTEGER PRIMARY KEY, uid TEXT, contributor INTEGER, descriptor BLOB)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2845
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2848
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Error adding the magazines table"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2849
    const-string v0, "unwanted.migration_to_v8_failed"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 2820
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
