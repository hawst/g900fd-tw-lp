.class Lflipboard/service/FlipboardManager$3;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/io/NetworkManager;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lflipboard/service/FlipboardManager$3;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 591
    check-cast p2, Ljava/lang/Boolean;

    check-cast p3, Ljava/lang/Boolean;

    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/FlipboardManager$3;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager$3;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v0

    iget-object v1, v0, Lflipboard/service/User;->k:Lflipboard/objs/UserState;

    if-eqz v1, :cond_0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lflipboard/service/User;->a(J)V

    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$3;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/FlipboardManager;)V

    :cond_1
    return-void
.end method
