.class Lflipboard/service/Flap$ActivityRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:Lflipboard/service/Flap$CommentaryObserver;

.field final synthetic c:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2819
    iput-object p1, p0, Lflipboard/service/Flap$ActivityRequest;->c:Lflipboard/service/Flap;

    .line 2820
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2821
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$ActivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")",
            "Lflipboard/service/Flap$ActivityRequest;"
        }
    .end annotation

    .prologue
    .line 2825
    iput-object p1, p0, Lflipboard/service/Flap$ActivityRequest;->a:Ljava/util/List;

    .line 2826
    iput-object p2, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    .line 2827
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2828
    return-object p0
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2834
    iget-object v0, p0, Lflipboard/service/Flap$ActivityRequest;->c:Lflipboard/service/Flap;

    const-string v1, "/v1/social/activity"

    iget-object v2, p0, Lflipboard/service/Flap$ActivityRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "oid"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$ActivityRequest;->a:Ljava/util/List;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2835
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 2837
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2838
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2839
    iget-object v0, p0, Lflipboard/service/Flap$ActivityRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2840
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2841
    new-instance v3, Lflipboard/service/Flap$CommentaryInputStream;

    iget-object v4, p0, Lflipboard/service/Flap$ActivityRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$ActivityRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lflipboard/service/Flap$CommentaryInputStream;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    .line 2842
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2843
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2844
    iget-object v0, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2855
    iput-object v5, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    .line 2856
    :goto_0
    return-void

    .line 2847
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$CommentaryInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult;

    .line 2848
    iget-object v1, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2855
    iput-object v5, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    goto :goto_0

    .line 2849
    :catch_0
    move-exception v0

    .line 2850
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2855
    iput-object v5, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    goto :goto_0

    .line 2851
    :catch_1
    move-exception v0

    .line 2852
    :try_start_3
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2853
    iget-object v1, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2855
    iput-object v5, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$ActivityRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    throw v0
.end method
