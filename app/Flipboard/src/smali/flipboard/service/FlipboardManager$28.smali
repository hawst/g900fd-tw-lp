.class Lflipboard/service/FlipboardManager$28;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 2367
    iput-object p1, p0, Lflipboard/service/FlipboardManager$28;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2395
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading services: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2396
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2371
    :try_start_0
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 2372
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->j()Lflipboard/objs/ConfigServices;

    move-result-object v2

    .line 2373
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    .line 2374
    iget-object v0, v2, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2375
    const/4 v4, 0x1

    iput-boolean v4, v0, Lflipboard/objs/ConfigService;->bO:Z

    .line 2376
    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2388
    :catch_0
    move-exception v0

    .line 2389
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "failed to parse services.json: %-E"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2390
    throw v0

    .line 2378
    :cond_0
    :try_start_1
    iget-object v0, v2, Lflipboard/objs/ConfigServices;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2379
    iget-object v0, v2, Lflipboard/objs/ConfigServices;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2380
    const/4 v4, 0x1

    iput-boolean v4, v0, Lflipboard/objs/ConfigService;->bM:Z

    .line 2381
    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2383
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager$28;->a:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/objs/ConfigServices;->c:Ljava/util/List;

    invoke-static {v0, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Ljava/util/List;)Ljava/util/List;

    .line 2387
    :goto_2
    iget-object v0, p0, Lflipboard/service/FlipboardManager$28;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Ljava/util/concurrent/ConcurrentHashMap;)Ljava/util/concurrent/ConcurrentHashMap;

    .line 2391
    return-void

    .line 2385
    :cond_2
    iget-object v0, p0, Lflipboard/service/FlipboardManager$28;->a:Lflipboard/service/FlipboardManager;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Ljava/util/List;)Ljava/util/List;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2399
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading services, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2400
    return-void
.end method
