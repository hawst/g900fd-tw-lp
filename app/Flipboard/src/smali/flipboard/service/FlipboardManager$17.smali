.class Lflipboard/service/FlipboardManager$17;
.super Lflipboard/service/Flap$AccountRequestObserver;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/util/Observer;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V
    .locals 0

    .prologue
    .line 1483
    iput-object p1, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    invoke-direct {p0}, Lflipboard/service/Flap$AccountRequestObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1517
    iget-object v0, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->e(Lflipboard/service/FlipboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0332

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1518
    iget-object v1, p0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager$LoginMessage;->b:Lflipboard/service/FlipboardManager$LoginMessage;

    invoke-interface {v1, v2, v3, v0}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1519
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 1527
    iget-object v0, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->e(Lflipboard/service/FlipboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1528
    const/16 v1, 0x44e

    if-eq p1, v1, :cond_0

    const/16 v1, 0x453

    if-ne p1, v1, :cond_1

    .line 1529
    :cond_0
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1530
    iget-object v1, p0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager$LoginMessage;->b:Lflipboard/service/FlipboardManager$LoginMessage;

    invoke-interface {v1, v2, v3, v0}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1540
    :goto_0
    return-void

    .line 1531
    :cond_1
    const/16 v1, 0x834

    if-ne p1, v1, :cond_2

    .line 1532
    iget-object v0, p0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager$LoginMessage;->d:Lflipboard/service/FlipboardManager$LoginMessage;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1533
    :cond_2
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1534
    const v1, 0x7f0d011d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1535
    iget-object v1, p0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager$LoginMessage;->b:Lflipboard/service/FlipboardManager$LoginMessage;

    invoke-interface {v1, v2, v3, v0}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1537
    :cond_3
    const v1, 0x7f0d011c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1538
    iget-object v1, p0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager$LoginMessage;->b:Lflipboard/service/FlipboardManager$LoginMessage;

    invoke-interface {v1, v2, v3, v0}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1483
    check-cast p1, Lflipboard/objs/UserInfo;

    iget v0, p1, Lflipboard/objs/UserInfo;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lflipboard/service/User;

    invoke-direct {v1, v0}, Lflipboard/service/User;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    iget-object v2, p1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-virtual {v1, v0, v2}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p1, Lflipboard/objs/UserInfo;->n:Ljava/util/List;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    invoke-virtual {v1}, Lflipboard/service/User;->r()V

    new-instance v0, Lflipboard/service/FlipboardManager$17$1;

    invoke-direct {v0, p0}, Lflipboard/service/FlipboardManager$17$1;-><init>(Lflipboard/service/FlipboardManager$17;)V

    invoke-virtual {v1, v0}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    return-void
.end method
