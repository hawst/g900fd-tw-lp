.class Lflipboard/service/FlipboardManager$15;
.super Lflipboard/service/Flap$AccountRequestObserver;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/util/Observer;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V
    .locals 0

    .prologue
    .line 1401
    iput-object p1, p0, Lflipboard/service/FlipboardManager$15;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$15;->a:Lflipboard/util/Observer;

    invoke-direct {p0}, Lflipboard/service/Flap$AccountRequestObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 1406
    new-instance v0, Lflipboard/service/Flap$ErrorData;

    invoke-direct {v0, p1, p2}, Lflipboard/service/Flap$ErrorData;-><init>(ILjava/lang/String;)V

    .line 1407
    iget-object v1, p0, Lflipboard/service/FlipboardManager$15;->a:Lflipboard/util/Observer;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager$CreateAccountMessage;->b:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    invoke-interface {v1, v2, v3, v0}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1408
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1401
    check-cast p1, Lflipboard/objs/UserInfo;

    iget v0, p1, Lflipboard/objs/UserInfo;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Lflipboard/service/User;

    invoke-direct {v0, v1}, Lflipboard/service/User;-><init>(Ljava/lang/String;)V

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    :cond_0
    iget-object v1, p1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    iget-object v2, p1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v1, p0, Lflipboard/service/FlipboardManager$15;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->f:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-virtual {v1, v2, v0}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$15;->a:Lflipboard/util/Observer;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager$CreateAccountMessage;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    invoke-interface {v0, v1, v2, p1}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
