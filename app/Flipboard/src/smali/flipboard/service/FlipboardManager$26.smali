.class Lflipboard/service/FlipboardManager$26;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 0

    .prologue
    .line 2133
    iput-object p1, p0, Lflipboard/service/FlipboardManager$26;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$26;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 2136
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->b:Lflipboard/service/FlipboardManager;

    const-string v1, "access_token"

    invoke-virtual {p1, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Ljava/lang/String;)Ljava/lang/String;

    .line 2137
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->b:Lflipboard/service/FlipboardManager;

    const-string v1, "refresh_token"

    invoke-virtual {p1, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/FlipboardManager;Ljava/lang/String;)Ljava/lang/String;

    .line 2138
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Lflipboard/service/FlipboardManager;)V

    .line 2139
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->a:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2140
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    .line 2142
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2146
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "failure when requesting api token %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2147
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->a:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2148
    iget-object v0, p0, Lflipboard/service/FlipboardManager$26;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 2150
    :cond_0
    return-void
.end method
