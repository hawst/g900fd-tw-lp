.class public Lflipboard/service/HintManager;
.super Ljava/lang/Object;
.source "HintManager.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static b:Lflipboard/service/HintManager;


# instance fields
.field public c:Lflipboard/objs/ConfigHints;

.field public volatile d:Z

.field e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigHints$Hint;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:I

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "hint"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sput-object p0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/service/HintManager;->e:Ljava/util/HashMap;

    .line 61
    return-void
.end method

.method private a(Ljava/util/List;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigHints$Condition;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x3e8

    const/4 v5, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 237
    if-nez p1, :cond_0

    .line 246
    :goto_0
    return v5

    .line 240
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigHints$Condition;

    .line 241
    iget-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->b:Ljava/lang/String;

    if-eqz v1, :cond_2c

    iget-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->a:Ljava/lang/String;

    if-eqz v1, :cond_2c

    iget-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    if-eqz v1, :cond_2c

    iget-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->a:Ljava/lang/String;

    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->b:Ljava/lang/String;

    sget-object v8, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v9, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    if-eqz v1, :cond_2f

    if-eqz v4, :cond_2f

    const-string v9, "count"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1e

    const-string v4, "currentVersionLaunches"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Lflipboard/app/FlipboardApplication;->j()I

    move-result v1

    int-to-float v1, v1

    :goto_1
    iget v8, v0, Lflipboard/objs/ConfigHints$Condition;->d:F

    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, "<"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    cmpg-float v4, v1, v8

    if-gez v4, :cond_20

    move v4, v5

    :goto_2
    sget-object v9, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, v0, Lflipboard/objs/ConfigHints$Condition;->a:Ljava/lang/String;

    aput-object v10, v9, v6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v9, v5

    const/4 v1, 0x2

    iget-object v10, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    aput-object v10, v9, v1

    const/4 v1, 0x3

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v9, v1

    const/4 v1, 0x4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v9, v1

    if-eqz v4, :cond_2

    iget-object v0, v0, Lflipboard/objs/ConfigHints$Condition;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/service/HintManager;->a(Ljava/util/List;)Z

    move-result v4

    :cond_2
    :goto_3
    if-eqz v4, :cond_1

    goto :goto_0

    :cond_3
    const-string v4, "hasFlipboardAccount"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v1, v8, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    const-string v4, "numberOfTiles"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v1, v8, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    goto :goto_1

    :cond_6
    const-string v4, "canCompose"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/Account;

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/objs/ConfigService;->g:Z

    if-eqz v1, :cond_7

    move v1, v3

    goto/16 :goto_1

    :cond_8
    move v1, v2

    goto/16 :goto_1

    :cond_9
    const-string v4, "currentSectionIsPrivate"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v8}, Lflipboard/service/FlipboardManager;->C()Lflipboard/service/Section;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, v1, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v1, :cond_a

    move v1, v3

    goto/16 :goto_1

    :cond_a
    move v1, v2

    goto/16 :goto_1

    :cond_b
    const-string v4, "currentSectionIsPartner"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v8}, Lflipboard/service/FlipboardManager;->C()Lflipboard/service/Section;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    if-eqz v1, :cond_c

    move v1, v3

    goto/16 :goto_1

    :cond_c
    move v1, v2

    goto/16 :goto_1

    :cond_d
    const-string v4, "currentSection="

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0xf

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lflipboard/service/FlipboardManager;->C()Lflipboard/service/Section;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    move v1, v3

    goto/16 :goto_1

    :cond_e
    move v1, v2

    goto/16 :goto_1

    :cond_f
    const-string v4, "currentSectionHasPrefix:"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x18

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lflipboard/service/FlipboardManager;->C()Lflipboard/service/Section;

    move-result-object v4

    if-eqz v4, :cond_10

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    move v1, v3

    goto/16 :goto_1

    :cond_10
    move v1, v2

    goto/16 :goto_1

    :cond_11
    const-string v4, "appHasBeenUpdated"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-static {}, Lflipboard/app/FlipboardApplication;->i()I

    move-result v1

    invoke-static {}, Lflipboard/app/FlipboardApplication;->j()I

    move-result v4

    if-eq v1, v4, :cond_12

    move v1, v3

    goto/16 :goto_1

    :cond_12
    move v1, v2

    goto/16 :goto_1

    :cond_13
    const-string v4, "isPopoverShown"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget-boolean v1, v8, Lflipboard/service/FlipboardManager;->am:Z

    if-eqz v1, :cond_14

    move v1, v3

    goto/16 :goto_1

    :cond_14
    move v1, v2

    goto/16 :goto_1

    :cond_15
    const-string v4, "uuid"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    goto/16 :goto_1

    :cond_16
    const-string v4, "uiid"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    iget v1, v8, Lflipboard/service/FlipboardManager;->J:I

    int-to-float v1, v1

    goto/16 :goto_1

    :cond_17
    const-string v4, "appVariantIsChina"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v1, :cond_18

    move v1, v3

    goto/16 :goto_1

    :cond_18
    move v1, v2

    goto/16 :goto_1

    :cond_19
    const-string v4, "isSimplePhoneUI"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v1

    sget-object v4, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v1, v4, :cond_1a

    move v1, v2

    goto/16 :goto_1

    :cond_1a
    move v1, v3

    goto/16 :goto_1

    :cond_1b
    const-string v4, "numberOfMagazines"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1c

    move v1, v2

    goto/16 :goto_1

    :cond_1c
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    goto/16 :goto_1

    :cond_1d
    sget-object v4, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    iget-object v4, v4, Lflipboard/io/UsageManager;->j:Landroid/content/SharedPreferences;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "__count_"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    goto/16 :goto_1

    :cond_1e
    const-string v8, "timeInterval"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2f

    const-string v4, "timeSinceFirstLaunch"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {}, Lflipboard/app/FlipboardApplication;->k()J

    move-result-wide v10

    sub-long/2addr v8, v10

    div-long/2addr v8, v12

    long-to-float v1, v8

    goto/16 :goto_1

    :cond_1f
    sget-object v4, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v4, v4, Lflipboard/io/UsageManager;->j:Landroid/content/SharedPreferences;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, "__updated_"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v10, 0x0

    invoke-interface {v4, v1, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    sub-long/2addr v8, v10

    div-long/2addr v8, v12

    long-to-float v1, v8

    goto/16 :goto_1

    :cond_20
    move v4, v6

    goto/16 :goto_2

    :cond_21
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, ">"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    cmpl-float v4, v1, v8

    if-lez v4, :cond_22

    move v4, v5

    goto/16 :goto_2

    :cond_22
    move v4, v6

    goto/16 :goto_2

    :cond_23
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, "<="

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    cmpg-float v4, v1, v8

    if-gtz v4, :cond_24

    move v4, v5

    goto/16 :goto_2

    :cond_24
    move v4, v6

    goto/16 :goto_2

    :cond_25
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, ">="

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    cmpl-float v4, v1, v8

    if-ltz v4, :cond_26

    move v4, v5

    goto/16 :goto_2

    :cond_26
    move v4, v6

    goto/16 :goto_2

    :cond_27
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, "=="

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_28

    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, "="

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    :cond_28
    cmpl-float v4, v1, v8

    if-nez v4, :cond_29

    move v4, v5

    goto/16 :goto_2

    :cond_29
    move v4, v6

    goto/16 :goto_2

    :cond_2a
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    const-string v9, "!="

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2e

    cmpl-float v4, v1, v8

    if-eqz v4, :cond_2b

    move v4, v5

    goto/16 :goto_2

    :cond_2b
    move v4, v6

    goto/16 :goto_2

    :cond_2c
    sget-object v1, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    const-string v4, "bad EVAL: %s"

    new-array v8, v5, [Ljava/lang/Object;

    aput-object v0, v8, v6

    invoke-virtual {v1, v4, v8}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v6

    goto/16 :goto_3

    :cond_2d
    move v5, v6

    .line 246
    goto/16 :goto_0

    :cond_2e
    move v4, v6

    goto/16 :goto_2

    :cond_2f
    move v1, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lflipboard/service/HintManager;->e:Ljava/util/HashMap;

    const-string v1, "homeTabHint"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const-string v1, "homeTabHint"

    invoke-virtual {p0, v0, v1}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lflipboard/service/HintManager;->e:Ljava/util/HashMap;

    const-string v1, "searchTabHint"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const-string v1, "searchTabHint"

    invoke-virtual {p0, v0, v1}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/service/HintManager;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void
.end method

.method public final a(Lflipboard/objs/ConfigHints$Hint;Z)V
    .locals 3

    .prologue
    .line 462
    monitor-enter p0

    .line 463
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lflipboard/service/HintManager;->d:Z

    .line 464
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    if-eqz p2, :cond_0

    .line 467
    iget v0, p0, Lflipboard/service/HintManager;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/service/HintManager;->g:I

    .line 468
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/service/HintManager;->h:J

    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hint-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 472
    sget-object v1, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v1, v0}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 473
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->C:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 474
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 475
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 477
    :cond_0
    return-void

    .line 464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lflipboard/objs/ConfigHints;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 91
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 92
    iget-object v0, p1, Lflipboard/objs/ConfigHints;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigHints$Hint;

    .line 93
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "enable_all_hints"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v4

    :cond_1
    :goto_1
    if-nez v1, :cond_a

    .line 94
    iput-boolean v3, v0, Lflipboard/objs/ConfigHints$Hint;->d:Z

    goto :goto_0

    .line 93
    :cond_2
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->d:Z

    if-nez v1, :cond_10

    move v1, v3

    :goto_2
    if-eqz v1, :cond_3

    iget v7, v0, Lflipboard/objs/ConfigHints$Hint;->e:I

    if-ltz v7, :cond_3

    iget v7, v0, Lflipboard/objs/ConfigHints$Hint;->e:I

    if-ge v4, v7, :cond_3

    move v1, v3

    :cond_3
    if-eqz v1, :cond_4

    iget v7, v0, Lflipboard/objs/ConfigHints$Hint;->f:I

    if-ltz v7, :cond_4

    iget v7, v0, Lflipboard/objs/ConfigHints$Hint;->f:I

    if-gtz v7, :cond_4

    move v1, v3

    :cond_4
    if-eqz v1, :cond_5

    iget-object v7, v0, Lflipboard/objs/ConfigHints$Hint;->g:Ljava/util/List;

    if-eqz v7, :cond_5

    iget-object v7, v0, Lflipboard/objs/ConfigHints$Hint;->g:Ljava/util/List;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v1, v3

    :cond_5
    if-eqz v1, :cond_6

    iget-object v2, v0, Lflipboard/objs/ConfigHints$Hint;->h:Ljava/util/List;

    if-nez v2, :cond_7

    :cond_6
    iget-object v2, v0, Lflipboard/objs/ConfigHints$Hint;->i:Ljava/util/List;

    if-eqz v2, :cond_8

    :cond_7
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, Lflipboard/objs/ConfigHints$Hint;->h:Ljava/util/List;

    if-eqz v8, :cond_9

    iget-object v8, v0, Lflipboard/objs/ConfigHints$Hint;->h:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    move v1, v3

    :cond_8
    :goto_3
    if-eqz v1, :cond_1

    iget-object v2, v0, Lflipboard/objs/ConfigHints$Hint;->j:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lflipboard/objs/ConfigHints$Hint;->j:Ljava/util/List;

    invoke-direct {p0, v2}, Lflipboard/service/HintManager;->a(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1

    move v1, v3

    goto :goto_1

    :cond_9
    iget-object v7, v0, Lflipboard/objs/ConfigHints$Hint;->i:Ljava/util/List;

    if-eqz v7, :cond_8

    iget-object v7, v0, Lflipboard/objs/ConfigHints$Hint;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v1, v3

    goto :goto_3

    .line 97
    :cond_a
    iget-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->B:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 98
    iget-object v2, v0, Lflipboard/objs/ConfigHints$Hint;->B:Ljava/lang/String;

    .line 99
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 100
    if-nez v1, :cond_b

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 102
    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :cond_b
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_c
    iget-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->C:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->C:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 108
    if-eqz v1, :cond_d

    .line 109
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 110
    if-nez v2, :cond_e

    .line 111
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 112
    invoke-interface {v5, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_e
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 119
    :cond_f
    iput-object v5, p0, Lflipboard/service/HintManager;->f:Ljava/util/Map;

    .line 120
    iput-object p1, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    .line 121
    return-void

    :cond_10
    move v1, v4

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/service/HintManager;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public final b(Landroid/view/View;Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 178
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "show_hints"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/HintManager;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 185
    invoke-static {p1}, Lflipboard/util/AndroidUtil;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 186
    sget-object v0, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p2, v0, v3

    aput-object p1, v0, v2

    .line 187
    iget-object v0, p0, Lflipboard/service/HintManager;->f:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 188
    if-eqz v0, :cond_0

    .line 191
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigHints$Hint;

    .line 192
    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->d()I

    move-result v1

    if-nez v1, :cond_8

    move v1, v2

    :goto_1
    if-eqz v1, :cond_3

    iget-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->E:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/service/HintManager;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    if-eqz v1, :cond_b

    iget-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->p:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    iget v1, v1, Lflipboard/objs/ConfigHints;->a:F

    cmpl-float v1, v1, v12

    if-lez v1, :cond_9

    iget v1, p0, Lflipboard/service/HintManager;->g:I

    int-to-float v1, v1

    iget-object v5, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    iget v5, v5, Lflipboard/objs/ConfigHints;->a:F

    cmpl-float v1, v1, v5

    if-lez v1, :cond_9

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v5, "ignore_hint_session_freq_cap"

    invoke-interface {v1, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v1, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v5, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v5, v1, v3

    iget v5, p0, Lflipboard/service/HintManager;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    iget-object v5, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    iget v5, v5, Lflipboard/objs/ConfigHints;->a:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v1, v10

    :cond_3
    :goto_2
    move v1, v3

    :goto_3
    if-eqz v1, :cond_2

    .line 193
    sget-object v1, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    aput-object p2, v1, v3

    iget-object v4, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v4, v1, v2

    aput-object p1, v1, v10

    .line 194
    iget-boolean v1, p0, Lflipboard/service/HintManager;->d:Z

    if-nez v1, :cond_17

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lflipboard/service/HintManager;->d:Z

    if-nez v1, :cond_16

    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/service/HintManager;->d:Z

    move v1, v2

    :goto_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_5
    if-eqz v1, :cond_14

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    iget-boolean v4, v1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v4, :cond_13

    sget-object v4, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v5, v4, v3

    aput-object p1, v4, v2

    iget-object v4, v0, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/ConfigHints$Hint;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0300f3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/hints/PulseHintView;

    iput-object p1, v2, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    iput-object v0, v2, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    iput-object p0, v2, Lflipboard/gui/hints/PulseHintView;->i:Lflipboard/service/HintManager;

    iget-object v3, v2, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v4, v0, Lflipboard/objs/ConfigHints$Hint;->x:F

    iget v5, v2, Lflipboard/gui/hints/PulseHintView;->g:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v3, v0, Lflipboard/objs/ConfigHints$Hint;->y:F

    iget v4, v2, Lflipboard/gui/hints/PulseHintView;->g:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Lflipboard/gui/hints/PulseHintView;->h:I

    invoke-virtual {v0}, Lflipboard/objs/ConfigHints$Hint;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v4, v2, Lflipboard/gui/hints/PulseHintView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v0}, Lflipboard/objs/ConfigHints$Hint;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v4, v2, Lflipboard/gui/hints/PulseHintView;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget v3, v0, Lflipboard/objs/ConfigHints$Hint;->n:F

    cmpl-float v3, v3, v12

    if-lez v3, :cond_6

    iget-object v3, v2, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v2, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v4, v0, Lflipboard/objs/ConfigHints$Hint;->n:F

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    int-to-long v4, v4

    new-instance v6, Lflipboard/gui/hints/PulseHintView$1;

    invoke-direct {v6, v2}, Lflipboard/gui/hints/PulseHintView$1;-><init>(Lflipboard/gui/hints/PulseHintView;)V

    invoke-virtual {v3, v4, v5, v6}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    iget v3, v0, Lflipboard/objs/ConfigHints$Hint;->v:F

    cmpl-float v3, v3, v12

    if-lez v3, :cond_7

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v4, v0, Lflipboard/objs/ConfigHints$Hint;->n:F

    iget v0, v0, Lflipboard/objs/ConfigHints$Hint;->v:F

    add-float/2addr v0, v4

    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v4

    float-to-int v0, v0

    int-to-long v4, v0

    new-instance v0, Lflipboard/gui/hints/PulseHintView$2;

    invoke-direct {v0, v2}, Lflipboard/gui/hints/PulseHintView$2;-><init>(Lflipboard/gui/hints/PulseHintView;)V

    invoke-virtual {v3, v4, v5, v0}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    :cond_7
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v0, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v0}, Lflipboard/activities/FlipboardActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_8
    move v1, v3

    .line 192
    goto/16 :goto_1

    :cond_9
    iget-wide v6, p0, Lflipboard/service/HintManager;->h:J

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_b

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lflipboard/service/HintManager;->h:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-float v1, v6

    iget-object v5, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    iget v5, v5, Lflipboard/objs/ConfigHints;->b:F

    cmpg-float v1, v1, v5

    if-gtz v1, :cond_b

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v5, "ignore_hint_time_freq_cap"

    invoke-interface {v1, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_a

    sget-object v1, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v5, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v5, v1, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lflipboard/service/HintManager;->h:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    iget-object v5, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    iget v5, v5, Lflipboard/objs/ConfigHints;->b:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v1, v10

    goto/16 :goto_2

    :cond_a
    sget-object v1, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v5, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v5, v1, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lflipboard/service/HintManager;->h:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    iget-object v5, p0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    iget v5, v5, Lflipboard/objs/ConfigHints;->b:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v1, v10

    :cond_b
    move v1, v2

    goto/16 :goto_3

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_c
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/ConfigHints$Hint;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/objs/ConfigHints$Hint;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v0, Lflipboard/objs/ConfigHints$Hint;->u:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v5}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    invoke-virtual {v0}, Lflipboard/objs/ConfigHints$Hint;->a()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/objs/ConfigHints$Hint;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    if-eqz v1, :cond_d

    :goto_6
    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    if-eqz v2, :cond_e

    move-object v1, v2

    :goto_7
    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    iput-boolean v3, v5, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    new-instance v1, Lflipboard/service/HintManager$1;

    invoke-direct {v1, p0, v0, p1}, Lflipboard/service/HintManager$1;-><init>(Lflipboard/service/HintManager;Lflipboard/objs/ConfigHints$Hint;Landroid/view/View;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "dialog_hint"

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0d004a

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_e
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d023f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    :cond_f
    iget-object v4, v0, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/ConfigHints$Hint;->c:Ljava/lang/String;

    invoke-static {v4, v5}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lflipboard/activities/LightBoxHintActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Lflipboard/objs/ConfigHints$Hint;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_10

    const-string v5, "extra_title"

    invoke-virtual {v0}, Lflipboard/objs/ConfigHints$Hint;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_10
    iget-object v5, v0, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    if-eqz v5, :cond_11

    const-string v2, "extra_url"

    iget-object v0, v0, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    invoke-virtual {v4, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v4}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    const v0, 0x7f040007

    const v2, 0x7f040009

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :cond_11
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Trying to show a lightbox without a url, that can\'t work. Id: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {v1, v4, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_12
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Trying to show hint with unrecognized type: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {v1, v4, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p0, v0, v3}, Lflipboard/service/HintManager;->a(Lflipboard/objs/ConfigHints$Hint;Z)V

    goto/16 :goto_0

    :cond_14
    sget-object v1, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    aput-object v0, v1, v3

    aput-object p1, v1, v2

    iget-boolean v0, p0, Lflipboard/service/HintManager;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v1, v10

    goto/16 :goto_0

    .line 200
    :cond_15
    sget-object v0, Lflipboard/service/HintManager;->a:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p2, v0, v3

    goto/16 :goto_0

    :cond_16
    move v1, v3

    goto/16 :goto_4

    :cond_17
    move v1, v3

    goto/16 :goto_5
.end method
