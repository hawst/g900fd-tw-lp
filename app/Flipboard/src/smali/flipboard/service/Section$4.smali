.class Lflipboard/service/Section$4;
.super Ljava/lang/Object;
.source "Section.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Section;


# direct methods
.method constructor <init>(Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 0

    .prologue
    .line 2024
    iput-object p1, p0, Lflipboard/service/Section$4;->b:Lflipboard/service/Section;

    iput-object p2, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 2029
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Section$4;->b:Lflipboard/service/Section;

    new-instance v1, Lflipboard/json/JSONParser;

    const-string v2, "contributors"

    invoke-virtual {p1, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->f()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section;->G:Ljava/util/List;

    .line 2030
    iget-object v0, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2031
    iget-object v0, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2039
    :cond_0
    :goto_0
    return-void

    .line 2034
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    .line 2035
    iget-object v0, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2036
    iget-object v0, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Parser exception"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2043
    iget-object v0, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2044
    iget-object v0, p0, Lflipboard/service/Section$4;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 2046
    :cond_0
    return-void
.end method
