.class public abstract Lflipboard/service/ServiceReloginObserver;
.super Ljava/lang/Object;
.source "ServiceReloginObserver.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/service/ServiceReloginObserver;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 13
    const/4 v1, 0x0

    const-string v0, "facebook"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "fragmentAction"

    sget-object v2, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "errorMessage"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v1, Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-direct {v1}, Lflipboard/activities/FacebookAuthenticateFragment;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    move-object v0, v1

    check-cast v0, Lflipboard/activities/FacebookAuthenticateFragment;

    iput-object p0, v0, Lflipboard/activities/FacebookAuthenticateFragment;->f:Lflipboard/service/ServiceReloginObserver;

    iput-object p2, v0, Lflipboard/activities/FacebookAuthenticateFragment;->g:Ljava/lang/String;

    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 16
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/ServiceReloginObserver$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lflipboard/service/ServiceReloginObserver$1;-><init>(Lflipboard/service/ServiceReloginObserver;Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 31
    return-void
.end method
