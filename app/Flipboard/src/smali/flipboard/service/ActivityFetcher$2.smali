.class Lflipboard/service/ActivityFetcher$2;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ActivityFetcher.java"


# instance fields
.field final synthetic a:Lflipboard/service/ActivityFetcher;


# direct methods
.method constructor <init>(Lflipboard/service/ActivityFetcher;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    iput-object p1, p0, Lflipboard/service/ActivityFetcher$2;->a:Lflipboard/service/ActivityFetcher;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 43
    const-string v0, "CREATE TABLE activity(id TEXT, expires INTEGER, data TEXT, PRIMARY KEY(id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 47
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 48
    return-void
.end method
