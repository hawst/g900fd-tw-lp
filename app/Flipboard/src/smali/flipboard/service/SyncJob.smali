.class public Lflipboard/service/SyncJob;
.super Ljava/lang/Object;
.source "SyncJob.java"


# static fields
.field static final a:Lflipboard/util/Log;


# instance fields
.field final b:Lflipboard/activities/FlipboardActivity;

.field final c:Lflipboard/service/User;

.field final d:Lflipboard/gui/dialog/FLProgressBarDialog;

.field e:Z

.field f:Z

.field g:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/io/Download;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field j:I

.field k:I

.field l:I

.field final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "sync"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Lflipboard/activities/FlipboardActivity;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/FlipboardActivity;",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lflipboard/service/SyncJob;->b:Lflipboard/activities/FlipboardActivity;

    .line 48
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v0, p0, Lflipboard/service/SyncJob;->c:Lflipboard/service/User;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lflipboard/service/SyncJob;->m:Ljava/util/List;

    .line 52
    iget-object v0, p0, Lflipboard/service/SyncJob;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 53
    iget-object v0, p0, Lflipboard/service/SyncJob;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 54
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 56
    invoke-virtual {v0}, Lflipboard/service/Section;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 61
    :cond_1
    new-instance v0, Lflipboard/gui/dialog/FLProgressBarDialog;

    const v1, 0x7f0d031a

    invoke-virtual {p1, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lflipboard/gui/dialog/FLProgressBarDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/SyncJob;->d:Lflipboard/gui/dialog/FLProgressBarDialog;

    .line 62
    iget-object v0, p0, Lflipboard/service/SyncJob;->d:Lflipboard/gui/dialog/FLProgressBarDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressBarDialog;->a(I)V

    .line 63
    iget-object v0, p0, Lflipboard/service/SyncJob;->d:Lflipboard/gui/dialog/FLProgressBarDialog;

    new-instance v1, Lflipboard/service/SyncJob$1;

    invoke-direct {v1, p0}, Lflipboard/service/SyncJob$1;-><init>(Lflipboard/service/SyncJob;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressBarDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/SyncJob;->h:Ljava/util/List;

    .line 71
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/SyncJob;->f:Z

    .line 72
    return-void
.end method

.method private declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/SyncJob;->e:Z

    if-nez v0, :cond_0

    .line 248
    sget-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 249
    invoke-virtual {p0}, Lflipboard/service/SyncJob;->e()V

    .line 250
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/SyncJob$7;

    invoke-direct {v1, p0}, Lflipboard/service/SyncJob$7;-><init>(Lflipboard/service/SyncJob;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_0
    monitor-exit p0

    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 119
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 120
    :try_start_0
    sget-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 121
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    .line 123
    iget-object v1, p0, Lflipboard/service/SyncJob;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iget v1, p0, Lflipboard/service/SyncJob;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/service/SyncJob;->i:I

    .line 125
    new-instance v1, Lflipboard/service/SyncJob$4;

    const/16 v2, 0xa

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v1, p0, v2}, Lflipboard/service/SyncJob$4;-><init>(Lflipboard/service/SyncJob;I)V

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :cond_0
    monitor-exit p0

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 76
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lflipboard/service/SyncJob;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/service/SyncJob;->b:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f0d031b

    invoke-virtual {v1, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 112
    :goto_0
    return-void

    .line 81
    :cond_0
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 82
    const-string v1, "id"

    const-string v2, "fetchForOffline"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 85
    iget-object v0, p0, Lflipboard/service/SyncJob;->d:Lflipboard/gui/dialog/FLProgressBarDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLProgressBarDialog;->show()V

    .line 88
    new-instance v0, Lflipboard/service/SyncJob$2;

    invoke-direct {v0, p0}, Lflipboard/service/SyncJob$2;-><init>(Lflipboard/service/SyncJob;)V

    iput-object v0, p0, Lflipboard/service/SyncJob;->g:Lflipboard/util/Observer;

    .line 96
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v1, p0, Lflipboard/service/SyncJob;->g:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->a(Lflipboard/util/Observer;)V

    .line 99
    new-instance v0, Lflipboard/service/SyncJob$3;

    const-string v1, "sync"

    invoke-direct {v0, p0, v1}, Lflipboard/service/SyncJob$3;-><init>(Lflipboard/service/SyncJob;Ljava/lang/String;)V

    .line 111
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method final declared-synchronized a(Lflipboard/service/Section;)V
    .locals 8

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p1, Lflipboard/service/Section;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 153
    iget v0, p0, Lflipboard/service/SyncJob;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lflipboard/service/SyncJob;->j:I

    .line 154
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 158
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 159
    iget v0, p1, Lflipboard/service/Section;->f:I

    mul-int/lit8 v0, v0, 0x5

    rsub-int/lit8 v0, v0, 0x64

    .line 160
    iget-object v1, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 161
    iget-object v1, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 162
    sget-object v6, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v6, v6, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v7, v7, Lflipboard/app/FlipboardApplication;->e:I

    invoke-virtual {v0, v6, v7}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v6

    .line 163
    invoke-direct {p0, v6, v1}, Lflipboard/service/SyncJob;->a(Ljava/lang/String;I)V

    .line 164
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v6

    .line 165
    if-eqz v6, :cond_1

    iget-object v7, v6, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v7, :cond_1

    .line 166
    iget-object v6, v6, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v6}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v7, v1, -0xa

    invoke-direct {p0, v6, v7}, Lflipboard/service/SyncJob;->a(Ljava/lang/String;I)V

    .line 168
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 170
    invoke-virtual {v0, v2, v3}, Lflipboard/objs/FeedItem;->a(J)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 171
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 176
    :cond_2
    :try_start_1
    iget v0, p0, Lflipboard/service/SyncJob;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/service/SyncJob;->k:I

    .line 177
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/SyncJob$5;

    invoke-direct {v1, p0}, Lflipboard/service/SyncJob$5;-><init>(Lflipboard/service/SyncJob;)V

    invoke-virtual {v0, v4, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized b()V
    .locals 8

    .prologue
    .line 197
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-wide v2, v0, Lflipboard/io/NetworkManager;->i:J

    .line 200
    new-instance v1, Lflipboard/service/SyncJob$6;

    invoke-direct {v1, p0}, Lflipboard/service/SyncJob$6;-><init>(Lflipboard/service/SyncJob;)V

    .line 218
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    .line 219
    iget-object v4, p0, Lflipboard/service/SyncJob;->c:Lflipboard/service/User;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v5, v6}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v4

    .line 223
    iget-object v0, p0, Lflipboard/service/SyncJob;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 224
    iget-object v6, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v6, v6, Lflipboard/service/Section$Meta;->b:Z

    if-nez v6, :cond_0

    .line 225
    iget v6, p0, Lflipboard/service/SyncJob;->j:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lflipboard/service/SyncJob;->j:I

    .line 226
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v0, v6, v7}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 227
    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 230
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/service/SyncJob;->c:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->g()V

    .line 231
    invoke-virtual {v4}, Lflipboard/service/Flap$UpdateRequest;->c()V

    .line 234
    :goto_1
    iget-boolean v0, p0, Lflipboard/service/SyncJob;->e:Z

    if-nez v0, :cond_3

    iget v0, p0, Lflipboard/service/SyncJob;->j:I

    if-gtz v0, :cond_2

    iget v0, p0, Lflipboard/service/SyncJob;->i:I

    if-gtz v0, :cond_2

    iget v0, p0, Lflipboard/service/SyncJob;->k:I

    if-lez v0, :cond_3

    .line 235
    :cond_2
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 236
    sget-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v4, p0, Lflipboard/service/SyncJob;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    iget v4, p0, Lflipboard/service/SyncJob;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x2

    iget v4, p0, Lflipboard/service/SyncJob;->k:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    goto :goto_1

    .line 239
    :cond_3
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-wide v0, v0, Lflipboard/io/NetworkManager;->i:J

    sub-long/2addr v0, v2

    .line 242
    invoke-direct {p0, v0, v1}, Lflipboard/service/SyncJob;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized c()V
    .locals 2

    .prologue
    .line 267
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/SyncJob;->e:Z

    if-nez v0, :cond_0

    .line 268
    sget-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    .line 269
    invoke-virtual {p0}, Lflipboard/service/SyncJob;->e()V

    .line 270
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/SyncJob$8;

    invoke-direct {v1, p0}, Lflipboard/service/SyncJob$8;-><init>(Lflipboard/service/SyncJob;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    :cond_0
    monitor-exit p0

    return-void

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()V
    .locals 1

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/SyncJob;->e:Z

    if-nez v0, :cond_0

    .line 289
    sget-object v0, Lflipboard/service/SyncJob;->a:Lflipboard/util/Log;

    .line 290
    invoke-virtual {p0}, Lflipboard/service/SyncJob;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :cond_0
    monitor-exit p0

    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e()V
    .locals 2

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/SyncJob;->e:Z

    if-nez v0, :cond_0

    .line 297
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/SyncJob;->e:Z

    .line 298
    iget-object v0, p0, Lflipboard/service/SyncJob;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/service/SyncJob;->d:Lflipboard/gui/dialog/FLProgressBarDialog;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/DialogInterface;)V

    .line 299
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v1, p0, Lflipboard/service/SyncJob;->g:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->b(Lflipboard/util/Observer;)V

    .line 301
    iget-object v0, p0, Lflipboard/service/SyncJob;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 302
    invoke-virtual {v0}, Lflipboard/io/Download;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 305
    :cond_0
    monitor-exit p0

    return-void
.end method
