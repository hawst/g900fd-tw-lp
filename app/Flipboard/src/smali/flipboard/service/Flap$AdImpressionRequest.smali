.class Lflipboard/service/Flap$AdImpressionRequest;
.super Lflipboard/service/Flap$RetryRequest;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 5422
    iput-object p1, p0, Lflipboard/service/Flap$AdImpressionRequest;->b:Lflipboard/service/Flap;

    .line 5423
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$RetryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 5424
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;JLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdImpressionRequest;
    .locals 1

    .prologue
    .line 5428
    iput-object p1, p0, Lflipboard/service/Flap$AdImpressionRequest;->c:Ljava/lang/String;

    .line 5429
    iput-object p2, p0, Lflipboard/service/Flap$AdImpressionRequest;->f:Ljava/lang/String;

    .line 5430
    iput-wide p3, p0, Lflipboard/service/Flap$AdImpressionRequest;->g:J

    .line 5431
    iput-object p5, p0, Lflipboard/service/Flap$AdImpressionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 5432
    invoke-super {p0}, Lflipboard/service/Flap$RetryRequest;->c()V

    .line 5433
    return-object p0
.end method

.method protected final a(Lflipboard/json/FLObject;)V
    .locals 1

    .prologue
    .line 5438
    iget-object v0, p0, Lflipboard/service/Flap$AdImpressionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    .line 5439
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5443
    iget-object v0, p0, Lflipboard/service/Flap$AdImpressionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 5444
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 8

    .prologue
    .line 5448
    iget-object v0, p0, Lflipboard/service/Flap$AdImpressionRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/impression"

    iget-object v2, p0, Lflipboard/service/Flap$AdImpressionRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "impression_value"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/service/Flap$AdImpressionRequest;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "impression_event"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$AdImpressionRequest;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "impression_timestamp"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-wide v6, p0, Lflipboard/service/Flap$AdImpressionRequest;->g:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->b(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
