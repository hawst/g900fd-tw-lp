.class Lflipboard/service/User$34;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/User$StateChanger;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2296
    iput-object p1, p0, Lflipboard/service/User$34;->c:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$34;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/service/User$34;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2299
    iget-object v0, p0, Lflipboard/service/User$34;->c:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2300
    iget-object v0, p0, Lflipboard/service/User$34;->c:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v5

    iget-object v1, p0, Lflipboard/service/User$34;->a:Ljava/util/List;

    iget-object v0, p0, Lflipboard/service/User$34;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v4, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v4, v4, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v4, v4, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-eqz v4, :cond_0

    iget-object v4, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v4, v4, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v4, v4, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    :goto_0
    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$MutedAuthor;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_0
    iget-object v0, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    move-object v4, v0

    goto :goto_0

    :cond_1
    move v1, v2

    :cond_2
    if-lez v1, :cond_4

    move v0, v3

    :goto_3
    iput-boolean v0, v5, Lflipboard/objs/UserState;->m:Z

    if-lez v1, :cond_3

    move v2, v3

    .line 2303
    :cond_3
    :goto_4
    return v2

    :cond_4
    move v0, v2

    .line 2300
    goto :goto_3

    .line 2302
    :cond_5
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Can\'t unmute user, currentState is null"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method
