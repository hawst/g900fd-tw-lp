.class public Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$TypedResultObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1737
    iput-object p1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->b:Lflipboard/service/Flap;

    .line 1738
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1739
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1767
    iget-object v0, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->b:Lflipboard/service/Flap;

    iget-object v1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->e:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->n:Lflipboard/service/User;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "service"

    aput-object v5, v4, v7

    iget-object v5, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->c:Ljava/lang/String;

    aput-object v5, v4, v8

    const-string v5, "access_token"

    aput-object v5, v4, v6

    iget-object v5, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->d:Ljava/lang/String;

    aput-object v5, v4, v9

    invoke-virtual {v0, v1, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1768
    :goto_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 1770
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1771
    const-string v0, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v3}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1772
    iget-object v0, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->b:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 1773
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 1775
    new-instance v4, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest$1;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v4, p0, v0}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest$1;-><init>(Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;Ljava/io/InputStream;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1784
    :try_start_1
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 1785
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    .line 1786
    iget-object v1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1806
    :try_start_2
    invoke-virtual {v4}, Lflipboard/service/Flap$StreamingJSONInputStream;->close()V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1807
    iput-object v2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 1817
    :goto_1
    return-void

    .line 1767
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->b:Lflipboard/service/Flap;

    iget-object v1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->e:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->n:Lflipboard/service/User;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "service"

    aput-object v5, v4, v7

    iget-object v5, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->c:Ljava/lang/String;

    aput-object v5, v4, v8

    const-string v5, "access_token"

    aput-object v5, v4, v6

    iget-object v5, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->d:Ljava/lang/String;

    aput-object v5, v4, v9

    const/4 v5, 0x4

    const-string v6, "api_server_url"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-object v6, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->f:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1789
    :cond_1
    :try_start_3
    invoke-virtual {v4}, Lflipboard/service/Flap$StreamingJSONInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserInfo;

    .line 1790
    if-nez v0, :cond_2

    .line 1791
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const-string v1, "NULL result from flap: %T"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1806
    :try_start_4
    invoke-virtual {v4}, Lflipboard/service/Flap$StreamingJSONInputStream;->close()V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1807
    iput-object v2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_1

    .line 1794
    :cond_2
    :try_start_5
    iget-object v1, v0, Lflipboard/objs/UserInfo;->p:Lflipboard/json/FLObject;

    invoke-static {v1}, Lflipboard/abtest/Experiments;->a(Lflipboard/json/FLObject;)V

    .line 1795
    iget-object v1, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/UserState$State;

    .line 1797
    :goto_2
    if-eqz v1, :cond_3

    iget-object v1, v1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-eqz v1, :cond_3

    iget-object v3, v1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    if-eqz v3, :cond_3

    .line 1798
    iget-object v1, v1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    const-string v3, "tocSectionPages"

    invoke-virtual {v1, v3}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1799
    if-eqz v1, :cond_3

    .line 1800
    invoke-virtual {v0, v1}, Lflipboard/objs/UserInfo;->a(Ljava/util/List;)V

    .line 1803
    :cond_3
    iget-object v1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1806
    :try_start_6
    invoke-virtual {v4}, Lflipboard/service/Flap$StreamingJSONInputStream;->close()V
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1816
    iput-object v2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto/16 :goto_1

    :cond_4
    move-object v1, v2

    .line 1795
    goto :goto_2

    .line 1806
    :catchall_0
    move-exception v0

    :try_start_7
    invoke-virtual {v4}, Lflipboard/service/Flap$StreamingJSONInputStream;->close()V

    .line 1807
    throw v0
    :try_end_7
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1810
    :catch_0
    move-exception v0

    .line 1811
    :try_start_8
    iget-object v1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1816
    iput-object v2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto/16 :goto_1

    .line 1812
    :catch_1
    move-exception v0

    .line 1813
    :try_start_9
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 1814
    iget-object v1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1816
    iput-object v2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    iput-object v2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    throw v0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;)",
            "Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;"
        }
    .end annotation

    .prologue
    .line 1743
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 1745
    iput-object p1, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->c:Ljava/lang/String;

    .line 1746
    iput-object p2, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->d:Ljava/lang/String;

    .line 1747
    iput-object p5, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 1748
    iput-object p3, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->f:Ljava/lang/String;

    .line 1749
    iput-object p4, p0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->e:Ljava/lang/String;

    .line 1750
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 1751
    return-object p0
.end method
