.class public abstract Lflipboard/service/Flap$Request;
.super Ljava/lang/Object;
.source "Flap.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/io/InputStream;

.field final n:Lflipboard/service/User;

.field protected final o:Z

.field final synthetic p:Lflipboard/service/Flap;


# direct methods
.method protected constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 1

    .prologue
    .line 768
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Z)V

    .line 769
    return-void
.end method

.method protected constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Z)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lflipboard/service/Flap$Request;->p:Lflipboard/service/Flap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 773
    iput-object p2, p0, Lflipboard/service/Flap$Request;->n:Lflipboard/service/User;

    .line 774
    iput-boolean p3, p0, Lflipboard/service/Flap$Request;->o:Z

    .line 775
    return-void
.end method


# virtual methods
.method protected final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 779
    const-string v0, "X-Flipboard-GEOIP_COUNTRY_CODE"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 780
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 781
    :goto_0
    if-eqz v0, :cond_0

    .line 782
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 784
    :cond_0
    iget-object v1, p0, Lflipboard/service/Flap$Request;->p:Lflipboard/service/Flap;

    monitor-enter v1

    .line 785
    :try_start_0
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "country_code"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 786
    iget-object v2, p0, Lflipboard/service/Flap$Request;->p:Lflipboard/service/Flap;

    iput-object v0, v2, Lflipboard/service/Flap;->j:Ljava/lang/String;

    .line 787
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 789
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 798
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, p1, p2}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    .line 800
    :goto_1
    iget-object v0, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    return-object v0

    .line 780
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 787
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 793
    :sswitch_0
    invoke-static {}, Lflipboard/service/Flap;->f()Ljava/io/ByteArrayInputStream;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    goto :goto_1

    .line 789
    nop

    :sswitch_data_0
    .sparse-switch
        0xcc -> :sswitch_0
        0xcd -> :sswitch_0
        0x130 -> :sswitch_0
    .end sparse-switch
.end method

.method protected abstract a()V
.end method

.method public c()V
    .locals 1

    .prologue
    .line 827
    sget-object v0, Lflipboard/service/Flap;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 828
    return-void
.end method

.method public cancel()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 822
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const-string v1, "Don\'t know how to cancel this request: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 823
    return v4
.end method

.method public run()V
    .locals 3

    .prologue
    .line 805
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 807
    :try_start_0
    invoke-virtual {p0}, Lflipboard/service/Flap$Request;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    :try_start_1
    iget-object v0, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    invoke-static {}, Lflipboard/service/Flap;->f()Ljava/io/ByteArrayInputStream;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 811
    iget-object v0, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 809
    :catchall_0
    move-exception v0

    .line 810
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    invoke-static {}, Lflipboard/service/Flap;->f()Ljava/io/ByteArrayInputStream;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 811
    iget-object v1, p0, Lflipboard/service/Flap$Request;->a:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 814
    :cond_1
    :goto_1
    throw v0

    :catch_0
    move-exception v1

    goto :goto_1

    .line 815
    :catch_1
    move-exception v0

    goto :goto_0
.end method
