.class Lflipboard/service/User$24;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Lflipboard/service/Section;ZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1714
    iput-object p1, p0, Lflipboard/service/User$24;->e:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iput-boolean p3, p0, Lflipboard/service/User$24;->b:Z

    iput-boolean p4, p0, Lflipboard/service/User$24;->c:Z

    iput-object p5, p0, Lflipboard/service/User$24;->d:Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1717
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1718
    const-string v1, "uid"

    iget-object v2, p0, Lflipboard/service/User$24;->e:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    const-string v1, "pos"

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget v2, v2, Lflipboard/service/Section;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1721
    :try_start_0
    const-string v1, "descriptor"

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-static {v2}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/TOCSection;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1722
    const-string v1, "metaData"

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    invoke-static {v2}, Lflipboard/json/JSONSerializer;->a(Lflipboard/service/Section$Meta;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1727
    iget-boolean v1, p0, Lflipboard/service/User$24;->b:Z

    if-eqz v1, :cond_0

    .line 1728
    const-string v1, "items"

    iget-object v2, p0, Lflipboard/service/User$24;->e:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    invoke-static {v2}, Lflipboard/service/User;->c(Lflipboard/service/Section;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1732
    :cond_0
    iget-object v1, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    invoke-virtual {p0, v0}, Lflipboard/service/User$24;->a(Landroid/content/ContentValues;)I

    move-result v0

    iput v0, v1, Lflipboard/service/Section;->f:I

    .line 1733
    iget-object v0, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget v0, v0, Lflipboard/service/Section;->f:I

    if-ltz v0, :cond_2

    .line 1734
    iget-boolean v0, p0, Lflipboard/service/User$24;->c:Z

    if-eqz v0, :cond_1

    .line 1735
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->l:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 1736
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1739
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/service/User$24;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1740
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1741
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 1743
    :cond_1
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget v1, v1, Lflipboard/service/Section;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    aput-object v1, v0, v5

    .line 1745
    :cond_2
    :goto_0
    return-void

    .line 1723
    :catch_0
    move-exception v0

    .line 1724
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/service/User$24;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    goto :goto_0
.end method
