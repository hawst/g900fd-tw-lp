.class public Lflipboard/service/Flap$CreateAccountWithTokenRequest;
.super Lflipboard/service/Flap$AccountRequest;
.source "Flap.java"


# instance fields
.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field final synthetic k:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1701
    iput-object p1, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->k:Lflipboard/service/Flap;

    .line 1702
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$AccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1703
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1716
    iget-object v0, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1717
    iget-object v0, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->k:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/createWithSSOWithToken"

    iget-object v2, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "service"

    aput-object v4, v3, v5

    iget-object v4, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->h:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "access_token"

    aput-object v4, v3, v7

    iget-object v4, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->g:Ljava/lang/String;

    aput-object v4, v3, v8

    const-string v4, "from"

    aput-object v4, v3, v9

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1719
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->k:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/createWithSSOWithToken"

    iget-object v2, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->n:Lflipboard/service/User;

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "service"

    aput-object v4, v3, v5

    iget-object v4, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->h:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "access_token"

    aput-object v4, v3, v7

    iget-object v4, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->g:Ljava/lang/String;

    aput-object v4, v3, v8

    const-string v4, "api_server_url"

    aput-object v4, v3, v9

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->i:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "from"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic cancel()Z
    .locals 1

    .prologue
    .line 1694
    invoke-super {p0}, Lflipboard/service/Flap$AccountRequest;->cancel()Z

    move-result v0

    return v0
.end method
