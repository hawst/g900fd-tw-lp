.class Lflipboard/service/Flap$ComposeRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic c:Lflipboard/service/Flap;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lflipboard/service/Section;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 3136
    iput-object p1, p0, Lflipboard/service/Flap$ComposeRequest;->c:Lflipboard/service/Flap;

    .line 3137
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3130
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->f:Ljava/lang/String;

    .line 3138
    iput-object p3, p0, Lflipboard/service/Flap$ComposeRequest;->g:Ljava/lang/String;

    .line 3139
    iput-object p4, p0, Lflipboard/service/Flap$ComposeRequest;->h:Lflipboard/service/Section;

    .line 3140
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 12

    .prologue
    const/4 v7, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 3158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 3159
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 3160
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->c:Lflipboard/service/Flap;

    const-string v1, "/v1/social/compose"

    iget-object v4, p0, Lflipboard/service/Flap$ComposeRequest;->n:Lflipboard/service/User;

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "message"

    aput-object v6, v5, v9

    iget-object v6, p0, Lflipboard/service/Flap$ComposeRequest;->a:Ljava/lang/String;

    aput-object v6, v5, v10

    const-string v6, "service"

    aput-object v6, v5, v11

    iget-object v6, p0, Lflipboard/service/Flap$ComposeRequest;->d:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x4

    const-string v7, "url"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    iget-object v7, p0, Lflipboard/service/Flap$ComposeRequest;->f:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v1, v4, v5}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3166
    :goto_0
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->h:Lflipboard/service/Section;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->h:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 3167
    const-string v1, "%s&sectionid=%s"

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v0, v4, v9

    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->h:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v0, v4, v10

    invoke-static {v1, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 3170
    :goto_1
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->e:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 3171
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 3172
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3173
    const-string v6, "&target="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3174
    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 3162
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->c:Lflipboard/service/Flap;

    const-string v1, "/v1/social/shareWithComment"

    iget-object v4, p0, Lflipboard/service/Flap$ComposeRequest;->n:Lflipboard/service/User;

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "text"

    aput-object v6, v5, v9

    iget-object v6, p0, Lflipboard/service/Flap$ComposeRequest;->a:Ljava/lang/String;

    aput-object v6, v5, v10

    const-string v6, "service"

    aput-object v6, v5, v11

    iget-object v6, p0, Lflipboard/service/Flap$ComposeRequest;->d:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x4

    const-string v7, "oid"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    iget-object v7, p0, Lflipboard/service/Flap$ComposeRequest;->g:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "url"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    iget-object v7, p0, Lflipboard/service/Flap$ComposeRequest;->f:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v1, v4, v5}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3176
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3179
    :cond_2
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v0, v10, [Ljava/lang/Object;

    aput-object v1, v0, v9

    .line 3181
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3182
    const-string v1, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3183
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {v1, v0}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 3184
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 3185
    new-instance v5, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v0, v1}, Lflipboard/service/Flap$ComposeRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v5, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3186
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3187
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    .line 3188
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response from flap: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3218
    iput-object v8, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    .line 3219
    :goto_3
    return-void

    .line 3191
    :cond_3
    :try_start_1
    invoke-virtual {v5}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3192
    if-eqz v0, :cond_7

    .line 3193
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3196
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->d:Ljava/lang/String;

    const-string v4, "flipboard"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 3198
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->g:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 3199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->d:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/Flap$ComposeRequest;->f:Ljava/lang/String;

    iget-object v5, p0, Lflipboard/service/Flap$ComposeRequest;->h:Lflipboard/service/Section;

    invoke-static {v2, v3, v1, v4, v5}, Lflipboard/io/UsageEvent;->a(JLjava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V

    .line 3204
    :cond_4
    :goto_4
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3218
    :goto_5
    iput-object v8, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_3

    .line 3201
    :cond_5
    :try_start_2
    const-string v1, "shared"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, Lflipboard/service/Flap$ComposeRequest;->h:Lflipboard/service/Section;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 3212
    :catch_0
    move-exception v0

    .line 3213
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3218
    iput-object v8, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_3

    .line 3207
    :cond_6
    :try_start_4
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_5

    .line 3214
    :catch_1
    move-exception v0

    .line 3215
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3216
    iget-object v1, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3218
    iput-object v8, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_3

    .line 3210
    :cond_7
    :try_start_6
    iget-object v0, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 3218
    :catchall_0
    move-exception v0

    iput-object v8, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    throw v0

    :cond_8
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public compose(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ComposeRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")",
            "Lflipboard/service/Flap$ComposeRequest;"
        }
    .end annotation

    .prologue
    .line 3144
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 3145
    iput-object p1, p0, Lflipboard/service/Flap$ComposeRequest;->a:Ljava/lang/String;

    .line 3146
    iput-object p2, p0, Lflipboard/service/Flap$ComposeRequest;->d:Ljava/lang/String;

    .line 3147
    iput-object p3, p0, Lflipboard/service/Flap$ComposeRequest;->e:Ljava/util/List;

    .line 3148
    iput-object p4, p0, Lflipboard/service/Flap$ComposeRequest;->f:Ljava/lang/String;

    .line 3149
    iput-object p5, p0, Lflipboard/service/Flap$ComposeRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    .line 3150
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3151
    return-object p0
.end method
