.class public Lflipboard/service/Flap$FlipusRequst;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$TypedResultObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/FlipResponse;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/lang/String;

.field final synthetic c:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2250
    iput-object p1, p0, Lflipboard/service/Flap$FlipusRequst;->c:Lflipboard/service/Flap;

    .line 2251
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2252
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$FlipusRequst;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/FlipResponse;",
            ">;)",
            "Lflipboard/service/Flap$FlipusRequst;"
        }
    .end annotation

    .prologue
    .line 2256
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 2257
    iput-object p2, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2258
    iput-object p1, p0, Lflipboard/service/Flap$FlipusRequst;->b:Ljava/lang/String;

    .line 2259
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2260
    return-object p0
.end method

.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2266
    iget-object v1, p0, Lflipboard/service/Flap$FlipusRequst;->c:Lflipboard/service/Flap;

    const-string v2, "/v1/content/flipus"

    iget-object v3, p0, Lflipboard/service/Flap$FlipusRequst;->n:Lflipboard/service/User;

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/Object;

    const-string v0, "ownerid"

    aput-object v0, v4, v7

    iget-object v0, p0, Lflipboard/service/Flap$FlipusRequst;->n:Lflipboard/service/User;

    if-nez v0, :cond_0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v0, 0x2

    const-string v5, "url"

    aput-object v5, v4, v0

    const/4 v0, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$FlipusRequst;->b:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2267
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 2270
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2271
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    iget-object v0, p0, Lflipboard/service/Flap$FlipusRequst;->c:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2273
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2274
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$FlipusRequst;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2277
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2278
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    .line 2279
    iget-object v0, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected response from flap: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2300
    :try_start_2
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2301
    iput-object v6, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2311
    :goto_1
    return-void

    .line 2266
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$FlipusRequst;->n:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    goto :goto_0

    .line 2282
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 2283
    if-eqz v0, :cond_4

    .line 2284
    const-string v1, "flus"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v1

    const-string v2, "ok"

    invoke-virtual {v1, v2}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2285
    iget-object v1, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2300
    :goto_2
    :try_start_4
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2310
    iput-object v6, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_1

    .line 2287
    :cond_2
    :try_start_5
    const-string v1, "flip"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 2288
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 2289
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2290
    iget-object v1, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v2, Lflipboard/json/JSONParser;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->m()Lflipboard/objs/FlipResponse;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 2300
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->close()V

    .line 2301
    throw v0
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2304
    :catch_0
    move-exception v0

    .line 2305
    :try_start_7
    iget-object v1, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2310
    iput-object v6, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_1

    .line 2292
    :cond_3
    :try_start_8
    iget-object v0, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    const-string v1, "Unexpected response from flap, no flip result object found"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 2297
    :cond_4
    iget-object v0, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 2306
    :catch_1
    move-exception v0

    .line 2307
    :try_start_9
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2308
    iget-object v1, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2310
    iput-object v6, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    iput-object v6, p0, Lflipboard/service/Flap$FlipusRequst;->a:Lflipboard/service/Flap$TypedResultObserver;

    throw v0
.end method
