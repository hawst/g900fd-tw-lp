.class Lflipboard/service/User$39;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2673
    iput-object p1, p0, Lflipboard/service/User$39;->c:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$39;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/service/User$39;->b:Ljava/util/List;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2676
    const-string v0, "magazines"

    invoke-virtual {p0, v0}, Lflipboard/service/User$39;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2677
    const-string v0, "SELECT * FROM MAGAZINES WHERE uid = ? ORDER BY contributor ASC, id ASC"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/User$39;->c:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User$39;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2678
    :goto_0
    iget-object v0, p0, Lflipboard/service/User$39;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2679
    invoke-static {p0}, Lflipboard/objs/Magazine;->a(Lflipboard/service/DatabaseHandler;)Lflipboard/objs/Magazine;

    move-result-object v0

    .line 2680
    const-string v1, "contributor"

    invoke-virtual {p0, v1}, Lflipboard/service/User$39;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2681
    iget-object v1, p0, Lflipboard/service/User$39;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2683
    :cond_0
    iget-object v1, p0, Lflipboard/service/User$39;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2687
    :cond_1
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v1, "Unable to load magazines because table doesn\'t exist"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2689
    :cond_2
    return-void
.end method
