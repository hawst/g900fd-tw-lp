.class Lflipboard/service/User$41;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Ljava/util/List",
        "<",
        "Lflipboard/objs/Magazine;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2722
    iput-object p1, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2722
    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    invoke-static {v0, p1}, Lflipboard/service/User;->b(Lflipboard/service/User;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->t(Lflipboard/service/User;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->b(Ljava/util/List;Z)V

    iget-object v0, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->s(Lflipboard/service/User;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    iget-object v2, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->r(Lflipboard/service/User;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2735
    iget-object v0, p0, Lflipboard/service/User$41;->a:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->s(Lflipboard/service/User;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 2736
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 2737
    return-void
.end method
