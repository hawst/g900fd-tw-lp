.class Lflipboard/service/User$5;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Lflipboard/service/Account;

.field final synthetic b:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Lflipboard/service/Account;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 526
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 527
    iget-object v0, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->i:Ljava/util/Map;

    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {v2}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 529
    :try_start_0
    const-string v2, "uid"

    iget-object v3, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    iget-object v3, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const-string v2, "descriptor"

    iget-object v3, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget-object v3, v3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-static {v3}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserService;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 533
    const-string v2, "service"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 535
    const-string v2, "serviceId"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 536
    const-string v2, "name"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 537
    const-string v2, "screenName"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 538
    const-string v2, "email"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 539
    const-string v2, "image"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 540
    const-string v2, "profile"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 541
    const-string v2, "metaData"

    iget-object v3, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget-object v3, v3, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    invoke-static {v3}, Lflipboard/json/JSONSerializer;->a(Lflipboard/service/Account$Meta;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 543
    if-eqz v0, :cond_2

    .line 545
    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget v3, v0, Lflipboard/service/Account;->a:I

    iput v3, v2, Lflipboard/service/Account;->a:I

    .line 546
    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget-object v3, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iput-object v3, v2, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    .line 547
    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget-object v2, v2, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-boolean v2, v2, Lflipboard/service/Account$Meta;->a:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {v0, v2}, Lflipboard/service/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 548
    :cond_0
    const-string v2, "id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget v5, v0, Lflipboard/service/Account;->a:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/service/User$5;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Z

    .line 549
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget-object v3, v3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v0, v0, Lflipboard/service/Account;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    .line 550
    iget-object v0, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->i:Ljava/util/Map;

    iget-object v1, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {v1}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    :goto_0
    iget-object v0, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget-object v0, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/service/Account$Meta;->a:Z

    .line 562
    iget-object v0, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->g:Lflipboard/service/User$Message;

    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 563
    iget-object v0, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/Account;)V

    .line 567
    :cond_1
    :goto_1
    return-void

    .line 555
    :cond_2
    iget-object v0, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {p0, v1}, Lflipboard/service/User$5;->a(Landroid/content/ContentValues;)I

    move-result v1

    iput v1, v0, Lflipboard/service/Account;->a:I

    .line 556
    iget-object v0, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    iget v0, v0, Lflipboard/service/Account;->a:I

    if-ltz v0, :cond_3

    .line 557
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    aput-object v2, v0, v1

    .line 559
    :cond_3
    iget-object v0, p0, Lflipboard/service/User$5;->b:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->i:Ljava/util/Map;

    iget-object v1, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-virtual {v1}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/User$5;->a:Lflipboard/service/Account;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 564
    :catch_0
    move-exception v0

    .line 565
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "couldn\'t load account: %-E"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
