.class public Lflipboard/service/Flap$AlsoFlippedRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Lflipboard/service/Flap$AlsoFlippedObserver;

.field final synthetic c:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2767
    iput-object p1, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->c:Lflipboard/service/Flap;

    .line 2768
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2769
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/Flap$AlsoFlippedObserver;)Lflipboard/service/Flap$AlsoFlippedRequest;
    .locals 0

    .prologue
    .line 2773
    iput-object p1, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->a:Ljava/lang/String;

    .line 2774
    iput-object p2, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    .line 2775
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2776
    return-object p0
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2782
    iget-object v0, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->c:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/alsoFlippedIn"

    iget-object v2, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "oid"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2783
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 2785
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2786
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2787
    iget-object v0, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2788
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2789
    new-instance v3, Lflipboard/json/JSONParser;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$AlsoFlippedRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Lflipboard/json/JSONParser;->c()Lflipboard/objs/AlsoFlippedResult;

    move-result-object v0

    .line 2790
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 2791
    const/16 v3, 0xc8

    if-eq v1, v3, :cond_0

    .line 2792
    iget-object v0, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$AlsoFlippedObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2802
    iput-object v5, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    .line 2803
    :goto_0
    return-void

    .line 2795
    :cond_0
    :try_start_1
    iget-object v1, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$AlsoFlippedObserver;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2802
    iput-object v5, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    goto :goto_0

    .line 2796
    :catch_0
    move-exception v0

    .line 2797
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$AlsoFlippedObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2802
    iput-object v5, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    goto :goto_0

    .line 2798
    :catch_1
    move-exception v0

    .line 2799
    :try_start_3
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2800
    iget-object v1, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$AlsoFlippedObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2802
    iput-object v5, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$AlsoFlippedRequest;->b:Lflipboard/service/Flap$AlsoFlippedObserver;

    throw v0
.end method
