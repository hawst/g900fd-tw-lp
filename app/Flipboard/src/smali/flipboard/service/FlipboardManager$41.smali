.class public Lflipboard/service/FlipboardManager$41;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$CreateIssueObserver;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Ljava/lang/String;

.field final synthetic j:Ljava/lang/String;

.field final synthetic k:Ljava/lang/String;

.field final synthetic l:Ljava/lang/String;

.field final synthetic m:Ljava/lang/String;

.field final synthetic n:Ljava/lang/String;

.field final synthetic o:Ljava/lang/String;

.field final synthetic p:Ljava/io/File;

.field final synthetic q:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/service/FlipboardManager$CreateIssueObserver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 3495
    iput-object p1, p0, Lflipboard/service/FlipboardManager$41;->q:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$41;->a:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$41;->b:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/service/FlipboardManager$41;->c:Ljava/lang/String;

    iput-object p5, p0, Lflipboard/service/FlipboardManager$41;->d:Ljava/lang/String;

    iput-object p6, p0, Lflipboard/service/FlipboardManager$41;->e:Ljava/lang/String;

    iput-object p7, p0, Lflipboard/service/FlipboardManager$41;->f:Ljava/lang/String;

    iput-object p8, p0, Lflipboard/service/FlipboardManager$41;->g:Ljava/lang/String;

    iput-object p9, p0, Lflipboard/service/FlipboardManager$41;->h:Ljava/lang/String;

    iput-object p10, p0, Lflipboard/service/FlipboardManager$41;->i:Ljava/lang/String;

    iput-object p11, p0, Lflipboard/service/FlipboardManager$41;->j:Ljava/lang/String;

    iput-object p12, p0, Lflipboard/service/FlipboardManager$41;->k:Ljava/lang/String;

    iput-object p13, p0, Lflipboard/service/FlipboardManager$41;->l:Ljava/lang/String;

    iput-object p14, p0, Lflipboard/service/FlipboardManager$41;->m:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lflipboard/service/FlipboardManager$41;->n:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lflipboard/service/FlipboardManager$41;->o:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lflipboard/service/FlipboardManager$41;->p:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 17

    .prologue
    .line 3513
    new-instance v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/FlipboardManager$41;->q:Lflipboard/service/FlipboardManager;

    invoke-direct {v1, v2}, Lflipboard/service/FlipboardManager$CreateIssueRequest;-><init>(Lflipboard/service/FlipboardManager;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/FlipboardManager$41;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/FlipboardManager$41;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/FlipboardManager$41;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/service/FlipboardManager$41;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/service/FlipboardManager$41;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/service/FlipboardManager$41;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lflipboard/service/FlipboardManager$41;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lflipboard/service/FlipboardManager$41;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lflipboard/service/FlipboardManager$41;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lflipboard/service/FlipboardManager$41;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lflipboard/service/FlipboardManager$41;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lflipboard/service/FlipboardManager$41;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lflipboard/service/FlipboardManager$41;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lflipboard/service/FlipboardManager$41;->o:Ljava/lang/String;

    new-instance v16, Lflipboard/service/FlipboardManager$41$1;

    invoke-direct/range {v16 .. v17}, Lflipboard/service/FlipboardManager$41$1;-><init>(Lflipboard/service/FlipboardManager$41;)V

    iput-object v2, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->a:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->b:Ljava/lang/String;

    iput-object v4, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->f:Ljava/lang/String;

    iput-object v5, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->g:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v0, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->c:Ljava/lang/String;

    iput-object v6, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->h:Ljava/lang/String;

    iput-object v7, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->d:Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v0, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->e:Lflipboard/service/Flap$JSONResultObserver;

    iput-object v9, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->i:Ljava/lang/String;

    iput-object v10, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->j:Ljava/lang/String;

    iput-object v12, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->k:Ljava/lang/String;

    iput-object v13, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->l:Ljava/lang/String;

    iput-object v14, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->m:Ljava/lang/String;

    iput-object v11, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->q:Ljava/lang/String;

    iput-object v8, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->r:Ljava/lang/String;

    iput-object v15, v1, Lflipboard/service/FlipboardManager$CreateIssueRequest;->s:Ljava/lang/String;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager$CreateIssueRequest;->c()V

    .line 3528
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 3500
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/service/FlipboardManager$41;->b(Ljava/lang/String;)V

    .line 3501
    iget-object v0, p0, Lflipboard/service/FlipboardManager$41;->a:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    const-string v1, "name"

    invoke-virtual {p1, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->a(Ljava/lang/String;)V

    .line 3502
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3507
    iget-object v0, p0, Lflipboard/service/FlipboardManager$41;->a:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    invoke-interface {v0}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->a()V

    .line 3508
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/FlipboardManager$41;->b(Ljava/lang/String;)V

    .line 3509
    return-void
.end method
