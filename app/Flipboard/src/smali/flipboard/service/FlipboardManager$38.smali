.class Lflipboard/service/FlipboardManager$38;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Ljava/util/Collection;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/util/Collection;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 3203
    iput-object p1, p0, Lflipboard/service/FlipboardManager$38;->c:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$38;->a:Ljava/util/Collection;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$38;->b:Lflipboard/service/Section;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 3207
    iget-object v0, p0, Lflipboard/service/FlipboardManager$38;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 3208
    iget-object v2, p0, Lflipboard/service/FlipboardManager$38;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v2}, Lflipboard/service/FlipboardManager;->p(Lflipboard/service/FlipboardManager;)Ljava/util/Map;

    move-result-object v2

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3210
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$38;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->q(Lflipboard/service/FlipboardManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3211
    iget-object v0, p0, Lflipboard/service/FlipboardManager$38;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->p(Lflipboard/service/FlipboardManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 3213
    iget-object v0, p0, Lflipboard/service/FlipboardManager$38;->c:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$38;->b:Lflipboard/service/Section;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 3215
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3219
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "failed to mark %d items as read"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/service/FlipboardManager$38;->a:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3220
    iget-object v0, p0, Lflipboard/service/FlipboardManager$38;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->q(Lflipboard/service/FlipboardManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3221
    return-void
.end method
