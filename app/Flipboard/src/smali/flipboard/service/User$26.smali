.class Lflipboard/service/User$26;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:J

.field final synthetic d:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;ZLflipboard/service/Section;J)V
    .locals 0

    .prologue
    .line 1836
    iput-object p1, p0, Lflipboard/service/User$26;->d:Lflipboard/service/User;

    iput-boolean p2, p0, Lflipboard/service/User$26;->a:Z

    iput-object p3, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iput-wide p4, p0, Lflipboard/service/User$26;->c:J

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 1840
    iget-boolean v0, p0, Lflipboard/service/User$26;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/User$26;->d:Lflipboard/service/User;

    iget-object v0, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/User;->c(Lflipboard/service/Section;)[B

    move-result-object v0

    .line 1842
    :goto_0
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1844
    const-string v3, "descriptor"

    iget-object v4, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-static {v4}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/TOCSection;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1845
    const-string v3, "tocItem"

    iget-object v4, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-nez v4, :cond_3

    :goto_1
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1846
    iget-boolean v1, p0, Lflipboard/service/User$26;->a:Z

    if-eqz v1, :cond_0

    .line 1847
    const-string v1, "items"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1849
    :cond_0
    const-string v0, "pos"

    iget-object v1, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget v1, v1, Lflipboard/service/Section;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1850
    const-string v0, "metaData"

    iget-object v1, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    invoke-static {v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/service/Section$Meta;)[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1851
    iget-object v0, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/service/Section$Meta;->a:Z

    .line 1856
    const-string v0, "sectionId"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1858
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1859
    const-string v0, "service"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1860
    const-string v0, "image"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1861
    const-string v0, "remoteId"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1862
    const-string v0, "unreadRemoteId"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1863
    const-string v0, "private"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1864
    const-string v0, "unreadRemoteId"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1866
    const-string v0, "id = ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget v4, v4, Lflipboard/service/Section;->f:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v2, v0, v1}, Lflipboard/service/User$26;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1867
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-boolean v2, p0, Lflipboard/service/User$26;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x2

    iget-object v0, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-nez v0, :cond_4

    const-string v0, "no"

    :goto_2
    aput-object v0, v1, v2

    const/4 v0, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/service/User$26;->c:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1872
    :cond_1
    :goto_3
    return-void

    :cond_2
    move-object v0, v1

    .line 1840
    goto/16 :goto_0

    .line 1845
    :cond_3
    iget-object v1, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-static {v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItem;)[B

    move-result-object v1

    goto/16 :goto_1

    .line 1867
    :cond_4
    iget-object v0, p0, Lflipboard/service/User$26;->b:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_2

    .line 1869
    :catch_0
    move-exception v0

    .line 1870
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v2, "couldn\'t save section: %E"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method
