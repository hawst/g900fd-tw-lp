.class public Lflipboard/service/Flap$NotificationRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic d:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4304
    iput-object p1, p0, Lflipboard/service/Flap$NotificationRequest;->d:Lflipboard/service/Flap;

    .line 4305
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4306
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 4327
    iget-object v0, p0, Lflipboard/service/Flap$NotificationRequest;->d:Lflipboard/service/Flap;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/v1/social/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/Flap$NotificationRequest;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Notification"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/Flap$NotificationRequest;->n:Lflipboard/service/User;

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "registrationId"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$NotificationRequest;->b:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4329
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/service/Flap$NotificationRequest;->a:Ljava/lang/String;

    aput-object v2, v1, v6

    aput-object v0, v1, v7

    .line 4331
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 4332
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4333
    iget-object v0, p0, Lflipboard/service/Flap$NotificationRequest;->d:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 4334
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 4335
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$NotificationRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4336
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 4337
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 4338
    iget-object v0, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4356
    iput-object v5, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    .line 4357
    :goto_0
    return-void

    .line 4341
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 4342
    if-eqz v0, :cond_2

    .line 4343
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4344
    iget-object v1, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4356
    :goto_1
    iput-object v5, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 4346
    :cond_1
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 4351
    :catch_0
    move-exception v0

    .line 4352
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4356
    iput-object v5, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 4349
    :cond_2
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 4353
    :catch_1
    move-exception v0

    .line 4354
    :try_start_5
    iget-object v1, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4356
    iput-object v5, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method

.method public final a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1

    .prologue
    .line 4310
    const-string v0, "register"

    iput-object v0, p0, Lflipboard/service/Flap$NotificationRequest;->a:Ljava/lang/String;

    .line 4311
    iput-object p1, p0, Lflipboard/service/Flap$NotificationRequest;->b:Ljava/lang/String;

    .line 4312
    iput-object p2, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    .line 4313
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4314
    return-void
.end method

.method public final b(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1

    .prologue
    .line 4318
    const-string v0, "unregister"

    iput-object v0, p0, Lflipboard/service/Flap$NotificationRequest;->a:Ljava/lang/String;

    .line 4319
    iput-object p1, p0, Lflipboard/service/Flap$NotificationRequest;->b:Ljava/lang/String;

    .line 4320
    iput-object p2, p0, Lflipboard/service/Flap$NotificationRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    .line 4321
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4322
    return-void
.end method
