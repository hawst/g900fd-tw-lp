.class Lflipboard/service/Flap$APITokenRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field b:Ljava/lang/String;

.field final synthetic c:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4953
    iput-object p1, p0, Lflipboard/service/Flap$APITokenRequest;->c:Lflipboard/service/Flap;

    .line 4954
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4955
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$APITokenRequest;
    .locals 1

    .prologue
    .line 4959
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    .line 4960
    iput-object p1, p0, Lflipboard/service/Flap$APITokenRequest;->b:Ljava/lang/String;

    .line 4961
    iput-object p2, p0, Lflipboard/service/Flap$APITokenRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4962
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4963
    return-object p0
.end method

.method protected final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4971
    iget-object v0, p0, Lflipboard/service/Flap$APITokenRequest;->c:Lflipboard/service/Flap;

    const-string v1, "/v1/users/getApiToken"

    iget-object v2, p0, Lflipboard/service/Flap$APITokenRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "client_id"

    aput-object v4, v3, v5

    iget-object v4, p0, Lflipboard/service/Flap$APITokenRequest;->b:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4972
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v6, [Ljava/lang/Object;

    aput-object v0, v1, v5

    .line 4973
    iget-object v1, p0, Lflipboard/service/Flap$APITokenRequest;->c:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$APITokenRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->b(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4974
    return-void
.end method
