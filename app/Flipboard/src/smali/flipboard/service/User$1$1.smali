.class Lflipboard/service/User$1$1;
.super Landroid/os/AsyncTask;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lflipboard/service/User$1;


# direct methods
.method constructor <init>(Lflipboard/service/User$1;Lflipboard/service/Section;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lflipboard/service/User$1$1;->c:Lflipboard/service/User$1;

    iput-object p2, p0, Lflipboard/service/User$1$1;->a:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/service/User$1$1;->b:Ljava/lang/Object;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lflipboard/service/User$1$1;->a:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User$1$1;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->o()V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 173
    iget-object v0, p0, Lflipboard/service/User$1$1;->c:Lflipboard/service/User$1;

    iget-object v0, v0, Lflipboard/service/User$1;->a:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->a(Lflipboard/service/User;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    iget-object v0, p0, Lflipboard/service/User$1$1;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    sget-object v2, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v9

    iget-boolean v3, v1, Lflipboard/service/FlipboardManager;->al:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    if-eqz v0, :cond_0

    iget-boolean v2, v1, Lflipboard/service/FlipboardManager;->al:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    iput-boolean v8, v1, Lflipboard/service/FlipboardManager;->al:Z

    iget-object v3, v1, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    if-eqz v3, :cond_0

    iget-boolean v4, v3, Lflipboard/activities/FlipboardActivity;->Q:Z

    if-eqz v4, :cond_0

    new-instance v4, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v4}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    iget-object v5, v1, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const v6, 0x7f0d029f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    iget-object v5, v1, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const v6, 0x7f0d029e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v9

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    const v2, 0x7f0d004a

    invoke-virtual {v4, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    const v2, 0x7f0d023f

    invoke-virtual {v4, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    new-instance v2, Lflipboard/service/FlipboardManager$21;

    invoke-direct {v2, v1, v0, v3}, Lflipboard/service/FlipboardManager$21;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;Lflipboard/activities/FlipboardActivity;)V

    iput-object v2, v4, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    iget-object v0, v3, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "relogin"

    invoke-virtual {v4, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
