.class public Lflipboard/service/Flap$RemoveFromMagazineRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4725
    iput-object p1, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->b:Lflipboard/service/Flap;

    .line 4726
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4727
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$RemoveFromMagazineRequest;
    .locals 3

    .prologue
    .line 4731
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 4732
    iput-object p1, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->c:Ljava/lang/String;

    .line 4733
    iget-object v0, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->d:Ljava/lang/String;

    .line 4734
    iget-object v0, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->e:Ljava/lang/String;

    .line 4735
    iput-object p3, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4736
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4737
    return-object p0
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4743
    iget-object v0, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/social/destroy"

    iget-object v2, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "url"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->e:Ljava/lang/String;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    const-string v5, "oid"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "target"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4744
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 4745
    iget-object v1, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->b:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$RemoveFromMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4746
    return-void
.end method
