.class Lflipboard/service/FlipboardManager$17$1;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$17;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager$17;)V
    .locals 0

    .prologue
    .line 1502
    iput-object p1, p0, Lflipboard/service/FlipboardManager$17$1;->a:Lflipboard/service/FlipboardManager$17;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1502
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/FlipboardManager$44;->a:[I

    invoke-virtual {p2}, Lflipboard/service/User$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$17$1;->a:Lflipboard/service/FlipboardManager$17;

    iget-object v0, v0, Lflipboard/service/FlipboardManager$17;->a:Lflipboard/util/Observer;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$17$1;->a:Lflipboard/service/FlipboardManager$17;

    iget-object v1, v1, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager$LoginMessage;->a:Lflipboard/service/FlipboardManager$LoginMessage;

    invoke-interface {v0, v1, v2, v3}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$17$1;->a:Lflipboard/service/FlipboardManager$17;

    iget-object v0, v0, Lflipboard/service/FlipboardManager$17;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->g:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
