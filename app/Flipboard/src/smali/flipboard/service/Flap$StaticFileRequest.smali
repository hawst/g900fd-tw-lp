.class Lflipboard/service/Flap$StaticFileRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$StaticFileObserver;

.field b:Ljava/lang/String;

.field c:Ljava/io/File;

.field d:Z

.field final synthetic e:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Z)V
    .locals 0

    .prologue
    .line 2868
    iput-object p1, p0, Lflipboard/service/Flap$StaticFileRequest;->e:Lflipboard/service/Flap;

    .line 2869
    invoke-direct {p0, p1, p2, p3}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Z)V

    .line 2870
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2885
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->b:Ljava/lang/String;

    .line 2888
    iget-boolean v4, p0, Lflipboard/service/Flap$StaticFileRequest;->d:Z

    if-eqz v4, :cond_0

    .line 2889
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->e:Lflipboard/service/Flap;

    iget-object v4, p0, Lflipboard/service/Flap$StaticFileRequest;->b:Ljava/lang/String;

    iget-object v5, p0, Lflipboard/service/Flap$StaticFileRequest;->n:Lflipboard/service/User;

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5, v6}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2892
    :cond_0
    sget-object v4, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v4, v3, [Ljava/lang/Object;

    aput-object v0, v4, v2

    .line 2894
    :try_start_0
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v6, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2895
    const-string v4, "Content-Type"

    const-string v5, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v6, v4, v5}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2896
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2897
    invoke-virtual {v4}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2900
    iget-object v4, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    move v5, v3

    .line 2901
    :goto_0
    if-eqz v0, :cond_14

    .line 2902
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ETag-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2903
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "LastModified-"

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v3

    move-object v3, v0

    .line 2906
    :goto_1
    if-eqz v5, :cond_13

    if-eqz v4, :cond_13

    .line 2907
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2909
    :goto_2
    if-eqz v0, :cond_1

    .line 2910
    const-string v7, "If-None-Match"

    invoke-virtual {v6, v7, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2913
    :cond_1
    if-eqz v5, :cond_12

    if-eqz v3, :cond_12

    .line 2914
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const/4 v5, 0x0

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2916
    :goto_3
    if-eqz v0, :cond_2

    .line 2917
    const-string v5, "If-Modified-Since"

    invoke-virtual {v6, v5, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2920
    :cond_2
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->e:Lflipboard/service/Flap;

    iget-boolean v5, p0, Lflipboard/service/Flap$StaticFileRequest;->o:Z

    invoke-virtual {v0, v6, v5}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;Z)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 2921
    const-string v0, "X-Flipboard-Server"

    invoke-interface {v5, v0}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "x-amz-meta-flipboard"

    invoke-interface {v5, v0}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2922
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "Response does not have Flipboard headers, must have been captured by a login screen on the network"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2923
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2924
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    array-length v7, v4

    move v0, v2

    :goto_4
    if-ge v0, v7, :cond_4

    aget-object v2, v4, v0

    .line 2925
    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, ", "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2924
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_3
    move v5, v2

    .line 2900
    goto/16 :goto_0

    .line 2927
    :cond_4
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v3, v0, v2

    .line 2928
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, v6, v5}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2931
    :cond_5
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-gez v2, :cond_5

    .line 2933
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 2935
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    const-string v2, "Response does not have Flipboard headers, must have been captured by a login screen on the network"

    invoke-interface {v0, v2}, Lflipboard/service/Flap$StaticFileObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3005
    :goto_5
    iput-object v1, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    .line 3006
    :goto_6
    return-void

    .line 2933
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v2
    :try_end_3
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2999
    :catch_0
    move-exception v0

    .line 3000
    :try_start_4
    iget-object v2, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lflipboard/service/Flap$StaticFileObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3005
    iput-object v1, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    goto :goto_6

    .line 2937
    :cond_6
    :try_start_5
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 2938
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".new"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2939
    invoke-virtual {p0, v6, v5}, Lflipboard/service/Flap$StaticFileRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v6

    .line 2940
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2941
    const/16 v7, 0x130

    if-ne v0, v7, :cond_7

    .line 2944
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 2946
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lflipboard/service/Flap$StaticFileObserver;->a(Z)V
    :try_end_5
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_5

    .line 3001
    :catch_1
    move-exception v0

    .line 3002
    :try_start_6
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3003
    iget-object v2, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lflipboard/service/Flap$StaticFileObserver;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 3005
    iput-object v1, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    goto :goto_6

    .line 2947
    :cond_7
    const/16 v7, 0x1a2

    if-ne v0, v7, :cond_9

    .line 2948
    :try_start_7
    const-string v0, "X-Flipboard-Reason"

    invoke-interface {v5, v0}, Lorg/apache/http/HttpResponse;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 2949
    iget-object v2, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    if-nez v0, :cond_8

    const-string v0, "service down for maintenance"

    :goto_7
    invoke-interface {v2, v0}, Lflipboard/service/Flap$StaticFileObserver;->b(Ljava/lang/String;)V
    :try_end_7
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_5

    .line 3005
    :catchall_1
    move-exception v0

    iput-object v1, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    throw v0

    .line 2949
    :cond_8
    :try_start_8
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 2950
    :cond_9
    const/16 v7, 0xc8

    if-ne v0, v7, :cond_f

    .line 2951
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2953
    const/16 v0, 0x400

    :try_start_9
    new-array v0, v0, [B

    .line 2955
    :goto_8
    const/4 v8, 0x0

    const/16 v9, 0x400

    invoke-virtual {v6, v0, v8, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    if-lez v8, :cond_a

    .line 2956
    const/4 v9, 0x0

    invoke-virtual {v7, v0, v9, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_8

    .line 2959
    :catchall_2
    move-exception v0

    :try_start_a
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    throw v0

    :cond_a
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 2961
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2962
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2964
    :cond_b
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2965
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t rename "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/service/Flap$StaticFileObserver;->a(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2968
    :cond_c
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2970
    const-string v0, "Last-Modified"

    invoke-interface {v5, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 2971
    if-eqz v0, :cond_11

    .line 2972
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 2974
    :goto_9
    if-eqz v0, :cond_d

    .line 2975
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2982
    :goto_a
    const-string v0, "Etag"

    invoke-interface {v5, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 2983
    if-eqz v0, :cond_10

    .line 2984
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 2986
    :goto_b
    if-eqz v0, :cond_e

    .line 2987
    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2991
    :goto_c
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2993
    iget-object v0, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lflipboard/service/Flap$StaticFileObserver;->a(Z)V

    goto/16 :goto_5

    .line 2977
    :cond_d
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_a

    .line 2989
    :cond_e
    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_c

    .line 2996
    :cond_f
    iget-object v2, p0, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected http response: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lflipboard/service/Flap$StaticFileObserver;->a(Ljava/lang/String;)V
    :try_end_a
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_5

    :cond_10
    move-object v0, v1

    goto :goto_b

    :cond_11
    move-object v0, v1

    goto :goto_9

    :cond_12
    move-object v0, v1

    goto/16 :goto_3

    :cond_13
    move-object v0, v1

    goto/16 :goto_2

    :cond_14
    move-object v3, v1

    move-object v4, v1

    goto/16 :goto_1
.end method
