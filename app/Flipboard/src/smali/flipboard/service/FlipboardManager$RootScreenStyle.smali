.class public final enum Lflipboard/service/FlipboardManager$RootScreenStyle;
.super Ljava/lang/Enum;
.source "FlipboardManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/FlipboardManager$RootScreenStyle;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/FlipboardManager$RootScreenStyle;

.field public static final enum b:Lflipboard/service/FlipboardManager$RootScreenStyle;

.field private static final synthetic c:[Lflipboard/service/FlipboardManager$RootScreenStyle;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 488
    new-instance v0, Lflipboard/service/FlipboardManager$RootScreenStyle;

    const-string v1, "TOC"

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$RootScreenStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    new-instance v0, Lflipboard/service/FlipboardManager$RootScreenStyle;

    const-string v1, "TAB"

    invoke-direct {v0, v1, v3}, Lflipboard/service/FlipboardManager$RootScreenStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->c:[Lflipboard/service/FlipboardManager$RootScreenStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 488
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/FlipboardManager$RootScreenStyle;
    .locals 1

    .prologue
    .line 488
    const-class v0, Lflipboard/service/FlipboardManager$RootScreenStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/FlipboardManager$RootScreenStyle;

    return-object v0
.end method

.method public static values()[Lflipboard/service/FlipboardManager$RootScreenStyle;
    .locals 1

    .prologue
    .line 488
    sget-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->c:[Lflipboard/service/FlipboardManager$RootScreenStyle;

    invoke-virtual {v0}, [Lflipboard/service/FlipboardManager$RootScreenStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/FlipboardManager$RootScreenStyle;

    return-object v0
.end method
