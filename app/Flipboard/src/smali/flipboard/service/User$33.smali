.class Lflipboard/service/User$33;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/User$StateChanger;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2272
    iput-object p1, p0, Lflipboard/service/User$33;->c:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$33;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/service/User$33;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2275
    iget-object v0, p0, Lflipboard/service/User$33;->c:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2276
    iget-object v0, p0, Lflipboard/service/User$33;->c:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v5

    iget-object v2, p0, Lflipboard/service/User$33;->a:Ljava/util/List;

    iget-object v1, p0, Lflipboard/service/User$33;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v0, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v6, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-nez v6, :cond_0

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    :cond_0
    iget-object v0, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v6, v6, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v6, v6, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$MutedAuthor;

    iget-object v7, v0, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    if-nez v7, :cond_2

    iget-object v7, v0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    if-eqz v7, :cond_1

    :cond_2
    iget-object v7, v0, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    if-eqz v7, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    :goto_2
    move v2, v0

    goto :goto_1

    :cond_3
    iget-object v0, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v1, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-nez v1, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    :cond_4
    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    move-object v1, v0

    goto :goto_0

    :cond_5
    if-lez v2, :cond_7

    move v0, v4

    :goto_3
    iput-boolean v0, v5, Lflipboard/objs/UserState;->m:Z

    if-lez v2, :cond_6

    move v3, v4

    .line 2279
    :cond_6
    :goto_4
    return v3

    :cond_7
    move v0, v3

    .line 2276
    goto :goto_3

    .line 2278
    :cond_8
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Can\'t mute user, currentState is null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_2

    :cond_a
    move-object v1, v0

    goto :goto_0
.end method
