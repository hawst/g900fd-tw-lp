.class public Lflipboard/service/FLAdManager;
.super Lflipboard/util/Observable;
.source "FLAdManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/util/Observable",
        "<",
        "Lflipboard/service/FLAdManager;",
        "Lflipboard/service/FLAdManager$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lflipboard/util/Log;

.field static final synthetic e:Z


# instance fields
.field public b:Lflipboard/gui/section/GroupFranchiseMeta;

.field public c:I

.field public volatile d:I

.field private f:Z

.field private final g:Ljava/lang/String;

.field private final h:Lflipboard/objs/FeedItem;

.field private volatile i:Lflipboard/service/FLAdManager$AdAsset;

.field private volatile j:I

.field private volatile k:Lflipboard/objs/Ad;

.field private volatile l:Lflipboard/service/Flap$TypedResultObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/Ad;",
            ">;"
        }
    .end annotation
.end field

.field private volatile m:I

.field private n:Lflipboard/service/FLAdManager$AdPageIndices;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lflipboard/service/FLAdManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/service/FLAdManager;->e:Z

    .line 35
    const-string v0, "admanager"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/gui/section/GroupFranchiseMeta;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lflipboard/util/Observable;-><init>()V

    .line 114
    iput-object p1, p0, Lflipboard/service/FLAdManager;->g:Ljava/lang/String;

    .line 115
    iput-object p2, p0, Lflipboard/service/FLAdManager;->h:Lflipboard/objs/FeedItem;

    .line 116
    iput-object p3, p0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 117
    return-void
.end method

.method public static a()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 40
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    iget v0, v1, Lflipboard/service/FlipboardManager;->ao:I

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Point;

    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v2

    iget v3, v1, Lflipboard/service/FlipboardManager;->as:I

    sub-int/2addr v2, v3

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v3

    iget v1, v1, Lflipboard/service/FlipboardManager;->at:I

    sub-int v1, v3, v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 41
    :goto_0
    new-instance v1, Landroid/graphics/Point;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 42
    return-object v1

    .line 40
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    iget v2, v1, Lflipboard/service/FlipboardManager;->ao:I

    iget v1, v1, Lflipboard/service/FlipboardManager;->ap:I

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    :cond_1
    iget v0, v1, Lflipboard/service/FlipboardManager;->aq:I

    if-nez v0, :cond_2

    new-instance v0, Landroid/graphics/Point;

    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v2

    iget v3, v1, Lflipboard/service/FlipboardManager;->as:I

    sub-int/2addr v2, v3

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v3

    iget v1, v1, Lflipboard/service/FlipboardManager;->at:I

    sub-int v1, v3, v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/graphics/Point;

    iget v2, v1, Lflipboard/service/FlipboardManager;->aq:I

    iget v1, v1, Lflipboard/service/FlipboardManager;->ar:I

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/FLAdManager;Lflipboard/service/FLAdManager$AdAsset;)Lflipboard/service/FLAdManager$AdAsset;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    return-object p1
.end method

.method public static a(Ljava/lang/String;)Lflipboard/service/FLAdManager;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    new-instance v0, Lflipboard/service/FLAdManager;

    invoke-direct {v0, p0, v1, v1}, Lflipboard/service/FLAdManager;-><init>(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/gui/section/GroupFranchiseMeta;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lflipboard/gui/section/GroupFranchiseMeta;)Lflipboard/service/FLAdManager;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lflipboard/service/FLAdManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lflipboard/service/FLAdManager;-><init>(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/gui/section/GroupFranchiseMeta;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lflipboard/objs/FeedItem;)Lflipboard/service/FLAdManager;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lflipboard/service/FLAdManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lflipboard/service/FLAdManager;-><init>(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/gui/section/GroupFranchiseMeta;)V

    return-object v0
.end method

.method static synthetic a(Lflipboard/service/FLAdManager;)Lflipboard/service/Flap$TypedResultObserver;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 493
    if-eqz p2, :cond_0

    .line 494
    const-string v0, "market:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 495
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 496
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    const-string v0, "flipboard:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 498
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 499
    const-string v1, "source"

    const-string v2, "advertisement"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const-string v1, "originSectionIdentifier"

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Lflipboard/service/FlipboardUrlHandler;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/os/Bundle;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 505
    :cond_2
    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 431
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p1, p2}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;JJ)V

    .line 432
    return-void
.end method

.method private static a(Ljava/lang/String;JJ)V
    .locals 7

    .prologue
    .line 436
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v6, Lflipboard/service/FLAdManager$4;

    invoke-direct {v6}, Lflipboard/service/FLAdManager$4;-><init>()V

    new-instance v0, Lflipboard/service/Flap$AdMetricRequest;

    invoke-direct {v0, v1, v2}, Lflipboard/service/Flap$AdMetricRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move-object v1, p0

    move-wide v2, p3

    move-wide v4, p1

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap$AdMetricRequest;->a(Ljava/lang/String;JJLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdMetricRequest;

    .line 447
    return-void
.end method

.method public static a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lflipboard/service/FLAdManager$ImpressionEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 407
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz p1, :cond_1

    iget-object v3, p1, Lflipboard/service/FLAdManager$ImpressionEvent;->d:Ljava/lang/String;

    :goto_0
    new-instance v6, Lflipboard/service/FLAdManager$3;

    invoke-direct {v6}, Lflipboard/service/FLAdManager$3;-><init>()V

    new-instance v1, Lflipboard/service/Flap$AdImpressionRequest;

    invoke-direct {v1, v0, v2}, Lflipboard/service/Flap$AdImpressionRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move-object v2, p0

    invoke-virtual/range {v1 .. v6}, Lflipboard/service/Flap$AdImpressionRequest;->a(Ljava/lang/String;Ljava/lang/String;JLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdImpressionRequest;

    sget-object v0, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lflipboard/service/FLAdManager;->a(Ljava/util/List;)V

    .line 408
    :cond_0
    return-void

    .line 407
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 472
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Lflipboard/service/FLAdManager$6;

    invoke-direct {v4}, Lflipboard/service/FLAdManager$6;-><init>()V

    new-instance v5, Lflipboard/service/Flap$AdClickRequest;

    invoke-direct {v5, v0, v1}, Lflipboard/service/Flap$AdClickRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v5, p0, v2, v3, v4}, Lflipboard/service/Flap$AdClickRequest;->a(Ljava/lang/String;JLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdClickRequest;

    .line 485
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 487
    invoke-static {p1}, Lflipboard/service/FLAdManager;->a(Ljava/util/List;)V

    .line 489
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 452
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 454
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v4, Lflipboard/service/FLAdManager$5;

    invoke-direct {v4}, Lflipboard/service/FLAdManager$5;-><init>()V

    new-instance v5, Lflipboard/service/Flap$HttpRetryRequest;

    invoke-direct {v5, v2, v3}, Lflipboard/service/Flap$HttpRetryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const/4 v2, 0x1

    iput-boolean v2, v5, Lflipboard/service/Flap$HttpRetryRequest;->d:Z

    invoke-virtual {v5, v0, v4}, Lflipboard/service/Flap$HttpRetryRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$HttpRetryRequest;

    goto :goto_0

    .line 468
    :cond_1
    return-void
.end method

.method private a(ZILjava/lang/String;ILflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/lang/String;",
            "I",
            "Lflipboard/service/FLAdManager$ImpressionEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    new-instance v2, Lflipboard/service/FLAdManager$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lflipboard/service/FLAdManager$2;-><init>(Lflipboard/service/FLAdManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/FLAdManager;->h:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/FLAdManager;->h:Lflipboard/objs/FeedItem;

    iget-object v5, v2, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    .line 385
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/FLAdManager;->h:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_2

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/FLAdManager;->h:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 388
    :goto_1
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->h:Landroid/content/Context;

    invoke-static {v2}, Lflipboard/util/Connectivity;->a(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 389
    invoke-static {v2}, Lflipboard/util/Connectivity;->a(Landroid/net/NetworkInfo;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v17, "wifi"

    .line 390
    :goto_2
    const-string v3, "mobile"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v18

    .line 393
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v2, :cond_7

    .line 394
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v7, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/FLAdManager;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v6, v3, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    iget v8, v3, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    if-eqz p5, :cond_6

    move-object/from16 v0, p5

    iget-object v11, v0, Lflipboard/service/FLAdManager$ImpressionEvent;->d:Ljava/lang/String;

    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    move-object/from16 v16, v0

    new-instance v3, Lflipboard/service/Flap$AdQueryRequest;

    invoke-direct {v3, v2, v7}, Lflipboard/service/Flap$AdQueryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move/from16 v7, p1

    move/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v14, p4

    invoke-virtual/range {v3 .. v18}, Lflipboard/service/Flap$AdQueryRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Lflipboard/service/Flap$TypedResultObserver;Ljava/lang/String;I)Lflipboard/service/Flap$AdQueryRequest;

    .line 399
    :goto_5
    sget-object v2, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    move-object/from16 v0, p5

    if-ne v0, v2, :cond_0

    if-eqz p6, :cond_0

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 401
    invoke-static/range {p6 .. p6}, Lflipboard/service/FLAdManager;->a(Ljava/util/List;)V

    .line 403
    :cond_0
    return-void

    .line 384
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 385
    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    .line 389
    :cond_3
    invoke-static {v2}, Lflipboard/util/Connectivity;->b(Landroid/net/NetworkInfo;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v17, "mobile"

    goto :goto_2

    :cond_4
    const-string v17, "none"

    goto :goto_2

    .line 390
    :cond_5
    const/16 v18, 0x0

    goto :goto_3

    .line 394
    :cond_6
    const/4 v11, 0x0

    goto :goto_4

    .line 396
    :cond_7
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/service/FLAdManager;->g:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz p5, :cond_8

    move-object/from16 v0, p5

    iget-object v0, v0, Lflipboard/service/FLAdManager$ImpressionEvent;->d:Ljava/lang/String;

    move-object/from16 v25, v0

    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    move-object/from16 v30, v0

    new-instance v19, Lflipboard/service/Flap$AdQueryRequest;

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Lflipboard/service/Flap$AdQueryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move-object/from16 v21, v5

    move/from16 v22, p1

    move/from16 v23, p2

    move-object/from16 v24, p3

    move/from16 v28, p4

    move-object/from16 v29, v15

    move-object/from16 v31, v17

    move/from16 v32, v18

    invoke-virtual/range {v19 .. v32}, Lflipboard/service/Flap$AdQueryRequest;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Lflipboard/service/Flap$TypedResultObserver;Ljava/lang/String;I)Lflipboard/service/Flap$AdQueryRequest;

    goto :goto_5

    :cond_8
    const/16 v25, 0x0

    goto :goto_6
.end method

.method static synthetic b(Lflipboard/service/FLAdManager;)Lflipboard/service/Flap$TypedResultObserver;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 427
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;JJ)V

    .line 428
    return-void
.end method

.method static synthetic c(Lflipboard/service/FLAdManager;)Lflipboard/service/FLAdManager$AdAsset;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    return-object v0
.end method

.method public static c()Z
    .locals 3

    .prologue
    .line 595
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "show_hitrects_for_ads"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 3

    .prologue
    .line 600
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "disable_ad_frequency_cap"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 259
    iget-boolean v0, p0, Lflipboard/service/FLAdManager;->f:Z

    if-nez v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 263
    :cond_0
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 264
    monitor-enter p0

    .line 265
    :try_start_0
    iget-object v0, p0, Lflipboard/service/FLAdManager;->o:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    iget-object v0, p0, Lflipboard/service/FLAdManager;->p:Ljava/util/List;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(II)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v3, 0x0

    .line 124
    invoke-virtual {p0, v0, v2}, Lflipboard/service/FLAdManager;->a(IZ)V

    .line 127
    iput-object v3, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    .line 128
    iput p2, p0, Lflipboard/service/FLAdManager;->d:I

    .line 129
    iput v0, p0, Lflipboard/service/FLAdManager;->j:I

    .line 130
    iput-object v3, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    .line 131
    iput-object v3, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    .line 132
    iput v0, p0, Lflipboard/service/FLAdManager;->m:I

    .line 133
    new-instance v0, Lflipboard/service/FLAdManager$AdPageIndices;

    invoke-direct {v0, v2}, Lflipboard/service/FLAdManager$AdPageIndices;-><init>(B)V

    iput-object v0, p0, Lflipboard/service/FLAdManager;->n:Lflipboard/service/FLAdManager$AdPageIndices;

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/FLAdManager;->o:Ljava/util/List;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/FLAdManager;->p:Ljava/util/List;

    .line 136
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "enable_ads"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/FLAdManager;->f:Z

    .line 137
    iget-boolean v0, p0, Lflipboard/service/FLAdManager;->f:Z

    if-eqz v0, :cond_0

    move-object v0, p0

    move v2, p1

    move v4, p2

    move-object v5, v3

    move-object v6, v3

    .line 138
    invoke-direct/range {v0 .. v6}, Lflipboard/service/FLAdManager;->a(ZILjava/lang/String;ILflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    .line 140
    :cond_0
    return-void
.end method

.method public final a(ILflipboard/objs/Ad;)V
    .locals 9

    .prologue
    .line 170
    iget-boolean v0, p0, Lflipboard/service/FLAdManager;->f:Z

    if-nez v0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v3, p0, Lflipboard/service/FLAdManager;->o:Ljava/util/List;

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget v3, p0, Lflipboard/service/FLAdManager;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    iget v1, p0, Lflipboard/service/FLAdManager;->m:I

    const/4 v0, 0x0

    move v5, v0

    :goto_1
    iget-object v0, p0, Lflipboard/service/FLAdManager;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    iget-object v0, p0, Lflipboard/service/FLAdManager;->o:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v1, :cond_11

    iget-object v6, p0, Lflipboard/service/FLAdManager;->n:Lflipboard/service/FLAdManager$AdPageIndices;

    if-le v0, v1, :cond_3

    if-eq v0, p1, :cond_2

    const/4 v3, 0x1

    move v4, v3

    :goto_2
    iget-object v3, v6, Lflipboard/service/FLAdManager$AdPageIndices;->a:Ljava/util/TreeMap;

    mul-int/lit8 v6, v1, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    mul-int/lit8 v8, v0, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v6, v7, v8, v4}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v3

    sget-object v6, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v1, 0x2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v1

    const/4 v1, 0x3

    invoke-interface {v3}, Ljava/util/NavigableMap;->keySet()Ljava/util/Set;

    move-result-object v4

    aput-object v4, v6, v1

    move-object v1, v3

    :goto_3
    invoke-interface {v1}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v3

    if-eqz v3, :cond_10

    if-nez v2, :cond_f

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_4
    new-instance v2, Lflipboard/service/FLAdManager$1;

    invoke-direct {v2, p0, v3}, Lflipboard/service/FLAdManager$1;-><init>(Lflipboard/service/FLAdManager;Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    move v4, v3

    goto :goto_2

    :cond_3
    if-eq v0, p1, :cond_4

    const/4 v3, 0x1

    move v4, v3

    :goto_6
    iget-object v3, v6, Lflipboard/service/FLAdManager$AdPageIndices;->a:Ljava/util/TreeMap;

    mul-int/lit8 v6, v0, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    mul-int/lit8 v7, v1, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v6, v4, v7, v8}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v3

    sget-object v6, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v4

    const/4 v1, 0x3

    invoke-interface {v3}, Ljava/util/NavigableMap;->keySet()Ljava/util/Set;

    move-result-object v4

    aput-object v4, v6, v1

    move-object v1, v3

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    move v4, v3

    goto :goto_6

    :cond_5
    iget-object v0, p0, Lflipboard/service/FLAdManager;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lflipboard/service/FLAdManager;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_7

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 181
    :cond_6
    const/4 v3, 0x0

    .line 182
    const/4 v5, 0x0

    .line 183
    const/4 v6, 0x0

    .line 184
    const/4 v0, 0x0

    .line 189
    const/4 v4, -0x1

    .line 190
    monitor-enter p0

    .line 191
    :try_start_1
    iput p1, p0, Lflipboard/service/FLAdManager;->m:I

    .line 192
    if-eqz p2, :cond_8

    .line 194
    iget-object v3, p2, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    .line 195
    sget-object v5, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    .line 197
    iget-object v6, p2, Lflipboard/objs/Ad;->d:Ljava/util/List;

    .line 198
    iget-object v1, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    if-nez v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    if-nez v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    iget v1, v1, Lflipboard/objs/Ad;->i:I

    if-ne p1, v1, :cond_b

    .line 201
    iget-object v1, p0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-nez v1, :cond_b

    .line 203
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    iget v2, v2, Lflipboard/objs/Ad;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 204
    const/4 v0, 0x1

    .line 206
    const/4 v1, 0x0

    iput v1, p0, Lflipboard/service/FLAdManager;->d:I

    .line 207
    iput p1, p0, Lflipboard/service/FLAdManager;->j:I

    move v1, v0

    .line 239
    :goto_8
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 241
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v0

    const/4 v7, 0x1

    if-eqz p2, :cond_c

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v2, v7

    const/4 v0, 0x2

    iget v7, p0, Lflipboard/service/FLAdManager;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v0

    const/4 v0, 0x3

    iget v7, p0, Lflipboard/service/FLAdManager;->j:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v0

    .line 243
    if-eqz v1, :cond_d

    .line 244
    if-gez v4, :cond_7

    .line 245
    iget v4, p0, Lflipboard/service/FLAdManager;->d:I

    .line 247
    :cond_7
    const/4 v1, 0x0

    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v6}, Lflipboard/service/FLAdManager;->a(ZILjava/lang/String;ILflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto/16 :goto_0

    .line 213
    :cond_8
    :try_start_2
    iget v1, p0, Lflipboard/service/FLAdManager;->j:I

    if-ltz v1, :cond_9

    iget v1, p0, Lflipboard/service/FLAdManager;->j:I

    if-le p1, v1, :cond_a

    .line 214
    :cond_9
    iget v1, p0, Lflipboard/service/FLAdManager;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/service/FLAdManager;->d:I

    .line 215
    iput p1, p0, Lflipboard/service/FLAdManager;->j:I

    .line 218
    :cond_a
    iget-object v1, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    if-nez v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    if-nez v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    iget v1, v1, Lflipboard/objs/Ad;->i:I

    if-lt p1, v1, :cond_b

    .line 220
    iget-object v1, p0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-nez v1, :cond_b

    .line 224
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 225
    const/4 v0, 0x1

    .line 228
    const/4 v1, 0x1

    iput v1, p0, Lflipboard/service/FLAdManager;->d:I

    .line 234
    const/4 v4, 0x0

    .line 235
    iput p1, p0, Lflipboard/service/FLAdManager;->j:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_b
    move v1, v0

    goto :goto_8

    .line 239
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 241
    :cond_c
    const/4 v0, 0x0

    goto :goto_9

    .line 248
    :cond_d
    if-eqz v3, :cond_0

    .line 249
    sget-boolean v0, Lflipboard/service/FLAdManager;->e:Z

    if-nez v0, :cond_e

    if-nez v5, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 250
    :cond_e
    iget-object v0, p2, Lflipboard/objs/Ad;->d:Ljava/util/List;

    invoke-static {v3, v5, v0}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_f
    move-object v1, v2

    goto/16 :goto_4

    :cond_10
    move-object v1, v2

    goto/16 :goto_5

    :cond_11
    move v0, v1

    move-object v1, v2

    goto/16 :goto_5
.end method

.method public final a(IZ)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 605
    iget-boolean v0, p0, Lflipboard/service/FLAdManager;->f:Z

    if-nez v0, :cond_0

    .line 643
    :goto_0
    return-void

    .line 610
    :cond_0
    sget-boolean v0, Lflipboard/service/FLAdManager;->e:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 614
    :cond_1
    monitor-enter p0

    .line 615
    :try_start_0
    iget-object v0, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    if-eqz v0, :cond_2

    .line 617
    iget-object v0, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    .line 618
    const/4 v2, 0x0

    iput-object v2, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    move v2, v1

    .line 625
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 627
    if-eqz v0, :cond_4

    .line 628
    if-eqz p2, :cond_3

    .line 630
    iget-object v2, v0, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v3, v2, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    iget v4, p0, Lflipboard/service/FLAdManager;->d:I

    sget-object v5, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    iget-object v0, v0, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v6, v0, Lflipboard/objs/Ad;->d:Ljava/util/List;

    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v6}, Lflipboard/service/FLAdManager;->a(ZILjava/lang/String;ILflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_0

    .line 619
    :cond_2
    :try_start_1
    iget-object v0, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;

    if-eqz v0, :cond_6

    .line 622
    const/4 v0, 0x1

    .line 623
    const/4 v2, 0x0

    iput-object v2, p0, Lflipboard/service/FLAdManager;->l:Lflipboard/service/Flap$TypedResultObserver;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v0

    move-object v0, v3

    goto :goto_1

    .line 625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 633
    :cond_3
    iget-object v1, v0, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v1, v1, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    sget-object v2, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    iget-object v0, v0, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v0, v0, Lflipboard/objs/Ad;->d:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_0

    .line 635
    :cond_4
    if-eqz p2, :cond_5

    if-eqz v2, :cond_5

    .line 638
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    .line 639
    iget v4, p0, Lflipboard/service/FLAdManager;->d:I

    sget-object v5, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    move-object v0, p0

    move v2, p1

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lflipboard/service/FLAdManager;->a(ZILjava/lang/String;ILflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_0

    .line 641
    :cond_5
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    goto :goto_0

    :cond_6
    move-object v0, v3

    move v2, v1

    goto :goto_1
.end method

.method public final b(II)Lflipboard/service/FLAdManager$AdAsset;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 537
    iget-boolean v0, p0, Lflipboard/service/FLAdManager;->f:Z

    if-nez v0, :cond_1

    .line 587
    :cond_0
    :goto_0
    return-object v7

    .line 544
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/FLAdManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    monitor-enter p0

    .line 547
    :try_start_0
    iget-object v3, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    .line 548
    if-eqz v3, :cond_8

    .line 549
    iget-object v0, v3, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    .line 550
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 551
    invoke-virtual {v0, v4, v5}, Lflipboard/objs/Ad;->a(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 554
    const/4 v2, 0x0

    iput-object v2, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    .line 555
    sget-object v2, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v8, v0, Lflipboard/objs/Ad;->j:J

    sub-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    :goto_1
    monitor-exit p0

    .line 583
    if-eqz v0, :cond_0

    .line 584
    iget-object v3, v0, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    iget v4, p0, Lflipboard/service/FLAdManager;->d:I

    sget-object v5, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    iget-object v6, v0, Lflipboard/objs/Ad;->d:Ljava/util/List;

    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v6}, Lflipboard/service/FLAdManager;->a(ZILjava/lang/String;ILflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_0

    .line 556
    :cond_2
    :try_start_1
    iget v2, p0, Lflipboard/service/FLAdManager;->d:I

    iget v4, v0, Lflipboard/objs/Ad;->h:I

    if-ge v2, v4, :cond_3

    iget-object v2, p0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v2, :cond_8

    .line 559
    :cond_3
    iget-object v2, p0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v2, :cond_4

    .line 560
    iget v2, p0, Lflipboard/service/FLAdManager;->c:I

    iget v4, v0, Lflipboard/objs/Ad;->h:I

    add-int/2addr v2, v4

    .line 562
    if-lt p1, v2, :cond_7

    .line 564
    const/4 v3, 0x0

    iput-object v3, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    move v4, v2

    move-object v3, v7

    move-object v2, v0

    .line 570
    :goto_2
    if-eqz v3, :cond_6

    .line 571
    sget-object v1, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    const/4 v2, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    const/4 v2, 0x3

    iget v5, p0, Lflipboard/service/FLAdManager;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    const/4 v2, 0x4

    iget v5, p0, Lflipboard/service/FLAdManager;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 572
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    .line 573
    iput v4, v0, Lflipboard/objs/Ad;->i:I

    .line 574
    iput-object v0, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    .line 575
    iget-object v0, p0, Lflipboard/service/FLAdManager;->n:Lflipboard/service/FLAdManager$AdPageIndices;

    iget-object v1, v3, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-nez v1, :cond_5

    iget-object v0, v0, Lflipboard/service/FLAdManager$AdPageIndices;->a:Ljava/util/TreeMap;

    mul-int/lit8 v1, v4, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    :goto_3
    monitor-exit p0

    move-object v7, v3

    goto/16 :goto_0

    .line 567
    :cond_4
    iget v2, p0, Lflipboard/service/FLAdManager;->j:I

    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    move v4, v2

    move-object v2, v7

    goto :goto_2

    .line 575
    :cond_5
    iget-object v0, v0, Lflipboard/service/FLAdManager$AdPageIndices;->a:Ljava/util/TreeMap;

    mul-int/lit8 v1, v4, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    move-object v0, v2

    goto/16 :goto_1

    :cond_7
    move v4, v2

    move-object v2, v7

    goto :goto_2

    :cond_8
    move-object v0, v7

    goto/16 :goto_1
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 516
    iget-boolean v2, p0, Lflipboard/service/FLAdManager;->f:Z

    if-nez v2, :cond_1

    .line 527
    :cond_0
    :goto_0
    return v0

    .line 520
    :cond_1
    iget-object v3, p0, Lflipboard/service/FLAdManager;->i:Lflipboard/service/FLAdManager$AdAsset;

    .line 521
    if-eqz v3, :cond_0

    .line 522
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 523
    iget-object v2, p0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/service/FLAdManager;->k:Lflipboard/objs/Ad;

    if-nez v2, :cond_3

    move v2, v1

    .line 524
    :goto_1
    if-nez v2, :cond_2

    iget-object v2, v3, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    invoke-virtual {v2, v4, v5}, Lflipboard/objs/Ad;->a(J)Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lflipboard/service/FLAdManager;->d:I

    iget-object v3, v3, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v3, v3, Lflipboard/objs/Ad;->h:I

    if-lt v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    .line 523
    goto :goto_1
.end method
