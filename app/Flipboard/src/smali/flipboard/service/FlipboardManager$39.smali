.class Lflipboard/service/FlipboardManager$39;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 3324
    iput-object p1, p0, Lflipboard/service/FlipboardManager$39;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$39;->a:Landroid/app/Activity;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 3351
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->a(Landroid/support/v4/app/DialogFragment;)V

    .line 3353
    iget-object v0, p0, Lflipboard/service/FlipboardManager$39;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "rate_state"

    const-string v2, "no"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3355
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "user-notification"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3356
    const-string v1, "reason"

    const-string v2, "rate denied"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3357
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 3358
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 3328
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->b(Landroid/support/v4/app/DialogFragment;)V

    .line 3330
    iget-object v0, p0, Lflipboard/service/FlipboardManager$39;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "rate_state"

    const-string v2, "yes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3331
    iget-object v0, p0, Lflipboard/service/FlipboardManager$39;->a:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lflipboard/service/FlipboardManager$39;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v3}, Lflipboard/service/FlipboardManager;->r(Lflipboard/service/FlipboardManager;)Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget-object v3, v3, Lflipboard/model/ConfigSetting;->AppDownloadURL:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 3333
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "user-notification"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3334
    const-string v1, "reason"

    const-string v2, "rate clicked"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3335
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 3336
    return-void
.end method

.method public final e(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 3340
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->e(Landroid/support/v4/app/DialogFragment;)V

    .line 3342
    iget-object v0, p0, Lflipboard/service/FlipboardManager$39;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "rate_state"

    const-string v2, "later"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3344
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "user-notification"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3345
    const-string v1, "reason"

    const-string v2, "rate later"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3346
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 3347
    return-void
.end method
