.class Lflipboard/service/Flap$UserInfoRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field protected a:Lflipboard/service/Flap$TypedResultObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/service/Flap;

.field private c:I


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2110
    iput-object p1, p0, Lflipboard/service/Flap$UserInfoRequest;->b:Lflipboard/service/Flap;

    .line 2111
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2112
    return-void
.end method


# virtual methods
.method public final a(ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$UserInfoRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/UserInfo;",
            ">;)",
            "Lflipboard/service/Flap$UserInfoRequest;"
        }
    .end annotation

    .prologue
    .line 2116
    iput-object p2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2117
    iput p1, p0, Lflipboard/service/Flap$UserInfoRequest;->c:I

    .line 2118
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2119
    return-object p0
.end method

.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 2125
    iget-object v0, p0, Lflipboard/service/Flap$UserInfoRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/userInfo"

    iget-object v3, p0, Lflipboard/service/Flap$UserInfoRequest;->n:Lflipboard/service/User;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "states"

    aput-object v5, v4, v7

    const-string v5, "user"

    aput-object v5, v4, v8

    const/4 v5, 0x2

    const-string v6, "revisions"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, p0, Lflipboard/service/Flap$UserInfoRequest;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2126
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 2128
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2129
    const-string v0, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-interface {v1, v0, v3}, Lorg/apache/http/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2130
    iget-object v0, p0, Lflipboard/service/Flap$UserInfoRequest;->b:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2131
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 2132
    new-instance v4, Lflipboard/service/Flap$UserInfoRequest$1;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$UserInfoRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v4, p0, v0}, Lflipboard/service/Flap$UserInfoRequest$1;-><init>(Lflipboard/service/Flap$UserInfoRequest;Ljava/io/InputStream;)V

    .line 2139
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2140
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2141
    iget-object v0, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected response from flap: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2164
    iput-object v2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2165
    :goto_0
    return-void

    .line 2144
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Lflipboard/service/Flap$StreamingJSONInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserInfo;

    .line 2145
    if-nez v0, :cond_1

    .line 2146
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const-string v1, "NULL result from flap: %T"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2164
    iput-object v2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_0

    .line 2149
    :cond_1
    :try_start_2
    iget-object v1, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/UserState$State;

    .line 2151
    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, v1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-eqz v1, :cond_2

    iget-object v3, v1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    if-eqz v3, :cond_2

    .line 2152
    iget-object v1, v1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    const-string v3, "tocSectionPages"

    invoke-virtual {v1, v3}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2153
    if-eqz v1, :cond_2

    .line 2154
    invoke-virtual {v0, v1}, Lflipboard/objs/UserInfo;->a(Ljava/util/List;)V

    .line 2157
    :cond_2
    iget-object v1, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2164
    iput-object v2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 2149
    goto :goto_1

    .line 2158
    :catch_0
    move-exception v0

    .line 2159
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2164
    iput-object v2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_0

    .line 2160
    :catch_1
    move-exception v0

    .line 2161
    :try_start_4
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2162
    iget-object v1, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2164
    iput-object v2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lflipboard/service/Flap$UserInfoRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    throw v0
.end method
