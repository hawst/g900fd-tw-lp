.class public Lflipboard/service/Flap$SaveRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Lflipboard/objs/FeedItem;

.field c:Lflipboard/service/Section;

.field d:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic e:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 3733
    iput-object p1, p0, Lflipboard/service/Flap$SaveRequest;->e:Lflipboard/service/Flap;

    .line 3734
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3735
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$SaveRequest;
    .locals 0

    .prologue
    .line 3739
    iput-object p1, p0, Lflipboard/service/Flap$SaveRequest;->a:Ljava/lang/String;

    .line 3740
    iput-object p3, p0, Lflipboard/service/Flap$SaveRequest;->b:Lflipboard/objs/FeedItem;

    .line 3741
    iput-object p2, p0, Lflipboard/service/Flap$SaveRequest;->c:Lflipboard/service/Section;

    .line 3742
    iput-object p4, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    .line 3743
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3744
    return-object p0
.end method

.method protected final a()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 3749
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 3750
    iget-object v0, p0, Lflipboard/service/Flap$SaveRequest;->e:Lflipboard/service/Flap;

    const-string v1, "/v1/social/save"

    iget-object v4, p0, Lflipboard/service/Flap$SaveRequest;->n:Lflipboard/service/User;

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "url"

    aput-object v6, v5, v9

    iget-object v6, p0, Lflipboard/service/Flap$SaveRequest;->b:Lflipboard/objs/FeedItem;

    iget-object v6, v6, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    aput-object v6, v5, v10

    const-string v6, "service"

    aput-object v6, v5, v11

    const/4 v6, 0x3

    iget-object v7, p0, Lflipboard/service/Flap$SaveRequest;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "title"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    iget-object v7, p0, Lflipboard/service/Flap$SaveRequest;->b:Lflipboard/objs/FeedItem;

    iget-object v7, v7, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "oid"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    iget-object v7, p0, Lflipboard/service/Flap$SaveRequest;->b:Lflipboard/objs/FeedItem;

    iget-object v7, v7, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v1, v4, v5}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3751
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/service/Flap$SaveRequest;->a:Ljava/lang/String;

    aput-object v4, v1, v9

    aput-object v0, v1, v10

    .line 3753
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3754
    const-string v0, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3755
    iget-object v0, p0, Lflipboard/service/Flap$SaveRequest;->e:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3756
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 3757
    new-instance v5, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$SaveRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v5, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3758
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3759
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 3760
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response from flap: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3780
    iput-object v8, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    .line 3781
    :goto_0
    return-void

    .line 3763
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3764
    if-eqz v0, :cond_2

    .line 3765
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3766
    const-string v1, "save"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, Lflipboard/service/Flap$SaveRequest;->c:Lflipboard/service/Section;

    iget-object v5, p0, Lflipboard/service/Flap$SaveRequest;->b:Lflipboard/objs/FeedItem;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V

    .line 3767
    iget-object v1, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3780
    :goto_1
    iput-object v8, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3769
    :cond_1
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3774
    :catch_0
    move-exception v0

    .line 3775
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3780
    iput-object v8, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3772
    :cond_2
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 3776
    :catch_1
    move-exception v0

    .line 3777
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3778
    iget-object v1, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3780
    iput-object v8, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v8, p0, Lflipboard/service/Flap$SaveRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method
