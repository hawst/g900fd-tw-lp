.class public Lflipboard/service/RemoteWatchedFile;
.super Ljava/lang/Object;
.source "RemoteWatchedFile.java"

# interfaces
.implements Lflipboard/service/Flap$StaticFileObserver;


# static fields
.field static final a:Lflipboard/util/Log;

.field static final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final b:Ljava/lang/String;

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/RemoteWatchedFile$Observer;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/io/File;

.field public e:Z

.field f:Z

.field public g:Z

.field private i:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    sput-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 32
    sput-object v0, Lflipboard/service/RemoteWatchedFile;->h:Ljava/util/Map;

    const-string v1, "config.json"

    const-string v2, "config/config.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->h:Ljava/util/Map;

    const-string v1, "dynamicStrings.json"

    const-string v2, "config/dynamicStrings.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->h:Ljava/util/Map;

    const-string v1, "services.json"

    const-string v2, "config/services.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    .line 51
    iput-object p1, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    .line 52
    invoke-direct {p0}, Lflipboard/service/RemoteWatchedFile;->d()V

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/service/RemoteWatchedFile;->c(Z)V

    .line 54
    return-void
.end method

.method static synthetic a(Lflipboard/service/RemoteWatchedFile;)Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/RemoteWatchedFile;->i:Ljava/util/TimerTask;

    return-object v0
.end method

.method private declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 281
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->i:Ljava/util/TimerTask;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Lflipboard/service/RemoteWatchedFile$1;

    invoke-direct {v0, p0}, Lflipboard/service/RemoteWatchedFile$1;-><init>(Lflipboard/service/RemoteWatchedFile;)V

    iput-object v0, p0, Lflipboard/service/RemoteWatchedFile;->i:Ljava/util/TimerTask;

    .line 291
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->i:Ljava/util/TimerTask;

    invoke-virtual {v0, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 57
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 58
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/service/RemoteWatchedFile;->f:Z

    .line 63
    iget-boolean v1, p0, Lflipboard/service/RemoteWatchedFile;->f:Z

    if-eqz v1, :cond_4

    .line 64
    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-static {v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "flap static files must be one of Flap."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lflipboard/service/Flap;->b:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v2, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v4, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lflipboard/service/Flap;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "contentGuide.json"

    if-ne v3, v5, :cond_1

    const-string v5, "content_guide_language"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "content_guide_locale"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lflipboard/service/Flap;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->Q:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    .line 72
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lflipboard/service/RemoteWatchedFile;->h:Ljava/util/Map;

    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/service/RemoteWatchedFile;->b(Z)V

    .line 73
    return-void

    .line 67
    :cond_4
    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 68
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Remote watched files must be either static flap files or valid urls. This is neither: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 72
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Z)V
    .locals 8

    .prologue
    .line 250
    invoke-virtual {p0}, Lflipboard/service/RemoteWatchedFile;->c()[B

    move-result-object v3

    .line 251
    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to load file: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 255
    :goto_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lflipboard/service/RemoteWatchedFile;->b(Z)V

    .line 256
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_3

    .line 257
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile$Observer;

    .line 258
    if-eqz v1, :cond_1

    .line 259
    invoke-interface {v0, v1}, Lflipboard/service/RemoteWatchedFile$Observer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 260
    goto :goto_1

    .line 251
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 262
    :cond_1
    if-nez v2, :cond_2

    .line 263
    iget-object v4, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-interface {v0, v4, v3, p1}, Lflipboard/service/RemoteWatchedFile$Observer;->a(Ljava/lang/String;[BZ)V

    move v0, v2

    goto :goto_1

    .line 265
    :cond_2
    array-length v4, v3

    new-array v4, v4, [B

    .line 266
    const/4 v5, 0x0

    const/4 v6, 0x0

    array-length v7, v3

    invoke-static {v3, v5, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 267
    iget-object v5, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-interface {v0, v5, v4, p1}, Lflipboard/service/RemoteWatchedFile$Observer;->a(Ljava/lang/String;[BZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 269
    goto :goto_1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    invoke-virtual {p0, v0}, Lflipboard/service/RemoteWatchedFile;->a(Ljava/io/IOException;)V

    .line 273
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lflipboard/service/RemoteWatchedFile;->d()V

    .line 104
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/service/RemoteWatchedFile;->c(Z)V

    .line 105
    return-void
.end method

.method public final a(Lflipboard/service/RemoteWatchedFile$Observer;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method final a(Ljava/io/IOException;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 298
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Removing local copy of remote %s: exception=%E"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    invoke-virtual {p0, v4}, Lflipboard/service/RemoteWatchedFile;->b(Z)V

    .line 300
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 301
    const-wide/16 v0, 0x7d0

    invoke-direct {p0, v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(J)V

    .line 302
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 222
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 223
    iget-boolean v0, p0, Lflipboard/service/RemoteWatchedFile;->e:Z

    if-eqz v0, :cond_0

    const-wide/32 v0, 0xea60

    :goto_0
    invoke-direct {p0, v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(J)V

    .line 224
    return-void

    .line 223
    :cond_0
    const-wide/16 v0, 0x1388

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 212
    if-nez p1, :cond_0

    .line 213
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 217
    invoke-direct {p0, v2}, Lflipboard/service/RemoteWatchedFile;->d(Z)V

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x4e20

    .line 129
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 130
    :goto_0
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    iget-boolean v2, p0, Lflipboard/service/RemoteWatchedFile;->e:Z

    if-nez v2, :cond_0

    .line 131
    sub-long v0, v4, v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 134
    :cond_0
    monitor-exit p0

    return-void

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile$Observer;

    .line 228
    invoke-interface {v0, p1}, Lflipboard/service/RemoteWatchedFile$Observer;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 230
    :cond_0
    const-wide/32 v0, 0xea60

    invoke-direct {p0, v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(J)V

    .line 231
    return-void
.end method

.method final declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/RemoteWatchedFile;->e:Z

    if-eq v0, p1, :cond_0

    .line 147
    iput-boolean p1, p0, Lflipboard/service/RemoteWatchedFile;->e:Z

    .line 148
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    :cond_0
    monitor-exit p0

    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Z)V
    .locals 14

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 158
    sget-object v8, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 159
    iget-object v9, v8, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 160
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v10

    .line 161
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lflipboard/service/RemoteWatchedFile;->g:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 163
    :goto_0
    iget-object v3, v8, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    if-eqz v3, :cond_7

    iget-object v3, v8, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget v3, v3, Lflipboard/model/ConfigSetting;->RefetchSectionsAndConfigJSONBackgroundDuration:I

    int-to-long v4, v3

    :goto_1
    cmp-long v3, v4, v6

    if-gtz v3, :cond_4

    const-wide/32 v4, 0x36ee80

    .line 164
    :goto_2
    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    const-wide/32 v6, 0x5265c00

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 167
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    sub-long/2addr v6, v12

    cmp-long v3, v6, v4

    if-ltz v3, :cond_5

    move v3, v1

    .line 169
    :goto_3
    if-eqz v10, :cond_6

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lflipboard/service/RemoteWatchedFile;->e:Z

    if-eqz v0, :cond_1

    if-eqz v3, :cond_6

    .line 170
    :cond_1
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    iget-boolean v3, p0, Lflipboard/service/RemoteWatchedFile;->f:Z

    iget-boolean v4, p0, Lflipboard/service/RemoteWatchedFile;->g:Z

    new-instance v5, Lflipboard/service/Flap$StaticFileRequest;

    invoke-direct {v5, v8, v9, v4}, Lflipboard/service/Flap$StaticFileRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Z)V

    iput-object v0, v5, Lflipboard/service/Flap$StaticFileRequest;->b:Ljava/lang/String;

    iput-object v1, v5, Lflipboard/service/Flap$StaticFileRequest;->c:Ljava/io/File;

    iput-object p0, v5, Lflipboard/service/Flap$StaticFileRequest;->a:Lflipboard/service/Flap$StaticFileObserver;

    iput-boolean v3, v5, Lflipboard/service/Flap$StaticFileRequest;->d:Z

    invoke-virtual {v5}, Lflipboard/service/Flap$StaticFileRequest;->c()V

    .line 177
    :cond_2
    :goto_4
    iput-boolean v2, p0, Lflipboard/service/RemoteWatchedFile;->g:Z

    .line 178
    return-void

    :cond_3
    move v0, v2

    .line 161
    goto :goto_0

    .line 163
    :cond_4
    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    goto :goto_2

    :cond_5
    move v3, v2

    .line 167
    goto :goto_3

    .line 172
    :cond_6
    if-eqz p1, :cond_2

    .line 173
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v3, v0, v2

    iget-boolean v3, p0, Lflipboard/service/RemoteWatchedFile;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    .line 174
    invoke-direct {p0, v2}, Lflipboard/service/RemoteWatchedFile;->d(Z)V

    goto :goto_4

    :cond_7
    move-wide v4, v6

    goto :goto_1
.end method

.method public final c()[B
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 184
    .line 185
    :try_start_0
    iget-object v0, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reading from downloaded version for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 187
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v2, v0

    .line 192
    :goto_0
    if-eqz v2, :cond_1

    .line 193
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v0

    new-array v0, v0, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :try_start_1
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3, v0}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 197
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 206
    :goto_1
    return-object v0

    .line 188
    :cond_0
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->h:Ljava/util/Map;

    iget-object v2, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 189
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reading bundled version for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    sget-object v0, Lflipboard/service/RemoteWatchedFile;->h:Ljava/util/Map;

    iget-object v3, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 205
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    const-string v2, "failed to load %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 206
    goto :goto_1

    .line 201
    :cond_1
    :try_start_3
    sget-object v0, Lflipboard/service/RemoteWatchedFile;->a:Lflipboard/util/Log;

    const-string v2, "failed to load %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v0, v1

    .line 202
    goto :goto_1

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method
