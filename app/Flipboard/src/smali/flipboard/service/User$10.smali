.class Lflipboard/service/User$10;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/UserState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/objs/UserState;

.field final synthetic b:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Lflipboard/objs/UserState;)V
    .locals 0

    .prologue
    .line 783
    iput-object p1, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$10;->a:Lflipboard/objs/UserState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 783
    check-cast p1, Lflipboard/objs/UserState;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sync_social_follow_on_launch"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-boolean v0, p1, Lflipboard/objs/UserState;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/User$10;->a:Lflipboard/objs/UserState;

    invoke-static {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/User;Lflipboard/objs/UserState;)Lflipboard/objs/UserState;

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v1, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    aput-object v1, v0, v2

    iget-object v1, p1, Lflipboard/objs/UserState;->l:Ljava/lang/String;

    aput-object v1, v0, v3

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, p1, Lflipboard/objs/UserState;->l:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->k(Lflipboard/service/User;)V

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->f:Lflipboard/service/User$Message;

    invoke-static {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User;Lflipboard/service/User$Message;)V

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_2

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget v2, v0, Lflipboard/service/Section;->g:I

    if-eq v2, v1, :cond_0

    iput v1, v0, Lflipboard/service/Section;->g:I

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    new-instance v1, Lflipboard/service/User$10$1;

    invoke-direct {v1, p0}, Lflipboard/service/User$10$1;-><init>(Lflipboard/service/User$10;)V

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/Flap$TypedResultObserver;)V

    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 825
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 826
    iget-object v0, p0, Lflipboard/service/User$10;->b:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->e:Lflipboard/service/User$Message;

    invoke-static {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User;Lflipboard/service/User$Message;)V

    .line 827
    return-void
.end method
