.class public Lflipboard/service/FlipboardManager;
.super Lflipboard/service/Flap;
.source "FlipboardManager.java"


# static fields
.field public static final av:Ljava/util/concurrent/Executor;

.field private static final bb:Ljava/lang/String;

.field private static final bc:Ljava/lang/String;

.field private static final bd:Ljava/lang/String;

.field private static final be:Ljava/lang/String;

.field public static final l:Lflipboard/util/Log;

.field public static final m:Lflipboard/util/Log;

.field public static final n:Z

.field public static final o:Z

.field public static final p:Z

.field public static final q:Z

.field public static final r:Z

.field public static final s:Z

.field public static final t:Z

.field public static u:Lflipboard/service/FlipboardManager;


# instance fields
.field public A:Landroid/graphics/Typeface;

.field public B:Landroid/graphics/Typeface;

.field public C:Landroid/graphics/Typeface;

.field public D:Landroid/graphics/Typeface;

.field public final E:Ljava/util/Timer;

.field public final F:Landroid/content/SharedPreferences;

.field public G:Landroid/content/Context;

.field public H:Ljava/lang/String;

.field public I:Ljava/lang/String;

.field J:I

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Lflipboard/service/User;

.field public N:Lflipboard/io/DownloadManager;

.field public O:Lflipboard/service/PushServiceManager;

.field public P:Landroid/os/Handler;

.field public Q:Ljava/io/File;

.field public R:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation
.end field

.field public S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation
.end field

.field T:Lflipboard/model/ConfigSetting;

.field volatile U:Lflipboard/json/FLObject;

.field public V:Lflipboard/objs/ConfigPopularSearches;

.field public W:Landroid/os/Bundle;

.field public X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field public final Y:Lflipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable$Proxy",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public Z:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aA:Z

.field public aB:I

.field aC:Lflipboard/activities/FlipboardActivity;

.field public aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field private aE:Landroid/os/Handler;

.field private aF:Ljava/lang/String;

.field private aG:Lflipboard/io/NetworkManager;

.field private aH:Lflipboard/io/UsageManager;

.field private aI:Lflipboard/io/BitmapManager;

.field private aJ:Lflipboard/service/HintManager;

.field private aK:Ljava/lang/Thread;

.field private aL:Ljava/io/File;

.field private aM:Landroid/database/sqlite/SQLiteDatabase;

.field private aN:J

.field private aO:Z

.field private aP:J

.field private aQ:Z

.field private aR:I

.field private aS:Lflipboard/service/ContentDrawerHandler;

.field private final aT:Lflipboard/service/ActivityFetcher;

.field private aU:Lflipboard/gui/FLToast;

.field private aV:Lflipboard/service/audio/FLAudioManager;

.field private aW:Z

.field private aX:Z

.field private aY:Ljava/util/TimerTask;

.field private aZ:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public aa:Lnet/hockeyapp/android/CrashManagerListener;

.field public final ab:Lflipboard/objs/CrashInfo;

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Landroid/os/Bundle;

.field public aj:Lflipboard/objs/ConfigEdition;

.field public ak:Landroid/location/Location;

.field al:Z

.field public am:Z

.field public an:I

.field ao:I

.field ap:I

.field aq:I

.field ar:I

.field as:I

.field at:I

.field public au:Ljava/lang/String;

.field public aw:Lflipboard/util/TouchInfo;

.field final ax:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/service/RemoteWatchedFile;",
            ">;"
        }
    .end annotation
.end field

.field public ay:Lflipboard/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Callback",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public az:Z

.field private ba:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public final v:Landroid/graphics/Typeface;

.field public final w:Landroid/graphics/Typeface;

.field public final x:Landroid/graphics/Typeface;

.field public y:Landroid/graphics/Typeface;

.field public z:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 165
    const-string v0, "service"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    .line 166
    const-string v0, "location"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->m:Lflipboard/util/Log;

    .line 189
    sput-boolean v2, Lflipboard/service/FlipboardManager;->n:Z

    .line 190
    const-string v0, "release"

    const-string v3, "china"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    .line 191
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "Barnes"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lflipboard/service/FlipboardManager;->p:Z

    .line 192
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "Kindle"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "Amazon"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    .line 193
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "Ericsson"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lflipboard/service/FlipboardManager;->r:Z

    .line 194
    sget-boolean v0, Lflipboard/service/FlipboardManager;->p:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    sput-boolean v0, Lflipboard/service/FlipboardManager;->s:Z

    .line 196
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lflipboard/service/FlipboardManager;->p:Z

    if-nez v0, :cond_3

    :goto_2
    sput-boolean v2, Lflipboard/service/FlipboardManager;->t:Z

    .line 483
    invoke-static {}, Lflipboard/util/AndroidUtil;->h()Ljava/util/concurrent/Executor;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->av:Ljava/util/concurrent/Executor;

    .line 3225
    const-string v0, "AppUpdateAlertTitle"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->bb:Ljava/lang/String;

    .line 3227
    const-string v0, "AppUpdateAlertMessage"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->bc:Ljava/lang/String;

    .line 3229
    const-string v0, "AppUpdateRequiredAlertTitle"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->bd:Ljava/lang/String;

    .line 3231
    const-string v0, "AppUpdateRequiredAlertMessage"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/service/FlipboardManager;->be:Ljava/lang/String;

    return-void

    :cond_1
    move v0, v1

    .line 192
    goto :goto_0

    :cond_2
    move v0, v1

    .line 194
    goto :goto_1

    :cond_3
    move v2, v1

    .line 196
    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 492
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lflipboard/io/PersistentCookieStore;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "cookies.json"

    invoke-direct {v1, v2, v3}, Lflipboard/io/PersistentCookieStore;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lflipboard/service/Flap;-><init>(Landroid/content/Context;Lorg/apache/http/client/CookieStore;)V

    .line 323
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->S:Ljava/util/List;

    .line 344
    new-instance v0, Lflipboard/objs/CrashInfo;

    invoke-direct {v0}, Lflipboard/objs/CrashInfo;-><init>()V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->ab:Lflipboard/objs/CrashInfo;

    .line 346
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->ac:Z

    .line 350
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->aO:Z

    .line 352
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/service/FlipboardManager;->aP:J

    .line 424
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->ag:Z

    .line 438
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/service/FlipboardManager;->aR:I

    .line 1571
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    .line 3138
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aZ:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 3139
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->ba:Ljava/util/Map;

    .line 494
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v0, :cond_1

    .line 495
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "More than one MainApp instance!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 497
    :cond_1
    sput-object p0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 502
    const-string v0, "flipboard_settings"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 504
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lflipboard/abtest/Experiments;->a(Landroid/content/SharedPreferences;)V

    .line 505
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "should_show_data_use_dialog"

    iget-boolean v2, p0, Lflipboard/service/FlipboardManager;->ac:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->ac:Z

    .line 507
    new-instance v0, Lflipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lflipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    .line 509
    invoke-static {}, Lflipboard/service/FlipboardManager;->S()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->ah:Z

    .line 515
    new-instance v0, Lflipboard/service/FlipboardManager$1;

    const-string v1, "shared-timer"

    invoke-direct {v0, p0, v1}, Lflipboard/service/FlipboardManager$1;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    .line 553
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "country_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 554
    if-eqz v0, :cond_2

    .line 555
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->j:Ljava/lang/String;

    .line 561
    :cond_2
    new-instance v0, Lflipboard/service/FlipboardManager$2;

    const-string v1, "background-tasks"

    invoke-direct {v0, p0, v1}, Lflipboard/service/FlipboardManager$2;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V

    .line 576
    invoke-virtual {v0}, Lflipboard/service/FlipboardManager$2;->start()V

    .line 578
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    .line 579
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aL:Ljava/io/File;

    .line 580
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->aL:Ljava/io/File;

    const-string v2, "shared"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->Q:Ljava/io/File;

    .line 581
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Q:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 582
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Q:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 588
    :cond_3
    invoke-static {p1}, Lflipboard/util/AndroidUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->aA:Z

    .line 589
    new-instance v0, Lflipboard/io/NetworkManager;

    invoke-direct {v0, p1}, Lflipboard/io/NetworkManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aG:Lflipboard/io/NetworkManager;

    .line 590
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aG:Lflipboard/io/NetworkManager;

    invoke-static {}, Lflipboard/io/NetworkManager;->f()V

    .line 591
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aG:Lflipboard/io/NetworkManager;

    new-instance v1, Lflipboard/service/FlipboardManager$3;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$3;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->a(Lflipboard/util/Observer;)V

    .line 607
    new-instance v0, Lflipboard/io/DownloadManager;

    invoke-direct {v0, p1}, Lflipboard/io/DownloadManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->N:Lflipboard/io/DownloadManager;

    .line 608
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->N:Lflipboard/io/DownloadManager;

    invoke-virtual {v0}, Lflipboard/io/DownloadManager;->b()V

    .line 610
    new-instance v0, Lflipboard/io/BitmapManager;

    invoke-direct {v0, p1}, Lflipboard/io/BitmapManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aI:Lflipboard/io/BitmapManager;

    .line 611
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aI:Lflipboard/io/BitmapManager;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager;->a()V

    .line 613
    invoke-static {p1}, Lflipboard/service/PushServiceManager;->a(Landroid/content/Context;)Lflipboard/service/PushServiceManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->O:Lflipboard/service/PushServiceManager;

    .line 615
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aK:Ljava/lang/Thread;

    .line 616
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->P:Landroid/os/Handler;

    .line 619
    const/4 v1, 0x0

    .line 625
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aL:Ljava/io/File;

    const-string v3, "flip.props"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 626
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 629
    :try_start_0
    new-instance v3, Ljava/io/BufferedInputStream;

    const-string v0, "flip.props"

    new-instance v4, Ljava/io/FileInputStream;

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lflipboard/service/FlipboardManager;->aL:Ljava/io/File;

    invoke-direct {v5, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v0, 0x100

    invoke-direct {v3, v4, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 631
    :try_start_1
    new-instance v4, Ljava/util/Properties;

    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    .line 632
    invoke-virtual {v4, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 633
    const-string v0, "uid"

    invoke-virtual {v4, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 634
    :try_start_2
    const-string v1, "udid"

    invoke-virtual {v4, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 635
    const-string v1, "tuuid"

    invoke-virtual {v4, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 639
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 641
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v1, v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 646
    :goto_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_4

    .line 647
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "failed to delete %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 651
    :cond_4
    :goto_3
    if-nez v0, :cond_6

    .line 653
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->D()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 654
    const-string v0, "uid"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 655
    const-string v2, "udid"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 656
    const-string v2, "tuuid"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    .line 659
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    const-string v3, "41e29575c7361b2924f701502ca6d932b45b9e51"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 660
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 661
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "udid"

    iget-object v3, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 662
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "invalid udid detected, resetting to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 664
    :cond_5
    if-eqz v0, :cond_6

    .line 665
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 668
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "do_first_launch"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 669
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 675
    :cond_6
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 676
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 682
    :cond_7
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 683
    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lflipboard/service/FlipboardManager;->J:I

    .line 685
    if-nez v0, :cond_f

    .line 686
    const-string v0, "0"

    move-object v6, v0

    .line 690
    :goto_4
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "sync_sstream_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 691
    if-nez v0, :cond_8

    const-string v0, "0"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 692
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sync_sstream_account"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 693
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-static {v6, v0, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    :cond_8
    new-instance v0, Lflipboard/io/UsageManager;

    invoke-direct {v0, p1}, Lflipboard/io/UsageManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aH:Lflipboard/io/UsageManager;

    .line 700
    new-instance v7, Lflipboard/usage/UsageManagerV2;

    invoke-direct {v7, p1}, Lflipboard/usage/UsageManagerV2;-><init>(Landroid/content/Context;)V

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    new-instance v1, Lflipboard/usage/UsageManagerV2$2;

    invoke-direct {v1, v7}, Lflipboard/usage/UsageManagerV2$2;-><init>(Lflipboard/usage/UsageManagerV2;)V

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->c(Lflipboard/util/Observer;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const-wide/32 v2, 0x5265c00

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    int-to-long v4, v1

    const-wide/32 v8, 0x36ee80

    mul-long/2addr v4, v8

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v8, 0xea60

    mul-long/2addr v0, v8

    add-long/2addr v0, v4

    const-wide/32 v4, 0xea60

    add-long/2addr v0, v4

    sub-long/2addr v2, v0

    new-instance v0, Ljava/util/Timer;

    const-string v1, "usage"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    new-instance v1, Lflipboard/usage/UsageManagerV2$3;

    invoke-direct {v1, v7}, Lflipboard/usage/UsageManagerV2$3;-><init>(Lflipboard/usage/UsageManagerV2;)V

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    iget-object v0, v7, Lflipboard/usage/UsageManagerV2;->p:Lflipboard/usage/UsageManagerV2$Uploader;

    invoke-virtual {v0}, Lflipboard/usage/UsageManagerV2$Uploader;->start()V

    .line 703
    const-string v0, "config.json"

    new-instance v1, Lflipboard/service/FlipboardManager$27;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$27;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p0, v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    .line 709
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 710
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->SystemFontLanguages:Ljava/util/List;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->SystemFontLanguages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    .line 711
    :goto_5
    if-eqz v0, :cond_e

    .line 712
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    .line 717
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->z:Landroid/graphics/Typeface;

    .line 718
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->A:Landroid/graphics/Typeface;

    .line 720
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_c

    .line 721
    const-string v0, "sans-serif-medium"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    .line 726
    :goto_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_d

    .line 727
    const-string v0, "sans-serif-condensed"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->x:Landroid/graphics/Typeface;

    .line 733
    :goto_7
    new-instance v0, Lflipboard/service/FlipboardManager$4;

    invoke-direct {v0, p0}, Lflipboard/service/FlipboardManager$4;-><init>(Lflipboard/service/FlipboardManager;)V

    .line 748
    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/os/AsyncTask;)V

    .line 775
    :goto_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    sput-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    .line 778
    new-instance v0, Lflipboard/service/HintManager;

    invoke-direct {v0}, Lflipboard/service/HintManager;-><init>()V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aJ:Lflipboard/service/HintManager;

    .line 780
    const-string v0, "services.json"

    new-instance v1, Lflipboard/service/FlipboardManager$28;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$28;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p0, v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    .line 781
    const-string v0, "dynamicStrings.json"

    new-instance v1, Lflipboard/service/FlipboardManager$29;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$29;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p0, v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    .line 782
    const-string v0, "hints.json"

    new-instance v1, Lflipboard/service/FlipboardManager$31;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$31;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p0, v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    .line 787
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v1, Lflipboard/service/FlipboardManager$6;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$6;-><init>(Lflipboard/service/FlipboardManager;)V

    const-wide/32 v2, 0xea60

    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 794
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "track_location"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 795
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    new-instance v5, Lflipboard/service/FlipboardManager$42;

    invoke-direct {v5, p0, v0}, Lflipboard/service/FlipboardManager$42;-><init>(Lflipboard/service/FlipboardManager;Landroid/location/LocationManager;)V

    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/location/Location;Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->ak:Landroid/location/Location;

    .line 799
    :cond_9
    new-instance v0, Lflipboard/service/User;

    invoke-direct {v0, v6}, Lflipboard/service/User;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    .line 801
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aE:Landroid/os/Handler;

    new-instance v1, Lflipboard/service/FlipboardManager$7;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$7;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 810
    iget-object v6, p0, Lflipboard/service/FlipboardManager;->aH:Lflipboard/io/UsageManager;

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    new-instance v1, Lflipboard/io/UsageManager$1;

    invoke-direct {v1, v6}, Lflipboard/io/UsageManager$1;-><init>(Lflipboard/io/UsageManager;)V

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->c(Lflipboard/util/Observer;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const-wide/32 v2, 0x5265c00

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    int-to-long v4, v1

    const-wide/32 v8, 0x36ee80

    mul-long/2addr v4, v8

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v8, 0xea60

    mul-long/2addr v0, v8

    add-long/2addr v0, v4

    const-wide/32 v4, 0xea60

    add-long/2addr v0, v4

    sub-long/2addr v2, v0

    new-instance v0, Ljava/util/Timer;

    const-string v1, "usage"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    new-instance v1, Lflipboard/io/UsageManager$2;

    invoke-direct {v1, v6}, Lflipboard/io/UsageManager$2;-><init>(Lflipboard/io/UsageManager;)V

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    iget-object v0, v6, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0}, Lflipboard/io/UsageManager$Uploader;->start()V

    .line 812
    new-instance v0, Lflipboard/service/ActivityFetcher;

    invoke-direct {v0, p1}, Lflipboard/service/ActivityFetcher;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aT:Lflipboard/service/ActivityFetcher;

    .line 814
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    new-instance v1, Lflipboard/service/FlipboardManager$8;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$8;-><init>(Lflipboard/service/FlipboardManager;)V

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 823
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 824
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 825
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 827
    new-instance v1, Lflipboard/service/FlipboardManager$9;

    invoke-direct {v1, p0}, Lflipboard/service/FlipboardManager$9;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 844
    return-void

    .line 588
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 639
    :catchall_0
    move-exception v0

    :goto_9
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 642
    :catch_0
    move-exception v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    .line 644
    :goto_a
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Failed to load props file: %-E"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 710
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 723
    :cond_c
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    goto/16 :goto_6

    .line 729
    :cond_d
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->x:Landroid/graphics/Typeface;

    goto/16 :goto_7

    .line 751
    :cond_e
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 753
    const-string v1, "fonts/FaktFlipboard-Normal.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    .line 754
    const-string v1, "fonts/FaktFlipboard-Medium.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    .line 755
    const-string v1, "fonts/FaktFlipboard-SemiCondensed-Medium.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FlipboardManager;->x:Landroid/graphics/Typeface;

    .line 758
    new-instance v1, Lflipboard/service/FlipboardManager$5;

    invoke-direct {v1, p0, v0}, Lflipboard/service/FlipboardManager$5;-><init>(Lflipboard/service/FlipboardManager;Landroid/content/res/AssetManager;)V

    .line 770
    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/os/AsyncTask;)V

    goto/16 :goto_8

    .line 642
    :catch_1
    move-exception v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_a

    .line 639
    :catchall_1
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_9

    :cond_f
    move-object v6, v0

    goto/16 :goto_4

    :cond_10
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public static E()V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3133
    return-void
.end method

.method public static G()V
    .locals 1

    .prologue
    .line 4106
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 4107
    return-void
.end method

.method public static I()Z
    .locals 1

    .prologue
    .line 4206
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    return v0
.end method

.method public static J()V
    .locals 0

    .prologue
    .line 4221
    return-void
.end method

.method public static L()Z
    .locals 1

    .prologue
    .line 4285
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->MagazineEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private M()V
    .locals 3

    .prologue
    .line 2034
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->a:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2035
    return-void
.end method

.method private N()V
    .locals 4

    .prologue
    .line 2156
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v0, :cond_0

    .line 2157
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 2158
    if-eqz v0, :cond_0

    .line 2159
    iget-object v1, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->K:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/service/Account$Meta;->d:Ljava/lang/String;

    .line 2160
    iget-object v1, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->L:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/service/Account$Meta;->e:Ljava/lang/String;

    .line 2161
    invoke-virtual {v0}, Lflipboard/service/Account;->l()V

    .line 2162
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    const-string v2, "flipboard"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2165
    :cond_0
    return-void
.end method

.method private declared-synchronized O()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 2789
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aM:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 2792
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aM:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2854
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2805
    :cond_0
    :try_start_1
    new-instance v0, Lflipboard/service/FlipboardManager$33;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const-string v2, "users-v6.db"

    invoke-direct {v0, p0, v1, v2}, Lflipboard/service/FlipboardManager$33;-><init>(Lflipboard/service/FlipboardManager;Landroid/content/Context;Ljava/lang/String;)V

    .line 2853
    invoke-virtual {v0}, Lflipboard/service/FlipboardManager$33;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aM:Landroid/database/sqlite/SQLiteDatabase;

    .line 2854
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aM:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2789
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private P()V
    .locals 1

    .prologue
    .line 2892
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aY:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 2893
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aY:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 2894
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aY:Ljava/util/TimerTask;

    .line 2896
    :cond_0
    return-void
.end method

.method private declared-synchronized Q()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2933
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lflipboard/service/FlipboardManager;->aX:Z

    if-nez v1, :cond_0

    .line 2934
    sget-object v1, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    .line 2935
    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/service/FlipboardManager;->aX:Z

    .line 2936
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->aA:Z

    .line 2937
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->aA:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v0, :cond_0

    .line 2938
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2941
    :cond_0
    monitor-exit p0

    return-void

    .line 2936
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2933
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized R()V
    .locals 1

    .prologue
    .line 2946
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->aX:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aY:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 2947
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    .line 2948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->aX:Z

    .line 2949
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aY:Ljava/util/TimerTask;

    .line 2951
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->h()V

    .line 2954
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_0

    .line 2955
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2958
    :cond_0
    monitor-exit p0

    return-void

    .line 2946
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static S()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 4289
    const/4 v2, 0x0

    .line 4290
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 4305
    :goto_0
    return v0

    .line 4293
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 4294
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 4295
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 4296
    const/16 v4, 0x80

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 4297
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 4298
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 4299
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v4, "flipboard.internal"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 4301
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->aE:Landroid/os/Handler;

    return-object p1
.end method

.method public static a(Landroid/app/Activity;)Lcom/amazon/motiongestures/GestureManager;
    .locals 2

    .prologue
    .line 370
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.amazon.software.motiongestures"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->a()Lcom/amazon/motiongestures/GestureManager;

    move-result-object v0

    .line 373
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Lflipboard/json/FLObject;)Lflipboard/json/FLObject;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->U:Lflipboard/json/FLObject;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Lflipboard/model/ConfigSetting;)Lflipboard/model/ConfigSetting;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Lflipboard/objs/ConfigPopularSearches;)Lflipboard/objs/ConfigPopularSearches;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->V:Lflipboard/objs/ConfigPopularSearches;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    return-object v0
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->K:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->S:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Ljava/util/concurrent/ConcurrentHashMap;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    return-object p1
.end method

.method public static a(Lflipboard/service/Section;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Section;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3148
    const/4 v0, 0x0

    .line 3150
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 3151
    iget-boolean v3, v0, Lflipboard/objs/FeedItem;->be:Z

    if-nez v3, :cond_2

    .line 3153
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, p0, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 3154
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 3156
    goto :goto_0

    .line 3158
    :cond_0
    if-eqz v1, :cond_1

    .line 3159
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 3161
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(J)Z
    .locals 3

    .prologue
    .line 3432
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->AppMinimumVersion:Ljava/lang/String;

    .line 3433
    if-eqz v0, :cond_0

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->i(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager;Z)Z
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, Lflipboard/service/FlipboardManager;->aA:Z

    return p1
.end method

.method static synthetic b(Lflipboard/service/FlipboardManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->L:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lflipboard/service/FlipboardManager;)V
    .locals 4

    .prologue
    .line 163
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lflipboard/service/RemoteWatchedFile;->c(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    return-void
.end method

.method static synthetic b(Lflipboard/service/FlipboardManager;Z)Z
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, Lflipboard/service/FlipboardManager;->az:Z

    return p1
.end method

.method static synthetic c(Lflipboard/service/FlipboardManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->aF:Ljava/lang/String;

    return-object p1
.end method

.method public static c(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_3

    .line 2407
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2408
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2409
    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v4, "googlereader"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v4, "linkedin"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v4, "instagram"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2410
    :cond_0
    sget-object v3, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v0, v3, v4

    goto :goto_0

    .line 2412
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object p0, v1

    .line 2417
    :cond_3
    return-object p0
.end method

.method static synthetic c(Lflipboard/service/FlipboardManager;)Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->aX:Z

    return v0
.end method

.method static synthetic d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    return-object v0
.end method

.method static synthetic e(Lflipboard/service/FlipboardManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lflipboard/service/FlipboardManager;)Lflipboard/gui/FLToast;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aU:Lflipboard/gui/FLToast;

    if-nez v0, :cond_0

    new-instance v0, Lflipboard/gui/FLToast;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-direct {v0, v1}, Lflipboard/gui/FLToast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aU:Lflipboard/gui/FLToast;

    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aU:Lflipboard/gui/FLToast;

    return-object v0
.end method

.method public static g()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method static synthetic g(Lflipboard/service/FlipboardManager;)Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->al:Z

    return v0
.end method

.method static synthetic h(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->N()V

    return-void
.end method

.method public static h(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 4156
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4163
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unwanted.checkUIThread_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    const-string v2, "_"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 4164
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Only the UI thread is allowed to do that: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4167
    :cond_0
    return-void
.end method

.method private static i(Ljava/lang/String;)J
    .locals 10

    .prologue
    .line 3481
    const-string v0, "[^0-9.].*$"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3482
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v1, "."

    invoke-direct {v3, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3483
    const-wide/16 v0, 0x0

    .line 3484
    const/4 v2, 0x3

    .line 3485
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_0

    if-ltz v2, :cond_0

    .line 3486
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 3487
    int-to-long v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    int-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 3488
    add-int/lit8 v2, v2, -0x1

    .line 3489
    goto :goto_0

    .line 3490
    :cond_0
    return-wide v0
.end method

.method static synthetic i(Lflipboard/service/FlipboardManager;)Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->aW:Z

    return v0
.end method

.method static synthetic j(Lflipboard/service/FlipboardManager;)Lflipboard/service/HintManager;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aJ:Lflipboard/service/HintManager;

    return-object v0
.end method

.method static synthetic k(Lflipboard/service/FlipboardManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aF:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->R()V

    return-void
.end method

.method static synthetic m(Lflipboard/service/FlipboardManager;)Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/FlipboardManager;->aQ:Z

    return v0
.end method

.method static synthetic n(Lflipboard/service/FlipboardManager;)Lflipboard/objs/UsageEventV2$AppEnterNavFrom;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    return-object v0
.end method

.method static synthetic o(Lflipboard/service/FlipboardManager;)Lflipboard/objs/UsageEventV2$AppEnterNavFrom;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    return-object v0
.end method

.method static synthetic p(Lflipboard/service/FlipboardManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ba:Ljava/util/Map;

    return-object v0
.end method

.method public static p()Z
    .locals 3

    .prologue
    .line 1064
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "do_first_launch_category"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic q(Lflipboard/service/FlipboardManager;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aZ:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic r(Lflipboard/service/FlipboardManager;)Lflipboard/model/ConfigSetting;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    return-object v0
.end method

.method static synthetic s(Lflipboard/service/FlipboardManager;)I
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lflipboard/service/FlipboardManager;->aR:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lflipboard/service/FlipboardManager;->aR:I

    return v0
.end method

.method static synthetic t(Lflipboard/service/FlipboardManager;)I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lflipboard/service/FlipboardManager;->aR:I

    return v0
.end method


# virtual methods
.method public final A()Lflipboard/model/ConfigFirstLaunch;
    .locals 2

    .prologue
    .line 2728
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const v1, 0x7f0d003c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2731
    :goto_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "firstLaunchSections.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2732
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 2733
    const-class v1, Lflipboard/model/ConfigFirstLaunch;

    invoke-static {v0, v1}, Lflipboard/json/JsonSerializationWrapper;->a(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigFirstLaunch;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2736
    :goto_1
    return-object v0

    .line 2728
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const v1, 0x7f0d003b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2735
    :catch_0
    move-exception v0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "firstLaunchSections.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 2736
    const-class v1, Lflipboard/model/ConfigFirstLaunch;

    invoke-static {v0, v1}, Lflipboard/json/JsonSerializationWrapper;->a(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigFirstLaunch;

    goto :goto_1
.end method

.method public final B()Lflipboard/model/FirstLaunchTopicInfo;
    .locals 5

    .prologue
    .line 2743
    :try_start_0
    const-string v0, "first-launch-topic-info.json"

    .line 2744
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 2745
    const-class v1, Lflipboard/model/FirstLaunchTopicInfo;

    invoke-static {v0, v1}, Lflipboard/json/JsonSerializationWrapper;->a(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2748
    :goto_0
    return-object v0

    .line 2747
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "Error parsing %s file"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "first-launch-topic-info.json"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2748
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 3083
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    .line 3084
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->q()Lflipboard/service/Section;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final D()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 3105
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    const-string v1, "uid-prefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final F()Lflipboard/service/ContentDrawerHandler;
    .locals 2

    .prologue
    .line 4015
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aS:Lflipboard/service/ContentDrawerHandler;

    if-nez v0, :cond_0

    .line 4016
    new-instance v0, Lflipboard/service/ContentDrawerHandler;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-direct {v0, v1}, Lflipboard/service/ContentDrawerHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aS:Lflipboard/service/ContentDrawerHandler;

    .line 4018
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aS:Lflipboard/service/ContentDrawerHandler;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4191
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->au:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 4192
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "installed_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->au:Ljava/lang/String;

    .line 4194
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->au:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->h:Landroid/content/Context;

    invoke-static {v0}, Lflipboard/util/SharedAndroidUtil;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4195
    const-string v0, "unknownBundle"

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->au:Ljava/lang/String;

    .line 4197
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->au:Ljava/lang/String;

    return-object v0
.end method

.method public final K()V
    .locals 3

    .prologue
    .line 4281
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "show_widget_logo_hint"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4282
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Flap$Request;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$CreateAccountMessage;",
            "Ljava/lang/Object;",
            ">;)",
            "Lflipboard/service/Flap$Request;"
        }
    .end annotation

    .prologue
    .line 1371
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v1, Lflipboard/service/FlipboardManager$14;

    invoke-direct {v1, p0, p3}, Lflipboard/service/FlipboardManager$14;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V

    new-instance v2, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;

    invoke-direct {v2, p0, v0}, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object v1, v2, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iput-object p1, v2, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->g:Ljava/lang/String;

    if-nez p2, :cond_0

    const-string p2, "flipboard"

    :cond_0
    iput-object p2, v2, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->h:Ljava/lang/String;

    invoke-virtual {v2}, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->c()V

    return-object v2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZLflipboard/util/Observer;)Lflipboard/service/Flap$Request;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$LoginMessage;",
            "Ljava/lang/Object;",
            ">;)",
            "Lflipboard/service/Flap$Request;"
        }
    .end annotation

    .prologue
    .line 1483
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v5, Lflipboard/service/FlipboardManager$17;

    invoke-direct {v5, p0, p4}, Lflipboard/service/FlipboardManager$17;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V

    new-instance v0, Lflipboard/service/Flap$LoginRequest;

    invoke-direct {v0, p0, v1}, Lflipboard/service/Flap$LoginRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$LoginRequest;->login(Ljava/lang/String;Ljava/lang/String;ZZLflipboard/service/Flap$AccountRequestObserver;)Lflipboard/service/Flap$LoginRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;
    .locals 4

    .prologue
    .line 1581
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    monitor-enter v2

    .line 1582
    :try_start_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile;

    .line 1583
    if-nez v0, :cond_3

    .line 1584
    new-instance v0, Lflipboard/service/RemoteWatchedFile;

    invoke-direct {v0, p1}, Lflipboard/service/RemoteWatchedFile;-><init>(Ljava/lang/String;)V

    .line 1585
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 1587
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1588
    if-eqz p2, :cond_1

    .line 1589
    iget-object v0, v1, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Lflipboard/service/RemoteWatchedFile;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v0, v1, Lflipboard/service/RemoteWatchedFile;->e:Z

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v1}, Lflipboard/service/RemoteWatchedFile;->c()[B

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/service/RemoteWatchedFile;->b(Z)V

    iget-object v2, v1, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {p2, v2, v0, v3}, Lflipboard/service/RemoteWatchedFile$Observer;->a(Ljava/lang/String;[BZ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1591
    :cond_1
    :goto_1
    return-object v1

    .line 1587
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1589
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "missing local file for: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v1, Lflipboard/service/RemoteWatchedFile;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lflipboard/service/RemoteWatchedFile$Observer;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v1, v0}, Lflipboard/service/RemoteWatchedFile;->a(Ljava/io/IOException;)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lflipboard/objs/UserInfo;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Section;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lflipboard/objs/UserInfo;",
            "Ljava/lang/String;",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/Section;",
            "Ljava/lang/Object;",
            ">;)",
            "Lflipboard/service/Section;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1635
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p2, v0, v2

    .line 1641
    iget-object v0, p2, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 1642
    iget-object v0, p2, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 1643
    iget-object v5, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v3, v0

    .line 1649
    :goto_0
    if-nez v3, :cond_5

    .line 1650
    iget-object v0, p2, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 1651
    iget-object v6, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v7, v1

    move-object v1, v0

    move v0, v7

    .line 1661
    :goto_1
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v3, Lflipboard/service/Account;

    invoke-direct {v3, v1, v0}, Lflipboard/service/Account;-><init>(Lflipboard/objs/UserService;Z)V

    invoke-virtual {v2, v3}, Lflipboard/service/User;->a(Lflipboard/service/Account;)V

    .line 1663
    if-eqz v1, :cond_2

    .line 1665
    if-eqz v0, :cond_3

    move-object v0, v4

    .line 1672
    :goto_2
    invoke-interface {p4, p0, v0, v4}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1675
    :cond_2
    return-object v4

    .line 1667
    :cond_3
    iget-object v0, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->f:Z

    if-nez v0, :cond_4

    move-object v0, v4

    .line 1668
    goto :goto_2

    .line 1670
    :cond_4
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/service/Section;

    invoke-direct {v2, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/UserService;)V

    invoke-virtual {v0, v2, p3}, Lflipboard/service/User;->a(Lflipboard/service/Section;Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v0, v2

    move-object v1, v3

    goto :goto_1

    :cond_6
    move-object v3, v4

    goto :goto_0

    :cond_7
    move v0, v2

    move-object v1, v4

    goto :goto_1
.end method

.method public final a(IILandroid/content/Context;)V
    .locals 2

    .prologue
    .line 4242
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 4243
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 4244
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 4245
    iget v0, p0, Lflipboard/service/FlipboardManager;->ao:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lflipboard/service/FlipboardManager;->ap:I

    if-eq v0, p2, :cond_0

    .line 4246
    iput p1, p0, Lflipboard/service/FlipboardManager;->ao:I

    .line 4247
    iput p2, p0, Lflipboard/service/FlipboardManager;->ap:I

    .line 4248
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    iget v1, p0, Lflipboard/service/FlipboardManager;->ao:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/service/FlipboardManager;->as:I

    .line 4249
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v0

    iget v1, p0, Lflipboard/service/FlipboardManager;->ap:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/service/FlipboardManager;->at:I

    .line 4258
    :cond_0
    :goto_0
    return-void

    .line 4251
    :cond_1
    iget v0, p0, Lflipboard/service/FlipboardManager;->aq:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lflipboard/service/FlipboardManager;->ar:I

    if-eq v0, p2, :cond_0

    .line 4252
    iput p1, p0, Lflipboard/service/FlipboardManager;->aq:I

    .line 4253
    iput p2, p0, Lflipboard/service/FlipboardManager;->ar:I

    .line 4254
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    iget v1, p0, Lflipboard/service/FlipboardManager;->aq:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/service/FlipboardManager;->as:I

    .line 4255
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v0

    iget v1, p0, Lflipboard/service/FlipboardManager;->ar:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/service/FlipboardManager;->at:I

    goto :goto_0
.end method

.method public final a(ILjava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 1005
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aE:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v0, p2, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1006
    return-void
.end method

.method public final a(JLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 967
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->P:Landroid/os/Handler;

    invoke-virtual {v0, p3, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 968
    return-void
.end method

.method public final a(Lflipboard/activities/FlipboardActivity;)V
    .locals 4

    .prologue
    .line 1739
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    const-string v2, "flipboard"

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1742
    if-eqz p1, :cond_0

    .line 1744
    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 1748
    :cond_0
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->r()V

    .line 1751
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->s()V

    .line 1754
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->m()V

    .line 1757
    new-instance v0, Landroid/content/Intent;

    if-nez p1, :cond_1

    iget-object p1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    :cond_1
    const-class v1, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1758
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1759
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1760
    return-void
.end method

.method public final a(Lflipboard/objs/UserInfo;Lflipboard/util/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/objs/UserInfo;",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$LoginMessage;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1546
    iget v0, p1, Lflipboard/objs/UserInfo;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1547
    new-instance v1, Lflipboard/service/User;

    invoke-direct {v1, v0}, Lflipboard/service/User;-><init>(Ljava/lang/String;)V

    .line 1548
    iget-object v0, p1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    iget-object v2, p1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-virtual {v1, v0, v2}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/util/List;)V

    .line 1550
    invoke-virtual {p0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    .line 1551
    invoke-virtual {v1}, Lflipboard/service/User;->r()V

    .line 1555
    new-instance v0, Lflipboard/service/FlipboardManager$18;

    invoke-direct {v0, p0, p2}, Lflipboard/service/FlipboardManager$18;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V

    invoke-virtual {v1, v0}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    .line 1566
    return-void
.end method

.method public final a(Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2114
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aF:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2115
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v1, "apiClientId is empty. Request api client id"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2116
    new-instance v0, Lflipboard/service/FlipboardManager$25;

    invoke-direct {v0, p0, p1}, Lflipboard/service/FlipboardManager$25;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/Flap$JSONResultObserver;)V

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->c(Lflipboard/service/Flap$JSONResultObserver;)V

    .line 2153
    :goto_0
    return-void

    .line 2132
    :cond_0
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v1, "Request API token from flap"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2133
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->aF:Ljava/lang/String;

    new-instance v2, Lflipboard/service/FlipboardManager$26;

    invoke-direct {v2, p0, p1}, Lflipboard/service/FlipboardManager$26;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/Flap$JSONResultObserver;)V

    new-instance v3, Lflipboard/service/Flap$APITokenRequest;

    invoke-direct {v3, p0, v0}, Lflipboard/service/Flap$APITokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v1, v2}, Lflipboard/service/Flap$APITokenRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$APITokenRequest;

    goto :goto_0
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 3168
    if-eqz p2, :cond_1

    .line 3169
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->be:Z

    if-nez v0, :cond_0

    .line 3170
    invoke-virtual {p1, v2}, Lflipboard/service/Section;->e(Z)V

    .line 3171
    iput-boolean v2, p2, Lflipboard/objs/FeedItem;->be:Z

    .line 3172
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ba:Ljava/util/Map;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3223
    :cond_0
    :goto_0
    return-void

    .line 3177
    :cond_1
    if-eqz p1, :cond_0

    .line 3181
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ba:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3185
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3188
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3193
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aZ:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3195
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    goto :goto_0

    .line 3201
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->ba:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3202
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    new-instance v5, Lflipboard/service/FlipboardManager$38;

    invoke-direct {v5, p0, v0, p1}, Lflipboard/service/FlipboardManager$38;-><init>(Lflipboard/service/FlipboardManager;Ljava/util/Collection;Lflipboard/service/Section;)V

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v6

    if-lez v6, :cond_0

    new-instance v6, Lflipboard/service/Flap$ReadRequest;

    invoke-direct {v6, v1, v2}, Lflipboard/service/Flap$ReadRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v6, v3, v4, v0, v5}, Lflipboard/service/Flap$ReadRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Lflipboard/service/Flap$JSONResultObserver;)V

    goto :goto_0
.end method

.method public final a(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1084
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->b(Lflipboard/util/Observer;)V

    .line 1085
    return-void
.end method

.method public final a(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3076
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_0

    .line 3077
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 3079
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->P:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 951
    return-void
.end method

.method public final a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->P:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 955
    return-void
.end method

.method public final a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V
    .locals 1

    .prologue
    .line 2757
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 2758
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 972
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 973
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    .line 974
    new-instance v0, Lflipboard/service/TrackedRunnable;

    invoke-direct {v0, p2}, Lflipboard/service/TrackedRunnable;-><init>(Ljava/lang/Runnable;)V

    .line 975
    sget-object v1, Lflipboard/service/FlipboardManager;->av:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 976
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v2, Lflipboard/service/FlipboardManager$10;

    invoke-direct {v2, p0, v0, p1}, Lflipboard/service/FlipboardManager$10;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/TrackedRunnable;Ljava/lang/String;)V

    const-wide/16 v4, 0x2710

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1001
    :goto_0
    return-void

    .line 996
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->av:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 999
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aE:Landroid/os/Handler;

    invoke-virtual {v0, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$CreateAccountMessage;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1401
    new-instance v0, Lflipboard/service/FlipboardManager$15;

    invoke-direct {v0, p0, p5}, Lflipboard/service/FlipboardManager$15;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V

    .line 1428
    const-string v1, "facebook"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1430
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/service/Flap$CreateAccountWithFacebookWithTokenRequest;

    invoke-direct {v2, p0, v1}, Lflipboard/service/Flap$CreateAccountWithFacebookWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object v0, v2, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iput-object p2, v2, Lflipboard/service/Flap$CreateAccountWithFacebookWithTokenRequest;->h:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, v2, Lflipboard/service/Flap$CreateAccountWithFacebookWithTokenRequest;->g:Ljava/lang/String;

    if-nez p4, :cond_0

    const-string p4, "flipboard"

    :cond_0
    iput-object p4, v2, Lflipboard/service/Flap$CreateAccountWithFacebookWithTokenRequest;->i:Ljava/lang/String;

    invoke-virtual {v2}, Lflipboard/service/Flap$CreateAccountWithFacebookWithTokenRequest;->c()V

    .line 1436
    :cond_1
    :goto_0
    return-void

    .line 1431
    :cond_2
    const-string v1, "googleplus"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1432
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/service/Flap$CreateAccountWithGoogleWithTokenRequest;

    invoke-direct {v2, p0, v1}, Lflipboard/service/Flap$CreateAccountWithGoogleWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object v0, v2, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iput-object p2, v2, Lflipboard/service/Flap$CreateAccountWithGoogleWithTokenRequest;->g:Ljava/lang/String;

    if-nez p4, :cond_3

    const-string p4, "flipboard"

    :cond_3
    iput-object p4, v2, Lflipboard/service/Flap$CreateAccountWithGoogleWithTokenRequest;->h:Ljava/lang/String;

    invoke-virtual {v2}, Lflipboard/service/Flap$CreateAccountWithGoogleWithTokenRequest;->c()V

    goto :goto_0

    .line 1433
    :cond_4
    const-string v1, "samsung"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1434
    new-instance v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v1, p0, v2}, Lflipboard/service/Flap$CreateAccountWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object v0, v1, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iput-object p2, v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->g:Ljava/lang/String;

    iput-object p1, v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->h:Ljava/lang/String;

    iput-object p3, v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->i:Ljava/lang/String;

    iget-object v0, v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->j:Ljava/lang/String;

    if-nez v0, :cond_5

    const-string v0, "flipboard"

    :goto_1
    iput-object v0, v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->j:Ljava/lang/String;

    invoke-virtual {v1}, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->c()V

    goto :goto_0

    :cond_5
    iget-object v0, v1, Lflipboard/service/Flap$CreateAccountWithTokenRequest;->j:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLflipboard/service/Flap$JSONResultObserver;)V
    .locals 8

    .prologue
    .line 2193
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v0, Lflipboard/service/Flap$ShortenSectionRequest;

    invoke-direct {v0, p0, v1}, Lflipboard/service/Flap$ShortenSectionRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lflipboard/service/Flap$ShortenSectionRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ShortenSectionRequest;

    .line 2194
    return-void
.end method

.method public final a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V
    .locals 3

    .prologue
    .line 2762
    if-nez p2, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2763
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Database table "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is being accessed on the main thread when it shouldn\'t be"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2767
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2771
    :cond_0
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->O()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2772
    iput-object v1, p3, Lflipboard/service/DatabaseHandler;->g:Landroid/database/sqlite/SQLiteDatabase;

    .line 2773
    iput-object p1, p3, Lflipboard/service/DatabaseHandler;->f:Ljava/lang/String;

    .line 2774
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2776
    :try_start_0
    invoke-virtual {p3}, Lflipboard/service/DatabaseHandler;->a()V

    .line 2777
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2779
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2780
    invoke-virtual {p3}, Lflipboard/service/DatabaseHandler;->b()V

    .line 2781
    return-void

    .line 2779
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2780
    invoke-virtual {p3}, Lflipboard/service/DatabaseHandler;->b()V

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 389
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->allowMetaDataRequestForSections:Z

    if-nez v0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 393
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 394
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v1

    if-nez v1, :cond_2

    .line 395
    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    .line 396
    :goto_2
    invoke-virtual {v0}, Lflipboard/service/Section;->y()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    if-nez v4, :cond_5

    move v4, v2

    .line 397
    :goto_3
    if-nez v1, :cond_3

    if-eqz v4, :cond_2

    .line 398
    :cond_3
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move v1, v3

    .line 395
    goto :goto_2

    :cond_5
    move v4, v3

    .line 396
    goto :goto_3

    .line 402
    :cond_6
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1, v3, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v1

    .line 404
    iput v3, v1, Lflipboard/service/Flap$UpdateRequest;->k:I

    .line 405
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 406
    invoke-virtual {v1, v0, v7, v7}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_4

    .line 408
    :cond_7
    invoke-virtual {v1}, Lflipboard/service/Flap$UpdateRequest;->c()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1112
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1186
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1119
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1120
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1122
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1123
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->X()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v4, v5}, Lflipboard/objs/FeedItem;->a(J)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1124
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v9, "sectionCover"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_3
    move v1, v2

    .line 1125
    :goto_1
    if-eqz v1, :cond_8

    .line 1127
    iget-object v1, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    if-eqz v1, :cond_7

    move v1, v2

    .line 1128
    :goto_2
    if-eqz v1, :cond_4

    .line 1129
    iget-object v1, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    iget-object v9, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-interface {v3, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    iget-object v1, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1139
    :cond_4
    :goto_3
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1141
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1142
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->X()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v0, v4, v5}, Lflipboard/objs/FeedItem;->a(J)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1143
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1144
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v6

    .line 1124
    goto :goto_1

    :cond_7
    move v1, v6

    .line 1127
    goto :goto_2

    .line 1134
    :cond_8
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1135
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1151
    :cond_9
    iget-object v6, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    new-instance v0, Lflipboard/service/FlipboardManager$11;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/FlipboardManager$11;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/Flap$CommentaryObserver;Ljava/util/Map;J)V

    invoke-virtual {v6, v7, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/util/List;ZLjava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;Z",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1214
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1320
    :cond_0
    :goto_0
    return-void

    .line 1219
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1220
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1222
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1223
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1224
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1227
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v4, v4, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1228
    iget-object v4, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v4, v4, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1229
    iget-object v4, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v4, v4, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1232
    :cond_3
    iget-object v4, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 1233
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1234
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1241
    :cond_4
    new-instance v0, Lflipboard/service/FlipboardManager$12;

    invoke-direct {v0, p0, v1, p4, v2}, Lflipboard/service/FlipboardManager$12;-><init>(Lflipboard/service/FlipboardManager;Ljava/util/Map;Lflipboard/service/Flap$CommentaryObserver;Ljava/util/List;)V

    .line 1315
    if-eqz p2, :cond_5

    .line 1316
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v3, Lflipboard/service/Flap$CommentaryRequest;

    invoke-direct {v3, p0, v1}, Lflipboard/service/Flap$CommentaryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const-string v1, "/v1/social/likes"

    invoke-virtual {v3, v2, v1, p3, v0}, Lflipboard/service/Flap$CommentaryRequest;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;

    goto/16 :goto_0

    .line 1318
    :cond_5
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p0, v1, v2, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;

    goto/16 :goto_0
.end method

.method final a(Z)V
    .locals 3

    .prologue
    .line 2060
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->h:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2061
    return-void
.end method

.method public final a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;ZLflipboard/gui/dialog/FLDialogResponse;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 4118
    const-string v1, "FlipboardManager:tryShowLimitedAccessAlertForService"

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 4122
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, p2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 4124
    iget-boolean v2, v1, Lflipboard/objs/ConfigService;->bO:Z

    if-eqz v2, :cond_3

    iget-boolean v1, v1, Lflipboard/objs/ConfigService;->bx:Z

    if-eqz v1, :cond_3

    .line 4125
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, p2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 4126
    if-eqz v1, :cond_2

    .line 4127
    invoke-virtual {v1}, Lflipboard/service/Account;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4129
    invoke-virtual {v1}, Lflipboard/service/Account;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p3, :cond_3

    .line 4134
    :cond_0
    invoke-static {p2, p3}, Lflipboard/service/Account;->a(Ljava/lang/String;Z)I

    move-result v2

    .line 4135
    invoke-static {p2}, Lflipboard/service/Account;->d(Ljava/lang/String;)I

    move-result v1

    .line 4144
    :goto_0
    if-lez v2, :cond_1

    if-lez v1, :cond_1

    iget-boolean v3, p1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v3, :cond_1

    .line 4145
    invoke-static {v2, v1}, Lflipboard/activities/FlipboardActivity;->a(II)Lflipboard/gui/dialog/FLAlertDialogFragment;

    move-result-object v0

    .line 4146
    iput-object p4, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 4147
    iget-object v1, p1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "limited_access"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 4148
    const/4 v0, 0x1

    .line 4151
    :cond_1
    return v0

    .line 4139
    :cond_2
    invoke-static {p2}, Lflipboard/service/Account;->c(Ljava/lang/String;)I

    move-result v2

    .line 4140
    invoke-static {p2}, Lflipboard/service/Account;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_3
    move v1, v0

    move v2, v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 853
    const-string v0, "medium"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 854
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    .line 883
    :goto_0
    return-object v0

    .line 855
    :cond_0
    const-string v0, "bold"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 856
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->z:Landroid/graphics/Typeface;

    goto :goto_0

    .line 857
    :cond_1
    const-string v0, "light"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 858
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->y:Landroid/graphics/Typeface;

    goto :goto_0

    .line 859
    :cond_2
    const-string v0, "normalSerif"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 860
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->A:Landroid/graphics/Typeface;

    goto :goto_0

    .line 861
    :cond_3
    const-string v0, "normalItalicSerif"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 862
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->C:Landroid/graphics/Typeface;

    goto :goto_0

    .line 863
    :cond_4
    const-string v0, "boldSerif"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 864
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->B:Landroid/graphics/Typeface;

    goto :goto_0

    .line 865
    :cond_5
    const-string v0, "boldItalicSerif"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 867
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->D:Landroid/graphics/Typeface;

    if-nez v0, :cond_6

    .line 868
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 869
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->SystemFontLanguages:Ljava/util/List;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->SystemFontLanguages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    .line 870
    :goto_1
    if-eqz v0, :cond_8

    .line 871
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->D:Landroid/graphics/Typeface;

    .line 877
    :cond_6
    :goto_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->D:Landroid/graphics/Typeface;

    goto :goto_0

    .line 869
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 873
    :cond_8
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 874
    const-string v1, "fonts/TiemposText-SemiboldItalic.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->D:Landroid/graphics/Typeface;

    goto :goto_2

    .line 878
    :cond_9
    const-string v0, "mediumCondensed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 879
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->x:Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 881
    :cond_a
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    goto/16 :goto_0
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 14

    .prologue
    .line 3251
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->i()I

    move-result v2

    .line 3252
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    .line 3253
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 3256
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "rate_time"

    const-wide/16 v6, 0x0

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 3258
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "update_alert_count"

    const/4 v8, 0x0

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 3259
    iget v0, v3, Lflipboard/model/ConfigSetting;->MaxUpdateAlerts:I

    if-lez v0, :cond_2

    iget v0, v3, Lflipboard/model/ConfigSetting;->MaxUpdateAlerts:I

    if-lt v1, v0, :cond_2

    const/4 v0, 0x1

    .line 3261
    :goto_0
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lflipboard/service/FlipboardManager;->i(Ljava/lang/String;)J

    move-result-wide v8

    .line 3264
    if-nez v0, :cond_4

    const-wide/32 v10, 0x5265c00

    sub-long v10, v4, v10

    cmp-long v0, v6, v10

    if-gez v0, :cond_4

    iget v0, v3, Lflipboard/model/ConfigSetting;->MinLaunchesToDisplayUpdate:I

    if-le v2, v0, :cond_4

    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->AppLatestVersion:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->i(Ljava/lang/String;)J

    move-result-wide v10

    cmp-long v0, v10, v8

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    invoke-direct {p0, v8, v9}, Lflipboard/service/FlipboardManager;->a(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3267
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "rate_time"

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "update_alert_count"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3269
    invoke-virtual {p0, p1}, Lflipboard/service/FlipboardManager;->d(Landroid/app/Activity;)V

    .line 3308
    :cond_1
    :goto_2
    return-void

    .line 3259
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 3264
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 3272
    :cond_4
    invoke-static {}, Lflipboard/util/HappyUser;->e()Z

    move-result v8

    .line 3273
    if-eqz v3, :cond_8

    iget-object v0, v3, Lflipboard/model/ConfigSetting;->AppDownloadURL:Ljava/lang/String;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    .line 3274
    :goto_3
    if-eqz v0, :cond_7

    .line 3275
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v9, "rate_state"

    const/4 v10, 0x0

    invoke-interface {v1, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3276
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v10, "rate_launch"

    const/4 v11, 0x0

    invoke-interface {v1, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 3277
    if-eqz v9, :cond_9

    const-string v1, "no"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "yes"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_5
    const/4 v1, 0x1

    .line 3278
    :goto_4
    if-eqz v1, :cond_a

    .line 3280
    const/4 v0, 0x0

    .line 3297
    :cond_6
    :goto_5
    if-nez v0, :cond_7

    if-nez v1, :cond_7

    if-eqz v8, :cond_7

    .line 3299
    const-string v1, "happy user without rate me prompt"

    invoke-static {v1}, Lflipboard/io/UsageEvent;->c(Ljava/lang/String;)V

    .line 3304
    :cond_7
    if-eqz v0, :cond_1

    .line 3305
    invoke-virtual {p0, p1}, Lflipboard/service/FlipboardManager;->c(Landroid/app/Activity;)V

    goto :goto_2

    .line 3273
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 3277
    :cond_9
    const/4 v1, 0x0

    goto :goto_4

    .line 3281
    :cond_a
    const-wide/16 v12, 0x0

    cmp-long v11, v6, v12

    if-nez v11, :cond_b

    .line 3283
    const/4 v0, 0x0

    .line 3284
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "rate_launch"

    sget-object v6, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->i()I

    move-result v6

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "rate_time"

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_5

    .line 3285
    :cond_b
    if-eqz v9, :cond_c

    const-string v11, "later"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    long-to-float v9, v4

    long-to-float v11, v6

    iget v12, v3, Lflipboard/model/ConfigSetting;->MinTimeToDisplayRateMeAfterRateLater:F

    const/high16 v13, 0x447a0000    # 1000.0f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    cmpg-float v9, v9, v11

    if-gez v9, :cond_c

    .line 3287
    const/4 v0, 0x0

    goto :goto_5

    .line 3288
    :cond_c
    long-to-float v4, v4

    long-to-float v5, v6

    iget v6, v3, Lflipboard/model/ConfigSetting;->MinTimeToDisplayRateMe:F

    const/high16 v7, 0x447a0000    # 1000.0f

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_d

    .line 3290
    const/4 v0, 0x0

    goto :goto_5

    .line 3291
    :cond_d
    iget v4, v3, Lflipboard/model/ConfigSetting;->MinLaunchesToDisplayRateMe:I

    add-int/2addr v4, v10

    if-ge v2, v4, :cond_e

    .line 3293
    const/4 v0, 0x0

    goto :goto_5

    .line 3294
    :cond_e
    iget-boolean v2, v3, Lflipboard/model/ConfigSetting;->DisplayRateMeOnlyIfHappyUser:Z

    if-eqz v2, :cond_6

    if-nez v8, :cond_6

    .line 3295
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final declared-synchronized b(Lflipboard/activities/FlipboardActivity;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2879
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    .line 2881
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->P()V

    .line 2882
    iget-boolean v2, p0, Lflipboard/service/FlipboardManager;->aX:Z

    if-nez v2, :cond_0

    .line 2883
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->Q()V

    .line 2885
    :cond_0
    iget-boolean v2, p0, Lflipboard/service/FlipboardManager;->aO:Z

    if-eqz v2, :cond_7

    .line 2886
    const/4 v2, 0x0

    iput-boolean v2, p0, Lflipboard/service/FlipboardManager;->aO:Z

    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->h()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lflipboard/service/FlipboardManager;->aQ:Z

    if-nez v3, :cond_1

    const-string v3, "appResumedFromBackground"

    new-instance v4, Lflipboard/service/FlipboardManager$36;

    invoke-direct {v4, p0, v2}, Lflipboard/service/FlipboardManager$36;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/service/FlipboardManager;->aN:J

    invoke-static {}, Lflipboard/util/HappyUser;->d()V

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2}, Lflipboard/service/User;->e()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v4

    iget-wide v4, v4, Lflipboard/model/ConfigSetting;->MagazineFetchInterval:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iget-wide v6, p0, Lflipboard/service/FlipboardManager;->aP:J

    sub-long v6, v2, v6

    cmp-long v4, v6, v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v4}, Lflipboard/service/User;->r()V

    iput-wide v2, p0, Lflipboard/service/FlipboardManager;->aP:J

    :cond_2
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "sync_social_follow_on_launch"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    :cond_3
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "need_to_fetch_meta_data_for_sections"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_5

    :goto_0
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-boolean v1, v1, Lflipboard/model/ConfigSetting;->allowBackFillForSocialFollow:Z

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "sync_social_follow_on_launch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lflipboard/service/Section;->B()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2879
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v0, v1

    .line 2886
    goto :goto_0

    :cond_6
    :try_start_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v2, "flipboard"

    new-instance v3, Lflipboard/service/FlipboardManager$37;

    invoke-direct {v3, p0}, Lflipboard/service/FlipboardManager$37;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 2888
    :cond_7
    iget v0, p0, Lflipboard/service/FlipboardManager;->aB:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/service/FlipboardManager;->aB:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2889
    monitor-exit p0

    return-void
.end method

.method public final b(Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 2

    .prologue
    .line 2198
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v1, Lflipboard/service/Flap$ReserveUrlRequest;

    invoke-direct {v1, p0, v0}, Lflipboard/service/Flap$ReserveUrlRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v1, p1}, Lflipboard/service/Flap$ReserveUrlRequest;->a(Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ReserveUrlRequest;

    .line 2199
    return-void
.end method

.method public final b(Lflipboard/service/User;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 1949
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-nez v0, :cond_0

    .line 1950
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 1953
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-ne v0, p1, :cond_3

    .line 1954
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 1957
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->O:Lflipboard/service/PushServiceManager;

    invoke-virtual {v0, p1}, Lflipboard/service/PushServiceManager;->a(Lflipboard/service/User;)V

    .line 1958
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->M()V

    .line 2029
    :goto_0
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->D()Landroid/content/SharedPreferences;

    move-result-object v3

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "0"

    iget-object v4, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v4, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "0"

    const-string v4, "uid"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    aput-object v4, v0, v2

    :cond_2
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "uid"

    iget-object v5, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v5, v5, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "udid"

    iget-object v5, p0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "tuuid"

    iget-object v5, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    invoke-static {v0, v4, v7}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "sync_sstream_account"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v4, "uid"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    const-string v2, "udid"

    invoke-interface {v3, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "tuuid"

    invoke-interface {v3, v1, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 2030
    return-void

    .line 1960
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    aput-object v3, v0, v2

    aput-object p1, v0, v1

    .line 1964
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v0, :cond_8

    .line 1965
    iget-object v3, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 1968
    iget-object v0, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v4, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1971
    iput-object v7, p0, Lflipboard/service/FlipboardManager;->K:Ljava/lang/String;

    .line 1972
    iput-object v7, p0, Lflipboard/service/FlipboardManager;->L:Ljava/lang/String;

    .line 1973
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->N()V

    .line 1975
    if-nez v0, :cond_4

    .line 1977
    invoke-virtual {p0, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;)V

    .line 1981
    invoke-virtual {v3}, Lflipboard/service/User;->b()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1982
    iget-object v4, v3, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v5, "User:oneLastSyncThenDelete"

    new-instance v6, Lflipboard/service/User$12;

    invoke-direct {v6, v3}, Lflipboard/service/User$12;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v4, v5, v6}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1994
    :goto_2
    iget-object v4, p0, Lflipboard/service/FlipboardManager;->O:Lflipboard/service/PushServiceManager;

    invoke-virtual {v4, v3}, Lflipboard/service/PushServiceManager;->b(Lflipboard/service/User;)V

    .line 1998
    :cond_4
    :goto_3
    iput-object p1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 1999
    if-nez v0, :cond_5

    .line 2000
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->O:Lflipboard/service/PushServiceManager;

    iget-object v3, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v3}, Lflipboard/service/PushServiceManager;->a(Lflipboard/service/User;)V

    .line 2003
    :cond_5
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->M()V

    .line 2004
    new-instance v0, Lflipboard/service/FlipboardManager$23;

    invoke-direct {v0, p0}, Lflipboard/service/FlipboardManager$23;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p1, v0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    goto/16 :goto_0

    .line 1985
    :cond_6
    const/16 v4, 0x1f4

    new-instance v5, Lflipboard/service/FlipboardManager$22;

    invoke-direct {v5, p0, v3}, Lflipboard/service/FlipboardManager$22;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/User;)V

    invoke-virtual {p0, v4, v5}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    goto :goto_2

    :cond_7
    move v0, v2

    .line 2029
    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public final b(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1088
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->c(Lflipboard/util/Observer;)V

    .line 1089
    return-void
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 958
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 959
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 963
    :goto_0
    return-void

    .line 961
    :cond_0
    invoke-virtual {p0, p1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4041
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4042
    const-string v1, "language_system"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "locale_system"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4043
    const-string v1, "content_guide_language"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "content_guide_locale"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 4047
    :goto_0
    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 4048
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    const-string v2, "contentGuide.json"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    const-string v2, "contentGuide.json"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0}, Lflipboard/service/RemoteWatchedFile;->a()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 4045
    :cond_1
    const-string v1, "content_guide_language"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "content_guide_locale"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 4048
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 1109
    return-void
.end method

.method public final b(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1192
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aT:Lflipboard/service/ActivityFetcher;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v4, v2, Lflipboard/service/ActivityFetcher;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object v0, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    if-nez v1, :cond_3

    const/4 v0, -0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    if-nez v1, :cond_4

    new-instance v0, Lflipboard/objs/CommentaryResult;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {p2, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/Object;)V

    .line 1194
    :goto_2
    return-void

    .line 1192
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    :cond_4
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v5

    invoke-virtual {v2, v1, p2}, Lflipboard/service/ActivityFetcher;->a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    goto :goto_2

    :cond_5
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v5

    invoke-virtual {v2, v1, p2}, Lflipboard/service/ActivityFetcher;->a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    goto :goto_2

    :cond_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    sget-object v3, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v3

    new-instance v4, Lflipboard/service/ActivityFetcher$3;

    invoke-direct {v4, v2, v1, p2}, Lflipboard/service/ActivityFetcher$3;-><init>(Lflipboard/service/ActivityFetcher;Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    new-instance v2, Lflipboard/service/Flap$ActivityRequest;

    invoke-direct {v2, v3, v0}, Lflipboard/service/Flap$ActivityRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v2, v1, v4}, Lflipboard/service/Flap$ActivityRequest;->a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$ActivityRequest;

    goto :goto_2
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 3313
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "rate_launch"

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->i()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "rate_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3314
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "user-notification"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3315
    const-string v1, "reason"

    const-string v2, "rate"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3316
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 3318
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 3319
    const v1, 0x7f0d025f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 3320
    const v1, 0x7f0d025d

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 3321
    const v1, 0x7f0d0260

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 3322
    const v1, 0x7f0d025c

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->e(I)V

    .line 3323
    const v1, 0x7f0d025e

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 3324
    new-instance v1, Lflipboard/service/FlipboardManager$39;

    invoke-direct {v1, p0, p1}, Lflipboard/service/FlipboardManager$39;-><init>(Lflipboard/service/FlipboardManager;Landroid/app/Activity;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 3360
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iget-object v1, p1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "rate_me"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 3361
    return-void
.end method

.method public final declared-synchronized c(Lflipboard/activities/FlipboardActivity;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2901
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lflipboard/service/FlipboardManager;->aB:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lflipboard/service/FlipboardManager;->aB:I

    .line 2902
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    if-ne p1, v2, :cond_0

    .line 2903
    const/4 v2, 0x0

    iput-object v2, p0, Lflipboard/service/FlipboardManager;->aC:Lflipboard/activities/FlipboardActivity;

    .line 2905
    :cond_0
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aE:Landroid/os/Handler;

    new-instance v3, Lflipboard/service/FlipboardManager$34;

    invoke-direct {v3, p0}, Lflipboard/service/FlipboardManager$34;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2912
    invoke-direct {p0}, Lflipboard/service/FlipboardManager;->P()V

    .line 2913
    iget v2, p0, Lflipboard/service/FlipboardManager;->aB:I

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lflipboard/service/FlipboardManager;->aO:Z

    if-nez v2, :cond_4

    .line 2914
    sget-object v2, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lflipboard/service/FlipboardManager;->aO:Z

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->c()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lflipboard/io/UsageManager;->a(Z)V

    :cond_1
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v2}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v2, v2, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v3, v2, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v3}, Lflipboard/service/audio/FLMediaPlayer;->b()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v2, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v3}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    :cond_3
    new-instance v2, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v2, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/service/FlipboardManager;->aN:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x2932e00

    cmp-long v3, v4, v6

    if-gez v3, :cond_6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_6

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    :goto_0
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-static {}, Lflipboard/activities/FlipboardActivity;->u()I

    move-result v3

    if-nez v3, :cond_7

    :goto_1
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    if-eqz v0, :cond_8

    const-string v0, "exit"

    :goto_2
    invoke-virtual {v2, v1, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2;->c()V

    .line 2916
    :cond_4
    iget v0, p0, Lflipboard/service/FlipboardManager;->aB:I

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->aX:Z

    if-eqz v0, :cond_5

    .line 2917
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v3, Lflipboard/service/FlipboardManager$35;

    invoke-direct {v3, p0}, Lflipboard/service/FlipboardManager$35;-><init>(Lflipboard/service/FlipboardManager;)V

    iput-object v3, p0, Lflipboard/service/FlipboardManager;->aY:Ljava/util/TimerTask;

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    .line 2922
    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-wide v0, v0, Lflipboard/model/ConfigSetting;->PauseNetworkAfterBackgroundedDelayWifi:J

    mul-long/2addr v0, v8

    .line 2917
    :goto_3
    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2924
    :cond_5
    monitor-exit p0

    return-void

    .line 2914
    :cond_6
    :try_start_1
    const-string v3, "time_spent on exit event is too high/low to be accurate"

    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "unwanted.invalid_time_spent_on_app_exit"

    invoke-static {v3}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2901
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    move v0, v1

    .line 2914
    goto :goto_1

    :cond_8
    :try_start_2
    const-string v0, "background"

    goto :goto_2

    .line 2922
    :cond_9
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-wide v0, v0, Lflipboard/model/ConfigSetting;->PauseNetworkAfterBackgroundedDelay:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    mul-long/2addr v0, v8

    goto :goto_3
.end method

.method public final c(Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 3

    .prologue
    .line 2656
    invoke-static {}, Lflipboard/util/AndroidUtil;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2657
    const-string v0, "briefing"

    .line 2662
    :goto_0
    const-string v1, "apiClients.json"

    new-instance v2, Lflipboard/service/FlipboardManager$32;

    invoke-direct {v2, p0, v0, p1}, Lflipboard/service/FlipboardManager$32;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    invoke-virtual {p0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    .line 2706
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/RemoteWatchedFile;->g:Z

    .line 2707
    return-void

    .line 2660
    :cond_0
    const-string v0, "fdl"

    goto :goto_0
.end method

.method final c(Lflipboard/service/User;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x0

    .line 2065
    iget-object v0, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2078
    :cond_0
    return-void

    .line 2068
    :cond_1
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "sections"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v3, "accounts"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "userstate"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "magazines"

    aput-object v3, v2, v0

    move v0, v1

    .line 2069
    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v3, v2, v0

    .line 2070
    new-instance v4, Lflipboard/service/FlipboardManager$24;

    invoke-direct {v4, p0, p1}, Lflipboard/service/FlipboardManager$24;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/service/User;)V

    invoke-virtual {p0, v3, v1, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 2069
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 1680
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1681
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 1682
    if-eqz v0, :cond_1

    .line 1684
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/User;->a(Ljava/util/List;)V

    .line 1687
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p1}, Lflipboard/service/User;->f(Ljava/lang/String;)V

    .line 1690
    const-string v0, "flipboard.settings.%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v8

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1692
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1693
    invoke-static {v0}, Lflipboard/settings/Settings;->getPrefsFor(Ljava/lang/Class;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1694
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1700
    :goto_0
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v4, Lflipboard/service/Flap$LogoutRequest;

    invoke-direct {v4, v0, v1}, Lflipboard/service/Flap$LogoutRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object p1, v4, Lflipboard/service/Flap$LogoutRequest;->a:Ljava/lang/String;

    iput-object v5, v4, Lflipboard/service/Flap$LogoutRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v4}, Lflipboard/service/Flap$LogoutRequest;->c()V

    .line 1701
    const-string v1, "logout"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    move-object v4, p1

    move-object v6, v5

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1704
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    invoke-static {v0, v1, p1, v8}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1707
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->i:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 1708
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1709
    invoke-virtual {v0, v8}, Lflipboard/service/Section;->d(Z)Z

    goto :goto_1

    .line 1713
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1202
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, p2}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;ZLjava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 1203
    return-void
.end method

.method public createAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Flap$Request;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$CreateAccountMessage;",
            "Ljava/lang/Object;",
            ">;)",
            "Lflipboard/service/Flap$Request;"
        }
    .end annotation

    .prologue
    .line 1327
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v9, Lflipboard/service/FlipboardManager$13;

    move-object/from16 v0, p7

    invoke-direct {v9, p0, v0}, Lflipboard/service/FlipboardManager$13;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    invoke-virtual/range {v1 .. v9}, Lflipboard/service/FlipboardManager;->createAccount(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$AccountRequestObserver;)Lflipboard/service/Flap$CreateAccountRequest;

    move-result-object v1

    return-object v1
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 3364
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 3365
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->i(Ljava/lang/String;)J

    move-result-wide v0

    .line 3366
    new-instance v3, Lflipboard/service/FlipboardManager$40;

    invoke-direct {v3, p0, p1}, Lflipboard/service/FlipboardManager$40;-><init>(Lflipboard/service/FlipboardManager;Landroid/app/Activity;)V

    iput-object v3, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 3389
    const v3, 0x7f0d033a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 3392
    invoke-direct {p0, v0, v1}, Lflipboard/service/FlipboardManager;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3394
    sget-object v0, Lflipboard/service/FlipboardManager;->bd:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3395
    sget-object v0, Lflipboard/service/FlipboardManager;->be:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3397
    new-instance v3, Lflipboard/io/UsageEvent;

    const-string v4, "user-notification"

    invoke-direct {v3, v4}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3398
    const-string v4, "reason"

    const-string v5, "update forced"

    invoke-virtual {v3, v4, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3399
    invoke-virtual {v3}, Lflipboard/io/UsageEvent;->a()V

    .line 3412
    :goto_0
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 3413
    iput-object v1, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 3414
    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 3415
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iget-object v0, p1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "upgrade"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 3417
    :cond_0
    return-void

    .line 3402
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->bb:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3403
    sget-object v0, Lflipboard/service/FlipboardManager;->bc:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3404
    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 3406
    new-instance v3, Lflipboard/io/UsageEvent;

    const-string v4, "user-notification"

    invoke-direct {v3, v4}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3407
    const-string v4, "reason"

    const-string v5, "update available"

    invoke-virtual {v3, v4, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3408
    invoke-virtual {v3}, Lflipboard/io/UsageEvent;->a()V

    goto :goto_0
.end method

.method public final d(Lflipboard/service/User;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2320
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2363
    :cond_0
    :goto_0
    return-void

    .line 2324
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    iget-object v3, v0, Lflipboard/model/ConfigSetting;->PushNotificationSettings:Ljava/util/List;

    .line 2327
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->PushNotificationSettingsDefaults:Lflipboard/json/FLObject;

    .line 2328
    if-nez v0, :cond_a

    .line 2329
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    move-object v1, v0

    .line 2331
    :goto_1
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 2332
    :goto_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2333
    if-eqz v3, :cond_3

    .line 2334
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2335
    new-instance v4, Landroid/util/Pair;

    invoke-virtual {v1, v0, v5}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {v4, v0, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2
    move v0, v2

    .line 2331
    goto :goto_2

    .line 2341
    :cond_3
    invoke-virtual {p1}, Lflipboard/service/User;->s()Lflipboard/json/FLObject;

    move-result-object v0

    .line 2342
    if-nez v0, :cond_9

    .line 2343
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    move-object v3, v0

    .line 2345
    :goto_4
    invoke-virtual {v3}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v0, v2

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2346
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2347
    if-nez v0, :cond_4

    move v4, v0

    .line 2354
    :goto_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v2

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 2355
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2356
    iget-object v6, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v4, :cond_5

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v5

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v5

    :goto_8
    move v6, v0

    .line 2359
    goto :goto_6

    :cond_5
    move v0, v2

    .line 2356
    goto :goto_7

    .line 2360
    :cond_6
    if-eqz v6, :cond_0

    .line 2361
    invoke-virtual {p1, v3}, Lflipboard/service/User;->a(Lflipboard/json/FLObject;)V

    goto/16 :goto_0

    :cond_7
    move v0, v6

    goto :goto_8

    :cond_8
    move v4, v0

    goto :goto_5

    :cond_9
    move-object v3, v0

    goto :goto_4

    :cond_a
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final d(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigEdition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4023
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigEdition;

    .line 4024
    iget-boolean v2, v0, Lflipboard/objs/ConfigEdition;->d:Z

    if-eqz v2, :cond_0

    .line 4025
    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    goto :goto_0

    .line 4028
    :cond_1
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2432
    if-eqz p1, :cond_0

    .line 2433
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2434
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 2436
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lflipboard/objs/ConfigService;
    .locals 3

    .prologue
    .line 2441
    if-nez p1, :cond_0

    .line 2442
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "null service provided for getConfigService"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2443
    const-string v0, "twitter"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2445
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2446
    if-nez v0, :cond_1

    .line 2448
    new-instance v0, Lflipboard/objs/ConfigService;

    invoke-direct {v0}, Lflipboard/objs/ConfigService;-><init>()V

    .line 2449
    iput-object p1, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    .line 2451
    :cond_1
    return-object v0
.end method

.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2579
    if-nez p1, :cond_1

    .line 2580
    const/4 p1, 0x0

    .line 2593
    :cond_0
    :goto_0
    return-object p1

    .line 2583
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->U:Lflipboard/json/FLObject;

    .line 2585
    if-eqz v0, :cond_0

    .line 2589
    invoke-virtual {v0, p1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 3092
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lflipboard/service/Flap;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "?udid=%s&tuuid=%s&ver=%s&device=%s&lang=%s&locale=%s&screensize=%.1f"

    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {p0}, Lflipboard/service/Flap;->c()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-virtual {p0}, Lflipboard/service/Flap;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    aput-object v3, v5, v6

    const/4 v3, 0x5

    aput-object v2, v5, v3

    const/4 v2, 0x6

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->h()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "&userid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_2

    const-string v0, "&variant=china"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 945
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/FlipboardManager;->aK:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1018
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    iget-boolean v0, v0, Lflipboard/util/TouchInfo;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1022
    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    iget v3, v2, Lflipboard/util/TouchInfo;->k:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v4, Lflipboard/util/TouchInfo;->a:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    iget v2, v2, Lflipboard/util/TouchInfo;->l:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sget v3, Lflipboard/util/TouchInfo;->a:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1026
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    iget-boolean v0, v0, Lflipboard/util/TouchInfo;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lflipboard/service/FlipboardManager$RootScreenStyle;
    .locals 4

    .prologue
    .line 1034
    sget-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    .line 1036
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v1, :cond_0

    .line 1037
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "settings_tab_homescreen"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1038
    if-eqz v1, :cond_0

    .line 1039
    sget-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    .line 1042
    :cond_0
    return-object v0
.end method

.method public final m()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1050
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "do_first_launch"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1051
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "do_first_launch_category"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1052
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1055
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "do_first_launch"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1056
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "do_first_launch_category"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1057
    return-void
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 1060
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "do_first_launch"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1072
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "freeze_feeds"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1073
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 1074
    const/4 v0, 0x1

    .line 1076
    :cond_0
    return v0
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 1826
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1827
    const-string v1, "flipboard.app.QUIT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1828
    iget-object v1, p0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1829
    return-void
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 1933
    new-instance v0, Lflipboard/service/User;

    const-string v1, "0"

    invoke-direct {v0, v1}, Lflipboard/service/User;-><init>(Ljava/lang/String;)V

    .line 1936
    invoke-virtual {v0}, Lflipboard/service/User;->j()V

    .line 1937
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Ljava/util/List;)V

    .line 1938
    iget-object v1, v0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1941
    invoke-virtual {p0, v0}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    .line 1942
    return-void
.end method

.method public final declared-synchronized t()Lflipboard/service/Flap;
    .locals 0

    .prologue
    .line 2208
    monitor-enter p0

    monitor-exit p0

    return-object p0
.end method

.method public final u()Lflipboard/model/ConfigSetting;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2218
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->aW:Z

    if-nez v0, :cond_0

    .line 2220
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2221
    new-instance v0, Lflipboard/model/ConfigSetting;

    invoke-direct {v0}, Lflipboard/model/ConfigSetting;-><init>()V

    .line 2223
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    goto :goto_0
.end method

.method public final v()Lflipboard/model/ConfigSetting;
    .locals 3

    .prologue
    .line 2234
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v0, :cond_0

    .line 2235
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->c()Z

    move-result v0

    .line 2236
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->g()V

    .line 2237
    const-string v1, "config.json"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    .line 2238
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/service/RemoteWatchedFile;->b(Z)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/service/RemoteWatchedFile;->c(Z)V

    .line 2240
    :try_start_0
    invoke-virtual {v1}, Lflipboard/service/RemoteWatchedFile;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2245
    :goto_0
    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v0, v0, Lflipboard/service/FlipboardManager;->aB:I

    if-nez v0, :cond_0

    .line 2246
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->h()V

    .line 2249
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    return-object v0

    .line 2241
    :catch_0
    move-exception v1

    .line 2242
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 2253
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->T:Lflipboard/model/ConfigSetting;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2456
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2457
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_1

    .line 2458
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2459
    iget-object v3, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    .line 2460
    if-eqz v3, :cond_0

    .line 2461
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2465
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "User == null || serviceMap == null while trying to get logged in services"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2467
    :cond_2
    return-object v1
.end method

.method public final y()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2472
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2473
    invoke-virtual {p0}, Lflipboard/service/FlipboardManager;->x()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2474
    iget-object v3, p0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    .line 2475
    if-eqz v3, :cond_0

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->g:Z

    if-eqz v0, :cond_0

    .line 2476
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2479
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 2480
    return-object v1
.end method

.method public final z()Lflipboard/service/audio/FLAudioManager;
    .locals 2

    .prologue
    .line 2711
    iget-boolean v0, p0, Lflipboard/service/FlipboardManager;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    if-nez v0, :cond_0

    .line 2712
    new-instance v0, Lflipboard/service/audio/FLAudioManager;

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-direct {v0, v1}, Lflipboard/service/audio/FLAudioManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    .line 2715
    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager;->aV:Lflipboard/service/audio/FLAudioManager;

    return-object v0
.end method
