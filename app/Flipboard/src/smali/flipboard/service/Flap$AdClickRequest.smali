.class Lflipboard/service/Flap$AdClickRequest;
.super Lflipboard/service/Flap$RetryRequest;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private f:J


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 5506
    iput-object p1, p0, Lflipboard/service/Flap$AdClickRequest;->b:Lflipboard/service/Flap;

    .line 5507
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$RetryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 5508
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdClickRequest;
    .locals 0

    .prologue
    .line 5512
    iput-object p1, p0, Lflipboard/service/Flap$AdClickRequest;->c:Ljava/lang/String;

    .line 5513
    iput-wide p2, p0, Lflipboard/service/Flap$AdClickRequest;->f:J

    .line 5514
    iput-object p4, p0, Lflipboard/service/Flap$AdClickRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 5515
    invoke-super {p0}, Lflipboard/service/Flap$RetryRequest;->c()V

    .line 5516
    return-object p0
.end method

.method protected final a(Lflipboard/json/FLObject;)V
    .locals 1

    .prologue
    .line 5521
    iget-object v0, p0, Lflipboard/service/Flap$AdClickRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    .line 5522
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5526
    iget-object v0, p0, Lflipboard/service/Flap$AdClickRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 5527
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 8

    .prologue
    .line 5531
    iget-object v0, p0, Lflipboard/service/Flap$AdClickRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/click"

    iget-object v2, p0, Lflipboard/service/Flap$AdClickRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "click_value"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/service/Flap$AdClickRequest;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "click_timestamp"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-wide v6, p0, Lflipboard/service/Flap$AdClickRequest;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->b(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
