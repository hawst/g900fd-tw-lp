.class Lflipboard/service/MetaData$1;
.super Lflipboard/service/DatabaseHandler;
.source "MetaData.java"


# instance fields
.field final synthetic a:Lflipboard/service/MetaData;


# direct methods
.method constructor <init>(Lflipboard/service/MetaData;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lflipboard/service/MetaData$1;->a:Lflipboard/service/MetaData;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 73
    :try_start_0
    const-string v0, "select metadata from %s where id=?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/MetaData$1;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 74
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/MetaData$1;->a:Lflipboard/service/MetaData;

    invoke-static {v3}, Lflipboard/service/MetaData;->a(Lflipboard/service/MetaData;)Lflipboard/service/DatabaseRow;

    move-result-object v3

    invoke-interface {v3}, Lflipboard/service/DatabaseRow;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lflipboard/service/MetaData$1;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lflipboard/service/MetaData$1;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lflipboard/service/MetaData$1;->a:Lflipboard/service/MetaData;

    const-string v1, "metaData"

    invoke-virtual {p0, v1}, Lflipboard/service/MetaData$1;->d(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/MetaData;->a([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/service/MetaData$1;->a:Lflipboard/service/MetaData;

    invoke-static {v0}, Lflipboard/service/MetaData;->b(Lflipboard/service/MetaData;)Lflipboard/json/FLObject;

    move-result-object v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lflipboard/service/MetaData$1;->a:Lflipboard/service/MetaData;

    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    invoke-static {v0, v1}, Lflipboard/service/MetaData;->a(Lflipboard/service/MetaData;Lflipboard/json/FLObject;)Lflipboard/json/FLObject;

    .line 84
    :cond_1
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    sget-object v1, Lflipboard/service/MetaData;->a:Lflipboard/util/Log;

    const-string v2, "Unexpected exception: %s loading %s.%d.metadata"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    iget-object v0, p0, Lflipboard/service/MetaData$1;->f:Ljava/lang/String;

    aput-object v0, v3, v5

    const/4 v0, 0x2

    iget-object v4, p0, Lflipboard/service/MetaData$1;->a:Lflipboard/service/MetaData;

    invoke-static {v4}, Lflipboard/service/MetaData;->a(Lflipboard/service/MetaData;)Lflipboard/service/DatabaseRow;

    move-result-object v4

    invoke-interface {v4}, Lflipboard/service/DatabaseRow;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
