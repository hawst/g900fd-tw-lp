.class public Lflipboard/service/Flap$ServiceLoginXauthRequest;
.super Lflipboard/service/Flap$AccountRequest;
.source "Flap.java"


# instance fields
.field g:Lflipboard/objs/ConfigService;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field final synthetic j:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/objs/ConfigService;)V
    .locals 0

    .prologue
    .line 1968
    iput-object p1, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->j:Lflipboard/service/Flap;

    .line 1969
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$AccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1970
    iput-object p3, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->g:Lflipboard/objs/ConfigService;

    .line 1971
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1984
    iget-object v0, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->g:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->ad:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->g:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->ad:Ljava/lang/String;

    .line 1986
    :goto_0
    iget-object v1, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->g:Lflipboard/objs/ConfigService;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->G:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1987
    iget-object v1, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->j:Lflipboard/service/Flap;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->g:Lflipboard/objs/ConfigService;

    iget-object v3, v3, Lflipboard/objs/ConfigService;->G:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->n:Lflipboard/service/User;

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v0, v4, v5

    iget-object v0, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->h:Ljava/lang/String;

    aput-object v0, v4, v6

    const-string v0, "password"

    aput-object v0, v4, v7

    iget-object v0, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->i:Ljava/lang/String;

    aput-object v0, v4, v8

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1991
    :goto_1
    return-object v0

    .line 1984
    :cond_0
    const-string v0, "username"

    goto :goto_0

    .line 1989
    :cond_1
    iget-object v1, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->j:Lflipboard/service/Flap;

    const-string v2, "/v1/users/xauthLogin"

    iget-object v3, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->n:Lflipboard/service/User;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    iget-object v0, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->h:Ljava/lang/String;

    aput-object v0, v4, v6

    const-string v0, "password"

    aput-object v0, v4, v7

    iget-object v0, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->i:Ljava/lang/String;

    aput-object v0, v4, v8

    const-string v0, "loginService"

    aput-object v0, v4, v9

    const/4 v0, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->g:Lflipboard/objs/ConfigService;

    iget-object v5, v5, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$AccountRequestObserver;)Lflipboard/service/Flap$ServiceLoginXauthRequest;
    .locals 0

    .prologue
    .line 1975
    iput-object p3, p0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    .line 1976
    iput-object p1, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->h:Ljava/lang/String;

    .line 1977
    iput-object p2, p0, Lflipboard/service/Flap$ServiceLoginXauthRequest;->i:Ljava/lang/String;

    .line 1978
    invoke-virtual {p0}, Lflipboard/service/Flap$ServiceLoginXauthRequest;->c()V

    .line 1979
    return-object p0
.end method
