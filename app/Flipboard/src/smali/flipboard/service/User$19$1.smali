.class Lflipboard/service/User$19$1;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Lflipboard/service/User$19;


# direct methods
.method constructor <init>(Lflipboard/service/User$19;)V
    .locals 0

    .prologue
    .line 1418
    iput-object p1, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1421
    const-string v1, "SELECT descriptor,sectionId,title,service,image,tocItem,id,pos,remoteId,private,unreadRemoteId,metaData FROM sections WHERE uid = ? LIMIT ? OFFSET ?"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v3, v3, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    iget-object v3, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v3, 0x1

    const-string v4, "999999"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget v4, v4, Lflipboard/service/User$19;->b:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lflipboard/service/User$19$1;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1425
    :cond_0
    :goto_0
    iget-object v1, p0, Lflipboard/service/User$19$1;->h:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1426
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, p0}, Lflipboard/service/Section;-><init>(Lflipboard/service/DatabaseHandler;)V

    .line 1429
    invoke-virtual {v1}, Lflipboard/service/Section;->t()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1430
    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->n(Lflipboard/service/User;)Lflipboard/service/Section;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1432
    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v2, v1}, Lflipboard/service/User;->a(Lflipboard/service/User;Lflipboard/service/Section;)Lflipboard/service/Section;

    .line 1439
    :cond_1
    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1440
    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-boolean v2, v2, Lflipboard/service/User$19;->d:Z

    iput-boolean v2, v1, Lflipboard/service/Section;->B:Z

    .line 1441
    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->o(Lflipboard/service/User;)Lflipboard/util/Observer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    goto :goto_0

    .line 1435
    :cond_2
    iget-object v2, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v2, v2, Lflipboard/service/Section$Meta;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v2, v2, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    iget-object v3, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1436
    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1444
    :cond_3
    iget-object v1, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v1, v1, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->p(Lflipboard/service/User;)V

    .line 1445
    iget-object v1, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v1, v1, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->n(Lflipboard/service/User;)Lflipboard/service/Section;

    move-result-object v1

    iget v1, v1, Lflipboard/service/Section;->g:I

    if-eqz v1, :cond_4

    .line 1446
    iget-object v1, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v1, v1, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->n(Lflipboard/service/User;)Lflipboard/service/Section;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1447
    iget-object v1, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v1, v1, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->n(Lflipboard/service/User;)Lflipboard/service/Section;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1449
    iget-object v1, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v1, v1, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 1450
    add-int/lit8 v2, v1, 0x1

    iput v1, v0, Lflipboard/service/Section;->g:I

    move v1, v2

    .line 1451
    goto :goto_1

    .line 1457
    :cond_4
    iget-object v0, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v0, v0, Lflipboard/service/User$19;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1459
    iget-object v0, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v0, v0, Lflipboard/service/User$19;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "sections"

    new-instance v2, Lflipboard/service/User$19$1$1;

    invoke-direct {v2, p0}, Lflipboard/service/User$19$1$1;-><init>(Lflipboard/service/User$19$1;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V

    .line 1470
    iget-object v0, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v0, v0, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 1473
    :cond_5
    iget-object v0, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v0, v0, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    iget-object v2, p0, Lflipboard/service/User$19$1;->a:Lflipboard/service/User$19;

    iget-object v2, v2, Lflipboard/service/User$19;->e:Lflipboard/service/User;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1474
    return-void
.end method
