.class abstract Lflipboard/service/Flap$StateRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lflipboard/objs/FlapObjectResult;",
        ">",
        "Lflipboard/service/Flap$Request;"
    }
.end annotation


# instance fields
.field protected a:Lflipboard/service/Flap$TypedResultObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$TypedResultObserver",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected b:Ljava/lang/String;

.field protected c:I

.field protected d:Ljava/lang/String;

.field final synthetic e:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 2003
    iput-object p1, p0, Lflipboard/service/Flap$StateRequest;->e:Lflipboard/service/Flap;

    .line 2004
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2005
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/io/InputStream;)Lflipboard/objs/FlapObjectResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/String;ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<TT;>;)",
            "Lflipboard/service/Flap$StateRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2009
    iput-object p1, p0, Lflipboard/service/Flap$StateRequest;->b:Ljava/lang/String;

    .line 2010
    iput p2, p0, Lflipboard/service/Flap$StateRequest;->c:I

    .line 2011
    iput-object p3, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2012
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2013
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<TT;>;)",
            "Lflipboard/service/Flap$StateRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2018
    iput-object p1, p0, Lflipboard/service/Flap$StateRequest;->b:Ljava/lang/String;

    .line 2019
    iput-object p4, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2020
    iput-object p2, p0, Lflipboard/service/Flap$StateRequest;->d:Ljava/lang/String;

    .line 2021
    iput p3, p0, Lflipboard/service/Flap$StateRequest;->c:I

    .line 2022
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2023
    return-object p0
.end method

.method protected final a()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 2029
    iget-object v1, p0, Lflipboard/service/Flap$StateRequest;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    move v1, v2

    .line 2030
    :goto_0
    iget-object v5, p0, Lflipboard/service/Flap$StateRequest;->e:Lflipboard/service/Flap;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v4, "/v1/social/"

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_2

    const-string v4, "put"

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "State"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lflipboard/service/Flap$StateRequest;->n:Lflipboard/service/User;

    const/4 v4, 0x5

    new-array v8, v4, [Ljava/lang/Object;

    const-string v4, "type"

    aput-object v4, v8, v3

    iget-object v4, p0, Lflipboard/service/Flap$StateRequest;->b:Ljava/lang/String;

    aput-object v4, v8, v2

    iget v4, p0, Lflipboard/service/Flap$StateRequest;->c:I

    if-ltz v4, :cond_3

    move v4, v2

    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v8, v11

    const-string v4, "revision"

    aput-object v4, v8, v12

    const/4 v4, 0x4

    iget v9, p0, Lflipboard/service/Flap$StateRequest;->c:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-virtual {v5, v6, v7, v8}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2031
    sget-object v4, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v6, v12, [Ljava/lang/Object;

    if-eqz v1, :cond_4

    const-string v4, "put"

    :goto_3
    aput-object v4, v6, v3

    iget-object v3, p0, Lflipboard/service/Flap$StateRequest;->b:Ljava/lang/String;

    aput-object v3, v6, v2

    aput-object v5, v6, v11

    .line 2034
    if-eqz v1, :cond_5

    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 2035
    :goto_4
    const-string v3, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-interface {v2, v3, v4}, Lorg/apache/http/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2036
    if-eqz v1, :cond_0

    .line 2037
    const-string v1, "Content-Encoding"

    const-string v3, "deflate"

    invoke-interface {v2, v1, v3}, Lorg/apache/http/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2038
    move-object v0, v2

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object v1, v0

    new-instance v3, Lorg/apache/http/entity/ByteArrayEntity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "data="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lflipboard/service/Flap$StateRequest;->d:Ljava/lang/String;

    invoke-static {v5}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/JavaUtil;->b([B)[B

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v1, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2041
    :cond_0
    iget-object v1, p0, Lflipboard/service/Flap$StateRequest;->e:Lflipboard/service/Flap;

    invoke-virtual {v1, v2}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 2042
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 2043
    invoke-virtual {p0, v2, v1}, Lflipboard/service/Flap$StateRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v1

    .line 2044
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 2045
    const/16 v4, 0xc8

    if-eq v2, v4, :cond_6

    .line 2046
    iget-object v1, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected response from flap: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2061
    iput-object v10, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 2062
    :goto_5
    return-void

    :cond_1
    move v1, v3

    .line 2029
    goto/16 :goto_0

    .line 2030
    :cond_2
    const-string v4, "get"

    goto/16 :goto_1

    :cond_3
    move v4, v3

    goto/16 :goto_2

    .line 2031
    :cond_4
    const-string v4, "get"

    goto/16 :goto_3

    .line 2034
    :cond_5
    :try_start_1
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 2055
    :catch_0
    move-exception v1

    .line 2056
    :try_start_2
    iget-object v2, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2061
    iput-object v10, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_5

    .line 2049
    :cond_6
    :try_start_3
    invoke-virtual {p0, v1}, Lflipboard/service/Flap$StateRequest;->a(Ljava/io/InputStream;)Lflipboard/objs/FlapObjectResult;

    move-result-object v1

    .line 2050
    if-nez v1, :cond_7

    .line 2051
    iget-object v1, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    const-string v2, "null result from flap"

    invoke-interface {v1, v2}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2061
    iput-object v10, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_5

    .line 2054
    :cond_7
    :try_start_4
    iget-object v2, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-interface {v2, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2061
    iput-object v10, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_5

    .line 2057
    :catch_1
    move-exception v1

    .line 2058
    :try_start_5
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2059
    iget-object v2, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2061
    iput-object v10, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    goto :goto_5

    :catchall_0
    move-exception v1

    iput-object v10, p0, Lflipboard/service/Flap$StateRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    throw v1
.end method
