.class Lflipboard/service/User$21;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:J

.field final synthetic c:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Lflipboard/service/Section;J)V
    .locals 1

    .prologue
    .line 1561
    iput-object p1, p0, Lflipboard/service/User$21;->c:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iput-wide p3, p0, Lflipboard/service/User$21;->b:J

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1564
    :try_start_0
    const-string v0, "SELECT items FROM SECTIONS where uid = ? and id = ?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/User$21;->c:Lflipboard/service/User;

    iget-object v3, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget v3, v3, Lflipboard/service/Section;->f:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User$21;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1567
    iget-object v0, p0, Lflipboard/service/User$21;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1568
    const-string v0, "items"

    invoke-virtual {p0, v0}, Lflipboard/service/User$21;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 1569
    if-eqz v0, :cond_3

    .line 1570
    iget-object v1, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v1}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1571
    iget-object v1, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "User:loadSectionItems - (byte[]) items.length = "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    array-length v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1572
    iget-object v1, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1573
    iget-object v1, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v1}, Lflipboard/util/DuplicateOccurrenceLog;->b()V

    .line 1575
    :cond_0
    iget-object v1, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    new-instance v2, Lflipboard/json/JSONParser;

    invoke-direct {v2, v0}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/Section;->a(Ljava/util/List;)V

    .line 1578
    iget-object v0, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_1

    .line 1579
    iget-object v0, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->a(Ljava/lang/Runnable;)V

    .line 1581
    :cond_1
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/service/User$21;->b:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1590
    :cond_2
    :goto_0
    return-void

    .line 1583
    :cond_3
    iget-object v0, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "User:loadSectionItems - (byte[]) items is null "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1586
    :catch_0
    move-exception v0

    .line 1587
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "ignoring error: failed to parse items from section %s: %-E"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/service/User$21;->a:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
