.class Lflipboard/service/Flap$AdMetricRequest;
.super Lflipboard/service/Flap$RetryRequest;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private f:J

.field private g:J


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 5465
    iput-object p1, p0, Lflipboard/service/Flap$AdMetricRequest;->b:Lflipboard/service/Flap;

    .line 5466
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$RetryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 5467
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JJLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdMetricRequest;
    .locals 0

    .prologue
    .line 5470
    iput-object p1, p0, Lflipboard/service/Flap$AdMetricRequest;->c:Ljava/lang/String;

    .line 5471
    iput-wide p2, p0, Lflipboard/service/Flap$AdMetricRequest;->f:J

    .line 5472
    iput-wide p4, p0, Lflipboard/service/Flap$AdMetricRequest;->g:J

    .line 5473
    iput-object p6, p0, Lflipboard/service/Flap$AdMetricRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 5474
    invoke-super {p0}, Lflipboard/service/Flap$RetryRequest;->c()V

    .line 5475
    return-object p0
.end method

.method protected final a(Lflipboard/json/FLObject;)V
    .locals 1

    .prologue
    .line 5480
    iget-object v0, p0, Lflipboard/service/Flap$AdMetricRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    .line 5481
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5485
    iget-object v0, p0, Lflipboard/service/Flap$AdMetricRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 5486
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 8

    .prologue
    .line 5490
    iget-object v0, p0, Lflipboard/service/Flap$AdMetricRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/metric"

    iget-object v2, p0, Lflipboard/service/Flap$AdMetricRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "metric_value"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/service/Flap$AdMetricRequest;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "metric_duration"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-wide v6, p0, Lflipboard/service/Flap$AdMetricRequest;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "metric_timestamp"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-wide v6, p0, Lflipboard/service/Flap$AdMetricRequest;->g:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->b(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
