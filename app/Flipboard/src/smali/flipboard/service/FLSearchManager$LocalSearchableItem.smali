.class public Lflipboard/service/FLSearchManager$LocalSearchableItem;
.super Lflipboard/objs/SearchResultItem;
.source "FLSearchManager.java"


# instance fields
.field a:Ljava/lang/String;

.field final synthetic b:Lflipboard/service/FLSearchManager;


# direct methods
.method public constructor <init>(Lflipboard/service/FLSearchManager;Lflipboard/objs/Magazine;)V
    .locals 2

    .prologue
    .line 242
    iput-object p1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->b:Lflipboard/service/FLSearchManager;

    invoke-direct {p0}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 243
    iget-object v0, p2, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    .line 244
    iget-object v0, p2, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    .line 245
    iget-object v0, p2, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->t:Ljava/lang/String;

    .line 246
    iget-object v0, p2, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->s:Ljava/lang/String;

    .line 247
    invoke-static {p1}, Lflipboard/service/FLSearchManager;->a(Lflipboard/service/FLSearchManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    sget-object v0, Lflipboard/objs/SearchResultItem;->k:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    .line 254
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 256
    iget-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 257
    iget-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->a:Ljava/lang/String;

    .line 261
    return-void

    .line 250
    :cond_1
    sget-object v0, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lflipboard/service/FLSearchManager;Lflipboard/service/Account;)V
    .locals 3

    .prologue
    .line 264
    iput-object p1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->b:Lflipboard/service/FLSearchManager;

    invoke-direct {p0}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 265
    iget-object v0, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    .line 267
    const-string v1, "flipboard"

    iget-object v2, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->e:Lflipboard/service/FLSearchManager$ResultType;

    iput-object v1, p1, Lflipboard/service/FLSearchManager;->e:Lflipboard/service/FLSearchManager$ResultType;

    .line 269
    iget-object v1, v0, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    .line 276
    :goto_0
    iget-object v1, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    .line 277
    iget-object v1, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->s:Ljava/lang/String;

    .line 278
    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->t:Ljava/lang/String;

    .line 279
    invoke-static {p1}, Lflipboard/service/FLSearchManager;->a(Lflipboard/service/FLSearchManager;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    sget-object v1, Lflipboard/objs/SearchResultItem;->m:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    .line 285
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 287
    iget-object v2, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 288
    iget-object v2, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_0
    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->v:Ljava/lang/String;

    .line 293
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->a:Ljava/lang/String;

    .line 294
    return-void

    .line 272
    :cond_1
    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->d:Lflipboard/service/FLSearchManager$ResultType;

    iput-object v1, p1, Lflipboard/service/FLSearchManager;->e:Lflipboard/service/FLSearchManager$ResultType;

    .line 273
    iget-object v1, v0, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    goto :goto_0

    .line 282
    :cond_2
    sget-object v1, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(Lflipboard/service/FLSearchManager;Lflipboard/service/Section;)V
    .locals 3

    .prologue
    .line 195
    iput-object p1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->b:Lflipboard/service/FLSearchManager;

    invoke-direct {p0}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 198
    iget-object v0, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    .line 199
    invoke-virtual {p2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    .line 200
    iget-object v0, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v0}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->t:Ljava/lang/String;

    .line 201
    iget-object v1, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    .line 202
    invoke-static {p1}, Lflipboard/service/FLSearchManager;->a(Lflipboard/service/FLSearchManager;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 203
    invoke-virtual {p2}, Lflipboard/service/Section;->y()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204
    sget-object v0, Lflipboard/objs/SearchResultItem;->j:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    .line 215
    :goto_0
    if-eqz v1, :cond_1

    .line 217
    iget-object v0, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    if-eqz v0, :cond_7

    iget-object v0, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v0

    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_8

    .line 218
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_0

    .line 220
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->s:Ljava/lang/String;

    .line 222
    :cond_0
    sget-object v0, Lflipboard/service/FLSearchManager$ResultType;->d:Lflipboard/service/FLSearchManager$ResultType;

    iput-object v0, p1, Lflipboard/service/FLSearchManager;->e:Lflipboard/service/FLSearchManager$ResultType;

    .line 226
    :goto_2
    invoke-virtual {v1}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->t:Ljava/lang/String;

    .line 227
    iget-object v0, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->v:Ljava/lang/String;

    .line 231
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    iget-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 234
    iget-object v1, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->a:Ljava/lang/String;

    .line 238
    return-void

    .line 205
    :cond_3
    invoke-virtual {p2}, Lflipboard/service/Section;->x()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 206
    sget-object v0, Lflipboard/objs/SearchResultItem;->k:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    goto :goto_0

    .line 207
    :cond_4
    invoke-virtual {p2}, Lflipboard/service/Section;->z()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 208
    sget-object v0, Lflipboard/objs/SearchResultItem;->m:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    goto :goto_0

    .line 210
    :cond_5
    sget-object v0, Lflipboard/objs/SearchResultItem;->l:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    goto :goto_0

    .line 213
    :cond_6
    sget-object v0, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 217
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 224
    :cond_8
    iget-object v0, v1, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->s:Ljava/lang/String;

    goto :goto_2
.end method
