.class public Lflipboard/service/ActivityFetcher;
.super Ljava/lang/Object;
.source "ActivityFetcher.java"


# static fields
.field static final a:Lflipboard/util/Log;


# instance fields
.field final b:Landroid/database/sqlite/SQLiteDatabase;

.field final c:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "activity"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lflipboard/service/ActivityFetcher;->c:Ljava/util/concurrent/ConcurrentHashMap;

    .line 31
    new-instance v0, Lflipboard/service/ActivityFetcher$1;

    invoke-direct {v0, p0}, Lflipboard/service/ActivityFetcher$1;-><init>(Lflipboard/service/ActivityFetcher;)V

    iput-object v0, p0, Lflipboard/service/ActivityFetcher;->d:Ljava/util/Map;

    .line 40
    new-instance v0, Lflipboard/service/ActivityFetcher$2;

    const-string v1, "activity-v1.db"

    invoke-direct {v0, p0, p1, v1}, Lflipboard/service/ActivityFetcher$2;-><init>(Lflipboard/service/ActivityFetcher;Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0}, Lflipboard/service/ActivityFetcher$2;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/ActivityFetcher;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb800

    sub-long/2addr v0, v2

    .line 53
    iget-object v2, p0, Lflipboard/service/ActivityFetcher;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "activity"

    const-string v4, "expires < ?"

    new-array v5, v9, [Ljava/lang/String;

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 54
    if-lez v0, :cond_0

    .line 55
    sget-object v1, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v8

    .line 60
    :cond_0
    const-string v0, "SELECT id, data FROM activity ORDER BY expires"

    .line 61
    iget-object v1, p0, Lflipboard/service/ActivityFetcher;->b:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 63
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 65
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 66
    iget-object v3, p0, Lflipboard/service/ActivityFetcher;->d:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 72
    sget-object v0, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    new-array v0, v9, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/ActivityFetcher;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    .line 74
    return-void
.end method

.method static synthetic a(Lflipboard/service/ActivityFetcher;Lflipboard/objs/CommentaryResult;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x3e8

    .line 24
    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    invoke-static {v0}, Lflipboard/json/JSONSerializerBase;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lflipboard/service/ActivityFetcher;->d:Ljava/util/Map;

    iget-object v6, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v6, v0, Lflipboard/objs/CommentaryResult$Item;->j:J

    mul-long/2addr v6, v12

    add-long/2addr v6, v2

    const-string v5, "INSERT OR REPLACE INTO activity (id,expires,data) VALUES (?,?,?)"

    iget-object v8, p0, Lflipboard/service/ActivityFetcher;->b:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    aput-object v0, v9, v10

    const/4 v0, 0x1

    div-long/2addr v6, v12

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v0

    const/4 v0, 0x2

    aput-object v4, v9, v0

    invoke-virtual {v8, v5, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    return-void
.end method


# virtual methods
.method final a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 150
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 156
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lflipboard/service/ActivityFetcher;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 158
    if-eqz v1, :cond_2

    .line 159
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, v1}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->e()Lflipboard/objs/CommentaryResult$Item;

    move-result-object v0

    .line 160
    const-wide/16 v6, 0x3c

    iput-wide v6, v0, Lflipboard/objs/CommentaryResult$Item;->j:J

    .line 161
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 163
    goto :goto_1

    .line 164
    :cond_2
    invoke-static {}, Lflipboard/objs/CommentaryResult$Item;->a()Lflipboard/objs/CommentaryResult$Item;

    move-result-object v1

    .line 165
    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    .line 166
    const-wide/16 v6, 0xa

    iput-wide v6, v1, Lflipboard/objs/CommentaryResult$Item;->j:J

    .line 167
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 177
    :catch_0
    move-exception v0

    .line 178
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 170
    :cond_3
    if-lez v2, :cond_4

    .line 171
    :try_start_1
    sget-object v0, Lflipboard/service/ActivityFetcher;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 174
    :cond_4
    new-instance v0, Lflipboard/objs/CommentaryResult;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult;-><init>()V

    .line 175
    iput-object v3, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    .line 176
    invoke-interface {p2, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
