.class public Lflipboard/service/TrackedRunnable;
.super Ljava/lang/Object;
.source "TrackedRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/lang/Runnable;

.field b:Z

.field c:Z


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lflipboard/service/TrackedRunnable;->a:Ljava/lang/Runnable;

    .line 17
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 21
    iput-boolean v4, p0, Lflipboard/service/TrackedRunnable;->c:Z

    .line 22
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 23
    iget-object v2, p0, Lflipboard/service/TrackedRunnable;->a:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 24
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 25
    iput-boolean v4, p0, Lflipboard/service/TrackedRunnable;->b:Z

    .line 27
    sub-long v0, v2, v0

    .line 28
    const-wide/16 v2, 0x2710

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 29
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Runnable "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lflipboard/service/TrackedRunnable;->a:Ljava/lang/Runnable;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " finally finished after more than 10000 milliseconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-virtual {v2}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 31
    new-instance v3, Lflipboard/service/TrackedRunnable$1;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/service/TrackedRunnable$1;-><init>(Lflipboard/service/TrackedRunnable;J)V

    invoke-static {v2, v3}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 42
    :cond_0
    return-void
.end method
