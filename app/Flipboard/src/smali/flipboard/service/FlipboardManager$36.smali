.class Lflipboard/service/FlipboardManager$36;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2999
    iput-object p1, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$36;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 3002
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 3003
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$36;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 3005
    :try_start_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v0

    .line 3006
    const-string v2, "ads_tracking_id"

    iget-object v3, v0, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3007
    const-string v2, "limit_ads_tracking"

    iget-boolean v0, v0, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_2

    .line 3017
    :goto_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->n(Lflipboard/service/FlipboardManager;)Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3018
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v2}, Lflipboard/service/FlipboardManager;->n(Lflipboard/service/FlipboardManager;)Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 3019
    iget-object v0, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->o(Lflipboard/service/FlipboardManager;)Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 3021
    :cond_0
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->b()V

    .line 3022
    return-void

    .line 3008
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 3012
    :catch_1
    move-exception v0

    iget-object v0, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->m(Lflipboard/service/FlipboardManager;)Z

    goto :goto_0

    .line 3015
    :catch_2
    move-exception v0

    iget-object v0, p0, Lflipboard/service/FlipboardManager$36;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->m(Lflipboard/service/FlipboardManager;)Z

    goto :goto_0
.end method
