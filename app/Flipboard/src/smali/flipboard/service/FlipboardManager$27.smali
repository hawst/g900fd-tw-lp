.class Lflipboard/service/FlipboardManager$27;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 2261
    iput-object p1, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2307
    iget-object v0, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0, v3}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/FlipboardManager;Z)Z

    .line 2308
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "fail loading config: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2309
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2264
    iget-object v0, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0, v4}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/FlipboardManager;Z)Z

    .line 2271
    :try_start_0
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v1

    .line 2273
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "idioms."

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "idioms."

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 2276
    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->putAll(Ljava/util/Map;)V

    .line 2277
    const-string v0, "idioms"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2280
    :cond_0
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_1

    const-string v0, "variants.china"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2282
    const-string v0, "variants.china"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 2283
    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->putAll(Ljava/util/Map;)V

    .line 2284
    const-string v0, "variants"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2287
    :cond_1
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v0, :cond_3

    const-string v0, "kindle"

    .line 2288
    :goto_0
    if-eqz v0, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stores."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2290
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stores."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 2291
    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->putAll(Ljava/util/Map;)V

    .line 2292
    const-string v0, "stores"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2296
    :cond_2
    iget-object v2, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lflipboard/model/ConfigSetting;

    invoke-static {v0, v1}, Lflipboard/json/JsonSerializationWrapper;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigSetting;

    invoke-static {v2, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Lflipboard/model/ConfigSetting;)Lflipboard/model/ConfigSetting;

    .line 2297
    iget-object v0, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->i(Lflipboard/service/FlipboardManager;)Z

    .line 2298
    iget-object v0, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/User;)V

    .line 2303
    return-void

    .line 2287
    :cond_3
    sget-boolean v0, Lflipboard/service/FlipboardManager;->p:Z

    if-eqz v0, :cond_4

    const-string v0, "nook"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2300
    :catch_0
    move-exception v0

    .line 2301
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "failed to parse config.json: %-E"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2302
    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2312
    iget-object v0, p0, Lflipboard/service/FlipboardManager$27;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0, v2}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/FlipboardManager;Z)Z

    .line 2313
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "fail loading config, maintenance: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2314
    return-void
.end method
