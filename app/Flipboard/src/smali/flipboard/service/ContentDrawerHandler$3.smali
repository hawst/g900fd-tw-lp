.class Lflipboard/service/ContentDrawerHandler$3;
.super Ljava/lang/Object;
.source "ContentDrawerHandler.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/service/ContentDrawerHandler;


# direct methods
.method constructor <init>(Lflipboard/service/ContentDrawerHandler;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 316
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const-string v1, "fail loading sections.json: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->d:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p1}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 318
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 236
    :try_start_0
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Z)Z

    .line 237
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->g()Lflipboard/objs/ConfigContentGuide;

    move-result-object v2

    .line 238
    if-eqz v2, :cond_0

    iget-object v0, v2, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    if-nez v0, :cond_1

    .line 239
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Failed to load valid content guide, it or its sections is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v4, v2, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    .line 245
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v2, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Ljava/util/List;)V

    move v1, v3

    .line 247
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 248
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigFolder;

    .line 250
    iget-object v5, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    const-string v6, "new"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v2, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v8, :cond_3

    .line 252
    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 254
    new-instance v5, Lflipboard/objs/GuideSwitch;

    invoke-direct {v5}, Lflipboard/objs/GuideSwitch;-><init>()V

    .line 255
    invoke-virtual {v0}, Lflipboard/objs/ConfigFolder;->c()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    const/4 v5, 0x0

    invoke-interface {v4, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 247
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 257
    :cond_3
    iget-object v5, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v0, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    const-string v5, "simplifiedui"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 311
    :catch_0
    move-exception v0

    .line 302
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "failed to parse sections.json: %-E"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-virtual {v1, v2, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v1

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->b:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v1, v2, v0}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 304
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1, v8}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Z)Z

    .line 305
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 306
    :try_start_1
    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 307
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 310
    throw v0

    .line 262
    :cond_4
    :try_start_2
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v5

    monitor-enter v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 265
    :try_start_3
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->f(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigFolder;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 266
    const/4 v0, 0x6

    .line 271
    :goto_3
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v0, :cond_a

    .line 272
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    .line 275
    :goto_4
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_6

    .line 276
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_5
    if-lt v0, v1, :cond_6

    .line 277
    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 276
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    .line 268
    :cond_5
    const/4 v0, 0x5

    goto :goto_3

    .line 280
    :cond_6
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 281
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->g(Lflipboard/service/ContentDrawerHandler;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move v1, v3

    .line 286
    :goto_6
    if-ge v1, v2, :cond_7

    .line 287
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 286
    :goto_7
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_6

    .line 291
    :cond_7
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    const/4 v7, 0x0

    invoke-direct {v1, v6, v7}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 292
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    add-int/lit8 v1, v2, 0x1

    invoke-interface {v0, v1, v4}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 293
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)Ljava/util/List;

    .line 294
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Z)Z

    .line 295
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 297
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_8

    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->g:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    .line 298
    :goto_8
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 299
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v5

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 297
    :cond_8
    :try_start_5
    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_8

    .line 307
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_9
    move v0, v2

    goto :goto_7

    :cond_a
    move v1, v0

    goto/16 :goto_4
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 322
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const-string v1, "fail loading sections.json, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$3;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->d:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p1}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 324
    return-void
.end method
