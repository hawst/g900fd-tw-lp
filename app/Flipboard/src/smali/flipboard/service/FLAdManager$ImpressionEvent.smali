.class public final enum Lflipboard/service/FLAdManager$ImpressionEvent;
.super Ljava/lang/Enum;
.source "FLAdManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/FLAdManager$ImpressionEvent;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/FLAdManager$ImpressionEvent;

.field public static final enum b:Lflipboard/service/FLAdManager$ImpressionEvent;

.field public static final enum c:Lflipboard/service/FLAdManager$ImpressionEvent;

.field private static final synthetic e:[Lflipboard/service/FLAdManager$ImpressionEvent;


# instance fields
.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-instance v0, Lflipboard/service/FLAdManager$ImpressionEvent;

    const-string v1, "IMPRESSION"

    const-string v2, "impression"

    invoke-direct {v0, v1, v3, v2}, Lflipboard/service/FLAdManager$ImpressionEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    .line 48
    new-instance v0, Lflipboard/service/FLAdManager$ImpressionEvent;

    const-string v1, "SKIPPED"

    const-string v2, "skipped"

    invoke-direct {v0, v1, v4, v2}, Lflipboard/service/FLAdManager$ImpressionEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/FLAdManager$ImpressionEvent;->b:Lflipboard/service/FLAdManager$ImpressionEvent;

    .line 49
    new-instance v0, Lflipboard/service/FLAdManager$ImpressionEvent;

    const-string v1, "UNPLACED"

    const-string v2, "unplaced"

    invoke-direct {v0, v1, v5, v2}, Lflipboard/service/FLAdManager$ImpressionEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/service/FLAdManager$ImpressionEvent;

    sget-object v1, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/FLAdManager$ImpressionEvent;->b:Lflipboard/service/FLAdManager$ImpressionEvent;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/FLAdManager$ImpressionEvent;->c:Lflipboard/service/FLAdManager$ImpressionEvent;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/service/FLAdManager$ImpressionEvent;->e:[Lflipboard/service/FLAdManager$ImpressionEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput-object p3, p0, Lflipboard/service/FLAdManager$ImpressionEvent;->d:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/FLAdManager$ImpressionEvent;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lflipboard/service/FLAdManager$ImpressionEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager$ImpressionEvent;

    return-object v0
.end method

.method public static values()[Lflipboard/service/FLAdManager$ImpressionEvent;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lflipboard/service/FLAdManager$ImpressionEvent;->e:[Lflipboard/service/FLAdManager$ImpressionEvent;

    invoke-virtual {v0}, [Lflipboard/service/FLAdManager$ImpressionEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/FLAdManager$ImpressionEvent;

    return-object v0
.end method
