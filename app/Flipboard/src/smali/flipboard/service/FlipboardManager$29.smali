.class Lflipboard/service/FlipboardManager$29;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 2485
    iput-object p1, p0, Lflipboard/service/FlipboardManager$29;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2568
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading dynamicStrings: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2569
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2489
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2490
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 2491
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 2492
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2493
    iget-object v0, p0, Lflipboard/service/FlipboardManager$29;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v1, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2495
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$29;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile;

    iget-object v0, v0, Lflipboard/service/RemoteWatchedFile;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2496
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2498
    if-nez p3, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2499
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    .line 2500
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2502
    :try_start_1
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, v1}, Lflipboard/json/JSONParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2517
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 2559
    :goto_0
    iget-object v1, p0, Lflipboard/service/FlipboardManager$29;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v1, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Lflipboard/json/FLObject;)Lflipboard/json/FLObject;

    .line 2560
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2564
    return-void

    .line 2511
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "unwanted.loadDynamicStrings_parseFLObject_ArrayIndexOutOfBoundsException"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 2513
    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2514
    const/4 v0, 0x0

    .line 2517
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 2561
    :catch_1
    move-exception v0

    .line 2562
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "failed to parse dynamicStrings.json: %-E"

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v0, v3, v9

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2563
    throw v0

    .line 2517
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 2519
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    .line 2521
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v6

    .line 2522
    invoke-virtual {v6, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2524
    invoke-virtual {v6, v4}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 2541
    :goto_1
    new-instance v1, Lflipboard/service/FlipboardManager$29$1;

    const-string v4, "write-localized-dynamicstrings"

    invoke-direct {v1, p0, v4, v5, v0}, Lflipboard/service/FlipboardManager$29$1;-><init>(Lflipboard/service/FlipboardManager$29;Ljava/lang/String;Ljava/io/File;Lflipboard/json/FLObject;)V

    .line 2556
    invoke-virtual {v1}, Lflipboard/service/FlipboardManager$29$1;->start()V

    goto :goto_0

    .line 2525
    :cond_1
    invoke-virtual {v6, v1}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2527
    invoke-virtual {v6, v1}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    goto :goto_1

    .line 2531
    :cond_2
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_3

    const-string v0, "zh-Hans"

    .line 2532
    :goto_2
    invoke-virtual {v6, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2533
    invoke-virtual {v6, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    goto :goto_1

    .line 2531
    :cond_3
    const-string v0, "en"

    goto :goto_2

    .line 2536
    :cond_4
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "unable to find dynamic strings for %s: keys=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v1, 0x1

    invoke-virtual {v6}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v6

    aput-object v6, v7, v1

    invoke-virtual {v0, v4, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2537
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2572
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading dynamicStrings, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2573
    return-void
.end method
