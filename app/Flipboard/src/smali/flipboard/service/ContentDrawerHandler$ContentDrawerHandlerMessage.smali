.class public final enum Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;
.super Ljava/lang/Enum;
.source "ContentDrawerHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum b:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum c:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum d:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum e:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum f:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum g:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum h:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum i:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field public static final enum j:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

.field private static final synthetic k:[Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "TOPLEVEL_INVALIDATED"

    invoke-direct {v0, v1, v3}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "FAIL_PARSE_SECTIONS"

    invoke-direct {v0, v1, v4}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->b:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "FAIL_PARSE_SERVICES"

    invoke-direct {v0, v1, v5}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->c:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "FAIL_FETCH_SECTIONS"

    invoke-direct {v0, v1, v6}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->d:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "FAIL_FETCH_SERVICES"

    invoke-direct {v0, v1, v7}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->e:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "SERVICES_INVALIDATED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->f:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "SECTIONS_INVALIDATED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->g:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "SECTIONS_RESET"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->h:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "FLIPBOARD_ACCOUNT_UPDATED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->i:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    new-instance v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const-string v1, "FLIPBOARD_ACCOUNT_LOGGED_IN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->j:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    .line 41
    const/16 v0, 0xa

    new-array v0, v0, [Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->b:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->c:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->d:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->e:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->f:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->g:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->h:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->i:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->j:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->k:[Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->k:[Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0}, [Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    return-object v0
.end method
