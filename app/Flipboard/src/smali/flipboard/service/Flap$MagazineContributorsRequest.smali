.class Lflipboard/service/Flap$MagazineContributorsRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4802
    iput-object p1, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->b:Lflipboard/service/Flap;

    .line 4803
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4804
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$MagazineContributorsRequest;
    .locals 2

    .prologue
    .line 4808
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 4809
    iput-object p1, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->c:Ljava/lang/String;

    .line 4810
    iput-object p2, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4811
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4812
    return-object p0
.end method

.method protected final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4818
    iget-object v0, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/magazineContributors"

    iget-object v2, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "sectionid"

    aput-object v4, v3, v5

    iget-object v4, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4819
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v6, [Ljava/lang/Object;

    aput-object v0, v1, v5

    .line 4820
    iget-object v1, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->b:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$MagazineContributorsRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4821
    return-void
.end method
