.class public Lflipboard/service/Section;
.super Lflipboard/util/Observable;
.source "Section.java"

# interfaces
.implements Lflipboard/service/DatabaseRow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/util/Observable",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;",
        "Lflipboard/service/DatabaseRow;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field static final b:Lflipboard/util/Log;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:Ljava/lang/String;

.field public F:Z

.field public G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field private K:Z

.field private L:Lflipboard/util/ProcrastinatingTimerTask;

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:I

.field h:Z

.field public i:Z

.field public j:J

.field public k:Z

.field public l:J

.field public m:Z

.field public n:Z

.field public o:Ljava/lang/String;

.field public final p:Lflipboard/util/DuplicateOccurrenceLog;

.field public q:Lflipboard/objs/TOCSection;

.field public r:Lflipboard/service/Section$Meta;

.field public s:Lflipboard/objs/FeedItem;

.field public t:Lflipboard/objs/FeedItem;

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public v:Z

.field public w:Ljava/util/concurrent/atomic/AtomicBoolean;

.field x:Z

.field public y:Z

.field public z:Lflipboard/service/User;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "auth/flipboard/coverstories"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/service/Section;->a:Ljava/lang/String;

    .line 65
    const-string v0, "section"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    .line 68
    const-string v0, "highDensity"

    sput-object v0, Lflipboard/service/Section;->c:Ljava/lang/String;

    .line 69
    const-string v0, "lowDensity"

    sput-object v0, Lflipboard/service/Section;->d:Ljava/lang/String;

    .line 70
    const-string v0, "smartDensity"

    sput-object v0, Lflipboard/service/Section;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 234
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 235
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 236
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-object p2, v1, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    iput-object p2, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 237
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/model/FirstRunSection;->imageURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v1, "twitter"

    iput-object v1, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/model/FirstRunSection;->description:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    .line 240
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/model/FirstRunSection;->keywords:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->j:Ljava/lang/String;

    .line 241
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 242
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/model/FirstRunSection;->feedType:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    .line 243
    invoke-virtual {p0}, Lflipboard/service/Section;->w()V

    .line 244
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 245
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/ConfigSection;)V
    .locals 2

    .prologue
    .line 260
    invoke-direct {p0, p1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/ContentDrawerListItemSection;)V

    .line 261
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v1, "twitter"

    iput-object v1, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 264
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ConfigSection;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->j:Ljava/lang/String;

    .line 265
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 266
    return-void
.end method

.method private constructor <init>(Lflipboard/objs/ContentDrawerListItemSection;)V
    .locals 2

    .prologue
    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 197
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->p:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 198
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->s:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    .line 199
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->v:I

    iput v1, v0, Lflipboard/objs/TOCSection;->B:I

    .line 200
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->bQ:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->bP:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, p1, Lflipboard/objs/ContentDrawerListItemSection;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/ContentDrawerListItemSection;->r:Ljava/lang/String;

    :goto_0
    iput-object v0, v1, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->bS:Z

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->v:Z

    .line 204
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->bR:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->g:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->y:Z

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 207
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->w:Z

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->D:Z

    .line 208
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, p1, Lflipboard/objs/ContentDrawerListItemSection;->x:Z

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->E:Z

    .line 209
    iget-object v0, p1, Lflipboard/objs/ContentDrawerListItemSection;->o:Ljava/lang/String;

    invoke-direct {p0, v0}, Lflipboard/service/Section;->c(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 211
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 291
    iget-object v1, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    move-object v0, p0

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 292
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 293
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    iput-boolean v5, p0, Lflipboard/service/Section;->x:Z

    .line 295
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 296
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Create synthetic feed with single item: EOF = true\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 297
    iput-boolean v5, p0, Lflipboard/service/Section;->v:Z

    .line 298
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 299
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 306
    iget-object v1, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iget-object v0, p2, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v4

    :goto_0
    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 307
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 308
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 309
    iget-object v2, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 310
    iget-object v2, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto :goto_1

    .line 306
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 313
    :cond_2
    iput-boolean v6, p0, Lflipboard/service/Section;->x:Z

    .line 314
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 315
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Create fake section from album items: EOF = true\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 316
    iput-boolean v6, p0, Lflipboard/service/Section;->v:Z

    .line 317
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/FeedSectionLink;)V
    .locals 3

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 217
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 219
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {p1}, Lflipboard/objs/FeedSectionLink;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, p1, Lflipboard/objs/FeedSectionLink;->i:Z

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 223
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, p1, Lflipboard/objs/FeedSectionLink;->o:Z

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->N:Z

    .line 224
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    .line 225
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->s:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    .line 227
    invoke-virtual {p0}, Lflipboard/service/Section;->w()V

    .line 228
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lflipboard/service/Section;->c(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/SearchResultItem;)V
    .locals 2

    .prologue
    .line 182
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 183
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/TOCSection;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 154
    invoke-direct {p0}, Lflipboard/util/Observable;-><init>()V

    .line 85
    iput-boolean v0, p0, Lflipboard/service/Section;->k:Z

    .line 93
    new-instance v1, Lflipboard/util/DuplicateOccurrenceLog;

    invoke-direct {v1}, Lflipboard/util/DuplicateOccurrenceLog;-><init>()V

    iput-object v1, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    .line 155
    if-nez p1, :cond_0

    new-instance p1, Lflipboard/objs/TOCSection;

    invoke-direct {p1}, Lflipboard/objs/TOCSection;-><init>()V

    :cond_0
    iput-object p1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    .line 156
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v1, p0, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 157
    new-instance v1, Lflipboard/service/Section$Meta;

    invoke-direct {v1}, Lflipboard/service/Section$Meta;-><init>()V

    iput-object v1, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    .line 158
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 160
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 162
    :cond_1
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v2, "googlereader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    iput-boolean v0, p0, Lflipboard/service/Section;->A:Z

    .line 163
    return-void

    .line 162
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lflipboard/objs/UserService;)V
    .locals 4

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 250
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, p1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 251
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, p1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 252
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {p1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 254
    iget-object v0, p1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lflipboard/service/Section;->c(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 256
    return-void
.end method

.method public constructor <init>(Lflipboard/service/Account;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 270
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 271
    iget-object v1, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    .line 272
    if-eqz v1, :cond_1

    .line 273
    iget-object v2, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    :goto_0
    iput-object v0, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 274
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 275
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-boolean v3, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 283
    :goto_1
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 284
    return-void

    .line 273
    :cond_0
    iget-object v0, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 280
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-boolean v3, v0, Lflipboard/objs/TOCSection;->s:Z

    goto :goto_1
.end method

.method public constructor <init>(Lflipboard/service/DatabaseHandler;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 325
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 328
    :try_start_0
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lflipboard/service/DatabaseHandler;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflipboard/service/Section;->f:I

    .line 330
    const-string v0, "descriptor"

    invoke-virtual {p1, v0}, Lflipboard/service/DatabaseHandler;->d(Ljava/lang/String;)[B

    move-result-object v3

    .line 331
    if-eqz v3, :cond_5

    move v0, v1

    .line 332
    :goto_0
    if-eqz v0, :cond_6

    .line 333
    new-instance v4, Lflipboard/json/JSONParser;

    invoke-direct {v4, v3}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v4}, Lflipboard/json/JSONParser;->w()Lflipboard/objs/TOCSection;

    move-result-object v3

    iput-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    .line 334
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 335
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 337
    :cond_0
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-direct {p0, v3}, Lflipboard/service/Section;->c(Ljava/lang/String;)V

    .line 347
    :goto_1
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Parsed section "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", private: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v5, v5, Lflipboard/objs/TOCSection;->s:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 348
    const-string v3, "tocItem"

    invoke-virtual {p1, v3}, Lflipboard/service/DatabaseHandler;->d(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 349
    if-eqz v3, :cond_1

    .line 351
    :try_start_1
    new-instance v4, Lflipboard/json/JSONParser;

    invoke-direct {v4, v3}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v4}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v3

    invoke-virtual {p0, v3}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 356
    :cond_1
    :goto_2
    :try_start_2
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 357
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 359
    :cond_2
    const-string v3, "pos"

    invoke-virtual {p1, v3}, Lflipboard/service/DatabaseHandler;->c(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lflipboard/service/Section;->g:I

    .line 362
    const-string v3, "metaData"

    invoke-virtual {p1, v3}, Lflipboard/service/DatabaseHandler;->d(Ljava/lang/String;)[B

    move-result-object v3

    .line 363
    if-eqz v0, :cond_7

    .line 364
    if-eqz v3, :cond_3

    .line 365
    new-instance v4, Lflipboard/json/JSONParser;

    invoke-direct {v4, v3}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v4}, Lflipboard/json/JSONParser;->D()Lflipboard/service/Section$Meta;

    move-result-object v3

    iput-object v3, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    .line 369
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->k:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 370
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v4, v4, Lflipboard/service/Section$Meta;->k:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    .line 384
    :cond_3
    :goto_3
    if-nez v0, :cond_4

    .line 387
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lflipboard/service/Section$Meta;->a:Z

    .line 392
    :cond_4
    :goto_4
    return-void

    :cond_5
    move v0, v2

    .line 331
    goto/16 :goto_0

    .line 340
    :cond_6
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v4, "sectionId"

    invoke-virtual {p1, v4}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 341
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v5, "title"

    invoke-virtual {p1, v5}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    iput-object v5, v3, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 342
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v4, "image"

    invoke-virtual {p1, v4}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 343
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v4, "private"

    invoke-virtual {p1, v4}, Lflipboard/service/DatabaseHandler;->e(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lflipboard/objs/TOCSection;->s:Z

    .line 344
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v4, "unreadRemoteId"

    invoke-virtual {p1, v4}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    .line 345
    const-string v3, "service"

    invoke-virtual {p1, v3}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lflipboard/service/Section;->c(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 389
    :catch_0
    move-exception v0

    .line 390
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    goto :goto_4

    .line 353
    :catch_1
    move-exception v3

    :try_start_3
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Lost tocItem in section %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v7, v7, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 375
    :cond_7
    new-instance v4, Lflipboard/service/MetaData;

    invoke-direct {v4, p0}, Lflipboard/service/MetaData;-><init>(Lflipboard/service/DatabaseRow;)V

    .line 376
    invoke-virtual {v4, v3}, Lflipboard/service/MetaData;->a([B)V

    .line 377
    const-string v3, "isPlaceHolder"

    invoke-virtual {v4}, Lflipboard/service/MetaData;->a()Lflipboard/json/FLObject;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 378
    iget-object v3, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lflipboard/service/Section$Meta;->b:Z

    .line 379
    iget-object v3, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lflipboard/service/Section$Meta;->a:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_3
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    .line 173
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-object p1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-object p2, v1, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    iput-object p2, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v0, p4}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-boolean p5, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 177
    invoke-direct {p0, p3}, Lflipboard/service/Section;->c(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/util/DuplicateOccurrenceLog;->b:Ljava/lang/String;

    .line 179
    return-void
.end method

.method private G()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1653
    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/service/Section;->D:Z

    .line 1654
    iget-object v1, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 1657
    iget-object v1, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    move-object v2, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1659
    iget-object v4, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v5, "sectionCover"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v1, :cond_1

    move-object v1, v0

    .line 1663
    :cond_1
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 1664
    if-nez v2, :cond_2

    .line 1665
    iget-object v0, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    move-object v2, v0

    goto :goto_0

    .line 1666
    :cond_2
    iget-object v4, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v0, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/Section;->D:Z

    .line 1672
    :cond_4
    if-eqz v1, :cond_5

    .line 1673
    iput-object v1, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 1676
    :cond_5
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1092
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 1093
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "enable_scrolling"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1094
    sget-object v2, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v2, :cond_1

    if-eqz v1, :cond_1

    .line 1095
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/GenericFragmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1096
    const-string v1, "fragment_type"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1097
    const-string v1, "extra_section_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1101
    :goto_0
    const-string v1, "sid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1102
    const-string v1, "extra_intent_start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1103
    if-eqz p2, :cond_0

    .line 1104
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1105
    :cond_0
    return-object v0

    .line 1099
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/SectionTabletActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Lflipboard/objs/FeedItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 869
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->O()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 871
    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 895
    :cond_0
    :goto_0
    return-object v0

    .line 873
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 874
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v1

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 875
    invoke-direct {p0, v0, p2}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 876
    if-eqz v0, :cond_2

    goto :goto_0

    .line 882
    :cond_3
    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 883
    goto :goto_0

    .line 884
    :cond_4
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 885
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    goto :goto_0

    .line 886
    :cond_5
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 887
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    goto :goto_0

    .line 888
    :cond_6
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 889
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 890
    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    .line 895
    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1730
    if-nez p0, :cond_0

    move-object v0, v1

    .line 1745
    :goto_0
    return-object v0

    .line 1734
    :cond_0
    invoke-interface {p0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 1735
    check-cast p0, Lflipboard/objs/ConfigSection;

    .line 1736
    new-instance v0, Lflipboard/service/Section;

    invoke-direct {v0, p0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/ConfigSection;)V

    goto :goto_0

    .line 1737
    :cond_1
    invoke-interface {p0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    move-object v0, p0

    check-cast v0, Lflipboard/objs/SectionListItem;

    iget-object v0, v0, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v2, "feed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1738
    check-cast p0, Lflipboard/objs/SectionListItem;

    .line 1739
    new-instance v0, Lflipboard/service/Section;

    invoke-direct {v0, p0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/ContentDrawerListItemSection;)V

    goto :goto_0

    .line 1740
    :cond_2
    invoke-interface {p0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_3

    .line 1741
    check-cast p0, Lflipboard/objs/SearchResult;

    .line 1742
    new-instance v0, Lflipboard/service/Section;

    invoke-direct {v0, p0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/ContentDrawerListItemSection;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 1745
    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lflipboard/service/Section;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lflipboard/service/Section;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lflipboard/service/Section;->x:Z

    return p1
.end method

.method static synthetic b(Lflipboard/service/Section;)Lflipboard/service/User;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lflipboard/objs/ContentDrawerListItem;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1750
    if-nez p0, :cond_0

    .line 1765
    :goto_0
    return-object v1

    .line 1755
    :cond_0
    invoke-interface {p0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 1756
    check-cast p0, Lflipboard/objs/ConfigSection;

    iget-object v0, p0, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 1765
    goto :goto_0

    .line 1758
    :cond_1
    invoke-interface {p0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    move-object v0, p0

    .line 1759
    check-cast v0, Lflipboard/objs/SectionListItem;

    iget-object v0, v0, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lflipboard/objs/SectionListItem;

    iget-object v0, v0, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v2, "feed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1760
    check-cast p0, Lflipboard/objs/SectionListItem;

    iget-object v0, p0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1762
    :cond_2
    invoke-interface {p0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_3

    .line 1763
    check-cast p0, Lflipboard/objs/SearchResult;

    iget-object v0, p0, Lflipboard/objs/SearchResult;->p:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lflipboard/service/Section;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lflipboard/service/Section;->E:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lflipboard/service/Section;)Ljava/util/List;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 444
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    if-eqz p1, :cond_0

    move-object v0, p1

    :goto_0
    iput-object v0, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 445
    if-eqz p1, :cond_1

    const-string v0, "googlereader"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lflipboard/service/Section;->A:Z

    .line 446
    return-void

    .line 444
    :cond_0
    const-string v0, "twitter"

    goto :goto_0

    .line 445
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d(Lflipboard/objs/FeedItem;)Lflipboard/objs/SidebarGroup;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2001
    const/4 v1, 0x0

    .line 2003
    iget-object v2, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v2, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 2004
    iput-boolean v0, p0, Lflipboard/service/Section;->m:Z

    move v2, v0

    .line 2005
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    if-nez v1, :cond_0

    .line 2006
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 2007
    iget-object v4, p1, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    iget-object v5, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2010
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    .line 2011
    goto :goto_0

    .line 2012
    :cond_0
    if-eqz v1, :cond_1

    .line 2013
    iget-object v0, v1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2014
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    iget-object v2, v1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2015
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2016
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    iput-object v0, v1, Lflipboard/objs/SidebarGroup;->i:Ljava/lang/String;

    .line 2019
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    return-object v0
.end method

.method static synthetic e(Lflipboard/service/Section;)Lflipboard/service/Section$Meta;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    return-object v0
.end method

.method static synthetic f(Lflipboard/service/Section;)Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/Section;->K:Z

    return v0
.end method

.method static synthetic g(Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lflipboard/service/Section;->G()V

    return-void
.end method

.method static synthetic h(Lflipboard/service/Section;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/service/Section;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lflipboard/service/Section;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lflipboard/service/Section;->y:Z

    return v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 1798
    invoke-virtual {p0}, Lflipboard/service/Section;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    .line 1800
    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1801
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1804
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Z
    .locals 2

    .prologue
    .line 1808
    invoke-virtual {p0}, Lflipboard/service/Section;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "flipboard"

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Z
    .locals 5

    .prologue
    .line 1815
    invoke-virtual {p0}, Lflipboard/service/Section;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1816
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 1817
    if-eqz v0, :cond_2

    .line 1818
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 1819
    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$RenderHints;

    .line 1820
    const-string v3, "profile"

    iget-object v4, v0, Lflipboard/objs/SidebarGroup$RenderHints;->m:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "pageboxProfile"

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821
    const/4 v0, 0x1

    .line 1827
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 1838
    iget-object v0, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1079
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 1080
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "service"

    iget-object v2, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1081
    const-string v1, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginTOC"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1086
    :goto_1
    return-object v0

    .line 1079
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1083
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lflipboard/service/Section;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lflipboard/objs/FeedItem;
    .locals 3

    .prologue
    .line 848
    invoke-virtual {p0}, Lflipboard/service/Section;->m()Ljava/util/List;

    .line 849
    const/4 v0, 0x0

    .line 850
    iget-object v1, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 851
    invoke-direct {p0, v0, p1}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 852
    if-eqz v0, :cond_0

    .line 853
    :cond_1
    if-nez v0, :cond_3

    iget-object v1, p0, Lflipboard/service/Section;->J:Ljava/util/Set;

    if-eqz v1, :cond_3

    .line 857
    iget-object v1, p0, Lflipboard/service/Section;->J:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 858
    invoke-direct {p0, v0, p1}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 859
    if-eqz v0, :cond_2

    .line 860
    :cond_3
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->O:Ljava/lang/String;

    .line 97
    if-nez v0, :cond_0

    .line 98
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->k()Lflipboard/objs/UserState;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    .line 100
    :cond_0
    return-object v0
.end method

.method public final a(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 529
    :goto_0
    iput-object p1, p0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    .line 530
    if-eqz p1, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "group"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    move-object p1, v0

    goto :goto_0

    .line 536
    :cond_0
    return-void
.end method

.method final a(Lflipboard/objs/FeedItem;Lflipboard/service/Section$UpdateObserver;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1967
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 1968
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    const-string v1, "sidebar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1969
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 1970
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->bM:Lflipboard/objs/FeedSectionLink;

    iput-object v1, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 1971
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->O:Lflipboard/objs/Image;

    iput-object v1, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    .line 1972
    iput-boolean v3, p0, Lflipboard/service/Section;->m:Z

    .line 1997
    :cond_0
    :goto_0
    return-void

    .line 1973
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    const-string v1, "group"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1974
    invoke-direct {p0, p1}, Lflipboard/service/Section;->d(Lflipboard/objs/FeedItem;)Lflipboard/objs/SidebarGroup;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1975
    iget-object v0, p0, Lflipboard/service/Section;->M:Ljava/util/List;

    if-nez v0, :cond_2

    .line 1976
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/Section;->M:Ljava/util/List;

    .line 1978
    :cond_2
    iget-object v0, p0, Lflipboard/service/Section;->M:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1980
    :cond_3
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    const-string v1, "EOS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lflipboard/service/Section;->M:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1982
    iget-object v0, p0, Lflipboard/service/Section;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1984
    invoke-direct {p0, v0}, Lflipboard/service/Section;->d(Lflipboard/objs/FeedItem;)Lflipboard/objs/SidebarGroup;

    goto :goto_1

    .line 1987
    :cond_4
    iput-object v4, p0, Lflipboard/service/Section;->M:Ljava/util/List;

    .line 1988
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iput-boolean v2, v0, Lflipboard/service/Section$Meta;->a:Z

    .line 1989
    invoke-virtual {p0}, Lflipboard/service/Section;->w()V

    .line 1990
    sget-object v0, Lflipboard/service/Section$Message;->g:Lflipboard/service/Section$Message;

    invoke-virtual {p0, v0, v4}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1991
    iput-boolean v2, p0, Lflipboard/service/Section;->m:Z

    .line 1992
    iget-boolean v0, p0, Lflipboard/service/Section;->n:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1993
    iput-boolean v3, p0, Lflipboard/service/Section;->n:Z

    .line 1994
    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    invoke-virtual {p2}, Lflipboard/service/Section$UpdateObserver;->a()Lflipboard/json/FLObject;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lflipboard/objs/Magazine;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1935
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 1936
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 1937
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, p1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    .line 1939
    iget-object v2, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 1940
    iget-object v0, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iput-object v0, v2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 1941
    iget-object v0, p1, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    iput-object v0, v2, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    .line 1942
    invoke-virtual {p0, v2}, Lflipboard/service/Section;->b(Lflipboard/objs/FeedItem;)V

    .line 1943
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1944
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1945
    invoke-virtual {v0, v2}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1946
    iget-object v1, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 1947
    iget-object v1, p1, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    .line 1951
    :cond_0
    invoke-virtual {p0, v4}, Lflipboard/service/Section;->e(Z)V

    .line 1952
    invoke-virtual {p0}, Lflipboard/service/Section;->u()V

    .line 1953
    invoke-virtual {p0, v5}, Lflipboard/service/Section;->a(Ljava/lang/Runnable;)V

    .line 1954
    sget-object v0, Lflipboard/service/Section$Message;->b:Lflipboard/service/Section$Message;

    invoke-virtual {p0, v0, v5}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1956
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 1957
    const-string v1, "changes"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1958
    const-string v1, "refresh"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1959
    const-string v1, "EOF"

    invoke-virtual {p0}, Lflipboard/service/Section;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1960
    const-string v1, "strategy"

    iget-object v2, p0, Lflipboard/service/Section;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->b(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 1962
    sget-object v1, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1963
    return-void

    .line 1943
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 5

    .prologue
    .line 2024
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflipboard/service/Section$4;

    invoke-direct {v2, p0, p1}, Lflipboard/service/Section$4;-><init>(Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V

    new-instance v3, Lflipboard/service/Flap$MagazineContributorsRequest;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v3, v0, v4}, Lflipboard/service/Flap$MagazineContributorsRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v1, v2}, Lflipboard/service/Flap$MagazineContributorsRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$MagazineContributorsRequest;

    .line 2048
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1221
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 1222
    if-nez v0, :cond_1

    .line 1224
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "Section:pickTOCItem"

    new-instance v2, Lflipboard/service/Section$1;

    invoke-direct {v2, p0, p1}, Lflipboard/service/Section$1;-><init>(Lflipboard/service/Section;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1254
    :cond_0
    :goto_0
    return-void

    .line 1234
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1235
    iget-object v2, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iget v3, p0, Lflipboard/service/Section;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1236
    :cond_3
    invoke-virtual {p0, v0, p1}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 1246
    :cond_4
    iget-object v0, p0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 1247
    invoke-virtual {p0, v4}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;)V

    .line 1248
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/service/Section;->e(Z)V

    .line 1249
    if-eqz p1, :cond_5

    .line 1250
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 1252
    :cond_5
    sget-object v0, Lflipboard/service/Section$Message;->b:Lflipboard/service/Section$Message;

    invoke-virtual {p0, v0, v4}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 762
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 763
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "setItems() called\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 764
    if-eqz p1, :cond_0

    .line 765
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "\titems.size(): "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 769
    :goto_0
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 770
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "\tSection.items.size(): "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 775
    :goto_1
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/service/Section;->b(Ljava/util/List;)V

    .line 776
    if-eqz p1, :cond_2

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    :goto_2
    iput-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 777
    invoke-direct {p0}, Lflipboard/service/Section;->G()V

    .line 778
    return-void

    .line 767
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Setting null items"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_0

    .line 772
    :cond_1
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "\tSection.items is null: "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_1

    .line 776
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->b:Z

    if-eq v0, p1, :cond_0

    .line 594
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iput-boolean p1, v0, Lflipboard/service/Section$Meta;->b:Z

    .line 595
    invoke-virtual {p0}, Lflipboard/service/Section;->w()V

    .line 597
    :cond_0
    return-void
.end method

.method final a(Lflipboard/objs/FeedItem;Ljava/lang/Runnable;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1258
    .line 1259
    if-eqz p1, :cond_0

    iget-object v2, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "sectionCover"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "sidebar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 1260
    :cond_0
    sget-object v2, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 1289
    :cond_1
    :goto_0
    return v0

    .line 1261
    :cond_2
    invoke-virtual {p0, p1}, Lflipboard/service/Section;->c(Lflipboard/objs/FeedItem;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1262
    sget-object v2, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    goto :goto_0

    .line 1263
    :cond_3
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->O()Z

    move-result v2

    if-eqz v2, :cond_4

    move v1, v0

    .line 1264
    :goto_1
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    if-nez v0, :cond_1

    .line 1265
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {p0, v0, p2}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Ljava/lang/Runnable;)Z

    move-result v2

    .line 1264
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    .line 1270
    :cond_4
    invoke-static {p1}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    .line 1271
    if-nez v2, :cond_5

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1272
    :cond_5
    iget-object v2, p0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    .line 1273
    invoke-virtual {p0, p1}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;)V

    .line 1277
    if-eqz v2, :cond_6

    invoke-virtual {v2, p1}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1278
    :cond_6
    if-eqz p2, :cond_7

    .line 1279
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 1282
    :cond_7
    sget-object v3, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v4, v3, v0

    if-nez v2, :cond_9

    const/4 v0, 0x0

    :goto_2
    aput-object v0, v3, v1

    const/4 v0, 0x2

    iget-object v2, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v2, v3, v0

    .line 1283
    sget-object v0, Lflipboard/service/Section$Message;->b:Lflipboard/service/Section$Message;

    iget-object v2, p0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {p0, v0, v2}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1284
    invoke-virtual {p0, v1}, Lflipboard/service/Section;->e(Z)V

    :cond_8
    move v0, v1

    .line 1286
    goto :goto_0

    .line 1282
    :cond_9
    iget-object v0, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    goto :goto_2
.end method

.method public final a(Lflipboard/objs/FeedSectionLink;)Z
    .locals 1

    .prologue
    .line 1016
    if-eqz p1, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/Section;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lflipboard/service/Section;)Z
    .locals 2

    .prologue
    .line 1609
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Lflipboard/service/User;)Z
    .locals 2

    .prologue
    .line 1854
    iget-object v0, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZLflipboard/util/Callback;ZLandroid/os/Bundle;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lflipboard/util/Callback",
            "<",
            "Ljava/lang/Object;",
            ">;Z",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1145
    iget-object v1, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v1}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1146
    iget-object v1, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "fetchNew()"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "FlipboardManager.instance.feedsFrozen(): "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "NetworkManager.instance.onDemand(): "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "isInteractive: "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "isLocal(): "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    iget-boolean v2, p0, Lflipboard/service/Section;->v:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1147
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1169
    :cond_0
    :goto_0
    return v0

    .line 1150
    :cond_1
    if-nez p1, :cond_2

    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1155
    :cond_2
    iget-boolean v1, p0, Lflipboard/service/Section;->v:Z

    if-nez v1, :cond_0

    .line 1156
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v2

    invoke-virtual {v1, v2, p1, p3}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v1

    .line 1157
    iget-object v2, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v2}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1158
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, p4}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1159
    if-eqz p2, :cond_3

    .line 1160
    iput-object p2, v1, Lflipboard/service/Flap$UpdateRequest;->j:Lflipboard/util/Callback;

    .line 1162
    :cond_3
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "fetchNew request started"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n\tisInteractive: "

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n\tsuppressUsage: "

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1163
    invoke-virtual {v1}, Lflipboard/service/Flap$UpdateRequest;->c()V

    .line 1164
    const/4 v0, 0x1

    goto :goto_0

    .line 1166
    :cond_4
    iget-object v1, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "fetchNew cancelled because we\'re already fetching"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1185
    iget-object v1, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v1}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1186
    iget-object v1, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "fetchMore()"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "FlipboardManager.instance.feedsFrozen(): "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "frozenForLoadMore: "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    iget-boolean v2, p0, Lflipboard/service/Section;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "actionRefresh: "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    iget-boolean v2, p0, Lflipboard/service/Section;->F:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "NetworkManager.instance.onDemand(): "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "isInteractive: "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n\t"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "isLocal(): "

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    iget-boolean v2, p0, Lflipboard/service/Section;->v:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1187
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lflipboard/service/Section;->i:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lflipboard/service/Section;->F:Z

    if-eqz v1, :cond_1

    .line 1209
    :cond_0
    :goto_0
    return v0

    .line 1190
    :cond_1
    if-nez p1, :cond_2

    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1193
    :cond_2
    iget-boolean v1, p0, Lflipboard/service/Section;->v:Z

    if-nez v1, :cond_3

    .line 1194
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v1

    .line 1195
    if-nez p2, :cond_7

    .line 1196
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->af()Ljava/lang/String;

    move-result-object v0

    .line 1201
    :goto_1
    iget-object v2, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v2}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1202
    invoke-virtual {v1, p0, v0, p3}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1203
    iget-object v2, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v3, "fetchMore request started"

    invoke-virtual {v2, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v2

    const-string v3, "\n\tisInteractive: "

    invoke-virtual {v2, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v2

    const-string v3, "\n\tpageKey: "

    invoke-virtual {v2, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v2

    if-eqz v0, :cond_5

    :goto_2
    invoke-virtual {v2, v0}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1204
    invoke-virtual {v1}, Lflipboard/service/Flap$UpdateRequest;->c()V

    .line 1209
    :cond_3
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1196
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1203
    :cond_5
    const-string v0, "null"

    goto :goto_2

    .line 1206
    :cond_6
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "fetchMore request cancelled, because we\'re already fetching"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_3

    :cond_7
    move-object v0, p2

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 454
    invoke-virtual {p0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 469
    :goto_0
    return-object v0

    .line 459
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    goto :goto_0

    .line 464
    :cond_1
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 465
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    goto :goto_0

    .line 469
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 540
    iput-object p1, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 541
    iget-object v0, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "sectionCover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    iget-object v0, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    const-string v1, "sectionCover"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 544
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    if-nez v0, :cond_1

    .line 545
    iget-object v0, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    .line 547
    :cond_1
    return-void
.end method

.method final b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 786
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 787
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 788
    iget-object v1, p0, Lflipboard/service/Section;->J:Ljava/util/Set;

    .line 789
    if-eqz v1, :cond_0

    .line 790
    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 792
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 793
    iput-object v0, p0, Lflipboard/service/Section;->J:Ljava/util/Set;

    .line 795
    :cond_1
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 601
    iget-boolean v0, p0, Lflipboard/service/Section;->C:Z

    if-eq v0, p1, :cond_1

    .line 602
    if-nez p1, :cond_0

    .line 605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/Section;->B:Z

    .line 607
    :cond_0
    iput-boolean p1, p0, Lflipboard/service/Section;->C:Z

    .line 609
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1005
    iget-object v2, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v1

    .line 1006
    :goto_0
    if-nez v2, :cond_4

    const-string v3, "auth/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1008
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "auth/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1009
    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 1011
    :cond_2
    :goto_1
    return v0

    :cond_3
    move v2, v0

    .line 1005
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    .line 476
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lflipboard/objs/FeedItem;)Z
    .locals 2

    .prologue
    .line 1614
    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final c(Z)Z
    .locals 2

    .prologue
    .line 1062
    iget-object v0, p0, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1063
    if-nez p1, :cond_0

    .line 1064
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->b()V

    .line 1066
    :cond_0
    sget-object v0, Lflipboard/service/Section$Message;->a:Lflipboard/service/Section$Message;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1067
    const/4 v0, 0x1

    .line 1069
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 950
    iget v0, p0, Lflipboard/service/Section;->f:I

    return v0
.end method

.method public final d(Z)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1134
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0, v1}, Lflipboard/service/Section;->a(ZLflipboard/util/Callback;ZLandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 955
    const-string v0, "sections"

    return-object v0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 1619
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/service/Section;->j:J

    .line 1620
    iput-boolean p1, p0, Lflipboard/service/Section;->y:Z

    .line 1621
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    const-string v1, "public"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lflipboard/service/Section;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/service/Section;->K:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 612
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_1

    .line 614
    invoke-virtual {p0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 616
    :goto_0
    return v0

    .line 614
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 616
    :cond_1
    iget-boolean v0, p0, Lflipboard/service/Section;->C:Z

    goto :goto_0
.end method

.method public final j()Z
    .locals 4

    .prologue
    .line 632
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 633
    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 634
    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$RenderHints;

    .line 635
    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v3, "sidebar"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 636
    const/4 v0, 0x1

    .line 641
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 698
    iget-boolean v0, p0, Lflipboard/service/Section;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/service/Section;->i:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 799
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-nez v0, :cond_1

    .line 800
    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/service/User;->a(Lflipboard/service/Section;)V

    .line 801
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 802
    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "sidebar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 803
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section$UpdateObserver;)V

    goto :goto_0

    .line 807
    :cond_1
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    return-object v0
.end method

.method public final n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 821
    iput-object v2, p0, Lflipboard/service/Section;->J:Ljava/util/Set;

    .line 823
    iget-boolean v0, p0, Lflipboard/service/Section;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 831
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/Section;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 832
    iget-boolean v0, p0, Lflipboard/service/Section;->C:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 836
    :goto_1
    iget-object v1, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v1, v0, :cond_0

    .line 837
    invoke-virtual {p0}, Lflipboard/service/Section;->u()V

    .line 838
    invoke-virtual {p0, v2}, Lflipboard/service/Section;->a(Ljava/util/List;)V

    .line 839
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/Section;->x:Z

    .line 840
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 841
    iget-object v0, p0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "unloadItems(): EOF = false\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_0

    .line 832
    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    .line 834
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final o()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 933
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 936
    :cond_0
    iput-object v3, p0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 937
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 939
    invoke-virtual {p0, v3}, Lflipboard/service/Section;->a(Ljava/lang/Runnable;)V

    .line 942
    invoke-virtual {p0}, Lflipboard/service/Section;->p()Lflipboard/service/User;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lflipboard/service/User;->a(Lflipboard/service/Section;Z)V

    .line 943
    return-void
.end method

.method final p()Lflipboard/service/User;
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lflipboard/service/Section;->z:Lflipboard/service/User;

    if-nez v0, :cond_0

    .line 965
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v0, p0, Lflipboard/service/Section;->z:Lflipboard/service/User;

    .line 967
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->z:Lflipboard/service/User;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 972
    iget-boolean v0, p0, Lflipboard/service/Section;->v:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/service/Section;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 977
    iget v0, p0, Lflipboard/service/Section;->f:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 981
    invoke-virtual {p0}, Lflipboard/service/Section;->r()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v0, v0, Lflipboard/objs/TOCSection;->N:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 986
    const-string v1, "auth/flipboard/coverstories"

    iget-object v2, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    .line 992
    :goto_0
    return v0

    .line 988
    :cond_0
    const-string v1, "auth/flipboard/coverstories"

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 989
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    const-string v2, "auth/flipboard/coverstories"

    iput-object v2, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    goto :goto_0

    .line 992
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1711
    const-string v1, "Section[id=%d, pos=%d: service=%s, sectionId=%s, remoteId=%s, unreadRemoteId=%s, title=%s, nitems=%d, meta=%s, observers=%s]"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lflipboard/service/Section;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget v4, p0, Lflipboard/service/Section;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/16 v0, 0x8

    iget-object v3, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    aput-object v3, v2, v0

    const/16 v0, 0x9

    iget-object v3, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1637
    iget-boolean v0, p0, Lflipboard/service/Section;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->a:Z

    if-eqz v0, :cond_1

    .line 1638
    :cond_0
    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v3

    .line 1639
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "Section:saveChanges"

    new-instance v2, Lflipboard/service/Section$2;

    invoke-direct {v2, p0}, Lflipboard/service/Section$2;-><init>(Lflipboard/service/Section;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1644
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iput-boolean v3, v0, Lflipboard/service/Section$Meta;->a:Z

    .line 1646
    :cond_1
    return-void
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 1680
    invoke-virtual {p0}, Lflipboard/service/Section;->m()Ljava/util/List;

    .line 1681
    iget-boolean v0, p0, Lflipboard/service/Section;->D:Z

    return v0
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 1692
    iget-object v0, p0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/Section$Meta;->a:Z

    .line 1693
    iget-object v0, p0, Lflipboard/service/Section;->L:Lflipboard/util/ProcrastinatingTimerTask;

    if-nez v0, :cond_0

    .line 1694
    new-instance v0, Lflipboard/service/Section$3;

    invoke-direct {v0, p0}, Lflipboard/service/Section$3;-><init>(Lflipboard/service/Section;)V

    iput-object v0, p0, Lflipboard/service/Section;->L:Lflipboard/util/ProcrastinatingTimerTask;

    .line 1706
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section;->L:Lflipboard/util/ProcrastinatingTimerTask;

    invoke-virtual {v0}, Lflipboard/util/ProcrastinatingTimerTask;->b()V

    .line 1707
    return-void
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 1770
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    const-string v1, "magazine"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    const-string v1, "topic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 1793
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    const-string v1, "profile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
