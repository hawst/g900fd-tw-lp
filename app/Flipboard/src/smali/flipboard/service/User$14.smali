.class Lflipboard/service/User$14;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/UserInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/Flap$TypedResultObserver;

.field final synthetic b:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Lflipboard/service/Flap$TypedResultObserver;)V
    .locals 0

    .prologue
    .line 1056
    iput-object p1, p0, Lflipboard/service/User$14;->b:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$14;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1056
    check-cast p1, Lflipboard/objs/UserInfo;

    invoke-virtual {p1}, Lflipboard/objs/UserInfo;->a()Lflipboard/objs/UserInfo;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/UserInfo;->a()Lflipboard/objs/UserInfo;

    move-result-object v0

    iget-object v2, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/User$14;->b:Lflipboard/service/User;

    iget-object v2, v1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    iget-object v3, v1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-virtual {v0, v2, v3}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/User$14;->b:Lflipboard/service/User;

    iget-object v2, v1, Lflipboard/objs/UserInfo;->n:Ljava/util/List;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/User$14;->b:Lflipboard/service/User;

    new-instance v2, Lflipboard/objs/UserState;

    invoke-direct {v2, v1}, Lflipboard/objs/UserState;-><init>(Lflipboard/objs/UserInfo;)V

    invoke-static {v0, v2}, Lflipboard/service/User;->c(Lflipboard/service/User;Lflipboard/objs/UserState;)V

    :cond_1
    iget-object v0, p0, Lflipboard/service/User$14;->a:Lflipboard/service/Flap$TypedResultObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/User$14;->a:Lflipboard/service/Flap$TypedResultObserver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1073
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1074
    iget-object v0, p0, Lflipboard/service/User$14;->a:Lflipboard/service/Flap$TypedResultObserver;

    if-eqz v0, :cond_0

    .line 1075
    iget-object v0, p0, Lflipboard/service/User$14;->a:Lflipboard/service/Flap$TypedResultObserver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    .line 1077
    :cond_0
    return-void
.end method
