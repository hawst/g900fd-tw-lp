.class Lflipboard/service/User$11;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/UserState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 864
    check-cast p1, Lflipboard/objs/UserState;

    iget-boolean v0, p1, Lflipboard/objs/UserState;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->f:Lflipboard/service/User$Message;

    invoke-static {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User;Lflipboard/service/User$Message;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lflipboard/objs/UserState;->a()I

    move-result v0

    iget-object v1, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->l(Lflipboard/service/User;)I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    invoke-static {v0, p1}, Lflipboard/service/User;->c(Lflipboard/service/User;Lflipboard/objs/UserState;)V

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lflipboard/objs/UserState;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/service/User$11;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 919
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 920
    iget-object v0, p0, Lflipboard/service/User$11;->a:Lflipboard/service/User;

    sget-object v1, Lflipboard/service/User$Message;->e:Lflipboard/service/User$Message;

    invoke-static {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User;Lflipboard/service/User$Message;)V

    .line 921
    return-void
.end method
