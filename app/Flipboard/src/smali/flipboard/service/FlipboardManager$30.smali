.class Lflipboard/service/FlipboardManager$30;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 2598
    iput-object p1, p0, Lflipboard/service/FlipboardManager$30;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2611
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading popularSearches: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2612
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 5

    .prologue
    .line 2602
    :try_start_0
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->i()Lflipboard/objs/ConfigPopularSearches;

    move-result-object v0

    .line 2603
    iget-object v1, p0, Lflipboard/service/FlipboardManager$30;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v1, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;Lflipboard/objs/ConfigPopularSearches;)Lflipboard/objs/ConfigPopularSearches;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2607
    return-void

    .line 2604
    :catch_0
    move-exception v0

    .line 2605
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "failed to parse popularSearches.json: %-E"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2606
    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2615
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading popularSearches, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2616
    return-void
.end method
