.class public Lflipboard/service/Account;
.super Ljava/lang/Object;
.source "Account.java"

# interfaces
.implements Lflipboard/objs/ContentDrawerListItem;
.implements Lflipboard/service/DatabaseRow;


# static fields
.field static final synthetic f:Z


# instance fields
.field a:I

.field public b:Lflipboard/objs/UserService;

.field public c:Lflipboard/service/Account$Meta;

.field public volatile d:Lflipboard/io/Download;

.field public volatile e:Lflipboard/io/Download;

.field private g:Lflipboard/util/ProcrastinatingTimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lflipboard/service/Account;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/service/Account;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lflipboard/objs/UserService;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lflipboard/service/Account$Meta;

    invoke-direct {v0}, Lflipboard/service/Account$Meta;-><init>()V

    iput-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    .line 80
    iput-object p1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    .line 81
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/UserService;Z)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lflipboard/service/Account;-><init>(Lflipboard/objs/UserService;)V

    .line 85
    iget-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-boolean v0, v0, Lflipboard/service/Account$Meta;->b:Z

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iput-boolean p2, v0, Lflipboard/service/Account$Meta;->b:Z

    invoke-virtual {p0}, Lflipboard/service/Account;->l()V

    .line 86
    :cond_0
    return-void
.end method

.method public constructor <init>(Lflipboard/service/DatabaseHandler;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 90
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lflipboard/service/Account;-><init>(Lflipboard/objs/UserService;)V

    .line 91
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lflipboard/service/DatabaseHandler;->c(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lflipboard/service/Account;->a:I

    .line 93
    const-string v1, "descriptor"

    invoke-virtual {p1, v1}, Lflipboard/service/DatabaseHandler;->d(Ljava/lang/String;)[B

    move-result-object v1

    .line 94
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 96
    :cond_0
    if-eqz v0, :cond_2

    .line 97
    :try_start_0
    new-instance v2, Lflipboard/json/JSONParser;

    invoke-direct {v2, v1}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->A()Lflipboard/objs/UserService;

    move-result-object v1

    iput-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    .line 111
    :goto_0
    const-string v1, "metaData"

    invoke-virtual {p1, v1}, Lflipboard/service/DatabaseHandler;->d(Ljava/lang/String;)[B

    move-result-object v1

    .line 112
    if-eqz v0, :cond_3

    .line 113
    if-eqz v1, :cond_1

    .line 114
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, v1}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->C()Lflipboard/service/Account$Meta;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    .line 125
    :cond_1
    :goto_1
    return-void

    .line 100
    :cond_2
    new-instance v1, Lflipboard/objs/UserService;

    invoke-direct {v1}, Lflipboard/objs/UserService;-><init>()V

    iput-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    .line 101
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "service"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "serviceId"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "name"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 104
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "screenName"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "email"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "image"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/objs/UserService;->a(Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    const-string v2, "profile"

    invoke-virtual {p1, v2}, Lflipboard/service/DatabaseHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/UserService;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "failed to load account: %-E"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 117
    :cond_3
    :try_start_1
    new-instance v0, Lflipboard/service/MetaData;

    invoke-direct {v0, p0}, Lflipboard/service/MetaData;-><init>(Lflipboard/service/DatabaseRow;)V

    .line 118
    invoke-virtual {v0, v1}, Lflipboard/service/MetaData;->a([B)V

    .line 119
    iget-object v1, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    const-string v2, "isReadLaterService"

    invoke-virtual {v0}, Lflipboard/service/MetaData;->a()Lflipboard/json/FLObject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lflipboard/service/Account$Meta;->b:Z

    .line 120
    iget-object v1, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    const-string v2, "selectedShareTargets"

    invoke-virtual {v0}, Lflipboard/service/MetaData;->a()Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    iput-object v0, v1, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    .line 121
    invoke-virtual {p0}, Lflipboard/service/Account;->l()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/String;Z)I
    .locals 1

    .prologue
    .line 403
    const-string v0, "nytimes"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    const v0, 0x7f0d023e

    .line 412
    :goto_0
    return v0

    .line 405
    :cond_0
    const-string v0, "ft"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    if-eqz p1, :cond_1

    .line 407
    const v0, 0x7f0d0162

    goto :goto_0

    .line 409
    :cond_1
    const v0, 0x7f0d0161

    goto :goto_0

    .line 412
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/Account;)Lflipboard/service/Account$Meta;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 376
    const-string v0, "nytimes"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    const v0, 0x7f0d0236

    .line 381
    :goto_0
    return v0

    .line 378
    :cond_0
    const-string v0, "ft"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    const v0, 0x7f0d015e

    goto :goto_0

    .line 381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 385
    const-string v0, "nytimes"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    const v0, 0x7f0d023d

    .line 390
    :goto_0
    return v0

    .line 387
    :cond_0
    const-string v0, "ft"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    const v0, 0x7f0d015f

    goto :goto_0

    .line 390
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 394
    const-string v0, "nytimes"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    const v0, 0x7f0d0236

    .line 399
    :goto_0
    return v0

    .line 396
    :cond_0
    const-string v0, "ft"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    const v0, 0x7f0d0160

    goto :goto_0

    .line 399
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 454
    const-string v0, "nytimes"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ft"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x2

    return v0
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 320
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 321
    iget-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iput-object p1, v0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    .line 322
    invoke-virtual {p0}, Lflipboard/service/Account;->l()V

    .line 323
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 471
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot set title on an account!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 458
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 277
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 278
    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 280
    :cond_0
    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 457
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 467
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lflipboard/service/Account;->a:I

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    const-string v0, "accounts"

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 257
    instance-of v0, p1, Lflipboard/service/Account;

    if-eqz v0, :cond_0

    .line 258
    check-cast p1, Lflipboard/service/Account;

    .line 259
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0, v1}, Lflipboard/objs/UserService;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v1, p1, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 346
    const-string v2, "nytimes"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 347
    sget-boolean v2, Lflipboard/service/Account;->f:Z

    if-nez v2, :cond_0

    const-string v2, "nytimes"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const-string v2, "none"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 351
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 347
    goto :goto_0

    .line 348
    :cond_3
    const-string v2, "ft"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 349
    sget-boolean v2, Lflipboard/service/Account;->f:Z

    if-nez v2, :cond_4

    const-string v2, "ft"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    const-string v2, "none"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 351
    goto :goto_0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 360
    const-string v1, "nytimes"

    iget-object v2, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    invoke-virtual {p0}, Lflipboard/service/Account;->i()Z

    move-result v0

    .line 368
    :cond_0
    :goto_0
    return v0

    .line 362
    :cond_1
    const-string v1, "ft"

    iget-object v2, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 364
    sget-boolean v0, Lflipboard/service/Account;->f:Z

    if-nez v0, :cond_2

    const-string v0, "ft"

    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    const-string v0, "premium"

    iget-object v1, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 365
    :cond_3
    const-string v1, "wsj"

    iget-object v2, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    const-string v1, "none"

    iget-object v2, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 269
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit16 v0, v0, 0xcd

    .line 271
    mul-int/lit8 v0, v0, 0x29

    iget-object v2, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    invoke-virtual {v1}, Lflipboard/service/Account$Meta;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 272
    return v0

    :cond_1
    move v0, v1

    .line 269
    goto :goto_0
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 426
    sget-boolean v2, Lflipboard/service/Account;->f:Z

    if-nez v2, :cond_0

    const-string v2, "nytimes"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 429
    :cond_0
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v2

    .line 430
    if-eqz v2, :cond_3

    .line 431
    const-string v2, "all"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "smartphone"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 433
    :cond_2
    :goto_0
    return v0

    :cond_3
    const-string v2, "all"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "tablet"

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x0

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/Account$Meta;->a:Z

    .line 487
    iget-object v0, p0, Lflipboard/service/Account;->g:Lflipboard/util/ProcrastinatingTimerTask;

    if-nez v0, :cond_0

    .line 489
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 490
    new-instance v1, Lflipboard/service/Account$1;

    invoke-direct {v1, p0, v0}, Lflipboard/service/Account$1;-><init>(Lflipboard/service/Account;Lflipboard/service/User;)V

    iput-object v1, p0, Lflipboard/service/Account;->g:Lflipboard/util/ProcrastinatingTimerTask;

    .line 508
    :cond_0
    iget-object v0, p0, Lflipboard/service/Account;->g:Lflipboard/util/ProcrastinatingTimerTask;

    invoke-virtual {v0}, Lflipboard/util/ProcrastinatingTimerTask;->b()V

    .line 509
    return-void
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 476
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 291
    const-string v0, "Account[%s:%s: %s: meta=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x0

    return v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x0

    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 523
    const/4 v0, 0x1

    return v0
.end method
