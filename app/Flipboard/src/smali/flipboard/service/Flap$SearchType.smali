.class public final enum Lflipboard/service/Flap$SearchType;
.super Ljava/lang/Enum;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/Flap$SearchType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/Flap$SearchType;

.field public static final enum b:Lflipboard/service/Flap$SearchType;

.field public static final enum c:Lflipboard/service/Flap$SearchType;

.field public static final enum d:Lflipboard/service/Flap$SearchType;

.field private static final synthetic e:[Lflipboard/service/Flap$SearchType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2316
    new-instance v0, Lflipboard/service/Flap$SearchType;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, Lflipboard/service/Flap$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Flap$SearchType;->a:Lflipboard/service/Flap$SearchType;

    new-instance v0, Lflipboard/service/Flap$SearchType;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, Lflipboard/service/Flap$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Flap$SearchType;->b:Lflipboard/service/Flap$SearchType;

    new-instance v0, Lflipboard/service/Flap$SearchType;

    const-string v1, "MEDIUM2"

    invoke-direct {v0, v1, v4}, Lflipboard/service/Flap$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    new-instance v0, Lflipboard/service/Flap$SearchType;

    const-string v1, "MORE"

    invoke-direct {v0, v1, v5}, Lflipboard/service/Flap$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Flap$SearchType;->d:Lflipboard/service/Flap$SearchType;

    .line 2315
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->a:Lflipboard/service/Flap$SearchType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/service/Flap$SearchType;->b:Lflipboard/service/Flap$SearchType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/Flap$SearchType;->d:Lflipboard/service/Flap$SearchType;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/service/Flap$SearchType;->e:[Lflipboard/service/Flap$SearchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2315
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/Flap$SearchType;
    .locals 1

    .prologue
    .line 2315
    const-class v0, Lflipboard/service/Flap$SearchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/Flap$SearchType;

    return-object v0
.end method

.method public static values()[Lflipboard/service/Flap$SearchType;
    .locals 1

    .prologue
    .line 2315
    sget-object v0, Lflipboard/service/Flap$SearchType;->e:[Lflipboard/service/Flap$SearchType;

    invoke-virtual {v0}, [Lflipboard/service/Flap$SearchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/Flap$SearchType;

    return-object v0
.end method
