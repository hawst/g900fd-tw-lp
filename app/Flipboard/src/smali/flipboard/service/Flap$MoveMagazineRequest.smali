.class public Lflipboard/service/Flap$MoveMagazineRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4834
    iput-object p1, p0, Lflipboard/service/Flap$MoveMagazineRequest;->b:Lflipboard/service/Flap;

    .line 4835
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4836
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$MoveMagazineRequest;
    .locals 2

    .prologue
    .line 4840
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 4841
    iput-object p1, p0, Lflipboard/service/Flap$MoveMagazineRequest;->d:Ljava/lang/String;

    .line 4842
    iput-object p2, p0, Lflipboard/service/Flap$MoveMagazineRequest;->c:Ljava/lang/String;

    .line 4843
    iput-object p3, p0, Lflipboard/service/Flap$MoveMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4844
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4845
    return-object p0
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4855
    iget-object v0, p0, Lflipboard/service/Flap$MoveMagazineRequest;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4857
    iget-object v0, p0, Lflipboard/service/Flap$MoveMagazineRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/moveMagazineAfterMagazine"

    iget-object v2, p0, Lflipboard/service/Flap$MoveMagazineRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "anchorId"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$MoveMagazineRequest;->c:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "moveId"

    aput-object v4, v3, v5

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$MoveMagazineRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4861
    :goto_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 4862
    iget-object v1, p0, Lflipboard/service/Flap$MoveMagazineRequest;->b:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$MoveMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4863
    return-void

    .line 4859
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$MoveMagazineRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/moveMagazineAfterMagazine"

    iget-object v2, p0, Lflipboard/service/Flap$MoveMagazineRequest;->n:Lflipboard/service/User;

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "moveId"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$MoveMagazineRequest;->d:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
