.class Lflipboard/service/Flap$FollowRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field final a:Lflipboard/objs/FeedItem;

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field c:Lflipboard/service/Flap$JSONResultObserver;

.field final d:Lflipboard/service/Section;

.field final e:Ljava/lang/String;

.field final f:Z

.field final synthetic g:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3020
    iput-object p1, p0, Lflipboard/service/Flap$FollowRequest;->g:Lflipboard/service/Flap;

    .line 3021
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3022
    iput-object p4, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    .line 3023
    iput-object p3, p0, Lflipboard/service/Flap$FollowRequest;->d:Lflipboard/service/Section;

    .line 3024
    iput-object v0, p0, Lflipboard/service/Flap$FollowRequest;->b:Ljava/util/List;

    .line 3025
    iput-object v0, p0, Lflipboard/service/Flap$FollowRequest;->e:Ljava/lang/String;

    .line 3026
    iput-boolean p5, p0, Lflipboard/service/Flap$FollowRequest;->f:Z

    .line 3027
    return-void
.end method

.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3029
    iput-object p1, p0, Lflipboard/service/Flap$FollowRequest;->g:Lflipboard/service/Flap;

    .line 3030
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3031
    iput-object p3, p0, Lflipboard/service/Flap$FollowRequest;->b:Ljava/util/List;

    .line 3032
    iput-object p4, p0, Lflipboard/service/Flap$FollowRequest;->e:Ljava/lang/String;

    .line 3033
    iput-object v0, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    .line 3034
    iput-object v0, p0, Lflipboard/service/Flap$FollowRequest;->d:Lflipboard/service/Section;

    .line 3035
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/Flap$FollowRequest;->f:Z

    .line 3036
    return-void
.end method

.method static synthetic a(Lflipboard/service/Flap$FollowRequest;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$FollowRequest;
    .locals 0

    .prologue
    .line 3010
    iput-object p1, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    return-object p0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3060
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 3061
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    .line 3067
    :goto_0
    return-object v0

    .line 3062
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->e:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3063
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->e:Ljava/lang/String;

    goto :goto_0

    .line 3065
    :cond_1
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->d:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 3073
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 3074
    iget-boolean v0, p0, Lflipboard/service/Flap$FollowRequest;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "/v1/social/follow"

    .line 3075
    :goto_0
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_2

    new-array v1, v11, [Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    aput-object v4, v1, v10

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 3077
    :goto_1
    iget-object v4, p0, Lflipboard/service/Flap$FollowRequest;->g:Lflipboard/service/Flap;

    iget-object v5, p0, Lflipboard/service/Flap$FollowRequest;->n:Lflipboard/service/User;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const-string v8, "serviceUserid"

    aput-object v8, v6, v10

    aput-object v1, v6, v11

    const-string v8, "service"

    aput-object v8, v6, v12

    const/4 v8, 0x3

    invoke-direct {p0}, Lflipboard/service/Flap$FollowRequest;->b()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {v4, v0, v5, v6}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3080
    iget-object v4, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 3081
    const-string v4, "%s&username=%s"

    new-array v5, v12, [Ljava/lang/Object;

    aput-object v0, v5, v10

    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    aput-object v0, v5, v11

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3083
    :cond_0
    sget-object v4, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v0, v4, v10

    .line 3086
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3087
    const-string v0, "Content-Type"

    const-string v5, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v4, v0, v5}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3088
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->g:Lflipboard/service/Flap;

    invoke-virtual {v0, v4}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3089
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    .line 3090
    new-instance v6, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v4, v0}, Lflipboard/service/Flap$FollowRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v6, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3091
    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3092
    const/16 v4, 0xc8

    if-eq v0, v4, :cond_4

    .line 3093
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response from flap: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3119
    iput-object v7, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    .line 3120
    :goto_2
    return-void

    .line 3074
    :cond_1
    const-string v0, "/v1/social/unfollow"

    goto/16 :goto_0

    .line 3075
    :cond_2
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->b:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->b:Ljava/util/List;

    goto/16 :goto_1

    :cond_3
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->d:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    if-eqz v1, :cond_8

    new-array v1, v11, [Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/Flap$FollowRequest;->d:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    aput-object v4, v1, v10

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_1

    .line 3096
    :cond_4
    :try_start_1
    invoke-virtual {v6}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3097
    if-eqz v0, :cond_7

    .line 3098
    const-string v4, "success"

    invoke-virtual {v0, v4}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 3100
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v11, :cond_5

    .line 3101
    new-instance v6, Lflipboard/objs/UserState$TargetAuthor;

    invoke-direct {v6}, Lflipboard/objs/UserState$TargetAuthor;-><init>()V

    .line 3102
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v6, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    .line 3103
    invoke-direct {p0}, Lflipboard/service/Flap$FollowRequest;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    .line 3104
    const-string v1, "follow"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, Lflipboard/service/Flap$FollowRequest;->d:Lflipboard/service/Section;

    iget-object v5, p0, Lflipboard/service/Flap$FollowRequest;->a:Lflipboard/objs/FeedItem;

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V

    .line 3106
    :cond_5
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3119
    :goto_3
    iput-object v7, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_2

    .line 3108
    :cond_6
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 3113
    :catch_0
    move-exception v0

    .line 3114
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3119
    iput-object v7, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto/16 :goto_2

    .line 3111
    :cond_7
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 3115
    :catch_1
    move-exception v0

    .line 3116
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3117
    iget-object v1, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3119
    iput-object v7, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    iput-object v7, p0, Lflipboard/service/Flap$FollowRequest;->c:Lflipboard/service/Flap$JSONResultObserver;

    throw v0

    :cond_8
    move-object v1, v7

    goto/16 :goto_1
.end method
