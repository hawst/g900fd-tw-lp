.class public final enum Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;
.super Ljava/lang/Enum;
.source "FlipboardManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum b:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum c:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum d:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum e:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum f:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum g:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum h:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum i:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field public static final enum j:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

.field private static final synthetic k:[Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 412
    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "RESET_SECTIONS"

    invoke-direct {v0, v1, v3}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->a:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "SECTIONS_CHANGED"

    invoke-direct {v0, v1, v4}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->b:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "ADD_ACCOUNT"

    invoke-direct {v0, v1, v5}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->c:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "REMOVE_ACCOUNT"

    invoke-direct {v0, v1, v6}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->d:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "FLIPBOARD_ACCOUNT_UPDATED"

    invoke-direct {v0, v1, v7}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->e:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "FLIPBOARD_ACCOUNT_CREATED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->f:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "FLIPBOARD_ACCOUNT_LOGGED_IN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->g:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "SOME_SECTIONS_NEED_UPDATING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->h:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "MAGAZINES_CHANGED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->i:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    new-instance v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const-string v1, "TOC_SECTIONS_CREATED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->j:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const/16 v0, 0xa

    new-array v0, v0, [Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->a:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->b:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->c:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->d:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->e:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->f:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->g:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->h:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->i:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->j:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->k:[Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;
    .locals 1

    .prologue
    .line 412
    const-class v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;
    .locals 1

    .prologue
    .line 412
    sget-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->k:[Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-virtual {v0}, [Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    return-object v0
.end method
