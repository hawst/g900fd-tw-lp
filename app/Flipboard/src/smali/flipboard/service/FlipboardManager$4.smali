.class Lflipboard/service/FlipboardManager$4;
.super Landroid/os/AsyncTask;
.source "FlipboardManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 733
    iput-object p1, p0, Lflipboard/service/FlipboardManager$4;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 733
    iget-object v0, p0, Lflipboard/service/FlipboardManager$4;->a:Lflipboard/service/FlipboardManager;

    sget-object v1, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->C:Landroid/graphics/Typeface;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$4;->a:Lflipboard/service/FlipboardManager;

    sget-object v1, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->B:Landroid/graphics/Typeface;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/service/FlipboardManager$4;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "sans-serif-light"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->y:Landroid/graphics/Typeface;

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$4;->a:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$4;->a:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->y:Landroid/graphics/Typeface;

    goto :goto_0
.end method
