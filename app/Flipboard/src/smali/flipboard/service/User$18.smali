.class Lflipboard/service/User$18;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Z

.field final synthetic d:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Ljava/util/List;Z)V
    .locals 1

    .prologue
    .line 1355
    iput-object p1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    const v0, 0x7fffffff

    iput v0, p0, Lflipboard/service/User$18;->a:I

    iput-object p2, p0, Lflipboard/service/User$18;->b:Ljava/util/List;

    iput-boolean p3, p0, Lflipboard/service/User$18;->c:Z

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1358
    const-string v0, "SELECT descriptor,sectionId,title,service,image,tocItem,id,pos,remoteId,private,unreadRemoteId,metaData FROM sections WHERE uid = ? LIMIT ? OFFSET 0"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v2, v1, v7

    iget v2, p0, Lflipboard/service/User$18;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User$18;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1360
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/service/User$18;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1361
    new-instance v0, Lflipboard/service/Section;

    invoke-direct {v0, p0}, Lflipboard/service/Section;-><init>(Lflipboard/service/DatabaseHandler;)V

    .line 1364
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1365
    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->n(Lflipboard/service/User;)Lflipboard/service/Section;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1367
    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    invoke-static {v1, v0}, Lflipboard/service/User;->a(Lflipboard/service/User;Lflipboard/service/Section;)Lflipboard/service/Section;

    .line 1374
    :cond_1
    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, v1, Lflipboard/objs/TOCSection;->H:Z

    if-eqz v1, :cond_4

    .line 1376
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-wide v4, v1, Lflipboard/objs/TOCSection;->I:J

    sub-long/2addr v2, v4

    .line 1377
    const-wide/32 v4, 0x240c8400

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 1378
    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1392
    :goto_1
    iget-boolean v1, p0, Lflipboard/service/User$18;->c:Z

    iput-boolean v1, v0, Lflipboard/service/Section;->B:Z

    .line 1393
    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    invoke-static {v1}, Lflipboard/service/User;->o(Lflipboard/service/User;)Lflipboard/util/Observer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    goto :goto_0

    .line 1370
    :cond_2
    iget-object v1, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v1, v1, Lflipboard/service/Section$Meta;->b:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, v1, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1371
    iget-object v1, p0, Lflipboard/service/User$18;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1380
    :cond_3
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-wide v2, v2, Lflipboard/objs/TOCSection;->I:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    .line 1382
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "sections"

    new-instance v3, Lflipboard/service/User$18$1;

    invoke-direct {v3, p0, v0}, Lflipboard/service/User$18$1;-><init>(Lflipboard/service/User$18;Lflipboard/service/Section;)V

    invoke-virtual {v1, v2, v6, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    goto :goto_1

    .line 1390
    :cond_4
    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1395
    :cond_5
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User$18;->d:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    .line 1396
    return-void
.end method
