.class Lflipboard/service/FlipboardManager$21;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/activities/FlipboardActivity;

.field final synthetic c:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 1860
    iput-object p1, p0, Lflipboard/service/FlipboardManager$21;->c:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$21;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$21;->b:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 1863
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->a(Landroid/support/v4/app/DialogFragment;)V

    .line 1864
    iget-object v0, p0, Lflipboard/service/FlipboardManager$21;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->g(Lflipboard/service/FlipboardManager;)Z

    .line 1865
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 7

    .prologue
    .line 1876
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->b(Landroid/support/v4/app/DialogFragment;)V

    .line 1877
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 1878
    iget-object v1, p0, Lflipboard/service/FlipboardManager$21;->a:Ljava/lang/String;

    .line 1880
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lflipboard/service/FlipboardManager$21;->b:Lflipboard/activities/FlipboardActivity;

    const-class v4, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1881
    const-string v3, "service"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1882
    const-string v3, "viewSectionAfterSuccess"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1883
    const-string v3, "extra_content_discovery_from_source"

    const-string v4, "usageSocialLoginOriginRelogin"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1885
    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    .line 1886
    iget-object v4, p0, Lflipboard/service/FlipboardManager$21;->b:Lflipboard/activities/FlipboardActivity;

    const/16 v5, 0xc8

    new-instance v6, Lflipboard/service/FlipboardManager$21$1;

    invoke-direct {v6, p0, v0, v1, v3}, Lflipboard/service/FlipboardManager$21$1;-><init>(Lflipboard/service/FlipboardManager$21;Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Account;)V

    invoke-virtual {v4, v2, v5, v6}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/Intent;ILflipboard/activities/FlipboardActivity$ActivityResultListener;)V

    .line 1923
    return-void
.end method

.method public final d(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 1869
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->d(Landroid/support/v4/app/DialogFragment;)V

    .line 1871
    iget-object v0, p0, Lflipboard/service/FlipboardManager$21;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->g(Lflipboard/service/FlipboardManager;)Z

    .line 1872
    return-void
.end method
