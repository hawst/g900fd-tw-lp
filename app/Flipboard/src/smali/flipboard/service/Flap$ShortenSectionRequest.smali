.class Lflipboard/service/Flap$ShortenSectionRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Z

.field final synthetic h:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 3363
    iput-object p1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->h:Lflipboard/service/Flap;

    .line 3364
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3365
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ShortenSectionRequest;
    .locals 2

    .prologue
    .line 3369
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 3370
    iput-object p7, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 3371
    iput-object p1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->b:Ljava/lang/String;

    .line 3372
    iput-object p2, p0, Lflipboard/service/Flap$ShortenSectionRequest;->c:Ljava/lang/String;

    .line 3373
    iput-object p3, p0, Lflipboard/service/Flap$ShortenSectionRequest;->d:Ljava/lang/String;

    .line 3374
    iput-object p4, p0, Lflipboard/service/Flap$ShortenSectionRequest;->e:Ljava/lang/String;

    .line 3375
    iput-object p5, p0, Lflipboard/service/Flap$ShortenSectionRequest;->f:Ljava/lang/String;

    .line 3376
    iput-boolean p6, p0, Lflipboard/service/Flap$ShortenSectionRequest;->g:Z

    .line 3377
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3378
    return-object p0
.end method

.method protected final a()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 3384
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->h:Lflipboard/service/Flap;

    const-string v2, "/v1/social/shortenSection"

    iget-object v3, p0, Lflipboard/service/Flap$ShortenSectionRequest;->n:Lflipboard/service/User;

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "sectionid"

    aput-object v5, v4, v8

    iget-object v5, p0, Lflipboard/service/Flap$ShortenSectionRequest;->b:Ljava/lang/String;

    aput-object v5, v4, v9

    const/4 v5, 0x2

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lflipboard/service/Flap$ShortenSectionRequest;->c:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "imageUrl"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-object v6, p0, Lflipboard/service/Flap$ShortenSectionRequest;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "description"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    iget-object v6, p0, Lflipboard/service/Flap$ShortenSectionRequest;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "message"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    iget-object v6, p0, Lflipboard/service/Flap$ShortenSectionRequest;->f:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3385
    iget-boolean v1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->g:Z

    if-eqz v1, :cond_0

    .line 3386
    const-string v1, "&createAction=inviteToContribute"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3388
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3389
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v9, [Ljava/lang/Object;

    aput-object v0, v1, v8

    .line 3391
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 3393
    iget-object v0, p0, Lflipboard/service/Flap$ShortenSectionRequest;->h:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3394
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3395
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$ShortenSectionRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3396
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3397
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    .line 3398
    iget-object v0, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3418
    iput-object v7, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 3419
    :goto_0
    return-void

    .line 3401
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3402
    if-eqz v0, :cond_3

    .line 3403
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3405
    iget-object v1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3418
    :goto_1
    iput-object v7, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3407
    :cond_2
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3412
    :catch_0
    move-exception v0

    .line 3413
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3418
    iput-object v7, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 3410
    :cond_3
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 3414
    :catch_1
    move-exception v0

    .line 3415
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3416
    iget-object v1, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3418
    iput-object v7, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v7, p0, Lflipboard/service/Flap$ShortenSectionRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method
