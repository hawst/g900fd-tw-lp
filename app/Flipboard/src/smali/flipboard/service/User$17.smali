.class Lflipboard/service/User$17;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1184
    iput-object p1, p0, Lflipboard/service/User$17;->a:Lflipboard/service/User;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1187
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1189
    :try_start_0
    const-string v1, "state"

    iget-object v2, p0, Lflipboard/service/User$17;->a:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->j(Lflipboard/service/User;)Lflipboard/objs/UserState;

    move-result-object v2

    invoke-static {v2}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1190
    const-string v1, "syncNeeded"

    iget-object v2, p0, Lflipboard/service/User$17;->a:Lflipboard/service/User;

    invoke-static {v2}, Lflipboard/service/User;->d(Lflipboard/service/User;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1191
    const-string v1, "uid = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/service/User$17;->a:Lflipboard/service/User;

    iget-object v4, v4, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/service/User$17;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1192
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    .line 1201
    :goto_0
    return-void

    .line 1194
    :cond_0
    const-string v1, "uid"

    iget-object v2, p0, Lflipboard/service/User$17;->a:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    invoke-virtual {p0, v0}, Lflipboard/service/User$17;->a(Landroid/content/ContentValues;)I

    .line 1196
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1198
    :catch_0
    move-exception v0

    .line 1199
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v2, "failed to save user state: %E"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
