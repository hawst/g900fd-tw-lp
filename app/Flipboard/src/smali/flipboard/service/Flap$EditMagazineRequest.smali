.class public Lflipboard/service/Flap$EditMagazineRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4762
    iput-object p1, p0, Lflipboard/service/Flap$EditMagazineRequest;->b:Lflipboard/service/Flap;

    .line 4763
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4764
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$EditMagazineRequest;
    .locals 2

    .prologue
    .line 4768
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    const/4 v1, 0x4

    aput-object p5, v0, v1

    .line 4769
    iput-object p1, p0, Lflipboard/service/Flap$EditMagazineRequest;->c:Ljava/lang/String;

    .line 4770
    iput-object p2, p0, Lflipboard/service/Flap$EditMagazineRequest;->d:Ljava/lang/String;

    .line 4771
    iput-object p3, p0, Lflipboard/service/Flap$EditMagazineRequest;->e:Ljava/lang/String;

    .line 4772
    iput-object p4, p0, Lflipboard/service/Flap$EditMagazineRequest;->f:Ljava/lang/String;

    .line 4773
    iput-object p6, p0, Lflipboard/service/Flap$EditMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4774
    iput-object p5, p0, Lflipboard/service/Flap$EditMagazineRequest;->g:Ljava/lang/String;

    .line 4775
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4776
    return-object p0
.end method

.method protected final a()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4782
    iget-object v0, p0, Lflipboard/service/Flap$EditMagazineRequest;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4784
    iget-object v0, p0, Lflipboard/service/Flap$EditMagazineRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/editMagazine"

    iget-object v2, p0, Lflipboard/service/Flap$EditMagazineRequest;->n:Lflipboard/service/User;

    const/16 v3, 0x12

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "target"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$EditMagazineRequest;->c:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "key"

    aput-object v4, v3, v5

    const-string v4, "magazineVisibility"

    aput-object v4, v3, v8

    const-string v4, "key"

    aput-object v4, v3, v9

    const/4 v4, 0x5

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "key"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "description"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "key"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "magazineCategory"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0x10

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0x11

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4788
    :goto_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 4789
    iget-object v1, p0, Lflipboard/service/Flap$EditMagazineRequest;->b:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$EditMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4790
    return-void

    .line 4786
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$EditMagazineRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/editMagazine"

    iget-object v2, p0, Lflipboard/service/Flap$EditMagazineRequest;->n:Lflipboard/service/User;

    const/16 v3, 0xe

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "target"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$EditMagazineRequest;->c:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "key"

    aput-object v4, v3, v5

    const-string v4, "magazineVisibility"

    aput-object v4, v3, v8

    const-string v4, "key"

    aput-object v4, v3, v9

    const/4 v4, 0x5

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "key"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "description"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "value"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    iget-object v5, p0, Lflipboard/service/Flap$EditMagazineRequest;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
