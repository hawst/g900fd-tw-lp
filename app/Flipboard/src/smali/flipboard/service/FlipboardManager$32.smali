.class Lflipboard/service/FlipboardManager$32;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic c:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 0

    .prologue
    .line 2663
    iput-object p1, p0, Lflipboard/service/FlipboardManager$32;->c:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$32;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2693
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading apiClients: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2694
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2695
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 2697
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 5

    .prologue
    .line 2667
    const/4 v0, 0x0

    .line 2668
    :try_start_0
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-direct {v1, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v1

    .line 2669
    const-string v2, "device"

    invoke-virtual {v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2670
    const-string v2, "device"

    invoke-virtual {v1, v2}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v2

    .line 2675
    iget-object v3, p0, Lflipboard/service/FlipboardManager$32;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2676
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2679
    :cond_0
    iget-object v2, p0, Lflipboard/service/FlipboardManager$32;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v2, v0}, Lflipboard/service/FlipboardManager;->c(Lflipboard/service/FlipboardManager;Ljava/lang/String;)Ljava/lang/String;

    .line 2680
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->c:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->k(Lflipboard/service/FlipboardManager;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_1

    .line 2681
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2689
    :cond_1
    return-void

    .line 2683
    :catch_0
    move-exception v0

    .line 2684
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "failed to parse apiClients.json: %-E"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2685
    iget-object v1, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v1, :cond_2

    .line 2686
    iget-object v1, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 2688
    :cond_2
    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2700
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "fail loading apiClients, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2701
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    if-eqz v0, :cond_0

    .line 2702
    iget-object v0, p0, Lflipboard/service/FlipboardManager$32;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 2704
    :cond_0
    return-void
.end method
