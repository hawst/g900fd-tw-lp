.class Lflipboard/service/FlipboardManager$23;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 2004
    iput-object p1, p0, Lflipboard/service/FlipboardManager$23;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2004
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/FlipboardManager$44;->a:[I

    invoke-virtual {p2}, Lflipboard/service/User$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lflipboard/service/FlipboardManager$23;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->b:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p3, v0, v2

    iget-object v0, p0, Lflipboard/service/FlipboardManager$23;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->c:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-virtual {v0, v1, p3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p3, v0, v2

    iget-object v0, p0, Lflipboard/service/FlipboardManager$23;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->d:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-virtual {v0, v1, p3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lflipboard/service/FlipboardManager$23;->a:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->i:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    invoke-virtual {v0, v1, p3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
