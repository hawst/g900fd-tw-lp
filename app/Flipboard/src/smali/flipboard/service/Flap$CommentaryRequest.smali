.class public Lflipboard/service/Flap$CommentaryRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:Lflipboard/service/Flap$CommentaryObserver;

.field final synthetic c:Lflipboard/service/Flap;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 1

    .prologue
    .line 2712
    iput-object p1, p0, Lflipboard/service/Flap$CommentaryRequest;->c:Lflipboard/service/Flap;

    .line 2713
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2708
    const-string v0, "/v1/social/commentary"

    iput-object v0, p0, Lflipboard/service/Flap$CommentaryRequest;->d:Ljava/lang/String;

    .line 2714
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$CommentaryObserver;",
            ")",
            "Lflipboard/service/Flap$CommentaryRequest;"
        }
    .end annotation

    .prologue
    .line 2723
    iput-object p1, p0, Lflipboard/service/Flap$CommentaryRequest;->a:Ljava/util/List;

    .line 2724
    iput-object p4, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    .line 2725
    iput-object p2, p0, Lflipboard/service/Flap$CommentaryRequest;->d:Ljava/lang/String;

    .line 2726
    iput-object p3, p0, Lflipboard/service/Flap$CommentaryRequest;->e:Ljava/lang/String;

    .line 2727
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2728
    return-object p0
.end method

.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2735
    iget-object v0, p0, Lflipboard/service/Flap$CommentaryRequest;->c:Lflipboard/service/Flap;

    iget-object v1, p0, Lflipboard/service/Flap$CommentaryRequest;->d:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/Flap$CommentaryRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "oid"

    aput-object v4, v3, v7

    iget-object v4, p0, Lflipboard/service/Flap$CommentaryRequest;->a:Ljava/util/List;

    aput-object v4, v3, v8

    const/4 v4, 0x2

    const-string v5, "pageKey"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$CommentaryRequest;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2736
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 2738
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2739
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2740
    iget-object v0, p0, Lflipboard/service/Flap$CommentaryRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2741
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2742
    new-instance v3, Lflipboard/service/Flap$CommentaryInputStream;

    iget-object v4, p0, Lflipboard/service/Flap$CommentaryRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$CommentaryRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lflipboard/service/Flap$CommentaryInputStream;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    .line 2743
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2744
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2745
    iget-object v0, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2756
    iput-object v6, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    .line 2757
    :goto_0
    return-void

    .line 2748
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$CommentaryInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult;

    .line 2749
    iget-object v1, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2756
    iput-object v6, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    goto :goto_0

    .line 2750
    :catch_0
    move-exception v0

    .line 2751
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2756
    iput-object v6, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    goto :goto_0

    .line 2752
    :catch_1
    move-exception v0

    .line 2753
    :try_start_3
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2754
    iget-object v1, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2756
    iput-object v6, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lflipboard/service/Flap$CommentaryRequest;->b:Lflipboard/service/Flap$CommentaryObserver;

    throw v0
.end method
