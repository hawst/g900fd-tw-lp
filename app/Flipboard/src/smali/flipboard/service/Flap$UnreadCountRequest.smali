.class Lflipboard/service/Flap$UnreadCountRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic c:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4183
    iput-object p1, p0, Lflipboard/service/Flap$UnreadCountRequest;->c:Lflipboard/service/Flap;

    .line 4184
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4185
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 4197
    iget-object v0, p0, Lflipboard/service/Flap$UnreadCountRequest;->c:Lflipboard/service/Flap;

    const-string v1, "/v1/social/unreadCount"

    iget-object v2, p0, Lflipboard/service/Flap$UnreadCountRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "service"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$UnreadCountRequest;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4198
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 4200
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 4201
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4203
    iget-object v0, p0, Lflipboard/service/Flap$UnreadCountRequest;->c:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 4204
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 4205
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$UnreadCountRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4206
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 4207
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 4208
    iget-object v0, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4226
    :goto_0
    iput-object v5, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    .line 4227
    :goto_1
    return-void

    .line 4210
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 4211
    if-eqz v0, :cond_2

    .line 4212
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4213
    iget-object v1, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4221
    :catch_0
    move-exception v0

    .line 4222
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4226
    iput-object v5, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_1

    .line 4215
    :cond_1
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4223
    :catch_1
    move-exception v0

    .line 4224
    :try_start_4
    iget-object v1, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4226
    iput-object v5, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_1

    .line 4218
    :cond_2
    :try_start_5
    iget-object v0, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 4226
    :catchall_0
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method

.method public final a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 0

    .prologue
    .line 4189
    iput-object p1, p0, Lflipboard/service/Flap$UnreadCountRequest;->a:Ljava/lang/String;

    .line 4190
    iput-object p2, p0, Lflipboard/service/Flap$UnreadCountRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    .line 4191
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4192
    return-void
.end method
