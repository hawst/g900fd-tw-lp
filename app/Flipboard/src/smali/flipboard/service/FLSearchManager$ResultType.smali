.class public final enum Lflipboard/service/FLSearchManager$ResultType;
.super Ljava/lang/Enum;
.source "FLSearchManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/FLSearchManager$ResultType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/FLSearchManager$ResultType;

.field public static final enum b:Lflipboard/service/FLSearchManager$ResultType;

.field public static final enum c:Lflipboard/service/FLSearchManager$ResultType;

.field public static final enum d:Lflipboard/service/FLSearchManager$ResultType;

.field public static final enum e:Lflipboard/service/FLSearchManager$ResultType;

.field public static final enum f:Lflipboard/service/FLSearchManager$ResultType;

.field private static final synthetic g:[Lflipboard/service/FLSearchManager$ResultType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, Lflipboard/service/FLSearchManager$ResultType;

    const-string v1, "Undefined"

    invoke-direct {v0, v1, v3}, Lflipboard/service/FLSearchManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->a:Lflipboard/service/FLSearchManager$ResultType;

    new-instance v0, Lflipboard/service/FLSearchManager$ResultType;

    const-string v1, "GeneralSection"

    invoke-direct {v0, v1, v4}, Lflipboard/service/FLSearchManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->b:Lflipboard/service/FLSearchManager$ResultType;

    new-instance v0, Lflipboard/service/FLSearchManager$ResultType;

    const-string v1, "Magazine"

    invoke-direct {v0, v1, v5}, Lflipboard/service/FLSearchManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->c:Lflipboard/service/FLSearchManager$ResultType;

    new-instance v0, Lflipboard/service/FLSearchManager$ResultType;

    const-string v1, "AccountHome"

    invoke-direct {v0, v1, v6}, Lflipboard/service/FLSearchManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->d:Lflipboard/service/FLSearchManager$ResultType;

    new-instance v0, Lflipboard/service/FLSearchManager$ResultType;

    const-string v1, "MyFlipboardAccount"

    invoke-direct {v0, v1, v7}, Lflipboard/service/FLSearchManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->e:Lflipboard/service/FLSearchManager$ResultType;

    new-instance v0, Lflipboard/service/FLSearchManager$ResultType;

    const-string v1, "ConfigService"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/FLSearchManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->f:Lflipboard/service/FLSearchManager$ResultType;

    .line 25
    const/4 v0, 0x6

    new-array v0, v0, [Lflipboard/service/FLSearchManager$ResultType;

    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->a:Lflipboard/service/FLSearchManager$ResultType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->b:Lflipboard/service/FLSearchManager$ResultType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->c:Lflipboard/service/FLSearchManager$ResultType;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->d:Lflipboard/service/FLSearchManager$ResultType;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/FLSearchManager$ResultType;->e:Lflipboard/service/FLSearchManager$ResultType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/FLSearchManager$ResultType;->f:Lflipboard/service/FLSearchManager$ResultType;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/FLSearchManager$ResultType;->g:[Lflipboard/service/FLSearchManager$ResultType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/FLSearchManager$ResultType;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lflipboard/service/FLSearchManager$ResultType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLSearchManager$ResultType;

    return-object v0
.end method

.method public static values()[Lflipboard/service/FLSearchManager$ResultType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lflipboard/service/FLSearchManager$ResultType;->g:[Lflipboard/service/FLSearchManager$ResultType;

    invoke-virtual {v0}, [Lflipboard/service/FLSearchManager$ResultType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/FLSearchManager$ResultType;

    return-object v0
.end method
