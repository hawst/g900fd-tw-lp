.class public Lflipboard/service/Flap$CreateAccountWithFacebookRequest;
.super Lflipboard/service/Flap$AccountRequest;
.source "Flap.java"


# instance fields
.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field final synthetic i:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1621
    iput-object p1, p0, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->i:Lflipboard/service/Flap;

    .line 1622
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$AccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1623
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1636
    iget-object v0, p0, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->i:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/createWithSSO"

    iget-object v2, p0, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "service"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "facebook"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "serviceId"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "from"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountWithFacebookRequest;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic cancel()Z
    .locals 1

    .prologue
    .line 1615
    invoke-super {p0}, Lflipboard/service/Flap$AccountRequest;->cancel()Z

    move-result v0

    return v0
.end method
