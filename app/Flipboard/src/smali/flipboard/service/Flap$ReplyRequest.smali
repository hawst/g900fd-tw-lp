.class Lflipboard/service/Flap$ReplyRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field final a:Lflipboard/objs/FeedItem;

.field b:Ljava/lang/String;

.field c:Lflipboard/service/ServiceReloginObserver;

.field final d:Lflipboard/service/Section;

.field e:Lflipboard/objs/CommentaryResult$CommentType;

.field final synthetic f:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 2488
    iput-object p1, p0, Lflipboard/service/Flap$ReplyRequest;->f:Lflipboard/service/Flap;

    .line 2489
    invoke-direct {p0, p1, p3}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2490
    iput-object p5, p0, Lflipboard/service/Flap$ReplyRequest;->a:Lflipboard/objs/FeedItem;

    .line 2491
    iput-object p4, p0, Lflipboard/service/Flap$ReplyRequest;->d:Lflipboard/service/Section;

    .line 2492
    iput-object p2, p0, Lflipboard/service/Flap$ReplyRequest;->e:Lflipboard/objs/CommentaryResult$CommentType;

    .line 2493
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)Lflipboard/service/Flap$ReplyRequest;
    .locals 0

    .prologue
    .line 2497
    iput-object p1, p0, Lflipboard/service/Flap$ReplyRequest;->b:Ljava/lang/String;

    .line 2498
    iput-object p2, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    .line 2499
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2500
    return-object p0
.end method

.method protected final a()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2506
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2507
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRequest;->f:Lflipboard/service/Flap;

    const-string v1, "/v1/social/reply"

    iget-object v4, p0, Lflipboard/service/Flap$ReplyRequest;->n:Lflipboard/service/User;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "oid"

    aput-object v6, v5, v9

    iget-object v6, p0, Lflipboard/service/Flap$ReplyRequest;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v6}, Lflipboard/objs/FeedItem;->m()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    const/4 v6, 0x2

    const-string v7, "text"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lflipboard/service/Flap$ReplyRequest;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v1, v4, v5}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2508
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v10, [Ljava/lang/Object;

    aput-object v0, v1, v9

    .line 2510
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2511
    const-string v0, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2512
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRequest;->f:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2513
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 2514
    new-instance v5, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$ReplyRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v5, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2515
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2516
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2517
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response from flap: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2559
    iput-object v8, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    .line 2560
    :goto_0
    return-void

    .line 2520
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 2521
    if-eqz v0, :cond_4

    .line 2522
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2523
    sget-object v1, Lflipboard/service/Flap$2;->a:[I

    iget-object v4, p0, Lflipboard/service/Flap$ReplyRequest;->e:Lflipboard/objs/CommentaryResult$CommentType;

    invoke-virtual {v4}, Lflipboard/objs/CommentaryResult$CommentType;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 2529
    :goto_1
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2559
    :cond_1
    :goto_2
    iput-object v8, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    goto :goto_0

    .line 2525
    :pswitch_0
    :try_start_2
    new-instance v6, Lflipboard/objs/UserState$TargetAuthor;

    invoke-direct {v6}, Lflipboard/objs/UserState$TargetAuthor;-><init>()V

    .line 2526
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    iput-object v1, v6, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    .line 2527
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRequest;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iput-object v1, v6, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    .line 2528
    const-string v1, "replied"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, Lflipboard/service/Flap$ReplyRequest;->d:Lflipboard/service/Section;

    iget-object v5, p0, Lflipboard/service/Flap$ReplyRequest;->a:Lflipboard/objs/FeedItem;

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2553
    :catch_0
    move-exception v0

    .line 2554
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2559
    iput-object v8, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    goto :goto_0

    .line 2539
    :cond_2
    :try_start_4
    const-string v1, "action"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2540
    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2541
    if-eqz v2, :cond_1

    .line 2542
    if-eqz v1, :cond_3

    const-string v3, "relogin"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2543
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    const/4 v3, 0x0

    const-string v4, "service"

    invoke-virtual {v0, v4}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0, v2}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 2555
    :catch_1
    move-exception v0

    .line 2556
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2557
    iget-object v1, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2559
    iput-object v8, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    goto/16 :goto_0

    .line 2545
    :cond_3
    :try_start_6
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    invoke-virtual {v0, v2}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 2559
    :catchall_0
    move-exception v0

    iput-object v8, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    throw v0

    .line 2551
    :cond_4
    :try_start_7
    iget-object v0, p0, Lflipboard/service/Flap$ReplyRequest;->c:Lflipboard/service/ServiceReloginObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-virtual {v0, v1}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_7
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 2523
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
