.class public Lflipboard/service/Flap$CreateAccountRequest;
.super Lflipboard/service/Flap$AccountRequest;
.source "Flap.java"


# instance fields
.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field final synthetic m:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1589
    iput-object p1, p0, Lflipboard/service/Flap$CreateAccountRequest;->m:Lflipboard/service/Flap;

    .line 1590
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$AccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1591
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1608
    iget-object v0, p0, Lflipboard/service/Flap$CreateAccountRequest;->m:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/create"

    iget-object v2, p0, Lflipboard/service/Flap$CreateAccountRequest;->n:Lflipboard/service/User;

    const/16 v3, 0xc

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "username"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountRequest;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "password"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountRequest;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "email"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountRequest;->i:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "realName"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountRequest;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "image"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountRequest;->k:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "from"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    iget-object v5, p0, Lflipboard/service/Flap$CreateAccountRequest;->l:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic cancel()Z
    .locals 1

    .prologue
    .line 1579
    invoke-super {p0}, Lflipboard/service/Flap$AccountRequest;->cancel()Z

    move-result v0

    return v0
.end method
