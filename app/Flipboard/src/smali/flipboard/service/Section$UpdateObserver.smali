.class Lflipboard/service/Section$UpdateObserver;
.super Ljava/lang/Object;
.source "Section.java"

# interfaces
.implements Lflipboard/service/Flap$UpdateObserver;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field private b:Z

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method constructor <init>(Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 1295
    iput-object p1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/List;)Landroid/util/SparseArray;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)",
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1587
    const/4 v2, 0x0

    .line 1588
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    .line 1589
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_3

    .line 1590
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 1591
    add-int/lit8 v0, v3, 0x1

    move v4, v0

    :goto_1
    if-ge v4, v5, :cond_2

    .line 1592
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1593
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1594
    if-nez v2, :cond_0

    .line 1595
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 1597
    :cond_0
    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1598
    invoke-virtual {v2, v4, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1591
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1589
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1602
    :cond_3
    return-object v2
.end method


# virtual methods
.method public final a()Lflipboard/json/FLObject;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1568
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    .line 1570
    new-instance v3, Lflipboard/json/FLObject;

    invoke-direct {v3}, Lflipboard/json/FLObject;-><init>()V

    .line 1571
    const-string v4, "changes"

    iget-boolean v0, p0, Lflipboard/service/Section$UpdateObserver;->f:Z

    if-nez v0, :cond_0

    iget v0, p0, Lflipboard/service/Section$UpdateObserver;->d:I

    if-lez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1572
    const-string v0, "refresh"

    iget-boolean v4, p0, Lflipboard/service/Section$UpdateObserver;->e:Z

    if-nez v4, :cond_2

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1573
    const-string v0, "EOF"

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->k()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1574
    const-string v0, "relogin"

    iget-boolean v1, p0, Lflipboard/service/Section$UpdateObserver;->h:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lflipboard/json/FLObject;->b(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 1575
    const-string v0, "strategy"

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v1}, Lflipboard/service/Section;->h(Lflipboard/service/Section;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lflipboard/json/FLObject;->b(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 1576
    return-object v3

    :cond_1
    move v0, v1

    .line 1571
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1572
    goto :goto_1
.end method

.method public final a(Lflipboard/objs/FeedItem;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1427
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1428
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Section:UpdateObserver.notifyEndUpdate() called\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1429
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1430
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    const-string v1, "relogin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1432
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "notifyEndUpdate - relogin\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1433
    iput-boolean v5, p0, Lflipboard/service/Section$UpdateObserver;->h:Z

    .line 1434
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1435
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "notifyEndUpdate - clear updateItems\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1436
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1438
    :cond_0
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->o()V

    .line 1439
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    sget-object v1, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1440
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->f(Lflipboard/service/Section;)Z

    .line 1558
    :cond_1
    :goto_0
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-boolean v0, v0, Lflipboard/service/Section;->m:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/objs/TOCSection;->P:Z

    if-nez v0, :cond_f

    .line 1559
    :cond_2
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    sget-object v1, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    invoke-virtual {p0}, Lflipboard/service/Section$UpdateObserver;->a()Lflipboard/json/FLObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1563
    :goto_1
    return-void

    .line 1441
    :cond_3
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    const-string v1, "refresh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1442
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "notifyEndUpdate - refresh\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1443
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iput-boolean v5, v0, Lflipboard/service/Section;->F:Z

    goto :goto_0

    .line 1446
    :cond_4
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "notifyEndUpdate - parameter meta is null - creating smeta from Section.meta\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1447
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->e(Lflipboard/service/Section;)Lflipboard/service/Section$Meta;

    move-result-object v0

    .line 1448
    iget-object v1, p1, Lflipboard/objs/FeedItem;->aT:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1449
    iget-object v1, p1, Lflipboard/objs/FeedItem;->aT:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    .line 1450
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->w()V

    .line 1453
    :cond_5
    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget v1, p0, Lflipboard/service/Section$UpdateObserver;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x2

    iget v2, p0, Lflipboard/service/Section$UpdateObserver;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1454
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "section "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, " :completed, "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    iget v1, p0, Lflipboard/service/Section$UpdateObserver;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, " abbrev,"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    iget v1, p0, Lflipboard/service/Section$UpdateObserver;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, " full\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1455
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->c(Lflipboard/service/Section;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    if-eqz v0, :cond_7

    move v2, v3

    move v4, v3

    .line 1458
    :goto_2
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->c(Lflipboard/service/Section;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 1459
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1460
    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v1}, Lflipboard/service/Section;->c(Lflipboard/service/Section;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 1461
    invoke-virtual {v0, v1}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1462
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1463
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Mismatch in items with UpdateObserver.updateItems in notifyEndUpdate at index "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move v1, v5

    .line 1458
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v1

    goto :goto_2

    .line 1467
    :cond_6
    if-eqz v4, :cond_9

    .line 1468
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "updateItems"

    iget-object v2, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1469
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1470
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "existing items"

    iget-object v2, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v2}, Lflipboard/service/Section;->c(Lflipboard/service/Section;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1477
    :cond_7
    :goto_4
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v1}, Lflipboard/service/Section;->c(Lflipboard/service/Section;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Ljava/util/List;)V

    .line 1478
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->a(Ljava/util/List;)V

    .line 1479
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ah:Z

    if-nez v0, :cond_8

    invoke-static {}, Lflipboard/io/UsageEvent;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_8
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 1480
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-static {v0}, Lflipboard/service/Section$UpdateObserver;->a(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v6

    .line 1481
    if-eqz v6, :cond_b

    .line 1482
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v0, :cond_e

    .line 1483
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Number of duplicates: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wasMore: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lflipboard/service/Section$UpdateObserver;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", total items size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", abbrevCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lflipboard/service/Section$UpdateObserver;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fullCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lflipboard/service/Section$UpdateObserver;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1484
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v1, "Duplicate items in model after updateFeed"

    invoke-direct {v7, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1485
    invoke-virtual {v7}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 1488
    const/4 v2, 0x0

    .line 1489
    new-instance v8, Ljava/util/ArrayList;

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1490
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v1, v3

    :goto_5
    if-ltz v4, :cond_a

    if-nez v1, :cond_a

    .line 1491
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/RequestLogEntry;

    .line 1492
    iget-object v9, v0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v10, "updateFeed"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10

    move-object v1, v0

    move v0, v5

    .line 1490
    :goto_6
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move-object v2, v1

    move v1, v0

    goto :goto_5

    .line 1472
    :cond_9
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1473
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "NO Mismatch found in items with UpdateObserver.updateItems in notifyEndUpdate\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto/16 :goto_4

    .line 1499
    :cond_a
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x1f4

    new-instance v4, Lflipboard/service/Section$UpdateObserver$2;

    invoke-direct {v4, p0, v7, v6, v2}, Lflipboard/service/Section$UpdateObserver$2;-><init>(Lflipboard/service/Section$UpdateObserver;Ljava/lang/Exception;Landroid/util/SparseArray;Lflipboard/io/RequestLogEntry;)V

    invoke-virtual {v0, v1, v4}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    .line 1535
    :cond_b
    :goto_7
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->g(Lflipboard/service/Section;)V

    .line 1537
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 1538
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/service/Section;Ljava/lang/String;)Ljava/lang/String;

    .line 1541
    :cond_c
    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v3

    .line 1542
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->az:Z

    if-eqz v0, :cond_d

    .line 1543
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0, v5}, Lflipboard/service/Section;->a(Lflipboard/service/Section;Z)Z

    .line 1544
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1545
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "notifyEndUpdate: EOF = true\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1547
    :cond_d
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->u()V

    .line 1548
    iget-boolean v0, p0, Lflipboard/service/Section$UpdateObserver;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1549
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x64

    new-instance v2, Lflipboard/service/Section$UpdateObserver$3;

    invoke-direct {v2, p0}, Lflipboard/service/Section$UpdateObserver$3;-><init>(Lflipboard/service/Section$UpdateObserver;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1530
    :cond_e
    const-string v0, "unwanted.Section_duplicate_items_detected"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_7

    .line 1561
    :cond_f
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iput-boolean v5, v0, Lflipboard/service/Section;->n:Z

    goto/16 :goto_1

    :cond_10
    move v0, v1

    move-object v1, v2

    goto/16 :goto_6

    :cond_11
    move v1, v4

    goto/16 :goto_3
.end method

.method public final a(Lflipboard/objs/FeedItem;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1312
    sget-boolean v0, Lflipboard/util/DuplicateOccurrenceLog;->a:Z

    if-eqz v0, :cond_0

    .line 1313
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1314
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v3, "Section:UpdateObserver.notifyBeginUpdate called\n"

    invoke-virtual {v0, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1315
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v3, "\tisMore: "

    invoke-virtual {v0, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1316
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v3, "notifyBeginUpdate"

    iget-object v4, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v4}, Lflipboard/service/Section;->c(Lflipboard/service/Section;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1318
    :cond_0
    iput-boolean p2, p0, Lflipboard/service/Section$UpdateObserver;->e:Z

    .line 1320
    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 1323
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->f:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 1324
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p0, v0, v2

    .line 1331
    :goto_0
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v3, v3, Lflipboard/objs/FeedSection;->e:Z

    iput-boolean v3, v0, Lflipboard/objs/TOCSection;->s:Z

    .line 1332
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->b:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 1335
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1336
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 1338
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    .line 1339
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-virtual {v3}, Lflipboard/objs/FeedSection;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/objs/TOCSection;->a(Ljava/lang/String;)V

    .line 1341
    :cond_2
    iget-object v0, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1342
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-static {v0, v3}, Lflipboard/service/Section;->a(Lflipboard/service/Section;Ljava/lang/String;)V

    .line 1344
    :cond_3
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1345
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    .line 1347
    :cond_4
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->x:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1348
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->x:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    .line 1350
    :cond_5
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->y:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1351
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->y:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    .line 1353
    :cond_6
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1354
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    .line 1356
    :cond_7
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->q:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1357
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->q:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    .line 1359
    :cond_8
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1360
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    .line 1362
    :cond_9
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1363
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->j:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    .line 1365
    :cond_a
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v3, v3, Lflipboard/objs/FeedSection;->B:Z

    iput-boolean v3, v0, Lflipboard/objs/TOCSection;->P:Z

    .line 1366
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 1367
    if-eqz p2, :cond_f

    if-eqz v3, :cond_f

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_1
    iput-object v0, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    .line 1368
    if-nez p2, :cond_b

    .line 1369
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0, v2}, Lflipboard/service/Section;->a(Lflipboard/service/Section;Z)Z

    .line 1371
    :cond_b
    if-nez p2, :cond_10

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lflipboard/service/Section$UpdateObserver;->b:Z

    .line 1372
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->e(Lflipboard/service/Section;)Lflipboard/service/Section$Meta;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->h:Ljava/lang/String;

    invoke-static {v0, v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    .line 1373
    invoke-static {v0}, Lflipboard/service/Section;->e(Lflipboard/service/Section;)Lflipboard/service/Section$Meta;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->i:Ljava/lang/String;

    invoke-static {v0, v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    .line 1374
    invoke-static {v0}, Lflipboard/service/Section;->e(Lflipboard/service/Section;)Lflipboard/service/Section$Meta;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section$Meta;->g:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->g:Ljava/lang/String;

    invoke-static {v0, v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1377
    :cond_c
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->e(Lflipboard/service/Section;)Lflipboard/service/Section$Meta;

    move-result-object v0

    .line 1378
    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->h:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    .line 1379
    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->i:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    .line 1380
    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->g:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/service/Section$Meta;->g:Ljava/lang/String;

    .line 1381
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->w()V

    .line 1384
    :cond_d
    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1390
    return-void

    .line 1327
    :cond_e
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 1328
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-static {v0}, Lflipboard/service/Section;->d(Lflipboard/service/Section;)Lflipboard/objs/TOCSection;

    move-result-object v0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->f:Ljava/lang/String;

    iput-object v3, v0, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 1367
    :cond_f
    new-instance v0, Ljava/util/ArrayList;

    const/16 v3, 0x1e

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 1371
    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 1581
    sget-object v0, Lflipboard/service/Section;->b:Lflipboard/util/Log;

    const-string v1, "exception in section %s: %E"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1582
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    sget-object v1, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    invoke-virtual {v0, v1, p1}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1583
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    sget-object v1, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    invoke-virtual {p0}, Lflipboard/service/Section$UpdateObserver;->a()Lflipboard/json/FLObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1584
    return-void
.end method

.method public final b(Lflipboard/objs/FeedItem;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1398
    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "sidebar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1399
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v0, p1, p0}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section$UpdateObserver;)V

    .line 1419
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1420
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1421
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "\tisMore: "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    iget-boolean v1, p0, Lflipboard/service/Section$UpdateObserver;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1422
    iget-object v0, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\t"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1423
    return-void

    .line 1401
    :cond_1
    if-eqz p2, :cond_2

    .line 1402
    iget v1, p0, Lflipboard/service/Section$UpdateObserver;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/service/Section$UpdateObserver;->c:I

    .line 1410
    :goto_1
    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1411
    iput-boolean v0, p0, Lflipboard/service/Section$UpdateObserver;->f:Z

    .line 1412
    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/service/Section;->e(Z)V

    .line 1414
    iget-boolean v1, p0, Lflipboard/service/Section$UpdateObserver;->b:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lflipboard/service/Section$UpdateObserver;->c:I

    if-nez v1, :cond_0

    .line 1415
    iget-object v1, p0, Lflipboard/service/Section$UpdateObserver;->a:Lflipboard/service/Section;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;Ljava/lang/Runnable;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_2
    iput-boolean v0, p0, Lflipboard/service/Section$UpdateObserver;->b:Z

    goto/16 :goto_0

    .line 1404
    :cond_2
    iget v1, p0, Lflipboard/service/Section$UpdateObserver;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/service/Section$UpdateObserver;->d:I

    goto :goto_1

    .line 1415
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
