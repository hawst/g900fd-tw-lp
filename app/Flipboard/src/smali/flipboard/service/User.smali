.class public Lflipboard/service/User;
.super Lflipboard/util/Observable;
.source "User.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/util/Observable",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;",
        "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;"
    }
.end annotation


# static fields
.field public static final b:Lflipboard/util/Log;


# instance fields
.field private A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public final a:Lflipboard/service/FlipboardManager;

.field public c:Lflipboard/service/User$MagazineChangeListener;

.field public d:Ljava/lang/String;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lflipboard/service/Section;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lflipboard/objs/UserState;

.field k:Lflipboard/objs/UserState;

.field public l:J

.field public m:J

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation
.end field

.field public o:I

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lflipboard/service/Section;

.field private final r:I

.field private final s:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Section$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z

.field private u:J

.field private v:Ljava/util/TimerTask;

.field private w:Ljava/lang/Object;

.field private x:Z

.field private y:Lflipboard/service/User$Message;

.field private z:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-string v0, "service"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 164
    invoke-direct {p0}, Lflipboard/util/Observable;-><init>()V

    .line 87
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    .line 104
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/User;->f:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/service/User;->g:Ljava/util/List;

    .line 116
    iput v5, p0, Lflipboard/service/User;->r:I

    .line 119
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    .line 139
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lflipboard/service/User;->w:Ljava/lang/Object;

    .line 151
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/service/User;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/User;->A:Ljava/util/ArrayList;

    .line 165
    iput-object p1, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    .line 167
    new-instance v0, Lflipboard/service/User$1;

    invoke-direct {v0, p0}, Lflipboard/service/User$1;-><init>(Lflipboard/service/User;)V

    iput-object v0, p0, Lflipboard/service/User;->s:Lflipboard/util/Observer;

    .line 193
    invoke-direct {p0}, Lflipboard/service/User;->A()V

    .line 194
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "userstate"

    new-instance v2, Lflipboard/service/User$15;

    invoke-direct {v2, p0}, Lflipboard/service/User$15;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v1, v7, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v0, :cond_3

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    invoke-virtual {v1}, Lflipboard/objs/UserState;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/User;)V

    .line 196
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "accounts"

    new-instance v2, Lflipboard/service/User$4;

    invoke-direct {v2, p0}, Lflipboard/service/User$4;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v1, v7, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 197
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "User:loadMagazines"

    new-instance v2, Lflipboard/service/User$38;

    invoke-direct {v2, p0}, Lflipboard/service/User$38;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 200
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    sget-object v9, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/service/User;->l:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_4

    move v6, v7

    :goto_1
    const-string v0, "sections"

    new-instance v1, Lflipboard/service/User$18;

    invoke-direct {v1, p0, v8, v6}, Lflipboard/service/User$18;-><init>(Lflipboard/service/User;Ljava/util/List;Z)V

    invoke-virtual {v9, v0, v7, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v1, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    aput-object v1, v0, v7

    iget-object v0, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    if-nez v0, :cond_1

    new-instance v0, Lflipboard/service/Section;

    const-string v1, "auth/flipboard/coverstories"

    const-string v2, "Cover Stories"

    const-string v3, ""

    const-string v4, ""

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    iget-object v0, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v5, v7, v1}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    iget-object v0, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    iput-boolean v7, v0, Lflipboard/service/Section;->B:Z

    :cond_1
    const-string v0, "User:loadSections"

    new-instance v1, Lflipboard/service/User$19;

    invoke-direct {v1, p0, v9, v8, v6}, Lflipboard/service/User$19;-><init>(Lflipboard/service/User;Lflipboard/service/FlipboardManager;Ljava/util/List;Z)V

    invoke-virtual {v9, v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 208
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 209
    const-string v1, "show_status_updates"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/service/User;->t:Z

    .line 210
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 212
    invoke-virtual {p0}, Lflipboard/service/User;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lflipboard/service/User$Message;->f:Lflipboard/service/User$Message;

    :goto_2
    iput-object v0, p0, Lflipboard/service/User;->y:Lflipboard/service/User$Message;

    .line 214
    invoke-virtual {p0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 215
    invoke-direct {p0}, Lflipboard/service/User;->z()V

    .line 217
    :cond_2
    return-void

    .line 194
    :cond_3
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v1, v0, v5

    goto/16 :goto_0

    :cond_4
    move v6, v5

    .line 200
    goto :goto_1

    .line 212
    :cond_5
    sget-object v0, Lflipboard/service/User$Message;->e:Lflipboard/service/User$Message;

    goto :goto_2
.end method

.method private A()V
    .locals 4

    .prologue
    .line 1155
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1171
    :goto_0
    return-void

    .line 1158
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "userstate"

    const/4 v2, 0x1

    new-instance v3, Lflipboard/service/User$16;

    invoke-direct {v3, p0}, Lflipboard/service/User$16;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    goto :goto_0
.end method

.method private B()V
    .locals 4

    .prologue
    .line 1179
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203
    :goto_0
    return-void

    .line 1183
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "userstate"

    const/4 v2, 0x1

    new-instance v3, Lflipboard/service/User$17;

    invoke-direct {v3, p0}, Lflipboard/service/User$17;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    goto :goto_0
.end method

.method private C()I
    .locals 1

    .prologue
    .line 1207
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    invoke-virtual {v0}, Lflipboard/objs/UserState;->a()I

    move-result v0

    goto :goto_0
.end method

.method private D()V
    .locals 2

    .prologue
    .line 1499
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 1516
    :goto_0
    return-void

    .line 1506
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lflipboard/service/Section;

    .line 1507
    iget-object v1, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1508
    new-instance v1, Lflipboard/service/User$20;

    invoke-direct {v1, p0}, Lflipboard/service/User$20;-><init>(Lflipboard/service/User;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1515
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>([Ljava/lang/Object;)V

    iput-object v1, p0, Lflipboard/service/User;->e:Ljava/util/List;

    goto :goto_0
.end method

.method private G()V
    .locals 3

    .prologue
    .line 2381
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 2382
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflipboard/service/Section;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2384
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/service/User;J)J
    .locals 1

    .prologue
    .line 79
    iput-wide p1, p0, Lflipboard/service/User;->u:J

    return-wide p1
.end method

.method static synthetic a(Lflipboard/service/User;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method private a(Lflipboard/service/Section;IZZZLjava/lang/String;)Lflipboard/service/Section;
    .locals 9

    .prologue
    .line 1682
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1683
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 1684
    invoke-virtual {v0, p1}, Lflipboard/service/Section;->a(Lflipboard/service/Section;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1767
    :goto_0
    return-object v0

    .line 1690
    :cond_1
    new-instance v0, Lflipboard/service/User$23;

    invoke-direct {v0, p0, p1, p2}, Lflipboard/service/User$23;-><init>(Lflipboard/service/User;Lflipboard/service/Section;I)V

    invoke-virtual {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 1711
    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    const/4 v3, 0x1

    .line 1713
    :goto_1
    iget-object v6, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v7, "sections"

    const/4 v8, 0x1

    new-instance v0, Lflipboard/service/User$24;

    move-object v1, p0

    move-object v2, p1

    move v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lflipboard/service/User$24;-><init>(Lflipboard/service/User;Lflipboard/service/Section;ZZLjava/lang/String;)V

    invoke-virtual {v6, v7, v8, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 1749
    iget-object v0, p0, Lflipboard/service/User;->s:Lflipboard/util/Observer;

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 1751
    sget-object v0, Lflipboard/service/User$Message;->a:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, p1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1752
    if-eqz p3, :cond_6

    .line 1753
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {p0, v0, p5}, Lflipboard/service/User;->a(Ljava/util/List;Z)V

    .line 1757
    :cond_2
    :goto_2
    if-eqz v3, :cond_3

    .line 1758
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->e(Z)V

    .line 1761
    :cond_3
    if-eqz p5, :cond_4

    iget-object v0, p1, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1762
    invoke-virtual {p1}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1764
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;)V

    :cond_4
    move-object v0, p1

    .line 1767
    goto :goto_0

    .line 1711
    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 1754
    :cond_6
    if-eqz p5, :cond_2

    .line 1755
    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method static synthetic a(Lflipboard/service/User;Lflipboard/service/Section;)Lflipboard/service/Section;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/User;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lflipboard/service/User;->n:Ljava/util/List;

    return-object p1
.end method

.method private a(Lflipboard/service/User$Message;)V
    .locals 3

    .prologue
    .line 1137
    iput-object p1, p0, Lflipboard/service/User;->y:Lflipboard/service/User$Message;

    .line 1138
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1139
    sget-object v0, Lflipboard/service/User$45;->b:[I

    invoke-virtual {p1}, Lflipboard/service/User$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1150
    :goto_0
    return-void

    .line 1142
    :pswitch_0
    iget-object v1, p0, Lflipboard/service/User;->A:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1143
    :try_start_0
    iget-object v0, p0, Lflipboard/service/User;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/util/Observer;

    .line 1144
    invoke-virtual {p0, v0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1147
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1146
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/service/User;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1147
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1139
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lflipboard/service/User;Lflipboard/objs/UserState;)V
    .locals 3

    .prologue
    .line 79
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    new-instance v1, Lflipboard/service/User$10;

    invoke-direct {v1, p0, p1}, Lflipboard/service/User$10;-><init>(Lflipboard/service/User;Lflipboard/objs/UserState;)V

    new-instance v2, Lflipboard/service/Flap$UserStateRequest;

    invoke-direct {v2, v0, p0}, Lflipboard/service/Flap$UserStateRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v2, p1, v1}, Lflipboard/service/Flap$UserStateRequest;->a(Lflipboard/objs/UserState;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;

    return-void
.end method

.method static synthetic a(Lflipboard/service/User;Lflipboard/service/User$Message;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lflipboard/service/User;->a(Lflipboard/service/User$Message;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lflipboard/util/Callback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lflipboard/util/Callback",
            "<",
            "Lflipboard/service/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 592
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 593
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v2, v2, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 594
    invoke-interface {p2, v0}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 598
    :cond_1
    iget-object v0, p0, Lflipboard/service/User;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 599
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v2, v2, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 600
    invoke-interface {p2, v0}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 603
    :cond_3
    return-void
.end method

.method public static a()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 231
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->TopicLocales:Ljava/util/List;

    .line 232
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget-object v3, v3, Lflipboard/model/ConfigSetting;->TopicLanguageCodes:Ljava/util/List;

    .line 233
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v4}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v4

    iget-object v4, v4, Lflipboard/model/ConfigSetting;->TopicCountryCodes:Ljava/util/List;

    .line 237
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    .line 238
    if-eqz v0, :cond_6

    if-eqz v5, :cond_6

    .line 239
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 242
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 243
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    if-eqz v5, :cond_0

    .line 244
    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 247
    :cond_0
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    .line 248
    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    .line 249
    if-eqz v0, :cond_4

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 252
    :cond_1
    :goto_2
    if-nez v0, :cond_5

    :goto_3
    return v1

    :cond_2
    move v0, v2

    .line 239
    goto :goto_0

    :cond_3
    move v0, v2

    .line 244
    goto :goto_1

    :cond_4
    move v0, v2

    .line 249
    goto :goto_2

    :cond_5
    move v1, v2

    .line 252
    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/service/User;J)J
    .locals 1

    .prologue
    .line 79
    iput-wide p1, p0, Lflipboard/service/User;->l:J

    return-wide p1
.end method

.method static synthetic b(Lflipboard/service/User;Lflipboard/objs/UserState;)Lflipboard/objs/UserState;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    return-object p1
.end method

.method static synthetic b(Lflipboard/service/User;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->q:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic b(Lflipboard/service/User;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lflipboard/service/User;->p:Ljava/util/List;

    return-object p1
.end method

.method public static b(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2371
    iget-object v1, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v2

    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v3, v5, [Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v0, v2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    :goto_0
    aput-object v0, v3, v4

    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v5

    .line 2374
    return-void

    .line 2371
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lflipboard/service/Section;ZZLjava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 2168
    iget-object v0, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    if-ne p1, v0, :cond_0

    move v0, v6

    .line 2227
    :goto_0
    return v0

    .line 2173
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 2174
    if-gez v2, :cond_1

    .line 2175
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v1, "failed to find section for deleting: %s"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 2176
    goto :goto_0

    .line 2180
    :cond_1
    new-instance v0, Lflipboard/service/User$31;

    move-object v1, p0

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lflipboard/service/User$31;-><init>(Lflipboard/service/User;ILflipboard/service/Section;ZLjava/lang/String;)V

    invoke-virtual {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 2206
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "sections"

    new-instance v2, Lflipboard/service/User$32;

    invoke-direct {v2, p0, p1}, Lflipboard/service/User$32;-><init>(Lflipboard/service/User;Lflipboard/service/Section;)V

    invoke-virtual {v0, v1, v7, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 2214
    if-eqz p2, :cond_2

    .line 2216
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {p0, v0, v7}, Lflipboard/service/User;->a(Ljava/util/List;Z)V

    .line 2221
    :goto_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_has_removed_section"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2224
    iput v6, p1, Lflipboard/service/Section;->f:I

    .line 2225
    sget-object v0, Lflipboard/service/User$Message;->b:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, p1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v7

    .line 2227
    goto :goto_0

    .line 2219
    :cond_2
    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static synthetic c(Lflipboard/service/User;)Ljava/util/List;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lflipboard/service/User;Lflipboard/objs/UserState;)V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 79
    invoke-direct {p0}, Lflipboard/service/User;->C()I

    move-result v0

    invoke-virtual {p1}, Lflipboard/objs/UserState;->a()I

    move-result v1

    if-ne v1, v0, :cond_0

    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v9

    invoke-virtual {p0, v2}, Lflipboard/service/User;->b(Lflipboard/service/Account;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-boolean v1, v1, Lflipboard/objs/UserState$State;->e:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lflipboard/objs/UserState;->a()I

    move-result v1

    if-ge v1, v0, :cond_1

    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v9

    invoke-virtual {p1}, Lflipboard/objs/UserState;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v7

    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v1, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/service/User;->u:J

    invoke-direct {p0}, Lflipboard/service/User;->B()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    aput-object v2, v0, v9

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v1, "refusing to nuke all my sections on a user sync: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iput-boolean v7, p0, Lflipboard/service/User;->x:Z

    sget-object v12, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    :try_start_0
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_2
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_6

    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v3, v3, Lflipboard/objs/TOCSection;->H:Z

    if-nez v3, :cond_4

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    sget-object v3, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    move v0, v1

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v0, v3, v4, v5}, Lflipboard/service/User;->b(Lflipboard/service/Section;ZZLjava/lang/String;)Z

    move v0, v1

    goto :goto_2

    :cond_6
    iget-object v0, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v10, v7

    move v11, v9

    move v8, v9

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/TOCSection;

    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/TOCSection;)V

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lflipboard/service/User;->a(Lflipboard/service/Section;IZZZLjava/lang/String;)Lflipboard/service/Section;

    move-result-object v3

    if-ne v3, v1, :cond_8

    move v2, v7

    :goto_4
    or-int v0, v11, v2

    iget v1, v3, Lflipboard/service/Section;->g:I

    if-eq v1, v10, :cond_f

    iput v10, v3, Lflipboard/service/Section;->g:I

    const/4 v1, 0x0

    invoke-virtual {p0, v3, v1}, Lflipboard/service/User;->a(Lflipboard/service/Section;Z)V

    move v1, v7

    :goto_5
    if-eqz v2, :cond_9

    iget-object v4, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v12, v4}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Lflipboard/service/Section;->a(Z)V

    :cond_7
    :goto_6
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v11, v0

    move v8, v1

    goto :goto_3

    :cond_8
    move v2, v9

    goto :goto_4

    :cond_9
    if-nez v2, :cond_a

    iget-object v4, v3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v4, v4, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v4, :cond_a

    iget-object v4, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lflipboard/service/Section;->a(Z)V

    const/4 v0, 0x1

    iput-boolean v0, v3, Lflipboard/service/Section;->B:Z

    move v0, v7

    goto :goto_6

    :cond_a
    if-eqz v2, :cond_7

    const/4 v2, 0x1

    iput-boolean v2, v3, Lflipboard/service/Section;->B:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_6

    :catchall_0
    move-exception v0

    iput-boolean v9, p0, Lflipboard/service/User;->x:Z

    throw v0

    :cond_b
    if-eqz v8, :cond_e

    :try_start_1
    invoke-direct {p0}, Lflipboard/service/User;->D()V

    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, p0}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_c
    :goto_7
    if-eqz v11, :cond_d

    const/4 v0, 0x0

    invoke-virtual {v12, v0}, Lflipboard/service/FlipboardManager;->a(Z)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;)V

    :cond_d
    invoke-virtual {v12, p0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/User;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-boolean v9, p0, Lflipboard/service/User;->x:Z

    goto/16 :goto_0

    :cond_e
    if-eqz v11, :cond_c

    :try_start_2
    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_7

    :cond_f
    move v1, v8

    goto :goto_5
.end method

.method static c(Lflipboard/service/Section;)[B
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1887
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 1888
    if-eqz v0, :cond_4

    .line 1890
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1891
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1893
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v8, v0, Lflipboard/model/ConfigSetting;->MaxSavedItemCount:I

    move v3, v4

    move v5, v4

    .line 1897
    :goto_0
    if-ge v5, v8, :cond_3

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1898
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1899
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1900
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_1
    add-int/2addr v1, v5

    .line 1904
    :goto_2
    if-ge v1, v8, :cond_0

    .line 1905
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1907
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v5, v1

    .line 1908
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1900
    goto :goto_1

    .line 1902
    :cond_2
    add-int/lit8 v1, v5, 0x1

    goto :goto_2

    .line 1924
    :cond_3
    :try_start_0
    invoke-static {v7}, Lflipboard/json/JSONSerializer;->b(Ljava/util/List;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1929
    :goto_3
    return-object v0

    .line 1925
    :catch_0
    move-exception v0

    .line 1926
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "CANNOT SERIALIZE section items: %s %-E"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v4

    aput-object v0, v5, v2

    invoke-virtual {v1, v3, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1929
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method static synthetic d(Lflipboard/service/User;)J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lflipboard/service/User;->u:J

    return-wide v0
.end method

.method static synthetic e(Lflipboard/service/User;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->w:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lflipboard/service/User;)Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->v:Ljava/util/TimerTask;

    return-object v0
.end method

.method static synthetic g(Lflipboard/service/User;)Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/User;->v:Ljava/util/TimerTask;

    return-object v0
.end method

.method static synthetic h(Lflipboard/service/User;)Lflipboard/objs/UserState;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->k:Lflipboard/objs/UserState;

    return-object v0
.end method

.method static synthetic i(Lflipboard/service/User;)Lflipboard/objs/UserState;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/User;->k:Lflipboard/objs/UserState;

    return-object v0
.end method

.method static synthetic j(Lflipboard/service/User;)Lflipboard/objs/UserState;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    return-object v0
.end method

.method static synthetic k(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lflipboard/service/User;->B()V

    return-void
.end method

.method static synthetic l(Lflipboard/service/User;)I
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lflipboard/service/User;->C()I

    move-result v0

    return v0
.end method

.method static synthetic m(Lflipboard/service/User;)J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lflipboard/service/User;->l:J

    return-wide v0
.end method

.method private m(Ljava/lang/String;)Lflipboard/service/Section;
    .locals 3

    .prologue
    .line 467
    if-eqz p1, :cond_4

    .line 468
    iget-object v0, p0, Lflipboard/service/User;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 469
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 479
    :cond_1
    :goto_0
    return-object v0

    .line 473
    :cond_2
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 474
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 479
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic n(Lflipboard/service/User;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic o(Lflipboard/service/User;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->s:Lflipboard/util/Observer;

    return-object v0
.end method

.method static synthetic p(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lflipboard/service/User;->D()V

    return-void
.end method

.method static synthetic q(Lflipboard/service/User;)V
    .locals 5

    .prologue
    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v3, "magazines"

    new-instance v4, Lflipboard/service/User$39;

    invoke-direct {v4, p0, v1, v0}, Lflipboard/service/User$39;-><init>(Lflipboard/service/User;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V

    iput-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    iput-object v1, p0, Lflipboard/service/User;->p:Ljava/util/List;

    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loaded "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " magazines and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/User;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contributor magazines from database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method static synthetic r(Lflipboard/service/User;)Ljava/util/List;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic s(Lflipboard/service/User;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic t(Lflipboard/service/User;)Ljava/util/List;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/service/User;->p:Ljava/util/List;

    return-object v0
.end method

.method public static u()Z
    .locals 1

    .prologue
    .line 2818
    invoke-static {}, Lflipboard/activities/LaunchActivity;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static y()Z
    .locals 3

    .prologue
    .line 2917
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "user_has_removed_section"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private z()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 305
    new-instance v0, Lflipboard/service/Section;

    const-string v1, "auth/flipboard/curator%2Fnotifications"

    const/4 v5, 0x0

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lflipboard/service/User;->q:Lflipboard/service/Section;

    .line 306
    iget-object v0, p0, Lflipboard/service/User;->q:Lflipboard/service/Section;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/Section;->h:Z

    .line 307
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Ljava/lang/String;)Lflipboard/service/Section;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 1629
    const/4 v4, 0x0

    .line 1631
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 1632
    invoke-virtual {v0, p1}, Lflipboard/service/Section;->a(Lflipboard/service/Section;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1639
    :goto_1
    if-eqz v0, :cond_0

    .line 1640
    invoke-direct {p0, v0, v1, v3, p2}, Lflipboard/service/User;->b(Lflipboard/service/Section;ZZLjava/lang/String;)Z

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    move-object v6, p2

    .line 1644
    invoke-direct/range {v0 .. v6}, Lflipboard/service/User;->a(Lflipboard/service/Section;IZZZLjava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    return-object v0

    .line 1636
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 1637
    goto :goto_0

    :cond_2
    move-object v0, v4

    goto :goto_1
.end method

.method public final a(Lflipboard/service/Section;ZLjava/lang/String;)Lflipboard/service/Section;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1655
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lflipboard/service/Section;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1656
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v0, v0, Lflipboard/objs/TOCSection;->N:Z

    .line 1657
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v2, 0x0

    new-instance v3, Lflipboard/service/User$22;

    invoke-direct {v3, p0, p1, v0}, Lflipboard/service/User$22;-><init>(Lflipboard/service/User;Lflipboard/service/Section;Z)V

    invoke-virtual {v1, p0, v2, p1, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 1668
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-boolean v4, v0, Lflipboard/objs/TOCSection;->N:Z

    .line 1669
    sget-object v0, Lflipboard/service/User$Message;->l:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, p1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1671
    :cond_0
    invoke-virtual {p0, p1, p2, v4, p3}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 1672
    return-object v0
.end method

.method public final a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;
    .locals 7

    .prologue
    .line 1650
    const/4 v2, -0x1

    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lflipboard/service/User;->a(Lflipboard/service/Section;IZZZLjava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    return-object v0
.end method

.method final a(J)V
    .locals 3

    .prologue
    .line 726
    iget-object v1, p0, Lflipboard/service/User;->w:Ljava/lang/Object;

    monitor-enter v1

    .line 728
    :try_start_0
    iget-object v0, p0, Lflipboard/service/User;->v:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p0, Lflipboard/service/User;->v:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 732
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->k:Lflipboard/objs/UserState;

    if-nez v0, :cond_1

    .line 734
    monitor-exit v1

    .line 764
    :goto_0
    return-void

    .line 736
    :cond_1
    new-instance v0, Lflipboard/service/User$9;

    invoke-direct {v0, p0}, Lflipboard/service/User$9;-><init>(Lflipboard/service/User;)V

    iput-object v0, p0, Lflipboard/service/User;->v:Ljava/util/TimerTask;

    .line 763
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v2, p0, Lflipboard/service/User;->v:Ljava/util/TimerTask;

    invoke-virtual {v0, v2, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 764
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 1

    .prologue
    .line 2781
    new-instance v0, Lflipboard/service/User$43;

    invoke-direct {v0, p0, p1}, Lflipboard/service/User$43;-><init>(Lflipboard/service/User;Lflipboard/json/FLObject;)V

    invoke-virtual {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 2793
    return-void
.end method

.method public final a(Lflipboard/model/ConfigFirstLaunch;ILjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    .line 1776
    .line 1777
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lflipboard/model/ConfigFirstLaunch;->TopicPickerDefaultSections:Ljava/util/List;

    if-nez v0, :cond_3

    .line 1779
    :cond_0
    iget-object v0, p1, Lflipboard/model/ConfigFirstLaunch;->DefaultSections:Ljava/util/List;

    move-object v6, v0

    .line 1783
    :goto_0
    iget-object v0, p1, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v5

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    .line 1784
    iget-boolean v2, v0, Lflipboard/model/FirstRunSection;->preselected:Z

    if-eqz v2, :cond_1

    .line 1786
    if-lez p2, :cond_4

    .line 1787
    add-int/lit8 p2, p2, -0x1

    .line 1794
    :cond_1
    :goto_2
    add-int/lit8 v7, v1, 0x1

    .line 1795
    const/4 v0, 0x2

    if-ne v7, v0, :cond_5

    .line 1796
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lflipboard/model/FirstRunSection;

    .line 1797
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v3, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1799
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v3, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    iget-object v2, v3, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    iget-object v3, v3, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1800
    invoke-virtual {v0, v10}, Lflipboard/service/Section;->a(Z)V

    .line 1801
    invoke-virtual {p0, v0, v10, v10, p3}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    goto :goto_3

    .line 1781
    :cond_3
    iget-object v0, p1, Lflipboard/model/ConfigFirstLaunch;->TopicPickerDefaultSections:Ljava/util/List;

    move-object v6, v0

    goto :goto_0

    .line 1789
    :cond_4
    new-instance v2, Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    .line 1790
    invoke-virtual {p0, v2, v10, v10, p3}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    .line 1791
    iput-boolean v10, v2, Lflipboard/service/Section;->B:Z

    goto :goto_2

    :cond_5
    move v1, v7

    .line 1805
    goto :goto_1

    .line 1806
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    .line 1807
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1809
    new-instance v2, Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    .line 1810
    invoke-virtual {p0, v2, v10, v10, p3}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    .line 1811
    iput-boolean v10, v2, Lflipboard/service/Section;->B:Z

    goto :goto_4

    .line 1814
    :cond_8
    return-void
.end method

.method public final a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2258
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2259
    :goto_0
    new-instance v6, Lflipboard/objs/UserState$MutedAuthor;

    invoke-direct {v6}, Lflipboard/objs/UserState$MutedAuthor;-><init>()V

    .line 2260
    iget-object v1, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    iput-object v1, v6, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    .line 2261
    iget-object v1, p1, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    iput-object v1, v6, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    .line 2262
    invoke-static {p1}, Lflipboard/gui/section/ItemDisplayUtil;->f(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    .line 2263
    iget-object v1, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iput-object v1, v6, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    .line 2264
    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 2265
    const-string v1, "mute"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    move-object v4, p2

    move-object v5, p1

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V

    .line 2266
    sget-object v0, Lflipboard/service/User$Message;->j:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, v7}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2267
    return-void

    :cond_0
    move-object v0, v7

    .line 2258
    goto :goto_0
.end method

.method public final a(Lflipboard/objs/Magazine;)V
    .locals 3

    .prologue
    .line 2103
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2104
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2105
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2106
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2110
    :cond_0
    sget-object v0, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2111
    return-void

    .line 2103
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Lflipboard/service/Account;)V
    .locals 4

    .prologue
    .line 522
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "accounts"

    const/4 v2, 0x1

    new-instance v3, Lflipboard/service/User$5;

    invoke-direct {v3, p0, p1}, Lflipboard/service/User$5;-><init>(Lflipboard/service/User;Lflipboard/service/Account;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 569
    return-void
.end method

.method public final a(Lflipboard/service/Flap$TypedResultObserver;)V
    .locals 4

    .prologue
    .line 1053
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/service/User;->m:J

    .line 1054
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    .line 1055
    invoke-direct {p0}, Lflipboard/service/User;->C()I

    move-result v1

    new-instance v2, Lflipboard/service/User$14;

    invoke-direct {v2, p0, p1}, Lflipboard/service/User$14;-><init>(Lflipboard/service/User;Lflipboard/service/Flap$TypedResultObserver;)V

    new-instance v3, Lflipboard/service/Flap$UserInfoRequest;

    invoke-direct {v3, v0, p0}, Lflipboard/service/Flap$UserInfoRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v1, v2}, Lflipboard/service/Flap$UserInfoRequest;->a(ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$UserInfoRequest;

    .line 1079
    return-void
.end method

.method public final a(Lflipboard/service/Section;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1550
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1551
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "User:loadSectionItems\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1552
    iget-object v0, p1, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1553
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Section update is in-progress "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1554
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->b()V

    .line 1558
    :goto_0
    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1559
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1560
    iget-object v2, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v3, "sections"

    new-instance v4, Lflipboard/service/User$21;

    invoke-direct {v4, p0, p1, v0, v1}, Lflipboard/service/User$21;-><init>(Lflipboard/service/User;Lflipboard/service/Section;J)V

    invoke-virtual {v2, v3, v6, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 1594
    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1595
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 1596
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1597
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, " loadSectionItems - no items to load for section\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1598
    iget-object v0, p1, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1599
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->b()V

    .line 1601
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Ljava/util/List;)V

    .line 1604
    :cond_1
    return-void

    .line 1556
    :cond_2
    iget-object v0, p1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Section update is NOT in-progress "

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_0
.end method

.method public final a(Lflipboard/service/Section;Z)V
    .locals 8

    .prologue
    .line 1834
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1835
    iget-object v6, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v7, "sections"

    new-instance v0, Lflipboard/service/User$26;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lflipboard/service/User$26;-><init>(Lflipboard/service/User;ZLflipboard/service/Section;J)V

    invoke-virtual {v6, v7, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V

    .line 1876
    if-eqz p2, :cond_0

    .line 1877
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->e(Z)V

    .line 1879
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/service/User$StateChanger;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 665
    iget-object v2, p0, Lflipboard/service/User;->w:Ljava/lang/Object;

    monitor-enter v2

    .line 668
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Lflipboard/service/User$StateChanger;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 669
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v3

    if-nez v3, :cond_2

    .line 671
    monitor-exit v2

    .line 712
    :goto_0
    return-void

    .line 675
    :cond_2
    iget-boolean v3, p0, Lflipboard/service/User;->x:Z

    if-nez v3, :cond_3

    if-nez v0, :cond_4

    .line 676
    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 700
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 679
    :cond_4
    :try_start_1
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v1, :cond_5

    .line 680
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v1, "not sync\'ng after state change: there\'s only %d sections on client!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 681
    monitor-exit v2

    goto :goto_0

    .line 685
    :cond_5
    new-instance v0, Lflipboard/objs/UserState;

    iget-object v3, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    invoke-direct {v0, v3}, Lflipboard/objs/UserState;-><init>(Lflipboard/objs/UserState;)V

    iput-object v0, p0, Lflipboard/service/User;->k:Lflipboard/objs/UserState;

    .line 686
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lflipboard/service/User;->u:J

    .line 687
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 688
    iget-object v4, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    if-eq v0, v4, :cond_6

    .line 689
    iget-object v4, p0, Lflipboard/service/User;->k:Lflipboard/objs/UserState;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v4, v4, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v4, v4, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 694
    :cond_7
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 696
    monitor-exit v2

    goto :goto_0

    .line 699
    :cond_8
    const-wide/16 v4, 0x1388

    invoke-virtual {p0, v4, v5}, Lflipboard/service/User;->a(J)V

    .line 700
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 703
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v2, "userstate"

    new-instance v3, Lflipboard/service/User$8;

    invoke-direct {v3, p0}, Lflipboard/service/User$8;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v2, v1, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    goto :goto_0
.end method

.method public final a(Lflipboard/util/Observer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 842
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 843
    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    .line 846
    if-eqz p1, :cond_0

    .line 847
    iget-object v2, p0, Lflipboard/service/User;->A:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lflipboard/service/User;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 850
    :cond_0
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 851
    sget-object v0, Lflipboard/service/User$Message;->e:Lflipboard/service/User$Message;

    invoke-direct {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$Message;)V

    .line 923
    :goto_0
    return-void

    .line 847
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 855
    :cond_1
    sget-object v0, Lflipboard/service/User$45;->b:[I

    iget-object v2, p0, Lflipboard/service/User;->y:Lflipboard/service/User$Message;

    invoke-virtual {v2}, Lflipboard/service/User$Message;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 861
    sget-object v0, Lflipboard/service/User$Message;->d:Lflipboard/service/User$Message;

    invoke-direct {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$Message;)V

    .line 863
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-nez v0, :cond_2

    const/4 v0, -0x1

    :goto_1
    new-instance v2, Lflipboard/service/User$11;

    invoke-direct {v2, p0}, Lflipboard/service/User$11;-><init>(Lflipboard/service/User;)V

    new-instance v3, Lflipboard/service/Flap$UserStateRequest;

    invoke-direct {v3, v1, p0}, Lflipboard/service/Flap$UserStateRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v0, v2}, Lflipboard/service/Flap$UserStateRequest;->a(ILflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$StateRequest;

    goto :goto_0

    .line 857
    :pswitch_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 863
    :cond_2
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    invoke-virtual {v0}, Lflipboard/objs/UserState;->a()I

    move-result v0

    goto :goto_1

    .line 855
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 260
    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot set a user\'s uid to 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_0
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    iget-object v0, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    :goto_0
    return-void

    .line 267
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "warning new uid doesn\'t match existing: %s vs %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iget-object v3, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_2
    iput-object p1, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    .line 272
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "sections"

    new-instance v2, Lflipboard/service/User$2;

    invoke-direct {v2, p0, p1}, Lflipboard/service/User$2;-><init>(Lflipboard/service/User;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v4, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 290
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    .line 293
    invoke-direct {p0}, Lflipboard/service/User;->A()V

    .line 294
    invoke-direct {p0}, Lflipboard/service/User;->B()V

    .line 296
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ac:Z

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Z)V

    .line 297
    invoke-direct {p0}, Lflipboard/service/User;->z()V

    .line 300
    invoke-virtual {p0}, Lflipboard/service/User;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 576
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "accounts"

    const/4 v2, 0x1

    new-instance v3, Lflipboard/service/User$6;

    invoke-direct {v3, p0, p1}, Lflipboard/service/User$6;-><init>(Lflipboard/service/User;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 588
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$MutedAuthor;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2271
    new-instance v0, Lflipboard/service/User$33;

    invoke-direct {v0, p0, p1, p2}, Lflipboard/service/User$33;-><init>(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 2285
    invoke-direct {p0}, Lflipboard/service/User;->G()V

    .line 2286
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 406
    new-instance v1, Ljava/util/HashSet;

    const/16 v0, 0x1e

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 407
    if-eqz p1, :cond_0

    .line 408
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 409
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 412
    :cond_0
    if-eqz p2, :cond_1

    .line 413
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 417
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 418
    iget-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 419
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 420
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 425
    :cond_3
    invoke-virtual {p0, v2}, Lflipboard/service/User;->a(Ljava/util/List;)V

    .line 428
    if-eqz p1, :cond_4

    .line 429
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 430
    new-instance v2, Lflipboard/service/Account;

    invoke-direct {v2, v0}, Lflipboard/service/Account;-><init>(Lflipboard/objs/UserService;)V

    invoke-virtual {p0, v2}, Lflipboard/service/User;->a(Lflipboard/service/Account;)V

    goto :goto_3

    .line 433
    :cond_4
    if-eqz p2, :cond_5

    .line 434
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 435
    new-instance v2, Lflipboard/service/Account;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lflipboard/service/Account;-><init>(Lflipboard/objs/UserService;Z)V

    invoke-virtual {p0, v2}, Lflipboard/service/User;->a(Lflipboard/service/Account;)V

    goto :goto_4

    .line 438
    :cond_5
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1982
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2039
    :cond_0
    :goto_0
    return-void

    .line 1987
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1989
    new-instance v1, Lflipboard/service/User$28;

    invoke-direct {v1, p0, p1, v0}, Lflipboard/service/User$28;-><init>(Lflipboard/service/User;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 2014
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 2015
    sget-object v1, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    invoke-virtual {p0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2016
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2017
    iget-object v1, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const/16 v2, 0x3e8

    new-instance v3, Lflipboard/service/User$29;

    invoke-direct {v3, p0, v0}, Lflipboard/service/User$29;-><init>(Lflipboard/service/User;Ljava/util/List;)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    goto :goto_0

    .line 2036
    :cond_2
    if-eqz p2, :cond_0

    .line 2037
    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 951
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 952
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    .line 970
    :cond_0
    :goto_0
    return-void

    .line 956
    :cond_1
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 960
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 961
    iget-object v2, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v2, v2, Lflipboard/service/Section$Meta;->b:Z

    if-nez v2, :cond_2

    .line 962
    const/4 v2, 0x1

    iput-boolean v2, v0, Lflipboard/service/Section;->B:Z

    goto :goto_1

    .line 966
    :cond_3
    invoke-virtual {p0}, Lflipboard/service/User;->g()V

    .line 969
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p1}, Lflipboard/service/FlipboardManager;->a(Z)V

    goto :goto_0
.end method

.method public final a(IIZ)Z
    .locals 3

    .prologue
    .line 1939
    if-eqz p3, :cond_1

    .line 1940
    if-ltz p1, :cond_0

    .line 1941
    add-int/lit8 p1, p1, 0x1

    .line 1943
    :cond_0
    if-ltz p2, :cond_1

    .line 1944
    add-int/lit8 p2, p2, 0x1

    .line 1949
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 1950
    :cond_2
    const/4 v0, 0x0

    .line 1969
    :goto_0
    return v0

    .line 1953
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "User:MoveSection"

    new-instance v2, Lflipboard/service/User$27;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/service/User$27;-><init>(Lflipboard/service/User;II)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1969
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2321
    iget-boolean v0, p0, Lflipboard/service/User;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "status"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2342
    :goto_0
    return v0

    .line 2325
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v0, :cond_9

    .line 2327
    new-array v4, v8, [Lflipboard/objs/FeedItem;

    aput-object p1, v4, v2

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x2

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v3

    aput-object v3, v4, v0

    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_9

    aget-object v5, v4, v3

    .line 2328
    new-instance v6, Lflipboard/objs/UserState$MutedAuthor;

    invoke-direct {v6}, Lflipboard/objs/UserState$MutedAuthor;-><init>()V

    .line 2329
    iget-object v0, v5, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    iput-object v0, v6, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    .line 2330
    iget-object v0, v5, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    iput-object v0, v6, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    .line 2331
    iget-object v0, v5, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    iput-object v0, v6, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    .line 2332
    iget-object v0, v5, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iput-object v0, v6, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    .line 2333
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v7, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    if-eqz v7, :cond_1

    iget-object v7, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v7, v7, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-nez v7, :cond_2

    :cond_1
    move v0, v2

    :goto_2
    if-eqz v0, :cond_6

    move v0, v1

    .line 2334
    goto :goto_0

    .line 2333
    :cond_2
    iget-object v7, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v7, v7, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v7, v7, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-eqz v7, :cond_3

    iget-object v7, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v7, v7, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v7, v7, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move v0, v1

    goto :goto_2

    :cond_3
    if-eqz p2, :cond_5

    iget-object v7, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v7, v7, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v7, v7, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-eqz v7, :cond_5

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-interface {v0, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 2336
    :cond_6
    iget-object v0, v5, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 2337
    if-eqz v0, :cond_8

    iget-object v5, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v6, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v6, v6, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v6, v6, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    if-eqz v6, :cond_7

    iget-object v5, v5, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v5, v5, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v5, v5, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_8

    move v0, v1

    .line 2338
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 2337
    goto :goto_3

    .line 2327
    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 2342
    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)Lflipboard/service/Account;
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 381
    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lflipboard/objs/Magazine;)V
    .locals 2

    .prologue
    .line 2143
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2144
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2145
    sget-object v0, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2147
    :cond_0
    return-void
.end method

.method final b(Lflipboard/service/Account;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1215
    .line 1216
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 1217
    if-nez p1, :cond_0

    .line 1218
    iget-object v5, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v5, v5, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v5, :cond_3

    iget-object v5, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1219
    invoke-virtual {v0, v2}, Lflipboard/service/Section;->a(Z)V

    .line 1220
    iput-boolean v3, v0, Lflipboard/service/Section;->B:Z

    move v1, v3

    .line 1221
    goto :goto_0

    .line 1224
    :cond_0
    iget-object v5, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v5, v5, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v5, :cond_3

    iget-object v5, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iget-object v6, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v6, v6, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1226
    iput-boolean v3, v0, Lflipboard/service/Section;->B:Z

    move v0, v3

    :goto_1
    move v1, v0

    .line 1229
    goto :goto_0

    .line 1230
    :cond_1
    if-eqz v1, :cond_2

    .line 1231
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->a(Z)V

    .line 1233
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final b(Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 1612
    iget-object v0, p0, Lflipboard/service/User;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1613
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2643
    if-eqz p1, :cond_0

    iput-object p1, p0, Lflipboard/service/User;->n:Ljava/util/List;

    .line 2644
    :cond_0
    return-void
.end method

.method public final b(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$MutedAuthor;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2294
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 2295
    new-instance v0, Lflipboard/service/User$34;

    invoke-direct {v0, p0, p1, p2}, Lflipboard/service/User$34;-><init>(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 2309
    invoke-direct {p0}, Lflipboard/service/User;->G()V

    .line 2312
    sget-object v0, Lflipboard/service/User$Message;->j:Lflipboard/service/User$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2313
    return-void
.end method

.method public final b(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2742
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "magazines"

    new-instance v2, Lflipboard/service/User$42;

    invoke-direct {v2, p0, p2, p1}, Lflipboard/service/User$42;-><init>(Lflipboard/service/User;ZLjava/util/List;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V

    .line 2768
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 314
    const-string v0, "flipboard"

    invoke-virtual {p0, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 977
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 978
    iget-wide v4, p0, Lflipboard/service/User;->l:J

    sub-long v4, v2, v4

    cmp-long v4, v4, p1

    if-gez v4, :cond_1

    .line 979
    sget-object v4, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v4, p0, Lflipboard/service/User;->l:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1001
    :cond_0
    :goto_0
    return v0

    .line 983
    :cond_1
    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 984
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 988
    :cond_2
    iget-object v2, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v2

    if-nez v2, :cond_0

    .line 991
    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 995
    iget-object v2, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v2

    .line 996
    invoke-virtual {v2, p0, v0, v1}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v0

    .line 997
    iget-object v2, p0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-virtual {v0, v2, v6, v6}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 998
    invoke-virtual {v0}, Lflipboard/service/Flap$UpdateRequest;->c()V

    .line 1000
    invoke-virtual {p0}, Lflipboard/service/User;->g()V

    move v0, v1

    .line 1001
    goto :goto_0
.end method

.method public final b(Lflipboard/service/Section;ZLjava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2047
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lflipboard/service/User;->b(Lflipboard/service/Section;ZZLjava/lang/String;)Z

    move-result v6

    .line 2050
    invoke-virtual {p1}, Lflipboard/service/Section;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2051
    invoke-virtual {p1}, Lflipboard/service/Section;->s()Z

    move-result v0

    .line 2052
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v7, Lflipboard/service/User$30;

    invoke-direct {v7, p0, p1, v0}, Lflipboard/service/User$30;-><init>(Lflipboard/service/User;Lflipboard/service/Section;Z)V

    new-instance v0, Lflipboard/service/Flap$FollowRequest;

    const/4 v4, 0x0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Flap$FollowRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)V

    invoke-static {v0, v7}, Lflipboard/service/Flap$FollowRequest;->a(Lflipboard/service/Flap$FollowRequest;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$FollowRequest;

    .line 2063
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-boolean v5, v0, Lflipboard/objs/TOCSection;->N:Z

    .line 2064
    sget-object v0, Lflipboard/service/User$Message;->l:Lflipboard/service/User$Message;

    invoke-virtual {p0, v0, p1}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2067
    :cond_0
    return v6
.end method

.method public final c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2910
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 2911
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v0, :cond_0

    .line 2912
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v1, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iput-object p1, v1, Lflipboard/objs/UserState$Data;->g:Ljava/util/List;

    iput-boolean v2, v0, Lflipboard/objs/UserState;->m:Z

    .line 2914
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p0, p1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 329
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 330
    iget-object v0, p0, Lflipboard/service/User;->q:Lflipboard/service/Section;

    if-nez v0, :cond_0

    .line 331
    invoke-direct {p0}, Lflipboard/service/User;->z()V

    .line 333
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->q:Lflipboard/service/Section;

    .line 335
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lflipboard/service/Section;
    .locals 2

    .prologue
    .line 447
    if-eqz p1, :cond_2

    .line 449
    invoke-direct {p0, p1}, Lflipboard/service/User;->m(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 450
    if-nez v0, :cond_0

    .line 452
    const-string v0, "auth/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 458
    :goto_0
    invoke-direct {p0, v0}, Lflipboard/service/User;->m(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 462
    :cond_0
    :goto_1
    return-object v0

    .line 455
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 462
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d(Lflipboard/service/Section;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2072
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;ZZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lflipboard/service/Section;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 492
    invoke-virtual {p0, p1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 493
    if-nez v0, :cond_0

    .line 494
    new-instance v0, Lflipboard/service/Section;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 495
    invoke-virtual {p0, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 497
    :cond_0
    return-object v0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 341
    invoke-virtual {p0}, Lflipboard/service/User;->d()Lflipboard/service/Section;

    move-result-object v0

    if-nez v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 344
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "flipboard"

    new-instance v2, Lflipboard/service/User$3;

    invoke-direct {v2, p0}, Lflipboard/service/User$3;-><init>(Lflipboard/service/User;)V

    new-instance v3, Lflipboard/service/Flap$UnreadCountRequest;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v3, v0, v4}, Lflipboard/service/Flap$UnreadCountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v1, v2}, Lflipboard/service/Flap$UnreadCountRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 611
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 613
    new-instance v2, Lflipboard/service/User$7;

    invoke-direct {v2, p0, v0, v1}, Lflipboard/service/User$7;-><init>(Lflipboard/service/User;Ljava/util/List;Ljava/util/List;)V

    invoke-direct {p0, p1, v2}, Lflipboard/service/User;->a(Ljava/lang/String;Lflipboard/util/Callback;)V

    .line 624
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 625
    iget-object v3, p0, Lflipboard/service/User;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 627
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 628
    invoke-virtual {p0, v0}, Lflipboard/service/User;->d(Lflipboard/service/Section;)Z

    goto :goto_1

    .line 630
    :cond_1
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1025
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/service/User;->l:J

    .line 1026
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "userstate"

    const/4 v2, 0x1

    new-instance v3, Lflipboard/service/User$13;

    invoke-direct {v3, p0}, Lflipboard/service/User$13;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;ZLflipboard/service/DatabaseHandler;)V

    .line 1035
    return-void
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2081
    invoke-virtual {p0, p1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v2, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;ZZLjava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;)Lflipboard/objs/Magazine;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2155
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2162
    :goto_0
    return-object v0

    .line 2158
    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    .line 2159
    iget-object v3, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2162
    goto :goto_0
.end method

.method public final h()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1526
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 1527
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1528
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1531
    :cond_1
    return-object v1
.end method

.method public final i(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2502
    const/4 v0, 0x0

    .line 2503
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, p1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 2505
    iget-boolean v2, v1, Lflipboard/objs/ConfigService;->bO:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v1, Lflipboard/objs/ConfigService;->bx:Z

    if-eqz v2, :cond_0

    .line 2506
    iget-object v0, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 2507
    if-eqz v0, :cond_3

    .line 2509
    invoke-virtual {v0}, Lflipboard/service/Account;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2510
    invoke-virtual {v0}, Lflipboard/service/Account;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512
    const-string v0, "subscribed"

    .line 2526
    :cond_0
    :goto_0
    return-object v0

    .line 2515
    :cond_1
    const-string v0, "needsUpgrade"

    goto :goto_0

    .line 2519
    :cond_2
    const-string v0, "needsSubscription"

    goto :goto_0

    .line 2523
    :cond_3
    const-string v0, "needsSubscription"

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1620
    iget-object v0, p0, Lflipboard/service/User;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1621
    return-void
.end method

.method public final j(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2530
    invoke-virtual {p0, p1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 2531
    if-eqz v0, :cond_2

    .line 2532
    invoke-virtual {v0}, Lflipboard/service/Account;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2533
    const-string v0, "entitled"

    .line 2540
    :goto_0
    return-object v0

    .line 2534
    :cond_0
    invoke-virtual {v0}, Lflipboard/service/Account;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2535
    const-string v0, "subscribed"

    goto :goto_0

    .line 2537
    :cond_1
    const-string v0, "authenticated"

    goto :goto_0

    .line 2540
    :cond_2
    const-string v0, "unauthenticated"

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1818
    new-instance v0, Lflipboard/service/User$25;

    invoke-direct {v0, p0}, Lflipboard/service/User$25;-><init>(Lflipboard/service/User;)V

    invoke-virtual {p0, v0}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 1827
    return-void
.end method

.method public final k()Lflipboard/objs/UserState;
    .locals 2

    .prologue
    .line 2388
    new-instance v0, Lflipboard/objs/UserState;

    iget-object v1, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    invoke-direct {v0, v1}, Lflipboard/objs/UserState;-><init>(Lflipboard/objs/UserState;)V

    return-object v0
.end method

.method public final k(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2544
    invoke-virtual {p0, p1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 2545
    if-eqz v0, :cond_0

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(Ljava/lang/String;)Lflipboard/objs/Magazine;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2628
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2629
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    .line 2630
    iget-object v3, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2637
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 2634
    goto :goto_0

    .line 2636
    :cond_2
    invoke-virtual {p0}, Lflipboard/service/User;->r()V

    move-object v0, v1

    .line 2637
    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2393
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v0, :cond_1

    .line 2396
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 2397
    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 2398
    if-eqz v0, :cond_0

    .line 2399
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    .line 2403
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2553
    invoke-virtual {p0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 2577
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2578
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 2579
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedItem;)V

    .line 2580
    invoke-virtual {v0, v1}, Lflipboard/service/Section;->a(Ljava/util/List;)V

    .line 2581
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3}, Lflipboard/service/User;->a(Lflipboard/service/Section;Z)V

    goto :goto_0

    .line 2583
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 2587
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 2588
    iget-object v2, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-nez v2, :cond_0

    .line 2589
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->d(Z)Z

    .line 2593
    :cond_1
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 221
    const-string v0, "show_status_updates"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "show_status_updates"

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/User;->t:Z

    .line 224
    :cond_0
    return-void
.end method

.method public final p()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2597
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598
    invoke-virtual {p0}, Lflipboard/service/User;->r()V

    .line 2599
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 2601
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->n:Ljava/util/List;

    goto :goto_0
.end method

.method public final q()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2617
    iget-object v1, p0, Lflipboard/service/User;->n:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 2618
    iget-object v1, p0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2620
    :cond_0
    iget-object v1, p0, Lflipboard/service/User;->p:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 2621
    iget-object v1, p0, Lflipboard/service/User;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2623
    :cond_1
    return-object v0
.end method

.method public final r()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2698
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    aput-object v1, v0, v2

    .line 2699
    invoke-virtual {p0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2739
    :cond_0
    :goto_0
    return-void

    .line 2703
    :cond_1
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/User$40;

    invoke-direct {v1, p0}, Lflipboard/service/User$40;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v2, v1}, Lflipboard/service/FlipboardManager;->a(ZLflipboard/service/Flap$TypedResultObserver;)V

    .line 2721
    iget-object v0, p0, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/User$41;

    invoke-direct {v1, p0}, Lflipboard/service/User$41;-><init>(Lflipboard/service/User;)V

    invoke-virtual {v0, v3, v1}, Lflipboard/service/FlipboardManager;->a(ZLflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_0
.end method

.method public final s()Lflipboard/json/FLObject;
    .locals 2

    .prologue
    .line 2772
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-nez v0, :cond_0

    .line 2773
    const/4 v0, 0x0

    .line 2775
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v1, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    if-nez v0, :cond_1

    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    goto :goto_0

    :cond_1
    new-instance v0, Lflipboard/json/FLObject;

    iget-object v1, v1, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    invoke-direct {v0, v1}, Lflipboard/json/FLObject;-><init>(Lflipboard/json/FLObject;)V

    goto :goto_0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 2813
    iget-object v0, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2832
    const-string v0, "User[uid=%s: %d sections, %d accounts]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2823
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 2824
    iget-object v0, p0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 2825
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    goto :goto_0

    .line 2827
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 2828
    return-void
.end method

.method public final w()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2885
    iget-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2886
    :cond_0
    const-string v0, ""

    .line 2896
    :goto_0
    return-object v0

    .line 2888
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2889
    iget-object v0, p0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2890
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2891
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2892
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2893
    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2896
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final x()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2901
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v0, :cond_0

    .line 2902
    iget-object v0, p0, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->g:Ljava/util/List;

    .line 2904
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
