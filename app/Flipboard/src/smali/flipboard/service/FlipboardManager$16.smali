.class public Lflipboard/service/FlipboardManager$16;
.super Lflipboard/service/Flap$AccountRequestObserver;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/util/Observer;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V
    .locals 0

    .prologue
    .line 1443
    iput-object p1, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$16;->a:Lflipboard/util/Observer;

    invoke-direct {p0}, Lflipboard/service/Flap$AccountRequestObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1459
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "failure [%s]: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1460
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1463
    iget-object v1, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->e(Lflipboard/service/FlipboardManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    move p1, v0

    .line 1469
    :cond_0
    if-nez p1, :cond_1

    .line 1470
    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->a:Lflipboard/util/Observer;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->c:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    invoke-interface {v0, v1, v2, p2}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1474
    :goto_0
    return-void

    .line 1472
    :cond_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->a:Lflipboard/util/Observer;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->b:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    invoke-interface {v0, v1, v2, p2}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1443
    check-cast p1, Lflipboard/objs/UserInfo;

    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v0

    iget-object v1, p1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    iget-object v2, p1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v0

    iget-object v1, p1, Lflipboard/objs/UserInfo;->n:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->a:Lflipboard/util/Observer;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    invoke-interface {v0, v1, v2, v3}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->d(Lflipboard/service/FlipboardManager;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->e:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$16;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/FlipboardManager;)Lflipboard/service/User;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
