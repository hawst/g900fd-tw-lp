.class Lflipboard/service/Flap$SectionInfo;
.super Ljava/lang/Object;
.source "Flap.java"


# instance fields
.field final a:Lflipboard/service/Section;

.field final b:Ljava/lang/String;

.field final c:Lflipboard/service/Flap$UpdateObserver;

.field final d:Landroid/os/Bundle;

.field e:I

.field f:J

.field private final g:Z


# direct methods
.method constructor <init>(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;Lflipboard/service/Flap$UpdateObserver;)V
    .locals 1

    .prologue
    .line 843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 844
    iput-object p1, p0, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    .line 845
    iput-object p2, p0, Lflipboard/service/Flap$SectionInfo;->b:Ljava/lang/String;

    .line 846
    iput-object p4, p0, Lflipboard/service/Flap$SectionInfo;->c:Lflipboard/service/Flap$UpdateObserver;

    .line 847
    iput-object p3, p0, Lflipboard/service/Flap$SectionInfo;->d:Landroid/os/Bundle;

    .line 848
    invoke-virtual {p1}, Lflipboard/service/Section;->l()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/Flap$SectionInfo;->g:Z

    .line 849
    return-void
.end method

.method static synthetic a(Lflipboard/service/Flap$SectionInfo;)Z
    .locals 1

    .prologue
    .line 832
    iget-boolean v0, p0, Lflipboard/service/Flap$SectionInfo;->g:Z

    return v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
