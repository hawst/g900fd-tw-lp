.class Lflipboard/service/Flap$SearchResultStreamer;
.super Lflipboard/service/Flap$StreamingJSONInputStream;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/service/Flap$StreamingJSONInputStream",
        "<",
        "Lflipboard/objs/SearchResultItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 5039
    iput-object p1, p0, Lflipboard/service/Flap$SearchResultStreamer;->a:Lflipboard/service/Flap;

    .line 5040
    invoke-direct {p0, p2}, Lflipboard/service/Flap$StreamingJSONInputStream;-><init>(Ljava/io/InputStream;)V

    .line 5041
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/Flap$SearchResultStreamer;->g:Z

    .line 5042
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5036
    iget-object v0, p0, Lflipboard/service/Flap$SearchResultStreamer;->e:Lflipboard/json/JSONParser;

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->r()Lflipboard/objs/SearchResultItem;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x1000

    const/4 v4, 0x0

    .line 5051
    new-array v0, v5, [B

    .line 5052
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 5054
    :goto_0
    iget-object v2, p0, Lflipboard/service/Flap$SearchResultStreamer;->in:Ljava/io/InputStream;

    invoke-virtual {v2, v0, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 5055
    invoke-virtual {v1, v0, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 5057
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 5058
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/Flap$SearchResultStreamer;->b:[B

    .line 5059
    iget-object v0, p0, Lflipboard/service/Flap$SearchResultStreamer;->b:[B

    iget-object v1, p0, Lflipboard/service/Flap$SearchResultStreamer;->b:[B

    array-length v1, v1

    invoke-virtual {p0, v0, v4, v1}, Lflipboard/service/Flap$SearchResultStreamer;->a([BII)V

    .line 5060
    iget-object v0, p0, Lflipboard/service/Flap$SearchResultStreamer;->e:Lflipboard/json/JSONParser;

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->s()Lflipboard/objs/SearchResultStream;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/SearchResultStream;->a:Ljava/util/List;

    return-object v0
.end method
