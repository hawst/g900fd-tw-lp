.class Lflipboard/service/Flap$AdQueryRequest;
.super Lflipboard/service/Flap$RetryRequest;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$TypedResultObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/Ad;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:J

.field private q:I

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:I


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 5299
    iput-object p1, p0, Lflipboard/service/Flap$AdQueryRequest;->b:Lflipboard/service/Flap;

    .line 5300
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$RetryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 5301
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Lflipboard/service/Flap$TypedResultObserver;Ljava/lang/String;I)Lflipboard/service/Flap$AdQueryRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/Ad;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Lflipboard/service/Flap$AdQueryRequest;"
        }
    .end annotation

    .prologue
    .line 5310
    iput-object p3, p0, Lflipboard/service/Flap$AdQueryRequest;->g:Ljava/lang/String;

    .line 5311
    iput p5, p0, Lflipboard/service/Flap$AdQueryRequest;->i:I

    .line 5312
    iput-object p1, p0, Lflipboard/service/Flap$AdQueryRequest;->c:Ljava/lang/String;

    .line 5313
    iput-object p2, p0, Lflipboard/service/Flap$AdQueryRequest;->f:Ljava/lang/String;

    .line 5314
    iput-boolean p4, p0, Lflipboard/service/Flap$AdQueryRequest;->h:Z

    .line 5315
    iput p6, p0, Lflipboard/service/Flap$AdQueryRequest;->j:I

    .line 5316
    iput-object p7, p0, Lflipboard/service/Flap$AdQueryRequest;->k:Ljava/lang/String;

    .line 5317
    iput-object p8, p0, Lflipboard/service/Flap$AdQueryRequest;->l:Ljava/lang/String;

    .line 5318
    iput-wide p9, p0, Lflipboard/service/Flap$AdQueryRequest;->m:J

    .line 5319
    iput p11, p0, Lflipboard/service/Flap$AdQueryRequest;->q:I

    .line 5320
    iput-object p12, p0, Lflipboard/service/Flap$AdQueryRequest;->r:Ljava/lang/String;

    .line 5321
    move-object/from16 v0, p13

    iput-object v0, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 5322
    move-object/from16 v0, p14

    iput-object v0, p0, Lflipboard/service/Flap$AdQueryRequest;->s:Ljava/lang/String;

    .line 5323
    move/from16 v0, p15

    iput v0, p0, Lflipboard/service/Flap$AdQueryRequest;->t:I

    .line 5324
    invoke-super {p0}, Lflipboard/service/Flap$RetryRequest;->c()V

    .line 5325
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Lflipboard/service/Flap$TypedResultObserver;Ljava/lang/String;I)Lflipboard/service/Flap$AdQueryRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/String;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Lflipboard/objs/Ad;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Lflipboard/service/Flap$AdQueryRequest;"
        }
    .end annotation

    .prologue
    .line 5333
    iput-object p1, p0, Lflipboard/service/Flap$AdQueryRequest;->c:Ljava/lang/String;

    .line 5334
    iput-object p2, p0, Lflipboard/service/Flap$AdQueryRequest;->f:Ljava/lang/String;

    .line 5335
    iput-boolean p3, p0, Lflipboard/service/Flap$AdQueryRequest;->h:Z

    .line 5336
    iput p4, p0, Lflipboard/service/Flap$AdQueryRequest;->j:I

    .line 5337
    iput-object p5, p0, Lflipboard/service/Flap$AdQueryRequest;->k:Ljava/lang/String;

    .line 5338
    iput-object p6, p0, Lflipboard/service/Flap$AdQueryRequest;->l:Ljava/lang/String;

    .line 5339
    iput-wide p7, p0, Lflipboard/service/Flap$AdQueryRequest;->m:J

    .line 5340
    iput p9, p0, Lflipboard/service/Flap$AdQueryRequest;->q:I

    .line 5341
    iput-object p10, p0, Lflipboard/service/Flap$AdQueryRequest;->r:Ljava/lang/String;

    .line 5342
    iput-object p11, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    .line 5343
    iput-object p12, p0, Lflipboard/service/Flap$AdQueryRequest;->s:Ljava/lang/String;

    .line 5344
    iput p13, p0, Lflipboard/service/Flap$AdQueryRequest;->t:I

    .line 5345
    invoke-super {p0}, Lflipboard/service/Flap$RetryRequest;->c()V

    .line 5346
    return-object p0
.end method

.method protected final a(Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    .line 5351
    const-string v0, "success"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5352
    const-string v0, "ad"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5354
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v1, Lflipboard/json/JSONParser;

    const-string v2, "ad"

    invoke-virtual {p1, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->b()Lflipboard/objs/Ad;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5365
    :goto_0
    return-void

    .line 5355
    :catch_0
    move-exception v0

    .line 5356
    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failed to parse result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 5359
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No ad key in result object: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 5363
    :cond_1
    iget-object v0, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    const-string v1, "errormessage"

    invoke-virtual {p1, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5369
    iget-object v0, p0, Lflipboard/service/Flap$AdQueryRequest;->a:Lflipboard/service/Flap$TypedResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    .line 5370
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5376
    iget v0, p0, Lflipboard/service/Flap$AdQueryRequest;->q:I

    if-lez v0, :cond_4

    iget v0, p0, Lflipboard/service/Flap$AdQueryRequest;->q:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5377
    :goto_0
    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->k:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->l:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5379
    :cond_0
    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->b:Lflipboard/service/Flap;

    const-string v2, "/query"

    iget-object v3, p0, Lflipboard/service/Flap$AdQueryRequest;->n:Lflipboard/service/User;

    const/16 v4, 0x14

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "feed_id"

    aput-object v5, v4, v6

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->c:Ljava/lang/String;

    aput-object v5, v4, v7

    const-string v5, "partner_id"

    aput-object v5, v4, v8

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->f:Ljava/lang/String;

    aput-object v5, v4, v9

    const-string v5, "page_index"

    aput-object v5, v4, v10

    const/4 v5, 0x5

    iget v6, p0, Lflipboard/service/Flap$AdQueryRequest;->j:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "impression_value"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    iget-object v6, p0, Lflipboard/service/Flap$AdQueryRequest;->k:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "impression_event"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    iget-object v6, p0, Lflipboard/service/Flap$AdQueryRequest;->l:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "impression_timestamp"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    iget-wide v6, p0, Lflipboard/service/Flap$AdQueryRequest;->m:J

    .line 5380
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "pages_since_last_ad"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    aput-object v0, v4, v5

    const/16 v0, 0xe

    const-string v5, "subscription_status"

    aput-object v5, v4, v0

    const/16 v0, 0xf

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->r:Ljava/lang/String;

    aput-object v5, v4, v0

    const/16 v0, 0x10

    const-string v5, "connection_type"

    aput-object v5, v4, v0

    const/16 v0, 0x11

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->s:Ljava/lang/String;

    aput-object v5, v4, v0

    const/16 v0, 0x12

    const-string v5, "connection_subtype"

    aput-object v5, v4, v0

    const/16 v0, 0x13

    iget v5, p0, Lflipboard/service/Flap$AdQueryRequest;->t:I

    .line 5382
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 5379
    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->b(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 5390
    :goto_1
    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&franchise_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pages_remaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lflipboard/service/Flap$AdQueryRequest;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5395
    :cond_1
    invoke-static {}, Lflipboard/service/FLAdManager;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&disable_frequency_capping=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5398
    :cond_2
    iget-boolean v1, p0, Lflipboard/service/Flap$AdQueryRequest;->h:Z

    if-eqz v1, :cond_3

    .line 5399
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&refresh=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5402
    :cond_3
    return-object v0

    .line 5376
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 5385
    :cond_5
    iget-object v1, p0, Lflipboard/service/Flap$AdQueryRequest;->b:Lflipboard/service/Flap;

    const-string v2, "/query"

    iget-object v3, p0, Lflipboard/service/Flap$AdQueryRequest;->n:Lflipboard/service/User;

    const/16 v4, 0xe

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "feed_id"

    aput-object v5, v4, v6

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->c:Ljava/lang/String;

    aput-object v5, v4, v7

    const-string v5, "partner_id"

    aput-object v5, v4, v8

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->f:Ljava/lang/String;

    aput-object v5, v4, v9

    const-string v5, "page_index"

    aput-object v5, v4, v10

    const/4 v5, 0x5

    iget v6, p0, Lflipboard/service/Flap$AdQueryRequest;->j:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "pages_since_last_ad"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object v0, v4, v5

    const/16 v0, 0x8

    const-string v5, "subscription_status"

    aput-object v5, v4, v0

    const/16 v0, 0x9

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->r:Ljava/lang/String;

    aput-object v5, v4, v0

    const/16 v0, 0xa

    const-string v5, "connection_type"

    aput-object v5, v4, v0

    const/16 v0, 0xb

    iget-object v5, p0, Lflipboard/service/Flap$AdQueryRequest;->s:Ljava/lang/String;

    aput-object v5, v4, v0

    const/16 v0, 0xc

    const-string v5, "connection_subtype"

    aput-object v5, v4, v0

    const/16 v0, 0xd

    iget v5, p0, Lflipboard/service/Flap$AdQueryRequest;->t:I

    .line 5387
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 5385
    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->b(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method
