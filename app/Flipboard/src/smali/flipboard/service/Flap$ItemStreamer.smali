.class Lflipboard/service/Flap$ItemStreamer;
.super Lflipboard/service/Flap$StreamingJSONInputStream;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/service/Flap$StreamingJSONInputStream",
        "<",
        "Lflipboard/objs/FeedItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 4981
    iput-object p1, p0, Lflipboard/service/Flap$ItemStreamer;->a:Lflipboard/service/Flap;

    .line 4982
    invoke-direct {p0, p2}, Lflipboard/service/Flap$StreamingJSONInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4983
    return-void
.end method

.method private a(Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4994
    if-nez p1, :cond_1

    .line 5019
    :cond_0
    :goto_0
    return-void

    .line 4998
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->O()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4999
    :cond_2
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 5000
    invoke-direct {p0, v0}, Lflipboard/service/Flap$ItemStreamer;->a(Lflipboard/objs/FeedItem;)V

    goto :goto_1

    .line 5003
    :cond_3
    iget-object v0, p1, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 5004
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "No text, but excerptText is not null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5005
    iput-object v3, p1, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    .line 5007
    :cond_4
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    .line 5008
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    .line 5009
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aG:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 5010
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aG:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    .line 5011
    iput-object v3, p1, Lflipboard/objs/FeedItem;->aG:Ljava/lang/String;

    .line 5017
    :cond_5
    iput-object v3, p1, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4978
    iget-object v0, p0, Lflipboard/service/Flap$ItemStreamer;->e:Lflipboard/json/JSONParser;

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/service/Flap$ItemStreamer;->a(Lflipboard/objs/FeedItem;)V

    return-object v0
.end method
