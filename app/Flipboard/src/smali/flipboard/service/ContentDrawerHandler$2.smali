.class Lflipboard/service/ContentDrawerHandler$2;
.super Ljava/lang/Thread;
.source "ContentDrawerHandler.java"


# instance fields
.field final synthetic a:Lflipboard/service/ContentDrawerHandler;


# direct methods
.method constructor <init>(Lflipboard/service/ContentDrawerHandler;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    new-instance v1, Lflipboard/objs/ConfigFolder;

    invoke-direct {v1}, Lflipboard/objs/ConfigFolder;-><init>()V

    iput-object v1, v0, Lflipboard/service/ContentDrawerHandler;->h:Lflipboard/objs/ConfigFolder;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->h:Lflipboard/objs/ConfigFolder;

    const-string v2, "_favorites_"

    iput-object v2, v1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->h:Lflipboard/objs/ConfigFolder;

    iget-object v2, v0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    const v3, 0x7f0d024c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->h:Lflipboard/objs/ConfigFolder;

    const-string v2, "content_guide_flipboard"

    iput-object v2, v1, Lflipboard/objs/ConfigFolder;->bR:Ljava/lang/String;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    const/4 v2, 0x1

    iget-object v3, v0, Lflipboard/service/ContentDrawerHandler;->h:Lflipboard/objs/ConfigFolder;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    new-instance v1, Lflipboard/objs/ConfigFolder;

    invoke-direct {v1}, Lflipboard/objs/ConfigFolder;-><init>()V

    iput-object v1, v0, Lflipboard/service/ContentDrawerHandler;->i:Lflipboard/objs/ConfigFolder;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->i:Lflipboard/objs/ConfigFolder;

    const-string v2, "_favorites_"

    iput-object v2, v1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->i:Lflipboard/objs/ConfigFolder;

    iget-object v2, v0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    const v3, 0x7f0d0159

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->i:Lflipboard/objs/ConfigFolder;

    const-string v2, "content_guide_flipboard"

    iput-object v2, v1, Lflipboard/objs/ConfigFolder;->bR:Ljava/lang/String;

    iget-object v1, v0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    const/4 v2, 0x2

    iget-object v3, v0, Lflipboard/service/ContentDrawerHandler;->i:Lflipboard/objs/ConfigFolder;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-boolean v1, v0, Lflipboard/service/ContentDrawerHandler;->e:Z

    if-nez v1, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "contentGuide.json"

    new-instance v3, Lflipboard/service/ContentDrawerHandler$3;

    invoke-direct {v3, v0}, Lflipboard/service/ContentDrawerHandler$3;-><init>(Lflipboard/service/ContentDrawerHandler;)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    :cond_0
    iget-boolean v1, v0, Lflipboard/service/ContentDrawerHandler;->d:Z

    if-nez v1, :cond_1

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "services.json"

    new-instance v3, Lflipboard/service/ContentDrawerHandler$4;

    invoke-direct {v3, v0}, Lflipboard/service/ContentDrawerHandler$4;-><init>(Lflipboard/service/ContentDrawerHandler;)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    .line 194
    :cond_1
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 195
    :goto_0
    :try_start_0
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->d(Lflipboard/service/ContentDrawerHandler;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->e(Lflipboard/service/ContentDrawerHandler;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 197
    :cond_2
    :try_start_1
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 203
    :cond_3
    :try_start_3
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 204
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler$2;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v3}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 206
    :cond_4
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method
