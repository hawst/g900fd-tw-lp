.class Lflipboard/service/audio/FLAudioManager$1;
.super Ljava/lang/Object;
.source "FLAudioManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lflipboard/service/audio/FLAudioManager;


# direct methods
.method constructor <init>(Lflipboard/service/audio/FLAudioManager;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 182
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    .line 184
    check-cast p2, Lflipboard/service/audio/MediaPlayerServiceBinder;

    .line 185
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    iget-object v1, p2, Lflipboard/service/audio/MediaPlayerServiceBinder;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0, v1}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/MediaPlayerService;

    .line 187
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/MediaPlayerService;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    iget-object v2, v0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v2, v1}, Lflipboard/util/Observable$Proxy;->b(Lflipboard/util/Observer;)V

    iget-object v0, v0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    iget-object v0, v0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "MediaPlayerService has more than 1 observers, this should not happen"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/MediaPlayerService;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/audio/MediaPlayerService;->c()V

    .line 192
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 195
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->a:Z

    if-eqz v0, :cond_3

    .line 196
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/MediaPlayerService;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v1}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/audio/FLAudioManager$AudioItem;->c:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v2}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/audio/FLAudioManager$AudioItem;->d:Lflipboard/service/Section;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 223
    :cond_1
    :goto_0
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    .line 233
    :cond_2
    :goto_1
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->e:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 234
    return-void

    .line 200
    :cond_3
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    const-string v1, "play"

    if-ne v0, v1, :cond_5

    .line 204
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->a:Z

    if-eqz v0, :cond_4

    .line 205
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v1}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/audio/FLAudioManager$AudioItem;->c:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v2}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/audio/FLAudioManager$AudioItem;->d:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v3}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v3

    iget-object v3, v3, Lflipboard/service/audio/FLAudioManager$AudioItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_4
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v1}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/audio/FLAudioManager$AudioItem;->e:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v2}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/audio/FLAudioManager$AudioItem;->f:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v3}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v3

    iget-object v3, v3, Lflipboard/service/audio/FLAudioManager$AudioItem;->g:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v4}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v4

    iget-object v4, v4, Lflipboard/service/audio/FLAudioManager$AudioItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_5
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->a:Z

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/MediaPlayerService;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v1}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/audio/FLAudioManager$AudioItem;->c:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v2}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/audio/FLAudioManager$AudioItem;->d:Lflipboard/service/Section;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 214
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    const-string v1, "next"

    if-ne v0, v1, :cond_6

    .line 215
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->j()V

    goto/16 :goto_0

    .line 217
    :cond_6
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->k()V

    goto/16 :goto_0

    .line 226
    :cond_7
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 227
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "bound to MediaPlayerService but no song loaded and no song to play"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/MediaPlayerService;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v1}, Lflipboard/service/audio/FLAudioManager;->d(Lflipboard/service/audio/FLAudioManager;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/audio/MediaPlayerService;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    .line 240
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-static {v0, v2}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/service/audio/FLAudioManager;Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/MediaPlayerService;

    .line 242
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager$1;->a:Lflipboard/service/audio/FLAudioManager;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->f:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 243
    return-void
.end method
