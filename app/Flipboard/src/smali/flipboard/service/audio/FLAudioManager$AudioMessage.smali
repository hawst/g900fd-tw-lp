.class public final enum Lflipboard/service/audio/FLAudioManager$AudioMessage;
.super Ljava/lang/Enum;
.source "FLAudioManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/audio/FLAudioManager$AudioMessage;

.field public static final enum b:Lflipboard/service/audio/FLAudioManager$AudioMessage;

.field public static final enum c:Lflipboard/service/audio/FLAudioManager$AudioMessage;

.field public static final enum d:Lflipboard/service/audio/FLAudioManager$AudioMessage;

.field public static final enum e:Lflipboard/service/audio/FLAudioManager$AudioMessage;

.field public static final enum f:Lflipboard/service/audio/FLAudioManager$AudioMessage;

.field private static final synthetic g:[Lflipboard/service/audio/FLAudioManager$AudioMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const-string v1, "PLAYERSTATE_CHANGED"

    invoke-direct {v0, v1, v3}, Lflipboard/service/audio/FLAudioManager$AudioMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->a:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const-string v1, "SONG_CHANGED"

    invoke-direct {v0, v1, v4}, Lflipboard/service/audio/FLAudioManager$AudioMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->b:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const-string v1, "PLAYER_ERROR"

    invoke-direct {v0, v1, v5}, Lflipboard/service/audio/FLAudioManager$AudioMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->c:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const-string v1, "MEDIAPLAYERSERVICE_UNREACHABLE"

    invoke-direct {v0, v1, v6}, Lflipboard/service/audio/FLAudioManager$AudioMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->d:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const-string v1, "AUDIO_BECAME_ACTIVE"

    invoke-direct {v0, v1, v7}, Lflipboard/service/audio/FLAudioManager$AudioMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->e:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const-string v1, "AUDIO_BECAME_INACTIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/audio/FLAudioManager$AudioMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->f:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    .line 22
    const/4 v0, 0x6

    new-array v0, v0, [Lflipboard/service/audio/FLAudioManager$AudioMessage;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->a:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->b:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->c:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->d:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioMessage;->e:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/audio/FLAudioManager$AudioMessage;->f:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->g:[Lflipboard/service/audio/FLAudioManager$AudioMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/audio/FLAudioManager$AudioMessage;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/service/audio/FLAudioManager$AudioMessage;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->g:[Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-virtual {v0}, [Lflipboard/service/audio/FLAudioManager$AudioMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/audio/FLAudioManager$AudioMessage;

    return-object v0
.end method
