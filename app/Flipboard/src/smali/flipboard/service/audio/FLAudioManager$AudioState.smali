.class public final enum Lflipboard/service/audio/FLAudioManager$AudioState;
.super Ljava/lang/Enum;
.source "FLAudioManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/audio/FLAudioManager$AudioState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/audio/FLAudioManager$AudioState;

.field public static final enum b:Lflipboard/service/audio/FLAudioManager$AudioState;

.field public static final enum c:Lflipboard/service/audio/FLAudioManager$AudioState;

.field private static final synthetic d:[Lflipboard/service/audio/FLAudioManager$AudioState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioState;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, Lflipboard/service/audio/FLAudioManager$AudioState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioState;

    const-string v1, "NOT_PLAYING"

    invoke-direct {v0, v1, v3}, Lflipboard/service/audio/FLAudioManager$AudioState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioState;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v4}, Lflipboard/service/audio/FLAudioManager$AudioState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/service/audio/FLAudioManager$AudioState;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->d:[Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/audio/FLAudioManager$AudioState;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lflipboard/service/audio/FLAudioManager$AudioState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/audio/FLAudioManager$AudioState;

    return-object v0
.end method

.method public static values()[Lflipboard/service/audio/FLAudioManager$AudioState;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->d:[Lflipboard/service/audio/FLAudioManager$AudioState;

    invoke-virtual {v0}, [Lflipboard/service/audio/FLAudioManager$AudioState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/audio/FLAudioManager$AudioState;

    return-object v0
.end method
