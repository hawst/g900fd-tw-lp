.class Lflipboard/service/audio/MediaPlayerService$5;
.super Ljava/util/TimerTask;
.source "MediaPlayerService.java"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lflipboard/service/audio/MediaPlayerService;


# direct methods
.method constructor <init>(Lflipboard/service/audio/MediaPlayerService;)V
    .locals 1

    .prologue
    .line 637
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->a:Z

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 641
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    monitor-enter v1

    .line 643
    :try_start_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 644
    monitor-exit v1

    .line 653
    :goto_0
    return-void

    .line 647
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->f(Lflipboard/service/audio/MediaPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->c(Lflipboard/service/audio/MediaPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v2, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v2, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v2, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 648
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 650
    new-instance v1, Lflipboard/notifications/MediaPlayerNotification;

    iget-boolean v2, p0, Lflipboard/service/audio/MediaPlayerService$5;->a:Z

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v3}, Lflipboard/service/audio/MediaPlayerService;->b(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/Song;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lflipboard/notifications/MediaPlayerNotification;-><init>(ZLflipboard/service/audio/Song;Z)V

    .line 651
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    iget v2, v1, Lflipboard/notifications/FLNotification;->c:I

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v3}, Lflipboard/service/audio/MediaPlayerService;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflipboard/notifications/MediaPlayerNotification;->c(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/service/audio/MediaPlayerService;->startForeground(ILandroid/app/Notification;)V

    .line 652
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService$5;->b:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v2}, Lflipboard/service/audio/MediaPlayerService;->b(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/Song;

    move-result-object v2

    iget-object v1, v1, Lflipboard/notifications/MediaPlayerNotification;->e:Landroid/graphics/Bitmap;

    invoke-static {v0, v2, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;Lflipboard/service/audio/Song;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 647
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
