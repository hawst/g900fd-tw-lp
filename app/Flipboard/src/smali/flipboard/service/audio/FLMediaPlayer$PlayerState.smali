.class public final enum Lflipboard/service/audio/FLMediaPlayer$PlayerState;
.super Ljava/lang/Enum;
.source "FLMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/audio/FLMediaPlayer$PlayerState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field public static final enum b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field public static final enum c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field public static final enum d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field public static final enum e:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field public static final enum f:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field public static final enum g:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field private static final synthetic h:[Lflipboard/service/audio/FLMediaPlayer$PlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->a:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v4}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "PREPARED"

    invoke-direct {v0, v1, v5}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v6}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v7}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->e:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "STOPPED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->f:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const-string v1, "ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->g:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    .line 24
    const/4 v0, 0x7

    new-array v0, v0, [Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->a:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->e:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->f:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->g:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->h:[Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/audio/FLMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    return-object v0
.end method

.method public static values()[Lflipboard/service/audio/FLMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->h:[Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-virtual {v0}, [Lflipboard/service/audio/FLMediaPlayer$PlayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    return-object v0
.end method
