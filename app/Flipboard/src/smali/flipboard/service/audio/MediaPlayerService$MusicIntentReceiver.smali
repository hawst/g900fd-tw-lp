.class public Lflipboard/service/audio/MediaPlayerService$MusicIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaPlayerService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1458
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1462
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p2, v0, v4

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1464
    const-string v0, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    if-eqz v0, :cond_0

    .line 1466
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "audioBecomingNoisy"

    invoke-virtual {v0, v1, v3}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    .line 1469
    :cond_0
    const-string v0, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    if-eqz v0, :cond_1

    .line 1470
    const-string v0, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 1471
    sget-object v1, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object v0, v1, v4

    .line 1474
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 1476
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1509
    :cond_1
    :goto_0
    return-void

    .line 1478
    :sswitch_0
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1479
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "mediaButtonPlay"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1482
    :sswitch_1
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1483
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "mediaButtonStop"

    invoke-virtual {v0, v1, v3}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1486
    :sswitch_2
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1487
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "mediaButtonPause"

    invoke-virtual {v0, v1, v3}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1490
    :sswitch_3
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1491
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "mediaButtonPlayPause"

    invoke-static {v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;Ljava/lang/String;)V

    goto :goto_0

    .line 1494
    :sswitch_4
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1495
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "mediaButtonPrevious"

    invoke-virtual {v0, v4, v4, v1}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 1498
    :sswitch_5
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1499
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "mediaButtonNext"

    invoke-virtual {v0, v3, v4, v1}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 1502
    :sswitch_6
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1503
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "headSetHook"

    invoke-static {v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;Ljava/lang/String;)V

    goto :goto_0

    .line 1476
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_6
        0x55 -> :sswitch_3
        0x56 -> :sswitch_1
        0x57 -> :sswitch_5
        0x58 -> :sswitch_4
        0x7e -> :sswitch_0
        0x7f -> :sswitch_2
    .end sparse-switch
.end method
