.class Lflipboard/service/audio/MediaPlayerService$2;
.super Ljava/lang/Object;
.source "MediaPlayerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/audio/MediaPlayerService;


# direct methods
.method constructor <init>(Lflipboard/service/audio/MediaPlayerService;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 443
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/service/audio/FLMediaPlayer;->a(Z)V

    .line 446
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->setAudioStreamType(I)V

    .line 448
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->b(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/Song;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    .line 449
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    monitor-enter v1

    .line 453
    :try_start_0
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v2}, Lflipboard/service/audio/MediaPlayerService;->c(Lflipboard/service/audio/MediaPlayerService;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 454
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->d(Lflipboard/service/audio/MediaPlayerService;)Z

    .line 455
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->e(Lflipboard/service/audio/MediaPlayerService;)Z

    .line 456
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    :goto_0
    return-void

    .line 458
    :cond_0
    monitor-exit v1

    .line 464
    :try_start_1
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/service/audio/FLMediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v0}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->prepareAsync()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 470
    :catch_0
    move-exception v0

    .line 471
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 474
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService$2;->a:Lflipboard/service/audio/MediaPlayerService;

    invoke-static {v1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;

    move-result-object v1

    const v2, 0x864701

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/audio/MediaPlayerService;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0

    .line 458
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
