.class public Lflipboard/service/audio/FLAudioManager;
.super Lflipboard/util/Observable;
.source "FLAudioManager.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/util/Observable",
        "<",
        "Lflipboard/service/audio/FLAudioManager;",
        "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
        "Ljava/lang/Object;",
        ">;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/audio/MediaPlayerService;",
        "Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Lflipboard/service/audio/FLAudioManager;

.field public static b:Lflipboard/util/Log;


# instance fields
.field public c:Lflipboard/service/audio/MediaPlayerService;

.field public d:Lflipboard/service/audio/FLAudioManager$AudioItem;

.field private e:Landroid/content/Context;

.field private f:Landroid/content/Intent;

.field private g:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "audio"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lflipboard/util/Observable;-><init>()V

    .line 46
    iput-object p1, p0, Lflipboard/service/audio/FLAudioManager;->e:Landroid/content/Context;

    .line 47
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lflipboard/service/audio/FLAudioManager;->f:Landroid/content/Intent;

    .line 48
    sput-object p0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    .line 49
    return-void
.end method

.method static synthetic a(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/MediaPlayerService;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    return-object v0
.end method

.method static synthetic a(Lflipboard/service/audio/FLAudioManager;Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/MediaPlayerService;
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    return-object p1
.end method

.method public static a(Lflipboard/objs/FeedItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 447
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 448
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 454
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 451
    goto :goto_0

    :cond_1
    move v0, v1

    .line 454
    goto :goto_0
.end method

.method static synthetic b(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    return-object v0
.end method

.method static synthetic c(Lflipboard/service/audio/FLAudioManager;)Lflipboard/service/audio/FLAudioManager$AudioItem;
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 463
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 464
    const-string v1, "id"

    const-string v2, "Audio ControlsUsed"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 465
    const-string v1, "control"

    invoke-virtual {v0, v1, p0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 466
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 467
    return-void
.end method

.method static synthetic d(Lflipboard/service/audio/FLAudioManager;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->f:Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v2, v1, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v2, :cond_0

    iget-object v0, v1, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 107
    :cond_0
    return v0
.end method

.method public final a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioItem;

    invoke-direct {v0, p1, p2, p3}, Lflipboard/service/audio/FLAudioManager$AudioItem;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    .line 58
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    const-string v1, "play"

    iput-object v1, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    .line 59
    invoke-virtual {p0, v3}, Lflipboard/service/audio/FLAudioManager;->a(Z)V

    .line 69
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-static {p1}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Can\'t play this item, it\'s not an audio item: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v0, p1, p2, p3}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 17
    check-cast p2, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    sget-object v0, Lflipboard/service/audio/FLAudioManager$2;->a:[I

    invoke-virtual {p2}, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    check-cast p3, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->a:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-virtual {p0, v0, p3}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->b:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-virtual {p0, v0, p3}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->c:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-virtual {p0, v0, p3}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    check-cast p3, Lflipboard/service/audio/FLAudioManager$AudioItem;

    iput-object p3, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    .line 135
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioItem;

    invoke-direct {v0, p1, p2, p3, p4}, Lflipboard/service/audio/FLAudioManager$AudioItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    .line 78
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    const-string v1, "play"

    iput-object v1, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v2}, Lflipboard/service/audio/FLAudioManager;->a(Z)V

    .line 89
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    if-nez v0, :cond_1

    .line 84
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "No mediaPlayerService available, we can\'t play"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 175
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 177
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->g:Landroid/content/ServiceConnection;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lflipboard/service/audio/FLAudioManager$1;

    invoke-direct {v0, p0}, Lflipboard/service/audio/FLAudioManager$1;-><init>(Lflipboard/service/audio/FLAudioManager;)V

    iput-object v0, p0, Lflipboard/service/audio/FLAudioManager;->g:Landroid/content/ServiceConnection;

    .line 248
    :cond_0
    if-eqz p1, :cond_2

    .line 249
    sget-boolean v0, Lflipboard/service/audio/MediaPlayerService;->i:Z

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->e:Landroid/content/Context;

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager;->f:Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 258
    :cond_1
    :goto_0
    return-void

    .line 253
    :cond_2
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->e:Landroid/content/Context;

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager;->f:Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 254
    sget-boolean v0, Lflipboard/service/audio/MediaPlayerService;->i:Z

    if-nez v0, :cond_1

    .line 255
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->e:Landroid/content/Context;

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager;->f:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public final b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v2, v1, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v2, :cond_0

    iget-object v0, v1, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->getDuration()I

    move-result v0

    .line 116
    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v0, p1}, Lflipboard/service/audio/MediaPlayerService;->b(Ljava/lang/String;)V

    .line 159
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 265
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    .line 267
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v0, v0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p0}, Lflipboard/util/Observable$Proxy;->c(Lflipboard/util/Observer;)V

    .line 268
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->e:Landroid/content/Context;

    iget-object v1, p0, Lflipboard/service/audio/FLAudioManager;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 269
    iput-object v2, p0, Lflipboard/service/audio/FLAudioManager;->g:Landroid/content/ServiceConnection;

    .line 270
    iput-object v2, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    .line 272
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->f:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    invoke-virtual {p0, v0, v2}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 274
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 278
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v3, v2, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v3, :cond_1

    iget-object v2, v2, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v2, v2, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v3, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 288
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    invoke-virtual {v0}, Lflipboard/service/audio/MediaPlayerService;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 293
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v0, v0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v0, v0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lflipboard/service/audio/Song;
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v0, v0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 313
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    const-string v1, "next"

    iput-object v1, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    .line 315
    invoke-virtual {p0, v3}, Lflipboard/service/audio/FLAudioManager;->a(Z)V

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "inAppSkipForwardButton"

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 323
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->d:Lflipboard/service/audio/FLAudioManager$AudioItem;

    const-string v1, "previous"

    iput-object v1, v0, Lflipboard/service/audio/FLAudioManager$AudioItem;->h:Ljava/lang/String;

    .line 325
    invoke-virtual {p0, v2}, Lflipboard/service/audio/FLAudioManager;->a(Z)V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    const-string v1, "inAppSkipBackButton"

    invoke-virtual {v0, v2, v2, v1}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 388
    invoke-virtual {p0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    const/4 v0, 0x1

    .line 392
    :goto_0
    return v0

    .line 391
    :cond_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioMessage;->d:Lflipboard/service/audio/FLAudioManager$AudioMessage;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 392
    const/4 v0, 0x0

    goto :goto_0
.end method
