.class public final enum Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;
.super Ljava/lang/Enum;
.source "MediaPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

.field public static final enum b:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

.field public static final enum c:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

.field public static final enum d:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

.field private static final synthetic e:[Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    const-string v1, "PLAYERSTATE_CHANGED"

    invoke-direct {v0, v1, v2}, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->a:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    new-instance v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    const-string v1, "PLAYER_ERROR"

    invoke-direct {v0, v1, v3}, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->b:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    new-instance v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    const-string v1, "SONG_CHANGED"

    invoke-direct {v0, v1, v4}, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->c:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    new-instance v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    const-string v1, "STOP_SERVICE"

    invoke-direct {v0, v1, v5}, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->d:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    .line 143
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    sget-object v1, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->a:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->b:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->c:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->d:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->e:[Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;
    .locals 1

    .prologue
    .line 143
    const-class v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->e:[Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-virtual {v0}, [Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    return-object v0
.end method
