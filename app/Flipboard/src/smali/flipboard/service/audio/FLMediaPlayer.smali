.class public Lflipboard/service/audio/FLMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "FLMediaPlayer.java"


# static fields
.field public static a:Lflipboard/util/Log;


# instance fields
.field b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

.field c:Ljava/lang/String;

.field d:Lflipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable$Proxy",
            "<",
            "Lflipboard/service/audio/FLMediaPlayer;",
            "Lflipboard/service/audio/FLMediaPlayer$PlayerState;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    sput-object v0, Lflipboard/service/audio/FLMediaPlayer;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 42
    new-instance v0, Lflipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lflipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->d:Lflipboard/util/Observable$Proxy;

    .line 43
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 44
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->f:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V
    .locals 2

    .prologue
    .line 159
    iput-object p1, p0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    .line 160
    if-eqz p2, :cond_0

    .line 161
    iget-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->d:Lflipboard/util/Observable$Proxy;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 163
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-super {p0}, Landroid/media/MediaPlayer;->stop()V

    .line 81
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->c:Ljava/lang/String;

    .line 82
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 83
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->a:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-virtual {p0, v0, p1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 84
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->e:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflipboard/service/audio/FLMediaPlayer;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    :cond_0
    invoke-super {p0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 73
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/service/audio/FLMediaPlayer;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflipboard/service/audio/FLMediaPlayer;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    :cond_0
    invoke-super {p0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 63
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/media/MediaPlayer;->pause()V

    .line 117
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->e:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 118
    return-void
.end method

.method public prepare()V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepare()V

    .line 138
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 139
    return-void
.end method

.method public prepareAsync()V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 144
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 145
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 128
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->a:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 129
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/audio/FLMediaPlayer;->d:Lflipboard/util/Observable$Proxy;

    iget-object v2, v2, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/FLMediaPlayer;->d:Lflipboard/util/Observable$Proxy;

    .line 131
    invoke-super {p0}, Landroid/media/MediaPlayer;->release()V

    .line 133
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/service/audio/FLMediaPlayer;->a(Z)V

    .line 106
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lflipboard/service/audio/FLMediaPlayer;->c:Ljava/lang/String;

    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0}, Landroid/media/MediaPlayer;->start()V

    .line 111
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 112
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0}, Landroid/media/MediaPlayer;->stop()V

    .line 123
    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->f:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 124
    return-void
.end method
