.class public Lflipboard/service/audio/MediaPlayerService;
.super Landroid/app/Service;
.source "MediaPlayerService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Service;",
        "Landroid/media/AudioManager$OnAudioFocusChangeListener;",
        "Landroid/media/MediaPlayer$OnCompletionListener;",
        "Landroid/media/MediaPlayer$OnErrorListener;",
        "Landroid/media/MediaPlayer$OnInfoListener;",
        "Landroid/media/MediaPlayer$OnPreparedListener;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/audio/FLMediaPlayer;",
        "Lflipboard/service/audio/FLMediaPlayer$PlayerState;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Lflipboard/util/Log;

.field public static b:Lflipboard/service/audio/MediaPlayerService;

.field public static i:Z


# instance fields
.field public c:Lflipboard/service/audio/FLMediaPlayer;

.field public d:Lflipboard/service/audio/Song;

.field e:Lflipboard/objs/FeedItem;

.field f:Lflipboard/service/Section;

.field g:Lflipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable$Proxy",
            "<",
            "Lflipboard/service/audio/MediaPlayerService;",
            "Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field private j:Landroid/media/AudioManager;

.field private k:Z

.field private l:Landroid/net/wifi/WifiManager$WifiLock;

.field private final m:Ljava/util/Timer;

.field private n:Ljava/util/TimerTask;

.field private o:Ljava/util/TimerTask;

.field private p:Landroid/os/Handler;

.field private q:Z

.field private r:Z

.field private s:Lflipboard/io/UsageEvent;

.field private t:J

.field private u:J

.field private v:Ljava/lang/String;

.field private w:Landroid/os/HandlerThread;

.field private x:Lflipboard/service/audio/MediaPlayerServiceBinder;

.field private y:Landroid/media/RemoteControlClient;

.field private z:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    sput-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 151
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 152
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    if-eqz v0, :cond_0

    .line 153
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "More than one MediaPlayerService instance!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->stopSelf()V

    .line 157
    :cond_0
    sput-object p0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    .line 158
    new-instance v0, Ljava/util/Timer;

    const-string v1, "mediaplayer-timer"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->m:Ljava/util/Timer;

    .line 159
    new-instance v0, Lflipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lflipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    .line 162
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "mediaplayer-tasks"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->w:Landroid/os/HandlerThread;

    .line 163
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->w:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 164
    new-instance v0, Lflipboard/service/audio/MediaPlayerService$1;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->w:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflipboard/service/audio/MediaPlayerService$1;-><init>(Lflipboard/service/audio/MediaPlayerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->p:Landroid/os/Handler;

    .line 171
    return-void
.end method

.method static synthetic a(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/FLMediaPlayer;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    return-object v0
.end method

.method private a(Lflipboard/objs/FeedItem;)Lflipboard/service/audio/Song;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1250
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1251
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1252
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1258
    :goto_0
    iget-object v1, v0, Lflipboard/objs/FeedItem;->bu:Ljava/lang/String;

    .line 1259
    if-eqz v1, :cond_0

    const-string v3, ""

    if-ne v1, v3, :cond_1

    :cond_0
    iget-object v3, v0, Lflipboard/objs/FeedItem;->bx:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1260
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d024b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lflipboard/objs/FeedItem;->bx:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1263
    :cond_1
    new-instance v3, Lflipboard/service/audio/Song;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    invoke-direct {v3, v4, v1, v5}, Lflipboard/service/audio/Song;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    iget-object v1, v0, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    iput-object v1, v3, Lflipboard/service/audio/Song;->c:Ljava/lang/String;

    .line 1265
    iget-object v1, v0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    :goto_1
    iput-object v0, v3, Lflipboard/service/audio/Song;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1267
    :cond_2
    return-object v2

    :cond_3
    move-object v0, v2

    .line 1265
    goto :goto_1

    :cond_4
    move-object v0, p1

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/audio/MediaPlayerService;Lflipboard/service/audio/Song;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x2

    .line 44
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    if-nez v2, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->z:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    new-instance v3, Landroid/media/RemoteControlClient;

    const/4 v4, 0x0

    const/high16 v5, 0x10000000

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    :cond_0
    sget-object v2, Lflipboard/service/audio/MediaPlayerService$8;->a:[I

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v3, v3, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-virtual {v3}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    invoke-virtual {v2, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    const/16 v2, 0xb5

    invoke-virtual {v0, v2}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    iget-object v2, p1, Lflipboard/service/audio/Song;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    const/4 v1, 0x7

    iget-object v2, p1, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p2}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    return-void

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lflipboard/service/audio/MediaPlayerService;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->d:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, p1}, Lflipboard/service/audio/MediaPlayerService;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lflipboard/service/audio/Song;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-nez v0, :cond_0

    .line 304
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-instance v0, Lflipboard/service/audio/FLMediaPlayer;

    invoke-direct {v0}, Lflipboard/service/audio/FLMediaPlayer;-><init>()V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLMediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLMediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLMediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->d:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p0}, Lflipboard/util/Observable$Proxy;->b(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lflipboard/service/audio/FLMediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 306
    :cond_0
    iput-object p2, p0, Lflipboard/service/audio/MediaPlayerService;->h:Ljava/lang/String;

    .line 307
    invoke-direct {p0, p1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/Song;)Z

    .line 308
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    .line 309
    return-void
.end method

.method private a(Lflipboard/service/audio/Song;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 317
    sget-object v2, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p1, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 320
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v2, :cond_0

    .line 321
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v2}, Ljava/util/TimerTask;->cancel()Z

    .line 322
    iput-object v5, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 326
    :cond_0
    iget-boolean v2, p0, Lflipboard/service/audio/MediaPlayerService;->k:Z

    if-nez v2, :cond_1

    .line 329
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v2, p0, v3, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    .line 331
    if-ne v2, v1, :cond_4

    .line 332
    iput-boolean v1, p0, Lflipboard/service/audio/MediaPlayerService;->k:Z

    .line 343
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_2

    .line 344
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 347
    :cond_2
    monitor-enter p0

    .line 348
    :try_start_0
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    .line 349
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    sget-object v2, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->c:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-virtual {v0, v2, p1}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 352
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    if-eqz v0, :cond_3

    .line 353
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    .line 357
    :cond_3
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    if-eqz v0, :cond_5

    .line 358
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 359
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 368
    :goto_0
    return v0

    .line 335
    :cond_4
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Did not receive audio focus, can\'t start playing. Result from focus request %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {v3, v4, v1}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    sget-object v2, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->b:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-virtual {v1, v2, v5}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 362
    :cond_5
    :try_start_1
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    .line 364
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->e()V

    .line 366
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 368
    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lflipboard/service/audio/MediaPlayerService;)Lflipboard/service/audio/Song;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    return-object v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 703
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 706
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v2, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->a:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->stop()V

    .line 711
    :cond_0
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    .line 714
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_2

    .line 715
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 716
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 718
    :cond_1
    iput-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    .line 722
    :cond_2
    if-eqz p1, :cond_5

    .line 723
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v0, :cond_3

    .line 724
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 725
    iput-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 728
    :cond_3
    invoke-virtual {p0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    .line 730
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->m:Ljava/util/Timer;

    new-instance v1, Lflipboard/service/audio/MediaPlayerService$6;

    invoke-direct {v1, p0}, Lflipboard/service/audio/MediaPlayerService$6;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 741
    :goto_1
    return-void

    :cond_4
    move v0, v1

    .line 706
    goto :goto_0

    .line 739
    :cond_5
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->b()V

    goto :goto_1
.end method

.method static synthetic c(Lflipboard/service/audio/MediaPlayerService;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    return v0
.end method

.method static synthetic d(Lflipboard/service/audio/MediaPlayerService;)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 438
    new-instance v0, Lflipboard/service/audio/MediaPlayerService$2;

    invoke-direct {v0, p0}, Lflipboard/service/audio/MediaPlayerService$2;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    .line 479
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->p:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 480
    return-void
.end method

.method static synthetic e(Lflipboard/service/audio/MediaPlayerService;)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    return v0
.end method

.method private f()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 1337
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_2

    .line 1339
    iget-wide v0, p0, Lflipboard/service/audio/MediaPlayerService;->t:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lflipboard/service/audio/MediaPlayerService;->u:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/service/audio/MediaPlayerService;->t:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/service/audio/MediaPlayerService;->u:J

    iput-wide v6, p0, Lflipboard/service/audio/MediaPlayerService;->t:J

    .line 1342
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1343
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "stopReason"

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1344
    iput-object v8, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 1348
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "duration"

    iget-wide v2, p0, Lflipboard/service/audio/MediaPlayerService;->u:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1349
    iput-wide v6, p0, Lflipboard/service/audio/MediaPlayerService;->u:J

    .line 1350
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 1351
    iput-object v8, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    .line 1353
    :cond_2
    return-void
.end method

.method static synthetic f(Lflipboard/service/audio/MediaPlayerService;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 583
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 585
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 588
    :cond_0
    monitor-enter p0

    .line 590
    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    if-eqz v0, :cond_1

    .line 591
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    .line 595
    :cond_1
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    if-eqz v0, :cond_2

    .line 596
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 604
    :goto_0
    return-void

    .line 598
    :cond_2
    monitor-exit p0

    .line 600
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->start()V

    .line 601
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    :cond_3
    iput-wide v2, p0, Lflipboard/service/audio/MediaPlayerService;->t:J

    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v2, "listen"

    invoke-direct {v0, v2}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v2, "sourceURL"

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v3, "sectionIdentifier"

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v3, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v2, "startReason"

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v3, "startTime"

    int-to-long v4, v1

    const-wide/16 v6, 0x12c

    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    int-to-long v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v4

    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "sectionType"

    const-string v2, "service"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "twitter"

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 603
    :cond_4
    :goto_3
    invoke-virtual {p0, v8}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    goto/16 :goto_0

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 601
    :cond_5
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-wide/16 v0, 0x0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "sectionType"

    const-string v2, "feed"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public final a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 856
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    .line 857
    iput-object p2, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    .line 858
    invoke-direct {p0, p1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;)Lflipboard/service/audio/Song;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    .line 859
    return-void
.end method

.method public final a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 240
    if-nez p1, :cond_0

    .line 269
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 247
    iput-object p3, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 252
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {p1, v0}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 253
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->a()V

    goto :goto_0

    .line 258
    :cond_2
    invoke-static {p1}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 259
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Item played in MediaPlayerService is not an audio item: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 263
    :cond_3
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    .line 264
    iput-object p2, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    .line 267
    invoke-direct {p0, p1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;)Lflipboard/service/audio/Song;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/Song;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    check-cast p1, Lflipboard/service/audio/FLMediaPlayer;

    check-cast p2, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p2, v0, v2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    sget-object v1, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->a:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-virtual {v0, v1, p2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v0, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->g:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0, p1, v2, v2}, Lflipboard/service/audio/MediaPlayerService;->onError(Landroid/media/MediaPlayer;II)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377
    :cond_0
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->h:Ljava/lang/String;

    .line 380
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 381
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 384
    :cond_1
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->a()V

    .line 388
    :cond_2
    :goto_0
    return-void

    .line 386
    :cond_3
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    invoke-direct {p0, v0, p1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/Song;Ljava/lang/String;)V

    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "listen"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "startReason"

    invoke-virtual {v0, v1, p1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v1, "sourceURL"

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->s:Lflipboard/io/UsageEvent;

    const-string v2, "sectionIdentifier"

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 276
    new-instance v0, Lflipboard/service/audio/Song;

    invoke-direct {v0, p2, p3, p1}, Lflipboard/service/audio/Song;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p4}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/Song;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 540
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 543
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-nez v0, :cond_0

    .line 575
    :goto_0
    return-void

    .line 547
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->b:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->c:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    .line 548
    invoke-virtual {p0, p1}, Lflipboard/service/audio/MediaPlayerService;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 552
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->pause()V

    .line 554
    if-eqz p2, :cond_3

    .line 555
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->stopForeground(Z)V

    .line 561
    :goto_1
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    .line 563
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    .line 564
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 568
    :cond_2
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->m:Ljava/util/Timer;

    new-instance v1, Lflipboard/service/audio/MediaPlayerService$4;

    invoke-direct {v1, p0}, Lflipboard/service/audio/MediaPlayerService$4;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 557
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 624
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-nez v0, :cond_0

    .line 655
    :goto_0
    return-void

    .line 628
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    .line 629
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 630
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    .line 636
    :cond_1
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->m:Ljava/util/Timer;

    new-instance v3, Lflipboard/service/audio/MediaPlayerService$5;

    invoke-direct {v3, p0}, Lflipboard/service/audio/MediaPlayerService$5;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    iput-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    if-eqz p1, :cond_2

    const-wide/16 v0, 0x15e

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method final a(ZZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 875
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    iget v0, v0, Lflipboard/service/Section;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    aput-object v0, v1, v2

    .line 878
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    if-eqz v0, :cond_0

    .line 879
    iput-object p3, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 882
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    if-eqz v0, :cond_f

    .line 885
    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    iget-object v4, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    const-string v7, "audio"

    if-nez p2, :cond_3

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    iget-object v8, v2, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v4, :cond_1

    if-nez v8, :cond_4

    :cond_1
    const/4 v0, 0x0

    .line 887
    :goto_2
    sget-object v1, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 889
    if-eqz v0, :cond_e

    .line 890
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    .line 891
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    invoke-virtual {p0, v0, v1, p3}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 902
    :goto_3
    return-void

    .line 875
    :cond_2
    const-string v0, "null"

    goto :goto_0

    .line 885
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    const/4 v0, 0x0

    move v2, v0

    :goto_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v4}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_5
    if-ltz v2, :cond_11

    const/4 v0, 0x0

    const/4 v4, 0x0

    move v5, v2

    move v6, v0

    :cond_6
    :goto_5
    if-ltz v5, :cond_10

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_10

    if-nez v6, :cond_10

    if-nez v4, :cond_7

    if-eqz p1, :cond_7

    if-eqz v1, :cond_7

    add-int/lit8 v0, v5, 0x1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-ne v0, v9, :cond_7

    const/4 v4, 0x0

    const/4 v0, 0x1

    move v5, v4

    move v4, v0

    :goto_6
    if-ltz v5, :cond_10

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_10

    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v9, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    if-eqz p1, :cond_c

    const-string v0, "next"

    :goto_7
    aput-object v0, v9, v10

    const/4 v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v0

    const/4 v0, 0x2

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    aput-object v10, v9, v0

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bC:Z

    if-eqz v0, :cond_d

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    const/4 v3, 0x1

    move v6, v3

    move-object v3, v0

    goto :goto_5

    :cond_7
    if-nez v4, :cond_8

    if-nez p1, :cond_8

    if-eqz v1, :cond_8

    if-nez v5, :cond_8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    const/4 v0, 0x1

    move v5, v4

    move v4, v0

    goto :goto_6

    :cond_8
    if-eqz v4, :cond_a

    if-ne v5, v2, :cond_a

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bC:Z

    if-eqz v0, :cond_9

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    goto/16 :goto_2

    :cond_9
    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_10

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_10

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bC:Z

    if-eqz v0, :cond_10

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    goto/16 :goto_2

    :cond_a
    if-eqz p1, :cond_b

    add-int/lit8 v0, v5, 0x1

    :goto_8
    move v5, v0

    goto/16 :goto_6

    :cond_b
    add-int/lit8 v0, v5, -0x1

    goto :goto_8

    :cond_c
    const-string v0, "previous"

    goto/16 :goto_7

    :cond_d
    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_6

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v9, 0x0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bC:Z

    if-eqz v0, :cond_6

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    const/4 v3, 0x1

    move v6, v3

    move-object v3, v0

    goto/16 :goto_5

    .line 894
    :cond_e
    const-string v0, "playbackFinished"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    goto/16 :goto_3

    .line 899
    :cond_f
    const-string v0, "playbackFinished"

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 900
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V

    goto/16 :goto_3

    :cond_10
    move-object v0, v3

    goto/16 :goto_2

    :cond_11
    move-object v0, v3

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 745
    sget-object v1, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 748
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lflipboard/service/audio/MediaPlayerService;->stopForeground(Z)V

    .line 751
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v1, :cond_0

    .line 752
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 753
    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 757
    :cond_0
    iget-boolean v1, p0, Lflipboard/service/audio/MediaPlayerService;->k:Z

    if-eqz v1, :cond_1

    .line 758
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    invoke-virtual {v1, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 759
    iput-boolean v5, p0, Lflipboard/service/audio/MediaPlayerService;->k:Z

    .line 763
    :cond_1
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v1, :cond_2

    .line 765
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v1, v1, Lflipboard/service/audio/FLMediaPlayer;->d:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v1, p0}, Lflipboard/util/Observable$Proxy;->c(Lflipboard/util/Observer;)V

    .line 766
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v1}, Lflipboard/service/audio/FLMediaPlayer;->release()V

    .line 767
    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    .line 770
    :cond_2
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    iget-object v1, v1, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 773
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_5

    .line 774
    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioItem;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    const-string v3, "reset"

    invoke-direct {v0, v1, v2, v3}, Lflipboard/service/audio/FLAudioManager$AudioItem;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 780
    :cond_3
    :goto_0
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    sget-object v2, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->d:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-virtual {v1, v2, v0}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 783
    :cond_4
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->stopSelf()V

    .line 784
    sput-boolean v5, Lflipboard/service/audio/MediaPlayerService;->i:Z

    .line 785
    return-void

    .line 775
    :cond_5
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    if-eqz v1, :cond_3

    .line 776
    new-instance v0, Lflipboard/service/audio/FLAudioManager$AudioItem;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    iget-object v2, v2, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    iget-object v3, v3, Lflipboard/service/audio/Song;->d:Ljava/lang/String;

    const-string v4, "reset"

    invoke-direct {v0, v1, v2, v3, v4}, Lflipboard/service/audio/FLAudioManager$AudioItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 487
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 489
    iput-object p1, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 491
    monitor-enter p0

    .line 493
    :try_start_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-nez v0, :cond_0

    .line 494
    monitor-exit p0

    .line 525
    :goto_0
    return-void

    .line 499
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    .line 500
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 507
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    .line 508
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 509
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 512
    :cond_2
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->m:Ljava/util/Timer;

    new-instance v1, Lflipboard/service/audio/MediaPlayerService$3;

    invoke-direct {v1, p0}, Lflipboard/service/audio/MediaPlayerService$3;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 522
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    sget-object v1, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->f:Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-virtual {v0, v1, v4}, Lflipboard/service/audio/FLMediaPlayer;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;Z)V

    .line 524
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    goto :goto_0

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 791
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    .line 793
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1184
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v1, :cond_1

    .line 1185
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v1}, Lflipboard/service/audio/FLMediaPlayer;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1187
    :cond_1
    return v0
.end method

.method public onAudioFocusChange(I)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const v2, 0x3dcccccd    # 0.1f

    const/4 v1, 0x0

    .line 1393
    packed-switch p1, :pswitch_data_0

    .line 1449
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1397
    :pswitch_1
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1399
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    if-eqz v0, :cond_2

    .line 1401
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 1402
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->e:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    const-string v2, "audioFocusGain"

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0

    .line 1403
    :cond_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    if-eqz v0, :cond_0

    .line 1404
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    const-string v1, "audioFocusGain"

    invoke-direct {p0, v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Lflipboard/service/audio/Song;Ljava/lang/String;)V

    goto :goto_0

    .line 1409
    :cond_2
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v0, :cond_3

    .line 1411
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0, v3, v3}, Lflipboard/service/audio/FLMediaPlayer;->setVolume(FF)V

    .line 1412
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1413
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    const-string v1, "audioFocusLost"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "audioFocusGain"

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1419
    :cond_3
    invoke-direct {p0, v1}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V

    goto :goto_0

    .line 1425
    :pswitch_2
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1427
    const-string v0, "audioFocusLost"

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 1428
    invoke-direct {p0, v1}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V

    goto :goto_0

    .line 1432
    :pswitch_3
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1436
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1437
    const-string v0, "audioFocusLost"

    invoke-virtual {p0, v0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1442
    :pswitch_4
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 1444
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1445
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0, v2, v2}, Lflipboard/service/audio/FLMediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 1393
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 1145
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1146
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->x:Lflipboard/service/audio/MediaPlayerServiceBinder;

    if-nez v0, :cond_0

    .line 1147
    new-instance v0, Lflipboard/service/audio/MediaPlayerServiceBinder;

    invoke-direct {v0, p0}, Lflipboard/service/audio/MediaPlayerServiceBinder;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->x:Lflipboard/service/audio/MediaPlayerServiceBinder;

    .line 1149
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->x:Lflipboard/service/audio/MediaPlayerServiceBinder;

    return-object v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1122
    const-string v0, "autoPlayAfterPlaybackFinished"

    invoke-virtual {p0, v1, v1, v0}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    .line 1123
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 179
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 180
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 181
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    .line 182
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lflipboard/service/audio/MediaPlayerService$MusicIntentReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->z:Landroid/content/ComponentName;

    .line 183
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->z:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 185
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 186
    if-eqz v0, :cond_0

    .line 187
    const/4 v1, 0x1

    const-string v2, "mediaplayerservice-wifilock"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->l:Landroid/net/wifi/WifiManager$WifiLock;

    .line 190
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 797
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 798
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/service/audio/MediaPlayerService;->i:Z

    .line 799
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->b()V

    .line 802
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    .line 804
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 805
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 806
    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->n:Ljava/util/TimerTask;

    .line 809
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->x:Lflipboard/service/audio/MediaPlayerServiceBinder;

    if-eqz v0, :cond_1

    .line 810
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->x:Lflipboard/service/audio/MediaPlayerServiceBinder;

    iput-object v1, v0, Lflipboard/service/audio/MediaPlayerServiceBinder;->a:Lflipboard/service/audio/MediaPlayerService;

    .line 811
    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->x:Lflipboard/service/audio/MediaPlayerServiceBinder;

    .line 814
    :cond_1
    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    .line 816
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 817
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->p:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 818
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->w:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 819
    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->p:Landroid/os/Handler;

    .line 820
    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->w:Landroid/os/HandlerThread;

    .line 822
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->z:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 824
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_2

    .line 825
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->j:Landroid/media/AudioManager;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->y:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 828
    :cond_2
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 829
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 830
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1031
    const-string v0, "reset"

    iput-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->v:Ljava/lang/String;

    .line 1032
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->f()V

    .line 1034
    monitor-enter p0

    .line 1035
    :try_start_0
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    if-eqz v0, :cond_0

    .line 1036
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    .line 1038
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1040
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->g:Lflipboard/util/Observable$Proxy;

    sget-object v3, Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;->b:Lflipboard/service/audio/MediaPlayerService$MediaPlayerMessage;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1044
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 1065
    :goto_0
    invoke-virtual {p0, v2}, Lflipboard/service/audio/MediaPlayerService;->stopForeground(Z)V

    .line 1066
    invoke-virtual {p0, v1}, Lflipboard/service/audio/MediaPlayerService;->a(Z)V

    .line 1069
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    if-eqz v1, :cond_1

    .line 1070
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 1071
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    .line 1074
    :cond_1
    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->m:Ljava/util/Timer;

    new-instance v2, Lflipboard/service/audio/MediaPlayerService$7;

    invoke-direct {v2, p0}, Lflipboard/service/audio/MediaPlayerService$7;-><init>(Lflipboard/service/audio/MediaPlayerService;)V

    iput-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->o:Ljava/util/TimerTask;

    const-wide/32 v4, 0x927c0

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1082
    return v0

    .line 1038
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1046
    :sswitch_0
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    move v0, v1

    .line 1047
    goto :goto_0

    .line 1050
    :sswitch_1
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1051
    invoke-direct {p0, v1}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V

    :cond_2
    :goto_1
    move v0, v2

    .line 1062
    goto :goto_0

    .line 1058
    :sswitch_2
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1059
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v0, :cond_2

    .line 1060
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->reset()V

    goto :goto_1

    .line 1044
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_0
        0x864701 -> :sswitch_2
    .end sparse-switch
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1087
    sparse-switch p2, :sswitch_data_0

    .line 1110
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1114
    :goto_0
    return v2

    .line 1089
    :sswitch_0
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1092
    :sswitch_1
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1095
    :sswitch_2
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1098
    :sswitch_3
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1101
    :sswitch_4
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1104
    :sswitch_5
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1107
    :sswitch_6
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 1087
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2bd -> :sswitch_1
        0x2be -> :sswitch_2
        0x2bf -> :sswitch_6
        0x320 -> :sswitch_5
        0x321 -> :sswitch_3
        0x322 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 992
    monitor-enter p0

    .line 994
    :try_start_0
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    iget-object v2, v2, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v2, v2, Lflipboard/service/audio/FLMediaPlayer;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 997
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    .line 1000
    iget-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    if-eqz v0, :cond_0

    .line 1001
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->q:Z

    .line 1002
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V

    .line 1021
    :goto_0
    monitor-exit p0

    return-void

    .line 1007
    :cond_0
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1008
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1021
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1014
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v0, v0, Lflipboard/service/audio/FLMediaPlayer;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    iget-object v0, v0, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    iget-object v1, v1, Lflipboard/service/audio/FLMediaPlayer;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1015
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->a()V

    goto :goto_0

    .line 1017
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/audio/MediaPlayerService;->r:Z

    .line 1018
    invoke-direct {p0}, Lflipboard/service/audio/MediaPlayerService;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 197
    sput-boolean v4, Lflipboard/service/audio/MediaPlayerService;->i:Z

    .line 198
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 199
    if-eqz p1, :cond_0

    .line 200
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/service/audio/MediaPlayerService;->f:Lflipboard/service/Section;

    if-eqz v1, :cond_0

    .line 202
    const-string v1, "audio_action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 203
    packed-switch v0, :pswitch_data_0

    .line 232
    :cond_0
    :goto_0
    return v4

    .line 205
    :pswitch_0
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 206
    const-string v0, "remotePlayButton"

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 209
    :pswitch_1
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 210
    invoke-virtual {p0}, Lflipboard/service/audio/MediaPlayerService;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    const-string v0, "remotePauseButton"

    invoke-virtual {p0, v0}, Lflipboard/service/audio/MediaPlayerService;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 213
    :cond_1
    const-string v0, "remotePauseButton"

    invoke-virtual {p0, v0, v3}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 217
    :pswitch_2
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 218
    const-string v0, "remoteSkipForwardButton"

    invoke-virtual {p0, v4, v3, v0}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 221
    :pswitch_3
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 222
    const-string v0, "remoteSkipBackButton"

    invoke-virtual {p0, v3, v3, v0}, Lflipboard/service/audio/MediaPlayerService;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 225
    :pswitch_4
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    .line 227
    invoke-direct {p0, v3}, Lflipboard/service/audio/MediaPlayerService;->b(Z)V

    goto :goto_0

    .line 203
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 1157
    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1158
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
