.class Lflipboard/service/User$42;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;ZLjava/util/List;)V
    .locals 0

    .prologue
    .line 2742
    iput-object p1, p0, Lflipboard/service/User$42;->c:Lflipboard/service/User;

    iput-boolean p2, p0, Lflipboard/service/User$42;->a:Z

    iput-object p3, p0, Lflipboard/service/User$42;->b:Ljava/util/List;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2745
    const-string v0, "magazines"

    invoke-virtual {p0, v0}, Lflipboard/service/User$42;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2747
    const-string v1, "uid = ? AND contributor = ?"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    iget-object v0, p0, Lflipboard/service/User$42;->c:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v0, v3, v2

    const/4 v4, 0x1

    iget-boolean v0, p0, Lflipboard/service/User$42;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    aput-object v0, v3, v4

    invoke-virtual {p0, v1, v3}, Lflipboard/service/User$42;->c(Ljava/lang/String;[Ljava/lang/String;)I

    move v1, v2

    .line 2750
    :goto_1
    iget-object v0, p0, Lflipboard/service/User$42;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2751
    iget-object v0, p0, Lflipboard/service/User$42;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    .line 2753
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2754
    const-string v4, "uid"

    iget-object v5, p0, Lflipboard/service/User$42;->c:Lflipboard/service/User;

    iget-object v5, v5, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2755
    const-string v4, "contributor"

    iget-boolean v5, p0, Lflipboard/service/User$42;->a:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2756
    const-string v4, "descriptor"

    invoke-static {v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Magazine;)[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2757
    invoke-virtual {p0, v3}, Lflipboard/service/User$42;->a(Landroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2750
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2747
    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 2760
    :catch_0
    move-exception v3

    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t write magazine "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lflipboard/objs/Magazine;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2764
    :cond_1
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v1, "Unable to save magazines because table doesn\'t exist"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2766
    :cond_2
    return-void
.end method
