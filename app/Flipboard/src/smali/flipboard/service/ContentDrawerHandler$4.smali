.class Lflipboard/service/ContentDrawerHandler$4;
.super Ljava/lang/Object;
.source "ContentDrawerHandler.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/service/ContentDrawerHandler;


# direct methods
.method constructor <init>(Lflipboard/service/ContentDrawerHandler;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 367
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const-string v1, "fail loading services: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 368
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->e:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p1}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 369
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 334
    if-nez p2, :cond_0

    .line 335
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0, v4}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;Z)Z

    .line 336
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 337
    :try_start_0
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 338
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "data is null for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 340
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 341
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v1

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->c:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v1, v2, v0}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 362
    :goto_0
    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 346
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;Z)Z

    .line 347
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    new-instance v1, Lflipboard/json/JSONParser;

    invoke-direct {v1, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->j()Lflipboard/objs/ConfigServices;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Lflipboard/objs/ConfigServices;)Lflipboard/objs/ConfigServices;

    .line 348
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    .line 353
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 354
    :catch_0
    move-exception v0

    .line 355
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "failed to parse services.json: %-E"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1, v4}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/service/ContentDrawerHandler;Z)Z

    .line 357
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 358
    :try_start_2
    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v2}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 359
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 360
    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v1

    sget-object v2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->c:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v1, v2, v0}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 361
    throw v0

    .line 359
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 373
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const-string v1, "fail loading services, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 374
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler$4;->a:Lflipboard/service/ContentDrawerHandler;

    invoke-static {v0}, Lflipboard/service/ContentDrawerHandler;->c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->e:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    invoke-virtual {v0, v1, p1}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 375
    return-void
.end method
