.class public Lflipboard/service/Flap$AdDeleteAllRequest;
.super Lflipboard/service/Flap$RetryRequest;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 5545
    iput-object p1, p0, Lflipboard/service/Flap$AdDeleteAllRequest;->b:Lflipboard/service/Flap;

    .line 5546
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$RetryRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 5547
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdDeleteAllRequest;
    .locals 0

    .prologue
    .line 5551
    iput-object p1, p0, Lflipboard/service/Flap$AdDeleteAllRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 5552
    invoke-super {p0}, Lflipboard/service/Flap$RetryRequest;->c()V

    .line 5553
    return-object p0
.end method

.method protected final a(Lflipboard/json/FLObject;)V
    .locals 1

    .prologue
    .line 5558
    iget-object v0, p0, Lflipboard/service/Flap$AdDeleteAllRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    .line 5559
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5563
    iget-object v0, p0, Lflipboard/service/Flap$AdDeleteAllRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    .line 5564
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 5568
    iget-object v0, p0, Lflipboard/service/Flap$AdDeleteAllRequest;->b:Lflipboard/service/Flap;

    iget-object v0, v0, Lflipboard/service/Flap;->h:Landroid/content/Context;

    const-string v1, "flipboard_settings"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 5569
    const-string v1, "adserver_internal_baseurl"

    const-string v2, "http://flint01.beta.live.flipboard.com:35468"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5570
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/int/userstore/deleteAll?user_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Flap$AdDeleteAllRequest;->n:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&code=DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
