.class public final enum Lflipboard/service/Flap$FollowListType;
.super Ljava/lang/Enum;
.source "Flap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/Flap$FollowListType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/Flap$FollowListType;

.field public static final enum b:Lflipboard/service/Flap$FollowListType;

.field public static final enum c:Lflipboard/service/Flap$FollowListType;

.field public static final enum d:Lflipboard/service/Flap$FollowListType;

.field private static final synthetic f:[Lflipboard/service/Flap$FollowListType;


# instance fields
.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2623
    new-instance v0, Lflipboard/service/Flap$FollowListType;

    const-string v1, "FOLLOWERS"

    const-string v2, "/v1/social/followers"

    invoke-direct {v0, v1, v3, v2}, Lflipboard/service/Flap$FollowListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/Flap$FollowListType;->a:Lflipboard/service/Flap$FollowListType;

    .line 2624
    new-instance v0, Lflipboard/service/Flap$FollowListType;

    const-string v1, "FOLLOWING"

    const-string v2, "/v1/social/follows"

    invoke-direct {v0, v1, v4, v2}, Lflipboard/service/Flap$FollowListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/Flap$FollowListType;->b:Lflipboard/service/Flap$FollowListType;

    .line 2625
    new-instance v0, Lflipboard/service/Flap$FollowListType;

    const-string v1, "SUGGESTED_FOLLOWERS"

    const-string v2, "/v1/social/suggestedFollows"

    invoke-direct {v0, v1, v5, v2}, Lflipboard/service/Flap$FollowListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    .line 2626
    new-instance v0, Lflipboard/service/Flap$FollowListType;

    const-string v1, "SUGGESTED_FOLLOWERS_FROM_EMAIL"

    const-string v2, "/v1/social/suggestedFollowsFromEmail"

    invoke-direct {v0, v1, v6, v2}, Lflipboard/service/Flap$FollowListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    .line 2622
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/service/Flap$FollowListType;

    sget-object v1, Lflipboard/service/Flap$FollowListType;->a:Lflipboard/service/Flap$FollowListType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/Flap$FollowListType;->b:Lflipboard/service/Flap$FollowListType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    aput-object v1, v0, v6

    sput-object v0, Lflipboard/service/Flap$FollowListType;->f:[Lflipboard/service/Flap$FollowListType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2630
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2631
    iput-object p3, p0, Lflipboard/service/Flap$FollowListType;->e:Ljava/lang/String;

    .line 2632
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/Flap$FollowListType;
    .locals 1

    .prologue
    .line 2622
    const-class v0, Lflipboard/service/Flap$FollowListType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/Flap$FollowListType;

    return-object v0
.end method

.method public static values()[Lflipboard/service/Flap$FollowListType;
    .locals 1

    .prologue
    .line 2622
    sget-object v0, Lflipboard/service/Flap$FollowListType;->f:[Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v0}, [Lflipboard/service/Flap$FollowListType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/Flap$FollowListType;

    return-object v0
.end method
