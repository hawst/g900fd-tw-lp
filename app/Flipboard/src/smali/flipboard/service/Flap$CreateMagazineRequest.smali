.class public Lflipboard/service/Flap$CreateMagazineRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic b:Lflipboard/service/Flap;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4538
    iput-object p1, p0, Lflipboard/service/Flap$CreateMagazineRequest;->b:Lflipboard/service/Flap;

    .line 4539
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4540
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$CreateMagazineRequest;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4556
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 4557
    iput-object p1, p0, Lflipboard/service/Flap$CreateMagazineRequest;->c:Ljava/lang/String;

    .line 4558
    iput-object v2, p0, Lflipboard/service/Flap$CreateMagazineRequest;->d:Ljava/lang/String;

    .line 4559
    iput-object p2, p0, Lflipboard/service/Flap$CreateMagazineRequest;->e:Ljava/lang/String;

    .line 4560
    iput-object p4, p0, Lflipboard/service/Flap$CreateMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4561
    iput-object v2, p0, Lflipboard/service/Flap$CreateMagazineRequest;->f:Ljava/lang/String;

    .line 4562
    iput-object p3, p0, Lflipboard/service/Flap$CreateMagazineRequest;->g:Ljava/lang/String;

    .line 4563
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4564
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$CreateMagazineRequest;
    .locals 2

    .prologue
    .line 4544
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 4545
    iput-object p1, p0, Lflipboard/service/Flap$CreateMagazineRequest;->c:Ljava/lang/String;

    .line 4546
    iput-object p2, p0, Lflipboard/service/Flap$CreateMagazineRequest;->d:Ljava/lang/String;

    .line 4547
    iput-object p3, p0, Lflipboard/service/Flap$CreateMagazineRequest;->e:Ljava/lang/String;

    .line 4548
    iput-object p5, p0, Lflipboard/service/Flap$CreateMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    .line 4549
    iput-object p4, p0, Lflipboard/service/Flap$CreateMagazineRequest;->f:Ljava/lang/String;

    .line 4550
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4551
    return-object p0
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4570
    iget-object v0, p0, Lflipboard/service/Flap$CreateMagazineRequest;->b:Lflipboard/service/Flap;

    const-string v1, "/v1/curator/createMagazine"

    iget-object v2, p0, Lflipboard/service/Flap$CreateMagazineRequest;->n:Lflipboard/service/User;

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "magazineVisibility"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$CreateMagazineRequest;->e:Ljava/lang/String;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$CreateMagazineRequest;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "description"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$CreateMagazineRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "magazineCategory"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Lflipboard/service/Flap$CreateMagazineRequest;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "link"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    iget-object v5, p0, Lflipboard/service/Flap$CreateMagazineRequest;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4571
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 4572
    iget-object v1, p0, Lflipboard/service/Flap$CreateMagazineRequest;->b:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$CreateMagazineRequest;->a:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 4573
    return-void
.end method
