.class public abstract Lflipboard/service/Flap$RetryRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field public d:Z

.field final synthetic e:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 1

    .prologue
    .line 5127
    iput-object p1, p0, Lflipboard/service/Flap$RetryRequest;->e:Lflipboard/service/Flap;

    .line 5128
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 5125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/service/Flap$RetryRequest;->d:Z

    .line 5129
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5140
    invoke-virtual {p0}, Lflipboard/service/Flap$RetryRequest;->b()Ljava/lang/String;

    move-result-object v7

    .line 5144
    const/16 v2, 0x3e8

    move v5, v2

    move v6, v3

    .line 5146
    :goto_0
    :try_start_0
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    .line 5148
    iget-boolean v2, p0, Lflipboard/service/Flap$RetryRequest;->d:Z

    if-eqz v2, :cond_0

    .line 5149
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 5154
    :goto_1
    iget-boolean v2, p0, Lflipboard/service/Flap$RetryRequest;->d:Z

    if-nez v2, :cond_2

    .line 5155
    const-string v2, "Content-Type"

    const-string v8, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v3, v2, v8}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 5156
    invoke-static {}, Lflipboard/abtest/Experiments;->a()Ljava/lang/String;

    move-result-object v2

    .line 5157
    invoke-static {v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 5158
    const-string v8, ","

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 5159
    new-instance v9, Ljava/util/ArrayList;

    array-length v2, v8

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 5160
    array-length v10, v8

    move v2, v4

    :goto_2
    if-ge v2, v10, :cond_1

    aget-object v11, v8, v2

    .line 5161
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "ab_test"

    invoke-direct {v12, v13, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5160
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 5151
    :cond_0
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v3, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 5201
    :catch_0
    move-exception v2

    .line 5205
    invoke-virtual {v2}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lflipboard/service/Flap$RetryRequest;->a(Ljava/lang/String;)V

    .line 5210
    :goto_3
    return-void

    .line 5163
    :cond_1
    :try_start_1
    new-instance v8, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v8, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    .line 5164
    move-object v0, v3

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object v2, v0

    invoke-virtual {v2, v8}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 5167
    :cond_2
    iget-object v2, p0, Lflipboard/service/Flap$RetryRequest;->e:Lflipboard/service/Flap;

    invoke-virtual {v2, v3}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 5168
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    .line 5169
    new-instance v9, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v3, v2}, Lflipboard/service/Flap$RetryRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v9, v2}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 5171
    add-int/lit8 v3, v6, 0x1

    .line 5172
    :try_start_2
    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    .line 5173
    const/16 v6, 0x1f4

    if-lt v2, v6, :cond_3

    const/16 v6, 0x258

    if-ge v2, v6, :cond_3

    if-gt v3, v14, :cond_3

    .line 5176
    :try_start_3
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v6

    const/4 v6, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v6

    const/4 v6, 0x2

    aput-object v7, v2, v6

    .line 5177
    int-to-long v10, v5

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5178
    mul-int/lit8 v2, v5, 0x2

    .line 5196
    :try_start_4
    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move v5, v2

    move v6, v3

    .line 5197
    goto/16 :goto_0

    .line 5179
    :catch_1
    move-exception v2

    .line 5180
    :try_start_5
    sget-object v6, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v6, v2}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 5196
    :try_start_6
    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->close()V

    move v6, v3

    .line 5197
    goto/16 :goto_0

    .line 5196
    :catchall_0
    move-exception v2

    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    move v6, v3

    .line 5197
    goto/16 :goto_0

    .line 5184
    :cond_3
    const/16 v3, 0xc8

    if-eq v2, v3, :cond_4

    .line 5185
    :try_start_7
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lflipboard/service/Flap$RetryRequest;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 5196
    :try_start_8
    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_8
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_3

    .line 5206
    :catch_2
    move-exception v2

    .line 5207
    sget-object v3, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v3, v2}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 5208
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lflipboard/service/Flap$RetryRequest;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 5188
    :cond_4
    :try_start_9
    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/json/FLObject;

    .line 5189
    if-eqz v2, :cond_5

    .line 5190
    invoke-virtual {p0, v2}, Lflipboard/service/Flap$RetryRequest;->a(Lflipboard/json/FLObject;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 5196
    :goto_4
    :try_start_a
    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->close()V
    :try_end_a
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_3

    .line 5192
    :cond_5
    :try_start_b
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected null response from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lflipboard/service/Flap$RetryRequest;->a(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_4

    .line 5196
    :catchall_1
    move-exception v2

    :try_start_c
    invoke-virtual {v9}, Lflipboard/service/Flap$FLObjectInputStream;->close()V

    .line 5197
    throw v2
    :try_end_c
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
.end method

.method protected a(Lflipboard/json/FLObject;)V
    .locals 0

    .prologue
    .line 5135
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 5136
    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method
