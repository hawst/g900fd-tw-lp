.class public Lflipboard/service/Flap$LikeUnlikeRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field final a:Lflipboard/service/Section;

.field final b:Lflipboard/objs/FeedItem;

.field c:Z

.field d:Lflipboard/service/ServiceReloginObserver;

.field final synthetic e:Lflipboard/service/Flap;

.field private f:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 2411
    iput-object p1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->e:Lflipboard/service/Flap;

    .line 2412
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2413
    iput-object p3, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->a:Lflipboard/service/Section;

    .line 2414
    iput-object p4, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->b:Lflipboard/objs/FeedItem;

    .line 2415
    return-void
.end method


# virtual methods
.method public final a(ZLandroid/os/Bundle;Lflipboard/service/ServiceReloginObserver;)Lflipboard/service/Flap$LikeUnlikeRequest;
    .locals 0

    .prologue
    .line 2419
    iput-boolean p1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->c:Z

    .line 2420
    iput-object p3, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    .line 2421
    iput-object p2, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->f:Landroid/os/Bundle;

    .line 2422
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 2423
    return-object p0
.end method

.method protected final a()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2429
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2430
    iget-boolean v0, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "like"

    move-object v1, v0

    .line 2431
    :goto_0
    iget-object v0, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->e:Lflipboard/service/Flap;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/v1/social/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->n:Lflipboard/service/User;

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "oid"

    aput-object v7, v6, v9

    iget-object v7, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v7}, Lflipboard/objs/FeedItem;->ad()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v0, v2, v3, v6}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2432
    iget-object v2, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->f:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    .line 2433
    iget-object v2, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->f:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2434
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "&"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->f:Landroid/os/Bundle;

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2435
    goto :goto_1

    .line 2430
    :cond_0
    const-string v0, "unlike"

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v2, v0

    .line 2437
    :cond_2
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v0, v11, [Ljava/lang/Object;

    aput-object v1, v0, v9

    aput-object v2, v0, v10

    .line 2439
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2440
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441
    iget-object v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->e:Lflipboard/service/Flap;

    invoke-virtual {v1, v0}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 2442
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 2443
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v0, v1}, Lflipboard/service/Flap$LikeUnlikeRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2444
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 2445
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    .line 2446
    iget-object v0, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2474
    iput-object v8, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    .line 2475
    :goto_2
    return-void

    .line 2449
    :cond_3
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 2450
    if-eqz v0, :cond_8

    .line 2451
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2452
    iget-boolean v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->c:Z

    if-eqz v1, :cond_5

    const-string v1, "liked"

    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    iget-object v4, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->a:Lflipboard/service/Section;

    iget-object v5, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->b:Lflipboard/objs/FeedItem;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V

    .line 2453
    iget-object v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2474
    :cond_4
    :goto_4
    iput-object v8, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    goto :goto_2

    .line 2452
    :cond_5
    :try_start_2
    const-string v1, "unliked"

    goto :goto_3

    .line 2455
    :cond_6
    const-string v1, "action"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2456
    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2457
    if-eqz v2, :cond_4

    .line 2458
    if-eqz v1, :cond_7

    const-string v3, "relogin"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2459
    iget-object v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    const/4 v3, 0x0

    const-string v4, "service"

    invoke-virtual {v0, v4}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0, v2}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 2468
    :catch_0
    move-exception v0

    .line 2469
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2474
    iput-object v8, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    goto :goto_2

    .line 2461
    :cond_7
    :try_start_4
    iget-object v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 2470
    :catch_1
    move-exception v0

    .line 2471
    :try_start_5
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 2472
    iget-object v1, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2474
    iput-object v8, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    goto/16 :goto_2

    .line 2466
    :cond_8
    :try_start_6
    iget-object v0, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-virtual {v0, v1}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 2474
    :catchall_0
    move-exception v0

    iput-object v8, p0, Lflipboard/service/Flap$LikeUnlikeRequest;->d:Lflipboard/service/ServiceReloginObserver;

    throw v0
.end method
