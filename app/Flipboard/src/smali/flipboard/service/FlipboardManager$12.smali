.class Lflipboard/service/FlipboardManager$12;
.super Ljava/lang/Object;
.source "FlipboardManager.java"

# interfaces
.implements Lflipboard/service/Flap$CommentaryObserver;


# instance fields
.field a:I

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:Lflipboard/service/Flap$CommentaryObserver;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/util/Map;Lflipboard/service/Flap$CommentaryObserver;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1242
    iput-object p1, p0, Lflipboard/service/FlipboardManager$12;->e:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$12;->b:Ljava/util/Map;

    iput-object p3, p0, Lflipboard/service/FlipboardManager$12;->c:Lflipboard/service/Flap$CommentaryObserver;

    iput-object p4, p0, Lflipboard/service/FlipboardManager$12;->d:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1242
    check-cast p1, Lflipboard/objs/CommentaryResult;

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    iget-object v1, p0, Lflipboard/service/FlipboardManager$12;->b:Ljava/util/Map;

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/HasCommentaryItem;

    if-nez v1, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "Null item for %s"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_4

    iget-object v2, v1, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v2, v2, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v2, v2, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    iget-object v7, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v3, "twitter"

    iget-object v10, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/objs/CommentaryResult$Item$Commentary;

    iget-object v11, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-static {v11}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    iget-object v11, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-static {v11}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    iget-object v11, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    iget-object v3, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    :goto_2
    if-nez v3, :cond_1

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, v0, Lflipboard/objs/CommentaryResult$Item;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lflipboard/objs/CommentaryResult$Item;->b:I

    goto :goto_1

    :cond_3
    iput-object v8, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    :cond_4
    invoke-virtual {v1, v0}, Lflipboard/objs/HasCommentaryItem;->a(Lflipboard/objs/CommentaryResult$Item;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lflipboard/service/FlipboardManager$12;->c:Lflipboard/service/Flap$CommentaryObserver;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/service/FlipboardManager$12;->c:Lflipboard/service/Flap$CommentaryObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/Object;)V

    :cond_6
    return-void

    :cond_7
    move v3, v5

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1297
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "Failed to get commentary: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v0

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1298
    iget v2, p0, Lflipboard/service/FlipboardManager$12;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lflipboard/service/FlipboardManager$12;->a:I

    if-gt v2, v6, :cond_0

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1299
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 1301
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v3, 0x7d0

    new-instance v4, Lflipboard/service/FlipboardManager$12$1;

    invoke-direct {v4, p0, p0}, Lflipboard/service/FlipboardManager$12$1;-><init>(Lflipboard/service/FlipboardManager$12;Lflipboard/service/Flap$CommentaryObserver;)V

    invoke-virtual {v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    .line 1309
    :cond_0
    iget-object v2, p0, Lflipboard/service/FlipboardManager$12;->c:Lflipboard/service/Flap$CommentaryObserver;

    if-eqz v2, :cond_2

    .line 1310
    iget-object v2, p0, Lflipboard/service/FlipboardManager$12;->c:Lflipboard/service/Flap$CommentaryObserver;

    const-string v3, "%s;retrying:%s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v0

    iget v5, p0, Lflipboard/service/FlipboardManager$12;->a:I

    if-gt v5, v6, :cond_1

    move v0, v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lflipboard/service/Flap$CommentaryObserver;->a(Ljava/lang/String;)V

    .line 1312
    :cond_2
    return-void
.end method
