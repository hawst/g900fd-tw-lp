.class Lflipboard/service/ServiceReloginObserver$1;
.super Ljava/lang/Object;
.source "ServiceReloginObserver.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflipboard/service/ServiceReloginObserver;


# direct methods
.method constructor <init>(Lflipboard/service/ServiceReloginObserver;Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lflipboard/service/ServiceReloginObserver$1;->d:Lflipboard/service/ServiceReloginObserver;

    iput-object p2, p0, Lflipboard/service/ServiceReloginObserver$1;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p3, p0, Lflipboard/service/ServiceReloginObserver$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/service/ServiceReloginObserver$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/service/ServiceReloginObserver$1;->a:Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/ServiceReloginObserver$1;->a:Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lflipboard/service/ServiceReloginObserver$1;->d:Lflipboard/service/ServiceReloginObserver;

    iget-object v1, p0, Lflipboard/service/ServiceReloginObserver$1;->b:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/ServiceReloginObserver$1;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/service/ServiceReloginObserver;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    iget-object v1, p0, Lflipboard/service/ServiceReloginObserver$1;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 29
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/service/ServiceReloginObserver$1;->a:Lflipboard/activities/FlipboardActivity;

    const-class v2, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 25
    const-string v1, "service"

    iget-object v2, p0, Lflipboard/service/ServiceReloginObserver$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    const-string v1, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginRelogin"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
