.class public Lflipboard/service/Flap$GovernorException;
.super Ljava/io/IOException;
.source "Flap.java"


# static fields
.field static final synthetic b:Z


# instance fields
.field public final a:Lflipboard/objs/FeedItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 881
    const-class v0, Lflipboard/service/Flap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/service/Flap$GovernorException;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 887
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 888
    sget-boolean v0, Lflipboard/service/Flap$GovernorException;->b:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 889
    :cond_0
    iput-object p1, p0, Lflipboard/service/Flap$GovernorException;->a:Lflipboard/objs/FeedItem;

    .line 890
    return-void
.end method
