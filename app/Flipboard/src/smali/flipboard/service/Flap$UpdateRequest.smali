.class public Lflipboard/service/Flap$UpdateRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Flap$SectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field final b:Z

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field protected e:I

.field f:Z

.field g:Lflipboard/service/Flap$ItemStreamer;

.field h:J

.field final i:Z

.field public j:Lflipboard/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Callback",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field k:I

.field final synthetic l:Lflipboard/service/Flap;

.field private m:Z

.field private q:Z

.field private r:I


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;ZZ)V
    .locals 2

    .prologue
    .line 931
    iput-object p1, p0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    .line 932
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 918
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/Flap$UpdateRequest;->i:Z

    .line 926
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/model/ConfigSetting;->shouldUseInlineAML()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/service/Flap$UpdateRequest;->q:Z

    .line 928
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/service/Flap$UpdateRequest;->k:I

    .line 1408
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/service/Flap$UpdateRequest;->r:I

    .line 933
    iput-boolean p3, p0, Lflipboard/service/Flap$UpdateRequest;->b:Z

    .line 934
    iput-boolean p4, p0, Lflipboard/service/Flap$UpdateRequest;->m:Z

    .line 935
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    .line 936
    invoke-static {p1}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 937
    :try_start_0
    invoke-static {p1}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 938
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 6

    .prologue
    .line 1425
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Flap$SectionInfo;

    .line 1426
    iget-wide v2, v0, Lflipboard/service/Flap$SectionInfo;->f:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1427
    iget-object v0, v0, Lflipboard/service/Flap$SectionInfo;->c:Lflipboard/service/Flap$UpdateObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$UpdateObserver;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1430
    :cond_1
    return-void
.end method

.method private b()V
    .locals 15

    .prologue
    .line 1030
    iget-boolean v0, p0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v0, :cond_0

    .line 1031
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    invoke-static {v0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 1032
    :try_start_0
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    invoke-static {v0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1033
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1037
    :cond_0
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Flap$SectionInfo;

    .line 1038
    iget-object v0, v0, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflipboard/service/Section;->c(Z)Z

    goto :goto_0

    .line 1033
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1041
    :cond_1
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_3

    .line 1042
    const-wide/16 v6, 0x0

    .line 1043
    const-wide/16 v4, 0x0

    .line 1044
    const/4 v3, 0x0

    .line 1045
    const/4 v2, 0x0

    .line 1046
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v8, v0, [J

    .line 1047
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Flap$SectionInfo;

    .line 1048
    iget-wide v10, v0, Lflipboard/service/Flap$SectionInfo;->f:J

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-lez v1, :cond_7

    .line 1049
    add-int/lit8 v1, v2, 0x1

    iget-wide v10, v0, Lflipboard/service/Flap$SectionInfo;->f:J

    aput-wide v10, v8, v2

    .line 1050
    iget-wide v10, v0, Lflipboard/service/Flap$SectionInfo;->f:J

    add-long/2addr v6, v10

    .line 1051
    iget-wide v10, v0, Lflipboard/service/Flap$SectionInfo;->f:J

    cmp-long v2, v10, v4

    if-lez v2, :cond_6

    .line 1052
    iget-wide v2, v0, Lflipboard/service/Flap$SectionInfo;->f:J

    .line 1053
    iget-object v0, v0, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    move-wide v4, v6

    move-object v14, v0

    move v0, v1

    move-object v1, v14

    :goto_2
    move-wide v6, v4

    move-wide v4, v2

    move-object v3, v1

    move v2, v0

    .line 1056
    goto :goto_1

    .line 1057
    :cond_2
    const/4 v0, 0x0

    invoke-static {v8, v0, v2}, Ljava/util/Arrays;->sort([JII)V

    .line 1058
    if-lez v2, :cond_3

    .line 1059
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v1

    const/4 v1, 0x1

    int-to-long v10, v2

    div-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v0, v1

    const/4 v1, 0x2

    div-int/lit8 v2, v2, 0x2

    aget-wide v6, v8, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 1064
    :cond_3
    iget-object v1, p0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget-object v4, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v1, v0}, Lflipboard/service/User;->d(Lflipboard/service/Section;)Z

    goto :goto_3

    .line 1065
    :cond_5
    return-void

    :cond_6
    move v0, v1

    move-object v1, v3

    move-wide v2, v4

    move-wide v4, v6

    goto :goto_2

    :cond_7
    move v0, v2

    move-object v1, v3

    move-wide v2, v4

    move-wide v4, v6

    goto :goto_2
.end method


# virtual methods
.method protected final a()V
    .locals 18

    .prologue
    .line 1069
    const/4 v5, 0x0

    .line 1070
    const/4 v4, 0x0

    .line 1071
    new-instance v9, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v9, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 1072
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 1073
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 1074
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v7

    .line 1077
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/service/Flap$UpdateRequest;->k:I

    if-ltz v2, :cond_1

    .line 1078
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/service/Flap$UpdateRequest;->k:I

    .line 1084
    :goto_0
    if-nez v2, :cond_35

    .line 1086
    const/16 v2, 0xa

    move v3, v2

    .line 1092
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/service/Flap$SectionInfo;

    .line 1094
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    iget-object v5, v2, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    invoke-virtual {v4, v5}, Lflipboard/service/User;->a(Lflipboard/service/Section;)V

    .line 1096
    iget-object v4, v2, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v9, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    iget-object v6, v2, Lflipboard/service/Flap$SectionInfo;->b:Ljava/lang/String;

    .line 1098
    iget-object v5, v2, Lflipboard/service/Flap$SectionInfo;->d:Landroid/os/Bundle;

    .line 1099
    if-nez v6, :cond_0

    iget-object v4, v2, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->l()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1101
    iget-object v2, v2, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v4, v3

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 1102
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v14, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v2, Lflipboard/objs/FeedItem;->t:I

    int-to-long v14, v14

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1103
    iget-object v13, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v11, v13, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104
    add-int/lit8 v2, v4, -0x1

    if-gtz v2, :cond_34

    :cond_0
    move-object v4, v5

    move-object v5, v6

    .line 1106
    goto :goto_2

    .line 1079
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/service/Flap$SectionInfo;

    iget-object v2, v2, Lflipboard/service/Flap$SectionInfo;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1080
    iget v2, v7, Lflipboard/model/ConfigSetting;->FeedFetchLoadMoreItemCount:I

    goto/16 :goto_0

    .line 1082
    :cond_2
    iget v2, v7, Lflipboard/model/ConfigSetting;->FeedFetchInitialItemCount:I

    goto/16 :goto_0

    .line 1117
    :cond_3
    const-string v2, ","

    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    new-instance v8, Lflipboard/service/Flap$UpdateRequest$1;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lflipboard/service/Flap$UpdateRequest$1;-><init>(Lflipboard/service/Flap$UpdateRequest;)V

    invoke-static {v2, v6, v8}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;Lflipboard/util/Format$Selector;)Ljava/lang/String;

    move-result-object v2

    .line 1125
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    const-string v8, "/v1/users/updateFeed"

    move-object/from16 v0, p0

    iget-object v12, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    const/16 v13, 0x8

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string v15, "sections"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object v2, v13, v14

    const/4 v2, 0x2

    const-string v14, "limit"

    aput-object v14, v13, v2

    const/4 v2, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v13, v2

    const/4 v2, 0x4

    const-string v3, "topStoryCount"

    aput-object v3, v13, v2

    const/4 v2, 0x5

    iget v3, v7, Lflipboard/model/ConfigSetting;->TopStoriesRequestCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v13, v2

    const/4 v2, 0x6

    const-string v3, "wasAutoRefresh"

    aput-object v3, v13, v2

    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/service/Flap$UpdateRequest;->b:Z

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v13, v3

    invoke-virtual {v6, v8, v12, v13}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1126
    if-eqz v5, :cond_4

    .line 1127
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&pageKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v5}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1129
    :cond_4
    if-eqz v4, :cond_7

    .line 1130
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v3, v2

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1131
    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1132
    if-eqz v7, :cond_6

    .line 1133
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "&%s=%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    const/4 v2, 0x1

    aput-object v7, v12, v2

    invoke-static {v8, v12}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_5

    .line 1125
    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    .line 1135
    :cond_6
    sget-object v7, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v8, "Value is null for flap parameter: %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v7, v8, v12}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_7
    move-object v3, v2

    .line 1139
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->c:Ljava/util/List;

    if-eqz v2, :cond_9

    .line 1140
    const-string v2, ","

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->c:Ljava/util/List;

    new-instance v6, Lflipboard/service/Flap$UpdateRequest$2;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lflipboard/service/Flap$UpdateRequest$2;-><init>(Lflipboard/service/Flap$UpdateRequest;)V

    invoke-static {v2, v4, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;Lflipboard/util/Format$Selector;)Ljava/lang/String;

    move-result-object v2

    .line 1148
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&coverSections="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1153
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/service/Flap$UpdateRequest;->q:Z

    if-eqz v2, :cond_a

    .line 1154
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&inlineAML=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1156
    :cond_a
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v4, "use_legacy_cover_stories"

    const/4 v6, 0x0

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 1157
    if-eqz v2, :cond_b

    .line 1158
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&forceOldCoverStories=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1161
    :cond_b
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 1163
    const/4 v8, 0x0

    .line 1164
    const/4 v7, 0x0

    .line 1165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 1168
    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1169
    const-string v3, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const-string v3, "Content-Encoding"

    const-string v4, "deflate"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    new-instance v3, Lorg/apache/http/entity/ByteArrayEntity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "item="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&item="

    invoke-static {v6, v10}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/JavaUtil;->b([B)[B

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1174
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    iget-object v3, v3, Lflipboard/service/Flap;->h:Landroid/content/Context;

    invoke-static {v3}, Lflipboard/util/AndroidUtil;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1175
    if-eqz v3, :cond_c

    .line 1176
    invoke-static {v3}, Lflipboard/gui/FLWebView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1177
    const-string v4, "X-Flipboard-User-Agent"

    invoke-virtual {v2, v4, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/service/Flap$UpdateRequest;->b:Z

    invoke-virtual {v3, v2, v4}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;Z)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 1181
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 1182
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    .line 1183
    sparse-switch v6, :sswitch_data_0

    .line 1199
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected response from flap: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lflipboard/service/Flap$UpdateRequest;->a(Ljava/lang/Exception;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1403
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    .line 1404
    :goto_6
    return-void

    .line 1190
    :sswitch_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->j:Lflipboard/util/Callback;

    if-eqz v4, :cond_e

    .line 1191
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->j:Lflipboard/util/Callback;

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V

    .line 1195
    :cond_d
    :goto_7
    new-instance v4, Lflipboard/service/Flap$ServiceDownForMaintenanceException;

    invoke-direct {v4}, Lflipboard/service/Flap$ServiceDownForMaintenanceException;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lflipboard/service/Flap$UpdateRequest;->a(Ljava/lang/Exception;)V

    .line 1202
    :sswitch_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v4, :cond_12

    .line 1403
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    goto :goto_6

    .line 1192
    :cond_e
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/service/Flap$UpdateRequest;->b:Z

    if-eqz v4, :cond_d

    .line 1193
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v6, v4, Lflipboard/service/FlipboardManager;->ay:Lflipboard/util/Callback;
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v6, :cond_11

    :try_start_3
    iget-object v6, v4, Lflipboard/service/FlipboardManager;->ay:Lflipboard/util/Callback;

    const/4 v10, 0x0

    invoke-interface {v6, v10}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v6, 0x0

    :try_start_4
    iput-object v6, v4, Lflipboard/service/FlipboardManager;->ay:Lflipboard/util/Callback;
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_7

    .line 1393
    :catch_0
    move-exception v2

    .line 1394
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v3, :cond_f

    .line 1395
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lflipboard/service/Flap$UpdateRequest;->a(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1403
    :cond_f
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    goto :goto_6

    .line 1193
    :catchall_0
    move-exception v2

    const/4 v3, 0x0

    :try_start_6
    iput-object v3, v4, Lflipboard/service/FlipboardManager;->ay:Lflipboard/util/Callback;

    throw v2
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1397
    :catch_1
    move-exception v2

    .line 1399
    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v3, :cond_10

    .line 1400
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lflipboard/service/Flap$UpdateRequest;->a(Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1403
    :cond_10
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    goto :goto_6

    .line 1193
    :cond_11
    :try_start_8
    new-instance v6, Lflipboard/service/FlipboardManager$20;

    invoke-direct {v6, v4}, Lflipboard/service/FlipboardManager$20;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v4, v6}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_8
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_7

    .line 1403
    :catchall_1
    move-exception v2

    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    throw v2

    .line 1207
    :cond_12
    :try_start_9
    new-instance v4, Lflipboard/service/Flap$ItemStreamer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lflipboard/service/Flap$UpdateRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v4, v6, v2}, Lflipboard/service/Flap$ItemStreamer;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;
    :try_end_9
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1213
    :cond_13
    :goto_8
    :try_start_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    invoke-virtual {v2}, Lflipboard/service/Flap$ItemStreamer;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_2c

    .line 1215
    iget-object v3, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v3, :cond_15

    iget-object v3, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "governor"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 1216
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 1217
    new-instance v3, Lflipboard/service/Flap$GovernorException;

    invoke-direct {v3, v2}, Lflipboard/service/Flap$GovernorException;-><init>(Lflipboard/objs/FeedItem;)V

    throw v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 1377
    :catchall_2
    move-exception v2

    :try_start_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    if-eqz v3, :cond_14

    .line 1378
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    iget v3, v3, Lflipboard/service/Flap$ItemStreamer;->f:I

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/service/Flap$UpdateRequest;->e:I

    .line 1379
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    invoke-virtual {v3}, Lflipboard/service/Flap$ItemStreamer;->close()V

    :cond_14
    throw v2
    :try_end_b
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1221
    :cond_15
    :try_start_c
    iget-object v3, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v3, :cond_17

    iget-object v3, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "userMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1222
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    invoke-virtual {v3}, Lflipboard/service/User;->c()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 1223
    iget-object v3, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget-object v3, v3, Lflipboard/objs/UserServices;->d:Lflipboard/objs/UserServices$StateRevisions;

    if-eqz v3, :cond_33

    iget-object v3, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget-object v3, v3, Lflipboard/objs/UserServices;->d:Lflipboard/objs/UserServices$StateRevisions;

    iget-object v3, v3, Lflipboard/objs/UserServices$StateRevisions;->a:Ljava/lang/String;

    if-eqz v3, :cond_33

    .line 1224
    iget-object v3, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget-object v3, v3, Lflipboard/objs/UserServices;->d:Lflipboard/objs/UserServices$StateRevisions;

    iget-object v3, v3, Lflipboard/objs/UserServices$StateRevisions;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    move v3, v8

    .line 1226
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    iget-object v6, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget-object v6, v6, Lflipboard/objs/UserServices;->a:Ljava/util/List;

    iget-object v8, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget-object v8, v8, Lflipboard/objs/UserServices;->b:Ljava/util/List;

    invoke-virtual {v4, v6, v8}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/util/List;)V

    move v4, v3

    move v3, v7

    .line 1232
    :goto_a
    iget-object v2, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget-object v2, v2, Lflipboard/objs/UserServices;->f:Lflipboard/json/FLObject;

    invoke-static {v2}, Lflipboard/abtest/Experiments;->a(Lflipboard/json/FLObject;)V

    move v7, v3

    move v8, v4

    .line 1233
    goto/16 :goto_8

    .line 1227
    :cond_16
    iget-object v3, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget v3, v3, Lflipboard/objs/UserServices;->e:I

    if-lez v3, :cond_32

    .line 1228
    sget-object v3, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v6, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget v6, v6, Lflipboard/objs/UserServices;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    .line 1229
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    iget-object v4, v2, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    iget v4, v4, Lflipboard/objs/UserServices;->e:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflipboard/service/User;->a(Ljava/lang/String;)V

    .line 1230
    const/4 v3, 0x1

    move v4, v8

    goto :goto_a

    .line 1240
    :cond_17
    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->P()Z

    move-result v3

    if-nez v3, :cond_18

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->O()Z

    move-result v3

    if-eqz v3, :cond_1b

    :cond_18
    iget-object v3, v2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v3, :cond_1b

    .line 1241
    iget-object v3, v2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1242
    :cond_19
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1243
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_19

    .line 1245
    const-string v4, "unwanted.FeedItem_subitem_null"

    invoke-static {v4}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 1246
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_b

    .line 1249
    :cond_1a
    iget-object v3, v2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1b

    .line 1251
    const-string v2, "unwanted.FeedItem_subitem_all_null"

    invoke-static {v2}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1256
    :cond_1b
    iget-object v3, v2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-interface {v9, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/service/Flap$SectionInfo;

    .line 1257
    if-nez v3, :cond_1c

    .line 1258
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Missing section for id: %s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v14, v2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    aput-object v14, v6, v10

    const/4 v10, 0x1

    aput-object v2, v6, v10

    invoke-virtual {v3, v4, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_8

    .line 1261
    :cond_1c
    iget-object v10, v3, Lflipboard/service/Flap$SectionInfo;->c:Lflipboard/service/Flap$UpdateObserver;

    .line 1263
    iget-object v4, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v4, :cond_28

    iget-object v4, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v6, "meta"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 1264
    iget-object v4, v2, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    if-eqz v4, :cond_1e

    iget-object v4, v2, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    const-string v6, "resetUser"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 1266
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    .line 1268
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 1270
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->l:Lflipboard/service/Flap;

    iget-object v2, v2, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    sget-object v3, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v4, "User reset by flap"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v3, Lflipboard/service/FlipboardManager$19;

    invoke-direct {v3, v2}, Lflipboard/service/FlipboardManager$19;-><init>(Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 1377
    :try_start_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    if-eqz v2, :cond_1d

    .line 1378
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    iget v2, v2, Lflipboard/service/Flap$ItemStreamer;->f:I

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/service/Flap$UpdateRequest;->e:I

    .line 1379
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    invoke-virtual {v2}, Lflipboard/service/Flap$ItemStreamer;->close()V
    :try_end_d
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1403
    :cond_1d
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    goto/16 :goto_6

    .line 1273
    :cond_1e
    :try_start_e
    iget-object v4, v2, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    if-eqz v4, :cond_1f

    iget-object v4, v2, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    iget-object v4, v4, Lflipboard/objs/Invite;->h:Ljava/lang/String;

    if-eqz v4, :cond_1f

    iget-object v4, v2, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    iget-object v4, v4, Lflipboard/objs/Invite;->g:Ljava/lang/String;

    if-eqz v4, :cond_1f

    .line 1274
    new-instance v4, Lflipboard/io/UsageEvent;

    const-string v6, "event"

    invoke-direct {v4, v6}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 1275
    const-string v6, "id"

    const-string v14, "didReceiveContributorInvite"

    invoke-virtual {v4, v6, v14}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1276
    const-string v6, "sectionIdentifier"

    iget-object v14, v3, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    iget-object v14, v14, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v14, v14, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v4, v6, v14}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1277
    invoke-virtual {v4}, Lflipboard/io/UsageEvent;->a()V

    .line 1278
    iget-object v4, v3, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    sget-object v6, Lflipboard/service/Section$Message;->h:Lflipboard/service/Section$Message;

    iget-object v14, v2, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    invoke-virtual {v4, v6, v14}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1280
    :cond_1f
    iget-boolean v4, v2, Lflipboard/objs/FeedItem;->ax:Z

    if-eqz v4, :cond_25

    .line 1281
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/service/Flap$UpdateRequest;->d:Z

    if-nez v4, :cond_20

    .line 1283
    iget-object v4, v3, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    const/4 v6, 0x0

    iput-boolean v6, v4, Lflipboard/service/Section;->F:Z

    .line 1285
    :cond_20
    invoke-interface {v10, v2}, Lflipboard/service/Flap$UpdateObserver;->a(Lflipboard/objs/FeedItem;)V

    .line 1287
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lflipboard/service/Flap$UpdateRequest;->h:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    iput-wide v14, v3, Lflipboard/service/Flap$SectionInfo;->f:J

    .line 1293
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lflipboard/service/Flap$UpdateRequest;->h:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    .line 1294
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v12

    .line 1295
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/service/Flap$UpdateRequest;->d:Z

    if-nez v4, :cond_24

    .line 1296
    iget-object v4, v2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v4, :cond_23

    iget-object v4, v2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    const-string v6, "auth/flipboard/coverstories"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 1297
    const-string v4, "cover_stories_load_time"

    move-wide/from16 v0, v16

    invoke-static {v4, v0, v1}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1298
    const-string v4, "cover_stories_load_time_plus_client_start_delay"

    invoke-static {v4, v14, v15}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1299
    const-string v4, "cover_stories_load_server_time"

    iget v6, v2, Lflipboard/objs/FeedItem;->r:I

    int-to-long v14, v6

    invoke-static {v4, v14, v15}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1310
    :cond_21
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/service/Flap$UpdateRequest;->m:Z

    if-nez v4, :cond_13

    .line 1311
    new-instance v4, Lflipboard/io/UsageEvent;

    const-string v6, "section"

    invoke-direct {v4, v6}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 1312
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v12

    iput-wide v14, v4, Lflipboard/io/UsageEvent;->g:J

    .line 1313
    const-string v6, "action"

    const-string v10, "load"

    invoke-virtual {v4, v6, v10}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1314
    const-string v6, "success"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v4, v6, v10}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1315
    const-string v6, "sectionType"

    const-string v10, "feed"

    invoke-virtual {v4, v6, v10}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1316
    const-string v6, "sectionIdentifier"

    iget-object v10, v3, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    invoke-virtual {v10}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v6, v10}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1317
    const-string v6, "itemReceivedCount"

    iget v3, v3, Lflipboard/service/Flap$SectionInfo;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1318
    const-string v3, "isLoadMore"

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lflipboard/service/Flap$UpdateRequest;->d:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v3, v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1319
    iget-object v3, v2, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    if-eqz v3, :cond_22

    .line 1320
    const-string v3, "strategy"

    iget-object v2, v2, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    invoke-virtual {v4, v3, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1322
    :cond_22
    invoke-virtual {v4}, Lflipboard/io/UsageEvent;->a()V

    goto/16 :goto_8

    .line 1300
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/service/Flap$SectionInfo;

    invoke-static {v4}, Lflipboard/service/Flap$SectionInfo;->a(Lflipboard/service/Flap$SectionInfo;)Z

    move-result v4

    if-nez v4, :cond_21

    .line 1301
    const-string v4, "section_load_on_demand_time"

    move-wide/from16 v0, v16

    invoke-static {v4, v0, v1}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1302
    const-string v4, "section_load_on_demand_time_plus_client_start_delay"

    invoke-static {v4, v14, v15}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1303
    const-string v4, "section_load_on_demand_server_time"

    iget v6, v2, Lflipboard/objs/FeedItem;->r:I

    int-to-long v14, v6

    invoke-static {v4, v14, v15}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    goto/16 :goto_c

    .line 1306
    :cond_24
    const-string v4, "section_load_load_more"

    move-wide/from16 v0, v16

    invoke-static {v4, v0, v1}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1307
    const-string v4, "section_load_load_more_plus_client_start_delay"

    invoke-static {v4, v14, v15}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1308
    const-string v4, "section_load_load_more_server_time"

    iget v6, v2, Lflipboard/objs/FeedItem;->r:I

    int-to-long v14, v6

    invoke-static {v4, v14, v15}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    goto/16 :goto_c

    .line 1325
    :cond_25
    iget-object v4, v3, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    .line 1327
    if-eqz v4, :cond_26

    iget-object v6, v2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v6, :cond_26

    .line 1328
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    new-instance v14, Lflipboard/service/Flap$UpdateRequest$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v4, v2, v3}, Lflipboard/service/Flap$UpdateRequest$3;-><init>(Lflipboard/service/Flap$UpdateRequest;Lflipboard/objs/TOCSection;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$SectionInfo;)V

    invoke-virtual {v6, v14}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 1360
    :cond_26
    if-eqz v5, :cond_27

    const/4 v3, 0x1

    :goto_d
    invoke-interface {v10, v2, v3}, Lflipboard/service/Flap$UpdateObserver;->a(Lflipboard/objs/FeedItem;Z)V

    goto/16 :goto_8

    :cond_27
    const/4 v3, 0x0

    goto :goto_d

    .line 1363
    :cond_28
    iget-object v4, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-nez v4, :cond_2a

    const/4 v4, 0x1

    move v6, v4

    .line 1364
    :goto_e
    if-eqz v6, :cond_29

    .line 1365
    iget-object v4, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v11, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/objs/FeedItem;

    .line 1366
    if-nez v4, :cond_2b

    .line 1367
    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v14, "missing item for abbrev item: %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v2, v15, v16

    invoke-virtual {v4, v14, v15}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1372
    :cond_29
    :goto_f
    iget v4, v3, Lflipboard/service/Flap$SectionInfo;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lflipboard/service/Flap$SectionInfo;->e:I

    .line 1373
    invoke-interface {v10, v2, v6}, Lflipboard/service/Flap$UpdateObserver;->b(Lflipboard/objs/FeedItem;Z)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto/16 :goto_8

    .line 1363
    :cond_2a
    const/4 v4, 0x0

    move v6, v4

    goto :goto_e

    :cond_2b
    move-object v2, v4

    .line 1369
    goto :goto_f

    .line 1377
    :cond_2c
    :try_start_f
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    if-eqz v2, :cond_2d

    .line 1378
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    iget v2, v2, Lflipboard/service/Flap$ItemStreamer;->f:I

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/service/Flap$UpdateRequest;->e:I

    .line 1379
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    invoke-virtual {v2}, Lflipboard/service/Flap$ItemStreamer;->close()V

    .line 1384
    :cond_2d
    if-eqz v8, :cond_30

    .line 1385
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1386
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    iget-object v3, v2, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    if-eqz v3, :cond_2e

    iget-object v3, v2, Lflipboard/service/User;->j:Lflipboard/objs/UserState;

    invoke-virtual {v3}, Lflipboard/objs/UserState;->a()I

    move-result v3

    if-eq v8, v3, :cond_2f

    :cond_2e
    sget-object v3, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V
    :try_end_f
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 1403
    :cond_2f
    :goto_10
    invoke-direct/range {p0 .. p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    goto/16 :goto_6

    .line 1387
    :cond_30
    if-eqz v7, :cond_31

    .line 1388
    :try_start_10
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    .line 1389
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/service/Flap$UpdateRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    goto :goto_10

    .line 1391
    :cond_31
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;
    :try_end_10
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto :goto_10

    :cond_32
    move v3, v7

    move v4, v8

    goto/16 :goto_a

    :cond_33
    move v3, v8

    goto/16 :goto_9

    :cond_34
    move v4, v2

    goto/16 :goto_3

    :cond_35
    move v3, v2

    goto/16 :goto_1

    .line 1183
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x1a2 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 948
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->c:Ljava/util/List;

    .line 949
    return-void
.end method

.method public final a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 966
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 967
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Pagekey: \'%s\', section: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v0

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 968
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Added section with pagekey that is not null but still empty"

    invoke-direct {v3, v4, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 972
    invoke-virtual {v3}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 973
    new-instance v2, Lflipboard/service/FlCrashListener;

    invoke-direct {v2}, Lflipboard/service/FlCrashListener;-><init>()V

    invoke-static {v3, v2}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 976
    :cond_0
    invoke-virtual {p1}, Lflipboard/service/Section;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    .line 977
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 978
    iput-boolean v0, p1, Lflipboard/service/Section;->B:Z

    .line 997
    :cond_1
    :goto_0
    return v0

    .line 981
    :cond_2
    iget-object v2, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v2, v2, Lflipboard/service/Section$Meta;->b:Z

    if-nez v2, :cond_1

    .line 984
    iput-boolean v0, p1, Lflipboard/service/Section;->B:Z

    .line 985
    invoke-virtual {p1, v1}, Lflipboard/service/Section;->c(Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 986
    if-eqz p2, :cond_4

    .line 987
    iget-boolean v0, p0, Lflipboard/service/Flap$UpdateRequest;->d:Z

    if-eqz v0, :cond_3

    .line 988
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "only one \'more\' request at a time is supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 990
    :cond_3
    iput-boolean v1, p0, Lflipboard/service/Flap$UpdateRequest;->d:Z

    .line 992
    :cond_4
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    new-instance v2, Lflipboard/service/Flap$SectionInfo;

    new-instance v3, Lflipboard/service/Section$UpdateObserver;

    invoke-direct {v3, p1}, Lflipboard/service/Section$UpdateObserver;-><init>(Lflipboard/service/Section;)V

    invoke-direct {v2, p1, p2, p3, v3}, Lflipboard/service/Flap$SectionInfo;-><init>(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;Lflipboard/service/Flap$UpdateObserver;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 993
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/TOCSection;->I:J

    move v0, v1

    .line 994
    goto :goto_0

    .line 996
    :cond_5
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1019
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/service/Flap$UpdateRequest;->h:J

    .line 1020
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1021
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 1026
    :goto_0
    return-void

    .line 1023
    :cond_0
    sget-object v0, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    const-string v1, "No sections to update!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1024
    invoke-direct {p0}, Lflipboard/service/Flap$UpdateRequest;->b()V

    goto :goto_0
.end method

.method public declared-synchronized cancel()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1003
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    if-nez v1, :cond_1

    .line 1004
    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/service/Flap$UpdateRequest;->f:Z

    .line 1005
    iget-object v1, p0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    if-eqz v1, :cond_0

    .line 1006
    iget-object v1, p0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;

    invoke-virtual {v1}, Lflipboard/service/Flap$ItemStreamer;->b()V

    .line 1007
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/Flap$UpdateRequest;->g:Lflipboard/service/Flap$ItemStreamer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1003
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
