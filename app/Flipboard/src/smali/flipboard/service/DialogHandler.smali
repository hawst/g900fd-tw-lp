.class public Lflipboard/service/DialogHandler;
.super Ljava/lang/Object;
.source "DialogHandler.java"


# direct methods
.method public static a(Lflipboard/activities/FlipboardActivity;)V
    .locals 2

    .prologue
    .line 194
    if-eqz p0, :cond_0

    const-string v0, "maintenance"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 198
    new-instance v1, Lflipboard/service/DialogHandler$5;

    invoke-direct {v1, p0}, Lflipboard/service/DialogHandler$5;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 232
    if-nez p0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 235
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 236
    new-instance v1, Lflipboard/service/DialogHandler$6;

    invoke-direct {v1, p0, p1}, Lflipboard/service/DialogHandler$6;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 79
    if-eqz p0, :cond_0

    invoke-static {p0, p2}, Lflipboard/service/DialogHandler;->c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 84
    new-instance v1, Lflipboard/service/DialogHandler$2;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/service/DialogHandler$2;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 109
    if-eqz p0, :cond_0

    const-string v0, "loading"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 114
    new-instance v1, Lflipboard/service/DialogHandler$3;

    invoke-direct {v1, p0, p2, p1, p3}, Lflipboard/service/DialogHandler$3;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 37
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 38
    new-instance v1, Lflipboard/service/DialogHandler$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lflipboard/service/DialogHandler$1;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->a()V

    :cond_0
    return-void
.end method

.method public static b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 156
    if-eqz p0, :cond_0

    const-string v0, "authenticating"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 160
    new-instance v1, Lflipboard/service/DialogHandler$4;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/service/DialogHandler$4;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private static c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 267
    if-eqz p0, :cond_0

    iget-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
