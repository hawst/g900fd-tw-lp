.class Lflipboard/service/Flap$ReadRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Lflipboard/service/Flap$JSONResultObserver;

.field e:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic f:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4095
    iput-object p1, p0, Lflipboard/service/Flap$ReadRequest;->f:Lflipboard/service/Flap;

    .line 4096
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4097
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 4130
    iget-object v0, p0, Lflipboard/service/Flap$ReadRequest;->f:Lflipboard/service/Flap;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/v1/social/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/service/Flap$ReadRequest;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/service/Flap$ReadRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "sectionid"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$ReadRequest;->b:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "service"

    aput-object v4, v3, v8

    iget-object v4, p0, Lflipboard/service/Flap$ReadRequest;->c:Ljava/lang/String;

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 4131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 4132
    iget-object v0, p0, Lflipboard/service/Flap$ReadRequest;->e:Ljava/util/Collection;

    if-eqz v0, :cond_1

    .line 4133
    iget-object v0, p0, Lflipboard/service/Flap$ReadRequest;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 4134
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 4135
    const-string v4, "&"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4137
    :cond_0
    const-string v4, "oid="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4138
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 4141
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4143
    sget-object v2, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/service/Flap$ReadRequest;->a:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v1, v2, v7

    aput-object v0, v2, v8

    .line 4145
    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 4146
    const-string v1, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v2, v1, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4147
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v2, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 4149
    iget-object v0, p0, Lflipboard/service/Flap$ReadRequest;->f:Lflipboard/service/Flap;

    invoke-virtual {v0, v2}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 4150
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 4151
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v2, v0}, Lflipboard/service/Flap$ReadRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4152
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 4153
    const/16 v2, 0xc8

    if-eq v0, v2, :cond_2

    .line 4154
    iget-object v0, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4172
    iput-object v5, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    .line 4173
    :goto_1
    return-void

    .line 4157
    :cond_2
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 4158
    if-eqz v0, :cond_4

    .line 4159
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4160
    iget-object v1, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4172
    :goto_2
    iput-object v5, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_1

    .line 4162
    :cond_3
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 4167
    :catch_0
    move-exception v0

    .line 4168
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4172
    iput-object v5, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_1

    .line 4165
    :cond_4
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 4169
    :catch_1
    move-exception v0

    .line 4170
    :try_start_5
    iget-object v1, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4172
    iput-object v5, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4101
    const-string v0, "read"

    iput-object v0, p0, Lflipboard/service/Flap$ReadRequest;->a:Ljava/lang/String;

    .line 4102
    iput-object p1, p0, Lflipboard/service/Flap$ReadRequest;->b:Ljava/lang/String;

    .line 4103
    iput-object p2, p0, Lflipboard/service/Flap$ReadRequest;->c:Ljava/lang/String;

    .line 4104
    iput-object p3, p0, Lflipboard/service/Flap$ReadRequest;->e:Ljava/util/Collection;

    .line 4105
    iput-object p4, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    .line 4106
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4107
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Collection;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4120
    const-string v0, "unread"

    iput-object v0, p0, Lflipboard/service/Flap$ReadRequest;->a:Ljava/lang/String;

    .line 4121
    iput-object p1, p0, Lflipboard/service/Flap$ReadRequest;->b:Ljava/lang/String;

    .line 4122
    iput-object p2, p0, Lflipboard/service/Flap$ReadRequest;->e:Ljava/util/Collection;

    .line 4123
    iput-object p3, p0, Lflipboard/service/Flap$ReadRequest;->d:Lflipboard/service/Flap$JSONResultObserver;

    .line 4124
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4125
    return-void
.end method
