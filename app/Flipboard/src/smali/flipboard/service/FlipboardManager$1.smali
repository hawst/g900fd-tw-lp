.class Lflipboard/service/FlipboardManager$1;
.super Ljava/util/Timer;
.source "FlipboardManager.java"


# instance fields
.field final a:I

.field b:I

.field final synthetic c:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 515
    iput-object p1, p0, Lflipboard/service/FlipboardManager$1;->c:Lflipboard/service/FlipboardManager;

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    .line 516
    const/16 v0, 0xa

    iput v0, p0, Lflipboard/service/FlipboardManager$1;->a:I

    return-void
.end method

.method private a(Ljava/lang/IllegalStateException;)V
    .locals 5

    .prologue
    .line 546
    iget v0, p0, Lflipboard/service/FlipboardManager$1;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/service/FlipboardManager$1;->b:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 547
    throw p1

    .line 549
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "ignoring timer exception %d: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/service/FlipboardManager$1;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 550
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 522
    invoke-super {p0}, Ljava/util/Timer;->cancel()V

    .line 523
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v1, "It is an error to cancel this shared timer, dudes: %T"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 527
    return-void
.end method

.method public schedule(Ljava/util/TimerTask;J)V
    .locals 2

    .prologue
    .line 531
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 535
    :goto_0
    return-void

    .line 532
    :catch_0
    move-exception v0

    .line 533
    invoke-direct {p0, v0}, Lflipboard/service/FlipboardManager$1;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0
.end method

.method public schedule(Ljava/util/TimerTask;JJ)V
    .locals 2

    .prologue
    .line 539
    :try_start_0
    invoke-super/range {p0 .. p5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    :goto_0
    return-void

    .line 540
    :catch_0
    move-exception v0

    .line 541
    invoke-direct {p0, v0}, Lflipboard/service/FlipboardManager$1;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0
.end method
