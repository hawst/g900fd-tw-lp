.class public Lflipboard/service/Flap$ShareListRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Lflipboard/objs/ConfigService;

.field b:Ljava/lang/String;

.field c:Lflipboard/service/Flap$SectionListObserver;

.field d:Lflipboard/service/Flap$SectionListInputStream;

.field e:Z

.field final synthetic f:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 3855
    iput-object p1, p0, Lflipboard/service/Flap$ShareListRequest;->f:Lflipboard/service/Flap;

    .line 3856
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3857
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/ConfigService;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$ShareListRequest;
    .locals 1

    .prologue
    .line 3861
    iput-object p1, p0, Lflipboard/service/Flap$ShareListRequest;->a:Lflipboard/objs/ConfigService;

    .line 3862
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->b:Ljava/lang/String;

    .line 3863
    iput-object p2, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    .line 3864
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 3865
    return-object p0
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 3884
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->f:Lflipboard/service/Flap;

    const-string v1, "/v1/social/getMyShareLists"

    iget-object v2, p0, Lflipboard/service/Flap$ShareListRequest;->n:Lflipboard/service/User;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "service"

    aput-object v4, v3, v6

    iget-object v4, p0, Lflipboard/service/Flap$ShareListRequest;->a:Lflipboard/objs/ConfigService;

    iget-object v4, v4, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3885
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3886
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pageKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->b:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3888
    :cond_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 3890
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3891
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3892
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->f:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3893
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3894
    new-instance v3, Lflipboard/service/Flap$SectionListInputStream;

    iget-object v4, p0, Lflipboard/service/Flap$ShareListRequest;->f:Lflipboard/service/Flap;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$ShareListRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lflipboard/service/Flap$SectionListInputStream;-><init>(Lflipboard/service/Flap;Ljava/io/InputStream;)V

    iput-object v3, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3896
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 3897
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    .line 3898
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3907
    :try_start_2
    iget-boolean v0, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    if-nez v0, :cond_1

    .line 3908
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    invoke-virtual {v0}, Lflipboard/service/Flap$SectionListInputStream;->close()V

    .line 3909
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3918
    :cond_1
    iput-object v5, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    .line 3919
    :goto_0
    return-void

    .line 3900
    :cond_2
    :try_start_3
    iget-boolean v0, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    if-eqz v0, :cond_4

    .line 3901
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    const-string v1, "request was canceled"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3907
    :try_start_4
    iget-boolean v0, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    if-nez v0, :cond_3

    .line 3908
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    invoke-virtual {v0}, Lflipboard/service/Flap$SectionListInputStream;->close()V

    .line 3909
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3918
    :cond_3
    iput-object v5, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    .line 3904
    :cond_4
    :try_start_5
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    invoke-virtual {v0}, Lflipboard/service/Flap$SectionListInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListResult;

    .line 3905
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3907
    :try_start_6
    iget-boolean v0, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    if-nez v0, :cond_5

    .line 3908
    iget-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    invoke-virtual {v0}, Lflipboard/service/Flap$SectionListInputStream;->close()V

    .line 3909
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;
    :try_end_6
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 3918
    :cond_5
    iput-object v5, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    .line 3907
    :catchall_0
    move-exception v0

    :try_start_7
    iget-boolean v1, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    if-nez v1, :cond_6

    .line 3908
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    invoke-virtual {v1}, Lflipboard/service/Flap$SectionListInputStream;->close()V

    .line 3909
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    :cond_6
    throw v0
    :try_end_7
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 3912
    :catch_0
    move-exception v0

    .line 3913
    :try_start_8
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 3918
    iput-object v5, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    .line 3914
    :catch_1
    move-exception v0

    .line 3915
    :try_start_9
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 3916
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$SectionListObserver;->a(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 3918
    iput-object v5, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    goto :goto_0

    :catchall_1
    move-exception v0

    iput-object v5, p0, Lflipboard/service/Flap$ShareListRequest;->c:Lflipboard/service/Flap$SectionListObserver;

    throw v0
.end method

.method public cancel()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3870
    iget-boolean v1, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    if-nez v1, :cond_1

    .line 3871
    iput-boolean v0, p0, Lflipboard/service/Flap$ShareListRequest;->e:Z

    .line 3872
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    if-eqz v1, :cond_0

    .line 3873
    iget-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    invoke-virtual {v1}, Lflipboard/service/Flap$SectionListInputStream;->b()V

    .line 3874
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/service/Flap$ShareListRequest;->d:Lflipboard/service/Flap$SectionListInputStream;

    .line 3878
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
