.class public final enum Lflipboard/service/Section$Message;
.super Ljava/lang/Enum;
.source "Section.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/Section$Message;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/Section$Message;

.field public static final enum b:Lflipboard/service/Section$Message;

.field public static final enum c:Lflipboard/service/Section$Message;

.field public static final enum d:Lflipboard/service/Section$Message;

.field public static final enum e:Lflipboard/service/Section$Message;

.field public static final enum f:Lflipboard/service/Section$Message;

.field public static final enum g:Lflipboard/service/Section$Message;

.field public static final enum h:Lflipboard/service/Section$Message;

.field private static final synthetic i:[Lflipboard/service/Section$Message;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 151
    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->a:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "NEW_TOC_ITEM"

    invoke-direct {v0, v1, v4}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->b:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "RELOGIN"

    invoke-direct {v0, v1, v5}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "END_UPDATE"

    invoke-direct {v0, v1, v6}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "EXCEPTION"

    invoke-direct {v0, v1, v7}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "NEW_COVER_ITEM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->f:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "NEW_SIDEBAR_DATA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->g:Lflipboard/service/Section$Message;

    new-instance v0, Lflipboard/service/Section$Message;

    const-string v1, "ACCEPT_INVITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/service/Section$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/Section$Message;->h:Lflipboard/service/Section$Message;

    const/16 v0, 0x8

    new-array v0, v0, [Lflipboard/service/Section$Message;

    sget-object v1, Lflipboard/service/Section$Message;->a:Lflipboard/service/Section$Message;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/Section$Message;->b:Lflipboard/service/Section$Message;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/Section$Message;->f:Lflipboard/service/Section$Message;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/service/Section$Message;->g:Lflipboard/service/Section$Message;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/service/Section$Message;->h:Lflipboard/service/Section$Message;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/Section$Message;->i:[Lflipboard/service/Section$Message;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/Section$Message;
    .locals 1

    .prologue
    .line 151
    const-class v0, Lflipboard/service/Section$Message;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section$Message;

    return-object v0
.end method

.method public static values()[Lflipboard/service/Section$Message;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lflipboard/service/Section$Message;->i:[Lflipboard/service/Section$Message;

    invoke-virtual {v0}, [Lflipboard/service/Section$Message;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/Section$Message;

    return-object v0
.end method
