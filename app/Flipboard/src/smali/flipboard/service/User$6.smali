.class Lflipboard/service/User$6;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 577
    iput-object p1, p0, Lflipboard/service/User$6;->b:Lflipboard/service/User;

    iput-object p2, p0, Lflipboard/service/User$6;->a:Ljava/util/List;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 580
    iget-object v0, p0, Lflipboard/service/User$6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 581
    sget-object v2, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    .line 582
    const-string v2, "id = ?"

    new-array v3, v6, [Ljava/lang/String;

    iget v4, v0, Lflipboard/service/Account;->a:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lflipboard/service/User$6;->b(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 583
    iget-object v2, p0, Lflipboard/service/User$6;->b:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-virtual {v0}, Lflipboard/service/Account;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    iget-object v2, p0, Lflipboard/service/User$6;->b:Lflipboard/service/User;

    sget-object v3, Lflipboard/service/User$Message;->h:Lflipboard/service/User$Message;

    invoke-virtual {v2, v3, v0}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 586
    :cond_0
    return-void
.end method
