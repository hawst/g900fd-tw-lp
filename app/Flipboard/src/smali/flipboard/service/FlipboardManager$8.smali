.class Lflipboard/service/FlipboardManager$8;
.super Landroid/content/BroadcastReceiver;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 814
    iput-object p1, p0, Lflipboard/service/FlipboardManager$8;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 817
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    .line 818
    iget-object v0, p0, Lflipboard/service/FlipboardManager$8;->a:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->U:Lflipboard/json/FLObject;

    iget-boolean v1, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lflipboard/service/FlipboardManager;->ax:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0}, Lflipboard/service/RemoteWatchedFile;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 819
    :cond_1
    return-void
.end method
