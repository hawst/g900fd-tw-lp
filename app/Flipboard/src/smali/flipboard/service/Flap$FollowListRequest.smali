.class public Lflipboard/service/Flap$FollowListRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field public a:Lflipboard/service/Flap$FollowListType;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field g:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic h:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 1

    .prologue
    .line 2645
    iput-object p1, p0, Lflipboard/service/Flap$FollowListRequest;->h:Lflipboard/service/Flap;

    .line 2646
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 2642
    const-string v0, "flipboard"

    iput-object v0, p0, Lflipboard/service/Flap$FollowListRequest;->f:Ljava/lang/String;

    .line 2647
    return-void
.end method

.method public static synthetic a(Lflipboard/service/Flap$FollowListRequest;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$FollowListType;ZLjava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1

    .prologue
    .line 2635
    iput-object p1, p0, Lflipboard/service/Flap$FollowListRequest;->c:Ljava/lang/String;

    iput-object p2, p0, Lflipboard/service/Flap$FollowListRequest;->d:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/service/Flap$FollowListRequest;->a:Lflipboard/service/Flap$FollowListType;

    iput-boolean p4, p0, Lflipboard/service/Flap$FollowListRequest;->e:Z

    invoke-static {p5}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p5, p0, Lflipboard/service/Flap$FollowListRequest;->f:Ljava/lang/String;

    :cond_0
    iput-object p6, p0, Lflipboard/service/Flap$FollowListRequest;->g:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {p0}, Lflipboard/service/Flap$FollowListRequest;->c()V

    return-void
.end method

.method public static synthetic a(Lflipboard/service/Flap$FollowListRequest;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 1

    .prologue
    .line 2635
    iput-object p1, p0, Lflipboard/service/Flap$FollowListRequest;->c:Ljava/lang/String;

    iput-object p2, p0, Lflipboard/service/Flap$FollowListRequest;->b:Ljava/util/List;

    sget-object v0, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    iput-object v0, p0, Lflipboard/service/Flap$FollowListRequest;->a:Lflipboard/service/Flap$FollowListType;

    invoke-static {p3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p3, p0, Lflipboard/service/Flap$FollowListRequest;->f:Ljava/lang/String;

    :cond_0
    iput-object p4, p0, Lflipboard/service/Flap$FollowListRequest;->g:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {p0}, Lflipboard/service/Flap$FollowListRequest;->c()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2673
    const/4 v0, 0x0

    .line 2675
    iget-object v1, p0, Lflipboard/service/Flap$FollowListRequest;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 2676
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2677
    iget-object v0, p0, Lflipboard/service/Flap$FollowListRequest;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2678
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "friend"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 2682
    :cond_1
    iget-object v1, p0, Lflipboard/service/Flap$FollowListRequest;->h:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$FollowListRequest;->a:Lflipboard/service/Flap$FollowListType;

    iget-object v2, v2, Lflipboard/service/Flap$FollowListType;->e:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/service/Flap$FollowListRequest;->n:Lflipboard/service/User;

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "pageKey"

    aput-object v5, v4, v7

    iget-object v5, p0, Lflipboard/service/Flap$FollowListRequest;->d:Ljava/lang/String;

    aput-object v5, v4, v8

    const/4 v5, 0x2

    const-string v6, "service"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lflipboard/service/Flap$FollowListRequest;->f:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "serviceUserid"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-object v6, p0, Lflipboard/service/Flap$FollowListRequest;->c:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "email"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object v0, v4, v5

    const/16 v0, 0x8

    const-string v5, "showStaffPicks"

    aput-object v5, v4, v0

    const/16 v0, 0x9

    iget-boolean v5, p0, Lflipboard/service/Flap$FollowListRequest;->e:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2683
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 2684
    iget-object v1, p0, Lflipboard/service/Flap$FollowListRequest;->h:Lflipboard/service/Flap;

    iget-object v2, p0, Lflipboard/service/Flap$FollowListRequest;->g:Lflipboard/service/Flap$JSONResultObserver;

    invoke-static {v1, v2, v0, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/Flap;Lflipboard/service/Flap$JSONResultObserver;Ljava/lang/String;Lflipboard/service/Flap$Request;)V

    .line 2685
    return-void
.end method
