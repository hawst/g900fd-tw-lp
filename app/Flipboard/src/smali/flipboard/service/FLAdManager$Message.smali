.class public final enum Lflipboard/service/FLAdManager$Message;
.super Ljava/lang/Enum;
.source "FLAdManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/FLAdManager$Message;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/FLAdManager$Message;

.field private static final synthetic b:[Lflipboard/service/FLAdManager$Message;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 85
    new-instance v0, Lflipboard/service/FLAdManager$Message;

    const-string v1, "AD_RECEIVED"

    invoke-direct {v0, v1}, Lflipboard/service/FLAdManager$Message;-><init>(Ljava/lang/String;)V

    sput-object v0, Lflipboard/service/FLAdManager$Message;->a:Lflipboard/service/FLAdManager$Message;

    .line 84
    const/4 v0, 0x1

    new-array v0, v0, [Lflipboard/service/FLAdManager$Message;

    const/4 v1, 0x0

    sget-object v2, Lflipboard/service/FLAdManager$Message;->a:Lflipboard/service/FLAdManager$Message;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/FLAdManager$Message;->b:[Lflipboard/service/FLAdManager$Message;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/FLAdManager$Message;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lflipboard/service/FLAdManager$Message;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager$Message;

    return-object v0
.end method

.method public static values()[Lflipboard/service/FLAdManager$Message;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lflipboard/service/FLAdManager$Message;->b:[Lflipboard/service/FLAdManager$Message;

    invoke-virtual {v0}, [Lflipboard/service/FLAdManager$Message;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/FLAdManager$Message;

    return-object v0
.end method
