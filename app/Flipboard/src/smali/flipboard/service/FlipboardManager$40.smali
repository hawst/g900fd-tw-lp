.class Lflipboard/service/FlipboardManager$40;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lflipboard/service/FlipboardManager;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 3366
    iput-object p1, p0, Lflipboard/service/FlipboardManager$40;->b:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/service/FlipboardManager$40;->a:Landroid/app/Activity;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 3370
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogAdapter;->b(Landroid/support/v4/app/DialogFragment;)V

    .line 3372
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->AppDownloadURL:Ljava/lang/String;

    .line 3373
    if-eqz v0, :cond_0

    .line 3374
    sget-boolean v1, Lflipboard/service/FlipboardManager;->p:Z

    if-eqz v1, :cond_1

    .line 3375
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3376
    const-string v1, "com.bn.sdk.shop.details"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3377
    const-string v1, "product_details_ean"

    const-string v2, "2940043906816"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3378
    iget-object v1, p0, Lflipboard/service/FlipboardManager$40;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 3383
    :cond_0
    :goto_0
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "user-notification"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 3384
    const-string v1, "reason"

    const-string v2, "update clicked"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3385
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 3386
    return-void

    .line 3380
    :cond_1
    iget-object v1, p0, Lflipboard/service/FlipboardManager$40;->a:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
