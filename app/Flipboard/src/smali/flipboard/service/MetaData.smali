.class public Lflipboard/service/MetaData;
.super Ljava/lang/Object;
.source "MetaData.java"


# static fields
.field static final a:Lflipboard/util/Log;


# instance fields
.field private b:Lflipboard/service/DatabaseRow;

.field private c:Lflipboard/json/FLObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    sput-object v0, Lflipboard/service/MetaData;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Lflipboard/service/DatabaseRow;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lflipboard/service/MetaData;->b:Lflipboard/service/DatabaseRow;

    .line 27
    return-void
.end method

.method static synthetic a(Lflipboard/service/MetaData;Lflipboard/json/FLObject;)Lflipboard/json/FLObject;
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/MetaData;)Lflipboard/service/DatabaseRow;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lflipboard/service/MetaData;->b:Lflipboard/service/DatabaseRow;

    return-object v0
.end method

.method static synthetic b(Lflipboard/service/MetaData;)Lflipboard/json/FLObject;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    return-object v0
.end method


# virtual methods
.method public final a()Lflipboard/json/FLObject;
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    if-nez v0, :cond_0

    .line 32
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/service/MetaData;->b:Lflipboard/service/DatabaseRow;

    invoke-interface {v1}, Lflipboard/service/DatabaseRow;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflipboard/service/MetaData$1;

    invoke-direct {v2, p0}, Lflipboard/service/MetaData$1;-><init>(Lflipboard/service/MetaData;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    return-object v0
.end method

.method public final a([B)V
    .locals 5

    .prologue
    .line 51
    if-eqz p1, :cond_1

    .line 52
    :try_start_0
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p1}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    .line 53
    iget-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lflipboard/service/MetaData;->b:Lflipboard/service/DatabaseRow;

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    iput-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    sget-object v1, Lflipboard/service/MetaData;->a:Lflipboard/util/Log;

    const-string v2, "failed to load meta data: %e"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 193
    instance-of v0, p1, Lflipboard/service/MetaData;

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lflipboard/service/MetaData;->a()Lflipboard/json/FLObject;

    move-result-object v0

    check-cast p1, Lflipboard/service/MetaData;

    invoke-virtual {p1}, Lflipboard/service/MetaData;->a()Lflipboard/json/FLObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 196
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    if-nez v0, :cond_0

    const-string v0, "<not loaded>"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/service/MetaData;->c:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
