.class public Lflipboard/service/FLSearchManager;
.super Ljava/lang/Object;
.source "FLSearchManager.java"


# instance fields
.field final a:I

.field b:Landroid/content/Context;

.field public c:Ljava/lang/String;

.field public d:Lflipboard/service/Flap$SearchType;

.field e:Lflipboard/service/FLSearchManager$ResultType;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/FLSearchManager$LocalSearchableItem;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lflipboard/service/FLSearchManager;->a:I

    .line 46
    iput-object p1, p0, Lflipboard/service/FLSearchManager;->b:Landroid/content/Context;

    .line 47
    iput-boolean p2, p0, Lflipboard/service/FLSearchManager;->g:Z

    .line 48
    return-void
.end method

.method static synthetic a(Lflipboard/service/FLSearchManager;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lflipboard/service/FLSearchManager;->g:Z

    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 174
    if-nez p0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lflipboard/service/FLSearchManager$LocalSearchObserver;)V
    .locals 6

    .prologue
    .line 140
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->R:Ljava/util/concurrent/ConcurrentHashMap;

    .line 141
    const-string v1, "flipsearch"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    new-instance v1, Lflipboard/objs/ConfigService;

    invoke-direct {v1}, Lflipboard/objs/ConfigService;-><init>()V

    .line 143
    const-string v2, "flipsearch"

    iput-object v2, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    .line 144
    const-string v2, "v1/social/sectionList"

    iput-object v2, v1, Lflipboard/objs/ConfigService;->F:Ljava/lang/String;

    .line 145
    const-string v2, "flipsearch"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :cond_0
    iget-object v0, p0, Lflipboard/service/FLSearchManager;->f:Ljava/util/List;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/service/FLSearchManager;->f:Ljava/util/List;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/service/FLSearchManager;->f:Ljava/util/List;

    new-instance v2, Lflipboard/service/FLSearchManager$LocalSearchableItem;

    invoke-direct {v2, p0, v0}, Lflipboard/service/FLSearchManager$LocalSearchableItem;-><init>(Lflipboard/service/FLSearchManager;Lflipboard/service/Account;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    if-eqz v2, :cond_2

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lflipboard/service/FLSearchManager;->f:Ljava/util/List;

    new-instance v3, Lflipboard/service/FLSearchManager$LocalSearchableItem;

    invoke-direct {v3, p0, v0}, Lflipboard/service/FLSearchManager$LocalSearchableItem;-><init>(Lflipboard/service/FLSearchManager;Lflipboard/service/Section;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    iget-object v2, v0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lflipboard/service/FLSearchManager;->f:Ljava/util/List;

    new-instance v3, Lflipboard/service/FLSearchManager$LocalSearchableItem;

    invoke-direct {v3, p0, v0}, Lflipboard/service/FLSearchManager$LocalSearchableItem;-><init>(Lflipboard/service/FLSearchManager;Lflipboard/objs/Magazine;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 153
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 154
    iget-object v0, p0, Lflipboard/service/FLSearchManager;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLSearchManager$LocalSearchableItem;

    .line 155
    iget-object v4, v0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    if-eqz v4, :cond_6

    .line 157
    iget-object v4, v0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, v0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 161
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, v0, Lflipboard/service/FLSearchManager$LocalSearchableItem;->w:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 163
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_6

    .line 169
    :cond_7
    invoke-interface {p2, v1, v2}, Lflipboard/service/FLSearchManager$LocalSearchObserver;->a(Ljava/util/List;Ljava/util/HashSet;)V

    .line 170
    return-void
.end method

.method public final a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 128
    iput-object p1, p0, Lflipboard/service/FLSearchManager;->c:Ljava/lang/String;

    .line 129
    iput-object p2, p0, Lflipboard/service/FLSearchManager;->d:Lflipboard/service/Flap$SearchType;

    .line 130
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)Lflipboard/service/Flap$SearchRequest;

    .line 131
    return-void
.end method
