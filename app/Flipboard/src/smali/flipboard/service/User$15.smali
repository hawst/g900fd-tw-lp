.class Lflipboard/service/User$15;
.super Lflipboard/service/DatabaseHandler;
.source "User.java"


# instance fields
.field final synthetic a:Lflipboard/service/User;


# direct methods
.method constructor <init>(Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1092
    iput-object p1, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1095
    const-string v0, "SELECT * FROM userstate where uid = ?"

    new-array v1, v7, [Ljava/lang/String;

    iget-object v2, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lflipboard/service/User$15;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1096
    iget-object v0, p0, Lflipboard/service/User$15;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1097
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/service/User$15;->h:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    .line 1099
    :try_start_0
    const-string v0, "state"

    invoke-virtual {p0, v0}, Lflipboard/service/User$15;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 1100
    if-eqz v0, :cond_0

    .line 1101
    iget-object v1, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    new-instance v2, Lflipboard/json/JSONParser;

    invoke-direct {v2, v0}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->B()Lflipboard/objs/UserState;

    move-result-object v0

    invoke-static {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/User;Lflipboard/objs/UserState;)Lflipboard/objs/UserState;

    .line 1103
    :cond_0
    iget-object v0, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    const-string v1, "syncNeeded"

    invoke-virtual {p0, v1}, Lflipboard/service/User$15;->c(Ljava/lang/String;)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lflipboard/service/User;->a(Lflipboard/service/User;J)J

    .line 1104
    iget-object v0, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/service/User;->d(Lflipboard/service/User;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1105
    iget-object v0, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 1108
    :cond_1
    iget-object v0, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    const-wide/16 v2, 0x3e8

    const-string v1, "lastRefresh"

    invoke-virtual {p0, v1}, Lflipboard/service/User$15;->c(Ljava/lang/String;)I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/service/User;->b(Lflipboard/service/User;J)J

    .line 1109
    sget-object v0, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lflipboard/service/User$15;->a:Lflipboard/service/User;

    invoke-static {v3}, Lflipboard/service/User;->m(Lflipboard/service/User;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    aput-object v2, v0, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1114
    :cond_2
    :goto_0
    return-void

    .line 1110
    :catch_0
    move-exception v0

    .line 1111
    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    const-string v2, "failed to load user state: %E"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
