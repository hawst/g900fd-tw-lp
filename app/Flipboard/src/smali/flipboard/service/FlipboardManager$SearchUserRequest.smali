.class public Lflipboard/service/FlipboardManager$SearchUserRequest;
.super Lflipboard/service/Flap$Request;
.source "FlipboardManager.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic c:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 1

    .prologue
    .line 3548
    iput-object p1, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->c:Lflipboard/service/FlipboardManager;

    .line 3549
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3550
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3563
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    const-string v1, "https://jira.flipboard.com/rest/api/2/user/search?username=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 3564
    const-string v1, "Accept"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3565
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3566
    const-string v1, "Authorization"

    const-string v2, "Basic amlyYWJvdDpGIWlwcWF0M3N0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3568
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->c:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->a()Lorg/apache/http/protocol/HttpContext;

    invoke-virtual {v1, v0}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 3569
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3570
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 3571
    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3, v0, v1}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    :try_end_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 3573
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 3575
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3576
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 3577
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3597
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_2
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 3599
    :catch_0
    move-exception v0

    .line 3600
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$DNSTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3607
    :goto_1
    return-void

    .line 3579
    :cond_0
    :try_start_3
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    .line 3580
    invoke-virtual {v0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3582
    const/4 v1, 0x0

    .line 3583
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 3584
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3586
    :goto_2
    if-eqz v0, :cond_2

    .line 3587
    div-int/lit8 v1, v2, 0x64

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 3588
    iget-object v1, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3597
    :goto_3
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 3601
    :catch_1
    move-exception v0

    .line 3602
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 3590
    :cond_1
    :try_start_5
    iget-object v1, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V

    goto :goto_3

    .line 3593
    :cond_2
    iget-object v0, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from jira"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 3603
    :catch_2
    move-exception v0

    .line 3604
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 3605
    iget-object v1, p0, Lflipboard/service/FlipboardManager$SearchUserRequest;->b:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
