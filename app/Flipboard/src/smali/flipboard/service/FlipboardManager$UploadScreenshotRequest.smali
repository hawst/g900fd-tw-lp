.class Lflipboard/service/FlipboardManager$UploadScreenshotRequest;
.super Lflipboard/service/Flap$Request;
.source "FlipboardManager.java"


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/io/File;

.field private final e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lflipboard/service/FlipboardManager$CreateIssueObserver;)V
    .locals 1

    .prologue
    .line 3809
    iput-object p1, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->a:Lflipboard/service/FlipboardManager;

    .line 3810
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 3811
    iput-object p3, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->b:Ljava/lang/String;

    .line 3812
    iput-object p4, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->c:Ljava/lang/String;

    .line 3813
    iput-object p6, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    .line 3814
    iput-object p7, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    .line 3815
    iput-object p2, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->f:Ljava/lang/String;

    .line 3816
    iput-object p5, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->g:Ljava/lang/String;

    .line 3817
    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Lcom/android/gingerbread/backport/Part;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/io/RequestLogEntry;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/gingerbread/backport/Part;"
        }
    .end annotation

    .prologue
    .line 3986
    const/4 v1, 0x0

    .line 3987
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3988
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3989
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xc8

    if-le v1, v2, :cond_0

    .line 3990
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit16 v1, v1, -0xc8

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 3992
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 3993
    invoke-static {v0}, Lflipboard/json/JSONSerializer;->a(Ljava/util/List;)[B

    move-result-object v1

    .line 3994
    new-instance v0, Lcom/android/gingerbread/backport/FilePart;

    const-string v2, "file"

    new-instance v3, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$5;

    invoke-direct {v3, p0, v1, p2}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$5;-><init>(Lflipboard/service/FlipboardManager$UploadScreenshotRequest;[BLjava/lang/String;)V

    const-string v1, "text/plain"

    const-string v4, "utf-8"

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    .line 4009
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/service/FlipboardManager$UploadScreenshotRequest;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3799
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 8

    .prologue
    .line 3821
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-nez v0, :cond_1

    .line 3822
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    invoke-interface {v0}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->b()V

    .line 3983
    :cond_0
    :goto_0
    return-void

    .line 3826
    :cond_1
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    const-string v0, "https://jira.flipboard.com/rest/api/2/issue/%s/attachments"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 3827
    const-string v0, "Accept"

    const-string v2, "application/json"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3828
    const-string v0, "Authorization"

    const-string v2, "Basic amlyYWJvdDpGIWlwcWF0M3N0"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3829
    const-string v0, "X-Atlassian-Token"

    const-string v2, "nocheck"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3832
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3833
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3834
    new-instance v0, Lcom/android/gingerbread/backport/FilePart;

    const-string v3, "file"

    new-instance v4, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$1;

    invoke-direct {v4, p0}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$1;-><init>(Lflipboard/service/FlipboardManager$UploadScreenshotRequest;)V

    const-string v5, "text/plain"

    const-string v6, "utf-8"

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3851
    :cond_2
    :try_start_1
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3852
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 3853
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 3854
    new-instance v4, Lflipboard/json/JSONSerializer;

    invoke-direct {v4, v3}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    .line 3855
    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    .line 3856
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 3857
    new-instance v4, Lcom/android/gingerbread/backport/FilePart;

    const-string v5, "file"

    new-instance v6, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$2;

    invoke-direct {v6, p0, v3}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$2;-><init>(Lflipboard/service/FlipboardManager$UploadScreenshotRequest;[B)V

    const-string v3, "text/plain"

    const-string v7, "utf-8"

    invoke-direct {v4, v5, v6, v3, v7}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3872
    iget-object v3, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    iget-boolean v4, v0, Lflipboard/service/Section;->x:Z

    iget-boolean v5, v0, Lflipboard/service/Section;->i:Z

    sget-boolean v6, Lflipboard/util/DuplicateOccurrenceLog;->a:Z

    if-eqz v6, :cond_3

    invoke-virtual {v3}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    const-string v6, "Section\'s EOF State: "

    invoke-virtual {v3, v6}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v6, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v4

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    const-string v4, "Section frozen for load more: "

    invoke-virtual {v3, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 3873
    :cond_3
    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 3874
    new-instance v3, Lcom/android/gingerbread/backport/FilePart;

    const-string v4, "file"

    new-instance v5, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$3;

    invoke-direct {v5, p0, v0}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$3;-><init>(Lflipboard/service/FlipboardManager$UploadScreenshotRequest;[B)V

    const-string v0, "text/plain"

    const-string v6, "utf-8"

    invoke-direct {v3, v4, v5, v0, v6}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3893
    :cond_4
    :goto_1
    :try_start_2
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3894
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 3895
    new-instance v3, Lcom/android/gingerbread/backport/FilePart;

    const-string v4, "file"

    new-instance v5, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$4;

    invoke-direct {v5, p0, v0}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest$4;-><init>(Lflipboard/service/FlipboardManager$UploadScreenshotRequest;[B)V

    const-string v0, "text/plain"

    const-string v6, "utf-8"

    invoke-direct {v3, v4, v5, v0, v6}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3910
    :cond_5
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    const-string v3, "apiRequests.txt"

    invoke-direct {p0, v0, v3}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->a(Ljava/util/List;Ljava/lang/String;)Lcom/android/gingerbread/backport/Part;

    move-result-object v0

    .line 3911
    if-eqz v0, :cond_6

    .line 3912
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3914
    :cond_6
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->e:Ljava/util/List;

    const-string v3, "externalRequests.txt"

    invoke-direct {p0, v0, v3}, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->a(Ljava/util/List;Ljava/lang/String;)Lcom/android/gingerbread/backport/Part;

    move-result-object v0

    .line 3915
    if-eqz v0, :cond_7

    .line 3916
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3918
    :cond_7
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-eqz v0, :cond_8

    .line 3919
    new-instance v0, Lcom/android/gingerbread/backport/FilePart;

    const-string v3, "file"

    iget-object v4, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    invoke-direct {v0, v3, v4}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Ljava/io/File;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3921
    :cond_8
    new-instance v3, Lcom/android/gingerbread/backport/MultipartEntity;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/gingerbread/backport/Part;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/gingerbread/backport/Part;

    invoke-direct {v3, v0}, Lcom/android/gingerbread/backport/MultipartEntity;-><init>([Lcom/android/gingerbread/backport/Part;)V

    invoke-virtual {v1, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 3923
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v2, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->a()Lorg/apache/http/protocol/HttpContext;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 3924
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 3925
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 3926
    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3, v1, v0}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v3

    .line 3927
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3930
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3931
    :goto_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 3932
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 3968
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_4
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3970
    :catch_0
    move-exception v0

    .line 3971
    :try_start_5
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$DNSTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3972
    iget-object v1, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DNS Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$DNSTimeoutException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->d(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3979
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 3980
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 3889
    :catch_1
    move-exception v0

    .line 3890
    :try_start_6
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 3891
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    .line 3973
    :catch_2
    move-exception v0

    .line 3974
    :try_start_7
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 3979
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 3980
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 3934
    :cond_9
    :try_start_8
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    .line 3936
    invoke-virtual {v0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v0

    .line 3937
    const/4 v1, 0x0

    .line 3938
    if-eqz v0, :cond_a

    instance-of v4, v0, Ljava/util/List;

    if-eqz v4, :cond_a

    .line 3939
    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    move-object v1, v0

    .line 3944
    :cond_a
    if-eqz v1, :cond_e

    .line 3945
    div-int/lit8 v0, v2, 0x64

    const/4 v2, 0x2

    if-eq v0, v2, :cond_d

    .line 3946
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3947
    const-string v0, "errorMessages"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 3948
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 3949
    invoke-virtual {v0}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3950
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 3952
    :cond_b
    const-string v0, "errors"

    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v1

    .line 3953
    invoke-virtual {v1}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3954
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3955
    const-string v5, " : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3956
    invoke-virtual {v1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3957
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 3959
    :cond_c
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->d(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 3968
    :goto_5
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 3979
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 3980
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 3961
    :cond_d
    :try_start_a
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    invoke-interface {v0}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->b()V

    goto :goto_5

    .line 3964
    :cond_e
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    const-string v1, "Null response from Jira"

    invoke-interface {v0, v1}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->d(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_5

    .line 3975
    :catch_3
    move-exception v0

    .line 3976
    :try_start_b
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 3977
    iget-object v1, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->e:Lflipboard/service/FlipboardManager$CreateIssueObserver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/FlipboardManager$CreateIssueObserver;->d(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 3979
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 3980
    iget-object v0, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 3979
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    if-eqz v1, :cond_f

    .line 3980
    iget-object v1, p0, Lflipboard/service/FlipboardManager$UploadScreenshotRequest;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_f
    throw v0
.end method
