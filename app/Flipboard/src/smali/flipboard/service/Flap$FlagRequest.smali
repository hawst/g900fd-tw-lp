.class Lflipboard/service/Flap$FlagRequest;
.super Lflipboard/service/Flap$Request;
.source "Flap.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Lflipboard/service/Flap$JSONResultObserver;

.field final synthetic g:Lflipboard/service/Flap;


# direct methods
.method constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 4241
    iput-object p1, p0, Lflipboard/service/Flap$FlagRequest;->g:Lflipboard/service/Flap;

    .line 4242
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$Request;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 4243
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 4259
    iget-object v0, p0, Lflipboard/service/Flap$FlagRequest;->g:Lflipboard/service/Flap;

    const-string v1, "/v1/social/flagItem"

    iget-object v2, p0, Lflipboard/service/Flap$FlagRequest;->n:Lflipboard/service/User;

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "oid"

    aput-object v4, v3, v7

    iget-object v4, p0, Lflipboard/service/Flap$FlagRequest;->b:Ljava/lang/String;

    aput-object v4, v3, v8

    const-string v4, "section"

    aput-object v4, v3, v9

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$FlagRequest;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "url"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$FlagRequest;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "type"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Lflipboard/service/Flap$FlagRequest;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4260
    iget-object v1, p0, Lflipboard/service/Flap$FlagRequest;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4263
    const-string v1, "%s&commentid=%s"

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v0, v2, v7

    iget-object v0, p0, Lflipboard/service/Flap$FlagRequest;->c:Ljava/lang/String;

    aput-object v0, v2, v8

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4265
    :cond_0
    sget-object v1, Lflipboard/service/Flap;->a:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    .line 4267
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 4268
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4269
    iget-object v0, p0, Lflipboard/service/Flap$FlagRequest;->g:Lflipboard/service/Flap;

    invoke-virtual {v0, v1}, Lflipboard/service/Flap;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 4270
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 4271
    new-instance v3, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-virtual {p0, v1, v0}, Lflipboard/service/Flap$FlagRequest;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4272
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 4273
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    .line 4274
    iget-object v0, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response from flap: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4292
    iput-object v6, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    .line 4293
    :goto_0
    return-void

    .line 4277
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    .line 4278
    if-eqz v0, :cond_3

    .line 4279
    const-string v1, "success"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4280
    iget-object v1, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Lflipboard/json/FLObject;)V
    :try_end_1
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4292
    :goto_1
    iput-object v6, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 4282
    :cond_2
    :try_start_2
    iget-object v1, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    const-string v2, "errormessage"

    invoke-virtual {v0, v2}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 4287
    :catch_0
    move-exception v0

    .line 4288
    :try_start_3
    iget-object v1, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager$BaseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4292
    iput-object v6, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    .line 4285
    :cond_3
    :try_start_4
    iget-object v0, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    const-string v1, "Unexpected null response from flap"

    invoke-interface {v0, v1}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lflipboard/io/NetworkManager$BaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 4289
    :catch_1
    move-exception v0

    .line 4290
    :try_start_5
    iget-object v1, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/service/Flap$JSONResultObserver;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4292
    iput-object v6, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 0

    .prologue
    .line 4247
    iput-object p1, p0, Lflipboard/service/Flap$FlagRequest;->a:Ljava/lang/String;

    .line 4248
    iput-object p2, p0, Lflipboard/service/Flap$FlagRequest;->b:Ljava/lang/String;

    .line 4249
    iput-object p3, p0, Lflipboard/service/Flap$FlagRequest;->c:Ljava/lang/String;

    .line 4250
    iput-object p4, p0, Lflipboard/service/Flap$FlagRequest;->d:Ljava/lang/String;

    .line 4251
    iput-object p5, p0, Lflipboard/service/Flap$FlagRequest;->e:Ljava/lang/String;

    .line 4252
    iput-object p6, p0, Lflipboard/service/Flap$FlagRequest;->f:Lflipboard/service/Flap$JSONResultObserver;

    .line 4253
    invoke-super {p0}, Lflipboard/service/Flap$Request;->c()V

    .line 4254
    return-void
.end method
