.class public Lflipboard/service/ContentDrawerHandler;
.super Ljava/lang/Object;
.source "ContentDrawerHandler.java"


# static fields
.field public static final j:Lflipboard/util/Log;


# instance fields
.field a:Landroid/content/Context;

.field public b:Z

.field public c:Lflipboard/objs/ConfigFolder;

.field d:Z

.field e:Z

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lflipboard/objs/ConfigServices;

.field h:Lflipboard/objs/ConfigFolder;

.field i:Lflipboard/objs/ConfigFolder;

.field private final k:Lflipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable$Proxy",
            "<",
            "Lflipboard/service/ContentDrawerHandler;",
            "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "contentdrawerhandler"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const v5, 0x7f0d0024

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->b:Z

    .line 68
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    .line 69
    new-instance v0, Lflipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lflipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/service/ContentDrawerHandler;->k:Lflipboard/util/Observable$Proxy;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    .line 71
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    const/4 v1, 0x0

    new-instance v2, Lflipboard/objs/ContentDrawerListItemHeader;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 73
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/service/ContentDrawerHandler$1;

    invoke-direct {v1, p0}, Lflipboard/service/ContentDrawerHandler$1;-><init>(Lflipboard/service/ContentDrawerHandler;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 154
    return-void
.end method

.method static synthetic a(Lflipboard/service/ContentDrawerHandler;Lflipboard/objs/ConfigServices;)Lflipboard/objs/ConfigServices;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    return-object p1
.end method

.method static synthetic a(Lflipboard/service/ContentDrawerHandler;)Ljava/util/List;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x4

    const/4 v9, 0x3

    .line 474
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 476
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 477
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 478
    add-int/lit8 v1, v4, 0x1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    add-int/lit8 v1, v4, 0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/ContentDrawerListItem;

    move-object v3, v1

    .line 480
    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v10, :cond_1

    move-object v1, v0

    .line 481
    check-cast v1, Lflipboard/objs/ConfigSection;

    .line 483
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_1

    .line 484
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v2, v2, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v2, v11, :cond_4

    .line 486
    new-instance v7, Lflipboard/objs/DualBrick;

    invoke-direct {v7}, Lflipboard/objs/DualBrick;-><init>()V

    .line 487
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v7, v2}, Lflipboard/objs/DualBrick;->a(Lflipboard/objs/ConfigBrick;)V

    .line 489
    if-eqz v3, :cond_0

    invoke-interface {v3}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v10, :cond_0

    move-object v2, v3

    .line 490
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 491
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v8, :cond_0

    .line 492
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v8, v8, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v8, v11, :cond_0

    .line 493
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iput-object v8, v7, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    .line 494
    iget-object v8, v7, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iput-object v2, v8, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    .line 495
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 496
    add-int/lit8 v4, v4, 0x1

    .line 500
    :cond_0
    iput-object v7, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    .line 533
    :cond_1
    :goto_2
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v9, :cond_2

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 534
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/objs/ContentDrawerListItem;->a(Ljava/util/List;)V

    .line 476
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    move-object v3, v5

    .line 478
    goto :goto_1

    .line 501
    :cond_4
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v2, v2, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v2, v9, :cond_1

    .line 503
    new-instance v7, Lflipboard/objs/TripleBrick;

    invoke-direct {v7}, Lflipboard/objs/TripleBrick;-><init>()V

    .line 504
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v7, v2}, Lflipboard/objs/TripleBrick;->a(Lflipboard/objs/ConfigBrick;)V

    .line 505
    if-eqz v3, :cond_5

    invoke-interface {v3}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v10, :cond_5

    move-object v2, v3

    .line 506
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 507
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v8, :cond_5

    .line 508
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v8, v8, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v8, v9, :cond_5

    .line 509
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iput-object v8, v7, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    .line 510
    iget-object v8, v7, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iput-object v2, v8, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    .line 511
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    add-int/lit8 v4, v4, 0x1

    .line 516
    :cond_5
    add-int/lit8 v2, v4, 0x1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    add-int/lit8 v2, v4, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/ContentDrawerListItem;

    move-object v3, v2

    .line 517
    :goto_3
    if-eqz v3, :cond_6

    invoke-interface {v3}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v10, :cond_6

    move-object v2, v3

    .line 518
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 519
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v8, :cond_6

    .line 520
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v8, v8, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v8, v9, :cond_6

    .line 521
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iput-object v8, v7, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    .line 522
    iget-object v8, v7, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iput-object v2, v8, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    .line 523
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524
    add-int/lit8 v4, v4, 0x1

    .line 528
    :cond_6
    iput-object v7, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    goto/16 :goto_2

    :cond_7
    move-object v3, v5

    .line 516
    goto :goto_3

    .line 538
    :cond_8
    invoke-interface {p0, v6}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 539
    return-object p0
.end method

.method static synthetic a(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v5, 0x3

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 32
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    iget-boolean v3, v0, Lflipboard/objs/ConfigService;->X:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v7, "flipboard"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v8, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v9, v0, Lflipboard/model/ConfigSetting;->GoogleReaderDisabled:Z

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v1, v2

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v8, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v11

    sget-object v3, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v12, v3, [Ljava/lang/Object;

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v3, v12, v2

    if-eqz v11, :cond_2

    move v3, v4

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v12, v4

    if-eqz v9, :cond_3

    if-nez v11, :cond_3

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v12, "googlereader"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    if-eqz v11, :cond_5

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v12, "flipboard"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->m()Lflipboard/objs/ConfigService;

    move-result-object v3

    iget-object v12, v11, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v12}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v3, Lflipboard/objs/ConfigService;->bR:Ljava/lang/String;

    iput-boolean v4, v3, Lflipboard/objs/ConfigService;->bV:Z

    iget-object v11, v11, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v11, v11, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v3, Lflipboard/objs/ConfigService;->bQ:Ljava/lang/String;

    if-nez v1, :cond_4

    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v11, p0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    const v12, 0x7f0d035a

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v11, v13}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v4

    :cond_4
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_5
    move v0, v1

    move v1, v0

    goto :goto_1

    :cond_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    new-instance v0, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    const v3, 0x7f0d001e

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v13}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_7
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    iput-object v7, v0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    const v3, 0x7f0d0021

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->bP:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    const-string v1, "content_guide_accounts"

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->bR:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    monitor-enter v1

    move v0, v5

    :goto_3
    :try_start_0
    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_8

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lflipboard/objs/ContentDrawerListItemHeader;

    if-eqz v3, :cond_a

    move v2, v0

    :cond_8
    add-int/lit8 v0, v2, -0x1

    :goto_4
    if-lt v0, v5, :cond_b

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    if-ne v2, v3, :cond_9

    const/4 v2, 0x0

    iput-object v2, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    :cond_9
    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    const/4 v2, 0x3

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_c
    invoke-virtual {v8}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v8}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lflipboard/service/ContentDrawerHandler;->c()V

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    const/4 v2, 0x4

    iget-object v3, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->d:Z

    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lflipboard/service/ContentDrawerHandler;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lflipboard/service/ContentDrawerHandler;->e:Z

    return p1
.end method

.method static synthetic b(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigServices;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    return-object v0
.end method

.method static synthetic b(Lflipboard/service/ContentDrawerHandler;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lflipboard/service/ContentDrawerHandler;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lflipboard/service/ContentDrawerHandler;->d:Z

    return p1
.end method

.method static synthetic c(Lflipboard/service/ContentDrawerHandler;)Lflipboard/util/Observable$Proxy;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->k:Lflipboard/util/Observable$Proxy;

    return-object v0
.end method

.method static synthetic d(Lflipboard/service/ContentDrawerHandler;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->e:Z

    return v0
.end method

.method static synthetic e(Lflipboard/service/ContentDrawerHandler;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->d:Z

    return v0
.end method

.method static synthetic f(Lflipboard/service/ContentDrawerHandler;)Lflipboard/objs/ConfigFolder;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    return-object v0
.end method

.method static synthetic g(Lflipboard/service/ContentDrawerHandler;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 181
    iget-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->d:Z

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->k:Lflipboard/util/Observable$Proxy;

    sget-object v1, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v2, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 210
    :goto_0
    return-void

    .line 187
    :cond_0
    new-instance v0, Lflipboard/service/ContentDrawerHandler$2;

    invoke-direct {v0, p0}, Lflipboard/service/ContentDrawerHandler$2;-><init>(Lflipboard/service/ContentDrawerHandler;)V

    .line 208
    invoke-virtual {v0}, Lflipboard/service/ContentDrawerHandler$2;->start()V

    goto :goto_0
.end method

.method public final a(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/ContentDrawerHandler;",
            "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->k:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->c(Lflipboard/util/Observer;)V

    .line 164
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 551
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 552
    :cond_0
    const/4 v0, 0x0

    .line 589
    :goto_0
    return-object v0

    .line 555
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 556
    invoke-virtual {v0}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v0

    .line 558
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 559
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    .line 560
    new-instance v3, Lflipboard/objs/ConfigSection;

    invoke-direct {v3}, Lflipboard/objs/ConfigSection;-><init>()V

    .line 561
    invoke-virtual {v3, v0}, Lflipboard/objs/ConfigSection;->a(Lflipboard/objs/Magazine;)V

    .line 562
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 566
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->p:Ljava/util/List;

    .line 567
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 568
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d00b1

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 569
    new-instance v3, Lflipboard/service/ContentDrawerHandler$5;

    invoke-direct {v3, p0, v2}, Lflipboard/service/ContentDrawerHandler$5;-><init>(Lflipboard/service/ContentDrawerHandler;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    .line 577
    new-instance v3, Lflipboard/service/ContentDrawerHandler$6;

    invoke-direct {v3, p0}, Lflipboard/service/ContentDrawerHandler$6;-><init>(Lflipboard/service/ContentDrawerHandler;)V

    .line 584
    invoke-virtual {v3, v0}, Lflipboard/objs/ConfigSection;->a(Lflipboard/objs/Magazine;)V

    .line 585
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 589
    goto :goto_0
.end method

.method public final b(Lflipboard/util/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/ContentDrawerHandler;",
            "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->k:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->b(Lflipboard/util/Observer;)V

    .line 170
    iget-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/service/ContentDrawerHandler;->d:Z

    if-eqz v0, :cond_0

    .line 171
    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    invoke-interface {p1, p0, v0, v1}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 177
    :goto_0
    return-void

    .line 174
    :cond_0
    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    const/4 v1, 0x0

    invoke-interface {p1, p0, v0, v1}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 175
    invoke-virtual {p0}, Lflipboard/service/ContentDrawerHandler;->a()V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 623
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    if-nez v0, :cond_0

    .line 624
    new-instance v0, Lflipboard/objs/ConfigFolder;

    invoke-direct {v0}, Lflipboard/objs/ConfigFolder;-><init>()V

    iput-object v0, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    .line 625
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    iget-object v1, p0, Lflipboard/service/ContentDrawerHandler;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    .line 626
    iget-object v0, p0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    const-string v1, "content_guide_notifications"

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->bR:Ljava/lang/String;

    .line 628
    :cond_0
    return-void
.end method
