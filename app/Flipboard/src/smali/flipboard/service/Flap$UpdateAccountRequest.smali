.class public Lflipboard/service/Flap$UpdateAccountRequest;
.super Lflipboard/service/Flap$AccountRequest;
.source "Flap.java"


# instance fields
.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field final synthetic k:Lflipboard/service/Flap;


# direct methods
.method public constructor <init>(Lflipboard/service/Flap;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 1833
    iput-object p1, p0, Lflipboard/service/Flap$UpdateAccountRequest;->k:Lflipboard/service/Flap;

    .line 1834
    invoke-direct {p0, p1, p2}, Lflipboard/service/Flap$AccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    .line 1835
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1850
    iget-object v0, p0, Lflipboard/service/Flap$UpdateAccountRequest;->k:Lflipboard/service/Flap;

    const-string v1, "/v1/flipboard/updateAccountProfile"

    iget-object v2, p0, Lflipboard/service/Flap$UpdateAccountRequest;->n:Lflipboard/service/User;

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "realName"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/service/Flap$UpdateAccountRequest;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "image"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lflipboard/service/Flap$UpdateAccountRequest;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "description"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lflipboard/service/Flap$UpdateAccountRequest;->i:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "username"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Lflipboard/service/Flap$UpdateAccountRequest;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic cancel()Z
    .locals 1

    .prologue
    .line 1825
    invoke-super {p0}, Lflipboard/service/Flap$AccountRequest;->cancel()Z

    move-result v0

    return v0
.end method
