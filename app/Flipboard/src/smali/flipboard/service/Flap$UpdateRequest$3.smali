.class Lflipboard/service/Flap$UpdateRequest$3;
.super Ljava/lang/Object;
.source "Flap.java"

# interfaces
.implements Lflipboard/service/User$StateChanger;


# instance fields
.field final synthetic a:Lflipboard/objs/TOCSection;

.field final synthetic b:Lflipboard/objs/FeedItem;

.field final synthetic c:Lflipboard/service/Flap$SectionInfo;

.field final synthetic d:Lflipboard/service/Flap$UpdateRequest;


# direct methods
.method constructor <init>(Lflipboard/service/Flap$UpdateRequest;Lflipboard/objs/TOCSection;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$SectionInfo;)V
    .locals 0

    .prologue
    .line 1328
    iput-object p1, p0, Lflipboard/service/Flap$UpdateRequest$3;->d:Lflipboard/service/Flap$UpdateRequest;

    iput-object p2, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iput-object p3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iput-object p4, p0, Lflipboard/service/Flap$UpdateRequest$3;->c:Lflipboard/service/Flap$SectionInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1331
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-boolean v0, v0, Lflipboard/objs/TOCSection;->D:Z

    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v3, v3, Lflipboard/objs/FeedSection;->C:Z

    if-eq v0, v3, :cond_3

    move v0, v1

    .line 1332
    :goto_0
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v4, v4, Lflipboard/objs/FeedSection;->C:Z

    iput-boolean v4, v3, Lflipboard/objs/TOCSection;->D:Z

    .line 1334
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-boolean v3, v3, Lflipboard/objs/TOCSection;->E:Z

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v4, v4, Lflipboard/objs/FeedSection;->A:Z

    if-eq v3, v4, :cond_4

    move v3, v1

    :goto_1
    or-int/2addr v3, v0

    .line 1335
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v4, v4, Lflipboard/objs/FeedSection;->A:Z

    iput-boolean v4, v0, Lflipboard/objs/TOCSection;->E:Z

    .line 1337
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1338
    iget-object v0, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    or-int/2addr v0, v3

    .line 1340
    :goto_3
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v4, v4, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    .line 1342
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->D:Ljava/lang/String;

    invoke-static {v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1343
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->D:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->c:Lflipboard/service/Flap$SectionInfo;

    iget-object v4, v4, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->o:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v1

    :goto_4
    or-int/2addr v0, v3

    .line 1345
    :cond_0
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->c:Lflipboard/service/Flap$SectionInfo;

    iget-object v3, v3, Lflipboard/service/Flap$SectionInfo;->a:Lflipboard/service/Section;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v4, v4, Lflipboard/objs/FeedSection;->D:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/service/Section;->o:Ljava/lang/String;

    .line 1347
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->l:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1348
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->l:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v3, v1

    :goto_5
    or-int/2addr v0, v3

    .line 1350
    :cond_1
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v4, v4, Lflipboard/objs/FeedSection;->l:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    .line 1352
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->n:Lflipboard/objs/Image;

    if-eqz v3, :cond_2

    .line 1353
    iget-object v3, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->n:Lflipboard/objs/Image;

    iget-object v4, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    invoke-virtual {v3, v4}, Lflipboard/objs/Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    :goto_6
    or-int/2addr v0, v1

    .line 1355
    :cond_2
    iget-object v1, p0, Lflipboard/service/Flap$UpdateRequest$3;->a:Lflipboard/objs/TOCSection;

    iget-object v2, p0, Lflipboard/service/Flap$UpdateRequest$3;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v2, v2, Lflipboard/objs/FeedSection;->n:Lflipboard/objs/Image;

    iput-object v2, v1, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    .line 1356
    return v0

    :cond_3
    move v0, v2

    .line 1331
    goto/16 :goto_0

    :cond_4
    move v3, v2

    .line 1334
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 1338
    goto/16 :goto_2

    :cond_6
    move v3, v2

    .line 1343
    goto :goto_4

    :cond_7
    move v3, v2

    .line 1348
    goto :goto_5

    :cond_8
    move v1, v2

    .line 1353
    goto :goto_6

    :cond_9
    move v0, v3

    goto/16 :goto_3
.end method
