.class public final enum Lflipboard/service/User$Message;
.super Ljava/lang/Enum;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/service/User$Message;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/service/User$Message;

.field public static final enum b:Lflipboard/service/User$Message;

.field public static final enum c:Lflipboard/service/User$Message;

.field public static final enum d:Lflipboard/service/User$Message;

.field public static final enum e:Lflipboard/service/User$Message;

.field public static final enum f:Lflipboard/service/User$Message;

.field public static final enum g:Lflipboard/service/User$Message;

.field public static final enum h:Lflipboard/service/User$Message;

.field public static final enum i:Lflipboard/service/User$Message;

.field public static final enum j:Lflipboard/service/User$Message;

.field public static final enum k:Lflipboard/service/User$Message;

.field public static final enum l:Lflipboard/service/User$Message;

.field private static final synthetic m:[Lflipboard/service/User$Message;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "SECTION_ADDED"

    invoke-direct {v0, v1, v3}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->a:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "SECTION_REMOVED"

    invoke-direct {v0, v1, v4}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->b:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "SECTIONS_CHANGED"

    invoke-direct {v0, v1, v5}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "SYNC_STARTED"

    invoke-direct {v0, v1, v6}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->d:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "SYNC_FAILED"

    invoke-direct {v0, v1, v7}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->e:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "SYNC_SUCCEEDED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->f:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "ACCOUNT_ADDED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->g:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "ACCOUNT_REMOVED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->h:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "MAGAZINES_CHANGED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "MUTED_AUTHORS_CHANGED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->j:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "UNREAD_NOTIFICATIONS_UPDATED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->k:Lflipboard/service/User$Message;

    new-instance v0, Lflipboard/service/User$Message;

    const-string v1, "FOLLOWING_CHANGED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lflipboard/service/User$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/service/User$Message;->l:Lflipboard/service/User$Message;

    .line 96
    const/16 v0, 0xc

    new-array v0, v0, [Lflipboard/service/User$Message;

    sget-object v1, Lflipboard/service/User$Message;->a:Lflipboard/service/User$Message;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/service/User$Message;->b:Lflipboard/service/User$Message;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/User$Message;->d:Lflipboard/service/User$Message;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/service/User$Message;->e:Lflipboard/service/User$Message;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/service/User$Message;->f:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/service/User$Message;->g:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/service/User$Message;->h:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/service/User$Message;->j:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lflipboard/service/User$Message;->k:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lflipboard/service/User$Message;->l:Lflipboard/service/User$Message;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/service/User$Message;->m:[Lflipboard/service/User$Message;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/service/User$Message;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lflipboard/service/User$Message;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/service/User$Message;

    return-object v0
.end method

.method public static values()[Lflipboard/service/User$Message;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lflipboard/service/User$Message;->m:[Lflipboard/service/User$Message;

    invoke-virtual {v0}, [Lflipboard/service/User$Message;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/service/User$Message;

    return-object v0
.end method
