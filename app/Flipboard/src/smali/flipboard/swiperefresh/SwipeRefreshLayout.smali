.class public Lflipboard/swiperefresh/SwipeRefreshLayout;
.super Landroid/view/ViewGroup;
.source "SwipeRefreshLayout.java"


# static fields
.field private static final s:[I


# instance fields
.field public a:Lflipboard/swiperefresh/SwipeProgressBar;

.field public b:I

.field private c:Landroid/view/View;

.field private d:I

.field private e:Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;

.field private f:Landroid/view/MotionEvent;

.field private g:I

.field private h:Z

.field private i:I

.field private j:F

.field private k:F

.field private l:I

.field private m:F

.field private n:F

.field private o:I

.field private p:Z

.field private final q:Landroid/view/animation/DecelerateInterpolator;

.field private final r:Landroid/view/animation/AccelerateInterpolator;

.field private final t:Landroid/view/animation/Animation;

.field private u:Landroid/view/animation/Animation;

.field private final v:Landroid/view/animation/Animation$AnimationListener;

.field private final w:Landroid/view/animation/Animation$AnimationListener;

.field private final x:Ljava/lang/Runnable;

.field private final y:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101000e

    aput v2, v0, v1

    sput-object v0, Lflipboard/swiperefresh/SwipeRefreshLayout;->s:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 181
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    iput-boolean v2, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->h:Z

    .line 78
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    .line 81
    iput v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->m:F

    .line 82
    iput v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    .line 94
    new-instance v0, Lflipboard/swiperefresh/SwipeRefreshLayout$1;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout$1;-><init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    .line 110
    new-instance v0, Lflipboard/swiperefresh/SwipeRefreshLayout$2;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout$2;-><init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->u:Landroid/view/animation/Animation;

    .line 118
    new-instance v0, Lflipboard/swiperefresh/SwipeRefreshLayout$3;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout$3;-><init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->v:Landroid/view/animation/Animation$AnimationListener;

    .line 127
    new-instance v0, Lflipboard/swiperefresh/SwipeRefreshLayout$4;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout$4;-><init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->w:Landroid/view/animation/Animation$AnimationListener;

    .line 134
    new-instance v0, Lflipboard/swiperefresh/SwipeRefreshLayout$5;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout$5;-><init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->x:Ljava/lang/Runnable;

    .line 146
    new-instance v0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout$6;-><init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    .line 183
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->i:I

    .line 185
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->l:I

    .line 188
    invoke-virtual {p0, v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setWillNotDraw(Z)V

    .line 189
    new-instance v0, Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeProgressBar;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    .line 191
    const/high16 v0, 0x41800000    # 16.0f

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setProgressBarHeight(F)V

    .line 192
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->q:Landroid/view/animation/DecelerateInterpolator;

    .line 193
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->r:Landroid/view/animation/AccelerateInterpolator;

    .line 195
    sget-object v0, Lflipboard/swiperefresh/SwipeRefreshLayout;->s:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 196
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setEnabled(Z)V

    .line 197
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 198
    return-void
.end method

.method static synthetic a(Lflipboard/swiperefresh/SwipeRefreshLayout;F)F
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->m:F

    return p1
.end method

.method static synthetic a(Lflipboard/swiperefresh/SwipeRefreshLayout;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->g:I

    return v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 291
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 292
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SwipeRefreshLayout can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    .line 297
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->d:I

    .line 299
    :cond_1
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 300
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_2

    .line 301
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 303
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v2

    const/high16 v2, 0x42f00000    # 120.0f

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    .line 302
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    .line 307
    :cond_2
    return-void
.end method

.method static synthetic a(Lflipboard/swiperefresh/SwipeRefreshLayout;I)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    return-void
.end method

.method static synthetic a(Lflipboard/swiperefresh/SwipeRefreshLayout;ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 60
    iput p1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->g:I

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    iget v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->l:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->q:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic b(Lflipboard/swiperefresh/SwipeRefreshLayout;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->d:I

    return v0
.end method

.method static synthetic c(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lflipboard/swiperefresh/SwipeRefreshLayout;)F
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->m:F

    return v0
.end method

.method static synthetic e(Lflipboard/swiperefresh/SwipeRefreshLayout;)Lflipboard/swiperefresh/SwipeProgressBar;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    return-object v0
.end method

.method static synthetic f(Lflipboard/swiperefresh/SwipeRefreshLayout;)I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->o:I

    return v0
.end method

.method static synthetic g(Lflipboard/swiperefresh/SwipeRefreshLayout;)F
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    return v0
.end method

.method static synthetic h(Lflipboard/swiperefresh/SwipeRefreshLayout;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->p:Z

    return v0
.end method

.method static synthetic i(Lflipboard/swiperefresh/SwipeRefreshLayout;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->o:I

    return v0
.end method

.method static synthetic j(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation$AnimationListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->v:Landroid/view/animation/Animation$AnimationListener;

    return-object v0
.end method

.method static synthetic k(Lflipboard/swiperefresh/SwipeRefreshLayout;)F
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    return v0
.end method

.method static synthetic l(Lflipboard/swiperefresh/SwipeRefreshLayout;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->l:I

    return v0
.end method

.method static synthetic m(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->u:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic n(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation$AnimationListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->w:Landroid/view/animation/Animation$AnimationListener;

    return-object v0
.end method

.method static synthetic o(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/DecelerateInterpolator;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->q:Landroid/view/animation/DecelerateInterpolator;

    return-object v0
.end method

.method private setTargetOffsetTopAndBottom(I)V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 455
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->o:I

    .line 456
    return-void
.end method

.method private setTriggerPercentage(F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 232
    cmpl-float v0, p1, v1

    if-nez v0, :cond_0

    .line 235
    iput v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    .line 240
    :goto_0
    return-void

    .line 238
    :cond_0
    iput p1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    .line 239
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0, p1}, Lflipboard/swiperefresh/SwipeProgressBar;->a(F)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)V
    .locals 5

    .prologue
    .line 272
    invoke-direct {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a()V

    .line 273
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 274
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 275
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 276
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 277
    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 278
    iget-object v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v4, v1, v2, v3, v0}, Lflipboard/swiperefresh/SwipeProgressBar;->a(IIII)V

    .line 279
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 311
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 312
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0, p1}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;)V

    .line 313
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 203
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 204
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->x:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 205
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 210
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->x:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 211
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 212
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 369
    invoke-direct {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a()V

    .line 371
    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iput-boolean v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->p:Z

    .line 374
    :cond_0
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->p:Z

    if-nez v0, :cond_7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v0, v3, :cond_6

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/AbsListView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v3

    if-gtz v3, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getPaddingTop()I

    move-result v0

    if-ge v3, v0, :cond_3

    :cond_1
    move v0, v2

    :goto_0
    if-nez v0, :cond_7

    .line 375
    invoke-virtual {p0, p1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 377
    :goto_1
    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 374
    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    if-lez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    const/4 v2, -0x1

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 317
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v0

    .line 318
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v1

    .line 319
    iget-object v2, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    iget v3, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->b:I

    invoke-virtual {v2, v0, v3}, Lflipboard/swiperefresh/SwipeProgressBar;->a(II)V

    .line 320
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 329
    :goto_0
    return-void

    .line 323
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 324
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v3

    .line 325
    iget v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->o:I

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    .line 326
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingRight()I

    move-result v5

    sub-int/2addr v0, v5

    .line 327
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingTop()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v1, v5

    .line 328
    add-int/2addr v0, v3

    add-int/2addr v1, v4

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 333
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 334
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SwipeRefreshLayout can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_0
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 338
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 340
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 339
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 343
    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 342
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 338
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 346
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 382
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 384
    packed-switch v0, :pswitch_data_0

    .line 433
    :cond_0
    :goto_0
    return v1

    .line 386
    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    .line 387
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    .line 388
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->k:F

    goto :goto_0

    .line 391
    :pswitch_1
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->p:Z

    if-nez v0, :cond_0

    .line 392
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 393
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    sub-float v0, v3, v0

    .line 394
    iget v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->i:I

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-lez v4, :cond_0

    .line 396
    iget v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    .line 398
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->x:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    invoke-virtual {p0, v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->e:Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;

    invoke-interface {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;->a()V

    move v1, v2

    .line 400
    goto :goto_0

    .line 403
    :cond_1
    iget-object v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->r:Landroid/view/animation/AccelerateInterpolator;

    iget v5, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    div-float v5, v0, v5

    .line 404
    invoke-virtual {v4, v5}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v4

    .line 403
    invoke-direct {p0, v4}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setTriggerPercentage(F)V

    .line 407
    iget v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->k:F

    cmpl-float v4, v4, v3

    if-lez v4, :cond_2

    .line 408
    iget v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->i:I

    int-to-float v4, v4

    sub-float/2addr v0, v4

    .line 410
    :cond_2
    float-to-int v0, v0

    iget-object v4, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v5, v0

    iget v6, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->j:F

    float-to-int v0, v0

    :cond_3
    :goto_1
    sub-int/2addr v0, v4

    invoke-direct {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    .line 411
    iget v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->k:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->i:I

    if-ge v0, v1, :cond_5

    .line 415
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 419
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->k:F

    move v1, v2

    .line 420
    goto/16 :goto_0

    .line 410
    :cond_4
    if-gez v0, :cond_3

    move v0, v1

    goto :goto_1

    .line 417
    :cond_5
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->y:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {p0, v0, v4, v5}, Lflipboard/swiperefresh/SwipeRefreshLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 427
    :pswitch_2
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->f:Landroid/view/MotionEvent;

    goto/16 :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setOnRefreshListener(Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->e:Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;

    .line 229
    return-void
.end method

.method public setProgressBarHeight(F)V
    .locals 1

    .prologue
    .line 490
    float-to-int v0, p1

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->b:I

    .line 491
    return-void
.end method

.method public setRefreshing(Z)V
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->h:Z

    if-eq v0, p1, :cond_0

    .line 250
    invoke-direct {p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a()V

    .line 251
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->n:F

    .line 252
    iput-boolean p1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->h:Z

    .line 253
    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->h:Z

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0}, Lflipboard/swiperefresh/SwipeProgressBar;->a()V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0}, Lflipboard/swiperefresh/SwipeProgressBar;->b()V

    goto :goto_0
.end method
