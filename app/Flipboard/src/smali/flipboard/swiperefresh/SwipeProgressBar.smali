.class public final Lflipboard/swiperefresh/SwipeProgressBar;
.super Ljava/lang/Object;
.source "SwipeProgressBar.java"


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/RectF;

.field private d:F

.field private e:J

.field private f:J

.field private g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Landroid/view/View;

.field private m:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lflipboard/swiperefresh/BakedBezierInterpolator;->a()Lflipboard/swiperefresh/BakedBezierInterpolator;

    move-result-object v0

    sput-object v0, Lflipboard/swiperefresh/SwipeProgressBar;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->b:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->c:Landroid/graphics/RectF;

    .line 68
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    .line 71
    iput-object p1, p0, Lflipboard/swiperefresh/SwipeProgressBar;->l:Landroid/view/View;

    .line 72
    const/high16 v0, -0x4d000000

    iput v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    .line 73
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->i:I

    .line 74
    const/high16 v0, 0x4d000000    # 1.34217728E8f

    iput v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->j:I

    .line 75
    const/high16 v0, 0x1a000000

    iput v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->k:I

    .line 76
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFIF)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 258
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 259
    sget-object v0, Lflipboard/swiperefresh/SwipeProgressBar;->a:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p5}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 260
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 261
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v1, p2, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 262
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 263
    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->b:Landroid/graphics/Paint;

    iget v1, p0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 243
    int-to-float v0, p2

    int-to-float v1, p3

    int-to-float v2, p2

    iget v3, p0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lflipboard/swiperefresh/SwipeProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 244
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->g:Z

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    .line 112
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->e:J

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->g:Z

    .line 114
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 116
    :cond_0
    return-void
.end method

.method final a(F)V
    .locals 2

    .prologue
    .line 101
    iput p1, p0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->e:J

    .line 103
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->l:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;)V

    .line 104
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 270
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 271
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->right:I

    .line 272
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    iput p2, v0, Landroid/graphics/Rect;->bottom:I

    .line 273
    return-void
.end method

.method public final a(IIII)V
    .locals 0

    .prologue
    .line 89
    iput p1, p0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    .line 90
    iput p2, p0, Lflipboard/swiperefresh/SwipeProgressBar;->i:I

    .line 91
    iput p3, p0, Lflipboard/swiperefresh/SwipeProgressBar;->j:I

    .line 92
    iput p4, p0, Lflipboard/swiperefresh/SwipeProgressBar;->k:I

    .line 93
    return-void
.end method

.method final a(Landroid/graphics/Canvas;)V
    .locals 18

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 139
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 140
    div-int/lit8 v10, v2, 0x2

    .line 141
    div-int/lit8 v11, v3, 0x2

    .line 142
    const/4 v2, 0x0

    .line 143
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v8

    .line 144
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 146
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/swiperefresh/SwipeProgressBar;->g:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lflipboard/swiperefresh/SwipeProgressBar;->f:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_c

    .line 147
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 148
    move-object/from16 v0, p0

    iget-wide v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->e:J

    sub-long v6, v4, v6

    const-wide/16 v12, 0x7d0

    rem-long/2addr v6, v12

    .line 149
    move-object/from16 v0, p0

    iget-wide v12, v0, Lflipboard/swiperefresh/SwipeProgressBar;->e:J

    sub-long v12, v4, v12

    const-wide/16 v14, 0x7d0

    div-long/2addr v12, v14

    .line 150
    long-to-float v6, v6

    const/high16 v7, 0x41a00000    # 20.0f

    div-float v14, v6, v7

    .line 154
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->g:Z

    if-nez v6, :cond_e

    .line 157
    move-object/from16 v0, p0

    iget-wide v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->f:J

    sub-long v6, v4, v6

    const-wide/16 v16, 0x3e8

    cmp-long v2, v6, v16

    if-ltz v2, :cond_1

    .line 158
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->f:J

    .line 239
    :goto_0
    return-void

    .line 165
    :cond_1
    move-object/from16 v0, p0

    iget-wide v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->f:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    rem-long/2addr v4, v6

    .line 166
    long-to-float v2, v4

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v2, v4

    .line 167
    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v2, v4

    .line 169
    int-to-float v4, v10

    sget-object v5, Lflipboard/swiperefresh/SwipeProgressBar;->a:Landroid/view/animation/Interpolator;

    invoke-interface {v5, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v2, v4

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/swiperefresh/SwipeProgressBar;->c:Landroid/graphics/RectF;

    int-to-float v5, v10

    sub-float/2addr v5, v2

    const/4 v6, 0x0

    int-to-float v7, v10

    add-float/2addr v2, v7

    int-to-float v3, v3

    invoke-virtual {v4, v5, v6, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->c:Landroid/graphics/RectF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    .line 176
    const/4 v2, 0x1

    move v9, v2

    .line 180
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-nez v2, :cond_8

    .line 181
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 200
    :goto_2
    const/4 v2, 0x0

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_2

    const/high16 v2, 0x41c80000    # 25.0f

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_2

    .line 201
    const/high16 v2, 0x41c80000    # 25.0f

    add-float/2addr v2, v14

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v7, v2, v3

    .line 202
    int-to-float v4, v10

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 204
    :cond_2
    const/4 v2, 0x0

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_3

    const/high16 v2, 0x42480000    # 50.0f

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_3

    .line 205
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v14

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v7, v2, v3

    .line 206
    int-to-float v4, v10

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->i:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 208
    :cond_3
    const/high16 v2, 0x41c80000    # 25.0f

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_4

    const/high16 v2, 0x42960000    # 75.0f

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_4

    .line 209
    const/high16 v2, 0x41c80000    # 25.0f

    sub-float v2, v14, v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v7, v2, v3

    .line 210
    int-to-float v4, v10

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->j:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 212
    :cond_4
    const/high16 v2, 0x42480000    # 50.0f

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_5

    const/high16 v2, 0x42c80000    # 100.0f

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_5

    .line 213
    const/high16 v2, 0x42480000    # 50.0f

    sub-float v2, v14, v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v7, v2, v3

    .line 214
    int-to-float v4, v10

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->k:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 216
    :cond_5
    const/high16 v2, 0x42960000    # 75.0f

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_6

    const/high16 v2, 0x42c80000    # 100.0f

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_6

    .line 217
    const/high16 v2, 0x42960000    # 75.0f

    sub-float v2, v14, v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v7, v2, v3

    .line 218
    int-to-float v4, v10

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 220
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_d

    if-eqz v9, :cond_d

    .line 225
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 226
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 227
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/swiperefresh/SwipeProgressBar;->m:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 228
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10, v11}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;II)V

    .line 231
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/swiperefresh/SwipeProgressBar;->l:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;)V

    move v8, v2

    .line 238
    :cond_7
    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    .line 183
    :cond_8
    const/4 v2, 0x0

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_9

    const/high16 v2, 0x41c80000    # 25.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_9

    .line 184
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->k:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_2

    .line 185
    :cond_9
    const/high16 v2, 0x41c80000    # 25.0f

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_a

    const/high16 v2, 0x42480000    # 50.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_a

    .line 186
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->h:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_2

    .line 187
    :cond_a
    const/high16 v2, 0x42480000    # 50.0f

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_b

    const/high16 v2, 0x42960000    # 75.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_b

    .line 188
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->i:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_2

    .line 190
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->j:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_2

    .line 234
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_7

    .line 235
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10, v11}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;II)V

    goto :goto_4

    :cond_d
    move v2, v8

    goto :goto_3

    :cond_e
    move v9, v2

    goto/16 :goto_1
.end method

.method final b()V
    .locals 2

    .prologue
    .line 122
    iget-boolean v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->g:Z

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->d:F

    .line 124
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->f:J

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->g:Z

    .line 126
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeProgressBar;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 128
    :cond_0
    return-void
.end method
