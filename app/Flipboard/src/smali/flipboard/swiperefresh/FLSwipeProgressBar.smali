.class public Lflipboard/swiperefresh/FLSwipeProgressBar;
.super Landroid/view/View;
.source "FLSwipeProgressBar.java"


# static fields
.field private static final d:[I


# instance fields
.field public a:Lflipboard/swiperefresh/SwipeProgressBar;

.field private b:Z

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101000e

    aput v2, v0, v1

    sput-object v0, Lflipboard/swiperefresh/FLSwipeProgressBar;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/swiperefresh/FLSwipeProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-boolean v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->b:Z

    .line 39
    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/FLSwipeProgressBar;->setWillNotDraw(Z)V

    .line 40
    new-instance v0, Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-direct {v0, p0}, Lflipboard/swiperefresh/SwipeProgressBar;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    .line 42
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0, p1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    .line 43
    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/FLSwipeProgressBar;->setProgressBarHeight(I)V

    .line 45
    iget v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->c:I

    invoke-virtual {p0, v0}, Lflipboard/swiperefresh/FLSwipeProgressBar;->setMinimumHeight(I)V

    .line 46
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 80
    iget-object v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0, p1}, Lflipboard/swiperefresh/SwipeProgressBar;->a(Landroid/graphics/Canvas;)V

    .line 81
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lflipboard/swiperefresh/FLSwipeProgressBar;->getMeasuredWidth()I

    move-result v0

    .line 86
    iget-object v1, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    iget v2, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->c:I

    invoke-virtual {v1, v0, v2}, Lflipboard/swiperefresh/SwipeProgressBar;->a(II)V

    .line 87
    return-void
.end method

.method public setProgressBarHeight(I)V
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->c:I

    .line 91
    return-void
.end method

.method public setRefreshing(Z)V
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->b:Z

    if-eq v0, p1, :cond_0

    .line 54
    iput-boolean p1, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->b:Z

    .line 55
    iget-boolean v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->b:Z

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0}, Lflipboard/swiperefresh/SwipeProgressBar;->a()V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lflipboard/swiperefresh/FLSwipeProgressBar;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v0}, Lflipboard/swiperefresh/SwipeProgressBar;->b()V

    goto :goto_0
.end method
