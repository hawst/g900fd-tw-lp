.class Lflipboard/swiperefresh/SwipeRefreshLayout$6;
.super Ljava/lang/Object;
.source "SwipeRefreshLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/swiperefresh/SwipeRefreshLayout;


# direct methods
.method constructor <init>(Lflipboard/swiperefresh/SwipeRefreshLayout;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 150
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->h(Lflipboard/swiperefresh/SwipeRefreshLayout;)Z

    .line 153
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->e(Lflipboard/swiperefresh/SwipeRefreshLayout;)Lflipboard/swiperefresh/SwipeProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->k(Lflipboard/swiperefresh/SwipeRefreshLayout;)F

    move-result v1

    invoke-static {v0, v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a(Lflipboard/swiperefresh/SwipeRefreshLayout;F)F

    .line 155
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->m(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->l(Lflipboard/swiperefresh/SwipeRefreshLayout;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 156
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->m(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->n(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 157
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->m(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 158
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->m(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->o(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/DecelerateInterpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 159
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->m(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    iget-object v1, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-static {v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->i(Lflipboard/swiperefresh/SwipeRefreshLayout;)I

    move-result v1

    iget-object v2, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-virtual {v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/swiperefresh/SwipeRefreshLayout$6;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    .line 162
    invoke-static {v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->j(Lflipboard/swiperefresh/SwipeRefreshLayout;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v2

    .line 161
    invoke-static {v0, v1, v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a(Lflipboard/swiperefresh/SwipeRefreshLayout;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 163
    return-void
.end method
