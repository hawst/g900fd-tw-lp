.class public Lflipboard/settings/Facebook;
.super Lflipboard/settings/Settings;
.source "Facebook.java"


# static fields
.field public static final STRINGS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static implicit_share_facebook:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/settings/Facebook;->implicit_share_facebook:Z

    .line 19
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 21
    sput-object v0, Lflipboard/settings/Facebook;->STRINGS:Ljava/util/Map;

    const-string v1, "facebook_implicit_share_facebook"

    const v2, 0x7f0d00e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-class v0, Lflipboard/settings/Facebook;

    invoke-static {v0}, Lflipboard/settings/Facebook;->init(Ljava/lang/Class;)V

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lflipboard/settings/Settings;-><init>()V

    return-void
.end method
