.class public Lflipboard/persist/DiskPersister;
.super Ljava/lang/Object;
.source "DiskPersister.java"

# interfaces
.implements Lflipboard/persist/Persister;


# instance fields
.field private final a:Lflipboard/persist/Serializer;

.field private final b:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Lflipboard/persist/Serializer;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lflipboard/persist/DiskPersister;->b:Ljava/io/File;

    .line 16
    iput-object p2, p0, Lflipboard/persist/DiskPersister;->a:Lflipboard/persist/Serializer;

    .line 17
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/persist/DiskPersister;->a:Lflipboard/persist/Serializer;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lflipboard/persist/DiskPersister;->b:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, Lflipboard/persist/Serializer;->a(Ljava/io/File;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lflipboard/persist/DiskPersister;->b:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 37
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lflipboard/persist/DiskPersister;->a:Lflipboard/persist/Serializer;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lflipboard/persist/DiskPersister;->b:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, Lflipboard/persist/Serializer;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 22
    return-void
.end method
