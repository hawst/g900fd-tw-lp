.class Lflipboard/remoteservice/MessengerService$IncomingHandler$2;
.super Ljava/lang/Object;
.source "MessengerService.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Landroid/os/Messenger;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/util/Locale;

.field final synthetic e:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lflipboard/remoteservice/MessengerService$IncomingHandler;


# direct methods
.method constructor <init>(Lflipboard/remoteservice/MessengerService$IncomingHandler;Landroid/os/Messenger;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Locale;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->g:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iput-object p2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a:Landroid/os/Messenger;

    iput-object p3, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->b:Landroid/os/Bundle;

    iput-object p4, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->d:Ljava/util/Locale;

    iput-object p6, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->e:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    iput-object p7, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 138
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 141
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->g:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->d:Ljava/util/HashMap;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 142
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 143
    if-eqz v0, :cond_0

    .line 144
    const-string v1, "apic"

    const-string v3, "AppPackageName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v1, "apiv"

    const-string v3, "LibraryVersion"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "apit"

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->b:Landroid/os/Bundle;

    const-string v3, "token"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 149
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->c:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->d:Ljava/util/Locale;

    iget-object v3, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->e:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    invoke-static {v0, v1, v3}, Lflipboard/remoteservice/RemoteServiceUtil;->a(Ljava/lang/String;Ljava/util/Locale;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;)Ljava/lang/String;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_7

    .line 151
    iget-object v8, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->g:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v9, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->f:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->b:Landroid/os/Bundle;

    const-string v3, "pageKey"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a:Landroid/os/Messenger;

    if-nez v1, :cond_1

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Invalid section id for: %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v9, v2, v5

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v8, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    sget-object v1, Lflipboard/remoteservice/MessengerService$ErrorMessage;->a:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    invoke-virtual {v1}, Lflipboard/remoteservice/MessengerService$ErrorMessage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v9, v5, v11, v1}, Lflipboard/remoteservice/MessengerService;->a(Lflipboard/remoteservice/MessengerService;Ljava/lang/String;ZLandroid/os/Messenger;Ljava/lang/String;)V

    .line 159
    :goto_0
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->g:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->c:Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0, p0}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 160
    return-void

    .line 151
    :cond_1
    invoke-static {v1}, Lflipboard/remoteservice/RemoteServiceUtil;->a(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Adding section to Flipboard: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lflipboard/service/Section;

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iput-boolean v6, v1, Lflipboard/objs/TOCSection;->H:Z

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0, v5, v6, v2}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    :cond_2
    new-instance v1, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    invoke-direct {v1, v8, v11, v9}, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;-><init>(Lflipboard/remoteservice/MessengerService$IncomingHandler;Landroid/os/Messenger;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v3, "get"

    invoke-direct {v1, v3}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "what"

    const-string v4, "section"

    invoke-virtual {v1, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "sectionIdentifier"

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz v10, :cond_3

    move v5, v6

    :cond_3
    const-string v3, "isLoadMore"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "apic"

    invoke-virtual {v7, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    const-string v4, "externalApp"

    invoke-virtual {v1, v4, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_4
    const-string v4, "apiv"

    invoke-virtual {v7, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_5

    const-string v3, "libraryVersion"

    invoke-virtual {v1, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_5
    const-string v3, "sectionType"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    if-eqz v5, :cond_6

    invoke-virtual {v0, v6, v10, v7}, Lflipboard/service/Section;->a(ZLjava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v0, v6, v2, v6, v7}, Lflipboard/service/Section;->a(ZLflipboard/util/Callback;ZLandroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 153
    :cond_7
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->g:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->f:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a:Landroid/os/Messenger;

    sget-object v3, Lflipboard/remoteservice/MessengerService$ErrorMessage;->b:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    invoke-virtual {v3}, Lflipboard/remoteservice/MessengerService$ErrorMessage;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v5, v2, v3}, Lflipboard/remoteservice/MessengerService;->a(Lflipboard/remoteservice/MessengerService;Ljava/lang/String;ZLandroid/os/Messenger;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 156
    :cond_8
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->g:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->f:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a:Landroid/os/Messenger;

    sget-object v3, Lflipboard/remoteservice/MessengerService$ErrorMessage;->b:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    invoke-virtual {v3}, Lflipboard/remoteservice/MessengerService$ErrorMessage;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v5, v2, v3}, Lflipboard/remoteservice/MessengerService;->a(Lflipboard/remoteservice/MessengerService;Ljava/lang/String;ZLandroid/os/Messenger;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 126
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    const-string v1, "feeds file failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    invoke-direct {p0}, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a()V

    .line 128
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    .line 121
    invoke-direct {p0}, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a()V

    .line 122
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 132
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    const-string v1, "feeds file maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    invoke-direct {p0}, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;->a()V

    .line 134
    return-void
.end method
