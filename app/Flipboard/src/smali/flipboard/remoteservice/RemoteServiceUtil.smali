.class public Lflipboard/remoteservice/RemoteServiceUtil;
.super Ljava/lang/Object;
.source "RemoteServiceUtil.java"


# static fields
.field public static a:Lflipboard/util/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "library server"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/remoteservice/RemoteServiceUtil;->a:Lflipboard/util/Log;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lflipboard/service/Section;
    .locals 3

    .prologue
    .line 65
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 66
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Locale;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 35
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 36
    :cond_0
    sget-object v1, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v2, "Get section id null. Either categoryId is null/empty or locale is none"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    :goto_0
    return-object v0

    .line 39
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "externalLibraryFeeds.json"

    invoke-virtual {v1, v2, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    .line 40
    iget-boolean v2, v1, Lflipboard/service/RemoteWatchedFile;->e:Z

    if-nez v2, :cond_2

    .line 41
    sget-object v1, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v2, "Get section id null. WatchFile not ready"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :cond_2
    :try_start_0
    invoke-virtual {v1}, Lflipboard/service/RemoteWatchedFile;->c()[B

    move-result-object v1

    .line 48
    if-eqz v1, :cond_4

    .line 49
    new-instance v2, Lflipboard/json/JSONParser;

    invoke-direct {v2, v1}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v2

    .line 50
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 51
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    const-string v1, "unknown"

    :cond_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 52
    invoke-virtual {v2, v1}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    sget-object v2, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v3, "Found section for API lib [Key: %s] [result: %s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v1

    .line 58
    sget-object v2, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v3, "Exception finding section %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 55
    :cond_4
    :try_start_1
    sget-object v1, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v2, "Feeds file was supposed to be ready, but it had no data"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
