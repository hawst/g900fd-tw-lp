.class Lflipboard/remoteservice/MessengerService$IncomingHandler$1;
.super Ljava/lang/Object;
.source "MessengerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Messenger;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflipboard/remoteservice/MessengerService$IncomingHandler;


# direct methods
.method constructor <init>(Lflipboard/remoteservice/MessengerService$IncomingHandler;Landroid/os/Messenger;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->d:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iput-object p2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->a:Landroid/os/Messenger;

    iput p3, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->b:I

    iput-object p4, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 109
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->d:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    const-string v1, "Timeout of %d seconds has occured while fetching items for library, replying error"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->d:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->c:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;->a:Landroid/os/Messenger;

    sget-object v3, Lflipboard/remoteservice/MessengerService$ErrorMessage;->c:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    invoke-virtual {v3}, Lflipboard/remoteservice/MessengerService$ErrorMessage;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v4, v2, v3}, Lflipboard/remoteservice/MessengerService;->a(Lflipboard/remoteservice/MessengerService;Ljava/lang/String;ZLandroid/os/Messenger;Ljava/lang/String;)V

    .line 113
    :cond_0
    return-void
.end method
