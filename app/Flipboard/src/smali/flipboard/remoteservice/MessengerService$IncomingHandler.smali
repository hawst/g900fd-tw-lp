.class Lflipboard/remoteservice/MessengerService$IncomingHandler;
.super Landroid/os/Handler;
.source "MessengerService.java"


# instance fields
.field final synthetic a:Lflipboard/remoteservice/MessengerService;


# direct methods
.method constructor <init>(Lflipboard/remoteservice/MessengerService;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14

    .prologue
    .line 75
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 167
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 78
    :pswitch_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->g()V

    .line 79
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    .line 80
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->d:Ljava/util/HashMap;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 84
    :pswitch_1
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->d:Ljava/util/HashMap;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v0, v0, Lflipboard/service/FlipboardManager;->aB:I

    if-nez v0, :cond_0

    .line 88
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->h()V

    .line 89
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 93
    :pswitch_2
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v8, v0

    :goto_1
    if-ltz v8, :cond_0

    .line 94
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 95
    sget-object v0, Lflipboard/remoteservice/MessengerService;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "token"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "categoryId"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "pageKey"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "contentType"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "country"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 96
    const-string v0, "contentType"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->valueOf(Ljava/lang/String;)Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    move-result-object v6

    .line 97
    new-instance v5, Ljava/util/Locale;

    const-string v0, "language"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v0, "categoryId"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "contentType"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 101
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Messenger;

    .line 104
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v0, v0, Lflipboard/model/ConfigSetting;->FeedFetchLibraryTimeoutInterval:I

    .line 105
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    int-to-long v10, v0

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    new-instance v9, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;

    invoke-direct {v9, p0, v2, v0, v7}, Lflipboard/remoteservice/MessengerService$IncomingHandler$1;-><init>(Lflipboard/remoteservice/MessengerService$IncomingHandler;Landroid/os/Messenger;ILjava/lang/String;)V

    invoke-virtual {v1, v10, v11, v9}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 116
    sget-object v9, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v10, "externalLibraryFeeds.json"

    new-instance v0, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lflipboard/remoteservice/MessengerService$IncomingHandler$2;-><init>(Lflipboard/remoteservice/MessengerService$IncomingHandler;Landroid/os/Messenger;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Locale;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;Ljava/lang/String;)V

    invoke-virtual {v9, v10, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    .line 162
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/RemoteWatchedFile;->g:Z

    .line 163
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/RemoteWatchedFile;->c(Z)V

    .line 93
    add-int/lit8 v0, v8, -0x1

    move v8, v0

    goto/16 :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
