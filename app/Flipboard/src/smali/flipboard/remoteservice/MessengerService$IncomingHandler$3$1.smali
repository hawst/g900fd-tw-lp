.class Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;
.super Ljava/lang/Object;
.source "MessengerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/Section$Message;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;


# direct methods
.method constructor <init>(Lflipboard/remoteservice/MessengerService$IncomingHandler$3;Lflipboard/service/Section$Message;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iput-object p2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->a:Lflipboard/service/Section$Message;

    iput-object p3, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->b:Lflipboard/service/Section;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 201
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v1, v1, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->a:Lflipboard/service/Section$Message;

    sget-object v1, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne v0, v1, :cond_1

    .line 204
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->b:Lflipboard/service/Section;

    invoke-virtual {v0, v4}, Lflipboard/service/Section;->a(Ljava/lang/Runnable;)V

    .line 205
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->b:Lflipboard/service/Section;

    invoke-virtual {v0, v3}, Lflipboard/service/Section;->e(Z)V

    .line 206
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->b:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->u()V

    .line 207
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v1, v1, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->b:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v2, v2, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->a:Landroid/os/Messenger;

    invoke-static {v0, v1, v3, v2, v4}, Lflipboard/remoteservice/MessengerService;->a(Lflipboard/remoteservice/MessengerService;Ljava/lang/String;ZLandroid/os/Messenger;Ljava/lang/String;)V

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->a:Lflipboard/service/Section$Message;

    sget-object v1, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->a:Lflipboard/service/Section$Message;

    sget-object v1, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    if-ne v0, v1, :cond_0

    .line 209
    :cond_2
    iget-object v0, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler;

    iget-object v0, v0, Lflipboard/remoteservice/MessengerService$IncomingHandler;->a:Lflipboard/remoteservice/MessengerService;

    iget-object v1, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v1, v1, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->b:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->c:Lflipboard/remoteservice/MessengerService$IncomingHandler$3;

    iget-object v3, v3, Lflipboard/remoteservice/MessengerService$IncomingHandler$3;->a:Landroid/os/Messenger;

    iget-object v4, p0, Lflipboard/remoteservice/MessengerService$IncomingHandler$3$1;->a:Lflipboard/service/Section$Message;

    invoke-virtual {v4}, Lflipboard/service/Section$Message;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lflipboard/remoteservice/MessengerService;->a(Lflipboard/remoteservice/MessengerService;Ljava/lang/String;ZLandroid/os/Messenger;Ljava/lang/String;)V

    goto :goto_0
.end method
