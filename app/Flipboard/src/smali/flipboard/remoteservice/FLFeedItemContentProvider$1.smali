.class Lflipboard/remoteservice/FLFeedItemContentProvider$1;
.super Lflipboard/service/DatabaseHandler;
.source "FLFeedItemContentProvider.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

.field final synthetic d:Landroid/database/MatrixCursor;

.field final synthetic e:Lflipboard/remoteservice/FLFeedItemContentProvider;


# direct methods
.method constructor <init>(Lflipboard/remoteservice/FLFeedItemContentProvider;Ljava/lang/String;Ljava/lang/String;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;Landroid/database/MatrixCursor;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->e:Lflipboard/remoteservice/FLFeedItemContentProvider;

    iput-object p2, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->c:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    iput-object p5, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->d:Landroid/database/MatrixCursor;

    invoke-direct {p0}, Lflipboard/service/DatabaseHandler;-><init>()V

    return-void
.end method

.method private a(Landroid/database/MatrixCursor;Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    .line 200
    iget-object v0, p2, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    .line 201
    :goto_0
    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    const/4 v0, 0x3

    iget-object v2, p2, Lflipboard/objs/FeedItem;->au:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-object v2, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->c:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    invoke-virtual {v2}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    iget-object v2, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x6

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->af()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x7

    iget-wide v2, p2, Lflipboard/objs/FeedItem;->Z:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 202
    return-void

    .line 200
    :cond_0
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 147
    :try_start_0
    iget-object v0, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->a:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->h:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    .line 150
    iget-object v0, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 151
    :goto_0
    iget-object v0, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 152
    const-string v0, "descriptor"

    invoke-virtual {p0, v0}, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 153
    if-eqz v0, :cond_8

    .line 154
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-direct {v1, v0}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->w()Lflipboard/objs/TOCSection;

    move-result-object v0

    .line 155
    iget-object v1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 156
    const-string v0, "items"

    invoke-virtual {p0, v0}, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 157
    if-eqz v0, :cond_3

    .line 158
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-direct {v1, v0}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v0

    .line 159
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 160
    iget-object v1, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->c:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    iget-object v3, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->d:Landroid/database/MatrixCursor;

    sget-object v4, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->a:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    if-ne v1, v4, :cond_5

    iget-object v4, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v5, "post"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v1, v0, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_1
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    :cond_2
    invoke-direct {p0, v3, v0}, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->a(Landroid/database/MatrixCursor;Lflipboard/objs/FeedItem;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 171
    :catch_0
    move-exception v0

    .line 172
    sget-object v1, Lflipboard/remoteservice/FLFeedItemContentProvider;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 176
    :cond_3
    return-void

    .line 160
    :cond_4
    :try_start_1
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->a:Lflipboard/util/Log;

    const-string v1, "Text item without text, skipping"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    sget-object v4, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->b:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    if-ne v1, v4, :cond_0

    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "audio"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-nez v1, :cond_6

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    :cond_6
    iget-object v1, v0, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-direct {p0, v3, v0}, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->a(Landroid/database/MatrixCursor;Lflipboard/objs/FeedItem;)V

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->a:Lflipboard/util/Log;

    const-string v1, "Audio item without a URL, skipping"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 167
    :cond_8
    iget-object v0, p0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
