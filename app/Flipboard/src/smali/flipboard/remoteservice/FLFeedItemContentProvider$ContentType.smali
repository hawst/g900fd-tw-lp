.class public final enum Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;
.super Ljava/lang/Enum;
.source "FLFeedItemContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

.field public static final enum b:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

.field private static final synthetic c:[Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->a:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    new-instance v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v3}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->b:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    sget-object v1, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->a:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->b:Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->c:[Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    return-object v0
.end method

.method public static values()[Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->c:[Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    invoke-virtual {v0}, [Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;

    return-object v0
.end method
