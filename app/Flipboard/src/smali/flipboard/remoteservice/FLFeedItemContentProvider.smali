.class public Lflipboard/remoteservice/FLFeedItemContentProvider;
.super Landroid/content/ContentProvider;
.source "FLFeedItemContentProvider.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static final b:Landroid/net/Uri;

.field private static final c:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 31
    const-string v0, "Content provider"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->a:Lflipboard/util/Log;

    .line 44
    const-string v0, "content://flipboard.remoteservice.feeds/feeditems"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->b:Landroid/net/Uri;

    .line 68
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 69
    sput-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->c:Landroid/content/UriMatcher;

    const-string v1, "flipboard.remoteservice.feeds"

    const-string v2, "feeditems"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 70
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->c:Landroid/content/UriMatcher;

    const-string v1, "flipboard.remoteservice.feeds.china"

    const-string v2, "feeditems"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 71
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->c:Landroid/content/UriMatcher;

    const-string v1, "flipboard.remoteservice.feeds.debug"

    const-string v2, "feeditems"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->c:Landroid/content/UriMatcher;

    const-string v1, "flipboard.remoteservice.feeds"

    const-string v2, "feeditems/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 54
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 226
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.flipboard.feeditems"

    .line 230
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.flipboard.feeditems"

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 238
    return-object p1
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11

    .prologue
    .line 78
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "author"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sourceurl"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pagekey"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "time"

    aput-object v2, v0, v1

    .line 79
    new-instance v5, Landroid/database/MatrixCursor;

    invoke-direct {v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 80
    const/4 v2, 0x0

    .line 81
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 82
    packed-switch v0, :pswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown URI"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :pswitch_0
    const-string v2, "SELECT items, descriptor FROM sections"

    .line 95
    :pswitch_1
    array-length v0, p4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget-object v0, p4, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    aget-object v0, p4, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    aget-object v0, p4, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    aget-object v0, p4, v0

    if-nez v0, :cond_2

    .line 97
    :cond_0
    sget-object v0, Lflipboard/remoteservice/FLFeedItemContentProvider;->a:Lflipboard/util/Log;

    const-string v1, "Invalid selection parameters - selection parameters are required"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    :cond_1
    :goto_0
    return-object v5

    .line 101
    :cond_2
    const/4 v0, 0x0

    aget-object v0, p4, v0

    .line 102
    const/4 v1, 0x1

    aget-object v1, p4, v1

    .line 103
    const/4 v3, 0x2

    aget-object v6, p4, v3

    .line 106
    const/4 v3, 0x3

    :try_start_0
    aget-object v3, p4, v3

    invoke-static {v3}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->valueOf(Ljava/lang/String;)Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 114
    :try_start_1
    new-instance v3, Ljava/util/Locale;

    invoke-direct {v3, v1, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 121
    invoke-static {v0, v3, v4}, Lflipboard/remoteservice/RemoteServiceUtil;->a(Ljava/lang/String;Ljava/util/Locale;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;)Ljava/lang/String;

    move-result-object v3

    .line 122
    if-eqz v3, :cond_1

    .line 128
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v7, "get"

    invoke-direct {v1, v7}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 129
    const-string v7, "what"

    const-string v8, "items"

    invoke-virtual {v1, v7, v8}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 130
    const-string v7, "sectionIdentifier"

    invoke-virtual {v1, v7, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 131
    const-string v7, "sectionType"

    const-string v8, "%s_%s_%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v0, 0x1

    aput-object v6, v9, v0

    const/4 v0, 0x2

    invoke-virtual {v4}, Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v0

    invoke-static {v8, v9}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v7, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 132
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 134
    if-eqz v2, :cond_1

    .line 140
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v7, "sections"

    new-instance v0, Lflipboard/remoteservice/FLFeedItemContentProvider$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/remoteservice/FLFeedItemContentProvider$1;-><init>(Lflipboard/remoteservice/FLFeedItemContentProvider;Ljava/lang/String;Ljava/lang/String;Lflipboard/remoteservice/FLFeedItemContentProvider$ContentType;Landroid/database/MatrixCursor;)V

    invoke-virtual {v6, v7, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/DatabaseHandler;)V

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 109
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    .line 117
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method
