.class public Lflipboard/remoteservice/UserContentProvider;
.super Landroid/content/ContentProvider;
.source "UserContentProvider.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field private static final b:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    const-string v0, "Content provider"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/remoteservice/UserContentProvider;->a:Lflipboard/util/Log;

    .line 38
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 39
    sput-object v0, Lflipboard/remoteservice/UserContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "flipboard.auth"

    const-string v2, "user"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "user"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 84
    return-object p1
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 44
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "username"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "numberOfMagazine"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "uid"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "accessToken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "refreshToken"

    aput-object v2, v0, v1

    .line 45
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lflipboard/app/FlipboardApplication;->p()V

    .line 47
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 49
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/service/User;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    const-string v2, "flipboard"

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 51
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v0}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 53
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v6, v6, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v6, v6, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v6}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v5

    const/4 v3, 0x4

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x5

    iget-object v3, v2, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v3, v3, Lflipboard/service/Account$Meta;->d:Ljava/lang/String;

    aput-object v3, v4, v0

    const/4 v0, 0x6

    iget-object v2, v2, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v2, v2, Lflipboard/service/Account$Meta;->e:Ljava/lang/String;

    aput-object v2, v4, v0

    invoke-virtual {v1, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 57
    :cond_0
    return-object v1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method
