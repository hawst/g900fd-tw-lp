.class final enum Lflipboard/remoteservice/MessengerService$ErrorMessage;
.super Ljava/lang/Enum;
.source "MessengerService.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/remoteservice/MessengerService$ErrorMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/remoteservice/MessengerService$ErrorMessage;

.field public static final enum b:Lflipboard/remoteservice/MessengerService$ErrorMessage;

.field public static final enum c:Lflipboard/remoteservice/MessengerService$ErrorMessage;

.field private static final synthetic d:[Lflipboard/remoteservice/MessengerService$ErrorMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;

    const-string v1, "INVALID_ARGUMENTS"

    invoke-direct {v0, v1, v2}, Lflipboard/remoteservice/MessengerService$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;->a:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    .line 62
    new-instance v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;

    const-string v1, "NO_NETWORK_CONNECTIVITY"

    invoke-direct {v0, v1, v3}, Lflipboard/remoteservice/MessengerService$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;->b:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    .line 63
    new-instance v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v4}, Lflipboard/remoteservice/MessengerService$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;->c:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/remoteservice/MessengerService$ErrorMessage;

    sget-object v1, Lflipboard/remoteservice/MessengerService$ErrorMessage;->a:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/remoteservice/MessengerService$ErrorMessage;->b:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/remoteservice/MessengerService$ErrorMessage;->c:Lflipboard/remoteservice/MessengerService$ErrorMessage;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;->d:[Lflipboard/remoteservice/MessengerService$ErrorMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/remoteservice/MessengerService$ErrorMessage;
    .locals 1

    .prologue
    .line 60
    const-class v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/remoteservice/MessengerService$ErrorMessage;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lflipboard/remoteservice/MessengerService$ErrorMessage;->d:[Lflipboard/remoteservice/MessengerService$ErrorMessage;

    invoke-virtual {v0}, [Lflipboard/remoteservice/MessengerService$ErrorMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/remoteservice/MessengerService$ErrorMessage;

    return-object v0
.end method
