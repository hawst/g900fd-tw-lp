.class public abstract enum Lbutterknife/ButterKnife$Finder;
.super Ljava/lang/Enum;
.source "ButterKnife.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbutterknife/ButterKnife$Finder;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbutterknife/ButterKnife$Finder;

.field public static final enum b:Lbutterknife/ButterKnife$Finder;

.field public static final enum c:Lbutterknife/ButterKnife$Finder;

.field private static final synthetic d:[Lbutterknife/ButterKnife$Finder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 92
    new-instance v0, Lbutterknife/ButterKnife$Finder$1;

    const-string v1, "VIEW"

    invoke-direct {v0, v1}, Lbutterknife/ButterKnife$Finder$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbutterknife/ButterKnife$Finder;->a:Lbutterknife/ButterKnife$Finder;

    .line 97
    new-instance v0, Lbutterknife/ButterKnife$Finder$2;

    const-string v1, "ACTIVITY"

    invoke-direct {v0, v1}, Lbutterknife/ButterKnife$Finder$2;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbutterknife/ButterKnife$Finder;->b:Lbutterknife/ButterKnife$Finder;

    .line 102
    new-instance v0, Lbutterknife/ButterKnife$Finder$3;

    const-string v1, "DIALOG"

    invoke-direct {v0, v1}, Lbutterknife/ButterKnife$Finder$3;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbutterknife/ButterKnife$Finder;->c:Lbutterknife/ButterKnife$Finder;

    .line 91
    const/4 v0, 0x3

    new-array v0, v0, [Lbutterknife/ButterKnife$Finder;

    const/4 v1, 0x0

    sget-object v2, Lbutterknife/ButterKnife$Finder;->a:Lbutterknife/ButterKnife$Finder;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbutterknife/ButterKnife$Finder;->b:Lbutterknife/ButterKnife$Finder;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbutterknife/ButterKnife$Finder;->c:Lbutterknife/ButterKnife$Finder;

    aput-object v2, v0, v1

    sput-object v0, Lbutterknife/ButterKnife$Finder;->d:[Lbutterknife/ButterKnife$Finder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lbutterknife/ButterKnife$Finder;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static varargs a([Landroid/view/View;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">([TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 113
    new-instance v0, Lbutterknife/ImmutableViewList;

    invoke-direct {v0, p0}, Lbutterknife/ImmutableViewList;-><init>([Landroid/view/View;)V

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbutterknife/ButterKnife$Finder;
    .locals 1

    .prologue
    .line 91
    const-class v0, Lbutterknife/ButterKnife$Finder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbutterknife/ButterKnife$Finder;

    return-object v0
.end method

.method public static values()[Lbutterknife/ButterKnife$Finder;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lbutterknife/ButterKnife$Finder;->d:[Lbutterknife/ButterKnife$Finder;

    invoke-virtual {v0}, [Lbutterknife/ButterKnife$Finder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbutterknife/ButterKnife$Finder;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;I)Landroid/view/View;
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;
    .locals 3

    .prologue
    .line 117
    invoke-virtual {p0, p1, p2}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 118
    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required view with id \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found. If this view is optional add \'@Optional\' annotation."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    return-object v0
.end method
