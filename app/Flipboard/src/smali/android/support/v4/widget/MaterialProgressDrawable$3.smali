.class Landroid/support/v4/widget/MaterialProgressDrawable$3;
.super Landroid/view/animation/Animation;
.source "MaterialProgressDrawable.java"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

.field final synthetic b:Landroid/support/v4/widget/MaterialProgressDrawable;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/MaterialProgressDrawable;Landroid/support/v4/widget/MaterialProgressDrawable$Ring;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->b:Landroid/support/v4/widget/MaterialProgressDrawable;

    iput-object p2, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 7

    .prologue
    const v6, 0x3f4ccccd    # 0.8f

    .line 336
    iget-object v0, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    iget v0, v0, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->g:F

    float-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    iget-object v4, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    iget-wide v4, v4, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->q:D

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 338
    iget-object v1, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    iget v1, v1, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->l:F

    .line 339
    iget-object v2, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    iget v2, v2, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->k:F

    .line 340
    iget-object v3, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    iget v3, v3, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->m:F

    .line 343
    sub-float v0, v6, v0

    .line 344
    invoke-static {}, Landroid/support/v4/widget/MaterialProgressDrawable;->a()Landroid/view/animation/Interpolator;

    move-result-object v4

    invoke-interface {v4, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v4

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    .line 346
    iget-object v1, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->b(F)V

    .line 348
    invoke-static {}, Landroid/support/v4/widget/MaterialProgressDrawable;->b()Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v6

    add-float/2addr v0, v2

    .line 351
    iget-object v1, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->a(F)V

    .line 353
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float/2addr v0, p1

    add-float/2addr v0, v3

    .line 354
    iget-object v1, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->a:Landroid/support/v4/widget/MaterialProgressDrawable$Ring;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/MaterialProgressDrawable$Ring;->c(F)V

    .line 356
    const/high16 v0, 0x43100000    # 144.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x44340000    # 720.0f

    iget-object v2, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->b:Landroid/support/v4/widget/MaterialProgressDrawable;

    invoke-static {v2}, Landroid/support/v4/widget/MaterialProgressDrawable;->c(Landroid/support/v4/widget/MaterialProgressDrawable;)F

    move-result v2

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 358
    iget-object v1, p0, Landroid/support/v4/widget/MaterialProgressDrawable$3;->b:Landroid/support/v4/widget/MaterialProgressDrawable;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/MaterialProgressDrawable;->b(F)V

    .line 359
    return-void
.end method
