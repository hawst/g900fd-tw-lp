.class public abstract Landroid/support/v4/app/NotificationCompat$Style;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# instance fields
.field d:Landroid/support/v4/app/NotificationCompat$Builder;

.field e:Ljava/lang/CharSequence;

.field f:Ljava/lang/CharSequence;

.field g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/NotificationCompat$Style;->g:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/NotificationCompat$Builder;)V
    .locals 1

    .prologue
    .line 1529
    iget-object v0, p0, Landroid/support/v4/app/NotificationCompat$Style;->d:Landroid/support/v4/app/NotificationCompat$Builder;

    if-eq v0, p1, :cond_0

    .line 1530
    iput-object p1, p0, Landroid/support/v4/app/NotificationCompat$Style;->d:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1531
    iget-object v0, p0, Landroid/support/v4/app/NotificationCompat$Style;->d:Landroid/support/v4/app/NotificationCompat$Builder;

    if-eqz v0, :cond_0

    .line 1532
    iget-object v0, p0, Landroid/support/v4/app/NotificationCompat$Style;->d:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1535
    :cond_0
    return-void
.end method
