.class public Lcom/samsung/android/app/gestureservice/SituationDetector;
.super Ljava/lang/Object;
.source "SituationDetector.java"


# static fields
.field private static final DETECT_G:F = 9.81f

.field private static final DETECT_GYROSCOPE_SHAKE:F = 1.5f

.field private static final G:F = 9.81f

.field private static final TAG:Ljava/lang/String; = "SituationDetector"


# instance fields
.field private mACCMaxX:F

.field private mACCMaxY:F

.field private mACCMaxZ:F

.field private mACCMinX:F

.field private mACCMinY:F

.field private mACCMinZ:F

.field private mACC_x0:F

.field private mACC_y0:F

.field private mACC_z0:F

.field private mAccelerometerListener:Landroid/hardware/SensorEventListener;

.field private mContext:Landroid/content/Context;

.field private mGYROMaxX:F

.field private mGYROMaxY:F

.field private mGYROMaxZ:F

.field private mGYROMinX:F

.field private mGYROMinY:F

.field private mGYROMinZ:F

.field private mGYRO_x0:F

.field private mGYRO_y0:F

.field private mGYRO_z0:F

.field private mGyroscopeListener:Landroid/hardware/SensorEventListener;

.field private mHandler:Landroid/os/Handler;

.field private mKickRunner:Ljava/lang/Runnable;

.field private mLock:Ljava/lang/Object;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mLock:Ljava/lang/Object;

    .line 32
    iput v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F

    .line 33
    iput v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F

    .line 34
    iput v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F

    .line 43
    iput v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_x0:F

    .line 44
    iput v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_y0:F

    .line 45
    iput v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_z0:F

    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mHandler:Landroid/os/Handler;

    .line 60
    new-instance v0, Lcom/samsung/android/app/gestureservice/SituationDetector$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/SituationDetector$1;-><init>(Lcom/samsung/android/app/gestureservice/SituationDetector;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mKickRunner:Ljava/lang/Runnable;

    .line 152
    new-instance v0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/SituationDetector$2;-><init>(Lcom/samsung/android/app/gestureservice/SituationDetector;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mAccelerometerListener:Landroid/hardware/SensorEventListener;

    .line 190
    new-instance v0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/SituationDetector$3;-><init>(Lcom/samsung/android/app/gestureservice/SituationDetector;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGyroscopeListener:Landroid/hardware/SensorEventListener;

    .line 71
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mContext:Landroid/content/Context;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/gestureservice/SituationDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->resetMinMax()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/gestureservice/SituationDetector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mKickRunner:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinZ:F

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinZ:F

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxZ:F

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxZ:F

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_x0:F

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_x0:F

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_y0:F

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_y0:F

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_z0:F

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_z0:F

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinX:F

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinX:F

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxX:F

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxX:F

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinY:F

    return v0
.end method

.method static synthetic access$1702(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinY:F

    return p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxY:F

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxY:F

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/gestureservice/SituationDetector;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinX:F

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinX:F

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxX:F

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxX:F

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinY:F

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinY:F

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/app/gestureservice/SituationDetector;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxY:F

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/SituationDetector;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxY:F

    return p1
.end method

.method private resetMinMax()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 95
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F

    .line 96
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F

    .line 97
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F

    .line 98
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinX:F

    .line 99
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxX:F

    .line 100
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinY:F

    .line 101
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxY:F

    .line 102
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinZ:F

    .line 103
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxZ:F

    .line 105
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_x0:F

    .line 106
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_y0:F

    .line 107
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_z0:F

    .line 108
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinX:F

    .line 109
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxX:F

    .line 110
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinY:F

    .line 111
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxY:F

    .line 112
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F

    .line 113
    iput v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F

    .line 114
    return-void
.end method


# virtual methods
.method public isMoving()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const v2, 0x411cf5c3    # 9.81f

    .line 132
    iget v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxX:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinX:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxY:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinY:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 138
    iget v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxZ:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinZ:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 141
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShaking()Z
    .locals 4

    .prologue
    const/high16 v3, 0x3fc00000    # 1.5f

    .line 145
    const-string v0, "SituationDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mGyromaxz : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mgyroMin z : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    .line 147
    :cond_0
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startDetection()V
    .locals 5

    .prologue
    .line 75
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mContext:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 78
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 79
    .local v0, "sensor":Landroid/hardware/Sensor;
    if-eqz v0, :cond_0

    .line 80
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mAccelerometerListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_1

    .line 85
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGyroscopeListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 87
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->resetMinMax()V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mKickRunner:Ljava/lang/Runnable;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 92
    return-void

    .line 87
    .end local v0    # "sensor":Landroid/hardware/Sensor;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public stopDetection()V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mKickRunner:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mAccelerometerListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mGyroscopeListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 127
    :cond_1
    monitor-exit v1

    .line 128
    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
