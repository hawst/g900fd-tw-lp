.class Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;
.super Ljava/lang/Thread;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventDeliveryThread"
.end annotation


# instance fields
.field public mEventDeliveryHandler:Landroid/os/Handler;

.field public mLooper:Landroid/os/Looper;

.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 134
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 135
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;-><init>(Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->mEventDeliveryHandler:Landroid/os/Handler;

    .line 178
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->mLooper:Landroid/os/Looper;

    .line 179
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 180
    return-void
.end method
