.class Lcom/samsung/android/app/gestureservice/GestureService$6;
.super Lcom/samsung/android/service/gesture/IGestureService$Stub;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Lcom/samsung/android/service/gesture/IGestureService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public disable()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 427
    return-void
.end method

.method public enable()V
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 422
    return-void
.end method

.method public getProviderInfo(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3300(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 439
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->getProviderInfoLocked(Ljava/lang/String;)Landroid/os/Bundle;
    invoke-static {v0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3500(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getProviders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 431
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3300(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 432
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->getProvidersLocked()Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3400(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerCallback(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "eventType"    # Ljava/lang/String;
    .param p4, "supportLandscape"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 352
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerCallback : binder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " provider = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " eventType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bGestureServiceEnable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z
    invoke-static {v3}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2600(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 356
    .local v0, "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    # getter for: Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->access$2700(Lcom/samsung/android/app/gestureservice/GestureService$Listener;)Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    :goto_0
    return-void

    .line 362
    .end local v0    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_1
    const/4 v0, 0x0

    .line 363
    .restart local v0    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .end local v0    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    and-int/lit8 v5, p4, 0x1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/gestureservice/GestureService$Listener;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;ZII)V

    .line 364
    .restart local v0    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 366
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateGestureSetting()V
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 369
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateFlags()V
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1200(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 370
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1300(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 372
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->increaseRefCountAndStartService(Ljava/lang/String;)V
    invoke-static {v1, p2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2900(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resetGestureService(Ljava/lang/String;)V
    .locals 2
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 415
    const-string v0, "GestureService"

    const-string v1, "resetGestureService : called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->removeProviders()V
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3200(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 417
    return-void
.end method

.method public sendInputEvent(Landroid/view/InputEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/InputEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 342
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 343
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->what:I

    .line 344
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 345
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2500(Lcom/samsung/android/app/gestureservice/GestureService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 346
    return-void
.end method

.method public unregisterCallback(Landroid/os/IBinder;Ljava/lang/String;)Z
    .locals 7
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 377
    const/4 v2, 0x0

    .line 378
    .local v2, "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 379
    .local v1, "l":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    # getter for: Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->access$2700(Lcom/samsung/android/app/gestureservice/GestureService$Listener;)Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 380
    const-string v4, "GestureService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unregisterCallback : binder = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " provider = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " eventType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const-string v4, "GestureService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " PID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " UID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    move-object v2, v1

    .line 387
    .end local v1    # "l":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_1
    if-nez v2, :cond_2

    .line 388
    const-string v4, "GestureService"

    const-string v5, "unregisterCallback : matching listener is NOT EXIST"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :goto_0
    return v3

    .line 392
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->decreaseRefCountAndStopService(Ljava/lang/String;)V
    invoke-static {v4, p2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3000(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)V

    .line 394
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 395
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 398
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateFlags()V
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1200(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 399
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1300(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 400
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 401
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1300(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_0

    .line 404
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/GestureService$6;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->clearStatusBar()V
    invoke-static {v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3100(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 405
    if-eqz v2, :cond_5

    .line 406
    invoke-interface {p1, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 410
    :cond_5
    const/4 v3, 0x1

    goto :goto_0
.end method
