.class public Lcom/samsung/android/app/gestureservice/TspGestureProvider;
.super Ljava/lang/Object;
.source "TspGestureProvider.java"

# interfaces
.implements Lcom/samsung/android/app/gestureservice/GestureProviderInterface;


# static fields
.field private static final FLING_DISTANCE:F = 100.0f

.field private static final FLING_DURATION:F = 800.0f

.field public static final NAME:Ljava/lang/String; = "tsp_provider"

.field public static final TAG:Ljava/lang/String; = "TspGestureProvider"


# instance fields
.field private mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

.field private mHoverEndTime:J

.field private mHoverStartTime:J

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;Landroid/content/Context;)V
    .locals 0
    .param p1, "gestureEventObserver"    # Lcom/samsung/android/app/gestureservice/IGestureEventObserver;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    .line 47
    return-void
.end method

.method private handleFlingEvent(FF)V
    .locals 4
    .param p1, "moveX"    # F
    .param p2, "moveY"    # F

    .prologue
    const/4 v3, 0x0

    .line 74
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 75
    invoke-static {}, Lcom/samsung/android/service/gesture/GestureEvent;->obtain()Lcom/samsung/android/service/gesture/GestureEvent;

    move-result-object v0

    .line 76
    .local v0, "event":Lcom/samsung/android/service/gesture/GestureEvent;
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 78
    cmpl-float v1, p1, v3

    if-lez v1, :cond_0

    .line 79
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 80
    const-string v1, "TspGestureProvider"

    const-string v2, "GESTURE_EVENT_SWEEP_RIGHT"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 87
    invoke-virtual {v0}, Lcom/samsung/android/service/gesture/GestureEvent;->recycle()V

    .line 103
    :goto_1
    return-void

    .line 82
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 83
    const-string v1, "TspGestureProvider"

    const-string v2, "GESTURE_EVENT_SWEEP_LEFT"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 89
    .end local v0    # "event":Lcom/samsung/android/service/gesture/GestureEvent;
    :cond_1
    invoke-static {}, Lcom/samsung/android/service/gesture/GestureEvent;->obtain()Lcom/samsung/android/service/gesture/GestureEvent;

    move-result-object v0

    .line 90
    .restart local v0    # "event":Lcom/samsung/android/service/gesture/GestureEvent;
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 92
    cmpl-float v1, p2, v3

    if-lez v1, :cond_2

    .line 93
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 94
    const-string v1, "TspGestureProvider"

    const-string v2, "GESTURE_EVENT_SWEEP_DOWN"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 101
    invoke-virtual {v0}, Lcom/samsung/android/service/gesture/GestureEvent;->recycle()V

    goto :goto_1

    .line 96
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 97
    const-string v1, "TspGestureProvider"

    const-string v2, "GESTURE_EVENT_SWEEP_UP"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    const-string v0, "tsp_provider"

    return-object v0
.end method

.method public getSupportedGestures()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v0, "gestures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    return-object v0
.end method

.method public onInputEvent(Landroid/view/InputEvent;)V
    .locals 10
    .param p1, "e"    # Landroid/view/InputEvent;

    .prologue
    const/high16 v9, 0x42c80000    # 100.0f

    .line 50
    move-object v0, p1

    check-cast v0, Landroid/view/MotionEvent;

    .line 52
    .local v0, "event":Landroid/view/MotionEvent;
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/16 v4, 0x9

    if-ne v3, v4, :cond_0

    .line 53
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mHoverStartTime:J

    .line 54
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mX:F

    .line 55
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mY:F

    .line 58
    :cond_0
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_2

    .line 59
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mHoverEndTime:J

    .line 61
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mX:F

    sub-float v1, v3, v4

    .line 62
    .local v1, "moveX":F
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mY:F

    sub-float v2, v3, v4

    .line 64
    .local v2, "moveY":F
    const-string v3, "TspGestureProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mHoverEndTime:J

    iget-wide v7, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mHoverStartTime:J

    sub-long/2addr v5, v7

    long-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v9

    if-gtz v3, :cond_1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v9

    if-lez v3, :cond_2

    :cond_1
    iget-wide v3, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mHoverEndTime:J

    iget-wide v5, p0, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->mHoverStartTime:J

    sub-long/2addr v3, v5

    long-to-float v3, v3

    const/high16 v4, 0x44480000    # 800.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 68
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->handleFlingEvent(FF)V

    .line 71
    .end local v1    # "moveX":F
    .end local v2    # "moveY":F
    :cond_2
    return-void
.end method

.method public serviceStatusChanged()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public startService()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public stopService()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method
