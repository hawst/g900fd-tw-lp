.class Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;
.super Landroid/os/Handler;
.source "IrGestureProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/IrGestureProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/IrGestureProvider;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/IrGestureProvider;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;->this$0:Lcom/samsung/android/app/gestureservice/IrGestureProvider;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 90
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;->this$0:Lcom/samsung/android/app/gestureservice/IrGestureProvider;

    const/4 v1, -0x1

    # setter for: Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousEvent:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->access$002(Lcom/samsung/android/app/gestureservice/IrGestureProvider;I)I

    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;->this$0:Lcom/samsung/android/app/gestureservice/IrGestureProvider;

    # setter for: Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mIgnoreSweep:Z
    invoke-static {v0, v2}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->access$102(Lcom/samsung/android/app/gestureservice/IrGestureProvider;Z)Z

    .line 95
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;->this$0:Lcom/samsung/android/app/gestureservice/IrGestureProvider;

    # setter for: Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mShakeStarted:Z
    invoke-static {v0, v2}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->access$202(Lcom/samsung/android/app/gestureservice/IrGestureProvider;Z)Z

    .line 98
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 99
    return-void
.end method
