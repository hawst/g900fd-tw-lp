.class public Lcom/samsung/android/app/gestureservice/IrGestureProvider;
.super Ljava/lang/Object;
.source "IrGestureProvider.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;
.implements Lcom/samsung/android/app/gestureservice/GestureProviderInterface;


# static fields
.field private static final ANGLE_DIRECTION_FROM_BOTTOM:I = 0xb4

.field private static final ANGLE_DIRECTION_FROM_LEFT:I = 0x5a

.field private static final ANGLE_DIRECTION_FROM_RIGHT:I = 0x10e

.field private static final ANGLE_DIRECTION_FROM_TOP:I = 0x0

.field private static final BIDIRECTION_RECOGNITION_RANGE:I = 0x46

.field private static final GET_RANGE_BEGIN:I = -0x1

.field private static final GET_RANGE_END:I = 0x1

.field private static final HORIZONTAL_RECOGNITION_RANGE:I = 0x1e

.field private static final MODE_4DIRECTION:I = 0x3

.field private static final MODE_HORIZONTAL:I = 0x2

.field private static final MODE_HORIZONTAL_AND_ORIENTATION_270:I = 0x22

.field private static final MODE_HORIZONTAL_AND_ORIENTATION_90:I = 0x12

.field private static final MODE_HORIZONTAL_WAVE:I = 0x4

.field private static final MODE_VIRTICAL:I = 0x1

.field private static final MODE_VIRTICAL_AND_ORIENTATION_270:I = 0x21

.field private static final MODE_VIRTICAL_AND_ORIENTATION_90:I = 0x11

.field public static final NAME:Ljava/lang/String; = "ir_provider"

.field private static final ORIENTAITION_0:I = 0x0

.field private static final ORIENTATION_270:I = 0x20

.field private static final ORIENTATION_90:I = 0x10

.field private static final SHAKE_RECOG_TIME:I = 0x320

.field private static final SHAKE_TIMER_EXPIRED:I = 0x2

.field private static final SWEEP_IGNORE_TIME:I = 0x1f4

.field private static final SWEEP_TIMER_EXPIRED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "IrGestureProvider"

.field private static final VIRTICAL_RECOGNITION_RANGE:I = 0x3c

.field private static mSContextManager:Landroid/hardware/scontext/SContextManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDirectionMode:B

.field private mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

.field private final mGestureService:Lcom/samsung/android/app/gestureservice/IGestureService;

.field private mHandler:Landroid/os/Handler;

.field private mIgnoreSweep:Z

.field private mLastOrientation:I

.field private mPreviousEvent:I

.field private mPreviousShake:I

.field private mServiceStarted:Z

.field private mShakeStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;Landroid/content/Context;Lcom/samsung/android/app/gestureservice/IGestureService;)V
    .locals 2
    .param p1, "gestureEventObserver"    # Lcom/samsung/android/app/gestureservice/IGestureEventObserver;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "gestureService"    # Lcom/samsung/android/app/gestureservice/IGestureService;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousEvent:I

    .line 48
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mIgnoreSweep:Z

    .line 50
    iput v1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousShake:I

    .line 51
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mShakeStarted:Z

    .line 83
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mServiceStarted:Z

    .line 85
    iput-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    .line 86
    iput v1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    .line 87
    new-instance v0, Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/IrGestureProvider$1;-><init>(Lcom/samsung/android/app/gestureservice/IrGestureProvider;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mHandler:Landroid/os/Handler;

    .line 104
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    .line 105
    iput-object p2, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mContext:Landroid/content/Context;

    .line 106
    iput-object p3, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureService:Lcom/samsung/android/app/gestureservice/IGestureService;

    .line 107
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/app/gestureservice/IrGestureProvider;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/IrGestureProvider;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousEvent:I

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/android/app/gestureservice/IrGestureProvider;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/IrGestureProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mIgnoreSweep:Z

    return p1
.end method

.method static synthetic access$202(Lcom/samsung/android/app/gestureservice/IrGestureProvider;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/IrGestureProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mShakeStarted:Z

    return p1
.end method


# virtual methods
.method public checkOppositeDirection(Lcom/samsung/android/service/gesture/GestureEvent;I)Z
    .locals 2
    .param p1, "e"    # Lcom/samsung/android/service/gesture/GestureEvent;
    .param p2, "previousValue"    # I

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "flag":Z
    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 148
    :pswitch_1
    if-nez p2, :cond_0

    .line 149
    const/4 v0, 0x1

    goto :goto_0

    .line 152
    :pswitch_2
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 153
    const/4 v0, 0x1

    goto :goto_0

    .line 156
    :pswitch_3
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 157
    const/4 v0, 0x1

    goto :goto_0

    .line 160
    :pswitch_4
    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    .line 161
    const/4 v0, 0x1

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getAngleRange2Direction(II)I
    .locals 2
    .param p1, "angleDirection"    # I
    .param p2, "sign"    # I

    .prologue
    .line 262
    mul-int/lit8 v1, p2, 0x46

    add-int/2addr v1, p1

    rem-int/lit16 v0, v1, 0x168

    .line 263
    .local v0, "calculatedAngle":I
    const/16 v1, 0x122

    if-ne v0, v1, :cond_0

    .line 264
    const/16 v0, -0x46

    .line 266
    .end local v0    # "calculatedAngle":I
    :cond_0
    return v0
.end method

.method public getAngleRange4Direction(IIZ)I
    .locals 4
    .param p1, "angleDirection"    # I
    .param p2, "sign"    # I
    .param p3, "landscape"    # Z

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "calculatedAngle":I
    const/4 v2, 0x0

    .line 172
    .local v2, "mVIRTICAL_ANGLE_RANGE":I
    const/4 v1, 0x0

    .line 174
    .local v1, "mHORIZONTAL_ANGLE_RANGE":I
    if-eqz p3, :cond_1

    .line 175
    const/16 v2, 0x1e

    .line 176
    const/16 v1, 0x3c

    .line 182
    :goto_0
    const/16 v3, 0xb4

    if-eq p1, v3, :cond_0

    if-nez p1, :cond_2

    .line 183
    :cond_0
    mul-int v3, v2, p2

    add-int/2addr v3, p1

    rem-int/lit16 v0, v3, 0x168

    .line 184
    rsub-int v3, v2, 0x168

    if-ne v0, v3, :cond_3

    .line 185
    neg-int v3, v2

    .line 191
    :goto_1
    return v3

    .line 178
    :cond_1
    const/16 v2, 0x3c

    .line 179
    const/16 v1, 0x1e

    goto :goto_0

    .line 188
    :cond_2
    mul-int v3, v1, p2

    add-int/2addr v3, p1

    rem-int/lit16 v0, v3, 0x168

    :cond_3
    move v3, v0

    .line 191
    goto :goto_1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    const-string v0, "ir_provider"

    return-object v0
.end method

.method public getSupportedGestures()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v0, "gestures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    return-object v0
.end method

.method public mapAngleTo2DirectionGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;I)V
    .locals 8
    .param p1, "e"    # Lcom/samsung/android/service/gesture/GestureEvent;
    .param p2, "angle"    # I

    .prologue
    const/16 v7, 0xb4

    const/16 v6, 0x5a

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 282
    iget-byte v1, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    iget v2, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    add-int/2addr v1, v2

    sparse-switch v1, :sswitch_data_0

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 286
    :sswitch_0
    const/16 v0, 0x122

    .line 289
    .local v0, "flipAngleRangeBegin":I
    if-ge v0, p2, :cond_1

    .line 290
    add-int/lit16 p2, p2, -0x168

    .line 293
    :cond_1
    invoke-virtual {p0, v5, v4}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-le p2, v1, :cond_2

    invoke-virtual {p0, v5, v3}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-ge p2, v1, :cond_2

    .line 294
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 295
    const-string v1, "ir_provider"

    invoke-virtual {p1, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 296
    :cond_2
    invoke-virtual {p0, v7, v4}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-le p2, v1, :cond_0

    invoke-virtual {p0, v7, v3}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 297
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 298
    const-string v1, "ir_provider"

    invoke-virtual {p1, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 304
    .end local v0    # "flipAngleRangeBegin":I
    :sswitch_1
    const/16 v1, 0x10e

    invoke-virtual {p0, v1, v4}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-le p2, v1, :cond_3

    const/16 v1, 0x10e

    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-ge p2, v1, :cond_3

    .line 305
    invoke-virtual {p1, v3}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 306
    const-string v1, "ir_provider"

    invoke-virtual {p1, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 307
    :cond_3
    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-le p2, v1, :cond_0

    invoke-virtual {p0, v6, v3}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange2Direction(II)I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 308
    invoke-virtual {p1, v5}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 309
    const-string v1, "ir_provider"

    invoke-virtual {p1, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_1
        0x12 -> :sswitch_0
        0x21 -> :sswitch_1
        0x22 -> :sswitch_0
    .end sparse-switch
.end method

.method public mapAngleTo4DirectionGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;I)V
    .locals 10
    .param p1, "e"    # Lcom/samsung/android/service/gesture/GestureEvent;
    .param p2, "angle"    # I

    .prologue
    const/16 v9, 0xb4

    const/16 v8, 0x5a

    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 206
    const-string v2, "IrGestureProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "In 4direction : Orientation("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), angle("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/16 v0, 0x12c

    .line 209
    .local v0, "flipAngleRangeBegin":I
    const/4 v1, 0x0

    .line 212
    .local v1, "landscape":Z
    iget v2, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    const/16 v3, 0x10

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1

    .line 213
    :cond_0
    const/16 v0, 0x14a

    .line 214
    const/4 v1, 0x1

    .line 218
    :cond_1
    if-ge v0, p2, :cond_2

    .line 219
    add-int/lit16 p2, p2, -0x168

    .line 222
    :cond_2
    invoke-virtual {p0, v7, v6, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-le p2, v2, :cond_4

    invoke-virtual {p0, v7, v5, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-gt p2, v2, :cond_4

    .line 223
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 224
    const-string v2, "ir_provider"

    invoke-virtual {p1, v2}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 235
    :cond_3
    :goto_0
    return-void

    .line 225
    :cond_4
    invoke-virtual {p0, v9, v6, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-le p2, v2, :cond_5

    invoke-virtual {p0, v9, v5, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-gt p2, v2, :cond_5

    .line 226
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 227
    const-string v2, "ir_provider"

    invoke-virtual {p1, v2}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_5
    const/16 v2, 0x10e

    invoke-virtual {p0, v2, v6, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-le p2, v2, :cond_6

    const/16 v2, 0x10e

    invoke-virtual {p0, v2, v5, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-gt p2, v2, :cond_6

    .line 229
    invoke-virtual {p1, v5}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 230
    const-string v2, "ir_provider"

    invoke-virtual {p1, v2}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_6
    invoke-virtual {p0, v8, v6, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-le p2, v2, :cond_3

    invoke-virtual {p0, v8, v5, v1}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getAngleRange4Direction(IIZ)I

    move-result v2

    if-gt p2, v2, :cond_3

    .line 232
    invoke-virtual {p1, v7}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 233
    const-string v2, "ir_provider"

    invoke-virtual {p1, v2}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public mapDirectionToGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;I)V
    .locals 1
    .param p1, "e"    # Lcom/samsung/android/service/gesture/GestureEvent;
    .param p2, "direction"    # I

    .prologue
    .line 238
    packed-switch p2, :pswitch_data_0

    .line 258
    :goto_0
    return-void

    .line 240
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 241
    const-string v0, "ir_provider"

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 245
    const-string v0, "ir_provider"

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :pswitch_2
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 249
    const-string v0, "ir_provider"

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :pswitch_3
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 253
    const-string v0, "ir_provider"

    invoke-virtual {p1, v0}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    const/4 v8, 0x3

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 319
    iget-object v2, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 320
    .local v2, "scontext":Landroid/hardware/scontext/SContext;
    invoke-virtual {v2}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v6

    if-ne v6, v9, :cond_0

    .line 321
    invoke-static {}, Lcom/samsung/android/service/gesture/GestureEvent;->obtain()Lcom/samsung/android/service/gesture/GestureEvent;

    move-result-object v1

    .line 322
    .local v1, "e":Lcom/samsung/android/service/gesture/GestureEvent;
    invoke-virtual {v1, v10}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 323
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 324
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v6, v1}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 326
    .end local v1    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    :cond_0
    invoke-virtual {v2}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v6

    const/4 v7, 0x7

    if-ne v6, v7, :cond_3

    .line 327
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getAirMotionContext()Landroid/hardware/scontext/SContextAirMotion;

    move-result-object v0

    .line 328
    .local v0, "airMotionContext":Landroid/hardware/scontext/SContextAirMotion;
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureService:Lcom/samsung/android/app/gestureservice/IGestureService;

    invoke-interface {v6}, Lcom/samsung/android/app/gestureservice/IGestureService;->getRotation()I

    move-result v6

    iput v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    .line 329
    iget v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    if-nez v6, :cond_4

    .line 330
    iput v11, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    .line 336
    :cond_1
    :goto_0
    invoke-static {}, Lcom/samsung/android/service/gesture/GestureEvent;->obtain()Lcom/samsung/android/service/gesture/GestureEvent;

    move-result-object v1

    .line 337
    .restart local v1    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    iget-byte v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    if-ne v6, v8, :cond_6

    .line 338
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextAirMotion;->getAngle()I

    move-result v6

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mapAngleTo4DirectionGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;I)V

    .line 344
    :goto_1
    const-string v6, "IrGestureProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onSContextChanged : Received Event = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/4 v3, 0x0

    .line 346
    .local v3, "shake":Z
    iget-boolean v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mShakeStarted:Z

    if-nez v6, :cond_8

    .line 347
    iput-boolean v9, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mShakeStarted:Z

    .line 348
    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v6

    iput v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousShake:I

    .line 349
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 350
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x320

    invoke-virtual {v6, v10, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 356
    :goto_2
    iget-boolean v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mIgnoreSweep:Z

    if-nez v6, :cond_9

    .line 357
    iput-boolean v9, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mIgnoreSweep:Z

    .line 358
    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v6

    iput v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousEvent:I

    .line 359
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 360
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x1f4

    invoke-virtual {v6, v9, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 361
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v6, v1}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 369
    :cond_2
    :goto_3
    if-eqz v3, :cond_3

    .line 370
    invoke-static {}, Lcom/samsung/android/service/gesture/GestureEvent;->obtain()Lcom/samsung/android/service/gesture/GestureEvent;

    move-result-object v4

    .line 371
    .local v4, "shakeEvent":Lcom/samsung/android/service/gesture/GestureEvent;
    const/4 v6, 0x6

    invoke-virtual {v4, v6}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 372
    const-string v6, "ir_provider"

    invoke-virtual {v4, v6}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 373
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v6, v4}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 376
    .end local v0    # "airMotionContext":Landroid/hardware/scontext/SContextAirMotion;
    .end local v1    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    .end local v3    # "shake":Z
    .end local v4    # "shakeEvent":Lcom/samsung/android/service/gesture/GestureEvent;
    :cond_3
    return-void

    .line 331
    .restart local v0    # "airMotionContext":Landroid/hardware/scontext/SContextAirMotion;
    :cond_4
    iget v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    if-ne v6, v9, :cond_5

    .line 332
    const/16 v6, 0x10

    iput v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    goto :goto_0

    .line 333
    :cond_5
    iget v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    if-ne v6, v8, :cond_1

    .line 334
    const/16 v6, 0x20

    iput v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mLastOrientation:I

    goto/16 :goto_0

    .line 339
    .restart local v1    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    :cond_6
    iget-byte v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    const/4 v7, 0x4

    if-ne v6, v7, :cond_7

    .line 340
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextAirMotion;->getDirection()I

    move-result v6

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mapDirectionToGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;I)V

    goto/16 :goto_1

    .line 342
    :cond_7
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextAirMotion;->getAngle()I

    move-result v6

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mapAngleTo2DirectionGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;I)V

    goto/16 :goto_1

    .line 352
    .restart local v3    # "shake":Z
    :cond_8
    iget v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousShake:I

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->checkOppositeDirection(Lcom/samsung/android/service/gesture/GestureEvent;I)Z

    move-result v3

    .line 353
    iput-boolean v11, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mShakeStarted:Z

    .line 354
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_2

    .line 363
    :cond_9
    const/4 v5, 0x0

    .line 364
    .local v5, "skip":Z
    iget v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mPreviousEvent:I

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->checkOppositeDirection(Lcom/samsung/android/service/gesture/GestureEvent;I)Z

    move-result v5

    .line 365
    if-nez v5, :cond_2

    .line 366
    iget-object v6, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v6, v1}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    goto :goto_3
.end method

.method public serviceStatusChanged()V
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureService:Lcom/samsung/android/app/gestureservice/IGestureService;

    invoke-interface {v0}, Lcom/samsung/android/app/gestureservice/IGestureService;->getVerticalStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureService:Lcom/samsung/android/app/gestureservice/IGestureService;

    invoke-interface {v0}, Lcom/samsung/android/app/gestureservice/IGestureService;->getHorizontalStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    iget-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    add-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mGestureService:Lcom/samsung/android/app/gestureservice/IGestureService;

    invoke-interface {v0}, Lcom/samsung/android/app/gestureservice/IGestureService;->getHorizontalWaveStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 387
    iget-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    add-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mDirectionMode:B

    .line 388
    :cond_2
    return-void
.end method

.method public startService()V
    .locals 2

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mServiceStarted:Z

    if-eqz v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mContext:Landroid/content/Context;

    const-string v1, "scontext"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    sput-object v0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 129
    sget-object v0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/4 v1, 0x7

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mServiceStarted:Z

    goto :goto_0
.end method

.method public stopService()V
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mServiceStarted:Z

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    sget-object v0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    invoke-virtual {v0, p0}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;)V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/IrGestureProvider;->mServiceStarted:Z

    goto :goto_0
.end method
