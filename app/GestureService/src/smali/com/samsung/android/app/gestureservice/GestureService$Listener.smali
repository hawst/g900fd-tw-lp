.class final Lcom/samsung/android/app/gestureservice/GestureService$Listener;
.super Ljava/lang/Object;
.source "GestureService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Listener"
.end annotation


# instance fields
.field public eventType:Ljava/lang/String;

.field public pid:I

.field public provider:Ljava/lang/String;

.field public supportLandscape:Z

.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;

.field private final token:Landroid/os/IBinder;

.field public uid:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;ZII)V
    .locals 2
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "provider"    # Ljava/lang/String;
    .param p4, "eventType"    # Ljava/lang/String;
    .param p5, "supportLandscape"    # Z
    .param p6, "pid"    # I
    .param p7, "uid"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1009
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 993
    iput-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    .line 996
    iput-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    .line 998
    iput v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->pid:I

    .line 999
    iput v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->uid:I

    .line 1002
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->supportLandscape:Z

    .line 1010
    iput-object p2, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;

    .line 1011
    iput-object p3, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    .line 1012
    iput-object p4, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    .line 1013
    iput-boolean p5, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->supportLandscape:Z

    .line 1014
    iput p6, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->pid:I

    .line 1015
    iput p7, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->uid:I

    .line 1016
    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/gestureservice/GestureService$Listener;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .prologue
    .line 988
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;

    return-object v0
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .prologue
    .line 1024
    const-string v0, "GestureService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "binderDied : binder = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " provider = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->decreaseRefCountAndStopService(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3000(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)V

    .line 1026
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->clearStatusBar()V
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$3100(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 1027
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1028
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 1029
    return-void
.end method

.method public callback(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;

    .prologue
    .line 1038
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;

    invoke-static {v1}, Lcom/samsung/android/service/gesture/IGestureCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/service/gesture/IGestureCallback;

    move-result-object v0

    .line 1039
    .local v0, "callback":Lcom/samsung/android/service/gesture/IGestureCallback;
    if-eqz v0, :cond_0

    .line 1040
    invoke-interface {v0, p1}, Lcom/samsung/android/service/gesture/IGestureCallback;->gestureCallback(Lcom/samsung/android/service/gesture/GestureEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1045
    .end local v0    # "callback":Lcom/samsung/android/service/gesture/IGestureCallback;
    :cond_0
    :goto_0
    return-void

    .line 1042
    :catch_0
    move-exception v1

    goto :goto_0
.end method
