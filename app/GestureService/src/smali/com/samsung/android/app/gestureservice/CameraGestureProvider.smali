.class public Lcom/samsung/android/app/gestureservice/CameraGestureProvider;
.super Ljava/lang/Object;
.source "CameraGestureProvider.java"

# interfaces
.implements Lcom/samsung/android/app/gestureservice/GestureProviderInterface;


# static fields
.field public static final NAME:Ljava/lang/String; = "camera_provider"


# instance fields
.field private mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

.field private mImageProcessing:Lcom/samsung/android/app/gestureservice/ImageProcessing;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;)V
    .locals 0
    .param p1, "gestureEventObserver"    # Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    .line 31
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "camera_provider"

    return-object v0
.end method

.method public getSupportedGestures()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v0, "gestures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    return-object v0
.end method

.method notifyEvent(I)V
    .locals 2
    .param p1, "result"    # I

    .prologue
    .line 64
    new-instance v0, Lcom/samsung/android/service/gesture/GestureEvent;

    invoke-direct {v0}, Lcom/samsung/android/service/gesture/GestureEvent;-><init>()V

    .line 65
    .local v0, "event":Lcom/samsung/android/service/gesture/GestureEvent;
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 66
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 67
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 70
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 72
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    goto :goto_0
.end method

.method public serviceStatusChanged()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public startService()V
    .locals 3

    .prologue
    .line 48
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mImageProcessing:Lcom/samsung/android/app/gestureservice/ImageProcessing;

    if-nez v1, :cond_0

    .line 49
    new-instance v1, Lcom/samsung/android/app/gestureservice/ImageProcessing;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;-><init>(Lcom/samsung/android/app/gestureservice/CameraGestureProvider;)V

    iput-object v1, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mImageProcessing:Lcom/samsung/android/app/gestureservice/ImageProcessing;

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mImageProcessing:Lcom/samsung/android/app/gestureservice/ImageProcessing;

    invoke-virtual {v1}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->startPreview()Z

    move-result v0

    .line 53
    .local v0, "ret":Z
    if-nez v0, :cond_1

    .line 54
    const-string v1, "camera_provider"

    const-string v2, "Camera Open Error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_1
    return-void
.end method

.method public stopService()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->mImageProcessing:Lcom/samsung/android/app/gestureservice/ImageProcessing;

    invoke-virtual {v0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->stopPreview()V

    .line 61
    return-void
.end method
