.class public Lcom/samsung/android/app/gestureservice/TestGestureProvider;
.super Ljava/lang/Object;
.source "TestGestureProvider.java"

# interfaces
.implements Lcom/samsung/android/app/gestureservice/GestureProviderInterface;


# static fields
.field private static final DELAY_IN_MILISECOND:I = 0x7d0

.field public static final NAME:Ljava/lang/String; = "test_provider"


# instance fields
.field private mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

.field private mTestHandler:Landroid/os/Handler;

.field private mTestRunner:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;)V
    .locals 1
    .param p1, "gestureEventObserver"    # Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestHandler:Landroid/os/Handler;

    .line 32
    new-instance v0, Lcom/samsung/android/app/gestureservice/TestGestureProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/TestGestureProvider$1;-><init>(Lcom/samsung/android/app/gestureservice/TestGestureProvider;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestRunner:Ljava/lang/Runnable;

    .line 41
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/gestureservice/TestGestureProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/TestGestureProvider;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->notifyEvent()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/gestureservice/TestGestureProvider;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/TestGestureProvider;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestRunner:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/gestureservice/TestGestureProvider;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/TestGestureProvider;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private notifyEvent()V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/samsung/android/service/gesture/GestureEvent;

    invoke-direct {v0}, Lcom/samsung/android/service/gesture/GestureEvent;-><init>()V

    .line 68
    .local v0, "event":Lcom/samsung/android/service/gesture/GestureEvent;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mGestureEventObserver:Lcom/samsung/android/app/gestureservice/IGestureEventObserver;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/gestureservice/IGestureEventObserver;->updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V

    .line 72
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "test_provider"

    return-object v0
.end method

.method public getSupportedGestures()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v0, "gestures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    return-object v0
.end method

.method public serviceStatusChanged()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public startService()V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestRunner:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 59
    return-void
.end method

.method public stopService()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/TestGestureProvider;->mTestRunner:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 64
    return-void
.end method
