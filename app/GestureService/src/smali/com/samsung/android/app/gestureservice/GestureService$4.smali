.class Lcom/samsung/android/app/gestureservice/GestureService$4;
.super Landroid/content/BroadcastReceiver;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$4;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 262
    const-string v0, "GestureService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gloveModeChangeReceiver: onReceive - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.glove.ENABLE"

    if-ne v0, v1, :cond_1

    .line 265
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$4;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "gloveEnable"

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2102(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 266
    const-string v0, "GestureService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bGloveModeEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$4;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeEnabled:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2100(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.glove.CALL_ENABLE"

    if-ne v0, v1, :cond_2

    .line 269
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$4;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "gloveEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->enableFastGloveModeForCall(Z)Z

    goto :goto_0

    .line 270
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.glove.LOCK_ENABLE"

    if-ne v0, v1, :cond_0

    .line 271
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$4;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "gloveEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->enableFastGloveModeForLock(Z)Z

    goto :goto_0
.end method
