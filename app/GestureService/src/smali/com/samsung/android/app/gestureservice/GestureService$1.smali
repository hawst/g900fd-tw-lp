.class Lcom/samsung/android/app/gestureservice/GestureService$1;
.super Landroid/content/BroadcastReceiver;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 187
    const-string v0, "GestureService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "airGestureSettingsChangeReceiver: onReceive - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    if-ne v0, v1, :cond_1

    .line 190
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "isEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$602(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 203
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateFlags()V
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1200(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 204
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1300(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "ir_provider"

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->checkAndStartService(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1400(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)Z

    .line 206
    return-void

    .line 191
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.gesture.AIR_SCROLL_SETTINGS_CHANGED"

    if-ne v0, v1, :cond_2

    .line 192
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "isEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$702(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0

    .line 193
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.gesture.AIR_BROWSE_SETTINGS_CHANGED"

    if-ne v0, v1, :cond_3

    .line 194
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "isEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$802(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0

    .line 195
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.gesture.AIR_MOVE_SETTINGS_CHANGED"

    if-ne v0, v1, :cond_4

    .line 196
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "isEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$902(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0

    .line 197
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.gesture.AIR_PIN_SETTINGS_CHANGED"

    if-ne v0, v1, :cond_5

    .line 198
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "isEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1002(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0

    .line 199
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.gesture.AIR_CALL_ACCEPT_SETTINGS_CHANGED"

    if-ne v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService$1;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v1, "isEnable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1102(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0
.end method
