.class Lcom/samsung/android/app/gestureservice/SituationDetector$2;
.super Ljava/lang/Object;
.source "SituationDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/SituationDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/SituationDetector;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 156
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 160
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    if-ne v0, v3, :cond_6

    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$302(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$402(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v5

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$502(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 167
    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinX:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$600(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v2

    sub-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$602(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 170
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxX:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$700(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 171
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_x0:F
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v2

    sub-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$702(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 173
    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinY:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$800(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v2

    sub-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$802(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 176
    :cond_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxY:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$900(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_y0:F
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v2

    sub-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$902(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 179
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinZ:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1000(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 180
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v5

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v2

    sub-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMinZ:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1002(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 182
    :cond_5
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxZ:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1100(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 183
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v5

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$2;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACC_z0:F
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v2

    sub-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mACCMaxZ:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1102(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 186
    :cond_6
    return-void
.end method
