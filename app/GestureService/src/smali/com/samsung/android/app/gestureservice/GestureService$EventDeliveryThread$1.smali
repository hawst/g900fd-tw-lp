.class Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;
.super Landroid/os/Handler;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 137
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/service/gesture/GestureEvent;

    .line 139
    .local v1, "event":Lcom/samsung/android/service/gesture/GestureEvent;
    invoke-static {}, Lcom/samsung/android/service/gesture/GestureEvent;->obtain()Lcom/samsung/android/service/gesture/GestureEvent;

    move-result-object v4

    .line 140
    .local v4, "rotatedEvent":Lcom/samsung/android/service/gesture/GestureEvent;
    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->getProvider()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/service/gesture/GestureEvent;->setProvider(Ljava/lang/String;)V

    .line 141
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v6

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->convertGestureByOrientation(I)I
    invoke-static {v5, v6}, Lcom/samsung/android/app/gestureservice/GestureService;->access$000(Lcom/samsung/android/app/gestureservice/GestureService;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/service/gesture/GestureEvent;->setEvent(I)V

    .line 143
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_2

    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$200(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 144
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 145
    .local v3, "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v5, v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    const-string v6, "air_motion_call_accept"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 146
    move-object v0, v1

    .line 147
    .local v0, "e":Lcom/samsung/android/service/gesture/GestureEvent;
    iget-boolean v5, v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->supportLandscape:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$300(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->isScreenRotationLocked()Z
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$400(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 149
    move-object v0, v4

    .line 151
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    iget-object v6, v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->checkEventStatus(Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z
    invoke-static {v5, v0, v6}, Lcom/samsung/android/app/gestureservice/GestureService;->access$500(Lcom/samsung/android/app/gestureservice/GestureService;Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 152
    const-string v5, "GestureService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EventDeliveryThread : Send event = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-virtual {v3, v0}, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->callback(Lcom/samsung/android/service/gesture/GestureEvent;)V

    goto :goto_0

    .line 160
    .end local v0    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 161
    .restart local v3    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v5, v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->getProvider()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 162
    move-object v0, v1

    .line 163
    .restart local v0    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    iget-boolean v5, v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->supportLandscape:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$300(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->isScreenRotationLocked()Z
    invoke-static {v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$400(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 165
    move-object v0, v4

    .line 167
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread$1;->this$1:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v5, v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    iget-object v6, v3, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->checkEventStatus(Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z
    invoke-static {v5, v0, v6}, Lcom/samsung/android/app/gestureservice/GestureService;->access$500(Lcom/samsung/android/app/gestureservice/GestureService;Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 168
    const-string v5, "GestureService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EventDeliveryThread : Send event = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-virtual {v3, v0}, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->callback(Lcom/samsung/android/service/gesture/GestureEvent;)V

    goto :goto_1

    .line 174
    .end local v0    # "e":Lcom/samsung/android/service/gesture/GestureEvent;
    .end local v3    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_5
    invoke-virtual {v4}, Lcom/samsung/android/service/gesture/GestureEvent;->recycle()V

    .line 175
    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureEvent;->recycle()V

    .line 176
    return-void
.end method
