.class public Lcom/samsung/android/app/gestureservice/GestureService;
.super Landroid/app/Service;
.source "GestureService.java"

# interfaces
.implements Lcom/samsung/android/app/gestureservice/IGestureEventObserver;
.implements Lcom/samsung/android/app/gestureservice/IGestureService;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/gestureservice/GestureService$Listener;,
        Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;
    }
.end annotation


# static fields
.field private static final AIR_BROWSE_CHANGE:Ljava/lang/String; = "com.sec.gesture.AIR_BROWSE_SETTINGS_CHANGED"

.field private static final AIR_CALL_ACCEPT_CHANGE:Ljava/lang/String; = "com.sec.gesture.AIR_CALL_ACCEPT_SETTINGS_CHANGED"

.field private static final AIR_GESTURE_ACTIVE_STATUS:Ljava/lang/String; = "com.samsung.android.app.gestureservice.GESTURE_ACTIVE_STATUS"

.field private static final AIR_JUMP_CHANGE:Ljava/lang/String; = "com.sec.gesture.AIR_SCROLL_SETTINGS_CHANGED"

.field private static final AIR_MOTION_CALL_ACCEPT_EVENT:Ljava/lang/String; = "air_motion_call_accept"

.field private static final AIR_MOTION_CHANGE:Ljava/lang/String; = "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

.field private static final AIR_MOTION_JUMP_EVENT:Ljava/lang/String; = "air_motion_scroll"

.field private static final AIR_MOTION_MOVE_EVENT:Ljava/lang/String; = "air_motion_item_move"

.field private static final AIR_MOTION_PIN_EVENT:Ljava/lang/String; = "air_motion_clip"

.field private static final AIR_MOTION_TURN_EVENT:Ljava/lang/String; = "air_motion_turn"

.field private static final AIR_MOVE_CHANGE:Ljava/lang/String; = "com.sec.gesture.AIR_MOVE_SETTINGS_CHANGED"

.field private static final AIR_PIN_CHANGE:Ljava/lang/String; = "com.sec.gesture.AIR_PIN_SETTINGS_CHANGED"

.field private static final CLEAR_COVER_CHANGE:Ljava/lang/String; = "com.samsung.cover.OPEN"

.field private static final CONFIG_CHANGE:Ljava/lang/String; = "android.intent.action.CONFIGURATION_CHANGED"

.field private static final DESKCLOCK_STATUS:Ljava/lang/String; = "com.sec.android.app.clockpackage.DESKCLOCK_STATUS"

.field private static final EXTRA_DESKCLOCK_RUNNING:Ljava/lang/String; = "DeskClock.isRunning"

.field private static final EXTRA_GLANCEVIEW_RUNNING:Ljava/lang/String; = "GlanceView.isRunning"

.field private static final EXTRA_MULTIWINDOW_FREESTYLE:Ljava/lang/String; = "com.sec.android.extra.MULTIWINDOW_FREESTYLE"

.field private static final EXTRA_MULTIWINDOW_RUNNING:Ljava/lang/String; = "com.sec.android.extra.MULTIWINDOW_RUNNING"

.field private static final EXTRA_SIP_RUNNING:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final GESTURE_STATUSBAR_SLOT:Ljava/lang/String; = "gesture"

.field private static final GLANCEVIEW_STATUS:Ljava/lang/String; = "com.sec.android.app.GlanceView.STATUS"

.field private static final GLOVE_MODE_CHANGE:Ljava/lang/String; = "com.samsung.glove.ENABLE"

.field private static final GLOVE_MODE_CHANGE_CALL:Ljava/lang/String; = "com.samsung.glove.CALL_ENABLE"

.field private static final GLOVE_MODE_CHANGE_LOCK:Ljava/lang/String; = "com.samsung.glove.LOCK_ENABLE"

.field private static final GLOVE_STATUSBAR_SLOT:Ljava/lang/String; = "glove"

.field private static final IR_GESTURE_PROVIDER:Ljava/lang/String; = "ir_provider"

.field private static final MSG_TOUCH_LOCK:I = 0x65

.field private static final NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String; = "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final TAG:Ljava/lang/String; = "GestureService"

.field private static final TIMER_TOUCH_MODE:I = 0x3e8

.field private static final bSupportAirMove:Z

.field private static final bSupportLandscape:Z = true

.field private static sProvidersLoaded:Z


# instance fields
.field private airGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

.field private bAirBrowseEnabled:Z

.field private bAirCallAcceptEnabled:Z

.field private bAirJumpEnabled:Z

.field private bAirMotionEnabled:Z

.field private bAirMoveEnabled:Z

.field private bAirPin:Z

.field private bAirPinEnabled:Z

.field private bClearCoverOpened:Z

.field private bDeskLockEnable:Z

.field private bGestureServiceEnable:Z

.field private bGlanceViewEnable:Z

.field private bGloveModeEnabled:Z

.field private bGloveModeSettingEnabled:Z

.field private bGloveModeforCallEnabled:Z

.field private bGloveModeforLockEnabled:Z

.field private bHorizontal:Z

.field private bHorizontalWave:Z

.field private bIsLandscape:Z

.field private bIsScreenOn:Z

.field private bIsTouchMode:Z

.field private bLockScreenOn:Z

.field private bMultiWindowEnable:Z

.field private bSIPEnable:Z

.field private bVertical:Z

.field private clearCoverChangeReceiver:Landroid/content/BroadcastReceiver;

.field private deviceConditionChangeReceiver:Landroid/content/BroadcastReceiver;

.field private gloveModeChangeReceiver:Landroid/content/BroadcastReceiver;

.field private final mBinder:Lcom/samsung/android/service/gesture/IGestureService$Stub;

.field private mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

.field private mEventHandler:Landroid/os/Handler;

.field private mLastOrientation:I

.field private final mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/android/app/gestureservice/GestureService$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/lang/Object;

.field private final mProviderRefCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/gestureservice/GestureProviderInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final mProvidersByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/gestureservice/GestureProviderInterface;",
            ">;"
        }
    .end annotation
.end field

.field private mRunnable:Ljava/lang/Runnable;

.field private mSituationDetector:Lcom/samsung/android/app/gestureservice/SituationDetector;

.field private mStatusBar:Landroid/app/StatusBarManager;

.field private powerManager:Landroid/os/PowerManager;

.field private serviceOnOffReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_GESTURE_ENABLE_AIR_MOVE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->bSupportAirMove:Z

    .line 58
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->sProvidersLoaded:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 48
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsTouchMode:Z

    .line 51
    iput-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventHandler:Landroid/os/Handler;

    .line 53
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviders:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;

    .line 60
    iput-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    .line 96
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z

    .line 97
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z

    .line 98
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z

    .line 99
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z

    .line 100
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z

    .line 101
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    .line 102
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    .line 103
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    .line 104
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    .line 105
    iput-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    .line 106
    iput-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bClearCoverOpened:Z

    .line 107
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforCallEnabled:Z

    .line 108
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforLockEnabled:Z

    .line 109
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeSettingEnabled:Z

    .line 110
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeEnabled:Z

    .line 111
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z

    .line 112
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z

    .line 113
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bDeskLockEnable:Z

    .line 114
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z

    .line 115
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bLockScreenOn:Z

    .line 121
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z

    .line 122
    iput-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsScreenOn:Z

    .line 123
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPin:Z

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLastOrientation:I

    .line 183
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$1;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->airGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 209
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$2;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->deviceConditionChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 246
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$3;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->clearCoverChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 259
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$4;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->gloveModeChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 276
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$5;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->serviceOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 338
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$6;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mBinder:Lcom/samsung/android/service/gesture/IGestureService$Stub;

    .line 612
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$8;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mRunnable:Ljava/lang/Runnable;

    .line 988
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/gestureservice/GestureService;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->convertGestureByOrientation(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->updateFlags()V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->checkAndStartService(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsScreenOn:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bLockScreenOn:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bClearCoverOpened:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bClearCoverOpened:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeEnabled:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeEnabled:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bDeskLockEnable:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bDeskLockEnable:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/gestureservice/GestureService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->updateGestureSetting()V

    return-void
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->increaseRefCountAndStartService(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->decreaseRefCountAndStopService(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->clearStatusBar()V

    return-void
.end method

.method static synthetic access$3200(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->removeProviders()V

    return-void
.end method

.method static synthetic access$3300(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/app/gestureservice/GestureService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getProvidersLocked()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/app/gestureservice/GestureService;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->getProviderInfoLocked(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/app/gestureservice/GestureService;Landroid/view/InputEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Landroid/view/InputEvent;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->handleTspInputEvent(Landroid/view/InputEvent;)V

    return-void
.end method

.method static synthetic access$3702(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsTouchMode:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/gestureservice/GestureService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->isScreenRotationLocked()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/gestureservice/GestureService;Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Lcom/samsung/android/service/gesture/GestureEvent;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/gestureservice/GestureService;->checkEventStatus(Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z

    return p1
.end method

.method static synthetic access$702(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z

    return p1
.end method

.method static synthetic access$802(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z

    return p1
.end method

.method static synthetic access$902(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/gestureservice/GestureService;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z

    return p1
.end method

.method private addProvider(Lcom/samsung/android/app/gestureservice/GestureProviderInterface;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    .prologue
    .line 932
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 933
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 934
    return-void
.end method

.method private checkAndStartService(Ljava/lang/String;)Z
    .locals 4
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 583
    if-nez p1, :cond_0

    .line 584
    const-string v0, "GestureService"

    const-string v2, "checkAndStartService : provider is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 609
    :goto_0
    return v0

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 589
    const-string v0, "GestureService"

    const-string v2, "checkAndStartService : mProvidersByName is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 590
    goto :goto_0

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 594
    const-string v0, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkAndStartService : mProvidersByName.get("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 595
    goto :goto_0

    .line 598
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    if-nez v0, :cond_3

    .line 599
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->stopService()V

    move v0, v1

    .line 600
    goto :goto_0

    .line 603
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    if-eqz v0, :cond_5

    .line 604
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->startService()V

    .line 605
    const/4 v0, 0x1

    goto :goto_0

    .line 608
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->stopService()V

    move v0, v1

    .line 609
    goto :goto_0
.end method

.method private checkEventStatus(Lcom/samsung/android/service/gesture/GestureEvent;Ljava/lang/String;)Z
    .locals 4
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;
    .param p2, "listenerType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 639
    const/4 v0, 0x0

    .line 645
    .local v0, "result":Z
    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bClearCoverOpened:Z

    if-nez v2, :cond_1

    .line 712
    :cond_0
    :goto_0
    return v1

    .line 650
    :cond_1
    sget-boolean v2, Lcom/samsung/android/app/gestureservice/GestureService;->bSupportAirMove:Z

    if-eqz v2, :cond_3

    .line 651
    const-string v2, "air_motion_item_move"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsTouchMode:Z

    if-nez v2, :cond_0

    .line 660
    :cond_2
    const-string v2, "air_motion_call_accept"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 661
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mSituationDetector:Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-virtual {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->isMoving()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 662
    const-string v2, "GestureService"

    const-string v3, "checkEventStatus : mSituationDetector detect move over 1G"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 655
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsTouchMode:Z

    if-eqz v2, :cond_2

    goto :goto_0

    .line 665
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mSituationDetector:Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-virtual {v2}, Lcom/samsung/android/app/gestureservice/SituationDetector;->isShaking()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 666
    const-string v2, "GestureService"

    const-string v3, "checkEventStatus : mSituationDetector detect Gyro over GyroShakeDetect value"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 669
    :cond_5
    const-string v1, "GestureService"

    const-string v2, "checkEventStatus : mSituationDetector Pass"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 708
    :cond_7
    :goto_1
    :pswitch_0
    const-string v1, "air_motion_clip"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPin:Z

    if-eqz v1, :cond_8

    .line 709
    const/4 v0, 0x0

    :cond_8
    move v1, v0

    .line 712
    goto :goto_0

    .line 675
    :pswitch_1
    const-string v1, "air_motion_turn"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z

    if-eqz v1, :cond_9

    .line 676
    const/4 v0, 0x1

    goto :goto_1

    .line 677
    :cond_9
    const-string v1, "air_motion_item_move"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z

    if-eqz v1, :cond_a

    .line 678
    const/4 v0, 0x1

    goto :goto_1

    .line 679
    :cond_a
    const-string v1, "air_motion_clip"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z

    if-eqz v1, :cond_b

    .line 680
    const/4 v0, 0x1

    goto :goto_1

    .line 681
    :cond_b
    const-string v1, "air_motion_call_accept"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    if-eqz v1, :cond_c

    .line 682
    const/4 v0, 0x1

    goto :goto_1

    .line 684
    :cond_c
    const/4 v0, 0x0

    .line 686
    goto :goto_1

    .line 689
    :pswitch_2
    const-string v1, "air_motion_scroll"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z

    if-eqz v1, :cond_d

    .line 690
    const/4 v0, 0x1

    goto :goto_1

    .line 691
    :cond_d
    const-string v1, "air_motion_call_accept"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    if-eqz v1, :cond_e

    .line 692
    const/4 v0, 0x1

    goto :goto_1

    .line 694
    :cond_e
    const/4 v0, 0x0

    .line 696
    goto :goto_1

    .line 698
    :pswitch_3
    const-string v1, "air_motion_call_accept"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    if-eqz v1, :cond_7

    .line 699
    const/4 v0, 0x1

    goto :goto_1

    .line 672
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private clearGloveIcon()V
    .locals 2

    .prologue
    .line 844
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v1, "glove"

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->removeIcon(Ljava/lang/String;)V

    .line 845
    return-void
.end method

.method private clearStatusBar()V
    .locals 2

    .prologue
    .line 831
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v1, "gesture"

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->removeIcon(Ljava/lang/String;)V

    .line 832
    return-void
.end method

.method private convertGestureByOrientation(I)I
    .locals 3
    .param p1, "event"    # I

    .prologue
    .line 540
    move v0, p1

    .line 541
    .local v0, "ret":I
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getCurrentRotation()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLastOrientation:I

    .line 543
    iget v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLastOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 544
    packed-switch p1, :pswitch_data_0

    .line 579
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 546
    :pswitch_1
    const/4 v0, 0x4

    .line 547
    goto :goto_0

    .line 549
    :pswitch_2
    const/4 v0, 0x3

    .line 550
    goto :goto_0

    .line 552
    :pswitch_3
    const/4 v0, 0x1

    .line 553
    goto :goto_0

    .line 555
    :pswitch_4
    const/4 v0, 0x0

    .line 556
    goto :goto_0

    .line 560
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLastOrientation:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 561
    packed-switch p1, :pswitch_data_1

    :pswitch_5
    goto :goto_0

    .line 566
    :pswitch_6
    const/4 v0, 0x4

    .line 567
    goto :goto_0

    .line 563
    :pswitch_7
    const/4 v0, 0x3

    .line 564
    goto :goto_0

    .line 569
    :pswitch_8
    const/4 v0, 0x0

    .line 570
    goto :goto_0

    .line 572
    :pswitch_9
    const/4 v0, 0x1

    .line 573
    goto :goto_0

    .line 544
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 561
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private decreaseRefCountAndStopService(Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 860
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 861
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 862
    .local v0, "newRefCount":I
    if-gez v0, :cond_0

    .line 863
    const/4 v0, 0x0

    .line 865
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 866
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    .end local v0    # "newRefCount":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_2

    .line 870
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->stopService()V

    .line 872
    :cond_2
    return-void
.end method

.method private disableService()V
    .locals 4

    .prologue
    .line 975
    const-string v2, "GestureService"

    const-string v3, "disableService!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    .line 978
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->clearStatusBar()V

    .line 980
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 981
    .local v1, "l":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->stopService()V

    goto :goto_0

    .line 983
    .end local v1    # "l":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_0
    return-void
.end method

.method private enableFastGloveMode(Z)Z
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1113
    const-string v2, "GestureService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableFastGloveMode ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    const/4 v0, 0x1

    .line 1116
    .local v0, "ret":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "auto_adjust_touch"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeSettingEnabled:Z

    .line 1118
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeSettingEnabled:Z

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    .line 1124
    :cond_1
    :goto_0
    return v0

    .line 1120
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeSettingEnabled:Z

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    goto :goto_0
.end method

.method private enableService()V
    .locals 4

    .prologue
    .line 958
    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bLockScreenOn:Z

    if-nez v2, :cond_1

    .line 959
    const-string v2, "GestureService"

    const-string v3, "MultiWindow Active!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    :cond_0
    return-void

    .line 963
    :cond_1
    const-string v2, "GestureService"

    const-string v3, "enableService!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    .line 966
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->updateFlags()V

    .line 967
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V

    .line 969
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 970
    .local v1, "l":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->provider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->startService()V

    goto :goto_0
.end method

.method private getCurrentRotation()I
    .locals 3

    .prologue
    .line 333
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/gestureservice/GestureService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 334
    .local v1, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 335
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    return v2
.end method

.method private getProviderInfoLocked(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 885
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 886
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    .line 887
    .local v3, "p":Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 889
    .local v4, "supportedGestures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v3, :cond_1

    .line 890
    const-string v5, "name"

    invoke-interface {v3}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    invoke-interface {v3}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->getSupportedGestures()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 893
    .local v1, "gesture":Ljava/lang/Integer;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 895
    .end local v1    # "gesture":Ljava/lang/Integer;
    :cond_0
    const-string v5, "supported_gesture"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 897
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object v0
.end method

.method private getProvidersLocked()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 875
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 877
    .local v2, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 878
    .local v1, "providername":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 881
    .end local v1    # "providername":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method private handleTspInputEvent(Landroid/view/InputEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/InputEvent;

    .prologue
    const/4 v4, 0x1

    .line 620
    instance-of v2, p1, Landroid/view/MotionEvent;

    if-nez v2, :cond_1

    .line 636
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    const-string v3, "tsp_provider"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    .line 624
    .local v1, "tspProvider":Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/samsung/android/app/gestureservice/TspGestureProvider;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 625
    check-cast v0, Landroid/view/MotionEvent;

    .line 627
    .local v0, "e":Landroid/view/MotionEvent;
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 628
    :cond_2
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsTouchMode:Z

    .line 629
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 630
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 634
    :cond_3
    :goto_1
    check-cast v1, Lcom/samsung/android/app/gestureservice/TspGestureProvider;

    .end local v1    # "tspProvider":Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/gestureservice/TspGestureProvider;->onInputEvent(Landroid/view/InputEvent;)V

    goto :goto_0

    .line 631
    .restart local v1    # "tspProvider":Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
    :cond_4
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 632
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsTouchMode:Z

    goto :goto_1
.end method

.method private increaseRefCountAndStartService(Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 848
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 849
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/GestureService;->checkAndStartService(Ljava/lang/String;)Z

    .line 857
    return-void

    .line 851
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 852
    .local v0, "newRefCount":I
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 853
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviderRefCount:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private initialize()V
    .locals 0

    .prologue
    .line 901
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->loadProviders()V

    .line 902
    return-void
.end method

.method private isFlagsOn()Z
    .locals 1

    .prologue
    .line 716
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPin:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScreenRotationLocked()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 536
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private loadProviders()V
    .locals 2

    .prologue
    .line 905
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 906
    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->sProvidersLoaded:Z

    if-eqz v0, :cond_0

    .line 907
    monitor-exit v1

    .line 913
    :goto_0
    return-void

    .line 910
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->loadProvidersLocked()V

    .line 911
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->sProvidersLoaded:Z

    .line 912
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private loadProvidersLocked()V
    .locals 6

    .prologue
    .line 916
    const-string v4, "GestureService"

    const-string v5, "loadProvidersLocked"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    new-instance v0, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;-><init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;)V

    .line 919
    .local v0, "cameraProvider":Lcom/samsung/android/app/gestureservice/CameraGestureProvider;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->addProvider(Lcom/samsung/android/app/gestureservice/GestureProviderInterface;)V

    .line 921
    new-instance v2, Lcom/samsung/android/app/gestureservice/IrGestureProvider;

    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, p0, v4, p0}, Lcom/samsung/android/app/gestureservice/IrGestureProvider;-><init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;Landroid/content/Context;Lcom/samsung/android/app/gestureservice/IGestureService;)V

    .line 922
    .local v2, "irProvider":Lcom/samsung/android/app/gestureservice/IrGestureProvider;
    invoke-direct {p0, v2}, Lcom/samsung/android/app/gestureservice/GestureService;->addProvider(Lcom/samsung/android/app/gestureservice/GestureProviderInterface;)V

    .line 924
    new-instance v3, Lcom/samsung/android/app/gestureservice/TspGestureProvider;

    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/app/gestureservice/TspGestureProvider;-><init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;Landroid/content/Context;)V

    .line 925
    .local v3, "tspProvider":Lcom/samsung/android/app/gestureservice/TspGestureProvider;
    invoke-direct {p0, v3}, Lcom/samsung/android/app/gestureservice/GestureService;->addProvider(Lcom/samsung/android/app/gestureservice/GestureProviderInterface;)V

    .line 927
    new-instance v1, Lcom/samsung/android/app/gestureservice/TestGestureProvider;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/gestureservice/TestGestureProvider;-><init>(Lcom/samsung/android/app/gestureservice/IGestureEventObserver;)V

    .line 928
    .local v1, "dummyProvider":Lcom/samsung/android/app/gestureservice/TestGestureProvider;
    invoke-direct {p0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->addProvider(Lcom/samsung/android/app/gestureservice/GestureProviderInterface;)V

    .line 929
    return-void
.end method

.method private removeProvider(Lcom/samsung/android/app/gestureservice/GestureProviderInterface;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    .prologue
    .line 953
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 954
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 955
    return-void
.end method

.method private removeProviders()V
    .locals 2

    .prologue
    .line 937
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 938
    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->sProvidersLoaded:Z

    if-eqz v0, :cond_0

    .line 939
    monitor-exit v1

    .line 945
    :goto_0
    return-void

    .line 942
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->removeProvidersLocked()V

    .line 943
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->sProvidersLoaded:Z

    .line 944
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private removeProvidersLocked()V
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 949
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProvidersByName:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 950
    return-void
.end method

.method private updateFlags()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 720
    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    if-nez v3, :cond_0

    .line 769
    :goto_0
    return-void

    .line 723
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    .line 724
    iput-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    .line 725
    iput-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    .line 726
    iput-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPin:Z

    .line 728
    const-string v3, "GestureService"

    const-string v6, "updateFlags started"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 730
    .local v1, "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    const-string v6, "air_motion_turn"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z

    if-eqz v3, :cond_2

    .line 731
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    goto :goto_1

    .line 734
    :cond_2
    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    const-string v6, "air_motion_item_move"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z

    if-eqz v3, :cond_3

    .line 735
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    goto :goto_1

    .line 738
    :cond_3
    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    const-string v6, "air_motion_clip"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z

    if-eqz v3, :cond_4

    .line 739
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    .line 740
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPin:Z

    goto :goto_1

    .line 743
    :cond_4
    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    const-string v6, "air_motion_call_accept"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    if-eqz v3, :cond_6

    .line 744
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    .line 754
    .end local v1    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_5
    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    if-eqz v3, :cond_7

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z

    if-eqz v3, :cond_7

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    .line 755
    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    if-eqz v3, :cond_8

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z

    if-eqz v3, :cond_8

    :goto_3
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    .line 757
    const-string v3, "GestureService"

    const-string v4, "================== GestureService Listener List =================="

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;

    .line 759
    .restart local v1    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    const-string v3, "GestureService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "listener type :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " pid : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->pid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uid : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->uid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Binder : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService$Listener;->token:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->access$2700(Lcom/samsung/android/app/gestureservice/GestureService$Listener;)Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 748
    :cond_6
    iget-object v3, v1, Lcom/samsung/android/app/gestureservice/GestureService$Listener;->eventType:Ljava/lang/String;

    const-string v6, "air_motion_scroll"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z

    if-eqz v3, :cond_1

    .line 749
    iput-boolean v4, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    goto/16 :goto_1

    .end local v1    # "listener":Lcom/samsung/android/app/gestureservice/GestureService$Listener;
    :cond_7
    move v3, v5

    .line 754
    goto :goto_2

    :cond_8
    move v4, v5

    .line 755
    goto :goto_3

    .line 762
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;

    .line 763
    .local v2, "p":Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
    invoke-interface {v2}, Lcom/samsung/android/app/gestureservice/GestureProviderInterface;->serviceStatusChanged()V

    goto :goto_5

    .line 766
    .end local v2    # "p":Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
    :cond_a
    const-string v3, "GestureService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bVertical = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    const-string v3, "GestureService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bHorizontal = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const-string v3, "GestureService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bHorizontalWave = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private updateGestureSetting()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1067
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "air_motion_engine"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z

    .line 1068
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "air_motion_scroll"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z

    .line 1069
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "air_motion_turn"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z

    .line 1070
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "air_motion_item_move"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z

    .line 1071
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "air_motion_clip"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z

    .line 1072
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "air_motion_call_accept"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    .line 1073
    const-string v0, "GestureService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateGestureSettingValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMotionEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirJumpEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirBrowseEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirMoveEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPinEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirCallAcceptEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    return-void

    :cond_0
    move v0, v2

    .line 1067
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1068
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1069
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 1070
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1071
    goto :goto_4

    :cond_5
    move v1, v2

    .line 1072
    goto :goto_5
.end method

.method private updateGloveIcon()V
    .locals 5

    .prologue
    .line 835
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeEnabled:Z

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v1, "glove"

    const v2, 0x7f020004

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    .line 837
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v1, "glove"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/StatusBarManager;->setIconVisibility(Ljava/lang/String;Z)V

    .line 841
    :goto_0
    return-void

    .line 839
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->clearGloveIcon()V

    goto :goto_0
.end method

.method private updateStatusBar()V
    .locals 7

    .prologue
    const v3, 0x7f020001

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 792
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGestureServiceEnable:Z

    if-nez v1, :cond_0

    .line 828
    :goto_0
    return-void

    .line 795
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bClearCoverOpened:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsScreenOn:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->isFlagsOn()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 796
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    if-eqz v1, :cond_1

    .line 798
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    const v3, 0x7f020003

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    .line 799
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mSituationDetector:Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-virtual {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->startDetection()V

    .line 817
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    invoke-virtual {v1, v2, v6}, Landroid/app/StatusBarManager;->setIconVisibility(Ljava/lang/String;Z)V

    .line 818
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.app.gestureservice.GESTURE_ACTIVE_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 819
    .local v0, "gestureActiveIntent":Landroid/content/Intent;
    const-string v1, "isActive"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 820
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 800
    .end local v0    # "gestureActiveIntent":Landroid/content/Intent;
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    if-eqz v1, :cond_2

    .line 802
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    const/high16 v3, 0x7f020000

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1

    .line 803
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    if-eqz v1, :cond_3

    .line 805
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1

    .line 806
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    if-nez v1, :cond_4

    .line 808
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    const v3, 0x7f020002

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1

    .line 809
    :cond_4
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bAirPin:Z

    if-eqz v1, :cond_5

    .line 811
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1

    .line 814
    :cond_5
    const-string v1, "GestureService"

    const-string v2, "[ERROR] updateStatusBar : this line cannot be performed because isFlagsOn() checks all flags."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 822
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    const-string v2, "gesture"

    invoke-virtual {v1, v2, v4}, Landroid/app/StatusBarManager;->setIconVisibility(Ljava/lang/String;Z)V

    .line 823
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mSituationDetector:Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-virtual {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->stopDetection()V

    .line 824
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.app.gestureservice.GESTURE_ACTIVE_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 825
    .restart local v0    # "gestureActiveIntent":Landroid/content/Intent;
    const-string v1, "isActive"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 826
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public enableFastGloveModeForCall(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 1095
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableFastGloveModeForCall ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    const/4 v0, 0x1

    .line 1098
    .local v0, "ret":Z
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforCallEnabled:Z

    .line 1099
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bGloveModeCallEnabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforCallEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    if-eqz p1, :cond_1

    .line 1102
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->enableFastGloveMode(Z)Z

    move-result v0

    .line 1109
    .end local v0    # "ret":Z
    :cond_0
    :goto_0
    return v0

    .line 1104
    .restart local v0    # "ret":Z
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforLockEnabled:Z

    if-nez v1, :cond_0

    .line 1105
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->enableFastGloveMode(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public enableFastGloveModeForLock(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 1077
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableFastGloveModeForLock ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    const/4 v0, 0x1

    .line 1080
    .local v0, "ret":Z
    iput-boolean p1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforLockEnabled:Z

    .line 1081
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bGloveModeforLockEnabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforLockEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    if-eqz p1, :cond_1

    .line 1084
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->enableFastGloveMode(Z)Z

    move-result v0

    .line 1091
    .end local v0    # "ret":Z
    :cond_0
    :goto_0
    return v0

    .line 1086
    .restart local v0    # "ret":Z
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bGloveModeforCallEnabled:Z

    if-nez v1, :cond_0

    .line 1087
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->enableFastGloveMode(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public getHorizontalStatus()Z
    .locals 1

    .prologue
    .line 778
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontal:Z

    return v0
.end method

.method public getHorizontalWaveStatus()Z
    .locals 1

    .prologue
    .line 783
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bHorizontalWave:Z

    return v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 789
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getCurrentRotation()I

    move-result v0

    return v0
.end method

.method public getVerticalStatus()Z
    .locals 1

    .prologue
    .line 773
    iget-boolean v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bVertical:Z

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 513
    const-string v0, "GestureService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->powerManager:Landroid/os/PowerManager;

    .line 517
    new-instance v0, Lcom/samsung/android/app/gestureservice/GestureService$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/gestureservice/GestureService$7;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventHandler:Landroid/os/Handler;

    .line 527
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 528
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/samsung/android/app/gestureservice/GestureService;->sProvidersLoaded:Z

    .line 529
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->initialize()V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mBinder:Lcom/samsung/android/service/gesture/IGestureService$Stub;

    return-object v0

    .line 529
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 446
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 448
    const-string v5, "statusbar"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/gestureservice/GestureService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/StatusBarManager;

    iput-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mStatusBar:Landroid/app/StatusBarManager;

    .line 449
    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    const/4 v5, 0x1

    :goto_0
    iput-boolean v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z

    .line 451
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->updateGestureSetting()V

    .line 453
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 454
    .local v0, "airGestureSettingsChangeFilter":Landroid/content/IntentFilter;
    const-string v5, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 455
    const-string v5, "com.sec.gesture.AIR_SCROLL_SETTINGS_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 456
    const-string v5, "com.sec.gesture.AIR_BROWSE_SETTINGS_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 457
    const-string v5, "com.sec.gesture.AIR_MOVE_SETTINGS_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 458
    const-string v5, "com.sec.gesture.AIR_PIN_SETTINGS_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 459
    const-string v5, "com.sec.gesture.AIR_CALL_ACCEPT_SETTINGS_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 460
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->airGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 462
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 463
    .local v2, "deviceConditionChangeFilter":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 464
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 465
    const-string v5, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 466
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->deviceConditionChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v2}, Lcom/samsung/android/app/gestureservice/GestureService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 468
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 469
    .local v1, "clearCoverChangeFilter":Landroid/content/IntentFilter;
    const-string v5, "com.samsung.cover.OPEN"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 470
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->clearCoverChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v1}, Lcom/samsung/android/app/gestureservice/GestureService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 472
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 473
    .local v3, "gloveModeChangeFilter":Landroid/content/IntentFilter;
    const-string v5, "com.samsung.glove.ENABLE"

    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 474
    const-string v5, "com.samsung.glove.CALL_ENABLE"

    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 475
    const-string v5, "com.samsung.glove.LOCK_ENABLE"

    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 476
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->gloveModeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v3}, Lcom/samsung/android/app/gestureservice/GestureService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 478
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 479
    .local v4, "serviceOnOffFilter":Landroid/content/IntentFilter;
    const-string v5, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 480
    const-string v5, "ResponseAxT9Info"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 481
    const-string v5, "com.sec.android.app.GlanceView.STATUS"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 482
    const-string v5, "com.sec.android.app.clockpackage.DESKCLOCK_STATUS"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 483
    const-string v5, "android.intent.action.USER_PRESENT"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 484
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->serviceOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v4}, Lcom/samsung/android/app/gestureservice/GestureService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 486
    new-instance v5, Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-virtual {p0}, Lcom/samsung/android/app/gestureservice/GestureService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/app/gestureservice/SituationDetector;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mSituationDetector:Lcom/samsung/android/app/gestureservice/SituationDetector;

    .line 488
    new-instance v5, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;-><init>(Lcom/samsung/android/app/gestureservice/GestureService;)V

    iput-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    .line 489
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    const-string v6, "EventDeliveryThread"

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->setName(Ljava/lang/String;)V

    .line 490
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    invoke-virtual {v5}, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->start()V

    .line 492
    return-void

    .line 449
    .end local v0    # "airGestureSettingsChangeFilter":Landroid/content/IntentFilter;
    .end local v1    # "clearCoverChangeFilter":Landroid/content/IntentFilter;
    .end local v2    # "deviceConditionChangeFilter":Landroid/content/IntentFilter;
    .end local v3    # "gloveModeChangeFilter":Landroid/content/IntentFilter;
    .end local v4    # "serviceOnOffFilter":Landroid/content/IntentFilter;
    :cond_0
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->airGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 497
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->deviceConditionChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->clearCoverChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->gloveModeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 500
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->serviceOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/gestureservice/GestureService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 502
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v0, v0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->mLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v0, v0, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->mLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 508
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 509
    return-void
.end method

.method public updateEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;

    .prologue
    const/4 v4, 0x0

    .line 1050
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1052
    .local v0, "msgEvent":Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->what:I

    .line 1053
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1055
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateEvent : event = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->powerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/PowerManager;->userActivity(JZ)V

    .line 1062
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService;->mEventDeliveryThread:Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;

    iget-object v1, v1, Lcom/samsung/android/app/gestureservice/GestureService$EventDeliveryThread;->mEventDeliveryHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1064
    return-void
.end method
