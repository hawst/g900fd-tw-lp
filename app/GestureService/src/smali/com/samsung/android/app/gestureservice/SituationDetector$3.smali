.class Lcom/samsung/android/app/gestureservice/SituationDetector$3;
.super Ljava/lang/Object;
.source "SituationDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/SituationDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/SituationDetector;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 194
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 198
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 199
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_x0:F
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1200(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_y0:F
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1300(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_z0:F
    invoke-static {v0}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1400(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_x0:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1202(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_y0:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1302(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 202
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYRO_z0:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1402(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 205
    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinX:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1500(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1502(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 208
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxX:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1600(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 209
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1602(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 211
    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinY:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1700(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 212
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1702(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 214
    :cond_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxY:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1800(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 215
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1802(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 217
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1900(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMinZ:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$1902(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 220
    :cond_5
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    # getter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$2000(Lcom/samsung/android/app/gestureservice/SituationDetector;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/SituationDetector$3;->this$0:Lcom/samsung/android/app/gestureservice/SituationDetector;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    # setter for: Lcom/samsung/android/app/gestureservice/SituationDetector;->mGYROMaxZ:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/gestureservice/SituationDetector;->access$2002(Lcom/samsung/android/app/gestureservice/SituationDetector;F)F

    .line 224
    :cond_6
    return-void
.end method
