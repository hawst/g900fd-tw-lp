.class Lcom/samsung/android/app/gestureservice/GestureService$2;
.super Landroid/content/BroadcastReceiver;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 213
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deviceConditionChangeReceiver: onReceive - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const-string v1, "keyguard"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 216
    .local v0, "mKeyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    if-ne v1, v2, :cond_0

    .line 217
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-virtual {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v4, :cond_2

    .line 218
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z
    invoke-static {v1, v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$302(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 224
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 225
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bIsScreenOn:Z
    invoke-static {v1, v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1502(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 242
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->updateStatusBar()V
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1300(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 243
    return-void

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-virtual {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 220
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bIsLandscape:Z
    invoke-static {v1, v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$302(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0

    .line 226
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bIsScreenOn:Z
    invoke-static {v1, v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1502(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 228
    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1600(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 230
    const-string v1, "GestureService"

    const-string v2, "isKeyguardLocked is true!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bLockScreenOn:Z
    invoke-static {v1, v4}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1702(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 232
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_1

    .line 233
    :cond_4
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1600(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 234
    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v1}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 235
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isKeyguardLocked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const-string v1, "GestureService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bGlanceViewEnable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/GestureService$2;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z
    invoke-static {v3}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1600(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 238
    :cond_5
    const-string v1, "GestureService"

    const-string v2, "isKeyguardLocked is false!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
