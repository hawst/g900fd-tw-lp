.class public Lcom/samsung/android/app/gestureservice/ImageProcessing;
.super Ljava/lang/Object;
.source "ImageProcessing.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# static fields
.field public static final COVER:I = 0x1

.field private static final DEBUG_HEIGHT:I = 0x1e

.field private static final DEBUG_WIDTH:I = 0x28

.field private static final HEIGHT:I = 0xf0

.field public static final LEFT:I = 0x2

.field public static final NONE:I = 0x0

.field private static final RESIZE_HEIGHT:I = 0x1e

.field private static final RESIZE_WIDTH:I = 0x28

.field public static final RIGHT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ImageProcessing"

.field public static final WAVE:I = 0x4

.field private static final WIDTH:I = 0x140


# instance fields
.field private final binary_para:I

.field mAcb:Ljava/lang/reflect/Method;

.field mArglist:[Ljava/lang/Object;

.field private mCameraProvider:Lcom/samsung/android/app/gestureservice/CameraGestureProvider;

.field public m_Camera:Landroid/hardware/Camera;

.field private m_VipImageProcessingJniLib:Lcom/samsung/imageprocessing/VipImageProcessingJniLib;

.field private m_dSaveTime:D

.field private m_nFcount:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/CameraGestureProvider;)V
    .locals 3
    .param p1, "cameraProvider"    # Lcom/samsung/android/app/gestureservice/CameraGestureProvider;

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v2, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_nFcount:I

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_dSaveTime:D

    .line 35
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->binary_para:I

    .line 36
    iput-object v2, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mCameraProvider:Lcom/samsung/android/app/gestureservice/CameraGestureProvider;

    .line 39
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mCameraProvider:Lcom/samsung/android/app/gestureservice/CameraGestureProvider;

    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->prepareCamera()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    const-string v0, "ImageProcessing"

    const-string v1, "Facing Camera Open failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_0
    return-void
.end method

.method private addCallbackBuffer([B)V
    .locals 3
    .param p1, "b"    # [B

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mArglist:[Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->initForACB()V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mArglist:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mAcb:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mArglist:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initForACB()V
    .locals 4

    .prologue
    .line 146
    :try_start_0
    const-string v2, "android.hardware.Camera"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 149
    .local v0, "mC":Ljava/lang/Class;
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Class;

    .line 150
    .local v1, "mPartypes":[Ljava/lang/Class;
    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [B

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v1, v2

    .line 151
    const-string v2, "addCallbackBuffer"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mAcb:Ljava/lang/reflect/Method;

    .line 153
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iput-object v2, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mArglist:[Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    .end local v0    # "mC":Ljava/lang/Class;
    .end local v1    # "mPartypes":[Ljava/lang/Class;
    :goto_0
    return-void

    .line 154
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private prepareCamera()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 47
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    .line 49
    .local v0, "CameraConut":I
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 51
    .local v1, "cameraInfo":Landroid/hardware/Camera$CameraInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 52
    invoke-static {v2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 53
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v3, v4, :cond_1

    .line 54
    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    .line 55
    const-string v4, "ImageProcessing"

    const-string v5, "Facing Camera opened"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    if-eqz v4, :cond_2

    .line 61
    const-string v4, "ImageProcessing"

    const-string v5, "setPreviewCallback"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v4, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v4, p0}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 67
    :goto_1
    return v3

    .line 51
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private setPreviewCallbackWithBuffer()V
    .locals 7

    .prologue
    .line 176
    :try_start_0
    const-string v5, "android.hardware.Camera"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 178
    .local v1, "c":Ljava/lang/Class;
    const/4 v4, 0x0

    .line 179
    .local v4, "spcwb":Ljava/lang/reflect/Method;
    invoke-virtual {v1}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    .line 180
    .local v3, "m":[Ljava/lang/reflect/Method;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    .line 181
    aget-object v5, v3, v2

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "setPreviewCallbackWithBuffer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 182
    aget-object v4, v3, v2

    .line 186
    :cond_0
    if-eqz v4, :cond_1

    .line 187
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/Object;

    .line 188
    .local v0, "arglist":[Ljava/lang/Object;
    const/4 v5, 0x0

    aput-object p0, v0, v5

    .line 189
    iget-object v5, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v4, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .end local v0    # "arglist":[Ljava/lang/Object;
    .end local v1    # "c":Ljava/lang/Class;
    .end local v2    # "i":I
    .end local v3    # "m":[Ljava/lang/reflect/Method;
    .end local v4    # "spcwb":Ljava/lang/reflect/Method;
    :cond_1
    :goto_1
    return-void

    .line 180
    .restart local v1    # "c":Ljava/lang/Class;
    .restart local v2    # "i":I
    .restart local v3    # "m":[Ljava/lang/reflect/Method;
    .restart local v4    # "spcwb":Ljava/lang/reflect/Method;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 192
    .end local v1    # "c":Ljava/lang/Class;
    .end local v2    # "i":I
    .end local v3    # "m":[Ljava/lang/reflect/Method;
    .end local v4    # "spcwb":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 7
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 198
    iget v3, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_nFcount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_nFcount:I

    .line 199
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    long-to-double v3, v3

    iget-wide v5, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_dSaveTime:D

    sub-double v0, v3, v5

    .line 200
    .local v0, "ms":D
    const-wide v3, 0x408f400000000000L    # 1000.0

    cmpl-double v3, v0, v3

    if-lez v3, :cond_0

    .line 201
    const-string v3, "ImageProcessing"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fps:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_nFcount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_nFcount:I

    .line 203
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    long-to-double v3, v3

    iput-wide v3, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_dSaveTime:D

    .line 206
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_VipImageProcessingJniLib:Lcom/samsung/imageprocessing/VipImageProcessingJniLib;

    const/4 v4, 0x1

    const/16 v5, 0xa

    invoke-virtual {v3, p1, v4, v5}, Lcom/samsung/imageprocessing/VipImageProcessingJniLib;->FrameProcess([BII)I

    move-result v2

    .line 208
    .local v2, "nIsDetected":I
    if-eqz v2, :cond_1

    .line 209
    iget-object v3, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->mCameraProvider:Lcom/samsung/android/app/gestureservice/CameraGestureProvider;

    invoke-virtual {v3, v2}, Lcom/samsung/android/app/gestureservice/CameraGestureProvider;->notifyEvent(I)V

    .line 211
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->addCallbackBuffer([B)V

    .line 212
    return-void
.end method

.method public startPreview()Z
    .locals 17

    .prologue
    .line 71
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    if-nez v1, :cond_0

    .line 72
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->prepareCamera()Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    const-string v1, "ImageProcessing"

    const-string v2, "Facing Camera Open failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v1, 0x0

    .line 137
    :goto_0
    return v1

    .line 78
    :cond_0
    const/4 v8, 0x0

    .line 79
    .local v8, "Parameters":Landroid/hardware/Camera$Parameters;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v8

    .line 81
    const-string v1, "ImageProcessing"

    const-string v2, "Front Camera Size Check!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v10

    .line 83
    .local v10, "Sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const/4 v9, 0x0

    .line 85
    .local v9, "SizeFinded":Z
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    if-ge v15, v1, :cond_1

    .line 86
    invoke-interface {v10, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/hardware/Camera$Size;

    .line 87
    .local v14, "getlist":Landroid/hardware/Camera$Size;
    iget v1, v14, Landroid/hardware/Camera$Size;->width:I

    const/16 v2, 0x140

    if-ne v1, v2, :cond_2

    iget v1, v14, Landroid/hardware/Camera$Size;->height:I

    const/16 v2, 0xf0

    if-ne v1, v2, :cond_2

    .line 88
    const/4 v9, 0x1

    .line 89
    const-string v1, "ImageProcessing"

    const-string v2, "1 m_DemoActivity.m_PV.nFrameWidth = 320"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v1, "ImageProcessing"

    const-string v2, "1 m_DemoActivity.m_PV.nFrameHeigh = 240"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const-string v1, "ImageProcessing"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1 getlist.width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v14, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v1, "ImageProcessing"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1 getlist.height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v14, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    .end local v14    # "getlist":Landroid/hardware/Camera$Size;
    :cond_1
    if-eqz v9, :cond_3

    .line 103
    const/16 v1, 0x140

    const/16 v2, 0xf0

    invoke-virtual {v8, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 110
    :try_start_0
    new-instance v1, Lcom/samsung/imageprocessing/VipImageProcessingJniLib;

    invoke-direct {v1}, Lcom/samsung/imageprocessing/VipImageProcessingJniLib;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_VipImageProcessingJniLib:Lcom/samsung/imageprocessing/VipImageProcessingJniLib;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    const-string v1, "ImageProcessing"

    const-string v2, "Library Opening"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_VipImageProcessingJniLib:Lcom/samsung/imageprocessing/VipImageProcessingJniLib;

    const/16 v2, 0x140

    const/16 v3, 0xf0

    const/16 v4, 0x28

    const/16 v5, 0x1e

    const/16 v6, 0x28

    const/16 v7, 0x1e

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/imageprocessing/VipImageProcessingJniLib;->OpenImageProcessingEngine(IIIIII)I

    .line 120
    const-string v1, "ImageProcessing"

    const-string v2, "Library Opened"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    new-instance v16, Landroid/graphics/PixelFormat;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/PixelFormat;-><init>()V

    .line 123
    .local v16, "p":Landroid/graphics/PixelFormat;
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v1

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Landroid/graphics/PixelFormat;->getPixelFormatInfo(ILandroid/graphics/PixelFormat;)V

    .line 124
    const v11, 0x1c200

    .line 125
    .local v11, "bufSize":I
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->initForACB()V

    .line 126
    new-array v12, v11, [B

    .line 127
    .local v12, "buffer":[B
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->addCallbackBuffer([B)V

    .line 128
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/gestureservice/ImageProcessing;->setPreviewCallbackWithBuffer()V

    .line 131
    const/16 v1, 0x7530

    const/16 v2, 0x7530

    invoke-virtual {v8, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 133
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v1, v8}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 134
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    .line 135
    const-string v1, "ImageProcessing"

    const-string v2, "Camera Callback Started"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 95
    .end local v11    # "bufSize":I
    .end local v12    # "buffer":[B
    .end local v16    # "p":Landroid/graphics/PixelFormat;
    .restart local v14    # "getlist":Landroid/hardware/Camera$Size;
    :cond_2
    const-string v1, "ImageProcessing"

    const-string v2, "2 m_DemoActivity.m_PV.nFrameWidth = 320"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v1, "ImageProcessing"

    const-string v2, "2 m_DemoActivity.m_PV.nFrameHeigh = 240"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v1, "ImageProcessing"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2 getlist.width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v14, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string v1, "ImageProcessing"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2 getlist.height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v14, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 105
    .end local v14    # "getlist":Landroid/hardware/Camera$Size;
    :cond_3
    const-string v1, "ImageProcessing"

    const-string v2, "Front Camera Preview Size Error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 111
    :catch_0
    move-exception v13

    .line 112
    .local v13, "e":Ljava/lang/Exception;
    const-string v1, "ImageProcessing"

    const-string v2, "Library Link Error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 114
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public stopPreview()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_VipImageProcessingJniLib:Lcom/samsung/imageprocessing/VipImageProcessingJniLib;

    invoke-virtual {v0}, Lcom/samsung/imageprocessing/VipImageProcessingJniLib;->CloseImageProcessingEngine()I

    .line 219
    iget-object v0, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 220
    iput-object v1, p0, Lcom/samsung/android/app/gestureservice/ImageProcessing;->m_Camera:Landroid/hardware/Camera;

    .line 222
    :cond_0
    return-void
.end method
