.class Lcom/samsung/android/app/gestureservice/GestureService$5;
.super Landroid/content/BroadcastReceiver;
.source "GestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/gestureservice/GestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/gestureservice/GestureService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/gestureservice/GestureService;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 279
    const-string v2, "GestureService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "serviceOnOffReceiver: onReceive - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    if-ne v2, v3, :cond_3

    .line 282
    const-string v2, "com.sec.android.extra.MULTIWINDOW_RUNNING"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 283
    .local v1, "bMultiWindowRunning":Z
    const-string v2, "com.sec.android.extra.MULTIWINDOW_FREESTYLE"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 285
    .local v0, "bMultiWindowCascade":Z
    if-ne v1, v6, :cond_1

    if-nez v0, :cond_1

    .line 286
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z
    invoke-static {v2, v6}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2202(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 291
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2200(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 292
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    .line 329
    .end local v0    # "bMultiWindowCascade":Z
    .end local v1    # "bMultiWindowRunning":Z
    :cond_0
    :goto_1
    return-void

    .line 288
    .restart local v0    # "bMultiWindowCascade":Z
    .restart local v1    # "bMultiWindowRunning":Z
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z
    invoke-static {v2, v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2202(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    goto :goto_0

    .line 295
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_1

    .line 297
    .end local v0    # "bMultiWindowCascade":Z
    .end local v1    # "bMultiWindowRunning":Z
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ResponseAxT9Info"

    if-ne v2, v3, :cond_5

    .line 298
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v3, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z
    invoke-static {v2, v3}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2302(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 299
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2300(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 300
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_1

    .line 303
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_1

    .line 305
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.app.GlanceView.STATUS"

    if-ne v2, v3, :cond_7

    .line 306
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v3, "GlanceView.isRunning"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z
    invoke-static {v2, v3}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1602(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 307
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bGlanceViewEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1600(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 308
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_1

    .line 311
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto :goto_1

    .line 313
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.app.clockpackage.DESKCLOCK_STATUS"

    if-ne v2, v3, :cond_b

    .line 314
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    const-string v3, "DeskClock.isRunning"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bDeskLockEnable:Z
    invoke-static {v2, v3}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2402(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 315
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bDeskLockEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2400(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 316
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2200(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2300(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 317
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto/16 :goto_1

    .line 319
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->enableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1800(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto/16 :goto_1

    .line 322
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto/16 :goto_1

    .line 324
    :cond_b
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.USER_PRESENT"

    if-ne v2, v3, :cond_0

    .line 325
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # setter for: Lcom/samsung/android/app/gestureservice/GestureService;->bLockScreenOn:Z
    invoke-static {v2, v5}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1702(Lcom/samsung/android/app/gestureservice/GestureService;Z)Z

    .line 326
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bMultiWindowEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2200(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # getter for: Lcom/samsung/android/app/gestureservice/GestureService;->bSIPEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$2300(Lcom/samsung/android/app/gestureservice/GestureService;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    :cond_c
    iget-object v2, p0, Lcom/samsung/android/app/gestureservice/GestureService$5;->this$0:Lcom/samsung/android/app/gestureservice/GestureService;

    # invokes: Lcom/samsung/android/app/gestureservice/GestureService;->disableService()V
    invoke-static {v2}, Lcom/samsung/android/app/gestureservice/GestureService;->access$1900(Lcom/samsung/android/app/gestureservice/GestureService;)V

    goto/16 :goto_1
.end method
