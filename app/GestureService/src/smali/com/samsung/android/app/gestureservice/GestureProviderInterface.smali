.class public interface abstract Lcom/samsung/android/app/gestureservice/GestureProviderInterface;
.super Ljava/lang/Object;
.source "GestureProviderInterface.java"


# virtual methods
.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSupportedGestures()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract serviceStatusChanged()V
.end method

.method public abstract startService()V
.end method

.method public abstract stopService()V
.end method
