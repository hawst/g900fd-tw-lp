.class public Lcom/samsung/imageprocessing/VipImageProcessingJniLib;
.super Ljava/lang/Object;
.source "VipImageProcessingJniLib.java"


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, "VIPImageProcessing"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 8
    return-void
.end method


# virtual methods
.method public native CloseImageProcessingEngine()I
.end method

.method public native DebugFrame(I)[I
.end method

.method public native FrameProcess([BII)I
.end method

.method public native OpenImageProcessingEngine(IIIIII)I
.end method
