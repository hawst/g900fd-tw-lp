.class Lcom/samsung/SMT/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Lcom/samsung/SMT/z;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Ljava/util/Locale;

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    const/4 v4, 0x7

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "%s (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/samsung/SMT/z;Lcom/samsung/SMT/z;)I
    .locals 3

    invoke-static {}, Lcom/samsung/SMT/DownloadList;->a()Ljava/text/Collator;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/samsung/SMT/k;->a(Lcom/samsung/SMT/z;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/samsung/SMT/k;->a(Lcom/samsung/SMT/z;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/samsung/SMT/z;

    check-cast p2, Lcom/samsung/SMT/z;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/SMT/k;->a(Lcom/samsung/SMT/z;Lcom/samsung/SMT/z;)I

    move-result v0

    return v0
.end method
