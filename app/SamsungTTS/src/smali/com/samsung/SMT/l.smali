.class public Lcom/samsung/SMT/l;
.super Landroid/preference/PreferenceFragment;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/samsung/SMT/w;


# static fields
.field private static final i:Ljava/text/Collator;


# instance fields
.field public a:Z

.field private b:Landroid/content/SharedPreferences;

.field private c:Lcom/samsung/SMT/af;

.field private d:Landroid/speech/tts/TtsEngines;

.field private e:Landroid/widget/Checkable;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/samsung/SMT/l;->i:Ljava/text/Collator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/samsung/SMT/l;->c:Lcom/samsung/SMT/af;

    iput-object v0, p0, Lcom/samsung/SMT/l;->d:Landroid/speech/tts/TtsEngines;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/SMT/l;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/l;->h:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Lcom/samsung/SMT/x;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x1

    const-string v0, ""

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3}, Lcom/samsung/SMT/x;->h(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HD"

    if-ne v2, v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p1, p2, p3}, Lcom/samsung/SMT/x;->e(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v3, v2, 0x1

    invoke-static {p2}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f06001f

    invoke-virtual {p0, v2}, Lcom/samsung/SMT/l;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eq v3, v1, :cond_3

    if-nez v0, :cond_3

    invoke-static {p2}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f06002b

    invoke-virtual {p0, v1}, Lcom/samsung/SMT/l;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_1
    const v2, 0x7f060020

    invoke-virtual {p0, v2}, Lcom/samsung/SMT/l;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method private a(Lcom/samsung/SMT/x;I)V
    .locals 7

    iget-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1, p2}, Lcom/samsung/SMT/x;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;I)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-Variant Info"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "%s%02d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-Variant Info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "%s%02d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "f"

    aput-object v4, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-Variant Info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "%s%02d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "f"

    aput-object v4, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/samsung/SMT/l;->i:Ljava/text/Collator;

    return-object v0
.end method

.method private d()V
    .locals 13

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    new-instance v0, Lcom/samsung/SMT/af;

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/l;->c:Lcom/samsung/SMT/af;

    const-string v0, "installed_languages_list"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/preference/PreferenceGroup;

    invoke-virtual {v7}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v0, p0, Lcom/samsung/SMT/l;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const-string v0, ""

    iget-object v0, p0, Lcom/samsung/SMT/l;->d:Landroid/speech/tts/TtsEngines;

    const-string v1, "com.samsung.SMT"

    invoke-virtual {v0, v1}, Landroid/speech/tts/TtsEngines;->getLocalePrefForEngine(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%s-%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->a(Ljava/lang/String;)V

    move v8, v9

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/l;->c:Lcom/samsung/SMT/af;

    invoke-virtual {v0}, Lcom/samsung/SMT/af;->a()I

    move-result v0

    if-lt v8, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/l;->h:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/SMT/o;

    invoke-direct {v1}, Lcom/samsung/SMT/o;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/samsung/SMT/l;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    const-string v0, "update_engine"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    const-string v0, "debug_mode"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    const-string v2, "DEBUG_OPTION_ENABLE"

    invoke-interface {v1, v2, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_2
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/l;->c:Lcom/samsung/SMT/af;

    invoke-virtual {v0, v8}, Lcom/samsung/SMT/af;->a(I)Lcom/samsung/SMT/x;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/SMT/x;->d()I

    move-result v0

    if-lez v0, :cond_4

    new-array v5, v0, [Ljava/lang/CharSequence;

    sget-object v4, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v6, v4

    move v2, v9

    move v0, v9

    :goto_3
    if-lt v2, v6, :cond_5

    invoke-virtual {v3}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/SMT/l;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/SMT/l;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v3, v0, v2}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v3, v9}, Lcom/samsung/SMT/x;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v9}, Lcom/samsung/SMT/l;->a(Lcom/samsung/SMT/x;I)V

    invoke-virtual {v3}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v10, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v1, v9

    :cond_3
    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/SMT/l;->a(Lcom/samsung/SMT/x;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v1}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;I)I

    move-result v6

    new-instance v0, Lcom/samsung/SMT/p;

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v3}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v3

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/SMT/p;-><init>(Landroid/content/Context;Lcom/samsung/SMT/l;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/CharSequence;I)V

    invoke-virtual {v0, p0}, Lcom/samsung/SMT/p;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/samsung/SMT/l;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_0

    :cond_5
    aget-object v11, v4, v2

    move v1, v0

    move v0, v9

    :goto_4
    invoke-virtual {v3, v11}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v12

    if-lt v0, v12, :cond_6

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v1

    goto :goto_3

    :cond_6
    invoke-direct {p0, v3, v11, v0}, Lcom/samsung/SMT/l;->a(Lcom/samsung/SMT/x;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/p;

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceGroup;->addItemFromInflater(Landroid/preference/Preference;)V

    goto/16 :goto_1

    :cond_8
    const-string v0, "debug_options_toast"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_2
.end method


# virtual methods
.method public a()Landroid/widget/Checkable;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/l;->e:Landroid/widget/Checkable;

    return-object v0
.end method

.method public a(Landroid/widget/Checkable;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/l;->e:Landroid/widget/Checkable;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/l;->f:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/l;->f:Ljava/lang/String;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {}, Lcom/samsung/SMT/al;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTheme(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "SamsungTTSSettings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    new-instance v0, Landroid/speech/tts/TtsEngines;

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/speech/tts/TtsEngines;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/l;->d:Landroid/speech/tts/TtsEngines;

    invoke-direct {p0}, Lcom/samsung/SMT/l;->d()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debug_options_toast"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/SMT/n;

    invoke-direct {v1, p0}, Lcom/samsung/SMT/n;-><init>(Lcom/samsung/SMT/l;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return v4

    :cond_1
    if-nez p2, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/l;->d:Landroid/speech/tts/TtsEngines;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TtsEngines;->parseLocaleString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/SMT/l;->d:Landroid/speech/tts/TtsEngines;

    const-string v2, "com.samsung.SMT"

    invoke-virtual {v1, v2, v0}, Landroid/speech/tts/TtsEngines;->updateLocalePrefForEngine(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v0, p0, Lcom/samsung/SMT/l;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    const-string v1, "03132"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/SMT/l;->b:Landroid/content/SharedPreferences;

    const-string v2, "DEBUG_OPTION_ENABLE"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "DEBUG_OPTION_ENABLE"

    invoke-interface {v1, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v0, "installed_languages_list"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/samsung/SMT/l;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/l;->addPreferencesFromResource(I)V

    :goto_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    invoke-virtual {v0}, Lcom/samsung/SMT/b;->a()V

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/SMT/l;->onResume()V

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/l;->g:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v0, "DEBUG_OPTION_ENABLE"

    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/SMT/l;->c:Lcom/samsung/SMT/af;

    invoke-virtual {v1}, Lcom/samsung/SMT/af;->d()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/SMT/l;->c:Lcom/samsung/SMT/af;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/samsung/SMT/af;->a(I)Lcom/samsung/SMT/x;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/samsung/SMT/l;->a(Lcom/samsung/SMT/x;I)V

    invoke-virtual {v1}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/SMT/l;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/SMT/l;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/SMT/l;->a(Lcom/samsung/SMT/x;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/samsung/SMT/l;->d()V

    return-void
.end method
