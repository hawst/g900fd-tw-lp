.class Lcom/samsung/SMT/e;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/DownloadList;


# direct methods
.method constructor <init>(Lcom/samsung/SMT/DownloadList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v0, "BROADCAST_FULL_UPDATE_LIST"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->a(Lcom/samsung/SMT/DownloadList;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->d(Lcom/samsung/SMT/DownloadList;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->b(Lcom/samsung/SMT/DownloadList;)V

    const-string v0, "BROADCAST_CURRENT_LANGUAGE_INFO"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getCharSequenceArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v2, Lcom/samsung/SMT/z;

    invoke-direct {v2, v0}, Lcom/samsung/SMT/z;-><init>([Ljava/lang/String;)V

    const-string v0, "BROADCAST_CURRENT_LANGUAGE_VERSION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/samsung/SMT/z;->a:Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->c(Lcom/samsung/SMT/DownloadList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->c(Lcom/samsung/SMT/DownloadList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    iget-object v0, v0, Lcom/samsung/SMT/z;->b:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsung/SMT/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->c(Lcom/samsung/SMT/DownloadList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    iget-object v3, v2, Lcom/samsung/SMT/z;->a:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsung/SMT/z;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->c(Lcom/samsung/SMT/DownloadList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/samsung/SMT/z;->i:Z

    iget-object v0, p0, Lcom/samsung/SMT/e;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->c(Lcom/samsung/SMT/DownloadList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    iget-object v1, v2, Lcom/samsung/SMT/z;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/SMT/z;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method
