.class Lcom/samsung/SMT/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/g;

.field private final synthetic b:Landroid/widget/ImageButton;


# direct methods
.method constructor <init>(Lcom/samsung/SMT/g;Landroid/widget/ImageButton;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    iput-object p2, p0, Lcom/samsung/SMT/i;->b:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v0}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->f(Lcom/samsung/SMT/DownloadList;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/i;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v1}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/SMT/DownloadList;->g(Lcom/samsung/SMT/DownloadList;)Landroid/widget/ImageButton;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v0}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/SMT/DownloadList;->h(Lcom/samsung/SMT/DownloadList;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v0}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/samsung/SMT/DownloadList;->a(Lcom/samsung/SMT/DownloadList;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v0}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/SMT/DownloadList;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/i;->b:Landroid/widget/ImageButton;

    const v1, 0x7f020007

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_1
    iget-object v0, p0, Lcom/samsung/SMT/i;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v1}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v1

    const v2, 0x7f060034

    invoke-virtual {v1, v2}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/SMT/i;->a:Lcom/samsung/SMT/g;

    invoke-static {v0}, Lcom/samsung/SMT/g;->a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/SMT/i;->b:Landroid/widget/ImageButton;

    invoke-static {v0, v1}, Lcom/samsung/SMT/DownloadList;->a(Lcom/samsung/SMT/DownloadList;Landroid/widget/ImageButton;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/i;->b:Landroid/widget/ImageButton;

    const v1, 0x7f020006

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1
.end method
