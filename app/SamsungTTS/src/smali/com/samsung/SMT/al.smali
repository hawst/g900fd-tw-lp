.class public Lcom/samsung/SMT/al;
.super Ljava/lang/Object;


# static fields
.field static a:[Ljava/lang/String;

.field static b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "T700"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "T701"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "T705"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "T800"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "T801"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "T805"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "T807"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/SMT/al;->a:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/SMT/al;->b:[Ljava/lang/String;

    return-void
.end method

.method static a()I
    .locals 1

    const v0, 0x103012b

    return v0
.end method

.method static a(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "ro.build.scafe.cream"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "white"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "ro.build.scafe.cream"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "black"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/samsung/SMT/al;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/samsung/SMT/al;->c()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/samsung/SMT/al;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static b()Z
    .locals 4

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, Lcom/samsung/SMT/al;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    :goto_1
    return v1

    :cond_0
    sget-object v3, Lcom/samsung/SMT/al;->a:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static b(Landroid/content/Context;)Z
    .locals 2

    :try_start_0
    const-string v0, "android.util.GeneralUtil"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "Samsung TTS"

    const-string v1, "Can not found GeneralUtil class."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c()Z
    .locals 4

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, Lcom/samsung/SMT/al;->b:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    :goto_1
    return v1

    :cond_0
    sget-object v3, Lcom/samsung/SMT/al;->b:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static d()Z
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    const v3, 0x408ccccd    # 4.4f

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
