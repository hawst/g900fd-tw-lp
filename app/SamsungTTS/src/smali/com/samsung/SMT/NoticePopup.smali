.class public Lcom/samsung/SMT/NoticePopup;
.super Landroid/app/Activity;


# instance fields
.field a:I

.field b:Lcom/samsung/SMT/aa;

.field private c:Landroid/app/AlertDialog;

.field private d:[Ljava/lang/String;

.field private e:Landroid/content/SharedPreferences;

.field private f:Landroid/widget/CheckBox;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/util/ArrayList;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Landroid/speech/tts/TextToSpeech;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/samsung/SMT/NoticePopup;->e:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->g:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/SMT/NoticePopup;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/SMT/NoticePopup;->a:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->j:Ljava/util/ArrayList;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->m:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->n:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->o:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/SMT/NoticePopup;->b:Lcom/samsung/SMT/aa;

    iput-object v1, p0, Lcom/samsung/SMT/NoticePopup;->p:Landroid/speech/tts/TextToSpeech;

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/NoticePopup;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->d()V

    return-void
.end method

.method private a()Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/samsung/SMT/z;)Z
    .locals 4

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    :goto_1
    return v2

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Samsung TTS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already installed HD voice : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/SMT/NoticePopup;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->f()V

    return-void
.end method

.method private b()Z
    .locals 6

    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    array-length v2, v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const-string v2, "%s-%s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    aget-object v4, v4, v1

    aput-object v4, v3, v1

    iget-object v4, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    aget-object v4, v4, v0

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/SMT/NoticePopup;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v0, p0, Lcom/samsung/SMT/NoticePopup;->h:Z

    goto :goto_0

    :cond_3
    iput-boolean v1, p0, Lcom/samsung/SMT/NoticePopup;->h:Z

    goto :goto_0
.end method

.method private c()Z
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/util/Locale;

    iget-object v1, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    aget-object v1, v1, v2

    iget-object v4, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-direct {v0, v1, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/SMT/NoticePopup;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->l:Ljava/lang/String;

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    :goto_1
    return v2

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    invoke-virtual {v4, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->d()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/SMT/NoticePopup;->m:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0x7f06001f

    invoke-virtual {p0, v5}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/SMT/NoticePopup;->n:Ljava/lang/String;

    :goto_2
    invoke-virtual {v0}, Lcom/samsung/SMT/z;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->o:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v0

    iget-boolean v4, p0, Lcom/samsung/SMT/NoticePopup;->h:Z

    if-eq v0, v4, :cond_0

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    const v5, 0x7f060020

    invoke-virtual {p0, v5}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/SMT/NoticePopup;->n:Ljava/lang/String;

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_1
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-NoticePopup Status"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method

.method private e()Z
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v0, "SMT_DOWNLOADABLE_LIST"

    invoke-virtual {p0, v0, v2}, Lcom/samsung/SMT/NoticePopup;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v0, v1

    :goto_1
    const-string v3, "BROADCAST_DOWNLOADABLE_LIST_COUNT"

    invoke-interface {v4, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-lt v0, v3, :cond_3

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "BROADCAST_DOWNLOADABLE_LIST"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    invoke-interface {v4, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {v3}, Lcom/samsung/SMT/z;->a(Ljava/lang/String;)Lcom/samsung/SMT/z;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/samsung/SMT/NoticePopup;->g:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/samsung/SMT/z;->a(Landroid/content/Context;Lcom/samsung/SMT/z;)Z

    move-result v3

    if-eqz v3, :cond_4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v5}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v2

    :goto_3
    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v3

    move v3, v1

    goto :goto_3

    :cond_5
    invoke-virtual {v5}, Lcom/samsung/SMT/z;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/SMT/NoticePopup;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-direct {p0, v0}, Lcom/samsung/SMT/NoticePopup;->a(Lcom/samsung/SMT/z;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/SMT/NoticePopup;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->o:Ljava/lang/String;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const v0, 0x14000020

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/samsung/SMT/NoticePopup;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->b:Lcom/samsung/SMT/aa;

    const-string v1, "ST06"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    const/4 v3, 0x0

    const/16 v2, 0x400

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->n:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/NoticePopup;->setTheme(I)V

    :goto_0
    const-string v0, "SamsungTTSSettings"

    invoke-virtual {p0, v0, v8}, Lcom/samsung/SMT/NoticePopup;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->e:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SMT_CURRENT_LANGUAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->d:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0, v7}, Lcom/samsung/SMT/NoticePopup;->requestWindowFeature(I)Z

    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/NoticePopup;->setContentView(I)V

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030008

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f08000c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-direct {p0}, Lcom/samsung/SMT/NoticePopup;->d()V

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->finish()V

    :goto_1
    return-void

    :cond_0
    const/high16 v0, 0x7f070000

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/NoticePopup;->setTheme(I)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    const-string v2, "com.samsung.SMT"

    invoke-direct {v0, p0, v3, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->p:Landroid/speech/tts/TextToSpeech;

    const v0, 0x7f08000b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v2, ""

    const v2, 0x7f06002c

    invoke-virtual {p0, v2}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "%s (%s)"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/SMT/NoticePopup;->k:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/samsung/SMT/NoticePopup;->l:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "%s\n%s"

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v2, v4, v8

    const v2, 0x7f06002d

    invoke-virtual {p0, v2}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060029

    invoke-virtual {p0, v3}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060027

    invoke-virtual {p0, v2}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/SMT/ab;

    invoke-direct {v3, p0}, Lcom/samsung/SMT/ab;-><init>(Lcom/samsung/SMT/NoticePopup;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060028

    invoke-virtual {p0, v2}, Lcom/samsung/SMT/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/SMT/ac;

    invoke-direct {v3, p0}, Lcom/samsung/SMT/ac;-><init>(Lcom/samsung/SMT/NoticePopup;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iget-object v2, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v1, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v1, v4

    float-to-int v1, v1

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/widget/CheckBox;->setPadding(IIII)V

    iget-object v1, p0, Lcom/samsung/SMT/NoticePopup;->f:Landroid/widget/CheckBox;

    new-instance v2, Lcom/samsung/SMT/ad;

    invoke-direct {v2, p0}, Lcom/samsung/SMT/ad;-><init>(Lcom/samsung/SMT/NoticePopup;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->c:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->c:Landroid/app/AlertDialog;

    new-instance v1, Lcom/samsung/SMT/ae;

    invoke-direct {v1, p0}, Lcom/samsung/SMT/ae;-><init>(Lcom/samsung/SMT/NoticePopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    new-instance v0, Lcom/samsung/SMT/aa;

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/aa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->b:Lcom/samsung/SMT/aa;

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->b:Lcom/samsung/SMT/aa;

    const-string v1, "ST04"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->c:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->p:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/NoticePopup;->p:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/SMT/NoticePopup;->p:Landroid/speech/tts/TextToSpeech;

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-virtual {p0}, Lcom/samsung/SMT/NoticePopup;->finish()V

    return-void
.end method
