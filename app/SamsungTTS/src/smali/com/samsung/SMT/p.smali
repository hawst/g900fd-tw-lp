.class public Lcom/samsung/SMT/p;
.super Landroid/preference/Preference;


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field c:Landroid/view/View$OnKeyListener;

.field private d:Landroid/content/Context;

.field private volatile e:Z

.field private final f:Lcom/samsung/SMT/w;

.field private g:Landroid/view/View;

.field private h:Ljava/util/Locale;

.field private i:Landroid/app/AlertDialog;

.field private j:[Ljava/lang/CharSequence;

.field private k:I

.field private l:Lcom/samsung/SMT/l;

.field private final m:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/SMT/l;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/CharSequence;I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/SMT/p;->d:Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/SMT/p;->h:Ljava/util/Locale;

    iput-object v1, p0, Lcom/samsung/SMT/p;->j:[Ljava/lang/CharSequence;

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/SMT/p;->k:I

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/p;->a:Ljava/lang/CharSequence;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/p;->b:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/samsung/SMT/p;->l:Lcom/samsung/SMT/l;

    new-instance v0, Lcom/samsung/SMT/q;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/q;-><init>(Lcom/samsung/SMT/p;)V

    iput-object v0, p0, Lcom/samsung/SMT/p;->m:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/samsung/SMT/r;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/r;-><init>(Lcom/samsung/SMT/p;)V

    iput-object v0, p0, Lcom/samsung/SMT/p;->c:Landroid/view/View$OnKeyListener;

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->n:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/p;->setLayoutResource(I)V

    :goto_0
    iput-object p1, p0, Lcom/samsung/SMT/p;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    new-instance v0, Ljava/util/Locale;

    const/4 v1, 0x3

    invoke-virtual {p3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x7

    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/speech/tts/TtsEngines;->normalizeTTSLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/p;->h:Ljava/util/Locale;

    iput-object p5, p0, Lcom/samsung/SMT/p;->j:[Ljava/lang/CharSequence;

    iput p6, p0, Lcom/samsung/SMT/p;->k:I

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/SMT/p;->h:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/SMT/p;->h:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/p;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p4}, Lcom/samsung/SMT/p;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/samsung/SMT/p;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/p;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/samsung/SMT/p;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/p;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, p3}, Lcom/samsung/SMT/p;->setKey(Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/samsung/SMT/p;->e:Z

    iput-object p2, p0, Lcom/samsung/SMT/p;->l:Lcom/samsung/SMT/l;

    return-void

    :cond_0
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/p;->setLayoutResource(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/SMT/p;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/p;->d:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/samsung/SMT/p;->j:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/SMT/p;->k:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/SMT/p;->k:I

    iget-object v1, p0, Lcom/samsung/SMT/p;->j:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/p;->l:Lcom/samsung/SMT/l;

    iget-boolean v0, v0, Lcom/samsung/SMT/l;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/SMT/p;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/SMT/p;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/SMT/p;->j:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/samsung/SMT/p;->k:I

    new-instance v3, Lcom/samsung/SMT/u;

    invoke-direct {v3, p0}, Lcom/samsung/SMT/u;-><init>(Lcom/samsung/SMT/p;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    new-instance v1, Lcom/samsung/SMT/v;

    invoke-direct {v1, p0}, Lcom/samsung/SMT/v;-><init>(Lcom/samsung/SMT/p;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Lcom/samsung/SMT/p;->l:Lcom/samsung/SMT/l;

    iput-boolean v4, v0, Lcom/samsung/SMT/l;->a:Z

    goto :goto_0
.end method

.method private a(Landroid/widget/CompoundButton;Z)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/samsung/SMT/p;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    invoke-interface {v0}, Lcom/samsung/SMT/w;->a()Landroid/widget/Checkable;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    invoke-interface {v0}, Lcom/samsung/SMT/w;->a()Landroid/widget/Checkable;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    invoke-interface {v0, p1}, Lcom/samsung/SMT/w;->a(Landroid/widget/Checkable;)V

    iget-object v0, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    invoke-virtual {p0}, Lcom/samsung/SMT/p;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/SMT/w;->a(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/samsung/SMT/p;->e:Z

    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iput-boolean v2, p0, Lcom/samsung/SMT/p;->e:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/p;->callChangeListener(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/SMT/p;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/SMT/p;->k:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/p;Landroid/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/p;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/SMT/p;->a(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/p;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/samsung/SMT/p;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/samsung/SMT/p;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/p;->a()V

    return-void
.end method

.method static synthetic c(Lcom/samsung/SMT/p;)I
    .locals 1

    iget v0, p0, Lcom/samsung/SMT/p;->k:I

    return v0
.end method

.method static synthetic d(Lcom/samsung/SMT/p;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/p;->i:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/samsung/SMT/p;)Lcom/samsung/SMT/l;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/p;->l:Lcom/samsung/SMT/l;

    return-object v0
.end method


# virtual methods
.method public getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/p;->c:Landroid/view/View$OnKeyListener;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-super {p0, p1, p2}, Landroid/preference/Preference;->getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const-string v0, "%s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/samsung/SMT/p;->a:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/samsung/SMT/p;->b:Ljava/lang/CharSequence;

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v0, 0x7f080006

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/samsung/SMT/p;->m:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/samsung/SMT/p;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    invoke-interface {v3}, Lcom/samsung/SMT/w;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/samsung/SMT/p;->f:Lcom/samsung/SMT/w;

    invoke-interface {v3, v0}, Lcom/samsung/SMT/w;->a(Landroid/widget/Checkable;)V

    :cond_0
    iput-boolean v5, p0, Lcom/samsung/SMT/p;->e:Z

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    iput-boolean v4, p0, Lcom/samsung/SMT/p;->e:Z

    const v2, 0x7f080005

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/samsung/SMT/s;

    invoke-direct {v3, p0, v0}, Lcom/samsung/SMT/s;-><init>(Lcom/samsung/SMT/p;Landroid/widget/RadioButton;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setFocusable(Z)V

    const v0, 0x7f080008

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/p;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/SMT/p;->g:Landroid/view/View;

    new-instance v2, Lcom/samsung/SMT/t;

    invoke-direct {v2, p0}, Lcom/samsung/SMT/t;-><init>(Lcom/samsung/SMT/p;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/SMT/p;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/SMT/p;->g:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/SMT/p;->d:Landroid/content/Context;

    const v3, 0x7f060031

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-object v1
.end method
