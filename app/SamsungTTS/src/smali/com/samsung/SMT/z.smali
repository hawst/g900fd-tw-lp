.class Lcom/samsung/SMT/z;
.super Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/z;->h:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/samsung/SMT/z;->i:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/z;->j:Ljava/util/ArrayList;

    array-length v0, p1

    if-lez v0, :cond_0

    aget-object v0, p1, v1

    iput-object v0, p0, Lcom/samsung/SMT/z;->b:Ljava/lang/String;

    :cond_0
    array-length v0, p1

    if-le v0, v2, :cond_1

    aget-object v0, p1, v2

    iput-object v0, p0, Lcom/samsung/SMT/z;->c:Ljava/lang/String;

    :cond_1
    array-length v0, p1

    if-le v0, v3, :cond_2

    aget-object v0, p1, v3

    iput-object v0, p0, Lcom/samsung/SMT/z;->d:Ljava/lang/String;

    :cond_2
    array-length v0, p1

    if-le v0, v4, :cond_3

    aget-object v0, p1, v4

    iput-object v0, p0, Lcom/samsung/SMT/z;->e:Ljava/lang/String;

    :cond_3
    array-length v0, p1

    if-le v0, v5, :cond_4

    aget-object v0, p1, v5

    iput-object v0, p0, Lcom/samsung/SMT/z;->f:Ljava/lang/String;

    :cond_4
    array-length v0, p1

    const/4 v1, 0x5

    if-le v0, v1, :cond_5

    const/4 v0, 0x5

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/samsung/SMT/z;->g:Ljava/lang/String;

    :cond_5
    array-length v0, p1

    const/4 v1, 0x6

    if-le v0, v1, :cond_6

    const/4 v0, 0x6

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/samsung/SMT/z;->h:Ljava/lang/String;

    :cond_6
    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/samsung/SMT/z;
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ""

    aput-object v2, v0, v1

    new-instance v1, Lcom/samsung/SMT/z;

    invoke-direct {v1, v0}, Lcom/samsung/SMT/z;-><init>([Ljava/lang/String;)V

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v2, "\t"

    invoke-direct {v0, p0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/SMT/z;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/SMT/z;->c:Ljava/lang/String;

    :cond_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/SMT/z;->d:Ljava/lang/String;

    :cond_2
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/SMT/z;->e:Ljava/lang/String;

    :cond_3
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/SMT/z;->f:Ljava/lang/String;

    :cond_4
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/SMT/z;->g:Ljava/lang/String;

    :cond_5
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/SMT/z;->h:Ljava/lang/String;

    :cond_6
    return-object v1
.end method

.method static a(Landroid/content/Context;Lcom/samsung/SMT/z;)Z
    .locals 10

    const/4 v2, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p1, Lcom/samsung/SMT/z;->f:Ljava/lang/String;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/samsung/SMT/z;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v0, p1, Lcom/samsung/SMT/z;->d:Ljava/lang/String;

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    invoke-static {v4}, Lcom/samsung/SMT/x;->f(Ljava/lang/String;)Z

    const-string v0, "SMT_STUB_CHECKLIST"

    invoke-virtual {p0, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v1

    const-string v8, "0"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/samsung/SMT/ak;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/ak;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/samsung/SMT/ak;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "/system/tts/lang_SVOXP"

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/SMT/ak;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    :goto_1
    if-eqz v1, :cond_0

    const-string v0, "SMT_DBFILE_LIST"

    invoke-virtual {p0, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-interface {v0, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v8, ""

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v4}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/samsung/SMT/ak;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/ak;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/samsung/SMT/ak;->a()Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v1, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/SMT/ak;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :goto_2
    if-eqz v0, :cond_0

    :cond_2
    move v6, v7

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lcom/samsung/SMT/EngineManager;

    invoke-direct {v0}, Lcom/samsung/SMT/EngineManager;-><init>()V

    const-string v1, "/system/tts/lang_SMT"

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/EngineManager;->getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v1

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/samsung/SMT/EngineManager;

    invoke-direct {v0}, Lcom/samsung/SMT/EngineManager;-><init>()V

    sget-object v1, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/EngineManager;->getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v1, v6

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/SMT/z;->i:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/z;->j:Ljava/util/ArrayList;

    return-object v0
.end method
