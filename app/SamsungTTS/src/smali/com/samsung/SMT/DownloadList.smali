.class public Lcom/samsung/SMT/DownloadList;
.super Landroid/app/ListActivity;


# static fields
.field public static f:Z

.field private static final k:Ljava/text/Collator;


# instance fields
.field a:Lcom/samsung/SMT/g;

.field b:I

.field c:Landroid/media/MediaPlayer;

.field d:[Ljava/lang/String;

.field e:Z

.field g:Landroid/content/BroadcastReceiver;

.field private h:Ljava/util/ArrayList;

.field private i:Ljava/lang/String;

.field private j:Landroid/content/SharedPreferences;

.field private l:Landroid/widget/ImageButton;

.field private m:Ljava/lang/String;

.field private n:Landroid/view/View$OnKeyListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/samsung/SMT/DownloadList;->k:Ljava/text/Collator;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/SMT/DownloadList;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    iput-object v1, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    iput v2, p0, Lcom/samsung/SMT/DownloadList;->b:I

    iput-object v1, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    iput-object v1, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    iput-boolean v2, p0, Lcom/samsung/SMT/DownloadList;->e:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->m:Ljava/lang/String;

    new-instance v0, Lcom/samsung/SMT/d;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/d;-><init>(Lcom/samsung/SMT/DownloadList;)V

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->n:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/SMT/e;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/e;-><init>(Lcom/samsung/SMT/DownloadList;)V

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->g:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    const-string v3, ""

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    move-object v0, v3

    :goto_1
    return-object v0

    :cond_0
    const-string v2, ""

    iget-object v4, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;

    aget-object v4, v4, v0

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v2, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v4, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;

    aget-object v4, v4, v0

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;

    aget-object v0, v1, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/samsung/SMT/DownloadList;->k:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/SMT/DownloadList;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->f()V

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/DownloadList;Landroid/widget/ImageButton;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/DownloadList;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/SMT/DownloadList;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/samsung/SMT/DownloadList;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/SMT/DownloadList;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->g()Z

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->f()V

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->d()V

    return-void
.end method

.method static synthetic b(Lcom/samsung/SMT/DownloadList;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->e()V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 9

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->h()V

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "SAMPLE/%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v8

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->close()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/samsung/SMT/f;

    invoke-direct {v1, p0}, Lcom/samsung/SMT/f;-><init>(Lcom/samsung/SMT/DownloadList;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iput-object p1, p0, Lcom/samsung/SMT/DownloadList;->m:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Samsung TTS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "####ERROR : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v7

    goto :goto_0
.end method

.method static synthetic c(Lcom/samsung/SMT/DownloadList;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.SMT.UpdateManager.UPDATE_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/DownloadList;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic c(Lcom/samsung/SMT/DownloadList;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/DownloadList;->m:Ljava/lang/String;

    return-void
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    invoke-virtual {v0}, Lcom/samsung/SMT/g;->clear()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    invoke-virtual {v0}, Lcom/samsung/SMT/g;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    new-instance v1, Lcom/samsung/SMT/k;

    invoke-direct {v1}, Lcom/samsung/SMT/k;-><init>()V

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/g;->sort(Ljava/util/Comparator;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-static {v2, v0}, Lcom/samsung/SMT/z;->a(Landroid/content/Context;Lcom/samsung/SMT/z;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v2, v0}, Lcom/samsung/SMT/g;->add(Ljava/lang/Object;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/samsung/SMT/DownloadList;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->d()V

    return-void
.end method

.method static synthetic e(Lcom/samsung/SMT/DownloadList;)Landroid/view/View$OnKeyListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->n:Landroid/view/View$OnKeyListener;

    return-object v0
.end method

.method private e()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    const-string v1, "BROADCAST_DOWNLOADABLE_LIST_VERSION"

    const-string v3, "0"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->i:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    const-string v3, "BROADCAST_DOWNLOADABLE_LIST_COUNT"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lt v1, v0, :cond_1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BROADCAST_DOWNLOADABLE_LIST"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Lcom/samsung/SMT/z;->a(Ljava/lang/String;)Lcom/samsung/SMT/z;

    move-result-object v6

    const/4 v4, 0x0

    move v3, v2

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_3

    move-object v0, v4

    :goto_3
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method private f()V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    const-string v2, "BROADCAST_DOWNLOADABLE_LIST_VERSION"

    const-string v3, "0"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    const-string v3, "BROADCAST_DOWNLOADABLE_LIST_COUNT"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BROADCAST_DOWNLOADABLE_LIST"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, Lcom/samsung/SMT/z;->a(Ljava/lang/String;)Lcom/samsung/SMT/z;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/SMT/DownloadList;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic f(Lcom/samsung/SMT/DownloadList;)Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->i()Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/samsung/SMT/DownloadList;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private g()Z
    .locals 2

    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    :try_start_0
    const-string v1, "SAMPLE"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->d:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v2, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/SMT/DownloadList;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    const v1, 0x7f020004

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    const v1, 0x7f060032

    invoke-virtual {p0, v1}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->m:Ljava/lang/String;

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->l:Landroid/widget/ImageButton;

    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/samsung/SMT/DownloadList;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->h()V

    return-void
.end method

.method static synthetic i(Lcom/samsung/SMT/DownloadList;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->m:Ljava/lang/String;

    return-object v0
.end method

.method private i()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const v1, 0x7f030002

    const/high16 v0, 0x7f030000

    sget-boolean v2, Lcom/samsung/SMT/SamsungTTSService;->n:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/samsung/SMT/al;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/DownloadList;->setTheme(I)V

    const v1, 0x7f030005

    iput-boolean v4, p0, Lcom/samsung/SMT/DownloadList;->e:Z

    const v0, 0x7f030001

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f060024

    invoke-virtual {p0, v3}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-static {}, Lcom/samsung/SMT/al;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/DownloadList;->setContentView(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/samsung/SMT/g;

    invoke-direct {v2, p0, p0, v1, v0}, Lcom/samsung/SMT/g;-><init>(Lcom/samsung/SMT/DownloadList;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    const-string v0, "SMT_DOWNLOADABLE_LIST"

    invoke-virtual {p0, v0, v4}, Lcom/samsung/SMT/DownloadList;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/DownloadList;->j:Landroid/content/SharedPreferences;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.samsung.SMT.ACTION_UPDATE_LIST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/SMT/DownloadList;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/SMT/DownloadList;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    sget v0, Lcom/samsung/SMT/SamsungTTSService;->m:I

    iput v0, p0, Lcom/samsung/SMT/DownloadList;->b:I

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->b()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->a:Lcom/samsung/SMT/g;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/DownloadList;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->c()V

    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    sput-boolean v4, Lcom/samsung/SMT/DownloadList;->f:Z

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    iget-object v0, p0, Lcom/samsung/SMT/DownloadList;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/DownloadList;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->h()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/SMT/DownloadList;->f:Z

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/SMT/DownloadList;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    invoke-direct {p0}, Lcom/samsung/SMT/DownloadList;->h()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    return-void
.end method
