.class Lcom/samsung/SMT/ai;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/SamsungTTSService;


# direct methods
.method constructor <init>(Lcom/samsung/SMT/SamsungTTSService;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/ai;->a:Lcom/samsung/SMT/SamsungTTSService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.SMT.ACTION_SHOW_DOWNLOAD_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/ai;->a:Lcom/samsung/SMT/SamsungTTSService;

    invoke-static {v0, v2}, Lcom/samsung/SMT/SamsungTTSService;->b(Lcom/samsung/SMT/SamsungTTSService;Z)V

    iget-object v0, p0, Lcom/samsung/SMT/ai;->a:Lcom/samsung/SMT/SamsungTTSService;

    invoke-static {v0, v2}, Lcom/samsung/SMT/SamsungTTSService;->c(Lcom/samsung/SMT/SamsungTTSService;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "com.samsung.SMT.ACTION_UPDATE_LIST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "BROADCAST_FULL_UPDATE_LIST"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BROADCAST_UPDATE_DB_FILE"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/ai;->a:Lcom/samsung/SMT/SamsungTTSService;

    invoke-static {v0, v2}, Lcom/samsung/SMT/SamsungTTSService;->a(Lcom/samsung/SMT/SamsungTTSService;Z)V

    goto :goto_0

    :cond_2
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/ai;->a:Lcom/samsung/SMT/SamsungTTSService;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/SamsungTTSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/SMT/ai;->a:Lcom/samsung/SMT/SamsungTTSService;

    invoke-static {v0}, Lcom/samsung/SMT/SamsungTTSService;->a(Lcom/samsung/SMT/SamsungTTSService;)V

    goto :goto_0
.end method
