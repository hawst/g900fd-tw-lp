.class Lcom/samsung/SMT/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/DownloadList;


# direct methods
.method constructor <init>(Lcom/samsung/SMT/DownloadList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/d;->a:Lcom/samsung/SMT/DownloadList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 11

    const/16 v10, 0x14

    const/16 v9, 0x13

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    if-eq p2, v10, :cond_0

    if-ne p2, v9, :cond_1

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/d;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v0}, Lcom/samsung/SMT/DownloadList;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v5

    move v0, v1

    :goto_0
    if-lt v0, v5, :cond_2

    move v3, v1

    move v0, v4

    :goto_1
    if-ne v0, v4, :cond_4

    :cond_1
    :goto_2
    return v1

    :cond_2
    iget-object v3, p0, Lcom/samsung/SMT/d;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v3}, Lcom/samsung/SMT/DownloadList;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-ne v3, v6, :cond_3

    iget-object v3, p0, Lcom/samsung/SMT/d;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v3}, Lcom/samsung/SMT/DownloadList;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v6, 0x7f080003

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    move v3, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    if-ne p2, v10, :cond_7

    add-int/lit8 v0, v0, 0x1

    :cond_5
    :goto_3
    if-lt v0, v5, :cond_8

    move v0, v1

    :cond_6
    :goto_4
    iget-object v4, p0, Lcom/samsung/SMT/d;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v4}, Lcom/samsung/SMT/DownloadList;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const v6, 0x7f080003

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f080004

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v6}, Landroid/view/View;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_4

    if-eqz v3, :cond_9

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :goto_5
    invoke-virtual {v4, v1}, Landroid/view/View;->playSoundEffect(I)V

    move v1, v2

    goto :goto_2

    :cond_7
    if-ne p2, v9, :cond_5

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_8
    if-gez v0, :cond_6

    add-int/lit8 v0, v5, -0x1

    goto :goto_4

    :cond_9
    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    goto :goto_5

    :cond_a
    move v3, v1

    goto :goto_1
.end method
