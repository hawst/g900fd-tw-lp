.class Lcom/samsung/SMT/g;
.super Landroid/widget/ArrayAdapter;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/DownloadList;

.field private b:Ljava/util/ArrayList;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/samsung/SMT/DownloadList;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/SMT/g;->c:I

    iput-object p4, p0, Lcom/samsung/SMT/g;->b:Ljava/util/ArrayList;

    iput p3, p0, Lcom/samsung/SMT/g;->c:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/g;)Lcom/samsung/SMT/DownloadList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/SMT/g;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/SMT/g;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const v0, 0x34000020

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/DownloadList;->startActivity(Landroid/content/Intent;)V

    new-instance v0, Lcom/samsung/SMT/aa;

    iget-object v1, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v1}, Lcom/samsung/SMT/DownloadList;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/aa;-><init>(Landroid/content/Context;)V

    const-string v1, "ST06"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const v10, 0x7f060032

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/DownloadList;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/samsung/SMT/g;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/g;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    if-eqz v0, :cond_5

    const v1, 0x7f080001

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080002

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    new-instance v3, Ljava/util/Locale;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    const/4 v7, 0x7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/speech/tts/TtsEngines;->normalizeTTSLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%s (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz v2, :cond_4

    const-string v3, ""

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    const v5, 0x7f06001f

    invoke-virtual {v3, v5}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v0}, Lcom/samsung/SMT/z;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " / "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    const v5, 0x7f06002b

    invoke-virtual {v4, v5}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_2
    invoke-virtual {v0}, Lcom/samsung/SMT/z;->e()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " / "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_3
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const v3, 0x7f080004

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    const v5, 0x7f060033

    invoke-virtual {v4, v5}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v4, Lcom/samsung/SMT/h;

    invoke-direct {v4, p0}, Lcom/samsung/SMT/h;-><init>(Lcom/samsung/SMT/g;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v4}, Lcom/samsung/SMT/DownloadList;->e(Lcom/samsung/SMT/DownloadList;)Landroid/view/View$OnKeyListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v4, 0x7f080003

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v5, v10}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/samsung/SMT/i;

    invoke-direct {v5, p0, v4}, Lcom/samsung/SMT/i;-><init>(Lcom/samsung/SMT/g;Landroid/widget/ImageButton;)V

    iget-object v6, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/SMT/DownloadList;->b(Lcom/samsung/SMT/DownloadList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v5}, Lcom/samsung/SMT/DownloadList;->e(Lcom/samsung/SMT/DownloadList;)Landroid/view/View$OnKeyListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-static {v5}, Lcom/samsung/SMT/DownloadList;->i(Lcom/samsung/SMT/DownloadList;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    iget-boolean v5, v5, Lcom/samsung/SMT/DownloadList;->e:Z

    if-eqz v5, :cond_7

    const v5, 0x7f020007

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_1
    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    const v7, 0x7f060034

    invoke-virtual {v5, v7}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v0}, Lcom/samsung/SMT/z;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    :goto_3
    const/high16 v0, 0x7f080000

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/SMT/j;

    invoke-direct {v1, p0}, Lcom/samsung/SMT/j;-><init>(Lcom/samsung/SMT/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_5
    return-object p2

    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    const v5, 0x7f060020

    invoke-virtual {v3, v5}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_7
    const v5, 0x7f020006

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    :cond_8
    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    iget-boolean v5, v5, Lcom/samsung/SMT/DownloadList;->e:Z

    if-eqz v5, :cond_9

    const v5, 0x7f020004

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_4
    iget-object v5, p0, Lcom/samsung/SMT/g;->a:Lcom/samsung/SMT/DownloadList;

    invoke-virtual {v5, v10}, Lcom/samsung/SMT/DownloadList;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_9
    const v5, 0x7f020003

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_4

    :cond_a
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    const-string v0, ""

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_3

    :cond_b
    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_3
.end method
