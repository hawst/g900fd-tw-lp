.class public Lcom/samsung/SMT/af;
.super Ljava/lang/Object;


# static fields
.field static a:Ljava/lang/String;


# instance fields
.field public b:Landroid/content/SharedPreferences;

.field private c:Landroid/content/Context;

.field private d:Lcom/samsung/SMT/ak;

.field private e:Lcom/samsung/SMT/EngineManager;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.SMT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/SMT/af;->c:Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    iput-object v1, p0, Lcom/samsung/SMT/af;->e:Lcom/samsung/SMT/EngineManager;

    const-string v0, "/system/tts/lang_SMT"

    iput-object v0, p0, Lcom/samsung/SMT/af;->f:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    iput-object v1, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    iput-object p1, p0, Lcom/samsung/SMT/af;->c:Landroid/content/Context;

    const-string v0, "SamsungTTSSettings"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/samsung/SMT/ak;

    iget-object v1, p0, Lcom/samsung/SMT/af;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/SMT/ak;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    new-instance v0, Lcom/samsung/SMT/EngineManager;

    invoke-direct {v0}, Lcom/samsung/SMT/EngineManager;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/af;->e:Lcom/samsung/SMT/EngineManager;

    invoke-virtual {p0}, Lcom/samsung/SMT/af;->c()V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, ""

    iget-object v2, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    const-string v3, "%s%s%02d"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v0

    aput-object p3, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {p3}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    invoke-virtual {p1, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x4

    const/4 v3, 0x7

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3, p2}, Lcom/samsung/SMT/ak;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/af;->c:Landroid/content/Context;

    const v1, 0x7f060022

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    invoke-static {p3}, Lcom/samsung/SMT/x;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/samsung/SMT/SamsungTTSService;->d:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {p3}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_2
    sget-object v1, Lcom/samsung/SMT/SamsungTTSService;->e:[[I

    array-length v1, v1

    invoke-interface {v3, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    if-le v1, v4, :cond_3

    if-lez p2, :cond_3

    add-int/lit8 v1, p2, -0x1

    iget-object v4, p0, Lcom/samsung/SMT/af;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v5, Lcom/samsung/SMT/SamsungTTSService;->e:[[I

    invoke-interface {v3, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    aget-object v5, v5, v6

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ge v1, v4, :cond_3

    iget-object v1, p0, Lcom/samsung/SMT/af;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/samsung/SMT/SamsungTTSService;->e:[[I

    invoke-interface {v3, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    aget-object v2, v2, v3

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p3}, Lcom/samsung/SMT/x;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "%s%02d"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p3, v4, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "x01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "You-jin"

    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Lcom/samsung/SMT/x;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/af;->c()V

    :cond_0
    const-string v4, ""

    const-string v3, ""

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    move v1, v2

    :cond_1
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "%02d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/SMT/af;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0, v5}, Lcom/samsung/SMT/af;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;I)I

    move-result v1

    :goto_3
    const-string v6, ""

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0, v3, v1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_3
    invoke-virtual {v0, v2}, Lcom/samsung/SMT/x;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v0, v2, v7}, Lcom/samsung/SMT/af;->a(Lcom/samsung/SMT/x;IZ)V

    iget-object v0, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    :cond_4
    const-string v1, ""

    new-instance v1, Lcom/samsung/SMT/a;

    invoke-direct {v1}, Lcom/samsung/SMT/a;-><init>()V

    const-string v6, "DEFAULT_VOICE"

    invoke-virtual {v1, v6, v5}, Lcom/samsung/SMT/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, ""

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v8, :cond_7

    invoke-virtual {v1, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_6
    move-object v0, v4

    goto/16 :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/af;->c()V

    :cond_0
    const-string v2, ""

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0, p3, p4}, Lcom/samsung/SMT/x;->f(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(Lcom/samsung/SMT/x;IZ)V
    .locals 7

    iget-object v0, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-Variant Info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez p3, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1, p2}, Lcom/samsung/SMT/x;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;I)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-Variant Info"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "%s%02d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/af;->f:Ljava/lang/String;

    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/af;->c()V

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    move v2, v3

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    move v0, v2

    :cond_1
    return v0

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_3

    if-eqz v0, :cond_3

    if-ne v0, v3, :cond_1

    :cond_3
    if-eqz v0, :cond_4

    if-ne v0, v3, :cond_5

    :cond_4
    move v2, v0

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/af;->f:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/af;->c()V

    :cond_0
    const-string v2, ""

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-Variant Info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 15

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    const/4 v9, 0x0

    const/4 v2, -0x1

    const/4 v1, -0x1

    const/4 v0, 0x0

    move v10, v0

    move v0, v1

    move v1, v2

    :goto_0
    sget-object v2, Lcom/samsung/SMT/SamsungTTSService;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lt v10, v2, :cond_0

    return-void

    :cond_0
    new-instance v12, Lcom/samsung/SMT/x;

    invoke-direct {v12}, Lcom/samsung/SMT/x;-><init>()V

    sget-object v2, Lcom/samsung/SMT/SamsungTTSService;->d:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v12, v2}, Lcom/samsung/SMT/x;->h(Ljava/lang/String;)V

    sget-object v13, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v14, v13

    const/4 v2, 0x0

    move v11, v2

    move v3, v1

    move v2, v0

    :goto_1
    if-lt v11, v14, :cond_2

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v12}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/af;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, ""

    if-nez v9, :cond_b

    new-instance v0, Lcom/samsung/SMT/a;

    invoke-direct {v0}, Lcom/samsung/SMT/a;-><init>()V

    :goto_2
    const-string v1, "DEFAULT_VOICE"

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/samsung/SMT/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x3

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v12, v4, v1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v12, v4, v1}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x0

    invoke-virtual {p0, v12, v1, v4}, Lcom/samsung/SMT/af;->a(Lcom/samsung/SMT/x;IZ)V

    :cond_1
    :goto_3
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move-object v9, v0

    move v1, v3

    move v0, v2

    goto :goto_0

    :cond_2
    aget-object v4, v13, v11

    const/4 v5, 0x0

    move v0, v2

    move v1, v3

    :goto_4
    const/4 v2, 0x4

    if-lt v5, v2, :cond_3

    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v3, v1

    move v2, v0

    goto :goto_1

    :cond_3
    const-string v6, "/system/tts/lang_SMT"

    const-string v8, "SD"

    iget-object v2, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    if-eqz v2, :cond_8

    invoke-static {v4}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    const-string v1, "/system/tts/lang_SVOXP"

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/SMT/ak;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    iget-object v0, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    sget-object v1, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/SMT/ak;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/SMT/af;->d:Lcom/samsung/SMT/ak;

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/SMT/ak;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    const-string v6, "/system/tts/lang_SVOXP"

    move v1, v7

    :cond_4
    :goto_5
    if-nez v1, :cond_9

    if-nez v0, :cond_9

    sget-object v6, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v5, v4}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v3, v12

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_6
    invoke-static {v4}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz v1, :cond_6

    if-nez v0, :cond_7

    :cond_6
    const-string v0, "%s%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v12, v4}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v12, v0, v1}, Lcom/samsung/SMT/af;->a(Lcom/samsung/SMT/x;IZ)V

    :cond_7
    const/4 v1, -0x1

    const/4 v0, -0x1

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    :cond_8
    invoke-static {v4}, Lcom/samsung/SMT/x;->g(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/samsung/SMT/af;->e:Lcom/samsung/SMT/EngineManager;

    const-string v1, "/system/tts/lang_SMT"

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/EngineManager;->getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v7

    iget-object v0, p0, Lcom/samsung/SMT/af;->e:Lcom/samsung/SMT/EngineManager;

    sget-object v1, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/EngineManager;->getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    const-string v6, "/system/tts/lang_SMT"

    invoke-static {v4}, Lcom/samsung/SMT/x;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v8, "HD"

    move v1, v7

    goto/16 :goto_5

    :cond_9
    if-nez v1, :cond_a

    if-eqz v0, :cond_a

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v5, v4}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v3, v12

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_a
    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    sget-object v6, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual {v12}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v5, v4}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v3, v12

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_b
    move-object v0, v9

    goto/16 :goto_2

    :cond_c
    move-object v0, v9

    goto/16 :goto_3

    :cond_d
    move v1, v7

    goto/16 :goto_5
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/af;->b:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-Variant Info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Ljava/util/ArrayList;
    .locals 3

    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/af;->c()V

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    return-object v2

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/af;->g:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/x;

    invoke-virtual {v0}, Lcom/samsung/SMT/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public finalize()V
    .locals 0

    return-void
.end method
