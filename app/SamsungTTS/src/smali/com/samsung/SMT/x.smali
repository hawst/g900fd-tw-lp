.class public Lcom/samsung/SMT/x;
.super Ljava/lang/Object;


# static fields
.field static final a:[Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Vector;

.field private c:Ljava/util/Vector;

.field private d:Ljava/util/Vector;

.field private e:Ljava/util/Vector;

.field private f:Ljava/util/Vector;

.field private g:Ljava/util/Vector;

.field private volatile h:Ljava/lang/String;

.field private volatile i:Ljava/util/Locale;

.field private volatile j:Z

.field private k:Landroid/preference/ListPreference;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "m"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "x"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "y"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "l"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "z"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/x;->b:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/x;->c:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/x;->d:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/x;->e:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/x;->f:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/x;->g:Ljava/util/Vector;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/x;->h:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/SMT/x;->i:Ljava/util/Locale;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/SMT/x;->j:Z

    iput-object v1, p0, Lcom/samsung/SMT/x;->k:Landroid/preference/ListPreference;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/SMT/x;->l:Ljava/lang/String;

    return-void
.end method

.method static b(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "y"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "l"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "l"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static d(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "f"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "l"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static e(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "f"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "m"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static f(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "y"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static g(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lcom/samsung/SMT/x;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/samsung/SMT/x;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l(Ljava/lang/String;)Ljava/util/Vector;
    .locals 1

    const-string v0, "f"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/x;->b:Ljava/util/Vector;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "m"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/x;->c:Ljava/util/Vector;

    goto :goto_0

    :cond_1
    const-string v0, "x"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/x;->d:Ljava/util/Vector;

    goto :goto_0

    :cond_2
    const-string v0, "y"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/SMT/x;->e:Ljava/util/Vector;

    goto :goto_0

    :cond_3
    const-string v0, "l"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/SMT/x;->f:Ljava/util/Vector;

    goto :goto_0

    :cond_4
    const-string v0, "z"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/x;->g:Ljava/util/Vector;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v1

    if-ge p2, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->c()I

    move-result v0

    :cond_0
    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5

    const/4 v2, 0x0

    const/4 v1, -0x2

    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/samsung/SMT/x;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, p3, p4}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, ""

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/SMT/x;->d()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/samsung/SMT/x;->c()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p3, p4}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    const-string v3, ""

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/samsung/SMT/x;->d()I

    move-result v3

    if-lez v3, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/samsung/SMT/x;->c()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/x;->h:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    sget-object v3, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    move v2, v0

    :goto_0
    if-lt v1, v4, :cond_1

    const-string v0, "f"

    :cond_0
    return-object v0

    :cond_1
    aget-object v0, v3, v1

    invoke-direct {p0, v0}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    add-int/2addr v5, v2

    if-gt v5, p1, :cond_0

    invoke-direct {p0, v0}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    invoke-direct {p0, p1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v6

    new-instance v0, Lcom/samsung/SMT/y;

    move-object v1, p0

    move-object v2, p3

    move v3, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/SMT/y;-><init>(Lcom/samsung/SMT/x;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/util/Locale;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/x;->i:Ljava/util/Locale;

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/SMT/x;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/samsung/SMT/x;->l:Ljava/lang/String;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;I)I
    .locals 7

    const/4 v0, 0x0

    sget-object v4, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v5, v4

    move v2, v0

    move v1, v0

    :goto_0
    if-lt v2, v5, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    return v0

    :cond_1
    aget-object v3, v4, v2

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    sub-int v1, p2, v1

    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_1

    :cond_2
    invoke-direct {p0, v3}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    add-int/2addr v3, v1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0
.end method

.method public b()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/x;->i:Ljava/util/Locale;

    return-object v0
.end method

.method public c(Ljava/lang/String;I)I
    .locals 6

    const/4 v0, 0x0

    sget-object v3, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-lt v1, v4, :cond_0

    :goto_1
    return v0

    :cond_0
    aget-object v2, v3, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    add-int/2addr v0, p2

    goto :goto_1

    :cond_1
    invoke-direct {p0, v2}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/SMT/x;->j:Z

    return v0
.end method

.method public d()I
    .locals 5

    const/4 v0, 0x0

    sget-object v2, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-lt v0, v3, :cond_0

    return v1

    :cond_0
    aget-object v4, v2, v0

    invoke-direct {p0, v4}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d(Ljava/lang/String;I)I
    .locals 8

    const/4 v2, 0x0

    sget-object v5, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v6, v5

    move v4, v2

    move v3, v2

    :goto_0
    if-lt v4, v6, :cond_0

    move v0, v3

    :goto_1
    return v0

    :cond_0
    aget-object v7, v5, v4

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :goto_2
    invoke-direct {p0, v7}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    :cond_1
    invoke-direct {p0, v7}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v7}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    add-int v0, v3, v1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public e(Ljava/lang/String;I)I
    .locals 7

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v2

    sget-object v3, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-lt v1, v4, :cond_0

    :goto_1
    return v0

    :cond_0
    aget-object v5, v3, v1

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/samsung/SMT/x;->b(Ljava/lang/String;I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_1
    invoke-static {v5}, Lcom/samsung/SMT/x;->d(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v2, :cond_2

    invoke-direct {p0, v5}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    add-int/2addr v0, v5

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/SMT/x;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/SMT/x;->d()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    const-string v1, ""

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v2

    if-lt v0, v2, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, p2, :cond_1

    invoke-direct {p0, p1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public g(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    const-string v1, ""

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v2

    if-lt v0, v2, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, p2, :cond_1

    invoke-direct {p0, p1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public h(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    invoke-virtual {p0, p1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v1

    if-le v1, p2, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->d()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lcom/samsung/SMT/x;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/SMT/x;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    iget-object v1, p0, Lcom/samsung/SMT/x;->h:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/SMT/x;->h:Ljava/lang/String;

    const/4 v3, 0x4

    const/4 v4, 0x7

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/x;->a(Ljava/util/Locale;)V

    :cond_0
    return-void
.end method

.method public i(Ljava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public i(Ljava/lang/String;I)Z
    .locals 3

    const/4 v1, 0x0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/SMT/x;->i(Ljava/lang/String;)I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/SMT/x;->a(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, p2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public j(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v4, 0x0

    const-string v2, "f"

    sget-object v6, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v7, v6

    move v5, v4

    :goto_0
    if-lt v5, v7, :cond_0

    return-object v2

    :cond_0
    aget-object v1, v6, v5

    move v3, v4

    :goto_1
    invoke-direct {p0, v1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v3, v0, :cond_1

    move-object v0, v2

    :goto_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v2, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method public k(Ljava/lang/String;)I
    .locals 8

    const/4 v2, 0x0

    sget-object v5, Lcom/samsung/SMT/x;->a:[Ljava/lang/String;

    array-length v6, v5

    move v4, v2

    move v3, v2

    :goto_0
    if-lt v4, v6, :cond_0

    return v3

    :cond_0
    aget-object v7, v5, v4

    move v1, v2

    :goto_1
    invoke-direct {p0, v7}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    move v1, v3

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v3, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v7}, Lcom/samsung/SMT/x;->l(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/y;

    invoke-virtual {v0}, Lcom/samsung/SMT/y;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
