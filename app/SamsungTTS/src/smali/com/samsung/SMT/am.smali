.class Lcom/samsung/SMT/am;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/UpdateManager;


# direct methods
.method constructor <init>(Lcom/samsung/SMT/UpdateManager;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v2, 0x0

    const-string v0, "BROADCAST_CURRENT_LANGUAGE_INFO"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getCharSequenceArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v1, Lcom/samsung/SMT/z;

    invoke-direct {v1, v0}, Lcom/samsung/SMT/z;-><init>([Ljava/lang/String;)V

    const-string v0, "BROADCAST_CURRENT_LANGUAGE_VERSION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/SMT/z;->a:Ljava/lang/String;

    const-string v0, "BROADCAST_DB_FILELIST"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getCharSequenceArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/SMT/z;->j:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/samsung/SMT/z;->i:Z

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0, v1}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;Lcom/samsung/SMT/z;)Z

    move-result v0

    const-string v1, "BROADCAST_UPDATE_DB_FILE"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "BROADCAST_DOWNLOADABLE_LIST_VERSION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v1}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    const-string v1, "BROADCAST_DOWNLOADABLE_LIST_VERSION"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v1, v2

    :goto_0
    const-string v0, "BROADCAST_DOWNLOADABLE_LIST_COUNT"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lt v1, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->c(Lcom/samsung/SMT/UpdateManager;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0, p2}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->d(Lcom/samsung/SMT/UpdateManager;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;I)V

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->d(Lcom/samsung/SMT/UpdateManager;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->e(Lcom/samsung/SMT/UpdateManager;)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "BROADCAST_DOWNLOADABLE_LIST"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getCharSequenceArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/SMT/am;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v3}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Lcom/samsung/SMT/z;

    invoke-direct {v4, v0}, Lcom/samsung/SMT/z;-><init>([Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
