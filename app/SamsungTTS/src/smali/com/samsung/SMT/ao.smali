.class Lcom/samsung/SMT/ao;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/samsung/SMT/UpdateManager;


# direct methods
.method private constructor <init>(Lcom/samsung/SMT/UpdateManager;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/SMT/UpdateManager;Lcom/samsung/SMT/ao;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/SMT/ao;-><init>(Lcom/samsung/SMT/UpdateManager;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0, v2}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;Z)V

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0, v3}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v4}, Lcom/samsung/SMT/UpdateManager;->f(Lcom/samsung/SMT/UpdateManager;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    const-string v5, "SMT_STUB_CHECKLIST_DATE"

    invoke-interface {v4, v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_6

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/SMT/UpdateManager;->a(Lcom/samsung/SMT/UpdateManager;Z)V

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->e(Lcom/samsung/SMT/UpdateManager;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->g()Ljava/lang/String;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->g()Ljava/lang/String;

    move-result-object v0

    const-string v4, "null"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    invoke-static {v0}, Lcom/samsung/SMT/UpdateManager;->b(Lcom/samsung/SMT/UpdateManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/z;

    invoke-virtual {v0}, Lcom/samsung/SMT/z;->e()Ljava/lang/String;

    move-result-object v0

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v0, "0"

    :cond_4
    new-instance v5, Lcom/samsung/SMT/an;

    iget-object v6, p0, Lcom/samsung/SMT/ao;->a:Lcom/samsung/SMT/UpdateManager;

    const-string v7, ""

    invoke-direct {v5, v6, v4, v0, v7}, Lcom/samsung/SMT/an;-><init>(Lcom/samsung/SMT/UpdateManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/an;

    iget-object v1, v0, Lcom/samsung/SMT/an;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/SMT/an;

    iget-object v0, v0, Lcom/samsung/SMT/an;->c:Ljava/lang/String;

    invoke-interface {v4, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2
.end method
