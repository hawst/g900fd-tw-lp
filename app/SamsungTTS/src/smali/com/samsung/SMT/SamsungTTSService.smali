.class public Lcom/samsung/SMT/SamsungTTSService;
.super Landroid/speech/tts/TextToSpeechService;


# static fields
.field static final b:Z

.field static c:Z

.field static final d:[Ljava/lang/String;

.field static final e:[[I

.field static k:Lcom/samsung/SMT/b;

.field static l:I

.field static m:I

.field static n:Z

.field private static s:I


# instance fields
.field private A:Landroid/content/Intent;

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:J

.field public a:Landroid/content/SharedPreferences;

.field public f:Lcom/samsung/SMT/ak;

.field g:Landroid/telephony/TelephonyManager;

.field h:I

.field i:Landroid/os/Handler;

.field j:Lcom/samsung/SMT/aa;

.field o:Landroid/telephony/PhoneStateListener;

.field p:Landroid/content/BroadcastReceiver;

.field q:Landroid/content/BroadcastReceiver;

.field private r:Lcom/samsung/SMT/af;

.field private final t:[B

.field private volatile u:[Ljava/lang/String;

.field private volatile v:Z

.field private volatile w:Z

.field private volatile x:I

.field private volatile y:I

.field private z:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x3e80

    sput v0, Lcom/samsung/SMT/SamsungTTSService;->s:I

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    sput-boolean v4, Lcom/samsung/SMT/SamsungTTSService;->c:Z

    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "eng-USA"

    aput-object v1, v0, v4

    const-string v1, "eng-GBR"

    aput-object v1, v0, v5

    const-string v1, "spa-ESP"

    aput-object v1, v0, v3

    const-string v1, "fra-FRA"

    aput-object v1, v0, v6

    const-string v1, "deu-DEU"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ita-ITA"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "por-PRT"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "por-BRA"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rus-RUS"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "kor-KOR"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "zho-CHN"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "jpn-JPN"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ces-CZE"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "nld-NLD"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "dan-DNK"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "fin-FIN"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ell-GRC"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "hun-HUN"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "nob-NOR"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "pol-POL"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "slk-SVK"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "swe-SWE"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "tur-TUR"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "ara-ARE"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "eng-AUS"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "zho-HKG"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "spa-MEX"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "fra-CAN"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "tha-THA"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "hin-IND"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/SMT/SamsungTTSService;->d:[Ljava/lang/String;

    const/16 v0, 0x1e

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v3, [I

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v3, [I

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v3, [I

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/SMT/SamsungTTSService;->e:[[I

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    sput v4, Lcom/samsung/SMT/SamsungTTSService;->l:I

    sput v4, Lcom/samsung/SMT/SamsungTTSService;->m:I

    sput-boolean v4, Lcom/samsung/SMT/SamsungTTSService;->n:Z

    :try_start_0
    const-string v0, "samsungtts"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Samsung TTS"

    const-string v1, "Fail to load libsamsungtts.so"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x7f050004
        0x7f050022
    .end array-data

    :array_1
    .array-data 4
        0x7f050009
        0x7f050027
    .end array-data

    :array_2
    .array-data 4
        0x7f050006
        0x7f050024
    .end array-data

    :array_3
    .array-data 4
        0x7f050008
        0x7f050026
    .end array-data

    :array_4
    .array-data 4
        0x7f050007
        0x7f050025
    .end array-data

    :array_5
    .array-data 4
        0x7f05000a
        0x7f050028
    .end array-data

    :array_6
    .array-data 4
        0x7f05000d
        0x7f05002b
    .end array-data

    :array_7
    .array-data 4
        0x7f050016
        0x7f05002c
    .end array-data

    :array_8
    .array-data 4
        0x7f05000c
        0x7f05002a
    .end array-data

    :array_9
    .array-data 4
        0x7f050003
        0x7f050021
    .end array-data

    :array_a
    .array-data 4
        0x7f050005
        0x7f050023
    .end array-data

    :array_b
    .array-data 4
        0x7f05000b
        0x7f050029
    .end array-data

    :array_c
    .array-data 4
        0x7f05000e
        0x7f05002d
    .end array-data

    :array_d
    .array-data 4
        0x7f05000f
        0x7f05002e
    .end array-data

    :array_e
    .array-data 4
        0x7f050010
        0x7f05002f
    .end array-data

    :array_f
    .array-data 4
        0x7f050011
        0x7f050030
    .end array-data

    :array_10
    .array-data 4
        0x7f050012
        0x7f050031
    .end array-data

    :array_11
    .array-data 4
        0x7f050013
        0x7f050032
    .end array-data

    :array_12
    .array-data 4
        0x7f050014
        0x7f050033
    .end array-data

    :array_13
    .array-data 4
        0x7f050015
        0x7f050034
    .end array-data

    :array_14
    .array-data 4
        0x7f050017
        0x7f050035
    .end array-data

    :array_15
    .array-data 4
        0x7f050018
        0x7f050036
    .end array-data

    :array_16
    .array-data 4
        0x7f050019
        0x7f050037
    .end array-data

    :array_17
    .array-data 4
        0x7f05001a
        0x7f050038
    .end array-data

    :array_18
    .array-data 4
        0x7f05001b
        0x7f050039
    .end array-data

    :array_19
    .array-data 4
        0x7f05001c
        0x7f05003a
    .end array-data

    :array_1a
    .array-data 4
        0x7f05001d
        0x7f05003b
    .end array-data

    :array_1b
    .array-data 4
        0x7f05001e
        0x7f05003c
    .end array-data

    :array_1c
    .array-data 4
        0x7f05001f
        0x7f05003d
    .end array-data

    :array_1d
    .array-data 4
        0x7f050020
        0x7f05003e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/speech/tts/TextToSpeechService;-><init>()V

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    const/16 v0, 0xa0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->t:[B

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->v:Z

    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->w:Z

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/SMT/SamsungTTSService;->x:I

    iput v3, p0, Lcom/samsung/SMT/SamsungTTSService;->y:I

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.samsung.SMT"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "com.google.android.apps.maps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.vlingo.midas"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.samsung.android.widgetapp.briefing"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.sec.android.app.samsungapps"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.samsung.helphub"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.android.app.shealth"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.samsung.svoiceprovider"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.sec.android.app.clockpackage"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->z:[Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->A:Landroid/content/Intent;

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->B:Z

    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->C:Z

    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->D:Z

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->g:Landroid/telephony/TelephonyManager;

    iput v3, p0, Lcom/samsung/SMT/SamsungTTSService;->h:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/SMT/SamsungTTSService;->E:J

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->i:Landroid/os/Handler;

    iput-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->j:Lcom/samsung/SMT/aa;

    new-instance v0, Lcom/samsung/SMT/ag;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/ag;-><init>(Lcom/samsung/SMT/SamsungTTSService;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->o:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/samsung/SMT/ah;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/ah;-><init>(Lcom/samsung/SMT/SamsungTTSService;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->p:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/samsung/SMT/ai;

    invoke-direct {v0, p0}, Lcom/samsung/SMT/ai;-><init>(Lcom/samsung/SMT/SamsungTTSService;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->q:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(Landroid/speech/tts/SynthesisCallback;)I
    .locals 4

    const/16 v3, 0xa0

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/samsung/SMT/SamsungTTSService;->v:Z

    if-eqz v2, :cond_0

    const/4 v0, -0x1

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->t:[B

    invoke-direct {p0, v2}, Lcom/samsung/SMT/SamsungTTSService;->getSynthesizedData([B)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->t:[B

    invoke-interface {p1, v1, v0, v3}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    sget-object v1, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->t:[B

    invoke-virtual {v1, v2, v3}, Lcom/samsung/SMT/b;->a([BI)V

    goto :goto_0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/samsung/SMT/SamsungTTSService;->B:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Samsung TTS"

    const-string v1, "reloadPref - reload engine"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v0}, Lcom/samsung/SMT/af;->c()V

    iput-boolean v2, p0, Lcom/samsung/SMT/SamsungTTSService;->B:Z

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/SamsungTTSService;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->d()V

    return-void
.end method

.method static synthetic a(Lcom/samsung/SMT/SamsungTTSService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/SMT/SamsungTTSService;->B:Z

    return-void
.end method

.method private a(Z)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v0, v0, v5

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v0, v0, v4

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v0, v0, v3

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v6, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v0, v0, v3

    const-string v1, "d01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "%s-%s"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v2, v2, v5

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v2, v2, v4

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v2, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v3}, Lcom/samsung/SMT/af;->d()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v3, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v3}, Lcom/samsung/SMT/af;->a()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v3, v0}, Lcom/samsung/SMT/af;->a(I)Lcom/samsung/SMT/x;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lcom/samsung/SMT/x;->g(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.SMT.NoticePopup.SHOW_POPUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x70820000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "SMT_CURRENT_LANGUAGE"

    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/SMT/SamsungTTSService;->E:J

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "<SMT:VoiceEffects>"

    const/16 v3, 0x12

    invoke-virtual {p1, v1, v2, v1, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "<SMT:NSVoiceEffects>"

    const/16 v3, 0x14

    invoke-virtual {p1, v1, v2, v1, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "<SMT:Spectrum>"

    const/16 v3, 0xe

    invoke-virtual {p1, v1, v2, v1, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private b()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/SMT/SamsungTTSService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.samsung.SMT.ACTION_SHOW_DOWNLOAD_POPUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.samsung.SMT.ACTION_UPDATE_LIST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/SMT/SamsungTTSService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic b(Lcom/samsung/SMT/SamsungTTSService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/SMT/SamsungTTSService;->a(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-NoticePopup Status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SMT_DOWNLOADABLE_LIST"

    invoke-virtual {p0, v0, v2}, Lcom/samsung/SMT/SamsungTTSService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    move v0, v1

    :goto_1
    const-string v3, "BROADCAST_DOWNLOADABLE_LIST_COUNT"

    invoke-interface {v4, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-NoticePopup Status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "BROADCAST_DOWNLOADABLE_LIST"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    invoke-interface {v4, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v3}, Lcom/samsung/SMT/z;->a(Ljava/lang/String;)Lcom/samsung/SMT/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/SMT/z;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/samsung/SMT/z;->a(Landroid/content/Context;Lcom/samsung/SMT/z;)Z

    move-result v5

    if-eqz v5, :cond_3

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/SMT/z;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v2

    :goto_2
    if-nez v3, :cond_3

    move v1, v2

    goto/16 :goto_0

    :catch_0
    move-exception v3

    move v3, v1

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method static synthetic c(Lcom/samsung/SMT/SamsungTTSService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/SMT/SamsungTTSService;->C:Z

    return-void
.end method

.method private d()V
    .locals 4

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.SMT.UpdateManager.UPDATE_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->A:Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->A:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Samsung TTS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not start UpdateManager : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->A:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->A:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->stopService(Landroid/content/Intent;)Z

    :cond_0
    return-void
.end method

.method private f()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    const-string v0, "SamsungTTSSettings"

    invoke-virtual {p0, v0, v3}, Lcom/samsung/SMT/SamsungTTSService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    const-string v1, "SETTING_SPECTRUM"

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/SMT/SamsungTTSService;->x:I

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    const-string v1, "SETTING_VOICEEFFECTS"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/SMT/SamsungTTSService;->y:I

    return-void
.end method

.method private g()V
    .locals 3

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v2, "com.sec.android.app.samsungapps"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/SMT/SamsungTTSService;->D:Z

    goto :goto_0
.end method

.method private native getSamplingRate()I
.end method

.method private native getSynthesizedData([B)I
.end method

.method private h()V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v0, Lcom/samsung/SMT/SamsungTTSService;->l:I

    new-instance v0, Ljava/io/File;

    const-string v1, "system/lib"

    const-string v2, "libsamsungtts.so"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const-string v3, "Europe/UK"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "1"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/samsung/SMT/SamsungTTSService;->l:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "Samsung TTS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Engine ver"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/SMT/SamsungTTSService;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Samsung TTS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Interface ver"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/SMT/SamsungTTSService;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private i()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/SMT/SamsungTTSService;->D:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Samsung TTS"

    const-string v2, "checkPopupAvailable() - BTP : No SamsungApps."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/samsung/SMT/SamsungTTSService;->E:J

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x493e0

    cmp-long v0, v3, v5

    if-gez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    if-eqz v0, :cond_3

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v4

    move v3, v1

    :goto_1
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->z:[Ljava/lang/String;

    array-length v0, v0

    if-lt v3, v0, :cond_5

    :cond_3
    iget v0, p0, Lcom/samsung/SMT/SamsungTTSService;->h:I

    if-eqz v0, :cond_9

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "Samsung TTS"

    const-string v2, "checkPopupAvailable() - BTP : During call."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/samsung/SMT/SamsungTTSService;->z:[Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_7

    const-string v0, "Samsung TTS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "checkPopupAvailable() - BTP : TopTask is "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/SMT/SamsungTTSService;->z:[Ljava/lang/String;

    aget-object v3, v4, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move v0, v1

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "car_mode_on"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_b

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_a

    const-string v0, "Samsung TTS"

    const-string v2, "checkPopupAvailable() - BTP : Driving mode on"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move v0, v1

    goto/16 :goto_0

    :cond_b
    const-string v0, "net.mirrorlink.on"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_c

    const-string v0, "Samsung TTS"

    const-string v2, "checkPopupAvailable() - BTP : MirrorLink is ON"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    move v0, v1

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "device_provisioned"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_f

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_e

    const-string v0, "Samsung TTS"

    const-string v2, "checkPopupAvailable() - BTP : DEVICE_PROVISIONED is false"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    move v0, v1

    goto/16 :goto_0

    :cond_f
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const/16 v3, 0x64

    if-lt v0, v3, :cond_12

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_10

    const-string v0, "Samsung TTS"

    const-string v2, "checkPopupAvailable() - BTP : KNOX"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_10
    move v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-boolean v2, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v2, :cond_11

    const-string v2, "Samsung TTS"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "checkPopupAvailable() - BTP : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    move v0, v1

    goto/16 :goto_0

    :cond_12
    move v0, v2

    goto/16 :goto_0
.end method

.method private native initialize()I
.end method

.method private native setParameters(IIFI)I
.end method

.method private native speak(Ljava/lang/String;)I
.end method

.method private native supervisor_GetUtterance([Ljava/lang/String;Z)Z
.end method

.method private native supervisor_InputText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native terminate()I
.end method


# virtual methods
.method public synchronized native declared-synchronized getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public onCreate()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/SMT/al;->a(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->n:Z

    const-string v0, "SamsungTTSSettings"

    invoke-virtual {p0, v0, v4}, Lcom/samsung/SMT/SamsungTTSService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->a:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/samsung/SMT/ak;

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/ak;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    new-instance v0, Lcom/samsung/SMT/af;

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    new-instance v0, Lcom/samsung/SMT/aa;

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/aa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->j:Lcom/samsung/SMT/aa;

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->j:Lcom/samsung/SMT/aa;

    const-string v1, "ST00"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/samsung/SMT/b;

    invoke-virtual {p0}, Lcom/samsung/SMT/SamsungTTSService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/SMT/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Landroid/os/Build;->TIME:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x240c8400

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    sput-boolean v5, Lcom/samsung/SMT/SamsungTTSService;->c:Z

    :cond_0
    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->h()V

    invoke-super {p0}, Landroid/speech/tts/TextToSpeechService;->onCreate()V

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->f()V

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->initialize()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iput-boolean v4, p0, Lcom/samsung/SMT/SamsungTTSService;->w:Z

    :goto_0
    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->b()V

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->d()V

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/samsung/SMT/SamsungTTSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->g:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->g:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->o:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->g()V

    return-void

    :cond_1
    iput-boolean v5, p0, Lcom/samsung/SMT/SamsungTTSService;->w:Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/speech/tts/TextToSpeechService;->onDestroy()V

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->i:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->terminate()I

    const-string v0, "Samsung TTS"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    invoke-virtual {v0}, Lcom/samsung/SMT/ak;->b()V

    iput-object v3, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    :cond_1
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->g:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->g:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->o:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_2
    sget-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    invoke-virtual {v0}, Lcom/samsung/SMT/b;->finalize()V

    :cond_3
    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->c()V

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->e()V

    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method protected onGetLanguage()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    return-object v0
.end method

.method protected onIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    const/4 v1, 0x0

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->a()V

    const-string v0, "d01"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v1, "/system/tts/lang_SMT"

    const-string v4, "f"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/SamsungTTSService;->getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v7

    sget-object v1, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    const-string v4, "f"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/SamsungTTSService;->getIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v1

    const/4 v0, 0x2

    if-nez v7, :cond_0

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    sget-object v2, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;)V

    :goto_0
    const-string v1, "Samsung TTS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onIsLanguageAvailable() - lang : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", country : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", variant : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", iResult : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    if-nez v7, :cond_1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    const-string v2, "/system/tts/lang_SMT"

    invoke-virtual {v1, v2}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz v7, :cond_2

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    sget-object v2, Lcom/samsung/SMT/af;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, -0x2

    goto :goto_0

    :cond_3
    const-string v0, ""

    const-string v2, ""

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p3, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p3, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :cond_4
    iget-object v2, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/samsung/SMT/af;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method protected onLoadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->a()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/SMT/SamsungTTSService;->onIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    const-string v0, ""

    const-string v0, ""

    const-string v0, ""

    const/4 v0, -0x2

    if-ne v7, v0, :cond_1

    const/4 v7, -0x2

    :cond_0
    :goto_0
    return v7

    :cond_1
    const/4 v0, -0x1

    if-ne v7, v0, :cond_2

    const/4 v7, -0x1

    goto :goto_0

    :cond_2
    packed-switch v7, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v0, p1}, Lcom/samsung/SMT/af;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "Samsung TTS"

    const-string v1, "onLoadLanguage() - LANG_AVAILABLE"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, p2

    move-object v2, p1

    :goto_1
    const-string v0, "Samsung TTS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "onLoadLanguage() - lang : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", country : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", variant : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, ""

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const-string v0, "Samsung TTS"

    const-string v1, "error : some language, country, variant field is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, -0x1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "Samsung TTS"

    const-string v1, "onLoadLanguage() - LANG_COUNTRY_AVAILABLE"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, p2

    move-object v2, p1

    goto :goto_1

    :pswitch_2
    const-string v0, "Samsung TTS"

    const-string v1, "onLoadLanguage() - LANG_COUNTRY_VAR_AVAILABLE"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, p2

    move-object v2, p1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v0, "d01"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v0}, Lcom/samsung/SMT/af;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "f"

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/SamsungTTSService;->setLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    move v1, v0

    :goto_2
    if-nez v1, :cond_9

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    move v0, v7

    :cond_6
    :goto_3
    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->j:Lcom/samsung/SMT/aa;

    const-string v2, "ST01"

    invoke-virtual {v1, v2}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V

    move v7, v0

    goto/16 :goto_0

    :cond_7
    invoke-static {v4}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/SMT/ak;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    move v1, v0

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->r:Lcom/samsung/SMT/af;

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/samsung/SMT/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/SMT/SamsungTTSService;->setLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    move v1, v0

    goto :goto_2

    :cond_9
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, ""

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, ""

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    const/4 v0, -0x2

    sget-boolean v2, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v2, :cond_6

    const-string v2, "Samsung TTS"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onLoadLanguage() - setLanguage is fail : r = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onStop()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/SMT/SamsungTTSService;->v:Z

    const-string v0, "Samsung TTS"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    invoke-virtual {v0}, Lcom/samsung/SMT/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    invoke-virtual {v0}, Lcom/samsung/SMT/ak;->d()V

    :cond_0
    return-void
.end method

.method protected onSynthesizeText(Landroid/speech/tts/SynthesisRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 10

    const/4 v8, 0x2

    const/4 v9, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/samsung/SMT/SamsungTTSService;->w:Z

    if-nez v0, :cond_1

    const-string v0, "Samsung TTS"

    const-string v1, "onSynthesizeText() - Samsung TTS is not available on non-samsung product."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getVariant()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v4}, Lcom/samsung/SMT/SamsungTTSService;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getText()Ljava/lang/String;

    move-result-object v4

    const-string v0, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Samsung TTS"

    const-string v1, "onSynthesizeText() - Empty input."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-direct {p0, v4}, Lcom/samsung/SMT/SamsungTTSService;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v1, v1, v8

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->u:[Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_4
    const-string v1, ""

    sget-object v5, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[Supervisor] "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/SMT/b;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getCountry()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/SMT/SamsungTTSService;->supervisor_InputText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v1

    move v1, v2

    :cond_5
    :goto_1
    if-nez v1, :cond_a

    sget-boolean v1, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-nez v1, :cond_6

    sget-boolean v1, Lcom/samsung/SMT/SamsungTTSService;->c:Z

    if-eqz v1, :cond_7

    :cond_6
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "streamType"

    invoke-virtual {v1, v4, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v4, ""

    if-ne v1, v9, :cond_b

    const-string v1, "Engine.DEFAULT_STREAM"

    :goto_2
    const-string v4, "Samsung TTS"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onSynthesizeText() - Stream Type = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Samsung TTS"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "onSynthesizeText() - szText : "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    sget-object v1, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    invoke-virtual {v1, v5}, Lcom/samsung/SMT/b;->b(Ljava/lang/String;)V

    sget-object v1, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    invoke-virtual {v1, v5}, Lcom/samsung/SMT/b;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    if-eqz v1, :cond_c

    invoke-static {v0}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->j:Lcom/samsung/SMT/aa;

    const-string v1, "ST03"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getSpeechRate()I

    move-result v1

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getPitch()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/SMT/ak;->a(II)V

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    invoke-virtual {v0, v5, p2}, Lcom/samsung/SMT/ak;->a(Ljava/lang/String;Landroid/speech/tts/SynthesisCallback;)I

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_8

    const-string v0, "Samsung TTS"

    const-string v1, "onSynthesizeText() - Synthesis is done."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_3
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.samsung.SMT.KEY_PARAM"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "DISABLE_NOTICE_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, ""

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/samsung/SMT/SamsungTTSService;->C:Z

    if-nez v0, :cond_9

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->i:Landroid/os/Handler;

    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->i:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/SMT/aj;

    invoke-direct {v1, p0}, Lcom/samsung/SMT/aj;-><init>(Lcom/samsung/SMT/SamsungTTSService;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_9
    :goto_4
    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->C:Z

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/SMT/SamsungTTSService;->f:Lcom/samsung/SMT/ak;

    if-eqz v1, :cond_13

    invoke-static {v0}, Lcom/samsung/SMT/x;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    move v1, v2

    :goto_5
    invoke-direct {p0, v4, v1}, Lcom/samsung/SMT/SamsungTTSService;->supervisor_GetUtterance([Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v4, v3

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    goto/16 :goto_1

    :cond_b
    const-string v4, "%d"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_c
    invoke-direct {p0}, Lcom/samsung/SMT/SamsungTTSService;->getSamplingRate()I

    move-result v0

    sput v0, Lcom/samsung/SMT/SamsungTTSService;->s:I

    sget-boolean v0, Lcom/samsung/SMT/SamsungTTSService;->b:Z

    if-eqz v0, :cond_d

    const-string v0, "Samsung TTS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "onSynthesizeText() - SAMPLING_RATE_HZ : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lcom/samsung/SMT/SamsungTTSService;->s:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    iget-object v0, p0, Lcom/samsung/SMT/SamsungTTSService;->j:Lcom/samsung/SMT/aa;

    const-string v1, "ST02"

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/aa;->a(Ljava/lang/String;)V

    sget v0, Lcom/samsung/SMT/SamsungTTSService;->s:I

    invoke-interface {p2, v0, v8, v2}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    invoke-direct {p0, v5}, Lcom/samsung/SMT/SamsungTTSService;->speak(Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/SMT/SamsungTTSService;->x:I

    iget v4, p0, Lcom/samsung/SMT/SamsungTTSService;->y:I

    :try_start_0
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v6, "com.samsung.SMT.KEY_VOICEEFFECT"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v6, ""

    invoke-virtual {v1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v6, "com.samsung.SMT.KEY_VOICEEFFECT"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v4, v1

    :cond_e
    :goto_6
    :try_start_1
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v6, "com.samsung.SMT.KEY_SPECTRUM"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v6, ""

    invoke-virtual {v1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v6, "com.samsung.SMT.KEY_SPECTRUM"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :cond_f
    :goto_7
    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getSpeechRate()I

    move-result v1

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getPitch()I

    move-result v6

    int-to-float v0, v0

    invoke-direct {p0, v1, v6, v0, v4}, Lcom/samsung/SMT/SamsungTTSService;->setParameters(IIFI)I

    iput-boolean v3, p0, Lcom/samsung/SMT/SamsungTTSService;->v:Z

    sget-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    sget v1, Lcom/samsung/SMT/SamsungTTSService;->s:I

    invoke-virtual {v0, v1}, Lcom/samsung/SMT/b;->a(I)V

    :cond_10
    invoke-direct {p0, p2}, Lcom/samsung/SMT/SamsungTTSService;->a(Landroid/speech/tts/SynthesisCallback;)I

    move-result v0

    if-eq v0, v9, :cond_11

    if-ne v0, v2, :cond_10

    :cond_11
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    sget-object v0, Lcom/samsung/SMT/SamsungTTSService;->k:Lcom/samsung/SMT/b;

    invoke-virtual {v0}, Lcom/samsung/SMT/b;->b()V

    const-string v0, "Samsung TTS"

    const-string v1, "onSynthesizeText() - Synthesis is done."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :catch_0
    move-exception v1

    const-string v6, "Samsung TTS"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid Param : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :catch_1
    move-exception v1

    const-string v6, "Samsung TTS"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid Param : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :cond_12
    const-string v0, "Samsung TTS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSynthesizeText() - KEY_PARAM : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "com.samsung.SMT.KEY_PARAM"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_13
    move v1, v3

    goto/16 :goto_5
.end method

.method public native setLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
.end method
