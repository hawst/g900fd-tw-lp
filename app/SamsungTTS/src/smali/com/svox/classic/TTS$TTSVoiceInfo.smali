.class public Lcom/svox/classic/TTS$TTSVoiceInfo;
.super Ljava/lang/Object;


# instance fields
.field private mAge:Lcom/svox/classic/TTS$TTSAge;

.field private mGender:Lcom/svox/classic/TTS$TTSGender;

.field private mLanguage:Ljava/lang/String;

.field private mLicensed:Z

.field private mName:Ljava/lang/String;

.field private mSampleRate:I

.field final synthetic this$0:Lcom/svox/classic/TTS;


# direct methods
.method public constructor <init>(Lcom/svox/classic/TTS;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 1

    iput-object p1, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->this$0:Lcom/svox/classic/TTS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mName:Ljava/lang/String;

    iput-object p3, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mLanguage:Ljava/lang/String;

    iput p6, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mSampleRate:I

    invoke-static {}, Lcom/svox/classic/TTS$TTSGender;->values()[Lcom/svox/classic/TTS$TTSGender;

    move-result-object v0

    aget-object v0, v0, p4

    iput-object v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mGender:Lcom/svox/classic/TTS$TTSGender;

    invoke-static {}, Lcom/svox/classic/TTS$TTSAge;->values()[Lcom/svox/classic/TTS$TTSAge;

    move-result-object v0

    aget-object v0, v0, p5

    iput-object v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mAge:Lcom/svox/classic/TTS$TTSAge;

    iput-boolean p7, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mLicensed:Z

    return-void
.end method


# virtual methods
.method public age()Lcom/svox/classic/TTS$TTSAge;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mAge:Lcom/svox/classic/TTS$TTSAge;

    return-object v0
.end method

.method public gender()Lcom/svox/classic/TTS$TTSGender;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mGender:Lcom/svox/classic/TTS$TTSGender;

    return-object v0
.end method

.method public isLicensed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mLicensed:Z

    return v0
.end method

.method public language()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public sampleRate()I
    .locals 1

    iget v0, p0, Lcom/svox/classic/TTS$TTSVoiceInfo;->mSampleRate:I

    return v0
.end method
