.class public Lcom/svox/classic/TTS$TTSDetectedLanguage;
.super Ljava/lang/Object;


# instance fields
.field private mLanguage:Ljava/lang/String;

.field private mProbability:I

.field final synthetic this$0:Lcom/svox/classic/TTS;


# direct methods
.method public constructor <init>(Lcom/svox/classic/TTS;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/svox/classic/TTS$TTSDetectedLanguage;->this$0:Lcom/svox/classic/TTS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/svox/classic/TTS$TTSDetectedLanguage;->mLanguage:Ljava/lang/String;

    iput p3, p0, Lcom/svox/classic/TTS$TTSDetectedLanguage;->mProbability:I

    return-void
.end method


# virtual methods
.method public language()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSDetectedLanguage;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public probability()I
    .locals 1

    iget v0, p0, Lcom/svox/classic/TTS$TTSDetectedLanguage;->mProbability:I

    return v0
.end method
