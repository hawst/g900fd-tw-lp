.class Lcom/svox/classic/TTS$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/svox/classic/TTS;

.field private final synthetic val$file:Ljava/lang/String;

.field private final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/svox/classic/TTS;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/svox/classic/TTS$1;->this$0:Lcom/svox/classic/TTS;

    iput-object p2, p0, Lcom/svox/classic/TTS$1;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/svox/classic/TTS$1;->val$file:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/svox/classic/TTS$1;->this$0:Lcom/svox/classic/TTS;

    iget-object v1, p0, Lcom/svox/classic/TTS$1;->val$path:Ljava/lang/String;

    iget-object v2, p0, Lcom/svox/classic/TTS$1;->val$file:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/svox/classic/TTS;->loadLingware(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    # getter for: Lcom/svox/classic/TTS;->mLoadListener:Lcom/svox/classic/TTS$TTSLoadEvent;
    invoke-static {}, Lcom/svox/classic/TTS;->access$0()Lcom/svox/classic/TTS$TTSLoadEvent;

    move-result-object v2

    if-eqz v2, :cond_0

    # getter for: Lcom/svox/classic/TTS;->mLoadListener:Lcom/svox/classic/TTS$TTSLoadEvent;
    invoke-static {}, Lcom/svox/classic/TTS;->access$0()Lcom/svox/classic/TTS$TTSLoadEvent;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/svox/classic/TTS$TTSLoadEvent;->onLoadDone(J)V

    :cond_0
    return-void
.end method
