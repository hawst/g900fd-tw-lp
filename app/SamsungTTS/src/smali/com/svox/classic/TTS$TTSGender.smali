.class public final enum Lcom/svox/classic/TTS$TTSGender;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/svox/classic/TTS$TTSGender;

.field public static final enum Female:Lcom/svox/classic/TTS$TTSGender;

.field public static final enum Male:Lcom/svox/classic/TTS$TTSGender;

.field public static final enum Undefined:Lcom/svox/classic/TTS$TTSGender;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/svox/classic/TTS$TTSGender;

    const-string v1, "Female"

    invoke-direct {v0, v1, v2}, Lcom/svox/classic/TTS$TTSGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSGender;->Female:Lcom/svox/classic/TTS$TTSGender;

    new-instance v0, Lcom/svox/classic/TTS$TTSGender;

    const-string v1, "Male"

    invoke-direct {v0, v1, v3}, Lcom/svox/classic/TTS$TTSGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSGender;->Male:Lcom/svox/classic/TTS$TTSGender;

    new-instance v0, Lcom/svox/classic/TTS$TTSGender;

    const-string v1, "Undefined"

    invoke-direct {v0, v1, v4}, Lcom/svox/classic/TTS$TTSGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSGender;->Undefined:Lcom/svox/classic/TTS$TTSGender;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/svox/classic/TTS$TTSGender;

    sget-object v1, Lcom/svox/classic/TTS$TTSGender;->Female:Lcom/svox/classic/TTS$TTSGender;

    aput-object v1, v0, v2

    sget-object v1, Lcom/svox/classic/TTS$TTSGender;->Male:Lcom/svox/classic/TTS$TTSGender;

    aput-object v1, v0, v3

    sget-object v1, Lcom/svox/classic/TTS$TTSGender;->Undefined:Lcom/svox/classic/TTS$TTSGender;

    aput-object v1, v0, v4

    sput-object v0, Lcom/svox/classic/TTS$TTSGender;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSGender;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/svox/classic/TTS$TTSGender;
    .locals 1

    const-class v0, Lcom/svox/classic/TTS$TTSGender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/svox/classic/TTS$TTSGender;

    return-object v0
.end method

.method public static values()[Lcom/svox/classic/TTS$TTSGender;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/svox/classic/TTS$TTSGender;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSGender;

    array-length v1, v0

    new-array v2, v1, [Lcom/svox/classic/TTS$TTSGender;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
