.class public final enum Lcom/svox/classic/TTS$TTSOutputType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/svox/classic/TTS$TTSOutputType;

.field public static final enum Signal:Lcom/svox/classic/TTS$TTSOutputType;

.field public static final enum SynthUnits:Lcom/svox/classic/TTS$TTSOutputType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/svox/classic/TTS$TTSOutputType;

    const-string v1, "Signal"

    invoke-direct {v0, v1, v2}, Lcom/svox/classic/TTS$TTSOutputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSOutputType;->Signal:Lcom/svox/classic/TTS$TTSOutputType;

    new-instance v0, Lcom/svox/classic/TTS$TTSOutputType;

    const-string v1, "SynthUnits"

    invoke-direct {v0, v1, v3}, Lcom/svox/classic/TTS$TTSOutputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSOutputType;->SynthUnits:Lcom/svox/classic/TTS$TTSOutputType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/svox/classic/TTS$TTSOutputType;

    sget-object v1, Lcom/svox/classic/TTS$TTSOutputType;->Signal:Lcom/svox/classic/TTS$TTSOutputType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/svox/classic/TTS$TTSOutputType;->SynthUnits:Lcom/svox/classic/TTS$TTSOutputType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/svox/classic/TTS$TTSOutputType;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSOutputType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/svox/classic/TTS$TTSOutputType;
    .locals 1

    const-class v0, Lcom/svox/classic/TTS$TTSOutputType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/svox/classic/TTS$TTSOutputType;

    return-object v0
.end method

.method public static values()[Lcom/svox/classic/TTS$TTSOutputType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/svox/classic/TTS$TTSOutputType;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSOutputType;

    array-length v1, v0

    new-array v2, v1, [Lcom/svox/classic/TTS$TTSOutputType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
