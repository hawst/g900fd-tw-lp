.class public final enum Lcom/svox/classic/TTS$TTSSynthIntModifiers;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/svox/classic/TTS$TTSSynthIntModifiers;

.field public static final enum MarkupMode:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

.field public static final enum Pitch:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

.field public static final enum Speed:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

.field public static final enum Volume:Lcom/svox/classic/TTS$TTSSynthIntModifiers;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    const-string v1, "Speed"

    invoke-direct {v0, v1, v2}, Lcom/svox/classic/TTS$TTSSynthIntModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->Speed:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    const-string v1, "Pitch"

    invoke-direct {v0, v1, v3}, Lcom/svox/classic/TTS$TTSSynthIntModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->Pitch:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    const-string v1, "Volume"

    invoke-direct {v0, v1, v4}, Lcom/svox/classic/TTS$TTSSynthIntModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->Volume:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    const-string v1, "MarkupMode"

    invoke-direct {v0, v1, v5}, Lcom/svox/classic/TTS$TTSSynthIntModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->MarkupMode:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->Speed:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    aput-object v1, v0, v2

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->Pitch:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    aput-object v1, v0, v3

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->Volume:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    aput-object v1, v0, v4

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->MarkupMode:Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    aput-object v1, v0, v5

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/svox/classic/TTS$TTSSynthIntModifiers;
    .locals 1

    const-class v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    return-object v0
.end method

.method public static values()[Lcom/svox/classic/TTS$TTSSynthIntModifiers;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    array-length v1, v0

    new-array v2, v1, [Lcom/svox/classic/TTS$TTSSynthIntModifiers;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
