.class public final enum Lcom/svox/classic/TTS$TTSSynthStrModifiers;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/svox/classic/TTS$TTSSynthStrModifiers;

.field public static final enum Language:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

.field public static final enum PreprocContext:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

.field public static final enum ProsodyContext:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

.field public static final enum Voice:Lcom/svox/classic/TTS$TTSSynthStrModifiers;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    const-string v1, "Voice"

    invoke-direct {v0, v1, v2}, Lcom/svox/classic/TTS$TTSSynthStrModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->Voice:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    const-string v1, "PreprocContext"

    invoke-direct {v0, v1, v3}, Lcom/svox/classic/TTS$TTSSynthStrModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->PreprocContext:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    const-string v1, "ProsodyContext"

    invoke-direct {v0, v1, v4}, Lcom/svox/classic/TTS$TTSSynthStrModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->ProsodyContext:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    new-instance v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    const-string v1, "Language"

    invoke-direct {v0, v1, v5}, Lcom/svox/classic/TTS$TTSSynthStrModifiers;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->Language:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->Voice:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    aput-object v1, v0, v2

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->PreprocContext:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    aput-object v1, v0, v3

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->ProsodyContext:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    aput-object v1, v0, v4

    sget-object v1, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->Language:Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    aput-object v1, v0, v5

    sput-object v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/svox/classic/TTS$TTSSynthStrModifiers;
    .locals 1

    const-class v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    return-object v0
.end method

.method public static values()[Lcom/svox/classic/TTS$TTSSynthStrModifiers;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->ENUM$VALUES:[Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    array-length v1, v0

    new-array v2, v1, [Lcom/svox/classic/TTS$TTSSynthStrModifiers;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
