.class public Lcom/svox/classic/TTS;
.super Ljava/lang/Object;


# static fields
.field private static mBreakListener:Lcom/svox/classic/TTS$TTSBreakEvent;

.field private static mLoadListener:Lcom/svox/classic/TTS$TTSLoadEvent;

.field private static mPosListener:Lcom/svox/classic/TTS$TTSPositionEvent;

.field private static mSpeechListener:Lcom/svox/classic/TTS$TTSSpeechData;

.field private static mVisemeListener:Lcom/svox/classic/TTS$TTSVisemeEvent;


# instance fields
.field private mSystemPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "svoxtts"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/svox/classic/TTS$TTSSpeechData;)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/svox/classic/TTS;->mSystemPath:Ljava/lang/String;

    :goto_0
    sput-object p2, Lcom/svox/classic/TTS;->mSpeechListener:Lcom/svox/classic/TTS$TTSSpeechData;

    sput-object v2, Lcom/svox/classic/TTS;->mLoadListener:Lcom/svox/classic/TTS$TTSLoadEvent;

    sput-object v2, Lcom/svox/classic/TTS;->mPosListener:Lcom/svox/classic/TTS$TTSPositionEvent;

    sput-object v2, Lcom/svox/classic/TTS;->mVisemeListener:Lcom/svox/classic/TTS$TTSVisemeEvent;

    sput-object v2, Lcom/svox/classic/TTS;->mBreakListener:Lcom/svox/classic/TTS$TTSBreakEvent;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/svox/classic/TTS;->mSystemPath:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/svox/classic/TTS;->native_setup(Ljava/lang/Object;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to initialize SVOX system. Error code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/svox/classic/TTS;->mSystemPath:Ljava/lang/String;

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic access$0()Lcom/svox/classic/TTS$TTSLoadEvent;
    .locals 1

    sget-object v0, Lcom/svox/classic/TTS;->mLoadListener:Lcom/svox/classic/TTS$TTSLoadEvent;

    return-object v0
.end method

.method static synthetic access$1()Lcom/svox/classic/TTS$TTSSpeechData;
    .locals 1

    sget-object v0, Lcom/svox/classic/TTS;->mSpeechListener:Lcom/svox/classic/TTS$TTSSpeechData;

    return-object v0
.end method

.method private native native_finalize()V
.end method

.method private final native native_getOutputType(JJ)I
.end method

.method private final native native_getSynthModifInt(JJI)I
.end method

.method private final native native_getSynthModifStr(JJI)Ljava/lang/String;
.end method

.method private final native native_putSynthModifInt(JJII)I
.end method

.method private final native native_putSynthModifStr(JJILjava/lang/String;)I
.end method

.method private final native native_setOutputType(JJI)I
.end method

.method private native native_setup(Ljava/lang/Object;Ljava/lang/String;)I
.end method

.method private static receiveBreak(III)V
    .locals 2

    sget-object v0, Lcom/svox/classic/TTS;->mBreakListener:Lcom/svox/classic/TTS$TTSBreakEvent;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/svox/classic/TTS;->mBreakListener:Lcom/svox/classic/TTS$TTSBreakEvent;

    invoke-static {}, Lcom/svox/classic/TTS$TTSBreakType;->values()[Lcom/svox/classic/TTS$TTSBreakType;

    move-result-object v1

    aget-object v1, v1, p0

    invoke-interface {v0, v1, p1, p2}, Lcom/svox/classic/TTS$TTSBreakEvent;->onBreak(Lcom/svox/classic/TTS$TTSBreakType;II)V

    :cond_0
    return-void
.end method

.method private static receivePosMarker(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/svox/classic/TTS;->mPosListener:Lcom/svox/classic/TTS$TTSPositionEvent;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/svox/classic/TTS;->mPosListener:Lcom/svox/classic/TTS$TTSPositionEvent;

    invoke-interface {v0, p0}, Lcom/svox/classic/TTS$TTSPositionEvent;->onPositionMarker(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static receiveSpeechData([S)V
    .locals 1

    sget-object v0, Lcom/svox/classic/TTS;->mSpeechListener:Lcom/svox/classic/TTS$TTSSpeechData;

    invoke-interface {v0, p0}, Lcom/svox/classic/TTS$TTSSpeechData;->onSpeechData([S)V

    return-void
.end method

.method private static receiveViseme(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 6

    sget-object v0, Lcom/svox/classic/TTS;->mVisemeListener:Lcom/svox/classic/TTS$TTSVisemeEvent;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/svox/classic/TTS;->mVisemeListener:Lcom/svox/classic/TTS$TTSVisemeEvent;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/svox/classic/TTS$TTSVisemeEvent;->onViseme(Ljava/lang/String;ILjava/lang/String;II)V

    :cond_0
    return-void
.end method

.method private final native setBreakListener(Z)V
.end method

.method private final native setPosListener(Z)V
.end method

.method private final native setVisemeListener(Z)V
.end method


# virtual methods
.method public final native abort(JJ)I
.end method

.method public final native checkConfiguration(Ljava/lang/String;)I
.end method

.method public final native closeChannel(JJ)I
.end method

.method public final native closeEngine(J)I
.end method

.method public final native detectLanguage(JLjava/lang/String;)[Lcom/svox/classic/TTS$TTSDetectedLanguage;
.end method

.method protected finalize()V
    .locals 0

    invoke-direct {p0}, Lcom/svox/classic/TTS;->native_finalize()V

    return-void
.end method

.method public final native findConfigurationMainFile(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public final native findLingwareFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public final native getAbortionState(JJ)Z
.end method

.method public final native getContextNames(JLjava/lang/String;)[Ljava/lang/String;
.end method

.method public final native getDetectableLanguageInfo()[Ljava/lang/String;
.end method

.method public final native getEngineStatusMessage(J)Ljava/lang/String;
.end method

.method public final native getEngineWarnings(J)[Lcom/svox/classic/TTS$TTSWarning;
.end method

.method public final native getFastG2PMode(JJ)Z
.end method

.method public final native getLingwareInfo(Ljava/lang/String;)Lcom/svox/classic/TTS$TTSLingwareInfo;
.end method

.method public final native getLingwareVoiceInfo(Ljava/lang/String;)[Lcom/svox/classic/TTS$TTSVoiceInfo;
.end method

.method public final native getOutputFile(JJ)Ljava/lang/String;
.end method

.method public getOutputType(JJ)Lcom/svox/classic/TTS$TTSOutputType;
    .locals 4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/svox/classic/TTS;->native_getOutputType(JJ)I

    move-result v0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in native getOutputType call : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {}, Lcom/svox/classic/TTS$TTSOutputType;->values()[Lcom/svox/classic/TTS$TTSOutputType;

    move-result-object v1

    aget-object v0, v1, v0

    return-object v0
.end method

.method public final native getPhonAlphabetsInfo(I)[Ljava/lang/String;
.end method

.method public getSynthModifInt(JJLcom/svox/classic/TTS$TTSSynthIntModifiers;)I
    .locals 6

    invoke-virtual {p5}, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->ordinal()I

    move-result v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/svox/classic/TTS;->native_getSynthModifInt(JJI)I

    move-result v0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in native call : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return v0
.end method

.method public getSynthModifStr(JJLcom/svox/classic/TTS$TTSSynthStrModifiers;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p5}, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->ordinal()I

    move-result v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/svox/classic/TTS;->native_getSynthModifStr(JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final native getSystemInfo()Lcom/svox/classic/TTS$TTSSystemInfo;
.end method

.method public final native getSystemStatusMessage()Ljava/lang/String;
.end method

.method public final native getSystemWarnings()[Lcom/svox/classic/TTS$TTSWarning;
.end method

.method public native getVoiceConfigurationInfo(Ljava/lang/String;)Lcom/svox/classic/TTS$TTSConfigurationInfo;
.end method

.method public native getVoiceConfigurationNames()[Ljava/lang/String;
.end method

.method public final native getVoicesInfo()[Lcom/svox/classic/TTS$TTSVoiceInfo;
.end method

.method public final native loadConfiguration(Ljava/lang/String;)J
.end method

.method public loadConfigurationAsync(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/svox/classic/TTS$2;

    invoke-direct {v1, p0, p1}, Lcom/svox/classic/TTS$2;-><init>(Lcom/svox/classic/TTS;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public native loadLingware(Ljava/lang/String;Ljava/lang/String;)J
.end method

.method public loadLingwareAsync(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/svox/classic/TTS$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/svox/classic/TTS$1;-><init>(Lcom/svox/classic/TTS;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final native newChannel(JLjava/lang/String;)J
.end method

.method public final native newEngine(Ljava/lang/String;)J
.end method

.method public final native putPosMarker(JJLjava/lang/String;)I
.end method

.method public putSynthModifInt(JJLcom/svox/classic/TTS$TTSSynthIntModifiers;I)I
    .locals 7

    invoke-virtual {p5}, Lcom/svox/classic/TTS$TTSSynthIntModifiers;->ordinal()I

    move-result v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/svox/classic/TTS;->native_putSynthModifInt(JJII)I

    move-result v0

    return v0
.end method

.method public putSynthModifStr(JJLcom/svox/classic/TTS$TTSSynthStrModifiers;Ljava/lang/String;)I
    .locals 7

    invoke-virtual {p5}, Lcom/svox/classic/TTS$TTSSynthStrModifiers;->ordinal()I

    move-result v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/svox/classic/TTS;->native_putSynthModifStr(JJILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final native resetAbort(JJ)I
.end method

.method public final native resetBreakCounts(JJ)I
.end method

.method public setBreakEventListener(Lcom/svox/classic/TTS$TTSBreakEvent;)V
    .locals 1

    sput-object p1, Lcom/svox/classic/TTS;->mBreakListener:Lcom/svox/classic/TTS$TTSBreakEvent;

    sget-object v0, Lcom/svox/classic/TTS;->mBreakListener:Lcom/svox/classic/TTS$TTSBreakEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/svox/classic/TTS;->setBreakListener(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/svox/classic/TTS;->setBreakListener(Z)V

    goto :goto_0
.end method

.method public final native setFastG2PMode(JJZ)I
.end method

.method public setLoadEventListener(Lcom/svox/classic/TTS$TTSLoadEvent;)V
    .locals 0

    sput-object p1, Lcom/svox/classic/TTS;->mLoadListener:Lcom/svox/classic/TTS$TTSLoadEvent;

    return-void
.end method

.method public final native setOutputFile(JJLjava/lang/String;)I
.end method

.method public setOutputType(JJLcom/svox/classic/TTS$TTSOutputType;)I
    .locals 6

    invoke-virtual {p5}, Lcom/svox/classic/TTS$TTSOutputType;->ordinal()I

    move-result v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/svox/classic/TTS;->native_setOutputType(JJI)I

    move-result v0

    return v0
.end method

.method public setPositionMarkerListener(Lcom/svox/classic/TTS$TTSPositionEvent;)V
    .locals 1

    sput-object p1, Lcom/svox/classic/TTS;->mPosListener:Lcom/svox/classic/TTS$TTSPositionEvent;

    sget-object v0, Lcom/svox/classic/TTS;->mPosListener:Lcom/svox/classic/TTS$TTSPositionEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/svox/classic/TTS;->setPosListener(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/svox/classic/TTS;->setPosListener(Z)V

    goto :goto_0
.end method

.method public final native setVisemeAlphabet(JJLjava/lang/String;)I
.end method

.method public setVisemeEventListener(Lcom/svox/classic/TTS$TTSVisemeEvent;)V
    .locals 1

    sput-object p1, Lcom/svox/classic/TTS;->mVisemeListener:Lcom/svox/classic/TTS$TTSVisemeEvent;

    sget-object v0, Lcom/svox/classic/TTS;->mVisemeListener:Lcom/svox/classic/TTS$TTSVisemeEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/svox/classic/TTS;->setVisemeListener(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/svox/classic/TTS;->setPosListener(Z)V

    goto :goto_0
.end method

.method public final native synthFinish(JJ)I
.end method

.method public final native synthItemFile(JJLjava/lang/String;)I
.end method

.method public final native synthStr(JJLjava/lang/String;)I
.end method

.method public final native synthString(JJLjava/lang/String;)I
.end method

.method public synthStringAsync(JJLjava/lang/String;)V
    .locals 8

    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Lcom/svox/classic/TTS$3;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/svox/classic/TTS$3;-><init>(Lcom/svox/classic/TTS;JJLjava/lang/String;)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final native synthTextFile(JJLjava/lang/String;)I
.end method

.method public final native unloadConfiguration(J)I
.end method

.method public final native unloadLingware(J)I
.end method
