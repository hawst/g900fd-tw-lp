.class Lcom/svox/classic/TTS$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/svox/classic/TTS;

.field private final synthetic val$channel:J

.field private final synthetic val$engine:J

.field private final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/svox/classic/TTS;JJLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/svox/classic/TTS$3;->this$0:Lcom/svox/classic/TTS;

    iput-wide p2, p0, Lcom/svox/classic/TTS$3;->val$engine:J

    iput-wide p4, p0, Lcom/svox/classic/TTS$3;->val$channel:J

    iput-object p6, p0, Lcom/svox/classic/TTS$3;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/svox/classic/TTS$3;->this$0:Lcom/svox/classic/TTS;

    iget-wide v1, p0, Lcom/svox/classic/TTS$3;->val$engine:J

    iget-wide v3, p0, Lcom/svox/classic/TTS$3;->val$channel:J

    iget-object v5, p0, Lcom/svox/classic/TTS$3;->val$text:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/svox/classic/TTS;->synthString(JJLjava/lang/String;)I

    move-result v0

    # getter for: Lcom/svox/classic/TTS;->mSpeechListener:Lcom/svox/classic/TTS$TTSSpeechData;
    invoke-static {}, Lcom/svox/classic/TTS;->access$1()Lcom/svox/classic/TTS$TTSSpeechData;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/svox/classic/TTS;->mSpeechListener:Lcom/svox/classic/TTS$TTSSpeechData;
    invoke-static {}, Lcom/svox/classic/TTS;->access$1()Lcom/svox/classic/TTS$TTSSpeechData;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/svox/classic/TTS$TTSSpeechData;->onSpeechDone(I)V

    :cond_0
    return-void
.end method
