.class public Lcom/svox/classic/TTS$TTSSystemInfo;
.super Ljava/lang/Object;


# instance fields
.field private mPlatform:Ljava/lang/String;

.field private mSystemID:I

.field private mVersion:Ljava/lang/String;

.field private mVersionMajor:I

.field private mVersionMinor:I

.field private mVersionRevision:I

.field private mVersionSubrevision:Ljava/lang/String;

.field final synthetic this$0:Lcom/svox/classic/TTS;


# direct methods
.method public constructor <init>(Lcom/svox/classic/TTS;Ljava/lang/String;IIILjava/lang/String;I)V
    .locals 2

    iput-object p1, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->this$0:Lcom/svox/classic/TTS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mPlatform:Ljava/lang/String;

    iput p3, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionMajor:I

    iput p4, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionMinor:I

    iput p5, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionRevision:I

    iput-object p6, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionSubrevision:Ljava/lang/String;

    iput p7, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mSystemID:I

    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionMajor:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionMinor:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionRevision:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionSubrevision:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersion:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public platform()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mPlatform:Ljava/lang/String;

    return-object v0
.end method

.method public system_id()I
    .locals 1

    iget v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mSystemID:I

    return v0
.end method

.method public version()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public version_major()I
    .locals 1

    iget v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionMajor:I

    return v0
.end method

.method public version_minor()I
    .locals 1

    iget v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionMinor:I

    return v0
.end method

.method public version_revision()I
    .locals 1

    iget v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionRevision:I

    return v0
.end method

.method public version_subrevision()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/svox/classic/TTS$TTSSystemInfo;->mVersionSubrevision:Ljava/lang/String;

    return-object v0
.end method
