.class public interface abstract Lcom/android/org/chromium/content_public/browser/WebContents;
.super Ljava/lang/Object;
.source "WebContents.java"


# virtual methods
.method public abstract getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getVisibleUrl()Ljava/lang/String;
.end method

.method public abstract stop()V
.end method
