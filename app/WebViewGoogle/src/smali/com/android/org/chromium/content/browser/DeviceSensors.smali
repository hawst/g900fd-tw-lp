.class Lcom/android/org/chromium/content/browser/DeviceSensors;
.super Ljava/lang/Object;
.source "DeviceSensors.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxyImpl;,
        Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;
    }
.end annotation


# static fields
.field static final DEVICE_MOTION_SENSORS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final DEVICE_ORIENTATION_SENSORS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sSingleton:Lcom/android/org/chromium/content/browser/DeviceSensors;

.field private static sSingletonLock:Ljava/lang/Object;


# instance fields
.field final mActiveSensors:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppContext:Landroid/content/Context;

.field mDeviceMotionIsActive:Z

.field mDeviceOrientationIsActive:Z

.field private mHandler:Landroid/os/Handler;

.field private final mHandlerLock:Ljava/lang/Object;

.field private mNativePtr:J

.field private final mNativePtrLock:Ljava/lang/Object;

.field private mSensorManagerProxy:Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

.field private mTruncatedRotationVector:[F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/org/chromium/content/browser/DeviceSensors;->sSingletonLock:Ljava/lang/Object;

    .line 70
    new-array v0, v2, [Ljava/lang/Integer;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/android/org/chromium/base/CollectionUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_ORIENTATION_SENSORS:Ljava/util/Set;

    .line 73
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/android/org/chromium/base/CollectionUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_MOTION_SENSORS:Ljava/util/Set;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mHandlerLock:Ljava/lang/Object;

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    .line 80
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceMotionIsActive:Z

    .line 81
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceOrientationIsActive:Z

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mAppContext:Landroid/content/Context;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/DeviceSensors;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DeviceSensors;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method public static computeDeviceOrientationFromRotationMatrix([F[D)[D
    .locals 9
    .param p0, "R"    # [F
    .param p1, "values"    # [D

    .prologue
    const/4 v5, 0x7

    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 266
    array-length v0, p0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-object p1

    .line 269
    :cond_1
    const/16 v0, 0x8

    aget v0, p0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 270
    aget v0, p0, v6

    neg-float v0, v0

    float-to-double v0, v0

    const/4 v2, 0x4

    aget v2, p0, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v7

    .line 271
    aget v0, p0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    aput-wide v0, p1, v6

    .line 272
    const/4 v0, 0x6

    aget v0, p0, v0

    neg-float v0, v0

    float-to-double v0, v0

    const/16 v2, 0x8

    aget v2, p0, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v8

    .line 297
    :goto_1
    aget-wide v0, p1, v7

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 298
    aget-wide v0, p1, v7

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v0, v2

    aput-wide v0, p1, v7

    goto :goto_0

    .line 273
    :cond_2
    const/16 v0, 0x8

    aget v0, p0, v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_4

    .line 274
    aget v0, p0, v6

    float-to-double v0, v0

    const/4 v2, 0x4

    aget v2, p0, v2

    neg-float v2, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v7

    .line 275
    aget v0, p0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    neg-double v0, v0

    aput-wide v0, p1, v6

    .line 276
    aget-wide v2, p1, v6

    aget-wide v0, p1, v6

    const-wide/16 v4, 0x0

    cmpl-double v0, v0, v4

    if-ltz v0, :cond_3

    const-wide v0, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    :goto_2
    add-double/2addr v0, v2

    aput-wide v0, p1, v6

    .line 277
    const/4 v0, 0x6

    aget v0, p0, v0

    float-to-double v0, v0

    const/16 v2, 0x8

    aget v2, p0, v2

    neg-float v2, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v8

    goto :goto_1

    .line 276
    :cond_3
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    goto :goto_2

    .line 279
    :cond_4
    const/4 v0, 0x6

    aget v0, p0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    .line 280
    aget v0, p0, v6

    neg-float v0, v0

    float-to-double v0, v0

    const/4 v2, 0x4

    aget v2, p0, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v7

    .line 281
    aget v0, p0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    aput-wide v0, p1, v6

    .line 282
    const-wide v0, -0x4006de04abbbd2e8L    # -1.5707963267948966

    aput-wide v0, p1, v8

    goto/16 :goto_1

    .line 283
    :cond_5
    const/4 v0, 0x6

    aget v0, p0, v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_7

    .line 284
    aget v0, p0, v6

    float-to-double v0, v0

    const/4 v2, 0x4

    aget v2, p0, v2

    neg-float v2, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v7

    .line 285
    aget v0, p0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    neg-double v0, v0

    aput-wide v0, p1, v6

    .line 286
    aget-wide v2, p1, v6

    aget-wide v0, p1, v6

    const-wide/16 v4, 0x0

    cmpl-double v0, v0, v4

    if-ltz v0, :cond_6

    const-wide v0, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    :goto_3
    add-double/2addr v0, v2

    aput-wide v0, p1, v6

    .line 287
    const-wide v0, -0x4006de04abbbd2e8L    # -1.5707963267948966

    aput-wide v0, p1, v8

    goto/16 :goto_1

    .line 286
    :cond_6
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    goto :goto_3

    .line 290
    :cond_7
    const/4 v0, 0x3

    aget v0, p0, v0

    float-to-double v0, v0

    aget v2, p0, v7

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    aput-wide v0, p1, v7

    .line 291
    aget v0, p0, v5

    cmpl-float v0, v0, v4

    if-lez v0, :cond_8

    const-wide v0, 0x3ff921fb54442d18L    # 1.5707963267948966

    :goto_4
    aput-wide v0, p1, v6

    .line 292
    const-wide/16 v0, 0x0

    aput-wide v0, p1, v8

    goto/16 :goto_1

    .line 291
    :cond_8
    const-wide v0, -0x4006de04abbbd2e8L    # -1.5707963267948966

    goto :goto_4
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 4

    .prologue
    .line 432
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v2

    .line 433
    :try_start_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 434
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DeviceMotionAndOrientation"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 435
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 436
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mHandler:Landroid/os/Handler;

    .line 438
    .end local v0    # "thread":Landroid/os/HandlerThread;
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mHandler:Landroid/os/Handler;

    monitor-exit v2

    return-object v1

    .line 439
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static getInstance(Landroid/content/Context;)Lcom/android/org/chromium/content/browser/DeviceSensors;
    .locals 2
    .param p0, "appContext"    # Landroid/content/Context;

    .prologue
    .line 444
    sget-object v1, Lcom/android/org/chromium/content/browser/DeviceSensors;->sSingletonLock:Ljava/lang/Object;

    monitor-enter v1

    .line 445
    :try_start_0
    sget-object v0, Lcom/android/org/chromium/content/browser/DeviceSensors;->sSingleton:Lcom/android/org/chromium/content/browser/DeviceSensors;

    if-nez v0, :cond_0

    .line 446
    new-instance v0, Lcom/android/org/chromium/content/browser/DeviceSensors;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/DeviceSensors;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/org/chromium/content/browser/DeviceSensors;->sSingleton:Lcom/android/org/chromium/content/browser/DeviceSensors;

    .line 448
    :cond_0
    sget-object v0, Lcom/android/org/chromium/content/browser/DeviceSensors;->sSingleton:Lcom/android/org/chromium/content/browser/DeviceSensors;

    monitor-exit v1

    return-object v0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getOrientationFromRotationVector([F)V
    .locals 9
    .param p1, "rotationVector"    # [F

    .prologue
    .line 304
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 305
    .local v0, "deviceRotationMatrix":[F
    invoke-static {v0, p1}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    .line 307
    const/4 v1, 0x3

    new-array v8, v1, [D

    .line 308
    .local v8, "rotationAngles":[D
    invoke-static {v0, v8}, Lcom/android/org/chromium/content/browser/DeviceSensors;->computeDeviceOrientationFromRotationMatrix([F[D)[D

    .line 310
    const/4 v1, 0x0

    aget-wide v2, v8, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    const/4 v1, 0x1

    aget-wide v4, v8, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    const/4 v1, 0x2

    aget-wide v6, v8, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/DeviceSensors;->gotOrientation(DDD)V

    .line 313
    return-void
.end method

.method private getSensorManagerProxy()Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;
    .locals 2

    .prologue
    .line 316
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mSensorManagerProxy:Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mSensorManagerProxy:Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

    .line 331
    :goto_0
    return-object v1

    .line 320
    :cond_0
    new-instance v1, Lcom/android/org/chromium/content/browser/DeviceSensors$1;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/content/browser/DeviceSensors$1;-><init>(Lcom/android/org/chromium/content/browser/DeviceSensors;)V

    invoke-static {v1}, Lcom/android/org/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 328
    .local v0, "sensorManager":Landroid/hardware/SensorManager;
    if-eqz v0, :cond_1

    .line 329
    new-instance v1, Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxyImpl;

    invoke-direct {v1, v0}, Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxyImpl;-><init>(Landroid/hardware/SensorManager;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mSensorManagerProxy:Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

    .line 331
    :cond_1
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mSensorManagerProxy:Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

    goto :goto_0
.end method

.method private native nativeGotAcceleration(JDDD)V
.end method

.method private native nativeGotAccelerationIncludingGravity(JDDD)V
.end method

.method private native nativeGotOrientation(JDDD)V
.end method

.method private native nativeGotRotationRate(JDDD)V
.end method

.method private registerForSensorType(II)Z
    .locals 3
    .param p1, "type"    # I
    .param p2, "rateInMilliseconds"    # I

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/DeviceSensors;->getSensorManagerProxy()Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

    move-result-object v1

    .line 389
    .local v1, "sensorManager":Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;
    if-nez v1, :cond_0

    .line 390
    const/4 v2, 0x0

    .line 393
    :goto_0
    return v2

    .line 392
    :cond_0
    mul-int/lit16 v0, p2, 0x3e8

    .line 393
    .local v0, "rateInMicroseconds":I
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/DeviceSensors;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-interface {v1, p0, p1, v0, v2}, Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;->registerListener(Landroid/hardware/SensorEventListener;IILandroid/os/Handler;)Z

    move-result v2

    goto :goto_0
.end method

.method private registerSensors(Ljava/util/Set;IZ)Z
    .locals 6
    .param p2, "rateInMilliseconds"    # I
    .param p3, "failOnMissingSensor"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;IZ)Z"
        }
    .end annotation

    .prologue
    .line 359
    .local p1, "sensorTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 360
    .local v3, "sensorsToActivate":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-interface {v3, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 361
    const/4 v4, 0x0

    .line 363
    .local v4, "success":Z
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 364
    .local v2, "sensorType":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v5, p2}, Lcom/android/org/chromium/content/browser/DeviceSensors;->registerForSensorType(II)Z

    move-result v1

    .line 365
    .local v1, "result":Z
    if-nez v1, :cond_2

    if-eqz p3, :cond_2

    .line 367
    invoke-direct {p0, v3}, Lcom/android/org/chromium/content/browser/DeviceSensors;->unregisterSensors(Ljava/lang/Iterable;)V

    .line 368
    const/4 v4, 0x0

    .line 375
    .end local v1    # "result":Z
    .end local v2    # "sensorType":Ljava/lang/Integer;
    .end local v4    # "success":Z
    :cond_1
    return v4

    .line 370
    .restart local v1    # "result":Z
    .restart local v2    # "sensorType":Ljava/lang/Integer;
    .restart local v4    # "success":Z
    :cond_2
    if-eqz v1, :cond_0

    .line 371
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 372
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private setEventTypeActive(IZ)V
    .locals 0
    .param p1, "eventType"    # I
    .param p2, "value"    # Z

    .prologue
    .line 340
    packed-switch p1, :pswitch_data_0

    .line 348
    :goto_0
    return-void

    .line 342
    :pswitch_0
    iput-boolean p2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceOrientationIsActive:Z

    goto :goto_0

    .line 345
    :pswitch_1
    iput-boolean p2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceMotionIsActive:Z

    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private unregisterSensors(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 379
    .local p1, "sensorTypes":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 380
    .local v1, "sensorType":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 381
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/DeviceSensors;->getSensorManagerProxy()Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2, p0, v3}, Lcom/android/org/chromium/content/browser/DeviceSensors$SensorManagerProxy;->unregisterListener(Landroid/hardware/SensorEventListener;I)V

    .line 382
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 385
    .end local v1    # "sensorType":Ljava/lang/Integer;
    :cond_1
    return-void
.end method


# virtual methods
.method public getNumberActiveDeviceMotionSensors()I
    .locals 3

    .prologue
    .line 125
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_MOTION_SENSORS:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 126
    .local v0, "deviceMotionSensors":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 127
    sget-object v1, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_MOTION_SENSORS:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    return v1
.end method

.method protected gotAcceleration(DDD)V
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D

    .prologue
    .line 405
    iget-object v10, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v10

    .line 406
    :try_start_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 407
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/android/org/chromium/content/browser/DeviceSensors;->nativeGotAcceleration(JDDD)V

    .line 409
    :cond_0
    monitor-exit v10

    .line 410
    return-void

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected gotAccelerationIncludingGravity(DDD)V
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D

    .prologue
    .line 413
    iget-object v10, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v10

    .line 414
    :try_start_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 415
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/android/org/chromium/content/browser/DeviceSensors;->nativeGotAccelerationIncludingGravity(JDDD)V

    .line 417
    :cond_0
    monitor-exit v10

    .line 418
    return-void

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected gotOrientation(DDD)V
    .locals 11
    .param p1, "alpha"    # D
    .param p3, "beta"    # D
    .param p5, "gamma"    # D

    .prologue
    .line 397
    iget-object v10, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v10

    .line 398
    :try_start_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 399
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/android/org/chromium/content/browser/DeviceSensors;->nativeGotOrientation(JDDD)V

    .line 401
    :cond_0
    monitor-exit v10

    .line 402
    return-void

    .line 401
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected gotRotationRate(DDD)V
    .locals 11
    .param p1, "alpha"    # D
    .param p3, "beta"    # D
    .param p5, "gamma"    # D

    .prologue
    .line 421
    iget-object v10, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v10

    .line 422
    :try_start_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 423
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/android/org/chromium/content/browser/DeviceSensors;->nativeGotRotationRate(JDDD)V

    .line 425
    :cond_0
    monitor-exit v10

    .line 426
    return-void

    .line 425
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 172
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 176
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {p0, v0, v1}, Lcom/android/org/chromium/content/browser/DeviceSensors;->sensorChanged(I[F)V

    .line 177
    return-void
.end method

.method sensorChanged(I[F)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "values"    # [F

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 181
    sparse-switch p1, :sswitch_data_0

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 183
    :sswitch_0
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceMotionIsActive:Z

    if-eqz v0, :cond_0

    .line 184
    aget v0, p2, v1

    float-to-double v2, v0

    aget v0, p2, v4

    float-to-double v4, v0

    aget v0, p2, v6

    float-to-double v6, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/DeviceSensors;->gotAccelerationIncludingGravity(DDD)V

    goto :goto_0

    .line 188
    :sswitch_1
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceMotionIsActive:Z

    if-eqz v0, :cond_0

    .line 189
    aget v0, p2, v1

    float-to-double v2, v0

    aget v0, p2, v4

    float-to-double v4, v0

    aget v0, p2, v6

    float-to-double v6, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/DeviceSensors;->gotAcceleration(DDD)V

    goto :goto_0

    .line 193
    :sswitch_2
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceMotionIsActive:Z

    if-eqz v0, :cond_0

    .line 194
    aget v0, p2, v1

    float-to-double v2, v0

    aget v0, p2, v4

    float-to-double v4, v0

    aget v0, p2, v6

    float-to-double v6, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/DeviceSensors;->gotRotationRate(DDD)V

    goto :goto_0

    .line 198
    :sswitch_3
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceOrientationIsActive:Z

    if-eqz v0, :cond_0

    .line 199
    array-length v0, p2

    if-le v0, v2, :cond_2

    .line 204
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mTruncatedRotationVector:[F

    if-nez v0, :cond_1

    .line 205
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mTruncatedRotationVector:[F

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mTruncatedRotationVector:[F

    invoke-static {p2, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 208
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mTruncatedRotationVector:[F

    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/DeviceSensors;->getOrientationFromRotationVector([F)V

    goto :goto_0

    .line 210
    :cond_2
    invoke-direct {p0, p2}, Lcom/android/org/chromium/content/browser/DeviceSensors;->getOrientationFromRotationVector([F)V

    goto :goto_0

    .line 181
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_2
        0xa -> :sswitch_1
        0xb -> :sswitch_3
    .end sparse-switch
.end method

.method public start(JII)Z
    .locals 7
    .param p1, "nativePtr"    # J
    .param p3, "eventType"    # I
    .param p4, "rateInMilliseconds"    # I

    .prologue
    const/4 v1, 0x0

    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "success":Z
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v2

    .line 102
    packed-switch p3, :pswitch_data_0

    .line 112
    :try_start_0
    const-string v3, "DeviceMotionAndOrientation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown event type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    monitor-exit v2

    .line 119
    :goto_0
    return v1

    .line 104
    :pswitch_0
    sget-object v1, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_ORIENTATION_SENSORS:Ljava/util/Set;

    const/4 v3, 0x1

    invoke-direct {p0, v1, p4, v3}, Lcom/android/org/chromium/content/browser/DeviceSensors;->registerSensors(Ljava/util/Set;IZ)Z

    move-result v0

    .line 115
    :goto_1
    if-eqz v0, :cond_0

    .line 116
    iput-wide p1, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    .line 117
    const/4 v1, 0x1

    invoke-direct {p0, p3, v1}, Lcom/android/org/chromium/content/browser/DeviceSensors;->setEventTypeActive(IZ)V

    .line 119
    :cond_0
    monitor-exit v2

    move v1, v0

    goto :goto_0

    .line 109
    :pswitch_1
    sget-object v1, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_MOTION_SENSORS:Ljava/util/Set;

    const/4 v3, 0x0

    invoke-direct {p0, v1, p4, v3}, Lcom/android/org/chromium/content/browser/DeviceSensors;->registerSensors(Ljava/util/Set;IZ)Z

    move-result v0

    .line 110
    goto :goto_1

    .line 120
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public stop(I)V
    .locals 6
    .param p1, "eventType"    # I

    .prologue
    .line 141
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 142
    .local v1, "sensorsToRemainActive":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v3

    .line 143
    packed-switch p1, :pswitch_data_0

    .line 155
    :try_start_0
    const-string v2, "DeviceMotionAndOrientation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown event type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    monitor-exit v3

    .line 167
    :goto_0
    return-void

    .line 145
    :pswitch_0
    iget-boolean v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceMotionIsActive:Z

    if-eqz v2, :cond_0

    .line 146
    sget-object v2, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_MOTION_SENSORS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 159
    :cond_0
    :goto_1
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 160
    .local v0, "sensorsToDeactivate":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 161
    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/DeviceSensors;->unregisterSensors(Ljava/lang/Iterable;)V

    .line 162
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/android/org/chromium/content/browser/DeviceSensors;->setEventTypeActive(IZ)V

    .line 163
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mActiveSensors:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 164
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mNativePtr:J

    .line 166
    :cond_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "sensorsToDeactivate":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 150
    :pswitch_1
    :try_start_1
    iget-boolean v2, p0, Lcom/android/org/chromium/content/browser/DeviceSensors;->mDeviceOrientationIsActive:Z

    if-eqz v2, :cond_0

    .line 151
    sget-object v2, Lcom/android/org/chromium/content/browser/DeviceSensors;->DEVICE_ORIENTATION_SENSORS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
