.class public Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
.super Ljava/lang/Object;
.source "DownloadInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/DownloadInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mContentDisposition:Ljava/lang/String;

.field private mContentLength:J

.field private mCookie:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mDownloadId:I

.field private mFileName:Ljava/lang/String;

.field private mFilePath:Ljava/lang/String;

.field private mHasDownloadId:Z

.field private mIsGETRequest:Z

.field private mIsSuccessful:Z

.field private mMimeType:Ljava/lang/String;

.field private mPercentCompleted:I

.field private mReferer:Ljava/lang/String;

.field private mTimeRemainingInMillis:J

.field private mUrl:Ljava/lang/String;

.field private mUserAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/android/org/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mPercentCompleted:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mDownloadId:I

    return v0
.end method

.method static synthetic access$1100(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mIsSuccessful:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mIsGETRequest:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mContentDisposition:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mPercentCompleted:I

    return v0
.end method

.method static synthetic access$1500(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mTimeRemainingInMillis:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mCookie:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mReferer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mContentLength:J

    return-wide v0
.end method

.method static synthetic access$900(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mHasDownloadId:Z

    return v0
.end method


# virtual methods
.method public build()Lcom/android/org/chromium/content/browser/DownloadInfo;
    .locals 2

    .prologue
    .line 214
    new-instance v0, Lcom/android/org/chromium/content/browser/DownloadInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/org/chromium/content/browser/DownloadInfo;-><init>(Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;Lcom/android/org/chromium/content/browser/DownloadInfo$1;)V

    return-object v0
.end method

.method public setContentDisposition(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "contentDisposition"    # Ljava/lang/String;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mContentDisposition:Ljava/lang/String;

    .line 199
    return-object p0
.end method

.method public setContentLength(J)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 1
    .param p1, "contentLength"    # J

    .prologue
    .line 173
    iput-wide p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mContentLength:J

    .line 174
    return-object p0
.end method

.method public setCookie(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "cookie"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mCookie:Ljava/lang/String;

    .line 149
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mDescription:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public setDownloadId(I)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "downloadId"    # I

    .prologue
    .line 188
    iput p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mDownloadId:I

    .line 189
    return-object p0
.end method

.method public setFileName(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mFileName:Ljava/lang/String;

    .line 154
    return-object p0
.end method

.method public setFilePath(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mFilePath:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public setHasDownloadId(Z)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "hasDownloadId"    # Z

    .prologue
    .line 183
    iput-boolean p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mHasDownloadId:Z

    .line 184
    return-object p0
.end method

.method public setIsGETRequest(Z)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "isGETRequest"    # Z

    .prologue
    .line 178
    iput-boolean p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mIsGETRequest:Z

    .line 179
    return-object p0
.end method

.method public setIsSuccessful(Z)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "isSuccessful"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mIsSuccessful:Z

    .line 194
    return-object p0
.end method

.method public setMimeType(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mMimeType:Ljava/lang/String;

    .line 144
    return-object p0
.end method

.method public setPercentCompleted(I)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 1
    .param p1, "percentCompleted"    # I

    .prologue
    .line 203
    sget-boolean v0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204
    :cond_0
    iput p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mPercentCompleted:I

    .line 205
    return-object p0
.end method

.method public setReferer(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "referer"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mReferer:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public setTimeRemainingInMillis(J)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 1
    .param p1, "timeRemainingInMillis"    # J

    .prologue
    .line 209
    iput-wide p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mTimeRemainingInMillis:J

    .line 210
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mUrl:Ljava/lang/String;

    .line 134
    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;
    .locals 0
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/DownloadInfo$Builder;->mUserAgent:Ljava/lang/String;

    .line 139
    return-object p0
.end method
