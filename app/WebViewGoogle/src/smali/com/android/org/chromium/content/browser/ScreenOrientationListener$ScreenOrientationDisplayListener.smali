.class Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationDisplayListener;
.super Ljava/lang/Object;
.source "ScreenOrientationListener.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;
.implements Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationListenerBackend;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/ScreenOrientationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenOrientationDisplayListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;


# direct methods
.method private constructor <init>(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationDisplayListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;Lcom/android/org/chromium/content/browser/ScreenOrientationListener$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/org/chromium/content/browser/ScreenOrientationListener;
    .param p2, "x1"    # Lcom/android/org/chromium/content/browser/ScreenOrientationListener$1;

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationDisplayListener;-><init>(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0
    .param p1, "displayId"    # I

    .prologue
    .line 130
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 1
    .param p1, "displayId"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationDisplayListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    # invokes: Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->notifyObservers()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->access$100(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)V

    .line 139
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0
    .param p1, "displayId"    # I

    .prologue
    .line 134
    return-void
.end method

.method public startListening()V
    .locals 3

    .prologue
    .line 114
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationDisplayListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    # getter for: Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->access$000(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 116
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 117
    return-void
.end method

.method public stopListening()V
    .locals 3

    .prologue
    .line 121
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationDisplayListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    # getter for: Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->access$000(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 123
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v0, p0}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 124
    return-void
.end method
