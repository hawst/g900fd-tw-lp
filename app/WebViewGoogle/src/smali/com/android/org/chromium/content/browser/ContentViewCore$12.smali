.class Lcom/android/org/chromium/content/browser/ContentViewCore$12;
.super Ljava/lang/Object;
.source "ContentViewCore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 2288
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 2291
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->allowTextHandleFadeIn()Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$2400(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2293
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$2500(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 2302
    :cond_0
    :goto_0
    return-void

    .line 2295
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$700(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2296
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$2600(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->beginHandleFadeIn()V

    .line 2298
    :cond_2
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$800(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$2700(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->beginHandleFadeIn()V

    goto :goto_0
.end method
