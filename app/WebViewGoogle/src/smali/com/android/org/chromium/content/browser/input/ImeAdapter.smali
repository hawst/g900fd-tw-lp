.class public Lcom/android/org/chromium/content/browser/input/ImeAdapter;
.super Ljava/lang/Object;
.source "ImeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;,
        Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;,
        Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;
    }
.end annotation


# static fields
.field static sEventTypeChar:I

.field static sEventTypeKeyUp:I

.field static sEventTypeRawKeyDown:I

.field static sModifierAlt:I

.field static sModifierCapsLockOn:I

.field static sModifierCtrl:I

.field static sModifierNumLockOn:I

.field static sModifierShift:I

.field static sTextInputTypeContentEditable:I

.field static sTextInputTypeEmail:I

.field static sTextInputTypeNone:I

.field static sTextInputTypeNumber:I

.field static sTextInputTypePassword:I

.field static sTextInputTypeSearch:I

.field static sTextInputTypeTel:I

.field static sTextInputTypeText:I

.field static sTextInputTypeTextArea:I

.field static sTextInputTypeUrl:I


# instance fields
.field private mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

.field private final mHandler:Landroid/os/Handler;

.field private mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

.field private mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

.field mIsShowWithoutHideOutstanding:Z

.field private mNativeImeAdapterAndroid:J

.field private mTextInputType:I

.field private final mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;


# direct methods
.method public constructor <init>(Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;)V
    .locals 1
    .param p1, "wrapper"    # Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;
    .param p2, "embedder"    # Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mIsShowWithoutHideOutstanding:Z

    .line 137
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    .line 138
    iput-object p2, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mHandler:Landroid/os/Handler;

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/input/ImeAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/ImeAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->dismissInput(Z)V

    return-void
.end method

.method private cancelComposition()V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->restartInput()V

    .line 548
    :cond_0
    return-void
.end method

.method private dismissInput(Z)V
    .locals 5
    .param p1, "unzoomIfNeeded"    # Z

    .prologue
    const/4 v4, 0x0

    .line 280
    iput-boolean v4, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mIsShowWithoutHideOutstanding:Z

    .line 281
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->getAttachedView()Landroid/view/View;

    move-result-object v0

    .line 282
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->getNewShowKeyboardReceiver()Landroid/os/ResultReceiver;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v4, v1}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->onDismissInput()V

    .line 287
    return-void

    .line 283
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private focusedNodeChanged(Z)V
    .locals 1
    .param p1, "isEditable"    # Z

    .prologue
    .line 523
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->restartInput()V

    .line 524
    :cond_0
    return-void
.end method

.method private static getModifiers(I)I
    .locals 2
    .param p0, "metaState"    # I

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "modifiers":I
    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    .line 197
    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierShift:I

    or-int/2addr v0, v1

    .line 199
    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    .line 200
    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierAlt:I

    or-int/2addr v0, v1

    .line 202
    :cond_1
    and-int/lit16 v1, p0, 0x1000

    if-eqz v1, :cond_2

    .line 203
    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierCtrl:I

    or-int/2addr v0, v1

    .line 205
    :cond_2
    const/high16 v1, 0x100000

    and-int/2addr v1, p0

    if-eqz v1, :cond_3

    .line 206
    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierCapsLockOn:I

    or-int/2addr v0, v1

    .line 208
    :cond_3
    const/high16 v1, 0x200000

    and-int/2addr v1, p0

    if-eqz v1, :cond_4

    .line 209
    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierNumLockOn:I

    or-int/2addr v0, v1

    .line 211
    :cond_4
    return v0
.end method

.method public static getTextInputTypeNone()I
    .locals 1

    .prologue
    .line 191
    sget v0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    return v0
.end method

.method private hasInputType()Z
    .locals 2

    .prologue
    .line 290
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initializeTextInputTypes(IIIIIIIIII)V
    .locals 0
    .param p0, "textInputTypeNone"    # I
    .param p1, "textInputTypeText"    # I
    .param p2, "textInputTypeTextArea"    # I
    .param p3, "textInputTypePassword"    # I
    .param p4, "textInputTypeSearch"    # I
    .param p5, "textInputTypeUrl"    # I
    .param p6, "textInputTypeEmail"    # I
    .param p7, "textInputTypeTel"    # I
    .param p8, "textInputTypeNumber"    # I
    .param p9, "textInputTypeContentEditable"    # I

    .prologue
    .line 509
    sput p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    .line 510
    sput p1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeText:I

    .line 511
    sput p2, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeTextArea:I

    .line 512
    sput p3, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypePassword:I

    .line 513
    sput p4, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeSearch:I

    .line 514
    sput p5, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeUrl:I

    .line 515
    sput p6, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeEmail:I

    .line 516
    sput p7, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeTel:I

    .line 517
    sput p8, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNumber:I

    .line 518
    sput p9, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeContentEditable:I

    .line 519
    return-void
.end method

.method private static initializeWebInputEvents(IIIIIIII)V
    .locals 0
    .param p0, "eventTypeRawKeyDown"    # I
    .param p1, "eventTypeKeyUp"    # I
    .param p2, "eventTypeChar"    # I
    .param p3, "modifierShift"    # I
    .param p4, "modifierAlt"    # I
    .param p5, "modifierCtrl"    # I
    .param p6, "modifierCapsLockOn"    # I
    .param p7, "modifierNumLockOn"    # I

    .prologue
    .line 494
    sput p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sEventTypeRawKeyDown:I

    .line 495
    sput p1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sEventTypeKeyUp:I

    .line 496
    sput p2, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sEventTypeChar:I

    .line 497
    sput p3, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierShift:I

    .line 498
    sput p4, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierAlt:I

    .line 499
    sput p5, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierCtrl:I

    .line 500
    sput p6, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierCapsLockOn:I

    .line 501
    sput p7, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sModifierNumLockOn:I

    .line 502
    return-void
.end method

.method private static isTextInputType(I)Z
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 294
    sget v0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    if-eq p0, v0, :cond_0

    invoke-static {p0}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->isDialogInputType(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeAppendBackgroundColorSpan(JIII)V
.end method

.method private static native nativeAppendUnderlineSpan(JII)V
.end method

.method private native nativeAttachImeAdapter(J)V
.end method

.method private native nativeCommitText(JLjava/lang/String;)V
.end method

.method private native nativeCopy(J)V
.end method

.method private native nativeCut(J)V
.end method

.method private native nativeDeleteSurroundingText(JII)V
.end method

.method private native nativeFinishComposingText(J)V
.end method

.method private native nativePaste(J)V
.end method

.method private native nativeResetImeAdapter(J)V
.end method

.method private native nativeSelectAll(J)V
.end method

.method private native nativeSendKeyEvent(JLandroid/view/KeyEvent;IIJIZI)Z
.end method

.method private native nativeSendSyntheticKeyEvent(JIJII)Z
.end method

.method private native nativeSetComposingRegion(JII)V
.end method

.method private native nativeSetComposingText(JLjava/lang/CharSequence;Ljava/lang/String;I)V
.end method

.method private native nativeSetEditableSelectionOffsets(JII)V
.end method

.method private native nativeUnselect(J)V
.end method

.method private populateUnderlinesFromSpans(Ljava/lang/CharSequence;J)V
    .locals 10
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "underlines"    # J

    .prologue
    .line 528
    instance-of v6, p1, Landroid/text/SpannableString;

    if-nez v6, :cond_1

    .line 543
    :cond_0
    return-void

    :cond_1
    move-object v4, p1

    .line 530
    check-cast v4, Landroid/text/SpannableString;

    .line 531
    .local v4, "spannableString":Landroid/text/SpannableString;
    const/4 v6, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const-class v8, Landroid/text/style/CharacterStyle;

    invoke-virtual {v4, v6, v7, v8}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/text/style/CharacterStyle;

    .line 533
    .local v5, "spans":[Landroid/text/style/CharacterStyle;
    move-object v0, v5

    .local v0, "arr$":[Landroid/text/style/CharacterStyle;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 534
    .local v3, "span":Landroid/text/style/CharacterStyle;
    instance-of v6, v3, Landroid/text/style/BackgroundColorSpan;

    if-eqz v6, :cond_3

    .line 535
    invoke-virtual {v4, v3}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v4, v3}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    check-cast v3, Landroid/text/style/BackgroundColorSpan;

    .end local v3    # "span":Landroid/text/style/CharacterStyle;
    invoke-virtual {v3}, Landroid/text/style/BackgroundColorSpan;->getBackgroundColor()I

    move-result v8

    invoke-static {p2, p3, v6, v7, v8}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeAppendBackgroundColorSpan(JIII)V

    .line 533
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 538
    .restart local v3    # "span":Landroid/text/style/CharacterStyle;
    :cond_3
    instance-of v6, v3, Landroid/text/style/UnderlineSpan;

    if-eqz v6, :cond_2

    .line 539
    invoke-virtual {v4, v3}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v4, v3}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-static {p2, p3, v6, v7}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeAppendUnderlineSpan(JII)V

    goto :goto_1
.end method

.method private shouldSendKeyEventWithKeyCode(Ljava/lang/String;)I
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v0, 0xe5

    .line 313
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 317
    :cond_0
    :goto_0
    return v0

    .line 315
    :cond_1
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x42

    goto :goto_0

    .line 316
    :cond_2
    const-string v1, "\t"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x3d

    goto :goto_0
.end method

.method private showKeyboard()V
    .locals 4

    .prologue
    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mIsShowWithoutHideOutstanding:Z

    .line 275
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->getAttachedView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v3}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->getNewShowKeyboardReceiver()Landroid/os/ResultReceiver;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)V

    .line 277
    return-void
.end method


# virtual methods
.method public attach(J)V
    .locals 1
    .param p1, "nativeImeAdapter"    # J

    .prologue
    .line 270
    sget v0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->attach(JI)V

    .line 271
    return-void
.end method

.method public attach(JI)V
    .locals 5
    .param p1, "nativeImeAdapter"    # J
    .param p3, "textInputType"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 251
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 252
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeResetImeAdapter(J)V

    .line 254
    :cond_0
    iput-wide p1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    .line 255
    iput p3, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    .line 256
    cmp-long v0, p1, v2

    if-eqz v0, :cond_1

    .line 257
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeAttachImeAdapter(J)V

    .line 259
    :cond_1
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    if-ne v0, v1, :cond_2

    .line 260
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->dismissInput(Z)V

    .line 262
    :cond_2
    return-void
.end method

.method checkCompositionQueueAndCallNative(Ljava/lang/CharSequence;IZ)Z
    .locals 14
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I
    .param p3, "isCommit"    # Z

    .prologue
    .line 336
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 360
    :goto_0
    return v0

    .line 337
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 340
    .local v11, "textStr":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    .line 341
    .local v13, "isFinish":Z
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v0, v13}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->onImeEvent(Z)V

    .line 342
    invoke-direct {p0, v11}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->shouldSendKeyEventWithKeyCode(Ljava/lang/String;)I

    move-result v6

    .line 343
    .local v6, "keyCode":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 345
    .local v4, "timeStampMs":J
    const/16 v0, 0xe5

    if-eq v6, v0, :cond_1

    .line 346
    const/4 v0, 0x6

    invoke-virtual {p0, v6, v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sendKeyEventWithKeyCode(II)V

    .line 360
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 349
    :cond_1
    iget-wide v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    sget v3, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sEventTypeRawKeyDown:I

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSendSyntheticKeyEvent(JIJII)Z

    .line 351
    if-eqz p3, :cond_2

    .line 352
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1, v11}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeCommitText(JLjava/lang/String;)V

    .line 356
    :goto_2
    iget-wide v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    sget v3, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sEventTypeKeyUp:I

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSendSyntheticKeyEvent(JIJII)Z

    goto :goto_1

    .line 354
    :cond_2
    iget-wide v8, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    move-object v7, p0

    move-object v10, p1

    move/from16 v12, p2

    invoke-direct/range {v7 .. v12}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSetComposingText(JLjava/lang/CharSequence;Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public copy()Z
    .locals 4

    .prologue
    .line 473
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 475
    :goto_0
    return v0

    .line 474
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeCopy(J)V

    .line 475
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cut()Z
    .locals 4

    .prologue
    .line 463
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 465
    :goto_0
    return v0

    .line 464
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeCut(J)V

    .line 465
    const/4 v0, 0x1

    goto :goto_0
.end method

.method deleteSurroundingText(II)Z
    .locals 4
    .param p1, "beforeLength"    # I
    .param p2, "afterLength"    # I

    .prologue
    .line 409
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 411
    :goto_0
    return v0

    .line 410
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeDeleteSurroundingText(JII)V

    .line 411
    const/4 v0, 0x1

    goto :goto_0
.end method

.method detach()V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 553
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    .line 554
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    .line 555
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 309
    invoke-virtual {p0, p1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method finishComposingText()V
    .locals 4

    .prologue
    .line 364
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 366
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeFinishComposingText(J)V

    goto :goto_0
.end method

.method getInputMethodManagerWrapper()Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    return-object v0
.end method

.method getTextInputType()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    return v0
.end method

.method public hasTextInputType()Z
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->isTextInputType(I)Z

    move-result v0

    return v0
.end method

.method public isSelectionPassword()Z
    .locals 2

    .prologue
    .line 305
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypePassword:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public paste()Z
    .locals 4

    .prologue
    .line 483
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 485
    :goto_0
    return v0

    .line 484
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativePaste(J)V

    .line 485
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public selectAll()Z
    .locals 4

    .prologue
    .line 453
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 455
    :goto_0
    return v0

    .line 454
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSelectAll(J)V

    .line 455
    const/4 v0, 0x1

    goto :goto_0
.end method

.method sendKeyEventWithKeyCode(II)V
    .locals 19
    .param p1, "keyCode"    # I
    .param p2, "flags"    # I

    .prologue
    .line 321
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 322
    .local v4, "eventTime":J
    new-instance v3, Landroid/view/KeyEvent;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x0

    move-wide v6, v4

    move/from16 v9, p1

    move/from16 v14, p2

    invoke-direct/range {v3 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    .line 326
    new-instance v7, Landroid/view/KeyEvent;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    const/4 v12, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-wide v10, v4

    move/from16 v13, p1

    move/from16 v18, p2

    invoke-direct/range {v7 .. v18}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    .line 330
    return-void
.end method

.method sendSyntheticKeyEvent(IJII)Z
    .locals 8
    .param p1, "eventType"    # I
    .param p2, "timestampMs"    # J
    .param p4, "keyCode"    # I
    .param p5, "unicodeChar"    # I

    .prologue
    .line 393
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 397
    :goto_0
    return v0

    .line 395
    :cond_0
    iget-wide v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    move-object v0, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSendSyntheticKeyEvent(JIJII)Z

    .line 397
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setComposingRegion(II)Z
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 433
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 435
    :goto_0
    return v0

    .line 434
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSetComposingRegion(JII)V

    .line 435
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setEditableSelectionOffsets(II)Z
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 421
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 423
    :goto_0
    return v0

    .line 422
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSetEditableSelectionOffsets(JII)V

    .line 423
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setInputConnection(Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;)V
    .locals 0
    .param p1, "inputConnection"    # Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    .line 177
    return-void
.end method

.method translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v9, 0x0

    .line 369
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return v9

    .line 371
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v11

    .line 372
    .local v11, "action":I
    if-eqz v11, :cond_2

    const/4 v0, 0x1

    if-ne v11, v0, :cond_0

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v0, v9}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->onImeEvent(Z)V

    .line 387
    iget-wide v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v0

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getModifiers(I)I

    move-result v5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v10

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeSendKeyEvent(JLandroid/view/KeyEvent;IIJIZI)Z

    move-result v9

    goto :goto_0
.end method

.method public unselect()Z
    .locals 4

    .prologue
    .line 443
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 445
    :goto_0
    return v0

    .line 444
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->nativeUnselect(J)V

    .line 445
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updateKeyboardVisibility(JIZ)V
    .locals 5
    .param p1, "nativeImeAdapter"    # J
    .param p3, "textInputType"    # I
    .param p4, "showIfNeeded"    # Z

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 226
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    if-ne v0, v1, :cond_1

    if-nez p4, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mNativeImeAdapterAndroid:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mTextInputType:I

    if-eq v0, p3, :cond_4

    .line 233
    :cond_2
    sget v0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    if-ne p3, v0, :cond_3

    .line 234
    new-instance v0, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;-><init>(Lcom/android/org/chromium/content/browser/input/ImeAdapter;J)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    .line 235
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mDismissInput:Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 239
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->attach(JI)V

    .line 241
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->mViewEmbedder:Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;->getAttachedView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->restartInput(Landroid/view/View;)V

    .line 242
    if-eqz p4, :cond_0

    .line 243
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->showKeyboard()V

    goto :goto_0

    .line 245
    :cond_4
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->hasInputType()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    .line 246
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->showKeyboard()V

    goto :goto_0
.end method
