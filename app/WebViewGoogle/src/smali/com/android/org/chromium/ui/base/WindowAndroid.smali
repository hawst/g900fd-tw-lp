.class public Lcom/android/org/chromium/ui/base/WindowAndroid;
.super Ljava/lang/Object;
.source "WindowAndroid.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mApplicationContext:Landroid/content/Context;

.field protected mIntentErrors:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNativeWindowAndroid:J

.field protected mOutstandingIntents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mVSyncListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

.field private final mVSyncMonitor:Lcom/android/org/chromium/ui/VSyncMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/android/org/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/ui/base/WindowAndroid;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mNativeWindowAndroid:J

    .line 50
    new-instance v0, Lcom/android/org/chromium/ui/base/WindowAndroid$1;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/ui/base/WindowAndroid$1;-><init>(Lcom/android/org/chromium/ui/base/WindowAndroid;)V

    iput-object v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mVSyncListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

    .line 64
    sget-boolean v0, Lcom/android/org/chromium/ui/base/WindowAndroid;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    iput-object p1, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mApplicationContext:Landroid/content/Context;

    .line 66
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mOutstandingIntents:Landroid/util/SparseArray;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mIntentErrors:Ljava/util/HashMap;

    .line 68
    new-instance v0, Lcom/android/org/chromium/ui/VSyncMonitor;

    iget-object v1, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mVSyncListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

    invoke-direct {v0, p1, v1}, Lcom/android/org/chromium/ui/VSyncMonitor;-><init>(Landroid/content/Context;Lcom/android/org/chromium/ui/VSyncMonitor$Listener;)V

    iput-object v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mVSyncMonitor:Lcom/android/org/chromium/ui/VSyncMonitor;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/ui/base/WindowAndroid;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/ui/base/WindowAndroid;

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mNativeWindowAndroid:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/ui/base/WindowAndroid;JJ)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/ui/base/WindowAndroid;
    .param p1, "x1"    # J
    .param p3, "x2"    # J

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/ui/base/WindowAndroid;->nativeOnVSync(JJ)V

    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit(J)J
.end method

.method private native nativeOnVSync(JJ)V
.end method

.method private requestVSyncUpdate()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mVSyncMonitor:Lcom/android/org/chromium/ui/VSyncMonitor;

    invoke-virtual {v0}, Lcom/android/org/chromium/ui/VSyncMonitor;->requestUpdate()V

    .line 233
    return-void
.end method


# virtual methods
.method public getNativePointer()J
    .locals 4

    .prologue
    .line 276
    iget-wide v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mNativeWindowAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mVSyncMonitor:Lcom/android/org/chromium/ui/VSyncMonitor;

    invoke-virtual {v0}, Lcom/android/org/chromium/ui/VSyncMonitor;->getVSyncPeriodInMicroseconds()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/ui/base/WindowAndroid;->nativeInit(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mNativeWindowAndroid:J

    .line 279
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/ui/base/WindowAndroid;->mNativeWindowAndroid:J

    return-wide v0
.end method

.method public showCancelableIntent(Landroid/content/Intent;Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;I)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callback"    # Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;
    .param p3, "errorId"    # I

    .prologue
    .line 119
    const-string v0, "WindowAndroid"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t show intent as context is not an Activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v0, -0x1

    return v0
.end method

.method public showIntent(Landroid/content/Intent;Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;I)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callback"    # Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;
    .param p3, "errorId"    # I

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/org/chromium/ui/base/WindowAndroid;->showCancelableIntent(Landroid/content/Intent;Lcom/android/org/chromium/ui/base/WindowAndroid$IntentCallback;I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
