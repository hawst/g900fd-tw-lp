.class public Lcom/android/org/chromium/android_webview/AwSettings;
.super Ljava/lang/Object;
.source "AwSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;,
        Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;,
        Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;,
        Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sAppCachePathIsSet:Z

.field private static final sGlobalContentSettingsLock:Ljava/lang/Object;


# instance fields
.field private mAcceptThirdPartyCookies:Z

.field private mAllowContentUrlAccess:Z

.field private mAllowFileAccessFromFileURLs:Z

.field private mAllowFileUrlAccess:Z

.field private mAllowUniversalAccessFromFileURLs:Z

.field private mAppCacheEnabled:Z

.field private mAutoCompleteEnabled:Z

.field private final mAwSettingsLock:Ljava/lang/Object;

.field private mBlockNetworkLoads:Z

.field private mBuiltInZoomControls:Z

.field private mCacheMode:I

.field private mCursiveFontFamily:Ljava/lang/String;

.field private mDIPScale:D

.field private mDatabaseEnabled:Z

.field private mDefaultFixedFontSize:I

.field private mDefaultFontSize:I

.field private mDefaultTextEncoding:Ljava/lang/String;

.field private mDefaultVideoPosterURL:Ljava/lang/String;

.field private mDisplayZoomControls:Z

.field private mDomStorageEnabled:Z

.field private mEnableSupportedHardwareAcceleratedFeatures:Z

.field private final mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

.field private mFantasyFontFamily:Ljava/lang/String;

.field private mFixedFontFamily:Ljava/lang/String;

.field private mForceZeroLayoutHeight:Z

.field private mFullscreenSupported:Z

.field private mGeolocationEnabled:Z

.field private final mHasInternetPermission:Z

.field private mImagesEnabled:Z

.field private mInitialPageScalePercent:F

.field private mJavaScriptCanOpenWindowsAutomatically:Z

.field private mJavaScriptEnabled:Z

.field private mLayoutAlgorithm:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

.field private mLoadWithOverviewMode:Z

.field private mLoadsImagesAutomatically:Z

.field private mMediaPlaybackRequiresUserGesture:Z

.field private mMinimumFontSize:I

.field private mMinimumLogicalFontSize:I

.field private mMixedContentMode:I

.field private mNativeAwSettings:J

.field private final mPasswordEchoEnabled:Z

.field private mPluginState:Landroid/webkit/WebSettings$PluginState;

.field private mSansSerifFontFamily:Ljava/lang/String;

.field private mSerifFontFamily:Ljava/lang/String;

.field private mShouldFocusFirstNode:Z

.field private mSpatialNavigationEnabled:Z

.field private mStandardFontFamily:Ljava/lang/String;

.field private final mSupportLegacyQuirks:Z

.field private mSupportMultipleWindows:Z

.field private mSupportZoom:Z

.field private mTextSizePercent:I

.field private mUseWideViewport:Z

.field private mUserAgent:Ljava/lang/String;

.field private mVideoOverlayForEmbeddedVideoEnabled:Z

.field private mZeroLayoutHeightDisablesViewportQuirk:Z

.field private mZoomChangeListener:Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const-class v0, Lcom/android/org/chromium/android_webview/AwSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    .line 127
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/org/chromium/android_webview/AwSettings;->sGlobalContentSettingsLock:Ljava/lang/Object;

    .line 131
    sput-boolean v1, Lcom/android/org/chromium/android_webview/AwSettings;->sAppCachePathIsSet:Z

    return-void

    :cond_0
    move v0, v1

    .line 30
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isAccessFromFileURLsGrantedByDefault"    # Z
    .param p3, "supportsLegacyQuirks"    # Z

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDIPScale:D

    .line 62
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    .line 64
    sget-object v3, Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLayoutAlgorithm:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    .line 65
    const/16 v3, 0x64

    iput v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mTextSizePercent:I

    .line 66
    const-string v3, "sans-serif"

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mStandardFontFamily:Ljava/lang/String;

    .line 67
    const-string v3, "monospace"

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFixedFontFamily:Ljava/lang/String;

    .line 68
    const-string v3, "sans-serif"

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSansSerifFontFamily:Ljava/lang/String;

    .line 69
    const-string v3, "serif"

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSerifFontFamily:Ljava/lang/String;

    .line 70
    const-string v3, "cursive"

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCursiveFontFamily:Ljava/lang/String;

    .line 71
    const-string v3, "fantasy"

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFantasyFontFamily:Ljava/lang/String;

    .line 74
    iput v6, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumFontSize:I

    .line 75
    iput v6, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumLogicalFontSize:I

    .line 76
    const/16 v3, 0x10

    iput v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFontSize:I

    .line 77
    const/16 v3, 0xd

    iput v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFixedFontSize:I

    .line 78
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadsImagesAutomatically:Z

    .line 79
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mImagesEnabled:Z

    .line 80
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptEnabled:Z

    .line 81
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowUniversalAccessFromFileURLs:Z

    .line 82
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileAccessFromFileURLs:Z

    .line 83
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    .line 84
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportMultipleWindows:Z

    .line 85
    sget-object v3, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    .line 86
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAppCacheEnabled:Z

    .line 87
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDomStorageEnabled:Z

    .line 88
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDatabaseEnabled:Z

    .line 89
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUseWideViewport:Z

    .line 90
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mZeroLayoutHeightDisablesViewportQuirk:Z

    .line 91
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mForceZeroLayoutHeight:Z

    .line 92
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadWithOverviewMode:Z

    .line 93
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMediaPlaybackRequiresUserGesture:Z

    .line 95
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mInitialPageScalePercent:F

    .line 97
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEnableSupportedHardwareAcceleratedFeatures:Z

    .line 98
    iput v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    .line 99
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mVideoOverlayForEmbeddedVideoEnabled:Z

    .line 110
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowContentUrlAccess:Z

    .line 111
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileUrlAccess:Z

    .line 112
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCacheMode:I

    .line 113
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mShouldFocusFirstNode:Z

    .line 114
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mGeolocationEnabled:Z

    .line 115
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAutoCompleteEnabled:Z

    .line 116
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFullscreenSupported:Z

    .line 117
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportZoom:Z

    .line 118
    iput-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBuiltInZoomControls:Z

    .line 119
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDisplayZoomControls:Z

    .line 134
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    .line 218
    const-string v3, "android.permission.INTERNET"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 222
    .local v0, "hasInternetPermission":Z
    :goto_0
    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v4

    .line 223
    :try_start_0
    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mHasInternetPermission:Z

    .line 224
    if-nez v0, :cond_2

    move v3, v1

    :goto_1
    iput-boolean v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBlockNetworkLoads:Z

    .line 225
    new-instance v3, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-direct {v3, p0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    .line 226
    if-eqz p2, :cond_0

    .line 227
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowUniversalAccessFromFileURLs:Z

    .line 228
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileAccessFromFileURLs:Z

    .line 231
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwResource;->getDefaultTextEncoding()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultTextEncoding:Ljava/lang/String;

    .line 232
    # getter for: Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;->sInstance:Ljava/lang/String;
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;->access$500()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUserAgent:Ljava/lang/String;

    .line 235
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v5, "android.hardware.touchscreen"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    :goto_2
    iput-boolean v3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSpatialNavigationEnabled:Z

    .line 239
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "show_password"

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_4

    :goto_3
    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPasswordEchoEnabled:Z

    .line 244
    iget v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mTextSizePercent:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mTextSizePercent:I

    .line 246
    iput-boolean p3, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportLegacyQuirks:Z

    .line 247
    monitor-exit v4

    .line 249
    return-void

    .end local v0    # "hasInternetPermission":Z
    :cond_1
    move v0, v2

    .line 218
    goto :goto_0

    .restart local v0    # "hasInternetPermission":Z
    :cond_2
    move v3, v2

    .line 224
    goto :goto_1

    :cond_3
    move v3, v2

    .line 235
    goto :goto_2

    :cond_4
    move v1, v2

    .line 239
    goto :goto_3

    .line 247
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeGetDefaultUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/android_webview/AwSettings;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/org/chromium/android_webview/AwSettings;)Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mZoomChangeListener:Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/org/chromium/android_webview/AwSettings;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p1, "x1"    # J

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeUpdateRendererPreferencesLocked(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/org/chromium/android_webview/AwSettings;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/org/chromium/android_webview/AwSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->updateWebkitPreferencesOnUiThreadLocked()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/org/chromium/android_webview/AwSettings;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p1, "x1"    # J

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeUpdateInitialPageScaleLocked(J)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/org/chromium/android_webview/AwSettings;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p1, "x1"    # J

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeUpdateFormDataPreferencesLocked(J)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/org/chromium/android_webview/AwSettings;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p1, "x1"    # J

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeUpdateUserAgentLocked(J)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/org/chromium/android_webview/AwSettings;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p1, "x1"    # J

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeResetScrollAndScaleState(J)V

    return-void
.end method

.method private clipFontSize(I)I
    .locals 2
    .param p1, "size"    # I

    .prologue
    const/16 v1, 0x48

    const/4 v0, 0x1

    .line 1632
    if-ge p1, v0, :cond_1

    move p1, v0

    .line 1637
    .end local p1    # "size":I
    :cond_0
    :goto_0
    return p1

    .line 1634
    .restart local p1    # "size":I
    :cond_1
    if-le p1, v1, :cond_0

    move p1, v1

    .line 1635
    goto :goto_0
.end method

.method private getAllowDisplayingInsecureContentLocked()Z
    .locals 2

    .prologue
    .line 1567
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1568
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAllowFileAccessFromFileURLsLocked()Z
    .locals 1

    .prologue
    .line 1059
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1060
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileAccessFromFileURLs:Z

    return v0
.end method

.method private getAllowRunningInsecureContentLocked()Z
    .locals 1

    .prologue
    .line 1561
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1562
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAllowUniversalAccessFromFileURLsLocked()Z
    .locals 1

    .prologue
    .line 1044
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1045
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowUniversalAccessFromFileURLs:Z

    return v0
.end method

.method private getAppCacheEnabledLocked()Z
    .locals 2

    .prologue
    .line 1321
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1322
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAppCacheEnabled:Z

    if-nez v0, :cond_1

    .line 1323
    const/4 v0, 0x0

    .line 1326
    :goto_0
    return v0

    .line 1325
    :cond_1
    sget-object v1, Lcom/android/org/chromium/android_webview/AwSettings;->sGlobalContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1326
    :try_start_0
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->sAppCachePathIsSet:Z

    monitor-exit v1

    goto :goto_0

    .line 1327
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getCursiveFontFamilyLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 785
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCursiveFontFamily:Ljava/lang/String;

    return-object v0
.end method

.method private getDIPScaleLocked()D
    .locals 2

    .prologue
    .line 259
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 260
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDIPScale:D

    return-wide v0
.end method

.method private getDatabaseEnabledLocked()Z
    .locals 1

    .prologue
    .line 1380
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1381
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDatabaseEnabled:Z

    return v0
.end method

.method private getDefaultFixedFontSizeLocked()I
    .locals 1

    .prologue
    .line 924
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 925
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFixedFontSize:I

    return v0
.end method

.method private getDefaultFontSizeLocked()I
    .locals 1

    .prologue
    .line 896
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 897
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFontSize:I

    return v0
.end method

.method private getDefaultTextEncodingLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1407
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1408
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultTextEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 550
    # getter for: Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;->sInstance:Ljava/lang/String;
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;->access$500()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDefaultVideoPosterURLLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1463
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultVideoPosterURL:Ljava/lang/String;

    return-object v0
.end method

.method private getDomStorageEnabledLocked()Z
    .locals 1

    .prologue
    .line 1353
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1354
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDomStorageEnabled:Z

    return v0
.end method

.method private getEnableSupportedHardwareAcceleratedFeaturesLocked()Z
    .locals 1

    .prologue
    .line 463
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 464
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEnableSupportedHardwareAcceleratedFeatures:Z

    return v0
.end method

.method private getFantasyFontFamilyLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 812
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFantasyFontFamily:Ljava/lang/String;

    return-object v0
.end method

.method private getFixedFontFamilyLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 704
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 705
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFixedFontFamily:Ljava/lang/String;

    return-object v0
.end method

.method private getForceZeroLayoutHeightLocked()Z
    .locals 1

    .prologue
    .line 1270
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1271
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mForceZeroLayoutHeight:Z

    return v0
.end method

.method private getFullscreenSupportedLocked()Z
    .locals 1

    .prologue
    .line 478
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 479
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFullscreenSupported:Z

    return v0
.end method

.method private getImagesEnabledLocked()Z
    .locals 1

    .prologue
    .line 1014
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1015
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mImagesEnabled:Z

    return v0
.end method

.method private getInitialPageScalePercentLocked()F
    .locals 1

    .prologue
    .line 433
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 434
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mInitialPageScalePercent:F

    return v0
.end method

.method private getJavaScriptCanOpenWindowsAutomaticallyLocked()Z
    .locals 1

    .prologue
    .line 1134
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1135
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    return v0
.end method

.method private getJavaScriptEnabledLocked()Z
    .locals 1

    .prologue
    .line 1029
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1030
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptEnabled:Z

    return v0
.end method

.method private getLoadWithOverviewModeLocked()Z
    .locals 1

    .prologue
    .line 623
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 624
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadWithOverviewMode:Z

    return v0
.end method

.method private getLoadsImagesAutomaticallyLocked()Z
    .locals 1

    .prologue
    .line 987
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 988
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadsImagesAutomatically:Z

    return v0
.end method

.method private getMediaPlaybackRequiresUserGestureLocked()Z
    .locals 1

    .prologue
    .line 1434
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1435
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMediaPlaybackRequiresUserGesture:Z

    return v0
.end method

.method private getMinimumFontSizeLocked()I
    .locals 1

    .prologue
    .line 840
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 841
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumFontSize:I

    return v0
.end method

.method private getMinimumLogicalFontSizeLocked()I
    .locals 1

    .prologue
    .line 868
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 869
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumLogicalFontSize:I

    return v0
.end method

.method private getPasswordEchoEnabledLocked()Z
    .locals 1

    .prologue
    .line 1276
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1277
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPasswordEchoEnabled:Z

    return v0
.end method

.method private getPluginsDisabledLocked()Z
    .locals 2

    .prologue
    .line 1097
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    sget-object v1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSansSerifFontFamilyLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 731
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSansSerifFontFamily:Ljava/lang/String;

    return-object v0
.end method

.method private getSaveFormDataLocked()Z
    .locals 1

    .prologue
    .line 541
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 542
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAutoCompleteEnabled:Z

    return v0
.end method

.method private getSerifFontFamilyLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 758
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSerifFontFamily:Ljava/lang/String;

    return-object v0
.end method

.method private getSpatialNavigationLocked()Z
    .locals 1

    .prologue
    .line 448
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 449
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSpatialNavigationEnabled:Z

    return v0
.end method

.method private getStandardFontFamilyLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 677
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mStandardFontFamily:Ljava/lang/String;

    return-object v0
.end method

.method private getSupportLegacyQuirksLocked()Z
    .locals 1

    .prologue
    .line 1199
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1200
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportLegacyQuirks:Z

    return v0
.end method

.method private getSupportMultipleWindowsLocked()Z
    .locals 1

    .prologue
    .line 1193
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1194
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportMultipleWindows:Z

    return v0
.end method

.method private getTextAutosizingEnabledLocked()Z
    .locals 2

    .prologue
    .line 1166
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1167
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLayoutAlgorithm:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    sget-object v1, Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTextSizePercentLocked()I
    .locals 1

    .prologue
    .line 650
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 651
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mTextSizePercent:I

    return v0
.end method

.method private getUseWideViewportLocked()Z
    .locals 1

    .prologue
    .line 1228
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1229
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUseWideViewport:Z

    return v0
.end method

.method private getUserAgentLocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 588
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method private getVideoOverlayForEmbeddedVideoEnabledLocked()Z
    .locals 1

    .prologue
    .line 1604
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1605
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mVideoOverlayForEmbeddedVideoEnabled:Z

    return v0
.end method

.method private getZeroLayoutHeightDisablesViewportQuirkLocked()Z
    .locals 1

    .prologue
    .line 1249
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1250
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mZeroLayoutHeightDisablesViewportQuirk:Z

    return v0
.end method

.method private nativeAwSettingsGone(J)V
    .locals 5
    .param p1, "nativeAwSettings"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 253
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 254
    :cond_1
    iput-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    .line 255
    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private static native nativeGetDefaultUserAgent()Ljava/lang/String;
.end method

.method private native nativeInit(J)J
.end method

.method private native nativePopulateWebPreferencesLocked(JJ)V
.end method

.method private native nativeResetScrollAndScaleState(J)V
.end method

.method private native nativeUpdateEverythingLocked(J)V
.end method

.method private native nativeUpdateFormDataPreferencesLocked(J)V
.end method

.method private native nativeUpdateInitialPageScaleLocked(J)V
.end method

.method private native nativeUpdateRendererPreferencesLocked(J)V
.end method

.method private native nativeUpdateUserAgentLocked(J)V
.end method

.method private native nativeUpdateWebkitPreferencesLocked(J)V
.end method

.method private onGestureZoomSupportChanged(ZZ)V
    .locals 2
    .param p1, "supportsDoubleTapZoom"    # Z
    .param p2, "supportsMultiTouchZoom"    # Z

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    new-instance v1, Lcom/android/org/chromium/android_webview/AwSettings$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings$5;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;ZZ)V

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->maybePostOnUiThread(Ljava/lang/Runnable;)V

    .line 1480
    return-void
.end method

.method private populateWebPreferences(J)V
    .locals 7
    .param p1, "webPrefsPtr"    # J

    .prologue
    .line 1649
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1650
    :try_start_0
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1652
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1651
    :cond_0
    :try_start_1
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    invoke-direct {p0, v2, v3, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativePopulateWebPreferencesLocked(JJ)V

    .line 1652
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1653
    return-void
.end method

.method private supportsDoubleTapZoomLocked()Z
    .locals 1

    .prologue
    .line 1610
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1611
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportZoom:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBuiltInZoomControls:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUseWideViewport:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private supportsMultiTouchZoomLocked()Z
    .locals 1

    .prologue
    .line 1615
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1616
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportZoom:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBuiltInZoomControls:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateEverything()V
    .locals 2

    .prologue
    .line 1642
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1643
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->updateEverythingLocked()V

    .line 1644
    monitor-exit v1

    .line 1645
    return-void

    .line 1644
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateEverythingLocked()V
    .locals 4

    .prologue
    .line 292
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 293
    :cond_0
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 294
    :cond_1
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeUpdateEverythingLocked(J)V

    .line 295
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsDoubleTapZoomLocked()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsMultiTouchZoomLocked()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings;->onGestureZoomSupportChanged(ZZ)V

    .line 297
    return-void
.end method

.method private updateWebkitPreferencesOnUiThreadLocked()V
    .locals 4

    .prologue
    .line 1656
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    # getter for: Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->access$1200(Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1657
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 1658
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1659
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeUpdateWebkitPreferencesLocked(J)V

    .line 1661
    :cond_1
    return-void
.end method


# virtual methods
.method public getAcceptThirdPartyCookies()Z
    .locals 2

    .prologue
    .line 338
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 339
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAcceptThirdPartyCookies:Z

    monitor-exit v1

    return v0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAllowContentAccess()Z
    .locals 2

    .prologue
    .line 378
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 379
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowContentUrlAccess:Z

    monitor-exit v1

    return v0

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAllowFileAccess()Z
    .locals 2

    .prologue
    .line 358
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 359
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileUrlAccess:Z

    monitor-exit v1

    return v0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAllowFileAccessFromFileURLs()Z
    .locals 2

    .prologue
    .line 1052
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1053
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getAllowFileAccessFromFileURLsLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1054
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAllowUniversalAccessFromFileURLs()Z
    .locals 2

    .prologue
    .line 1037
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1038
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getAllowUniversalAccessFromFileURLsLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1039
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBlockNetworkLoads()Z
    .locals 2

    .prologue
    .line 316
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 317
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBlockNetworkLoads:Z

    monitor-exit v1

    return v0

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBuiltInZoomControls()Z
    .locals 2

    .prologue
    .line 1521
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1522
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBuiltInZoomControls:Z

    monitor-exit v1

    return v0

    .line 1523
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCacheMode()I
    .locals 2

    .prologue
    .line 398
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 399
    :try_start_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCacheMode:I

    monitor-exit v1

    return v0

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCursiveFontFamily()Ljava/lang/String;
    .locals 2

    .prologue
    .line 778
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 779
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getCursiveFontFamilyLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 780
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDatabaseEnabled()Z
    .locals 2

    .prologue
    .line 1373
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1374
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDatabaseEnabled:Z

    monitor-exit v1

    return v0

    .line 1375
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDefaultFixedFontSize()I
    .locals 2

    .prologue
    .line 917
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 918
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getDefaultFixedFontSizeLocked()I

    move-result v0

    monitor-exit v1

    return v0

    .line 919
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDefaultFontSize()I
    .locals 2

    .prologue
    .line 889
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 890
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getDefaultFontSizeLocked()I

    move-result v0

    monitor-exit v1

    return v0

    .line 891
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDefaultTextEncodingName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1400
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1401
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getDefaultTextEncodingLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1402
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDisplayZoomControls()Z
    .locals 2

    .prologue
    .line 1539
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1540
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDisplayZoomControls:Z

    monitor-exit v1

    return v0

    .line 1541
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDomStorageEnabled()Z
    .locals 2

    .prologue
    .line 1346
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1347
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDomStorageEnabled:Z

    monitor-exit v1

    return v0

    .line 1348
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFantasyFontFamily()Ljava/lang/String;
    .locals 2

    .prologue
    .line 805
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 806
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getFantasyFontFamilyLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 807
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFixedFontFamily()Ljava/lang/String;
    .locals 2

    .prologue
    .line 697
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 698
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getFixedFontFamilyLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 699
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getGeolocationEnabled()Z
    .locals 2

    .prologue
    .line 506
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 507
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mGeolocationEnabled:Z

    monitor-exit v1

    return v0

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getImagesEnabled()Z
    .locals 2

    .prologue
    .line 1007
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1008
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mImagesEnabled:Z

    monitor-exit v1

    return v0

    .line 1009
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getJavaScriptCanOpenWindowsAutomatically()Z
    .locals 2

    .prologue
    .line 1127
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1128
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getJavaScriptCanOpenWindowsAutomaticallyLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1129
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getJavaScriptEnabled()Z
    .locals 2

    .prologue
    .line 1022
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1023
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptEnabled:Z

    monitor-exit v1

    return v0

    .line 1024
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLayoutAlgorithm()Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;
    .locals 2

    .prologue
    .line 1154
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1155
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLayoutAlgorithm:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    monitor-exit v1

    return-object v0

    .line 1156
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLoadWithOverviewMode()Z
    .locals 2

    .prologue
    .line 616
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 617
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getLoadWithOverviewModeLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 618
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLoadsImagesAutomatically()Z
    .locals 2

    .prologue
    .line 980
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 981
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getLoadsImagesAutomaticallyLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 982
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaPlaybackRequiresUserGesture()Z
    .locals 2

    .prologue
    .line 1427
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1428
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getMediaPlaybackRequiresUserGestureLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1429
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMinimumFontSize()I
    .locals 2

    .prologue
    .line 833
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 834
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getMinimumFontSizeLocked()I

    move-result v0

    monitor-exit v1

    return v0

    .line 835
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMinimumLogicalFontSize()I
    .locals 2

    .prologue
    .line 861
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 862
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getMinimumLogicalFontSizeLocked()I

    move-result v0

    monitor-exit v1

    return v0

    .line 863
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMixedContentMode()I
    .locals 2

    .prologue
    .line 1554
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1555
    :try_start_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    monitor-exit v1

    return v0

    .line 1556
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPluginState()Landroid/webkit/WebSettings$PluginState;
    .locals 2

    .prologue
    .line 1105
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1106
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    monitor-exit v1

    return-object v0

    .line 1107
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPluginsEnabled()Z
    .locals 3

    .prologue
    .line 1086
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1087
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    sget-object v2, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1088
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSansSerifFontFamily()Ljava/lang/String;
    .locals 2

    .prologue
    .line 724
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 725
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getSansSerifFontFamilyLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 726
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSaveFormData()Z
    .locals 2

    .prologue
    .line 534
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 535
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getSaveFormDataLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 536
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSerifFontFamily()Ljava/lang/String;
    .locals 2

    .prologue
    .line 751
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 752
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getSerifFontFamilyLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 753
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStandardFontFamily()Ljava/lang/String;
    .locals 2

    .prologue
    .line 670
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 671
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getStandardFontFamilyLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 672
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTextZoom()I
    .locals 2

    .prologue
    .line 643
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 644
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getTextSizePercentLocked()I

    move-result v0

    monitor-exit v1

    return v0

    .line 645
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getUseWideViewPort()Z
    .locals 2

    .prologue
    .line 1221
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1222
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getUseWideViewportLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getUserAgentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 581
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 582
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getUserAgentLocked()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 583
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getVideoOverlayForEmbeddedVideoEnabled()Z
    .locals 2

    .prologue
    .line 1597
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1598
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->getVideoOverlayForEmbeddedVideoEnabledLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1599
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAcceptThirdPartyCookies(Z)V
    .locals 2
    .param p1, "accept"    # Z

    .prologue
    .line 326
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 327
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAcceptThirdPartyCookies:Z

    if-eq v0, p1, :cond_0

    .line 328
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAcceptThirdPartyCookies:Z

    .line 330
    :cond_0
    monitor-exit v1

    .line 331
    return-void

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAllowContentAccess(Z)V
    .locals 2
    .param p1, "allow"    # Z

    .prologue
    .line 367
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 368
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowContentUrlAccess:Z

    if-eq v0, p1, :cond_0

    .line 369
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowContentUrlAccess:Z

    .line 371
    :cond_0
    monitor-exit v1

    .line 372
    return-void

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAllowFileAccess(Z)V
    .locals 2
    .param p1, "allow"    # Z

    .prologue
    .line 347
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 348
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileUrlAccess:Z

    if-eq v0, p1, :cond_0

    .line 349
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileUrlAccess:Z

    .line 351
    :cond_0
    monitor-exit v1

    .line 352
    return-void

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAllowFileAccessFromFileURLs(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 956
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 957
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileAccessFromFileURLs:Z

    if-eq v0, p1, :cond_0

    .line 958
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowFileAccessFromFileURLs:Z

    .line 959
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 961
    :cond_0
    monitor-exit v1

    .line 962
    return-void

    .line 961
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAllowUniversalAccessFromFileURLs(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 944
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 945
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowUniversalAccessFromFileURLs:Z

    if-eq v0, p1, :cond_0

    .line 946
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAllowUniversalAccessFromFileURLs:Z

    .line 947
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 949
    :cond_0
    monitor-exit v1

    .line 950
    return-void

    .line 949
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAppCacheEnabled(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 1284
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1285
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAppCacheEnabled:Z

    if-eq v0, p1, :cond_0

    .line 1286
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAppCacheEnabled:Z

    .line 1287
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1289
    :cond_0
    monitor-exit v1

    .line 1290
    return-void

    .line 1289
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAppCachePath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1296
    const/4 v0, 0x0

    .line 1297
    .local v0, "needToSync":Z
    sget-object v2, Lcom/android/org/chromium/android_webview/AwSettings;->sGlobalContentSettingsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1299
    :try_start_0
    sget-boolean v1, Lcom/android/org/chromium/android_webview/AwSettings;->sAppCachePathIsSet:Z

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1300
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/org/chromium/android_webview/AwSettings;->sAppCachePathIsSet:Z

    .line 1301
    const/4 v0, 0x1

    .line 1303
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1307
    if-eqz v0, :cond_1

    .line 1308
    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1309
    :try_start_1
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1310
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1312
    :cond_1
    return-void

    .line 1303
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1310
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public setBlockNetworkLoads(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 303
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 304
    if-nez p1, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mHasInternetPermission:Z

    if-nez v0, :cond_0

    .line 305
    new-instance v0, Ljava/lang/SecurityException;

    const-string v2, "Permission denied - application missing INTERNET permission"

    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 308
    :cond_0
    :try_start_1
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBlockNetworkLoads:Z

    .line 309
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    return-void
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1508
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1509
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBuiltInZoomControls:Z

    if-eq v0, p1, :cond_0

    .line 1510
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mBuiltInZoomControls:Z

    .line 1511
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsDoubleTapZoomLocked()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsMultiTouchZoomLocked()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings;->onGestureZoomSupportChanged(ZZ)V

    .line 1514
    :cond_0
    monitor-exit v1

    .line 1515
    return-void

    .line 1514
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setCacheMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 387
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 388
    :try_start_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCacheMode:I

    if-eq v0, p1, :cond_0

    .line 389
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCacheMode:I

    .line 391
    :cond_0
    monitor-exit v1

    .line 392
    return-void

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setCursiveFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 766
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 767
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCursiveFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 768
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mCursiveFontFamily:Ljava/lang/String;

    .line 769
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 771
    :cond_0
    monitor-exit v1

    .line 772
    return-void

    .line 771
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setDIPScale(D)V
    .locals 3
    .param p1, "dipScale"    # D

    .prologue
    .line 264
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 265
    :try_start_0
    iput-wide p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDIPScale:D

    .line 268
    monitor-exit v1

    .line 269
    return-void

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDatabaseEnabled(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 1361
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1362
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDatabaseEnabled:Z

    if-eq v0, p1, :cond_0

    .line 1363
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDatabaseEnabled:Z

    .line 1364
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1366
    :cond_0
    monitor-exit v1

    .line 1367
    return-void

    .line 1366
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDefaultFixedFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 904
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 905
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwSettings;->clipFontSize(I)I

    move-result p1

    .line 906
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFixedFontSize:I

    if-eq v0, p1, :cond_0

    .line 907
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFixedFontSize:I

    .line 908
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 910
    :cond_0
    monitor-exit v1

    .line 911
    return-void

    .line 910
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDefaultFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 876
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 877
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwSettings;->clipFontSize(I)I

    move-result p1

    .line 878
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFontSize:I

    if-eq v0, p1, :cond_0

    .line 879
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultFontSize:I

    .line 880
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 882
    :cond_0
    monitor-exit v1

    .line 883
    return-void

    .line 882
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDefaultTextEncodingName(Ljava/lang/String;)V
    .locals 2
    .param p1, "encoding"    # Ljava/lang/String;

    .prologue
    .line 1388
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1389
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultTextEncoding:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1390
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultTextEncoding:Ljava/lang/String;

    .line 1391
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1393
    :cond_0
    monitor-exit v1

    .line 1394
    return-void

    .line 1393
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDefaultVideoPosterURL(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1442
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1443
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultVideoPosterURL:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultVideoPosterURL:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultVideoPosterURL:Ljava/lang/String;

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 1445
    :cond_1
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDefaultVideoPosterURL:Ljava/lang/String;

    .line 1446
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1448
    :cond_2
    monitor-exit v1

    .line 1449
    return-void

    .line 1448
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDisplayZoomControls(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 1530
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1531
    :try_start_0
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDisplayZoomControls:Z

    .line 1532
    monitor-exit v1

    .line 1533
    return-void

    .line 1532
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDomStorageEnabled(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 1334
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1335
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDomStorageEnabled:Z

    if-eq v0, p1, :cond_0

    .line 1336
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDomStorageEnabled:Z

    .line 1337
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1339
    :cond_0
    monitor-exit v1

    .line 1340
    return-void

    .line 1339
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setEnableSupportedHardwareAcceleratedFeatures(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 453
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 454
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEnableSupportedHardwareAcceleratedFeatures:Z

    if-eq v0, p1, :cond_0

    .line 455
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEnableSupportedHardwareAcceleratedFeatures:Z

    .line 456
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 458
    :cond_0
    monitor-exit v1

    .line 459
    return-void

    .line 458
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setFantasyFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 793
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 794
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFantasyFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 795
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFantasyFontFamily:Ljava/lang/String;

    .line 796
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 798
    :cond_0
    monitor-exit v1

    .line 799
    return-void

    .line 798
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setFixedFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 685
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 686
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFixedFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 687
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFixedFontFamily:Ljava/lang/String;

    .line 688
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 690
    :cond_0
    monitor-exit v1

    .line 691
    return-void

    .line 690
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setForceZeroLayoutHeight(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 1254
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1255
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mForceZeroLayoutHeight:Z

    if-eq v0, p1, :cond_0

    .line 1256
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mForceZeroLayoutHeight:Z

    .line 1257
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1259
    :cond_0
    monitor-exit v1

    .line 1260
    return-void

    .line 1259
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setFullscreenSupported(Z)V
    .locals 2
    .param p1, "supported"    # Z

    .prologue
    .line 468
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 469
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFullscreenSupported:Z

    if-eq v0, p1, :cond_0

    .line 470
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mFullscreenSupported:Z

    .line 471
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 473
    :cond_0
    monitor-exit v1

    .line 474
    return-void

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGeolocationEnabled(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 495
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 496
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mGeolocationEnabled:Z

    if-eq v0, p1, :cond_0

    .line 497
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mGeolocationEnabled:Z

    .line 499
    :cond_0
    monitor-exit v1

    .line 500
    return-void

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setImagesEnabled(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 995
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 996
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mImagesEnabled:Z

    if-eq v0, p1, :cond_0

    .line 997
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mImagesEnabled:Z

    .line 998
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1000
    :cond_0
    monitor-exit v1

    .line 1001
    return-void

    .line 1000
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setInitialPageScale(F)V
    .locals 3
    .param p1, "scaleInPercent"    # F

    .prologue
    .line 416
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 417
    :try_start_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mInitialPageScalePercent:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 418
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mInitialPageScalePercent:F

    .line 419
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwSettings$1;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/android_webview/AwSettings$1;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    invoke-virtual {v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->runOnUiThreadBlockingAndLocked(Ljava/lang/Runnable;)V

    .line 428
    :cond_0
    monitor-exit v1

    .line 429
    return-void

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setJavaScriptCanOpenWindowsAutomatically(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 1115
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1116
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    if-eq v0, p1, :cond_0

    .line 1117
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    .line 1118
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1120
    :cond_0
    monitor-exit v1

    .line 1121
    return-void

    .line 1120
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setJavaScriptEnabled(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 932
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 933
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptEnabled:Z

    if-eq v0, p1, :cond_0

    .line 934
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mJavaScriptEnabled:Z

    .line 935
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 937
    :cond_0
    monitor-exit v1

    .line 938
    return-void

    .line 937
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setLayoutAlgorithm(Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;)V
    .locals 2
    .param p1, "l"    # Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    .prologue
    .line 1142
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1143
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLayoutAlgorithm:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    if-eq v0, p1, :cond_0

    .line 1144
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLayoutAlgorithm:Lcom/android/org/chromium/android_webview/AwSettings$LayoutAlgorithm;

    .line 1145
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1147
    :cond_0
    monitor-exit v1

    .line 1148
    return-void

    .line 1147
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setLoadWithOverviewMode(Z)V
    .locals 3
    .param p1, "overview"    # Z

    .prologue
    .line 596
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 597
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadWithOverviewMode:Z

    if-eq v0, p1, :cond_0

    .line 598
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadWithOverviewMode:Z

    .line 599
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwSettings$4;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/android_webview/AwSettings$4;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    invoke-virtual {v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->runOnUiThreadBlockingAndLocked(Ljava/lang/Runnable;)V

    .line 609
    :cond_0
    monitor-exit v1

    .line 610
    return-void

    .line 609
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setLoadsImagesAutomatically(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 968
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 969
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadsImagesAutomatically:Z

    if-eq v0, p1, :cond_0

    .line 970
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mLoadsImagesAutomatically:Z

    .line 971
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 973
    :cond_0
    monitor-exit v1

    .line 974
    return-void

    .line 973
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMediaPlaybackRequiresUserGesture(Z)V
    .locals 2
    .param p1, "require"    # Z

    .prologue
    .line 1415
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1416
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMediaPlaybackRequiresUserGesture:Z

    if-eq v0, p1, :cond_0

    .line 1417
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMediaPlaybackRequiresUserGesture:Z

    .line 1418
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1420
    :cond_0
    monitor-exit v1

    .line 1421
    return-void

    .line 1420
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMinimumFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 820
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 821
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwSettings;->clipFontSize(I)I

    move-result p1

    .line 822
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumFontSize:I

    if-eq v0, p1, :cond_0

    .line 823
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumFontSize:I

    .line 824
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 826
    :cond_0
    monitor-exit v1

    .line 827
    return-void

    .line 826
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMinimumLogicalFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 848
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 849
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwSettings;->clipFontSize(I)I

    move-result p1

    .line 850
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumLogicalFontSize:I

    if-eq v0, p1, :cond_0

    .line 851
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMinimumLogicalFontSize:I

    .line 852
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 854
    :cond_0
    monitor-exit v1

    .line 855
    return-void

    .line 854
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMixedContentMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1545
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1546
    :try_start_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    if-eq v0, p1, :cond_0

    .line 1547
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mMixedContentMode:I

    .line 1548
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1550
    :cond_0
    monitor-exit v1

    .line 1551
    return-void

    .line 1550
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPluginState(Landroid/webkit/WebSettings$PluginState;)V
    .locals 2
    .param p1, "state"    # Landroid/webkit/WebSettings$PluginState;

    .prologue
    .line 1074
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1075
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    if-eq v0, p1, :cond_0

    .line 1076
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    .line 1077
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1079
    :cond_0
    monitor-exit v1

    .line 1080
    return-void

    .line 1079
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPluginsEnabled(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 1067
    if-eqz p1, :cond_0

    sget-object v0, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/org/chromium/android_webview/AwSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 1068
    return-void

    .line 1067
    :cond_0
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    goto :goto_0
.end method

.method public setSansSerifFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 712
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 713
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSansSerifFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 714
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSansSerifFontFamily:Ljava/lang/String;

    .line 715
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 717
    :cond_0
    monitor-exit v1

    .line 718
    return-void

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSaveFormData(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 515
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 516
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAutoCompleteEnabled:Z

    if-eq v0, p1, :cond_0

    .line 517
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAutoCompleteEnabled:Z

    .line 518
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwSettings$2;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/android_webview/AwSettings$2;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    invoke-virtual {v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->runOnUiThreadBlockingAndLocked(Ljava/lang/Runnable;)V

    .line 527
    :cond_0
    monitor-exit v1

    .line 528
    return-void

    .line 527
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSerifFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 739
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 740
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSerifFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 741
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSerifFontFamily:Ljava/lang/String;

    .line 742
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 744
    :cond_0
    monitor-exit v1

    .line 745
    return-void

    .line 744
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setShouldFocusFirstNode(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 407
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 408
    :try_start_0
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mShouldFocusFirstNode:Z

    .line 409
    monitor-exit v1

    .line 410
    return-void

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setSpatialNavigationEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 438
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 439
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSpatialNavigationEnabled:Z

    if-eq v0, p1, :cond_0

    .line 440
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSpatialNavigationEnabled:Z

    .line 441
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 443
    :cond_0
    monitor-exit v1

    .line 444
    return-void

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setStandardFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 658
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 659
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mStandardFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 660
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mStandardFontFamily:Ljava/lang/String;

    .line 661
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 663
    :cond_0
    monitor-exit v1

    .line 664
    return-void

    .line 663
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSupportMultipleWindows(Z)V
    .locals 2
    .param p1, "support"    # Z

    .prologue
    .line 1174
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1175
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportMultipleWindows:Z

    if-eq v0, p1, :cond_0

    .line 1176
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportMultipleWindows:Z

    .line 1177
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1179
    :cond_0
    monitor-exit v1

    .line 1180
    return-void

    .line 1179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSupportZoom(Z)V
    .locals 3
    .param p1, "support"    # Z

    .prologue
    .line 1486
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1487
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportZoom:Z

    if-eq v0, p1, :cond_0

    .line 1488
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportZoom:Z

    .line 1489
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsDoubleTapZoomLocked()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsMultiTouchZoomLocked()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings;->onGestureZoomSupportChanged(ZZ)V

    .line 1492
    :cond_0
    monitor-exit v1

    .line 1493
    return-void

    .line 1492
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTextZoom(I)V
    .locals 2
    .param p1, "textZoom"    # I

    .prologue
    .line 631
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 632
    :try_start_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mTextSizePercent:I

    if-eq v0, p1, :cond_0

    .line 633
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mTextSizePercent:I

    .line 634
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 636
    :cond_0
    monitor-exit v1

    .line 637
    return-void

    .line 636
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setUseWideViewPort(Z)V
    .locals 3
    .param p1, "use"    # Z

    .prologue
    .line 1207
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1208
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUseWideViewport:Z

    if-eq v0, p1, :cond_0

    .line 1209
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUseWideViewport:Z

    .line 1210
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsDoubleTapZoomLocked()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsMultiTouchZoomLocked()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings;->onGestureZoomSupportChanged(ZZ)V

    .line 1212
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1214
    :cond_0
    monitor-exit v1

    .line 1215
    return-void

    .line 1214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setUserAgentString(Ljava/lang/String;)V
    .locals 4
    .param p1, "ua"    # Ljava/lang/String;

    .prologue
    .line 557
    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 558
    :try_start_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUserAgent:Ljava/lang/String;

    .line 559
    .local v0, "oldUserAgent":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 560
    :cond_0
    # getter for: Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;->sInstance:Ljava/lang/String;
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwSettings$LazyDefaultUserAgent;->access$500()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUserAgent:Ljava/lang/String;

    .line 564
    :goto_0
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 565
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    new-instance v3, Lcom/android/org/chromium/android_webview/AwSettings$3;

    invoke-direct {v3, p0}, Lcom/android/org/chromium/android_webview/AwSettings$3;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    invoke-virtual {v1, v3}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->runOnUiThreadBlockingAndLocked(Ljava/lang/Runnable;)V

    .line 574
    :cond_1
    monitor-exit v2

    .line 575
    return-void

    .line 562
    :cond_2
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mUserAgent:Ljava/lang/String;

    goto :goto_0

    .line 574
    .end local v0    # "oldUserAgent":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setVideoOverlayForEmbeddedVideoEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1577
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1578
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mVideoOverlayForEmbeddedVideoEnabled:Z

    if-eq v0, p1, :cond_0

    .line 1579
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mVideoOverlayForEmbeddedVideoEnabled:Z

    .line 1580
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwSettings$6;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/android_webview/AwSettings$6;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    invoke-virtual {v0, v2}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->runOnUiThreadBlockingAndLocked(Ljava/lang/Runnable;)V

    .line 1589
    :cond_0
    monitor-exit v1

    .line 1590
    return-void

    .line 1589
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setWebContents(J)V
    .locals 7
    .param p1, "nativeWebContents"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 278
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 279
    :try_start_0
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 280
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeDestroy(J)V

    .line 281
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 283
    :cond_0
    cmp-long v0, p1, v4

    if-eqz v0, :cond_1

    .line 284
    :try_start_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->bindUiThread()V

    .line 285
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwSettings;->nativeInit(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mNativeAwSettings:J

    .line 286
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->updateEverythingLocked()V

    .line 288
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    return-void
.end method

.method public setZeroLayoutHeightDisablesViewportQuirk(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 1233
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1234
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mZeroLayoutHeightDisablesViewportQuirk:Z

    if-eq v0, p1, :cond_0

    .line 1235
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mZeroLayoutHeightDisablesViewportQuirk:Z

    .line 1236
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mEventHandler:Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings$EventHandler;->updateWebkitPreferencesLocked()V

    .line 1238
    :cond_0
    monitor-exit v1

    .line 1239
    return-void

    .line 1238
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setZoomListener(Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;)V
    .locals 2
    .param p1, "zoomChangeListener"    # Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;

    .prologue
    .line 272
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 273
    :try_start_0
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mZoomChangeListener:Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;

    .line 274
    monitor-exit v1

    .line 275
    return-void

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method shouldDisplayZoomControls()Z
    .locals 2

    .prologue
    .line 1626
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1627
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwSettings;->supportsMultiTouchZoomLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mDisplayZoomControls:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1628
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public shouldFocusFirstNode()Z
    .locals 2

    .prologue
    .line 486
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 487
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mShouldFocusFirstNode:Z

    monitor-exit v1

    return v0

    .line 488
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public supportMultipleWindows()Z
    .locals 2

    .prologue
    .line 1186
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1187
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportMultipleWindows:Z

    monitor-exit v1

    return v0

    .line 1188
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public supportZoom()Z
    .locals 2

    .prologue
    .line 1499
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mAwSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1500
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwSettings;->mSupportZoom:Z

    monitor-exit v1

    return v0

    .line 1501
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
