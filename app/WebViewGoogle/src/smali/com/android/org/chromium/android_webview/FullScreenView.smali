.class public Lcom/android/org/chromium/android_webview/FullScreenView;
.super Landroid/widget/AbsoluteLayout;
.source "FullScreenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/android_webview/FullScreenView$1;,
        Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;
    }
.end annotation


# instance fields
.field private mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

.field private mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwViewMethods;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "awViewMethods"    # Lcom/android/org/chromium/android_webview/AwViewMethods;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/AbsoluteLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-virtual {p0, p2}, Lcom/android/org/chromium/android_webview/FullScreenView;->setAwViewMethods(Lcom/android/org/chromium/android_webview/AwViewMethods;)V

    .line 31
    new-instance v0, Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;-><init>(Lcom/android/org/chromium/android_webview/FullScreenView;Lcom/android/org/chromium/android_webview/FullScreenView$1;)V

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;

    .line 32
    return-void
.end method

.method static synthetic access$101(Lcom/android/org/chromium/android_webview/FullScreenView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/FullScreenView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 23
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$301(Lcom/android/org/chromium/android_webview/FullScreenView;Landroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/FullScreenView;
    .param p1, "x1"    # Landroid/view/KeyEvent;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$401(Lcom/android/org/chromium/android_webview/FullScreenView;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/FullScreenView;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$501(Lcom/android/org/chromium/android_webview/FullScreenView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/FullScreenView;

    .prologue
    .line 23
    invoke-super {p0}, Landroid/view/View;->getScrollBarStyle()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/org/chromium/android_webview/FullScreenView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/FullScreenView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/android/org/chromium/android_webview/FullScreenView;->setMeasuredDimension(II)V

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public getInternalAccessAdapter()Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Landroid/widget/AbsoluteLayout;->onAttachedToWindow()V

    .line 102
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onAttachedToWindow()V

    .line 103
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 97
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/widget/AbsoluteLayout;->onDetachedFromWindow()V

    .line 108
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onDetachedFromWindow()V

    .line 109
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onDraw(Landroid/graphics/Canvas;)V

    .line 45
    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AbsoluteLayout;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 121
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 123
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onMeasure(II)V

    .line 50
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "ow"    # I
    .param p4, "oh"    # I

    .prologue
    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AbsoluteLayout;->onSizeChanged(IIII)V

    .line 128
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onSizeChanged(IIII)V

    .line 129
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 133
    invoke-super {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 134
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onVisibilityChanged(Landroid/view/View;I)V

    .line 135
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->onWindowFocusChanged(Z)V

    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onWindowFocusChanged(Z)V

    .line 115
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 139
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->onWindowVisibilityChanged(I)V

    .line 140
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onWindowVisibilityChanged(I)V

    .line 141
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->requestFocus()V

    .line 55
    invoke-super {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public setAwViewMethods(Lcom/android/org/chromium/android_webview/AwViewMethods;)V
    .locals 0
    .param p1, "awViewMethods"    # Lcom/android/org/chromium/android_webview/AwViewMethods;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    .line 40
    return-void
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .locals 1
    .param p1, "layerType"    # I
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 61
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/FullScreenView;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->setLayerType(ILandroid/graphics/Paint;)V

    .line 62
    return-void
.end method
