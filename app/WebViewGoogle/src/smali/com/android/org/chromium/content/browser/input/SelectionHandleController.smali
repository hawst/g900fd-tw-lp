.class public abstract Lcom/android/org/chromium/content/browser/input/SelectionHandleController;
.super Ljava/lang/Object;
.source "SelectionHandleController.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/input/CursorController;


# instance fields
.field private mAllowAutomaticShowing:Z

.field private mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

.field private mFixedHandleX:I

.field private mFixedHandleY:I

.field private mIsShowing:Z

.field private mParent:Landroid/view/View;

.field private mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

.field private mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "positionObserver"    # Lcom/android/org/chromium/content/browser/PositionObserver;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mAllowAutomaticShowing:Z

    .line 41
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mParent:Landroid/view/View;

    .line 42
    iput-object p2, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    .line 43
    return-void
.end method

.method private createHandlesIfNeeded(II)V
    .locals 6
    .param p1, "startDir"    # I
    .param p2, "endDir"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 196
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-nez v0, :cond_0

    .line 197
    new-instance v3, Lcom/android/org/chromium/content/browser/input/HandleView;

    if-ne p1, v1, :cond_2

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mParent:Landroid/view/View;

    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-direct {v3, p0, v0, v4, v5}, Lcom/android/org/chromium/content/browser/input/HandleView;-><init>(Lcom/android/org/chromium/content/browser/input/CursorController;ILandroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    iput-object v3, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-nez v0, :cond_1

    .line 202
    new-instance v0, Lcom/android/org/chromium/content/browser/input/HandleView;

    if-ne p2, v1, :cond_3

    :goto_1
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-direct {v0, p0, v2, v1, v3}, Lcom/android/org/chromium/content/browser/input/HandleView;-><init>(Lcom/android/org/chromium/content/browser/input/CursorController;ILandroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    .line 206
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 197
    goto :goto_0

    :cond_3
    move v2, v1

    .line 202
    goto :goto_1
.end method

.method private showHandlesIfNeeded()V
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mIsShowing:Z

    if-nez v0, :cond_0

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mIsShowing:Z

    .line 211
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->show()V

    .line 212
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->show()V

    .line 213
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->setHandleVisibility(I)V

    .line 215
    :cond_0
    return-void
.end method


# virtual methods
.method public allowAutomaticShowing()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mAllowAutomaticShowing:Z

    .line 48
    return-void
.end method

.method public beforeStartUpdatingPosition(Lcom/android/org/chromium/content/browser/input/HandleView;)V
    .locals 2
    .param p1, "handle"    # Lcom/android/org/chromium/content/browser/input/HandleView;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    .line 89
    .local v0, "fixedHandle":Lcom/android/org/chromium/content/browser/input/HandleView;
    :goto_0
    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getAdjustedPositionX()I

    move-result v1

    iput v1, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mFixedHandleX:I

    .line 90
    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getLineAdjustedPositionY()I

    move-result v1

    iput v1, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mFixedHandleY:I

    .line 91
    return-void

    .line 88
    .end local v0    # "fixedHandle":Lcom/android/org/chromium/content/browser/input/HandleView;
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    goto :goto_0
.end method

.method public beginHandleFadeIn()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->beginFadeIn()V

    .line 149
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->beginFadeIn()V

    .line 150
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mIsShowing:Z

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->hide()V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->hide()V

    .line 66
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mIsShowing:Z

    .line 68
    :cond_2
    return-void
.end method

.method public hideAndDisallowAutomaticShowing()V
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->hide()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mAllowAutomaticShowing:Z

    .line 54
    return-void
.end method

.method public isDragging()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->isDragging()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mIsShowing:Z

    return v0
.end method

.method public onSelectionChanged(II)V
    .locals 1
    .param p1, "startDir"    # I
    .param p2, "endDir"    # I

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mAllowAutomaticShowing:Z

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0, p1, p2}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->showHandles(II)V

    .line 170
    :cond_0
    return-void
.end method

.method public onTouchModeChanged(Z)V
    .locals 0
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 117
    if-nez p1, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->hide()V

    .line 120
    :cond_0
    return-void
.end method

.method protected abstract selectBetweenCoordinates(IIII)V
.end method

.method public setEndHandlePosition(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/HandleView;->positionAt(II)V

    .line 141
    return-void
.end method

.method public setHandleVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/input/HandleView;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mEndHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/input/HandleView;->setVisibility(I)V

    .line 158
    return-void
.end method

.method public setStartHandlePosition(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mStartHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/HandleView;->positionAt(II)V

    .line 132
    return-void
.end method

.method public showHandles(II)V
    .locals 0
    .param p1, "startDir"    # I
    .param p2, "endDir"    # I

    .prologue
    .line 181
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->createHandlesIfNeeded(II)V

    .line 182
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->showHandlesIfNeeded()V

    .line 183
    return-void
.end method

.method public updatePosition(Lcom/android/org/chromium/content/browser/input/HandleView;II)V
    .locals 2
    .param p1, "handle"    # Lcom/android/org/chromium/content/browser/input/HandleView;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 83
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mFixedHandleX:I

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->mFixedHandleY:I

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->selectBetweenCoordinates(IIII)V

    .line 84
    return-void
.end method
