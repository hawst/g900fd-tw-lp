.class public Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;
.super Lcom/android/org/chromium/content/browser/ContentVideoView;
.source "ContentVideoViewLegacy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;,
        Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;
    }
.end annotation


# instance fields
.field private mCanPause:Z

.field private mCanSeekBackward:Z

.field private mCanSeekForward:Z

.field private mCurrentBufferPercentage:I

.field private mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

.field private mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;


# direct methods
.method constructor <init>(Landroid/content/Context;JLcom/android/org/chromium/content/browser/ContentVideoViewClient;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nativeContentVideoView"    # J
    .param p4, "client"    # Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/content/browser/ContentVideoView;-><init>(Landroid/content/Context;JLcom/android/org/chromium/content/browser/ContentVideoViewClient;)V

    .line 83
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->setBackgroundColor(I)V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCurrentBufferPercentage:I

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->toggleMediaControlsVisiblity()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanPause:Z

    return v0
.end method

.method static synthetic access$300(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanSeekBackward:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanSeekForward:Z

    return v0
.end method

.method static synthetic access$500(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .prologue
    .line 22
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCurrentBufferPercentage:I

    return v0
.end method

.method private toggleMediaControlsVisiblity()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->hide()V

    .line 276
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected destroyContentVideoView(Z)V
    .locals 2
    .param p1, "nativeViewDestroyed"    # Z

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->setEnabled(Z)V

    .line 282
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->hide()V

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    .line 285
    :cond_0
    invoke-super {p0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->destroyContentVideoView(Z)V

    .line 286
    return-void
.end method

.method protected onBufferingUpdate(I)V
    .locals 0
    .param p1, "percent"    # I

    .prologue
    .line 165
    invoke-super {p0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->onBufferingUpdate(I)V

    .line 166
    iput p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCurrentBufferPercentage:I

    .line 167
    return-void
.end method

.method protected onCompletion()V
    .locals 1

    .prologue
    .line 256
    invoke-super {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->onCompletion()V

    .line 257
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->hide()V

    .line 260
    :cond_0
    return-void
.end method

.method public onMediaPlayerError(I)V
    .locals 1
    .param p1, "errorType"    # I

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->onMediaPlayerError(I)V

    .line 159
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->hide()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 290
    const/4 v0, 0x1

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-eqz v0, :cond_0

    .line 265
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->toggleMediaControlsVisiblity()V

    .line 267
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onUpdateMediaMetadata(IIIZZZ)V
    .locals 2
    .param p1, "videoWidth"    # I
    .param p2, "videoHeight"    # I
    .param p3, "duration"    # I
    .param p4, "canPause"    # Z
    .param p5, "canSeekBack"    # Z
    .param p6, "canSeekForward"    # Z

    .prologue
    .line 177
    invoke-super/range {p0 .. p6}, Lcom/android/org/chromium/content/browser/ContentVideoView;->onUpdateMediaMetadata(IIIZZZ)V

    .line 179
    iput-boolean p4, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanPause:Z

    .line 180
    iput-boolean p5, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanSeekBackward:Z

    .line 181
    iput-boolean p6, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanSeekForward:Z

    .line 183
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->setEnabled(Z)V

    .line 187
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->show()V

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->show(I)V

    goto :goto_0
.end method

.method protected openVideo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 207
    invoke-super {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->openVideo()V

    .line 209
    iput v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCurrentBufferPercentage:I

    .line 211
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-eqz v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 212
    :cond_0
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    invoke-direct {v0, v1, p0, v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    .line 213
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    new-instance v1, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;-><init>(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    .line 250
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->setAnchorView(Landroid/view/View;)V

    .line 251
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v0, v3}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected showContentVideoView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 89
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v0

    .line 90
    .local v0, "surfaceView":Landroid/view/SurfaceView;
    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setZOrderOnTop(Z)V

    .line 91
    new-instance v1, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;-><init>(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 140
    new-instance v1, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;-><init>(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V

    invoke-virtual {p0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 150
    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setFocusable(Z)V

    .line 151
    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setFocusableInTouchMode(Z)V

    .line 152
    invoke-virtual {v0}, Landroid/view/SurfaceView;->requestFocus()Z

    .line 153
    invoke-super {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->showContentVideoView()V

    .line 154
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v1, 0x1

    .line 196
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/content/browser/ContentVideoView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 197
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v0

    .line 198
    .local v0, "surfaceView":Landroid/view/SurfaceView;
    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setFocusable(Z)V

    .line 199
    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setFocusableInTouchMode(Z)V

    .line 200
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->show()V

    .line 203
    :cond_0
    return-void
.end method
