.class Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;
.super Ljava/lang/Object;
.source "ContentVideoViewLegacy.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->showContentVideoView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v6, 0x54

    const/16 v5, 0x52

    const/4 v4, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 94
    if-eq p2, v4, :cond_2

    const/16 v3, 0x18

    if-eq p2, v3, :cond_2

    const/16 v3, 0x19

    if-eq p2, v3, :cond_2

    const/16 v3, 0xa4

    if-eq p2, v3, :cond_2

    const/4 v3, 0x5

    if-eq p2, v3, :cond_2

    if-eq p2, v5, :cond_2

    if-eq p2, v6, :cond_2

    const/4 v3, 0x6

    if-eq p2, v3, :cond_2

    move v0, v1

    .line 103
    .local v0, "isKeyCodeSupported":Z
    :goto_0
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v3}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isInPlaybackState()Z

    move-result v3

    if-eqz v3, :cond_9

    if-eqz v0, :cond_9

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    invoke-static {v3}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 104
    const/16 v3, 0x4f

    if-eq p2, v3, :cond_0

    const/16 v3, 0x55

    if-ne p2, v3, :cond_4

    .line 106
    :cond_0
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 107
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->pause()V

    .line 108
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->show()V

    .line 137
    :cond_1
    :goto_1
    return v1

    .end local v0    # "isKeyCodeSupported":Z
    :cond_2
    move v0, v2

    .line 94
    goto :goto_0

    .line 110
    .restart local v0    # "isKeyCodeSupported":Z
    :cond_3
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->start()V

    .line 111
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->hide()V

    goto :goto_1

    .line 114
    :cond_4
    const/16 v3, 0x7e

    if-ne p2, v3, :cond_5

    .line 115
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    .line 116
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->start()V

    .line 117
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->hide()V

    goto :goto_1

    .line 120
    :cond_5
    const/16 v3, 0x56

    if-eq p2, v3, :cond_6

    const/16 v3, 0x7f

    if-ne p2, v3, :cond_7

    .line 122
    :cond_6
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->pause()V

    .line 124
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->show()V

    goto :goto_1

    .line 128
    :cond_7
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # invokes: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->toggleMediaControlsVisiblity()V
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V

    :cond_8
    move v1, v2

    .line 137
    goto :goto_1

    .line 130
    :cond_9
    if-ne p2, v4, :cond_a

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v1, :cond_a

    .line 132
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$1;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v3, v2}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->exitFullscreen(Z)V

    goto :goto_1

    .line 134
    :cond_a
    if-eq p2, v5, :cond_1

    if-ne p2, v6, :cond_8

    goto :goto_1
.end method
