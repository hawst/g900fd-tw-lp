.class Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
.super Landroid/widget/MediaController;
.source "ContentVideoViewLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullScreenMediaController"
.end annotation


# instance fields
.field final mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

.field final mVideoView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "video"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p2, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mVideoView:Landroid/view/View;

    .line 58
    iput-object p3, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    .line 59
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mVideoView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mVideoView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;->onMediaControlsVisibilityChanged(Z)V

    .line 76
    :cond_1
    invoke-super {p0}, Landroid/widget/MediaController;->hide()V

    .line 77
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/MediaController;->show()V

    .line 64
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mListener:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$MediaControlsVisibilityListener;->onMediaControlsVisibilityChanged(Z)V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mVideoView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;->mVideoView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 68
    :cond_1
    return-void
.end method
