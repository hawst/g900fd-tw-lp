.class Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;
.super Ljava/lang/Object;
.source "InsertionHandleController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PastePopupMenu"
.end annotation


# instance fields
.field private final mContainer:Landroid/widget/PopupWindow;

.field private final mPasteViewLayouts:[I

.field private final mPasteViews:[Landroid/view/View;

.field private mPositionX:I

.field private mPositionY:I

.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;


# direct methods
.method public constructor <init>(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 206
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    new-instance v3, Landroid/widget/PopupWindow;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$000(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x10102c8

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    .line 209
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setSplitTouchEnabled(Z)V

    .line 210
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v8}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 212
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v7}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 213
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v7}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 215
    const/4 v3, 0x4

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    .line 222
    .local v0, "POPUP_LAYOUT_ATTRS":[I
    array-length v3, v0

    new-array v3, v3, [Landroid/view/View;

    iput-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPasteViews:[Landroid/view/View;

    .line 223
    array-length v3, v0

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPasteViewLayouts:[I

    .line 225
    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$000(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 226
    .local v1, "attrs":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 227
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPasteViewLayouts:[I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    invoke-virtual {v1, v4, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    aput v4, v3, v2

    .line 226
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 229
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 230
    return-void

    .line 215
    nop

    :array_0
    .array-data 4
        0x1010314
        0x1010315
        0x101035e
        0x101035f
    .end array-data
.end method

.method private updateContent(Z)V
    .locals 9
    .param p1, "onTop"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 237
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->viewIndex(Z)I

    move-result v4

    .line 238
    .local v4, "viewIndex":I
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPasteViews:[Landroid/view/View;

    aget-object v3, v5, v4

    .line 240
    .local v3, "view":Landroid/view/View;
    if-nez v3, :cond_2

    .line 241
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPasteViewLayouts:[I

    aget v1, v5, v4

    .line 242
    .local v1, "layout":I
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$000(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 244
    .local v0, "inflater":Landroid/view/LayoutInflater;
    if-eqz v0, :cond_0

    .line 245
    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 248
    :cond_0
    if-nez v3, :cond_1

    .line 249
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Unable to inflate TextEdit paste window"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 252
    :cond_1
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 253
    .local v2, "size":I
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    invoke-virtual {v3, v2, v2}, Landroid/view/View;->measure(II)V

    .line 257
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPasteViews:[Landroid/view/View;

    aput-object v3, v5, v4

    .line 262
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "layout":I
    .end local v2    # "size":I
    :cond_2
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v5, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 263
    return-void
.end method

.method private viewIndex(Z)I
    .locals 3
    .param p1, "onTop"    # Z

    .prologue
    const/4 v1, 0x0

    .line 233
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->canPaste()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method hide()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 272
    return-void
.end method

.method isShowing()Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->canPaste()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->paste()V

    .line 283
    :cond_0
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    .line 284
    return-void
.end method

.method positionAtCursor()V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 287
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v8}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 288
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 289
    .local v7, "width":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 291
    .local v4, "height":I
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->getLineHeight()I

    move-result v5

    .line 293
    .local v5, "lineHeight":I
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$100(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Lcom/android/org/chromium/content/browser/input/HandleView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/org/chromium/content/browser/input/HandleView;->getAdjustedPositionX()I

    move-result v8

    int-to-float v8, v8

    int-to-float v9, v7

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPositionX:I

    .line 294
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$100(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Lcom/android/org/chromium/content/browser/input/HandleView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/org/chromium/content/browser/input/HandleView;->getAdjustedPositionY()I

    move-result v8

    sub-int/2addr v8, v4

    sub-int/2addr v8, v5

    iput v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPositionY:I

    .line 296
    const/4 v8, 0x2

    new-array v1, v8, [I

    .line 297
    .local v1, "coords":[I
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mParent:Landroid/view/View;
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$200(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 298
    aget v8, v1, v12

    iget v9, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPositionX:I

    add-int/2addr v8, v9

    aput v8, v1, v12

    .line 299
    aget v8, v1, v11

    iget v9, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mPositionY:I

    add-int/2addr v8, v9

    aput v8, v1, v11

    .line 301
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$000(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v6, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 302
    .local v6, "screenWidth":I
    aget v8, v1, v11

    if-gez v8, :cond_1

    .line 303
    invoke-direct {p0, v12}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->updateContent(Z)V

    .line 305
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v8}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 307
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 311
    aget v8, v1, v11

    add-int/2addr v8, v4

    aput v8, v1, v11

    .line 312
    aget v8, v1, v11

    add-int/2addr v8, v5

    aput v8, v1, v11

    .line 315
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$100(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Lcom/android/org/chromium/content/browser/input/HandleView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/org/chromium/content/browser/input/HandleView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 316
    .local v2, "handle":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    div-int/lit8 v3, v8, 0x2

    .line 318
    .local v3, "handleHalfWidth":I
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$100(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Lcom/android/org/chromium/content/browser/input/HandleView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/org/chromium/content/browser/input/HandleView;->getAdjustedPositionX()I

    move-result v8

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_0

    .line 319
    aget v8, v1, v12

    div-int/lit8 v9, v7, 0x2

    add-int/2addr v9, v3

    add-int/2addr v8, v9

    aput v8, v1, v12

    .line 329
    .end local v2    # "handle":Landroid/graphics/drawable/Drawable;
    .end local v3    # "handleHalfWidth":I
    :goto_0
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    iget-object v9, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->this$0:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    # getter for: Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mParent:Landroid/view/View;
    invoke-static {v9}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->access$200(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/view/View;

    move-result-object v9

    aget v10, v1, v12

    aget v11, v1, v11

    invoke-virtual {v8, v9, v12, v10, v11}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 330
    return-void

    .line 321
    .restart local v2    # "handle":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "handleHalfWidth":I
    :cond_0
    aget v8, v1, v12

    div-int/lit8 v9, v7, 0x2

    add-int/2addr v9, v3

    sub-int/2addr v8, v9

    aput v8, v1, v12

    goto :goto_0

    .line 325
    .end local v2    # "handle":Landroid/graphics/drawable/Drawable;
    .end local v3    # "handleHalfWidth":I
    :cond_1
    aget v8, v1, v12

    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    aput v8, v1, v12

    .line 326
    sub-int v8, v6, v7

    aget v9, v1, v12

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    aput v8, v1, v12

    goto :goto_0
.end method

.method show()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->updateContent(Z)V

    .line 267
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->positionAtCursor()V

    .line 268
    return-void
.end method
