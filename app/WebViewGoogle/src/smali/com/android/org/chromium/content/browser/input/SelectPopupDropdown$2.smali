.class Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$2;
.super Ljava/lang/Object;
.source "SelectPopupDropdown.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Ljava/util/List;Landroid/graphics/Rect;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$2;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$2;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    # getter for: Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->mAlreadySelectedItems:Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->access$100(Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$2;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    # getter for: Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->access$000(Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->selectPopupMenuItems([I)V

    .line 70
    :cond_0
    return-void
.end method
