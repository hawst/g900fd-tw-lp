.class Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;
.super Landroid/view/SurfaceView;
.source "ContentVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/ContentVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoSurfaceView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;


# direct methods
.method public constructor <init>(Lcom/android/org/chromium/content/browser/ContentVideoView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    .line 99
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 100
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 106
    const/4 v1, 0x1

    .line 107
    .local v1, "width":I
    const/4 v0, 0x1

    .line 108
    .local v0, "height":I
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    if-lez v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    invoke-static {v2, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->getDefaultSize(II)I

    move-result v1

    .line 110
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    invoke-static {v2, p2}, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->getDefaultSize(II)I

    move-result v0

    .line 111
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    mul-int/2addr v2, v0

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I
    invoke-static {v3}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v3

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_1

    .line 112
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    mul-int/2addr v2, v1

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I
    invoke-static {v3}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v3

    div-int v0, v2, v3

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->setMeasuredDimension(II)V

    .line 118
    return-void

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    mul-int/2addr v2, v0

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I
    invoke-static {v3}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v3

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    .line 114
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v2

    mul-int/2addr v2, v0

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoView;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I
    invoke-static {v3}, Lcom/android/org/chromium/content/browser/ContentVideoView;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I

    move-result v3

    div-int v1, v2, v3

    goto :goto_0
.end method
