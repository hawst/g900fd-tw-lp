.class public Lcom/android/org/chromium/base/library_loader/LibraryLoader;
.super Ljava/lang/Object;
.source "LibraryLoader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sInitialized:Z

.field private static sLoaded:Z

.field private static final sLock:Ljava/lang/Object;

.field private static sNativeLibraryHackWasUsed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const-class v0, Lcom/android/org/chromium/base/library_loader/LibraryLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->$assertionsDisabled:Z

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    .line 38
    sput-boolean v1, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLoaded:Z

    .line 43
    sput-boolean v1, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    .line 48
    sput-boolean v1, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    return-void

    :cond_0
    move v0, v1

    .line 30
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ensureInitialized()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->ensureInitialized(Landroid/content/Context;Z)V

    .line 58
    return-void
.end method

.method public static ensureInitialized(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shouldDeleteOldWorkaroundLibraries"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    .line 80
    sget-object v1, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 81
    :try_start_0
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    .line 83
    monitor-exit v1

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-static {p0, p1}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->loadAlreadyLocked(Landroid/content/Context;Z)V

    .line 86
    invoke-static {}, Lcom/android/org/chromium/base/CommandLine;->getJavaSwitchesOrNull()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->initializeAlreadyLocked([Ljava/lang/String;)V

    .line 87
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static initialize([Ljava/lang/String;)V
    .locals 2
    .param p0, "initCommandLine"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    .line 137
    sget-object v1, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    invoke-static {p0}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->initializeAlreadyLocked([Ljava/lang/String;)V

    .line 139
    monitor-exit v1

    .line 140
    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static initializeAlreadyLocked([Ljava/lang/String;)V
    .locals 3
    .param p0, "initCommandLine"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 206
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-static {p0}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->nativeLibraryLoaded([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 210
    const-string v0, "LibraryLoader"

    const-string v1, "error calling nativeLibraryLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    new-instance v0, Lcom/android/org/chromium/base/library_loader/ProcessInitException;

    invoke-direct {v0, v2}, Lcom/android/org/chromium/base/library_loader/ProcessInitException;-><init>(I)V

    throw v0

    .line 216
    :cond_1
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    .line 217
    invoke-static {}, Lcom/android/org/chromium/base/CommandLine;->enableNativeProxy()V

    .line 220
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->registerNativeEnabledObserver()V

    .line 223
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->isUsed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->loadAtFixedAddressFailed()Z

    move-result v0

    invoke-static {}, Lcom/android/org/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->nativeRecordChromiumAndroidLinkerHistogram(ZZ)V

    .line 228
    :cond_2
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    invoke-static {v0}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->nativeRecordNativeLibraryHack(Z)V

    goto :goto_0
.end method

.method private static loadAlreadyLocked(Landroid/content/Context;Z)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shouldDeleteOldWorkaroundLibraries"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    .line 147
    :try_start_0
    sget-boolean v12, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLoaded:Z

    if-nez v12, :cond_7

    .line 148
    sget-boolean v12, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->$assertionsDisabled:Z

    if-nez v12, :cond_0

    sget-boolean v12, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    if-eqz v12, :cond_0

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :catch_0
    move-exception v3

    .line 190
    .local v3, "e":Ljava/lang/UnsatisfiedLinkError;
    new-instance v12, Lcom/android/org/chromium/base/library_loader/ProcessInitException;

    const/4 v13, 0x2

    invoke-direct {v12, v13, v3}, Lcom/android/org/chromium/base/library_loader/ProcessInitException;-><init>(ILjava/lang/Throwable;)V

    throw v12

    .line 150
    .end local v3    # "e":Ljava/lang/UnsatisfiedLinkError;
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 151
    .local v8, "startTime":J
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->isUsed()Z

    move-result v7

    .line 153
    .local v7, "useChromiumLinker":Z
    if-eqz v7, :cond_1

    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->prepareLibraryLoad()V

    .line 155
    :cond_1
    sget-object v2, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->LIBRARIES:[Ljava/lang/String;

    .local v2, "arr$":[Ljava/lang/String;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_4

    aget-object v6, v2, v4

    .line 156
    .local v6, "library":Ljava/lang/String;
    const-string v12, "LibraryLoader"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Loading: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-eqz v7, :cond_2

    .line 158
    invoke-static {v6}, Lcom/android/org/chromium/base/library_loader/Linker;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    .line 155
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 161
    :cond_2
    :try_start_2
    invoke-static {v6}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 162
    :catch_1
    move-exception v3

    .line 163
    .restart local v3    # "e":Ljava/lang/UnsatisfiedLinkError;
    if-eqz p0, :cond_3

    :try_start_3
    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/android/org/chromium/base/library_loader/LibraryLoaderHelper;->tryLoadLibraryUsingWorkaround(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 166
    const/4 v12, 0x1

    sput-boolean v12, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    goto :goto_1

    .line 168
    :cond_3
    throw v3

    .line 173
    .end local v3    # "e":Ljava/lang/UnsatisfiedLinkError;
    .end local v6    # "library":Ljava/lang/String;
    :cond_4
    if-eqz v7, :cond_5

    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->finishLibraryLoad()V

    .line 175
    :cond_5
    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    sget-boolean v12, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    if-nez v12, :cond_6

    .line 178
    invoke-static/range {p0 .. p0}, Lcom/android/org/chromium/base/library_loader/LibraryLoaderHelper;->deleteWorkaroundLibrariesAsynchronously(Landroid/content/Context;)V

    .line 182
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 183
    .local v10, "stopTime":J
    const-string v12, "LibraryLoader"

    const-string v13, "Time to load native libraries: %d ms (timestamps %d-%d)"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    sub-long v16, v10, v8

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-wide/16 v16, 0x2710

    rem-long v16, v8, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-wide/16 v16, 0x2710

    rem-long v16, v10, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    const/4 v12, 0x1

    sput-boolean v12, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLoaded:Z
    :try_end_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_3 .. :try_end_3} :catch_0

    .line 193
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "useChromiumLinker":Z
    .end local v8    # "startTime":J
    .end local v10    # "stopTime":J
    :cond_7
    const-string v12, "LibraryLoader"

    const-string v13, "Expected native library version number \"%s\",actual native library version number \"%s\""

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    sget-object v16, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->VERSION_NUMBER:Ljava/lang/String;

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static {}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->nativeGetVersionNumber()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    sget-object v12, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->VERSION_NUMBER:Ljava/lang/String;

    invoke-static {}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->nativeGetVersionNumber()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 199
    new-instance v12, Lcom/android/org/chromium/base/library_loader/ProcessInitException;

    const/4 v13, 0x3

    invoke-direct {v12, v13}, Lcom/android/org/chromium/base/library_loader/ProcessInitException;-><init>(I)V

    throw v12

    .line 201
    :cond_8
    return-void
.end method

.method public static loadNow()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->loadNow(Landroid/content/Context;Z)V

    .line 107
    return-void
.end method

.method public static loadNow(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shouldDeleteOldWorkaroundLibraries"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/chromium/base/library_loader/ProcessInitException;
        }
    .end annotation

    .prologue
    .line 124
    sget-object v1, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 125
    :try_start_0
    invoke-static {p0, p1}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->loadAlreadyLocked(Landroid/content/Context;Z)V

    .line 126
    monitor-exit v1

    .line 127
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static native nativeGetVersionNumber()Ljava/lang/String;
.end method

.method private static native nativeLibraryLoaded([Ljava/lang/String;)Z
.end method

.method private static native nativeRecordChromiumAndroidLinkerHistogram(ZZ)V
.end method

.method private static native nativeRecordNativeLibraryHack(Z)V
.end method
