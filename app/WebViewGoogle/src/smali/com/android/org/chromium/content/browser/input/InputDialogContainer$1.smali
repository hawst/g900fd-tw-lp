.class Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;
.super Ljava/lang/Object;
.source "InputDialogContainer.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->showSuggestionDialog(IDDDD[Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

.field final synthetic val$adapter:Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;

.field final synthetic val$dialogType:I

.field final synthetic val$dialogValue:D

.field final synthetic val$max:D

.field final synthetic val$min:D

.field final synthetic val$step:D


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/input/InputDialogContainer;Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;IDDDD)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    iput-object p2, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$adapter:Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;

    iput p3, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$dialogType:I

    iput-wide p4, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$dialogValue:D

    iput-wide p6, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$min:D

    iput-wide p8, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$max:D

    iput-wide p10, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$step:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$adapter:Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->dismissDialog()V

    .line 150
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$dialogType:I

    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$dialogValue:D

    iget-wide v4, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$min:D

    iget-wide v6, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$max:D

    iget-wide v8, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$step:D

    invoke-virtual/range {v0 .. v9}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->showPickerDialog(IDDDD)V

    .line 157
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->val$adapter:Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;

    invoke-virtual {v0, p3}, Lcom/android/org/chromium/content/browser/input/DateTimeSuggestionListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;->value()D

    move-result-wide v10

    .line 153
    .local v10, "suggestionValue":D
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    # getter for: Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->mInputActionDelegate:Lcom/android/org/chromium/content/browser/input/InputDialogContainer$InputActionDelegate;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->access$000(Lcom/android/org/chromium/content/browser/input/InputDialogContainer;)Lcom/android/org/chromium/content/browser/input/InputDialogContainer$InputActionDelegate;

    move-result-object v0

    invoke-interface {v0, v10, v11}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$InputActionDelegate;->replaceDateTime(D)V

    .line 154
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->dismissDialog()V

    .line 155
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$1;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    const/4 v1, 0x1

    # setter for: Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->mDialogAlreadyDismissed:Z
    invoke-static {v0, v1}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->access$102(Lcom/android/org/chromium/content/browser/input/InputDialogContainer;Z)Z

    goto :goto_0
.end method
