.class Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;
.super Ljava/lang/Object;
.source "DateTimeChooserAndroid.java"


# instance fields
.field private final mInputDialogContainer:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

.field private final mNativeDateTimeChooserAndroid:J


# direct methods
.method private constructor <init>(Landroid/content/Context;J)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nativeDateTimeChooserAndroid"    # J

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p2, p0, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->mNativeDateTimeChooserAndroid:J

    .line 25
    new-instance v0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    new-instance v1, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid$1;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid$1;-><init>(Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;)V

    invoke-direct {v0, p1, v1}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;-><init>(Landroid/content/Context;Lcom/android/org/chromium/content/browser/input/InputDialogContainer$InputActionDelegate;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->mInputDialogContainer:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->mNativeDateTimeChooserAndroid:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;JD)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;
    .param p1, "x1"    # J
    .param p3, "x2"    # D

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->nativeReplaceDateTime(JD)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;
    .param p1, "x1"    # J

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->nativeCancelDialog(J)V

    return-void
.end method

.method private static createDateTimeChooser(Lcom/android/org/chromium/content/browser/ContentViewCore;JIDDDD[Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;)Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;
    .locals 12
    .param p0, "contentViewCore"    # Lcom/android/org/chromium/content/browser/ContentViewCore;
    .param p1, "nativeDateTimeChooserAndroid"    # J
    .param p3, "dialogType"    # I
    .param p4, "dialogValue"    # D
    .param p6, "min"    # D
    .param p8, "max"    # D
    .param p10, "step"    # D
    .param p12, "suggestions"    # [Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;

    .prologue
    .line 53
    new-instance v0, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;-><init>(Landroid/content/Context;J)V

    .local v0, "chooser":Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;
    move v1, p3

    move-wide/from16 v2, p4

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    move-wide/from16 v8, p10

    move-object/from16 v10, p12

    .line 57
    invoke-direct/range {v0 .. v10}, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->showDialog(IDDDD[Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;)V

    .line 58
    return-object v0
.end method

.method private static createSuggestionsArray(I)[Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 63
    new-array v0, p0, [Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;

    return-object v0
.end method

.method private static initializeDateInputTypes(IIIIII)V
    .locals 0
    .param p0, "textInputTypeDate"    # I
    .param p1, "textInputTypeDateTime"    # I
    .param p2, "textInputTypeDateTimeLocal"    # I
    .param p3, "textInputTypeMonth"    # I
    .param p4, "textInputTypeTime"    # I
    .param p5, "textInputTypeWeek"    # I

    .prologue
    .line 84
    invoke-static/range {p0 .. p5}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->initializeInputTypes(IIIIII)V

    .line 88
    return-void
.end method

.method private native nativeCancelDialog(J)V
.end method

.method private native nativeReplaceDateTime(JD)V
.end method

.method private static setDateTimeSuggestionAt([Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;IDLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "array"    # [Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;
    .param p1, "index"    # I
    .param p2, "value"    # D
    .param p4, "localizedValue"    # Ljava/lang/String;
    .param p5, "label"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v0, Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;-><init>(DLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, p0, p1

    .line 77
    return-void
.end method

.method private showDialog(IDDDD[Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;)V
    .locals 12
    .param p1, "dialogType"    # I
    .param p2, "dialogValue"    # D
    .param p4, "min"    # D
    .param p6, "max"    # D
    .param p8, "step"    # D
    .param p10, "suggestions"    # [Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/DateTimeChooserAndroid;->mInputDialogContainer:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    move v1, p1

    move-wide v2, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->showDialog(IDDDD[Lcom/android/org/chromium/content/browser/input/DateTimeSuggestion;)V

    .line 44
    return-void
.end method
