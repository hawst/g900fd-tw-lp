.class public Lcom/android/org/chromium/net/DefaultAndroidKeyStore;
.super Ljava/lang/Object;
.source "DefaultAndroidKeyStore.java"

# interfaces
.implements Lcom/android/org/chromium/net/AndroidKeyStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public createKey(Ljava/security/PrivateKey;)Lcom/android/org/chromium/net/AndroidPrivateKey;
    .locals 1
    .param p1, "javaKey"    # Ljava/security/PrivateKey;

    .prologue
    .line 51
    new-instance v0, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    invoke-direct {v0, p1, p0}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;-><init>(Ljava/security/PrivateKey;Lcom/android/org/chromium/net/DefaultAndroidKeyStore;)V

    return-object v0
.end method

.method public getDSAKeyParamQ(Lcom/android/org/chromium/net/AndroidPrivateKey;)[B
    .locals 4
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    .line 66
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v0

    .line 67
    .local v0, "javaKey":Ljava/security/PrivateKey;
    instance-of v2, v0, Ljava/security/interfaces/DSAKey;

    if-eqz v2, :cond_0

    .line 68
    check-cast v0, Ljava/security/interfaces/DSAKey;

    .end local v0    # "javaKey":Ljava/security/PrivateKey;
    invoke-interface {v0}, Ljava/security/interfaces/DSAKey;->getParams()Ljava/security/interfaces/DSAParams;

    move-result-object v1

    .line 69
    .local v1, "params":Ljava/security/interfaces/DSAParams;
    invoke-interface {v1}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    .line 72
    .end local v1    # "params":Ljava/security/interfaces/DSAParams;
    :goto_0
    return-object v2

    .line 71
    .restart local v0    # "javaKey":Ljava/security/PrivateKey;
    :cond_0
    const-string v2, "AndroidKeyStoreInProcessImpl"

    const-string v3, "Not a DSAKey instance!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getECKeyOrder(Lcom/android/org/chromium/net/AndroidPrivateKey;)[B
    .locals 4
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    .line 77
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v0

    .line 78
    .local v0, "javaKey":Ljava/security/PrivateKey;
    instance-of v2, v0, Ljava/security/interfaces/ECKey;

    if-eqz v2, :cond_0

    .line 79
    check-cast v0, Ljava/security/interfaces/ECKey;

    .end local v0    # "javaKey":Ljava/security/PrivateKey;
    invoke-interface {v0}, Ljava/security/interfaces/ECKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v1

    .line 80
    .local v1, "params":Ljava/security/spec/ECParameterSpec;
    invoke-virtual {v1}, Ljava/security/spec/ECParameterSpec;->getOrder()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    .line 83
    .end local v1    # "params":Ljava/security/spec/ECParameterSpec;
    :goto_0
    return-object v2

    .line 82
    .restart local v0    # "javaKey":Ljava/security/PrivateKey;
    :cond_0
    const-string v2, "AndroidKeyStoreInProcessImpl"

    const-string v3, "Not an ECKey instance!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getOpenSSLHandleForPrivateKey(Lcom/android/org/chromium/net/AndroidPrivateKey;)J
    .locals 13
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    const-wide/16 v10, 0x0

    .line 147
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v5

    .line 149
    .local v5, "javaKey":Ljava/security/PrivateKey;
    if-nez v5, :cond_1

    .line 150
    const-string v8, "AndroidKeyStoreInProcessImpl"

    const-string v9, "key == null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 228
    :cond_0
    :goto_0
    return-wide v2

    .line 153
    :cond_1
    instance-of v8, v5, Ljava/security/interfaces/RSAPrivateKey;

    if-nez v8, :cond_2

    .line 154
    const-string v8, "AndroidKeyStoreInProcessImpl"

    const-string v9, "does not implement RSAPrivateKey"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 155
    goto :goto_0

    .line 161
    :cond_2
    :try_start_0
    const-string v8, "org.apache.harmony.xnet.provider.jsse.OpenSSLRSAPrivateKey"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 170
    .local v7, "superClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v7, v5}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 174
    const-string v8, "AndroidKeyStoreInProcessImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Private key is not an OpenSSLRSAPrivateKey instance, its class name is:"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 176
    goto :goto_0

    .line 163
    .end local v7    # "superClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "AndroidKeyStoreInProcessImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Cannot find system OpenSSLRSAPrivateKey class: "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 168
    goto :goto_0

    .line 184
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v7    # "superClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_3
    :try_start_1
    const-string v8, "getOpenSSLKey"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 185
    .local v1, "getKey":Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 186
    const/4 v6, 0x0

    .line 188
    .local v6, "opensslKey":Ljava/lang/Object;
    const/4 v8, 0x0

    :try_start_2
    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v5, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 190
    const/4 v8, 0x0

    :try_start_3
    invoke-virtual {v1, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 192
    if-nez v6, :cond_4

    .line 194
    const-string v8, "AndroidKeyStoreInProcessImpl"

    const-string v9, "getOpenSSLKey() returned null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 195
    goto :goto_0

    .line 190
    :catchall_0
    move-exception v8

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    throw v8
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 226
    .end local v1    # "getKey":Ljava/lang/reflect/Method;
    .end local v6    # "opensslKey":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 227
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "AndroidKeyStoreInProcessImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception while trying to retrieve system EVP_PKEY handle: "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 228
    goto/16 :goto_0

    .line 207
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "getKey":Ljava/lang/reflect/Method;
    .restart local v6    # "opensslKey":Ljava/lang/Object;
    :cond_4
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getPkeyContext"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Class;

    invoke-virtual {v8, v9, v12}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v4

    .line 213
    .local v4, "getPkeyContext":Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    :try_start_5
    invoke-virtual {v4, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 214
    const-wide/16 v2, 0x0

    .line 216
    .local v2, "evp_pkey":J
    const/4 v8, 0x0

    :try_start_6
    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->longValue()J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-wide v2

    .line 218
    const/4 v8, 0x0

    :try_start_7
    invoke-virtual {v4, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 220
    cmp-long v8, v2, v10

    if-nez v8, :cond_0

    .line 222
    const-string v8, "AndroidKeyStoreInProcessImpl"

    const-string v9, "getPkeyContext() returned null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 208
    .end local v2    # "evp_pkey":J
    .end local v4    # "getPkeyContext":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 210
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "AndroidKeyStoreInProcessImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "No getPkeyContext() method on OpenSSLKey member:"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v10

    .line 211
    goto/16 :goto_0

    .line 218
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "evp_pkey":J
    .restart local v4    # "getPkeyContext":Ljava/lang/reflect/Method;
    :catchall_1
    move-exception v8

    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    throw v8
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
.end method

.method public getPrivateKeyEncodedBytes(Lcom/android/org/chromium/net/AndroidPrivateKey;)[B
    .locals 2
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    .line 88
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v0

    .line 89
    .local v0, "javaKey":Ljava/security/PrivateKey;
    invoke-interface {v0}, Ljava/security/PrivateKey;->getEncoded()[B

    move-result-object v1

    return-object v1
.end method

.method public getPrivateKeyType(Lcom/android/org/chromium/net/AndroidPrivateKey;)I
    .locals 2
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    .line 134
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v0

    .line 135
    .local v0, "javaKey":Ljava/security/PrivateKey;
    instance-of v1, v0, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v1, :cond_0

    .line 136
    const/4 v1, 0x0

    .line 142
    :goto_0
    return v1

    .line 137
    :cond_0
    instance-of v1, v0, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v1, :cond_1

    .line 138
    const/4 v1, 0x1

    goto :goto_0

    .line 139
    :cond_1
    instance-of v1, v0, Ljava/security/interfaces/ECPrivateKey;

    if-eqz v1, :cond_2

    .line 140
    const/4 v1, 0x2

    goto :goto_0

    .line 142
    :cond_2
    const/16 v1, 0xff

    goto :goto_0
.end method

.method public getRSAKeyModulus(Lcom/android/org/chromium/net/AndroidPrivateKey;)[B
    .locals 3
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    .line 56
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v0

    .line 57
    .local v0, "javaKey":Ljava/security/PrivateKey;
    instance-of v1, v0, Ljava/security/interfaces/RSAKey;

    if-eqz v1, :cond_0

    .line 58
    check-cast v0, Ljava/security/interfaces/RSAKey;

    .end local v0    # "javaKey":Ljava/security/PrivateKey;
    invoke-interface {v0}, Ljava/security/interfaces/RSAKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    .line 61
    :goto_0
    return-object v1

    .line 60
    .restart local v0    # "javaKey":Ljava/security/PrivateKey;
    :cond_0
    const-string v1, "AndroidKeyStoreInProcessImpl"

    const-string v2, "Not a RSAKey instance!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public rawSignDigestWithPrivateKey(Lcom/android/org/chromium/net/AndroidPrivateKey;[B)[B
    .locals 7
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;
    .param p2, "message"    # [B

    .prologue
    const/4 v3, 0x0

    .line 95
    check-cast p1, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    .end local p1    # "key":Lcom/android/org/chromium/net/AndroidPrivateKey;
    invoke-virtual {p1}, Lcom/android/org/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->getJavaKey()Ljava/security/PrivateKey;

    move-result-object v1

    .line 97
    .local v1, "javaKey":Ljava/security/PrivateKey;
    const/4 v2, 0x0

    .line 101
    .local v2, "signature":Ljava/security/Signature;
    :try_start_0
    instance-of v4, v1, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v4, :cond_1

    .line 105
    const-string v4, "NONEwithRSA"

    invoke-static {v4}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 115
    :cond_0
    :goto_0
    if-nez v2, :cond_3

    .line 116
    const-string v4, "AndroidKeyStoreInProcessImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported private key algorithm: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_1
    return-object v3

    .line 106
    :cond_1
    :try_start_1
    instance-of v4, v1, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v4, :cond_2

    .line 107
    const-string v4, "NONEwithDSA"

    invoke-static {v4}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v2

    goto :goto_0

    .line 108
    :cond_2
    instance-of v4, v1, Ljava/security/interfaces/ECPrivateKey;

    if-eqz v4, :cond_0

    .line 109
    const-string v4, "NONEwithECDSA"

    invoke-static {v4}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    .line 122
    :cond_3
    :try_start_2
    invoke-virtual {v2, v1}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    .line 123
    invoke-virtual {v2, p2}, Ljava/security/Signature;->update([B)V

    .line 124
    invoke-virtual {v2}, Ljava/security/Signature;->sign()[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    goto :goto_1

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "AndroidKeyStoreInProcessImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception while signing message with "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " private key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 111
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public releaseKey(Lcom/android/org/chromium/net/AndroidPrivateKey;)V
    .locals 0
    .param p1, "key"    # Lcom/android/org/chromium/net/AndroidPrivateKey;

    .prologue
    .line 235
    return-void
.end method
