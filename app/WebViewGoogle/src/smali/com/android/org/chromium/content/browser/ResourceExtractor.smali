.class public Lcom/android/org/chromium/content/browser/ResourceExtractor;
.super Ljava/lang/Object;
.source "ResourceExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sExtractImplicitLocalePak:Z

.field private static sInstance:Lcom/android/org/chromium/content/browser/ResourceExtractor;

.field private static sMandatoryPaks:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34
    const-class v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->$assertionsDisabled:Z

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;

    .line 46
    sput-boolean v1, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sExtractImplicitLocalePak:Z

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;

    .line 274
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/ResourceExtractor;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ResourceExtractor;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->getOutputDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/ResourceExtractor;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ResourceExtractor;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->deleteFiles()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/ResourceExtractor;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ResourceExtractor;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sExtractImplicitLocalePak:Z

    return v0
.end method

.method static synthetic access$500(Lcom/android/org/chromium/content/browser/ResourceExtractor;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ResourceExtractor;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->getAppDataDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private deleteFiles()V
    .locals 10

    .prologue
    .line 329
    new-instance v5, Ljava/io/File;

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->getAppDataDir()Ljava/io/File;

    move-result-object v7

    const-string v8, "icudtl.dat"

    invoke-direct {v5, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 330
    .local v5, "icudata":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_0

    .line 331
    const-string v7, "ResourceExtractor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to remove the icudata "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->getOutputDir()Ljava/io/File;

    move-result-object v1

    .line 334
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 335
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 336
    .local v3, "files":[Ljava/io/File;
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v2, v0, v4

    .line 337
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_1

    .line 338
    const-string v7, "ResourceExtractor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to remove existing resource "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 342
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_2
    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/android/org/chromium/content/browser/ResourceExtractor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 239
    sget-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sInstance:Lcom/android/org/chromium/content/browser/ResourceExtractor;

    if-nez v0, :cond_0

    .line 240
    new-instance v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sInstance:Lcom/android/org/chromium/content/browser/ResourceExtractor;

    .line 242
    :cond_0
    sget-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sInstance:Lcom/android/org/chromium/content/browser/ResourceExtractor;

    return-object v0
.end method

.method private getAppDataDir()Ljava/io/File;
    .locals 2

    .prologue
    .line 314
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/org/chromium/base/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getOutputDir()Ljava/io/File;
    .locals 3

    .prologue
    .line 318
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->getAppDataDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "paks"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static varargs setMandatoryPaksToExtract([Ljava/lang/String;)V
    .locals 2
    .param p0, "mandatoryPaks"    # [Ljava/lang/String;

    .prologue
    .line 253
    sget-boolean v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sInstance:Lcom/android/org/chromium/content/browser/ResourceExtractor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sInstance:Lcom/android/org/chromium/content/browser/ResourceExtractor;

    iget-object v0, v0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Must be called before startExtractingResources is called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 254
    :cond_0
    sput-object p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;

    .line 256
    return-void
.end method

.method private static shouldSkipPakExtraction()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 350
    sget-boolean v2, Lcom/android/org/chromium/content/browser/ResourceExtractor;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 351
    :cond_0
    sget-object v2, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;

    array-length v2, v2

    if-ne v2, v0, :cond_1

    const-string v2, ""

    sget-object v3, Lcom/android/org/chromium/content/browser/ResourceExtractor;->sMandatoryPaks:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public startExtractingResources()V
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    if-eqz v0, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    invoke-static {}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->shouldSkipPakExtraction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    new-instance v0, Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;-><init>(Lcom/android/org/chromium/content/browser/ResourceExtractor;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    .line 310
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public waitForCompletion()V
    .locals 4

    .prologue
    .line 277
    invoke-static {}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->shouldSkipPakExtraction()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 293
    :goto_0
    return-void

    .line 281
    :cond_0
    sget-boolean v3, Lcom/android/org/chromium/content/browser/ResourceExtractor;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 284
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ResourceExtractor;->mExtractTask:Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;

    invoke-virtual {v3}, Lcom/android/org/chromium/content/browser/ResourceExtractor$ExtractTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->deleteFiles()V

    goto :goto_0

    .line 288
    .end local v0    # "e":Ljava/util/concurrent/CancellationException;
    :catch_1
    move-exception v1

    .line 289
    .local v1, "e2":Ljava/util/concurrent/ExecutionException;
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->deleteFiles()V

    goto :goto_0

    .line 290
    .end local v1    # "e2":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v2

    .line 291
    .local v2, "e3":Ljava/lang/InterruptedException;
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->deleteFiles()V

    goto :goto_0
.end method
