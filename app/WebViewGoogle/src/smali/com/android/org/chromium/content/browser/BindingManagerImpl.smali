.class Lcom/android/org/chromium/content/browser/BindingManagerImpl;
.super Ljava/lang/Object;
.source "BindingManagerImpl.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/BindingManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mIsLowMemoryDevice:Z

.field private mLastInForeground:Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

.field private final mLastInForegroundLock:Ljava/lang/Object;

.field private final mManagedConnections:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoveInitialBindingDelay:J

.field private final mRemoveStrongBindingDelay:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(ZJJ)V
    .locals 2
    .param p1, "isLowMemoryDevice"    # Z
    .param p2, "removeInitialBindingDelay"    # J
    .param p4, "removeStrongBindingDelay"    # J

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    .line 180
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mLastInForegroundLock:Ljava/lang/Object;

    .line 191
    iput-boolean p1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mIsLowMemoryDevice:Z

    .line 192
    iput-wide p2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mRemoveInitialBindingDelay:J

    .line 193
    iput-wide p4, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mRemoveStrongBindingDelay:J

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mRemoveInitialBindingDelay:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mIsLowMemoryDevice:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mRemoveStrongBindingDelay:J

    return-wide v0
.end method

.method public static createBindingManager()Lcom/android/org/chromium/content/browser/BindingManagerImpl;
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    .line 197
    new-instance v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    invoke-static {}, Lcom/android/org/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v1

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/org/chromium/content/browser/BindingManagerImpl;-><init>(ZJJ)V

    return-object v0
.end method


# virtual methods
.method public addNewConnection(ILcom/android/org/chromium/content/browser/ChildProcessConnection;)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "connection"    # Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .prologue
    .line 212
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mLastInForegroundLock:Ljava/lang/Object;

    monitor-enter v1

    .line 213
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mIsLowMemoryDevice:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mLastInForeground:Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mLastInForeground:Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    # invokes: Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->dropBindings()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->access$300(Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;)V

    .line 214
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    monitor-enter v1

    .line 219
    :try_start_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    new-instance v2, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    invoke-direct {v2, p0, p2}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;-><init>(Lcom/android/org/chromium/content/browser/BindingManagerImpl;Lcom/android/org/chromium/content/browser/ChildProcessConnection;)V

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 220
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 221
    return-void

    .line 214
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 220
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public clearConnection(I)V
    .locals 3
    .param p1, "pid"    # I

    .prologue
    .line 278
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    monitor-enter v2

    .line 279
    :try_start_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    .line 280
    .local v0, "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->clearConnection()V

    .line 282
    :cond_0
    return-void

    .line 280
    .end local v0    # "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public isOomProtected(I)Z
    .locals 3
    .param p1, "pid"    # I

    .prologue
    .line 269
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    monitor-enter v2

    .line 270
    :try_start_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    .line 271
    .local v0, "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->isOomProtected()Z

    move-result v1

    :goto_0
    return v1

    .line 271
    .end local v0    # "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 272
    .restart local v0    # "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setInForeground(IZ)V
    .locals 4
    .param p1, "pid"    # I
    .param p2, "inForeground"    # Z

    .prologue
    .line 226
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    monitor-enter v2

    .line 227
    :try_start_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mManagedConnections:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    .line 228
    .local v0, "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    if-nez v0, :cond_0

    .line 231
    const-string v1, "BindingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot setInForeground() - never saw a connection for the pid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :goto_0
    return-void

    .line 228
    .end local v0    # "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 236
    .restart local v0    # "managedConnection":Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
    :cond_0
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mLastInForegroundLock:Ljava/lang/Object;

    monitor-enter v2

    .line 237
    :try_start_2
    invoke-virtual {v0, p2}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->setInForeground(Z)V

    .line 238
    if-eqz p2, :cond_1

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mLastInForeground:Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    .line 239
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method
