.class Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;
.super Ljava/lang/Object;
.source "ImeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/input/ImeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DelayedDismissInput"
.end annotation


# instance fields
.field private final mNativeImeAdapter:J

.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/ImeAdapter;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/input/ImeAdapter;J)V
    .locals 0
    .param p2, "nativeImeAdapter"    # J

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;->this$0:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-wide p2, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;->mNativeImeAdapter:J

    .line 81
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;->this$0:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;->mNativeImeAdapter:J

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNone:I

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->attach(JI)V

    .line 86
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/ImeAdapter$DelayedDismissInput;->this$0:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/android/org/chromium/content/browser/input/ImeAdapter;->dismissInput(Z)V
    invoke-static {v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->access$000(Lcom/android/org/chromium/content/browser/input/ImeAdapter;Z)V

    .line 87
    return-void
.end method
