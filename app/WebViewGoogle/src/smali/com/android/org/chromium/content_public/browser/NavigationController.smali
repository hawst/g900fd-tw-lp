.class public interface abstract Lcom/android/org/chromium/content_public/browser/NavigationController;
.super Ljava/lang/Object;
.source "NavigationController.java"


# virtual methods
.method public abstract canGoBack()Z
.end method

.method public abstract canGoForward()Z
.end method

.method public abstract canGoToOffset(I)Z
.end method

.method public abstract goBack()V
.end method

.method public abstract goForward()V
.end method

.method public abstract goToOffset(I)V
.end method
