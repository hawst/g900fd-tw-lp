.class Lcom/android/org/chromium/content/app/ChildProcessService$2;
.super Ljava/lang/Object;
.source "ChildProcessService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/app/ChildProcessService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/app/ChildProcessService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    const-class v0, Lcom/android/org/chromium/content/app/ChildProcessService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/org/chromium/content/app/ChildProcessService;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 131
    :try_start_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->isUsed()Z

    move-result v9

    .line 133
    .local v9, "useLinker":Z
    if-eqz v9, :cond_1

    .line 134
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mIsBound:Z
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$700(Lcom/android/org/chromium/content/app/ChildProcessService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 138
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_2 .. :try_end_2} :catch_1

    .line 180
    .end local v9    # "useLinker":Z
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "ChildProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChildProcessMain startup failed: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-void

    .line 138
    .restart local v9    # "useLinker":Z
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :try_start_4
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mLinkerParams:Lcom/android/org/chromium/content/app/ChromiumLinkerParams;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$800(Lcom/android/org/chromium/content/app/ChildProcessService;)Lcom/android/org/chromium/content/app/ChromiumLinkerParams;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mLinkerParams:Lcom/android/org/chromium/content/app/ChromiumLinkerParams;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$800(Lcom/android/org/chromium/content/app/ChildProcessService;)Lcom/android/org/chromium/content/app/ChromiumLinkerParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/org/chromium/content/app/ChromiumLinkerParams;->mWaitForSharedRelro:Z

    if-eqz v1, :cond_2

    .line 141
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mLinkerParams:Lcom/android/org/chromium/content/app/ChromiumLinkerParams;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$800(Lcom/android/org/chromium/content/app/ChildProcessService;)Lcom/android/org/chromium/content/app/ChromiumLinkerParams;

    move-result-object v1

    iget-wide v6, v1, Lcom/android/org/chromium/content/app/ChromiumLinkerParams;->mBaseLoadAddress:J

    invoke-static {v6, v7}, Lcom/android/org/chromium/base/library_loader/Linker;->initServiceProcess(J)V

    .line 145
    :goto_2
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mLinkerParams:Lcom/android/org/chromium/content/app/ChromiumLinkerParams;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$800(Lcom/android/org/chromium/content/app/ChildProcessService;)Lcom/android/org/chromium/content/app/ChromiumLinkerParams;

    move-result-object v1

    iget-object v1, v1, Lcom/android/org/chromium/content/app/ChromiumLinkerParams;->mTestRunnerClassName:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/org/chromium/base/library_loader/Linker;->setTestRunnerClassName(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_4 .. :try_end_4} :catch_1

    .line 149
    :cond_1
    :try_start_5
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->loadNow(Landroid/content/Context;Z)V
    :try_end_5
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 154
    :goto_3
    :try_start_6
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v2

    monitor-enter v2
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_6 .. :try_end_6} :catch_1

    .line 155
    :goto_4
    :try_start_7
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$200(Lcom/android/org/chromium/content/app/ChildProcessService;)[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 156
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    goto :goto_4

    .line 158
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v1
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_8 .. :try_end_8} :catch_1

    .line 182
    .end local v9    # "useLinker":Z
    :catch_1
    move-exception v0

    .line 183
    .local v0, "e":Lcom/android/org/chromium/base/library_loader/ProcessInitException;
    const-string v1, "ChildProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChildProcessMain startup failed: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 143
    .end local v0    # "e":Lcom/android/org/chromium/base/library_loader/ProcessInitException;
    .restart local v9    # "useLinker":Z
    :cond_2
    :try_start_9
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->disableSharedRelros()V

    goto :goto_2

    .line 150
    :catch_2
    move-exception v0

    .line 151
    .restart local v0    # "e":Lcom/android/org/chromium/base/library_loader/ProcessInitException;
    const-string v1, "ChildProcessService"

    const-string v2, "Failed to load native library, exiting child process"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_3

    .line 158
    .end local v0    # "e":Lcom/android/org/chromium/base/library_loader/ProcessInitException;
    :cond_3
    :try_start_a
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 159
    :try_start_b
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$200(Lcom/android/org/chromium/content/app/ChildProcessService;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->initialize([Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v2

    monitor-enter v2
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_b .. :try_end_b} :catch_1

    .line 161
    :try_start_c
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    const/4 v5, 0x1

    # setter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mLibraryInitialized:Z
    invoke-static {v1, v5}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$902(Lcom/android/org/chromium/content/app/ChildProcessService;Z)Z

    .line 162
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 163
    :goto_5
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$500(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_4

    .line 164
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mMainThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$100(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    goto :goto_5

    .line 166
    :catchall_2
    move-exception v1

    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    throw v1
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_d .. :try_end_d} :catch_1

    :cond_4
    :try_start_e
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 167
    :try_start_f
    sget-boolean v1, Lcom/android/org/chromium/content/app/ChildProcessService$2;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$500(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$600(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 168
    :cond_5
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$500(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v3, v1, [I

    .line 169
    .local v3, "fileIds":[I
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$600(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v4, v1, [I

    .line 170
    .local v4, "fileFds":[I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_6
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$500(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_6

    .line 171
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$500(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v3, v8

    .line 172
    iget-object v1, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$600(Lcom/android/org/chromium/content/app/ChildProcessService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    move-result v1

    aput v1, v4, v8

    .line 170
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 174
    :cond_6
    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->sContext:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$1000()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/org/chromium/content/app/ContentMain;->initApplicationContext(Landroid/content/Context;)V

    .line 175
    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->sContext:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$1000()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    iget-object v5, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mCpuCount:I
    invoke-static {v5}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$300(Lcom/android/org/chromium/content/app/ChildProcessService;)I

    move-result v5

    iget-object v6, p0, Lcom/android/org/chromium/content/app/ChildProcessService$2;->this$0:Lcom/android/org/chromium/content/app/ChildProcessService;

    # getter for: Lcom/android/org/chromium/content/app/ChildProcessService;->mCpuFeatures:J
    invoke-static {v6}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$400(Lcom/android/org/chromium/content/app/ChildProcessService;)J

    move-result-wide v6

    # invokes: Lcom/android/org/chromium/content/app/ChildProcessService;->nativeInitChildProcess(Landroid/content/Context;Lcom/android/org/chromium/content/app/ChildProcessService;[I[IIJ)V
    invoke-static/range {v1 .. v7}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$1100(Landroid/content/Context;Lcom/android/org/chromium/content/app/ChildProcessService;[I[IIJ)V

    .line 178
    invoke-static {}, Lcom/android/org/chromium/content/app/ContentMain;->start()I

    .line 179
    # invokes: Lcom/android/org/chromium/content/app/ChildProcessService;->nativeExitChildProcess()V
    invoke-static {}, Lcom/android/org/chromium/content/app/ChildProcessService;->access$1200()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_1
.end method
