.class Lcom/android/org/chromium/ui/VSyncMonitor$1;
.super Ljava/lang/Object;
.source "VSyncMonitor.java"

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/ui/VSyncMonitor;-><init>(Landroid/content/Context;Lcom/android/org/chromium/ui/VSyncMonitor$Listener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/ui/VSyncMonitor;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/ui/VSyncMonitor;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/org/chromium/ui/VSyncMonitor$1;->this$0:Lcom/android/org/chromium/ui/VSyncMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doFrame(J)V
    .locals 5
    .param p1, "frameTimeNanos"    # J

    .prologue
    .line 95
    const-string v0, "VSync"

    invoke-static {v0}, Lcom/android/org/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor$1;->this$0:Lcom/android/org/chromium/ui/VSyncMonitor;

    # setter for: Lcom/android/org/chromium/ui/VSyncMonitor;->mGoodStartingPointNano:J
    invoke-static {v0, p1, p2}, Lcom/android/org/chromium/ui/VSyncMonitor;->access$002(Lcom/android/org/chromium/ui/VSyncMonitor;J)J

    .line 97
    iget-object v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor$1;->this$0:Lcom/android/org/chromium/ui/VSyncMonitor;

    iget-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor$1;->this$0:Lcom/android/org/chromium/ui/VSyncMonitor;

    # invokes: Lcom/android/org/chromium/ui/VSyncMonitor;->getCurrentNanoTime()J
    invoke-static {v1}, Lcom/android/org/chromium/ui/VSyncMonitor;->access$100(Lcom/android/org/chromium/ui/VSyncMonitor;)J

    move-result-wide v2

    # invokes: Lcom/android/org/chromium/ui/VSyncMonitor;->onVSyncCallback(JJ)V
    invoke-static {v0, p1, p2, v2, v3}, Lcom/android/org/chromium/ui/VSyncMonitor;->access$200(Lcom/android/org/chromium/ui/VSyncMonitor;JJ)V

    .line 98
    const-string v0, "VSync"

    invoke-static {v0}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 99
    return-void
.end method
