.class Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;
.super Ljava/lang/Object;
.source "AwContentViewClient.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/ContentVideoViewClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/android_webview/AwContentViewClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AwContentVideoViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;


# direct methods
.method private constructor <init>(Lcom/android/org/chromium/android_webview/AwContentViewClient;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/org/chromium/android_webview/AwContentViewClient;Lcom/android/org/chromium/android_webview/AwContentViewClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/org/chromium/android_webview/AwContentViewClient;
    .param p2, "x1"    # Lcom/android/org/chromium/android_webview/AwContentViewClient$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;-><init>(Lcom/android/org/chromium/android_webview/AwContentViewClient;)V

    return-void
.end method

.method private onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cb"    # Landroid/webkit/WebChromeClient$CustomViewCallback;

    .prologue
    .line 50
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$200(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    .local v0, "viewGroup":Landroid/widget/FrameLayout;
    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 52
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;

    invoke-direct {v1, p0, v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;-><init>(Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;Landroid/widget/FrameLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 66
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$100(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContentsClient;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 67
    return-void
.end method

.method private onShowCustomViewLegacy(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cb"    # Landroid/webkit/WebChromeClient$CustomViewCallback;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$100(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContentsClient;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 47
    return-void
.end method


# virtual methods
.method public getVideoLoadingProgressView()Landroid/view/View;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$100(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContentsClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->getVideoLoadingProgressView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyContentVideoView()V
    .locals 1

    .prologue
    .line 71
    # invokes: Lcom/android/org/chromium/android_webview/AwContentViewClient;->areHtmlControlsEnabled()Z
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$300(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->exitFullScreen()V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$100(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContentsClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onHideCustomView()V

    .line 75
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 28
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$1;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$1;-><init>(Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;)V

    .line 37
    .local v0, "cb":Landroid/webkit/WebChromeClient$CustomViewCallback;
    # invokes: Lcom/android/org/chromium/android_webview/AwContentViewClient;->areHtmlControlsEnabled()Z
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$000()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    invoke-direct {p0, p1, v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->onShowCustomViewLegacy(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 42
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 40
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    goto :goto_0
.end method
