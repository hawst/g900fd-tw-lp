.class Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;
.super Ljava/lang/Object;
.source "AwContentViewClient.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;

.field final synthetic val$viewGroup:Landroid/widget/FrameLayout;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;Landroid/widget/FrameLayout;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;->this$1:Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;

    iput-object p2, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;->val$viewGroup:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;->this$1:Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;

    iget-object v0, v0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$300(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;->val$viewGroup:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient$2;->this$1:Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;

    iget-object v1, v1, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;->this$0:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    # getter for: Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->access$300(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->enterFullScreen()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    return-void
.end method
