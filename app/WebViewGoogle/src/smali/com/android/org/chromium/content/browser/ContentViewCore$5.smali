.class Lcom/android/org/chromium/content/browser/ContentViewCore$5;
.super Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;
.source "ContentViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLcom/android/org/chromium/ui/base/WindowAndroid;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p2, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 649
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, p2}, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    return-void
.end method


# virtual methods
.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "baseUrl"    # Ljava/lang/String;
    .param p3, "isNavigationToDifferentPage"    # Z
    .param p4, "isFragmentNavigation"    # Z

    .prologue
    .line 653
    if-nez p3, :cond_0

    .line 657
    :goto_0
    return-void

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->hidePopups()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1000(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 655
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->resetScrollInProgress()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1100(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 656
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->resetGestureDetection()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1200(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public renderProcessGone(Z)V
    .locals 1
    .param p1, "wasOomProtected"    # Z

    .prologue
    .line 661
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->hidePopups()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1000(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 662
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->resetScrollInProgress()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1100(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 665
    return-void
.end method
