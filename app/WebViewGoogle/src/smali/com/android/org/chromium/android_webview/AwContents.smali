.class public Lcom/android/org/chromium/android_webview/AwContents;
.super Ljava/lang/Object;
.source "AwContents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;,
        Lcom/android/org/chromium/android_webview/AwContents$AwGeolocationCallback;,
        Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;,
        Lcom/android/org/chromium/android_webview/AwContents$AwComponentCallbacks;,
        Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;,
        Lcom/android/org/chromium/android_webview/AwContents$AwScrollOffsetManagerDelegate;,
        Lcom/android/org/chromium/android_webview/AwContents$AwLayoutSizerDelegate;,
        Lcom/android/org/chromium/android_webview/AwContents$InterceptNavigationDelegateImpl;,
        Lcom/android/org/chromium/android_webview/AwContents$IoThreadClientImpl;,
        Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;,
        Lcom/android/org/chromium/android_webview/AwContents$DestroyRunnable;,
        Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;,
        Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;,
        Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;,
        Lcom/android/org/chromium/android_webview/AwContents$HitTestData;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final SUPPORTS_ON_ANIMATION:Z

.field private static final sLocalGlobalVisibleRect:Landroid/graphics/Rect;


# instance fields
.field private mAwAutofillClient:Lcom/android/org/chromium/android_webview/AwAutofillClient;

.field private mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

.field private mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

.field private mBaseBackgroundColor:I

.field private final mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

.field private mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

.field private mContainerView:Landroid/view/ViewGroup;

.field private mContainerViewFocused:Z

.field private mContentHeightDip:F

.field private final mContentViewClient:Lcom/android/org/chromium/android_webview/AwContentViewClient;

.field private mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

.field private mContentWidthDip:F

.field private final mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

.field private final mContentsClientBridge:Lcom/android/org/chromium/android_webview/AwContentsClientBridge;

.field private final mContext:Landroid/content/Context;

.field private final mDIPScale:D

.field private final mDefaultVideoPosterRequestHandler:Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;

.field private mDeferredShouldOverrideUrlLoadingIsPendingForPopup:Z

.field private mFavicon:Landroid/graphics/Bitmap;

.field private final mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

.field private mHasRequestedVisitedHistoryFromClient:Z

.field private final mInterceptNavigationDelegate:Lcom/android/org/chromium/android_webview/AwContents$InterceptNavigationDelegateImpl;

.field private mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

.field private final mIoThreadClient:Lcom/android/org/chromium/android_webview/AwContentsIoThreadClient;

.field private mIsAttachedToWindow:Z

.field private mIsPaused:Z

.field private mIsViewVisible:Z

.field private mIsWindowVisible:Z

.field private final mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

.field private final mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

.field private mMaxPageScaleFactor:F

.field private mMinPageScaleFactor:F

.field private mNativeAwContents:J

.field private final mNativeGLDelegate:Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;

.field private mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

.field private mOverlayHorizontalScrollbar:Z

.field private mOverlayVerticalScrollbar:Z

.field private mPageScaleFactor:F

.field private mPictureListenerContentProvider:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Landroid/graphics/Picture;",
            ">;"
        }
    .end annotation
.end field

.field private final mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

.field private final mScrollAccessibilityHelper:Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;

.field private final mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

.field private final mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

.field private mTemporarilyDetached:Z

.field private final mWebContentsDelegate:Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;

.field private mWindowFocused:Z

.field private final mZoomControls:Lcom/android/org/chromium/android_webview/AwZoomControls;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    const-class v0, Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    .line 936
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/org/chromium/android_webview/AwContents;->sLocalGlobalVisibleRect:Landroid/graphics/Rect;

    .line 2025
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/android/org/chromium/android_webview/AwContents;->SUPPORTS_ON_ANIMATION:Z

    return-void

    :cond_0
    move v0, v2

    .line 81
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2025
    goto :goto_1
.end method

.method public constructor <init>(Lcom/android/org/chromium/android_webview/AwBrowserContext;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/android_webview/AwSettings;)V
    .locals 9
    .param p1, "browserContext"    # Lcom/android/org/chromium/android_webview/AwBrowserContext;
    .param p2, "containerView"    # Landroid/view/ViewGroup;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "internalAccessAdapter"    # Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .param p5, "nativeGLDelegate"    # Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;
    .param p6, "contentsClient"    # Lcom/android/org/chromium/android_webview/AwContentsClient;
    .param p7, "awSettings"    # Lcom/android/org/chromium/android_webview/AwSettings;

    .prologue
    .line 546
    new-instance v8, Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;

    invoke-direct {v8}, Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/org/chromium/android_webview/AwContents;-><init>(Lcom/android/org/chromium/android_webview/AwBrowserContext;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/android_webview/AwSettings;Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;)V

    .line 548
    return-void
.end method

.method public constructor <init>(Lcom/android/org/chromium/android_webview/AwBrowserContext;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/android_webview/AwSettings;Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;)V
    .locals 6
    .param p1, "browserContext"    # Lcom/android/org/chromium/android_webview/AwBrowserContext;
    .param p2, "containerView"    # Landroid/view/ViewGroup;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "internalAccessAdapter"    # Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .param p5, "nativeGLDelegate"    # Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;
    .param p6, "contentsClient"    # Lcom/android/org/chromium/android_webview/AwContentsClient;
    .param p7, "settings"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p8, "dependencyFactory"    # Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBaseBackgroundColor:I

    .line 214
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    invoke-direct {v1}, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;-><init>()V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    .line 227
    iput v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    .line 228
    iput v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMinPageScaleFactor:F

    .line 229
    iput v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMaxPageScaleFactor:F

    .line 1178
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayHorizontalScrollbar:Z

    .line 1179
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayVerticalScrollbar:Z

    .line 561
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    .line 562
    iput-object p2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    .line 563
    iput-object p3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    .line 564
    iput-object p4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    .line 565
    iput-object p5, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeGLDelegate:Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;

    .line 566
    iput-object p6, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    .line 567
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;

    invoke-direct {v1, p0, v5}, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    .line 568
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;-><init>(Landroid/view/ViewGroup;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwViewMethods;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .line 570
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContentViewClient;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    invoke-direct {v1, p6, p7, p0, v2}, Lcom/android/org/chromium/android_webview/AwContentViewClient;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/android_webview/AwSettings;Lcom/android/org/chromium/android_webview/AwContents;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewClient:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    .line 571
    invoke-virtual {p8}, Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;->createLayoutSizer()Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    .line 572
    iput-object p7, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    .line 573
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/org/chromium/ui/gfx/DeviceDisplayInfo;->create(Landroid/content/Context;)Lcom/android/org/chromium/ui/gfx/DeviceDisplayInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/org/chromium/ui/gfx/DeviceDisplayInfo;->getDIPScale()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    .line 574
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwContents$AwLayoutSizerDelegate;

    invoke-direct {v2, p0, v5}, Lcom/android/org/chromium/android_webview/AwContents$AwLayoutSizerDelegate;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    invoke-virtual {v1, v2}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->setDelegate(Lcom/android/org/chromium/android_webview/AwLayoutSizer$Delegate;)V

    .line 575
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    invoke-virtual {v1, v2, v3}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->setDIPScale(D)V

    .line 576
    new-instance v1, Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    invoke-direct {v1, p6, v2, v3}, Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClient;Landroid/view/View;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mWebContentsDelegate:Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;

    .line 578
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContentsClientBridge;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getKeyStore()Lcom/android/org/chromium/net/DefaultAndroidKeyStore;

    move-result-object v2

    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContentsStatics;->getClientCertLookupTable()Lcom/android/org/chromium/android_webview/ClientCertLookupTable;

    move-result-object v3

    invoke-direct {v1, p6, v2, v3}, Lcom/android/org/chromium/android_webview/AwContentsClientBridge;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/net/DefaultAndroidKeyStore;Lcom/android/org/chromium/android_webview/ClientCertLookupTable;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClientBridge:Lcom/android/org/chromium/android_webview/AwContentsClientBridge;

    .line 580
    new-instance v1, Lcom/android/org/chromium/android_webview/AwZoomControls;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/android_webview/AwZoomControls;-><init>(Lcom/android/org/chromium/android_webview/AwContents;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mZoomControls:Lcom/android/org/chromium/android_webview/AwZoomControls;

    .line 581
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$IoThreadClientImpl;

    invoke-direct {v1, p0, v5}, Lcom/android/org/chromium/android_webview/AwContents$IoThreadClientImpl;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIoThreadClient:Lcom/android/org/chromium/android_webview/AwContentsIoThreadClient;

    .line 582
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$InterceptNavigationDelegateImpl;

    invoke-direct {v1, p0, v5}, Lcom/android/org/chromium/android_webview/AwContents$InterceptNavigationDelegateImpl;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInterceptNavigationDelegate:Lcom/android/org/chromium/android_webview/AwContents$InterceptNavigationDelegateImpl;

    .line 584
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$1;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/android_webview/AwContents$1;-><init>(Lcom/android/org/chromium/android_webview/AwContents;)V

    .line 594
    .local v0, "zoomListener":Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/android_webview/AwSettings;->setZoomListener(Lcom/android/org/chromium/android_webview/AwSettings$ZoomSupportChangeListener;)V

    .line 595
    new-instance v1, Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-direct {v1, v2}, Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClient;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDefaultVideoPosterRequestHandler:Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;

    .line 596
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDefaultVideoPosterRequestHandler:Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;->getDefaultVideoPosterURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/org/chromium/android_webview/AwSettings;->setDefaultVideoPosterURL(Ljava/lang/String;)V

    .line 598
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    invoke-virtual {v1, v2, v3}, Lcom/android/org/chromium/android_webview/AwSettings;->setDIPScale(D)V

    .line 599
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$AwScrollOffsetManagerDelegate;

    invoke-direct {v1, p0, v5}, Lcom/android/org/chromium/android_webview/AwContents$AwScrollOffsetManagerDelegate;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    new-instance v2, Landroid/widget/OverScroller;

    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {p8, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents$DependencyFactory;->createScrollOffsetManager(Lcom/android/org/chromium/android_webview/AwScrollOffsetManager$Delegate;Landroid/widget/OverScroller;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    .line 601
    new-instance v1, Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-direct {v1, v2}, Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollAccessibilityHelper:Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;

    .line 603
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getOverScrollMode()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->setOverScrollMode(I)V

    .line 604
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;->super_getScrollBarStyle()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->setScrollBarStyle(I)V

    .line 605
    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    invoke-direct {v1, p0, v5}, Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    .line 606
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 608
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeInit(Lcom/android/org/chromium/android_webview/AwBrowserContext;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->setNewAwContents(J)V

    .line 610
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onContainerViewChanged()V

    .line 611
    return-void
.end method

.method static synthetic access$000(J)V
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeDestroy(J)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwSettings;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollAccessibilityHelper:Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents;->getGlobalVisibleRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/org/chromium/android_webview/AwContents;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsViewVisible:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/android/org/chromium/android_webview/AwContents;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsWindowVisible:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/android/org/chromium/android_webview/AwContents;JIZ)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # Z

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->nativeTrimMemory(JIZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDefaultVideoPosterRequestHandler:Lcom/android/org/chromium/android_webview/DefaultVideoPosterRequestHandler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwContentsClient;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/org/chromium/android_webview/AwContents;J[Ljava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # [Ljava/lang/String;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContents;->nativeAddVisitedLinks(J[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3400(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->generateArchiveAutoNamePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/org/chromium/android_webview/AwContents;Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/webkit/ValueCallback;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->saveWebArchiveInternal(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwBrowserContext;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/org/chromium/android_webview/AwContents;JZLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z
    .param p4, "x3"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->nativeInvokeGeolocationCallback(JZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/android/org/chromium/android_webview/AwContents;JLandroid/graphics/Canvas;ZIIIIII)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # Landroid/graphics/Canvas;
    .param p4, "x3"    # Z
    .param p5, "x4"    # I
    .param p6, "x5"    # I
    .param p7, "x6"    # I
    .param p8, "x7"    # I
    .param p9, "x8"    # I
    .param p10, "x9"    # I

    .prologue
    .line 82
    invoke-direct/range {p0 .. p10}, Lcom/android/org/chromium/android_webview/AwContents;->nativeOnDraw(JLandroid/graphics/Canvas;ZIIIIII)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/org/chromium/android_webview/AwContents;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDeferredShouldOverrideUrlLoadingIsPendingForPopup:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/org/chromium/android_webview/AwContents;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDeferredShouldOverrideUrlLoadingIsPendingForPopup:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/android/org/chromium/android_webview/AwContents;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeFocusFirstNode(J)V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/org/chromium/android_webview/AwContents;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z

    return v0
.end method

.method static synthetic access$4202(Lcom/android/org/chromium/android_webview/AwContents;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z

    return p1
.end method

.method static synthetic access$4300(Lcom/android/org/chromium/android_webview/AwContents;)D
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    return-wide v0
.end method

.method static synthetic access$4400(Lcom/android/org/chromium/android_webview/AwContents;JII)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # I

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->nativeRequestNewHitTestDataAt(JII)V

    return-void
.end method

.method static synthetic access$4500(Lcom/android/org/chromium/android_webview/AwContents;JII)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # I

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->nativeOnAttachedToWindow(JII)V

    return-void
.end method

.method static synthetic access$4700(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4800(J)V
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeOnDetachedFromWindow(J)V

    return-void
.end method

.method static synthetic access$4900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeGLDelegate:Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/android/org/chromium/android_webview/AwContents;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mWindowFocused:Z

    return p1
.end method

.method static synthetic access$5102(Lcom/android/org/chromium/android_webview/AwContents;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerViewFocused:Z

    return p1
.end method

.method static synthetic access$5200(Lcom/android/org/chromium/android_webview/AwContents;JIIII)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # I
    .param p6, "x5"    # I

    .prologue
    .line 82
    invoke-direct/range {p0 .. p6}, Lcom/android/org/chromium/android_webview/AwContents;->nativeOnSizeChanged(JIIII)V

    return-void
.end method

.method static synthetic access$5300(Lcom/android/org/chromium/android_webview/AwContents;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setViewVisibilityInternal(Z)V

    return-void
.end method

.method static synthetic access$5400(Lcom/android/org/chromium/android_webview/AwContents;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setWindowVisibilityInternal(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/org/chromium/android_webview/AwContents;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/android/org/chromium/android_webview/AwContents;JII)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # I

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->nativeScrollTo(JII)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwLayoutSizer;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    return-object v0
.end method

.method private static createAndInitializeContentViewCore(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;JLcom/android/org/chromium/content_public/browser/GestureStateListener;Lcom/android/org/chromium/content/browser/ContentViewClient;Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;)Lcom/android/org/chromium/content/browser/ContentViewCore;
    .locals 7
    .param p0, "containerView"    # Landroid/view/ViewGroup;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "internalDispatcher"    # Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .param p3, "nativeWebContents"    # J
    .param p5, "gestureStateListener"    # Lcom/android/org/chromium/content_public/browser/GestureStateListener;
    .param p6, "contentViewClient"    # Lcom/android/org/chromium/content/browser/ContentViewClient;
    .param p7, "zoomControlsDelegate"    # Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    .prologue
    .line 618
    new-instance v1, Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;-><init>(Landroid/content/Context;)V

    .line 619
    .local v1, "contentViewCore":Lcom/android/org/chromium/content/browser/ContentViewCore;
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    new-instance v6, Lcom/android/org/chromium/ui/base/ActivityWindowAndroid;

    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-direct {v6, p1}, Lcom/android/org/chromium/ui/base/ActivityWindowAndroid;-><init>(Landroid/app/Activity;)V

    :goto_0
    move-object v2, p0

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLcom/android/org/chromium/ui/base/WindowAndroid;)V

    .line 623
    invoke-virtual {v1, p5}, Lcom/android/org/chromium/content/browser/ContentViewCore;->addGestureStateListener(Lcom/android/org/chromium/content_public/browser/GestureStateListener;)V

    .line 624
    invoke-virtual {v1, p6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setContentViewClient(Lcom/android/org/chromium/content/browser/ContentViewClient;)V

    .line 625
    invoke-virtual {v1, p7}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setZoomControlsDelegate(Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;)V

    .line 626
    return-object v1

    .line 619
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v6, Lcom/android/org/chromium/ui/base/WindowAndroid;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/android/org/chromium/ui/base/WindowAndroid;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private didOverscroll(II)V
    .locals 1
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I

    .prologue
    .line 2092
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    if-eqz v0, :cond_0

    .line 2093
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->setOverScrollDeltas(II)V

    .line 2096
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->overScrollBy(II)V

    .line 2098
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2099
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 2101
    :cond_1
    return-void
.end method

.method private static generateArchiveAutoNamePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "originalUrl"    # Ljava/lang/String;
    .param p1, "baseName"    # Ljava/lang/String;

    .prologue
    .line 2143
    const/4 v2, 0x0

    .line 2144
    .local v2, "name":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2146
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 2147
    .local v3, "path":Ljava/lang/String;
    const/16 v5, 0x2f

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 2148
    .local v1, "lastSlash":I
    if-lez v1, :cond_2

    .line 2149
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2158
    .end local v1    # "lastSlash":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v2, "index"

    .line 2160
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".mht"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2161
    .local v4, "testName":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    move-object v5, v4

    .line 2169
    :goto_1
    return-object v5

    .line 2151
    .end local v4    # "testName":Ljava/lang/String;
    .restart local v1    # "lastSlash":I
    .restart local v3    # "path":Ljava/lang/String;
    :cond_2
    move-object v2, v3

    goto :goto_0

    .line 2163
    .end local v1    # "lastSlash":I
    .end local v3    # "path":Ljava/lang/String;
    .restart local v4    # "testName":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_2
    const/16 v5, 0x64

    if-ge v0, v5, :cond_5

    .line 2164
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".mht"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2165
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    move-object v5, v4

    goto :goto_1

    .line 2163
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2168
    :cond_5
    const-string v5, "AwContents"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to auto generate archive name for path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2169
    const/4 v5, 0x0

    goto :goto_1

    .line 2153
    .end local v0    # "i":I
    .end local v4    # "testName":Ljava/lang/String;
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private static generateMHTMLCallback(Ljava/lang/String;JLandroid/webkit/ValueCallback;)V
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "size"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931
    .local p3, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    if-nez p3, :cond_0

    .line 1933
    .end local p0    # "path":Ljava/lang/String;
    :goto_0
    return-void

    .line 1932
    .restart local p0    # "path":Ljava/lang/String;
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    const/4 p0, 0x0

    .end local p0    # "path":Ljava/lang/String;
    :cond_1
    invoke-interface {p3, p0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static getAwDrawGLFunction()J
    .locals 2

    .prologue
    .line 898
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContents;->nativeGetAwDrawGLFunction()J

    move-result-wide v0

    return-wide v0
.end method

.method private getGlobalVisibleRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 939
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    sget-object v1, Lcom/android/org/chromium/android_webview/AwContents;->sLocalGlobalVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 940
    sget-object v0, Lcom/android/org/chromium/android_webview/AwContents;->sLocalGlobalVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 942
    :cond_0
    sget-object v0, Lcom/android/org/chromium/android_webview/AwContents;->sLocalGlobalVisibleRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method private getLocationOnScreen()[I
    .locals 2

    .prologue
    .line 2046
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 2047
    .local v0, "result":[I
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 2048
    return-object v0
.end method

.method private invalidateOnFunctorDestroy()V
    .locals 1

    .prologue
    .line 2041
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 2042
    return-void
.end method

.method private isFlingActive()Z
    .locals 1

    .prologue
    .line 2070
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->isFlingActive()Z

    move-result v0

    return v0
.end method

.method private native nativeAddVisitedLinks(J[Ljava/lang/String;)V
.end method

.method private native nativeCapturePicture(JII)J
.end method

.method private native nativeClearCache(JZ)V
.end method

.method private native nativeClearMatches(J)V
.end method

.method private native nativeClearView(J)V
.end method

.method private native nativeCreatePdfExporter(JLcom/android/org/chromium/android_webview/AwPdfExporter;)V
.end method

.method private static native nativeDestroy(J)V
.end method

.method private native nativeDocumentHasImages(JLandroid/os/Message;)V
.end method

.method private native nativeEnableOnNewPicture(JZ)V
.end method

.method private native nativeFindAllAsync(JLjava/lang/String;)V
.end method

.method private native nativeFindNext(JZ)V
.end method

.method private native nativeFocusFirstNode(J)V
.end method

.method private native nativeGenerateMHTML(JLjava/lang/String;Landroid/webkit/ValueCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method private static native nativeGetAwDrawGLFunction()J
.end method

.method private native nativeGetAwDrawGLViewContext(J)J
.end method

.method private native nativeGetCertificate(J)[B
.end method

.method private static native nativeGetNativeInstanceCount()I
.end method

.method private native nativeGetOpaqueState(J)[B
.end method

.method private native nativeGetWebContents(J)J
.end method

.method private static native nativeInit(Lcom/android/org/chromium/android_webview/AwBrowserContext;)J
.end method

.method private native nativeInvokeGeolocationCallback(JZLjava/lang/String;)V
.end method

.method private native nativeOnAttachedToWindow(JII)V
.end method

.method private static native nativeOnDetachedFromWindow(J)V
.end method

.method private native nativeOnDraw(JLandroid/graphics/Canvas;ZIIIIII)Z
.end method

.method private native nativeOnSizeChanged(JIIII)V
.end method

.method private native nativePreauthorizePermission(JLjava/lang/String;J)V
.end method

.method private native nativeReleasePopupAwContents(J)J
.end method

.method private native nativeRequestNewHitTestDataAt(JII)V
.end method

.method private native nativeRestoreFromOpaqueState(J[B)Z
.end method

.method private native nativeScrollTo(JII)V
.end method

.method private static native nativeSetAwDrawGLFunctionTable(J)V
.end method

.method private static native nativeSetAwDrawSWFunctionTable(J)V
.end method

.method private native nativeSetBackgroundColor(JI)V
.end method

.method private native nativeSetDipScale(JF)V
.end method

.method private native nativeSetExtraHeadersForUrl(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSetIsPaused(JZ)V
.end method

.method private native nativeSetJavaPeers(JLcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwWebContentsDelegate;Lcom/android/org/chromium/android_webview/AwContentsClientBridge;Lcom/android/org/chromium/android_webview/AwContentsIoThreadClient;Lcom/android/org/chromium/components/navigation_interception/InterceptNavigationDelegate;)V
.end method

.method private native nativeSetJsOnlineProperty(JZ)V
.end method

.method private static native nativeSetShouldDownloadFavicons()V
.end method

.method private native nativeSetViewVisibility(JZ)V
.end method

.method private native nativeSetWindowVisibility(JZ)V
.end method

.method private native nativeTrimMemory(JIZ)V
.end method

.method private native nativeUpdateLastHitTestData(J)V
.end method

.method private onContainerViewChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 719
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialAwViewMethods()Lcom/android/org/chromium/android_webview/AwViewMethods;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2600(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/AwViewMethods;

    move-result-object v0

    .line 720
    .local v0, "awViewMethodsImpl":Lcom/android/org/chromium/android_webview/AwViewMethods;
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onVisibilityChanged(Landroid/view/View;I)V

    .line 721
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWindowVisibility()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onWindowVisibilityChanged(I)V

    .line 722
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->isAttachedToWindow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 723
    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onAttachedToWindow()V

    .line 727
    :goto_0
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onSizeChanged(IIII)V

    .line 729
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->hasWindowFocus()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onWindowFocusChanged(Z)V

    .line 730
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 731
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 732
    return-void

    .line 725
    :cond_0
    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method private static onDocumentHasImagesResponse(ZLandroid/os/Message;)V
    .locals 1
    .param p0, "result"    # Z
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1912
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 1913
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 1914
    return-void

    .line 1912
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onGeolocationPermissionsHidePrompt()V
    .locals 1

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onGeolocationPermissionsHidePrompt()V

    .line 1983
    return-void
.end method

.method private onGeolocationPermissionsShowPrompt(Ljava/lang/String;)V
    .locals 6
    .param p1, "origin"    # Ljava/lang/String;

    .prologue
    .line 1963
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1978
    :goto_0
    return-void

    .line 1964
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getGeolocationPermissions()Lcom/android/org/chromium/android_webview/AwGeolocationPermissions;

    move-result-object v0

    .line 1966
    .local v0, "permissions":Lcom/android/org/chromium/android_webview/AwGeolocationPermissions;
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwSettings;->getGeolocationEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1967
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const/4 v1, 0x0

    invoke-direct {p0, v2, v3, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeInvokeGeolocationCallback(JZLjava/lang/String;)V

    goto :goto_0

    .line 1971
    :cond_1
    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwGeolocationPermissions;->hasOrigin(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1972
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwGeolocationPermissions;->isOriginAllowed(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0, v2, v3, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeInvokeGeolocationCallback(JZLjava/lang/String;)V

    goto :goto_0

    .line 1976
    :cond_2
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwContents$AwGeolocationCallback;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/org/chromium/android_webview/AwContents$AwGeolocationCallback;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    invoke-virtual {v1, p1, v2}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V

    goto :goto_0
.end method

.method private onPermissionRequest(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V
    .locals 1
    .param p1, "awPermissionRequest"    # Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onPermissionRequest(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V

    .line 1988
    return-void
.end method

.method private onPermissionRequestCanceled(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V
    .locals 1
    .param p1, "awPermissionRequest"    # Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;

    .prologue
    .line 1992
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onPermissionRequestCanceled(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V

    .line 1993
    return-void
.end method

.method private onReceivedHttpAuthRequest(Lcom/android/org/chromium/android_webview/AwHttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "handler"    # Lcom/android/org/chromium/android_webview/AwHttpAuthHandler;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "realm"    # Ljava/lang/String;

    .prologue
    .line 1937
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onReceivedHttpAuthRequest(Lcom/android/org/chromium/android_webview/AwHttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 1938
    return-void
.end method

.method private onReceivedIcon(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onReceivedIcon(Landroid/graphics/Bitmap;)V

    .line 1924
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFavicon:Landroid/graphics/Bitmap;

    .line 1925
    return-void
.end method

.method private onReceivedTouchIconUrl(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "precomposed"    # Z

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onReceivedTouchIconUrl(Ljava/lang/String;Z)V

    .line 1919
    return-void
.end method

.method private onWebLayoutContentsSizeChanged(II)V
    .locals 1
    .param p1, "widthCss"    # I
    .param p2, "heightCss"    # I

    .prologue
    .line 2060
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->onContentSizeChanged(II)V

    .line 2061
    return-void
.end method

.method private onWebLayoutPageScaleFactorChanged(F)V
    .locals 1
    .param p1, "webLayoutPageScaleFactor"    # F

    .prologue
    .line 2054
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->onPageScaleChanged(F)V

    .line 2055
    return-void
.end method

.method private postInvalidateOnAnimation()V
    .locals 1

    .prologue
    .line 2030
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwContents;->SUPPORTS_ON_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 2031
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    .line 2035
    :goto_0
    return-void

    .line 2033
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->postInvalidate()V

    goto :goto_0
.end method

.method private receivePopupContents(J)V
    .locals 13
    .param p1, "popupNativeAwContents"    # J

    .prologue
    .line 793
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDeferredShouldOverrideUrlLoadingIsPendingForPopup:Z

    .line 795
    iget-boolean v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z

    .line 796
    .local v3, "wasAttached":Z
    iget-boolean v6, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsViewVisible:Z

    .line 797
    .local v6, "wasViewVisible":Z
    iget-boolean v8, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsWindowVisible:Z

    .line 798
    .local v8, "wasWindowVisible":Z
    iget-boolean v5, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    .line 799
    .local v5, "wasPaused":Z
    iget-boolean v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerViewFocused:Z

    .line 800
    .local v4, "wasFocused":Z
    iget-boolean v7, p0, Lcom/android/org/chromium/android_webview/AwContents;->mWindowFocused:Z

    .line 803
    .local v7, "wasWindowFocused":Z
    if-eqz v4, :cond_0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {p0, v9, v10, v11}, Lcom/android/org/chromium/android_webview/AwContents;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 804
    :cond_0
    if-eqz v7, :cond_1

    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/android/org/chromium/android_webview/AwContents;->onWindowFocusChanged(Z)V

    .line 805
    :cond_1
    if-eqz v6, :cond_2

    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/android/org/chromium/android_webview/AwContents;->setViewVisibilityInternal(Z)V

    .line 806
    :cond_2
    if-eqz v8, :cond_3

    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/android/org/chromium/android_webview/AwContents;->setWindowVisibilityInternal(Z)V

    .line 807
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onDetachedFromWindow()V

    .line 808
    :cond_4
    if-nez v5, :cond_5

    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onPause()V

    .line 811
    :cond_5
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 813
    .local v2, "javascriptInterfaces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/Object;Ljava/lang/Class;>;>;"
    iget-object v9, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    if-eqz v9, :cond_6

    .line 814
    iget-object v9, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v9}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getJavascriptInterfaces()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v2, v9}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 817
    :cond_6
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->setNewAwContents(J)V

    .line 820
    if-nez v5, :cond_7

    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onResume()V

    .line 821
    :cond_7
    if-eqz v3, :cond_8

    .line 822
    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onAttachedToWindow()V

    .line 823
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents;->postInvalidateOnAnimation()V

    .line 825
    :cond_8
    iget-object v9, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getWidth()I

    move-result v9

    iget-object v10, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getHeight()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p0, v9, v10, v11, v12}, Lcom/android/org/chromium/android_webview/AwContents;->onSizeChanged(IIII)V

    .line 826
    if-eqz v8, :cond_9

    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/android/org/chromium/android_webview/AwContents;->setWindowVisibilityInternal(Z)V

    .line 827
    :cond_9
    if-eqz v6, :cond_a

    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/android/org/chromium/android_webview/AwContents;->setViewVisibilityInternal(Z)V

    .line 828
    :cond_a
    if-eqz v7, :cond_b

    invoke-virtual {p0, v7}, Lcom/android/org/chromium/android_webview/AwContents;->onWindowFocusChanged(Z)V

    .line 829
    :cond_b
    if-eqz v4, :cond_c

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {p0, v9, v10, v11}, Lcom/android/org/chromium/android_webview/AwContents;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 832
    :cond_c
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 833
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/Object;Ljava/lang/Class;>;>;"
    iget-object v11, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/Pair;

    iget-object v12, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    iget-object v10, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Class;

    invoke-virtual {v11, v12, v9, v10}, Lcom/android/org/chromium/content/browser/ContentViewCore;->addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V

    goto :goto_0

    .line 838
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/Object;Ljava/lang/Class;>;>;"
    :cond_d
    return-void
.end method

.method private requestDrawGL(Landroid/graphics/Canvas;Z)Z
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "waitForCompletion"    # Z

    .prologue
    .line 2022
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeGLDelegate:Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-interface {v0, p1, p2, v1}, Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;->requestDrawGL(Landroid/graphics/Canvas;ZLandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private requestVisitedHistoryFromClient()V
    .locals 2

    .prologue
    .line 1025
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$3;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/android_webview/AwContents$3;-><init>(Lcom/android/org/chromium/android_webview/AwContents;)V

    .line 1037
    .local v0, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<[Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->getVisitedHistory(Landroid/webkit/ValueCallback;)V

    .line 1038
    return-void
.end method

.method private saveWebArchiveInternal(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2126
    .local p2, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2127
    :cond_0
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$6;

    invoke-direct {v0, p0, p2}, Lcom/android/org/chromium/android_webview/AwContents$6;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Landroid/webkit/ValueCallback;)V

    invoke-static {v0}, Lcom/android/org/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2136
    :goto_0
    return-void

    .line 2134
    :cond_1
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeGenerateMHTML(JLjava/lang/String;Landroid/webkit/ValueCallback;)V

    goto :goto_0
.end method

.method private scrollContainerViewTo(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->scrollContainerViewTo(II)V

    .line 2066
    return-void
.end method

.method private setAwAutofillClient(Lcom/android/org/chromium/android_webview/AwAutofillClient;)V
    .locals 1
    .param p1, "client"    # Lcom/android/org/chromium/android_webview/AwAutofillClient;

    .prologue
    .line 2086
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwAutofillClient:Lcom/android/org/chromium/android_webview/AwAutofillClient;

    .line 2087
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {p1, v0}, Lcom/android/org/chromium/android_webview/AwAutofillClient;->init(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 2088
    return-void
.end method

.method public static setAwDrawGLFunctionTable(J)V
    .locals 0
    .param p0, "functionTablePointer"    # J

    .prologue
    .line 894
    invoke-static {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetAwDrawGLFunctionTable(J)V

    .line 895
    return-void
.end method

.method public static setAwDrawSWFunctionTable(J)V
    .locals 0
    .param p0, "functionTablePointer"    # J

    .prologue
    .line 890
    invoke-static {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetAwDrawSWFunctionTable(J)V

    .line 891
    return-void
.end method

.method private setContainerView(Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "newContainerView"    # Landroid/view/ViewGroup;

    .prologue
    .line 701
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    .line 702
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setContainerView(Landroid/view/ViewGroup;)V

    .line 703
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwPdfExporter;->setContainerView(Landroid/view/ViewGroup;)V

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mWebContentsDelegate:Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;->setContainerView(Landroid/view/View;)V

    .line 708
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onContainerViewChanged()V

    .line 709
    return-void
.end method

.method private setInternalAccessAdapter(Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;)V
    .locals 2
    .param p1, "internalAccessAdapter"    # Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    .prologue
    .line 696
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    .line 697
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setContainerViewInternals(Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;)V

    .line 698
    return-void
.end method

.method private setNewAwContents(J)V
    .locals 19
    .param p1, "newAwContentsPtr"    # J

    .prologue
    .line 741
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 742
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/chromium/android_webview/AwContents;->destroy()V

    .line 743
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    .line 746
    :cond_0
    sget-boolean v4, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    if-eqz v4, :cond_2

    :cond_1
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 748
    :cond_2
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    .line 755
    new-instance v4, Lcom/android/org/chromium/content/common/CleanupReference;

    new-instance v5, Lcom/android/org/chromium/android_webview/AwContents$DestroyRunnable;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const/4 v10, 0x0

    invoke-direct {v5, v6, v7, v10}, Lcom/android/org/chromium/android_webview/AwContents$DestroyRunnable;-><init>(JLcom/android/org/chromium/android_webview/AwContents$1;)V

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/android/org/chromium/content/common/CleanupReference;-><init>(Ljava/lang/Object;Ljava/lang/Runnable;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    .line 757
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/org/chromium/android_webview/AwContents;->nativeGetWebContents(J)J

    move-result-wide v8

    .line 758
    .local v8, "nativeWebContents":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    new-instance v10, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v4}, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewClient:Lcom/android/org/chromium/android_webview/AwContentViewClient;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/org/chromium/android_webview/AwContents;->mZoomControls:Lcom/android/org/chromium/android_webview/AwZoomControls;

    invoke-static/range {v5 .. v12}, Lcom/android/org/chromium/android_webview/AwContents;->createAndInitializeContentViewCore(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;JLcom/android/org/chromium/content_public/browser/GestureStateListener;Lcom/android/org/chromium/content/browser/ContentViewClient;Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    .line 761
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/org/chromium/android_webview/AwContents;->mWebContentsDelegate:Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClientBridge:Lcom/android/org/chromium/android_webview/AwContentsClientBridge;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/org/chromium/android_webview/AwContents;->mIoThreadClient:Lcom/android/org/chromium/android_webview/AwContentsIoThreadClient;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/org/chromium/android_webview/AwContents;->mInterceptNavigationDelegate:Lcom/android/org/chromium/android_webview/AwContents$InterceptNavigationDelegateImpl;

    move-object/from16 v18, v0

    move-object/from16 v11, p0

    move-object/from16 v14, p0

    invoke-direct/range {v11 .. v18}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetJavaPeers(JLcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwWebContentsDelegate;Lcom/android/org/chromium/android_webview/AwContentsClientBridge;Lcom/android/org/chromium/android_webview/AwContentsIoThreadClient;Lcom/android/org/chromium/components/navigation_interception/InterceptNavigationDelegate;)V

    .line 763
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v4, v5}, Lcom/android/org/chromium/android_webview/AwContentsClient;->installWebContentsObserver(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 764
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    invoke-virtual {v4, v8, v9}, Lcom/android/org/chromium/android_webview/AwSettings;->setWebContents(J)V

    .line 765
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    double-to-float v6, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetDipScale(JF)V

    .line 768
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onShow()V

    .line 769
    return-void
.end method

.method private setPageScaleFactorAndLimits(FFF)V
    .locals 8
    .param p1, "pageScaleFactor"    # F
    .param p2, "minPageScaleFactor"    # F
    .param p3, "maxPageScaleFactor"    # F

    .prologue
    .line 2109
    iget v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    cmpl-float v1, v1, p1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMinPageScaleFactor:F

    cmpl-float v1, v1, p2

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMaxPageScaleFactor:F

    cmpl-float v1, v1, p3

    if-nez v1, :cond_1

    .line 2123
    :cond_0
    :goto_0
    return-void

    .line 2114
    :cond_1
    iput p2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMinPageScaleFactor:F

    .line 2115
    iput p3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMaxPageScaleFactor:F

    .line 2116
    iget v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 2117
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    .line 2118
    .local v0, "oldPageScaleFactor":F
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    .line 2119
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->getCallbackHelper()Lcom/android/org/chromium/android_webview/AwContentsClientCallbackHelper;

    move-result-object v1

    float-to-double v2, v0

    iget-wide v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iget v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    float-to-double v4, v3

    iget-wide v6, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    mul-double/2addr v4, v6

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/android/org/chromium/android_webview/AwContentsClientCallbackHelper;->postOnScaleChangedScaled(FF)V

    goto :goto_0
.end method

.method public static setShouldDownloadFavicons()V
    .locals 0

    .prologue
    .line 902
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetShouldDownloadFavicons()V

    .line 903
    return-void
.end method

.method private setViewVisibilityInternal(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    .line 1790
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsViewVisible:Z

    .line 1791
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1793
    :goto_0
    return-void

    .line 1792
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    iget-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsViewVisible:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetViewVisibility(JZ)V

    goto :goto_0
.end method

.method private setWindowVisibilityInternal(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    .line 1796
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsWindowVisible:Z

    .line 1797
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1799
    :goto_0
    return-void

    .line 1798
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    iget-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsWindowVisible:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetWindowVisibility(JZ)V

    goto :goto_0
.end method

.method private updateHitTestData(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "extra"    # Ljava/lang/String;
    .param p3, "href"    # Ljava/lang/String;
    .param p4, "anchorText"    # Ljava/lang/String;
    .param p5, "imgSrc"    # Ljava/lang/String;

    .prologue
    .line 2013
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iput p1, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->hitTestResultType:I

    .line 2014
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iput-object p2, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->hitTestResultExtraData:Ljava/lang/String;

    .line 2015
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iput-object p3, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->href:Ljava/lang/String;

    .line 2016
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iput-object p4, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->anchorText:Ljava/lang/String;

    .line 2017
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iput-object p5, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->imgSrc:Ljava/lang/String;

    .line 2018
    return-void
.end method

.method private updateScrollState(IIIIFFF)V
    .locals 1
    .param p1, "maxContainerViewScrollOffsetX"    # I
    .param p2, "maxContainerViewScrollOffsetY"    # I
    .param p3, "contentWidthDip"    # I
    .param p4, "contentHeightDip"    # I
    .param p5, "pageScaleFactor"    # F
    .param p6, "minPageScaleFactor"    # F
    .param p7, "maxPageScaleFactor"    # F

    .prologue
    .line 2077
    int-to-float v0, p3

    iput v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentWidthDip:F

    .line 2078
    int-to-float v0, p4

    iput v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentHeightDip:F

    .line 2079
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->setMaxScrollOffset(II)V

    .line 2081
    invoke-direct {p0, p5, p6, p7}, Lcom/android/org/chromium/android_webview/AwContents;->setPageScaleFactorAndLimits(FFF)V

    .line 2082
    return-void
.end method

.method private useLegacyGeolocationPermissionAPI()Z
    .locals 1

    .prologue
    .line 2443
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1847
    .local p3, "requiredAnnotation":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/org/chromium/content/browser/ContentViewCore;->addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V

    .line 1848
    return-void
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->canGoBack()Z

    move-result v0

    return v0
.end method

.method public canGoBackOrForward(I)Z
    .locals 1
    .param p1, "steps"    # I

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->canGoToOffset(I)Z

    move-result v0

    return v0
.end method

.method public canGoForward()Z
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->canGoForward()Z

    move-result v0

    return v0
.end method

.method public canZoomIn()Z
    .locals 3

    .prologue
    .line 1601
    iget v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMaxPageScaleFactor:F

    iget v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    sub-float v0, v1, v2

    .line 1602
    .local v0, "zoomInExtent":F
    const v1, 0x3be56042    # 0.007f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public canZoomOut()Z
    .locals 3

    .prologue
    .line 1611
    iget v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    iget v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mMinPageScaleFactor:F

    sub-float v0, v1, v2

    .line 1612
    .local v0, "zoomOutExtent":F
    const v1, 0x3be56042    # 0.007f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public capturePicture()Landroid/graphics/Picture;
    .locals 5

    .prologue
    .line 966
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 967
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/org/chromium/android_webview/AwPicture;

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeHorizontalScrollRange()I

    move-result v1

    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v4}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeVerticalScrollRange()I

    move-result v4

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/android/org/chromium/android_webview/AwContents;->nativeCapturePicture(JII)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/android/org/chromium/android_webview/AwPicture;-><init>(J)V

    goto :goto_0
.end method

.method public clearCache(Z)V
    .locals 4
    .param p1, "includeDiskFiles"    # Z

    .prologue
    .line 1426
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1428
    :goto_0
    return-void

    .line 1427
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeClearCache(JZ)V

    goto :goto_0
.end method

.method public clearHistory()V
    .locals 1

    .prologue
    .line 1483
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->clearHistory()V

    .line 1484
    return-void
.end method

.method public clearMatches()V
    .locals 4

    .prologue
    .line 1008
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1010
    :goto_0
    return-void

    .line 1009
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeClearMatches(J)V

    goto :goto_0
.end method

.method public clearSslPreferences()V
    .locals 1

    .prologue
    .line 1509
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->clearSslPreferences()V

    .line 1510
    return-void
.end method

.method public clearView()V
    .locals 4

    .prologue
    .line 973
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 975
    :goto_0
    return-void

    .line 974
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeClearView(J)V

    goto :goto_0
.end method

.method public computeHorizontalScrollOffset()I
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeHorizontalScrollOffset()I

    move-result v0

    return v0
.end method

.method public computeHorizontalScrollRange()I
    .locals 1

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 1265
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeScrollAndAbsorbGlow(Lcom/android/org/chromium/android_webview/OverScrollGlow;)V

    .line 1266
    return-void
.end method

.method public computeVerticalScrollExtent()I
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeVerticalScrollExtent()I

    move-result v0

    return v0
.end method

.method public computeVerticalScrollOffset()I
    .locals 1

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeVerticalScrollOffset()I

    move-result v0

    return v0
.end method

.method public computeVerticalScrollRange()I
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 844
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    if-eqz v0, :cond_2

    .line 845
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 848
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z

    if-eqz v0, :cond_1

    .line 849
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeOnDetachedFromWindow(J)V

    .line 856
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->destroy()V

    .line 857
    iput-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    .line 859
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/common/CleanupReference;->cleanupNow()V

    .line 860
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    .line 863
    :cond_2
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 864
    :cond_3
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 865
    :cond_4
    return-void
.end method

.method public disableJavascriptInterfacesInspection()V
    .locals 2

    .prologue
    .line 912
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setAllowJavascriptInterfacesInspection(Z)V

    .line 913
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public documentHasImages(Landroid/os/Message;)V
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1431
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1433
    :goto_0
    return-void

    .line 1432
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeDocumentHasImages(JLandroid/os/Message;)V

    goto :goto_0
.end method

.method public enableOnNewPicture(ZZ)V
    .locals 4
    .param p1, "enabled"    # Z
    .param p2, "invalidationOnly"    # Z

    .prologue
    .line 983
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 995
    :goto_0
    return-void

    .line 984
    :cond_0
    if-eqz p2, :cond_2

    .line 985
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPictureListenerContentProvider:Ljava/util/concurrent/Callable;

    .line 994
    :cond_1
    :goto_1
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeEnableOnNewPicture(JZ)V

    goto :goto_0

    .line 986
    :cond_2
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPictureListenerContentProvider:Ljava/util/concurrent/Callable;

    if-nez v0, :cond_1

    .line 987
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$2;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/android_webview/AwContents$2;-><init>(Lcom/android/org/chromium/android_webview/AwContents;)V

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPictureListenerContentProvider:Ljava/util/concurrent/Callable;

    goto :goto_1
.end method

.method enterFullScreen()Landroid/view/View;
    .locals 4

    .prologue
    .line 638
    sget-boolean v1, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 643
    :cond_0
    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->onDetachedFromWindow()V

    .line 647
    new-instance v0, Lcom/android/org/chromium/android_webview/FullScreenView;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-direct {v0, v1, v2}, Lcom/android/org/chromium/android_webview/FullScreenView;-><init>(Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwViewMethods;)V

    .line 648
    .local v0, "fullScreenView":Lcom/android/org/chromium/android_webview/FullScreenView;
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->enterFullScreen(Lcom/android/org/chromium/android_webview/FullScreenView;)V
    invoke-static {v1, v0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2500(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;Lcom/android/org/chromium/android_webview/FullScreenView;)V

    .line 649
    new-instance v1, Lcom/android/org/chromium/android_webview/NullAwViewMethods;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2, v3}, Lcom/android/org/chromium/android_webview/NullAwViewMethods;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    .line 650
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 651
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/FullScreenView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 654
    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/FullScreenView;->getInternalAccessAdapter()Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->setInternalAccessAdapter(Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;)V

    .line 655
    invoke-direct {p0, v0}, Lcom/android/org/chromium/android_webview/AwContents;->setContainerView(Landroid/view/ViewGroup;)V

    .line 657
    return-object v0
.end method

.method public evaluateJavaScript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 2
    .param p1, "script"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1670
    .local p2, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 1671
    .local v0, "jsCallback":Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;
    if-eqz p2, :cond_0

    .line 1672
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$5;

    .end local v0    # "jsCallback":Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;
    invoke-direct {v0, p0, p2}, Lcom/android/org/chromium/android_webview/AwContents$5;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Landroid/webkit/ValueCallback;)V

    .line 1680
    .restart local v0    # "jsCallback":Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1, p1, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->evaluateJavaScript(Ljava/lang/String;Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V

    .line 1681
    return-void
.end method

.method public evaluateJavaScriptEvenIfNotYetNavigated(Ljava/lang/String;)V
    .locals 1
    .param p1, "script"    # Ljava/lang/String;

    .prologue
    .line 1687
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->evaluateJavaScriptEvenIfNotYetNavigated(Ljava/lang/String;)V

    .line 1688
    return-void
.end method

.method exitFullScreen()V
    .locals 5

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->isFullScreen()Z

    move-result v3

    if-nez v3, :cond_0

    .line 693
    :goto_0
    return-void

    .line 675
    :cond_0
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialAwViewMethods()Lcom/android/org/chromium/android_webview/AwViewMethods;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2600(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/AwViewMethods;

    move-result-object v0

    .line 676
    .local v0, "awViewMethodsImpl":Lcom/android/org/chromium/android_webview/AwViewMethods;
    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onDetachedFromWindow()V

    .line 680
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getFullScreenView()Lcom/android/org/chromium/android_webview/FullScreenView;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2700(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/FullScreenView;

    move-result-object v1

    .line 681
    .local v1, "fullscreenView":Lcom/android/org/chromium/android_webview/FullScreenView;
    new-instance v3, Lcom/android/org/chromium/android_webview/NullAwViewMethods;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/FullScreenView;->getInternalAccessAdapter()Lcom/android/org/chromium/android_webview/FullScreenView$InternalAccessAdapter;

    move-result-object v4

    invoke-direct {v3, p0, v4, v1}, Lcom/android/org/chromium/android_webview/NullAwViewMethods;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Landroid/view/View;)V

    invoke-virtual {v1, v3}, Lcom/android/org/chromium/android_webview/FullScreenView;->setAwViewMethods(Lcom/android/org/chromium/android_webview/AwViewMethods;)V

    .line 683
    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    .line 684
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialContainerView()Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2800(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Landroid/view/ViewGroup;

    move-result-object v2

    .line 685
    .local v2, "initialContainerView":Landroid/view/ViewGroup;
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 686
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mLayoutChangeListener:Lcom/android/org/chromium/android_webview/AwContents$AwLayoutChangeListener;

    invoke-virtual {v1, v3}, Lcom/android/org/chromium/android_webview/FullScreenView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 689
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialInternalAccessDelegate()Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2900(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/org/chromium/android_webview/AwContents;->setInternalAccessAdapter(Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;)V

    .line 690
    invoke-direct {p0, v2}, Lcom/android/org/chromium/android_webview/AwContents;->setContainerView(Landroid/view/ViewGroup;)V

    .line 692
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->exitFullScreen()V
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$3000(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)V

    goto :goto_0
.end method

.method public extractSmartClipData(IIII)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 2174
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/content/browser/ContentViewCore;->extractSmartClipData(IIII)V

    .line 2175
    return-void
.end method

.method public findAllAsync(Ljava/lang/String;)V
    .locals 4
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 998
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1000
    :goto_0
    return-void

    .line 999
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeFindAllAsync(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public findNext(Z)V
    .locals 4
    .param p1, "forward"    # Z

    .prologue
    .line 1003
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1005
    :goto_0
    return-void

    .line 1004
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeFindNext(JZ)V

    goto :goto_0
.end method

.method public flingScroll(II)V
    .locals 1
    .param p1, "velocityX"    # I
    .param p2, "velocityY"    # I

    .prologue
    .line 1578
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->flingScroll(II)V

    .line 1579
    return-void
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .prologue
    .line 1864
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    return-object v0
.end method

.method public getAwDrawGLViewContext()J
    .locals 4

    .prologue
    .line 927
    sget-boolean v0, Lcom/android/org/chromium/android_webview/AwContents;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 931
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeGetAwDrawGLViewContext(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCertificate()Landroid/net/http/SslCertificate;
    .locals 4

    .prologue
    .line 1501
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1502
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeGetCertificate(J)[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/org/chromium/android_webview/SslUtil;->getCertificateFromDerBytes([B)Landroid/net/http/SslCertificate;

    move-result-object v0

    goto :goto_0
.end method

.method public getContentHeightCss()I
    .locals 2

    .prologue
    .line 958
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentHeightDip:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getContentViewCore()Lcom/android/org/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method public getContentWidthCss()I
    .locals 2

    .prologue
    .line 962
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentWidthDip:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method getEffectiveBackgroundColor()I
    .locals 4

    .prologue
    .line 1141
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->isCachedRendererBackgroundColorValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1142
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBaseBackgroundColor:I

    .line 1144
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->getCachedRendererBackgroundColor()I

    move-result v0

    goto :goto_0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFavicon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "realm"    # Ljava/lang/String;

    .prologue
    .line 1487
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getHttpAuthDatabase(Landroid/content/Context;)Lcom/android/org/chromium/android_webview/HttpAuthDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/HttpAuthDatabase;->getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastHitTestResult()Lcom/android/org/chromium/android_webview/AwContents$HitTestData;
    .locals 4

    .prologue
    .line 1522
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1524
    :goto_0
    return-object v0

    .line 1523
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeUpdateLastHitTestData(J)V

    .line 1524
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    goto :goto_0
.end method

.method public getMostRecentProgress()I
    .locals 1

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mWebContentsDelegate:Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwWebContentsDelegateAdapter;->getMostRecentProgress()I

    move-result v0

    return v0
.end method

.method public getNavigationHistory()Lcom/android/org/chromium/content/browser/NavigationHistory;
    .locals 1

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getNavigationHistory()Lcom/android/org/chromium/content/browser/NavigationHistory;

    move-result-object v0

    return-object v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1457
    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getNavigationHistory()Lcom/android/org/chromium/content/browser/NavigationHistory;

    move-result-object v1

    .line 1458
    .local v1, "history":Lcom/android/org/chromium/content/browser/NavigationHistory;
    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/NavigationHistory;->getCurrentEntryIndex()I

    move-result v0

    .line 1459
    .local v0, "currentIndex":I
    if-ltz v0, :cond_0

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/NavigationHistory;->getEntryCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1460
    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/NavigationHistory;->getEntryAtIndex(I)Lcom/android/org/chromium/content/browser/NavigationEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/NavigationEntry;->getOriginalUrl()Ljava/lang/String;

    move-result-object v2

    .line 1462
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getPdfExporter()Lcom/android/org/chromium/android_webview/AwPdfExporter;
    .locals 4

    .prologue
    .line 879
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 880
    const/4 v0, 0x0

    .line 886
    :goto_0
    return-object v0

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

    if-nez v0, :cond_1

    .line 883
    new-instance v0, Lcom/android/org/chromium/android_webview/AwPdfExporter;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-direct {v0, v1}, Lcom/android/org/chromium/android_webview/AwPdfExporter;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

    .line 884
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeCreatePdfExporter(JLcom/android/org/chromium/android_webview/AwPdfExporter;)V

    .line 886
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwPdfExporter:Lcom/android/org/chromium/android_webview/AwPdfExporter;

    goto :goto_0
.end method

.method public getScale()F
    .locals 4

    .prologue
    .line 1571
    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPageScaleFactor:F

    float-to-double v0, v0

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public getSettings()Lcom/android/org/chromium/android_webview/AwSettings;
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1116
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 1117
    .local v0, "url":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 1118
    .end local v0    # "url":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->goBack()V

    .line 1329
    return-void
.end method

.method public goBackOrForward(I)V
    .locals 1
    .param p1, "steps"    # I

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->goToOffset(I)V

    .line 1357
    return-void
.end method

.method public goForward()V
    .locals 1

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->goForward()V

    .line 1343
    return-void
.end method

.method public hideAutofillPopup()V
    .locals 1

    .prologue
    .line 1896
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwAutofillClient:Lcom/android/org/chromium/android_webview/AwAutofillClient;

    if-eqz v0, :cond_0

    .line 1897
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwAutofillClient:Lcom/android/org/chromium/android_webview/AwAutofillClient;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwAutofillClient;->hideAutofillPopup()V

    .line 1899
    :cond_0
    return-void
.end method

.method public invokeZoomPicker()V
    .locals 1

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->invokeZoomPicker()V

    .line 1656
    return-void
.end method

.method isFullScreen()Z
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mFullScreenTransitionsState:Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->isFullScreen()Z
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->access$2400(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Z

    move-result v0

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 1395
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    return v0
.end method

.method public loadUrl(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V
    .locals 10
    .param p1, "params"    # Lcom/android/org/chromium/content/browser/LoadUrlParams;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 1048
    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getLoadUrlType()I

    move-result v4

    if-ne v4, v9, :cond_0

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->isBaseUrlDataScheme()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1053
    invoke-virtual {p1, v8}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setCanLoadLocalResources(Z)V

    .line 1057
    :cond_0
    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v5}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getTransitionType()I

    move-result v4

    if-nez v4, :cond_1

    .line 1060
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setTransitionType(I)V

    .line 1062
    :cond_1
    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getTransitionType()I

    move-result v4

    const/high16 v5, 0x8000000

    or-int/2addr v4, v5

    invoke-virtual {p1, v4}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setTransitionType(I)V

    .line 1067
    invoke-virtual {p1, v9}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setOverrideUserAgent(I)V

    .line 1074
    const-string v0, "referer"

    .line 1075
    .local v0, "REFERER":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getExtraHeaders()Ljava/util/Map;

    move-result-object v1

    .line 1076
    .local v1, "extraHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_3

    .line 1077
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1078
    .local v2, "header":Ljava/lang/String;
    const-string v4, "referer"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1079
    new-instance v5, Lcom/android/org/chromium/content_public/Referrer;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v5, v4, v8}, Lcom/android/org/chromium/content_public/Referrer;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1, v5}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setReferrer(Lcom/android/org/chromium/content_public/Referrer;)V

    .line 1080
    invoke-virtual {p1, v1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setExtraHeaders(Ljava/util/Map;)V

    .line 1086
    .end local v2    # "header":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-wide v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    .line 1087
    iget-wide v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getExtraHttpRequestHeadersString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetExtraHeadersForUrl(JLjava/lang/String;Ljava/lang/String;)V

    .line 1090
    :cond_4
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1, v4}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setExtraHeaders(Ljava/util/Map;)V

    .line 1092
    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v4, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->loadUrl(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    .line 1097
    iget-boolean v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mHasRequestedVisitedHistoryFromClient:Z

    if-nez v4, :cond_5

    .line 1098
    iput-boolean v8, p0, Lcom/android/org/chromium/android_webview/AwContents;->mHasRequestedVisitedHistoryFromClient:Z

    .line 1099
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents;->requestVisitedHistoryFromClient()V

    .line 1102
    :cond_5
    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getLoadUrlType()I

    move-result v4

    if-ne v4, v9, :cond_6

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getBaseUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 1106
    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v4}, Lcom/android/org/chromium/android_webview/AwContentsClient;->getCallbackHelper()Lcom/android/org/chromium/android_webview/AwContentsClientCallbackHelper;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getBaseUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/org/chromium/android_webview/AwContentsClientCallbackHelper;->postOnPageStarted(Ljava/lang/String;)V

    .line 1108
    :cond_6
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 1726
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mTemporarilyDetached:Z

    .line 1727
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onAttachedToWindow()V

    .line 1728
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1719
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1720
    return-void
.end method

.method public onContainerViewOverScrolled(IIZZ)V
    .locals 7
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v3

    .line 1240
    .local v3, "oldX":I
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v4

    .line 1242
    .local v4, "oldY":I
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->onContainerViewOverScrolled(IIZZ)V

    .line 1244
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    if-eqz v0, :cond_0

    .line 1245
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v2

    iget-object v5, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v5}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeMaximumHorizontalScrollOffset()I

    move-result v5

    iget-object v6, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v6}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeMaximumVerticalScrollOffset()I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->pullGlow(IIIIII)V

    .line 1250
    :cond_0
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 1735
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onDetachedFromWindow()V

    .line 1736
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 950
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onDraw(Landroid/graphics/Canvas;)V

    .line 951
    return-void
.end method

.method public onFindResultReceived(IIZ)V
    .locals 1
    .param p1, "activeMatchOrdinal"    # I
    .param p2, "numberOfMatches"    # I
    .param p3, "isDoneCounting"    # Z

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onFindResultReceived(IIZ)V

    .line 1999
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1765
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mTemporarilyDetached:Z

    .line 1766
    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1749
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mTemporarilyDetached:Z

    if-nez v0, :cond_0

    .line 1750
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1752
    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1712
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1705
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1879
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1872
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 954
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onMeasure(II)V

    .line 955
    return-void
.end method

.method public onNewPicture()V
    .locals 2

    .prologue
    .line 2006
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContentsClient;->getCallbackHelper()Lcom/android/org/chromium/android_webview/AwContentsClientCallbackHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPictureListenerContentProvider:Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwContentsClientCallbackHelper;->postOnNewPicture(Ljava/util/concurrent/Callable;)V

    .line 2007
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 1377
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1380
    :cond_0
    :goto_0
    return-void

    .line 1378
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    .line 1379
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    iget-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetIsPaused(JZ)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 1386
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1389
    :cond_0
    :goto_0
    return-void

    .line 1387
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    .line 1388
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    iget-boolean v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mIsPaused:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetIsPaused(JZ)V

    goto :goto_0
.end method

.method public onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "ow"    # I
    .param p4, "oh"    # I

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onSizeChanged(IIII)V

    .line 1773
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1758
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mTemporarilyDetached:Z

    .line 1759
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 1779
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1780
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 1742
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onWindowFocusChanged(Z)V

    .line 1743
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/android_webview/AwViewMethods;->onWindowVisibilityChanged(I)V

    .line 1787
    return-void
.end method

.method public overlayHorizontalScrollbar()Z
    .locals 1

    .prologue
    .line 1211
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayHorizontalScrollbar:Z

    return v0
.end method

.method public overlayVerticalScrollbar()Z
    .locals 1

    .prologue
    .line 1218
    iget-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayVerticalScrollbar:Z

    return v0
.end method

.method public pageDown(Z)Z
    .locals 1
    .param p1, "bottom"    # Z

    .prologue
    .line 1592
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->pageDown(Z)Z

    move-result v0

    return v0
.end method

.method public pageUp(Z)Z
    .locals 1
    .param p1, "top"    # Z

    .prologue
    .line 1585
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->pageUp(Z)Z

    move-result v0

    return v0
.end method

.method public pauseTimers()V
    .locals 1

    .prologue
    .line 1363
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewStatics;->setWebKitSharedTimersSuspended(Z)V

    .line 1364
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "action"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 1889
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public reload()V
    .locals 2

    .prologue
    .line 1314
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->reload(Z)V

    .line 1315
    return-void
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .locals 1
    .param p1, "interfaceName"    # Ljava/lang/String;

    .prologue
    .line 1854
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 1855
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "immediate"    # Z

    .prologue
    .line 1256
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2, p2, p3}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->requestChildRectangleOnScreen(IILandroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method public requestFocus()V
    .locals 1

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwViewMethods;->requestFocus()V

    .line 1123
    return-void
.end method

.method public requestFocusNodeHref(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1531
    if-eqz p1, :cond_0

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1544
    :cond_0
    :goto_0
    return-void

    .line 1533
    :cond_1
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->nativeUpdateLastHitTestData(J)V

    .line 1534
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1539
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "url"

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iget-object v2, v2, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->href:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    const-string v1, "title"

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iget-object v2, v2, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->anchorText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1541
    const-string v1, "src"

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iget-object v2, v2, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->imgSrc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1543
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public requestImageRef(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1550
    if-eqz p1, :cond_0

    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1557
    :cond_0
    :goto_0
    return-void

    .line 1552
    :cond_1
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->nativeUpdateLastHitTestData(J)V

    .line 1553
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1554
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "url"

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mPossiblyStaleHitTestData:Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    iget-object v2, v2, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->imgSrc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1555
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1556
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 1826
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 1839
    :cond_0
    :goto_0
    return v0

    .line 1828
    :cond_1
    const-string v2, "WEBVIEW_CHROMIUM_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 1829
    .local v1, "state":[B
    if-eqz v1, :cond_0

    .line 1831
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v2, v3, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeRestoreFromOpaqueState(J[B)Z

    move-result v0

    .line 1837
    .local v0, "result":Z
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v3}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onReceivedTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resumeTimers()V
    .locals 1

    .prologue
    .line 1370
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewStatics;->setWebKitSharedTimersSuspended(Z)V

    .line 1371
    return-void
.end method

.method public saveState(Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 1811
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 1817
    :cond_0
    :goto_0
    return v1

    .line 1813
    :cond_1
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->nativeGetOpaqueState(J)[B

    move-result-object v0

    .line 1814
    .local v0, "state":[B
    if-eqz v0, :cond_0

    .line 1816
    const-string v1, "WEBVIEW_CHROMIUM_STATE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 1817
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V
    .locals 2
    .param p1, "basename"    # Ljava/lang/String;
    .param p2, "autoname"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1437
    .local p3, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    if-nez p2, :cond_0

    .line 1438
    invoke-direct {p0, p1, p3}, Lcom/android/org/chromium/android_webview/AwContents;->saveWebArchiveInternal(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 1454
    :goto_0
    return-void

    .line 1443
    :cond_0
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$4;

    invoke-direct {v0, p0, p1, p3}, Lcom/android/org/chromium/android_webview/AwContents$4;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 1126
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBaseBackgroundColor:I

    .line 1127
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetBackgroundColor(JI)V

    .line 1128
    :cond_0
    return-void
.end method

.method public setHorizontalScrollbarOverlay(Z)V
    .locals 0
    .param p1, "overlay"    # Z

    .prologue
    .line 1197
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayHorizontalScrollbar:Z

    .line 1198
    return-void
.end method

.method public setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "realm"    # Ljava/lang/String;
    .param p3, "username"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    .line 1493
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getHttpAuthDatabase(Landroid/content/Context;)Lcom/android/org/chromium/android_webview/HttpAuthDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/HttpAuthDatabase;->setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1495
    return-void
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .locals 1
    .param p1, "layerType"    # I
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwViewMethods;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1135
    return-void
.end method

.method public setNetworkAvailable(Z)V
    .locals 4
    .param p1, "networkUp"    # Z

    .prologue
    .line 1902
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1904
    :goto_0
    return-void

    .line 1903
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeSetJsOnlineProperty(JZ)V

    goto :goto_0
.end method

.method public setOverScrollMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 1166
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 1167
    new-instance v0, Lcom/android/org/chromium/android_webview/OverScrollGlow;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, v2}, Lcom/android/org/chromium/android_webview/OverScrollGlow;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    .line 1171
    :goto_0
    return-void

    .line 1169
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;

    goto :goto_0
.end method

.method public setScrollBarStyle(I)V
    .locals 1
    .param p1, "style"    # I

    .prologue
    .line 1185
    if-eqz p1, :cond_0

    const/high16 v0, 0x2000000

    if-ne p1, v0, :cond_1

    .line 1187
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayVerticalScrollbar:Z

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayHorizontalScrollbar:Z

    .line 1191
    :goto_0
    return-void

    .line 1189
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayVerticalScrollbar:Z

    iput-boolean v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayHorizontalScrollbar:Z

    goto :goto_0
.end method

.method public setSmartClipResultHandler(Landroid/os/Handler;)V
    .locals 2
    .param p1, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 2179
    if-nez p1, :cond_0

    .line 2180
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setSmartClipDataListener(Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;)V

    .line 2200
    :goto_0
    return-void

    .line 2183
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    new-instance v1, Lcom/android/org/chromium/android_webview/AwContents$7;

    invoke-direct {v1, p0, p1}, Lcom/android/org/chromium/android_webview/AwContents$7;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setSmartClipDataListener(Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;)V

    goto :goto_0
.end method

.method public setVerticalScrollbarOverlay(Z)V
    .locals 0
    .param p1, "overlay"    # Z

    .prologue
    .line 1204
    iput-boolean p1, p0, Lcom/android/org/chromium/android_webview/AwContents;->mOverlayVerticalScrollbar:Z

    .line 1205
    return-void
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->stopLoading()V

    .line 1308
    return-void
.end method

.method public supplyContentsForPopup(Lcom/android/org/chromium/android_webview/AwContents;)V
    .locals 4
    .param p1, "newContents"    # Lcom/android/org/chromium/android_webview/AwContents;

    .prologue
    .line 776
    iget-wide v2, p0, Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->nativeReleasePopupAwContents(J)J

    move-result-wide v0

    .line 777
    .local v0, "popupNativeAwContents":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 778
    const-string v2, "AwContents"

    const-string v3, "Popup WebView bind failed: no pending content."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/org/chromium/android_webview/AwContents;->destroy()V

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 782
    :cond_1
    if-nez p1, :cond_2

    .line 783
    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->nativeDestroy(J)V

    goto :goto_0

    .line 787
    :cond_2
    invoke-direct {p1, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->receivePopupContents(J)V

    goto :goto_0
.end method

.method public supportsAccessibilityAction(I)Z
    .locals 1
    .param p1, "action"    # I

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->supportsAccessibilityAction(I)Z

    move-result v0

    return v0
.end method

.method public zoomBy(F)Z
    .locals 2
    .param p1, "delta"    # F

    .prologue
    .line 1645
    const v0, 0x3c23d70a    # 0.01f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1646
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "zoom delta value outside [0.01, 100] range."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1648
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->pinchByDelta(F)Z

    move-result v0

    return v0
.end method

.method public zoomIn()Z
    .locals 1

    .prologue
    .line 1621
    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->canZoomIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1622
    const/4 v0, 0x0

    .line 1624
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/android_webview/AwContents;->zoomBy(F)Z

    move-result v0

    goto :goto_0
.end method

.method public zoomOut()Z
    .locals 1

    .prologue
    .line 1633
    invoke-virtual {p0}, Lcom/android/org/chromium/android_webview/AwContents;->canZoomOut()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1634
    const/4 v0, 0x0

    .line 1636
    :goto_0
    return v0

    :cond_0
    const v0, 0x3f4ccccd    # 0.8f

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/android_webview/AwContents;->zoomBy(F)Z

    move-result v0

    goto :goto_0
.end method
