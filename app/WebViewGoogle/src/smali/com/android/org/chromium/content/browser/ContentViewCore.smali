.class public Lcom/android/org/chromium/content/browser/ContentViewCore;
.super Ljava/lang/Object;
.source "ContentViewCore.java"

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;
.implements Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;,
        Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;,
        Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;,
        Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sIsSPenSupported:Ljava/lang/Boolean;


# instance fields
.field private mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

.field private final mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAccessibilityScriptInjectionObserver:Landroid/database/ContentObserver;

.field private mActionMode:Landroid/view/ActionMode;

.field private mAdapterInputConnectionFactory:Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;

.field private mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

.field private mContainerView:Landroid/view/ViewGroup;

.field private mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

.field private mContentSettings:Lcom/android/org/chromium/content/browser/ContentSettings;

.field private mContentViewClient:Lcom/android/org/chromium/content/browser/ContentViewClient;

.field private final mContext:Landroid/content/Context;

.field private mCurrentTouchOffsetX:F

.field private mCurrentTouchOffsetY:F

.field private mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

.field private mDownloadDelegate:Lcom/android/org/chromium/content/browser/ContentViewDownloadDelegate;

.field private final mEditable:Landroid/text/Editable;

.field private final mEndHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

.field private mFakeMouseMoveRunnable:Ljava/lang/Runnable;

.field private final mFocusPreOSKViewportRect:Landroid/graphics/Rect;

.field private final mGestureStateListeners:Lcom/android/org/chromium/base/ObserverList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/org/chromium/base/ObserverList",
            "<",
            "Lcom/android/org/chromium/content_public/browser/GestureStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/org/chromium/base/ObserverList$RewindableIterator",
            "<",
            "Lcom/android/org/chromium/content_public/browser/GestureStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mHasSelection:Z

.field private mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

.field private mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

.field private mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

.field private mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

.field private final mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

.field private final mJavaScriptInterfaces:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Class;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLastSelectedText:Ljava/lang/String;

.field private mLastTapX:I

.field private mLastTapY:I

.field private mNativeAccessibilityAllowed:Z

.field private mNativeAccessibilityEnabled:Z

.field private mNativeContentViewCore:J

.field private mOverdrawBottomHeightPix:I

.field private mPhysicalBackingHeightPix:I

.field private mPhysicalBackingWidthPix:I

.field private mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

.field private mPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

.field private mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

.field private mPotentiallyActiveFlingCount:I

.field private final mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

.field private final mRetainedJavaScriptObjects:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

.field private mSelectionEditable:Z

.field private mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

.field private mShouldSetAccessibilityFocusOnPageLoad:Z

.field private mSmartClipDataListener:Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;

.field private mSmartClipOffsetX:I

.field private mSmartClipOffsetY:I

.field private final mStartHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

.field private mTouchExplorationEnabled:Z

.field private mTouchScrollInProgress:Z

.field private mUnselectAllOnActionModeDismiss:Z

.field private mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

.field private mViewportHeightPix:I

.field private mViewportSizeOffsetHeightPix:I

.field private mViewportSizeOffsetWidthPix:I

.field private mViewportWidthPix:I

.field private mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

.field private mWebContentsObserver:Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;

.field private mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    .line 144
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRetainedJavaScriptObjects:Ljava/util/HashSet;

    .line 241
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    .line 250
    iput-object v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    .line 322
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    .line 339
    iput-object v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSmartClipDataListener:Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;

    .line 369
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    .line 371
    new-instance v2, Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;

    invoke-direct {v2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;-><init>()V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAdapterInputConnectionFactory:Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;

    .line 372
    new-instance v2, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    .line 374
    new-instance v2, Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-direct {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates;-><init>()V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    .line 375
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 376
    .local v0, "deviceScaleFactor":F
    invoke-static {}, Lcom/android/org/chromium/base/CommandLine;->getInstance()Lcom/android/org/chromium/base/CommandLine;

    move-result-object v2

    const-string v3, "force-device-scale-factor"

    invoke-virtual {v2, v3}, Lcom/android/org/chromium/base/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "forceScaleFactor":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 379
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 381
    :cond_0
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v2, v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->setDeviceScaleFactor(F)V

    .line 382
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->createNormalizedPoint()Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    move-result-object v2

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    .line 383
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->createNormalizedPoint()Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    move-result-object v2

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    .line 384
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->createNormalizedPoint()Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    move-result-object v2

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    .line 385
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 387
    new-instance v2, Lcom/android/org/chromium/base/ObserverList;

    invoke-direct {v2}, Lcom/android/org/chromium/base/ObserverList;-><init>()V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListeners:Lcom/android/org/chromium/base/ObserverList;

    .line 388
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListeners:Lcom/android/org/chromium/base/ObserverList;

    invoke-virtual {v2}, Lcom/android/org/chromium/base/ObserverList;->rewindableIterator()Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    .line 390
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEditable:Landroid/text/Editable;

    .line 391
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEditable:Landroid/text/Editable;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 392
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hidePopups()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->resetScrollInProgress()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->resetGestureDetection()V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/org/chromium/content/browser/ContentViewCore;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    return-wide v0
.end method

.method static synthetic access$1500(Lcom/android/org/chromium/content/browser/ContentViewCore;JJFF)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;
    .param p1, "x1"    # J
    .param p3, "x2"    # J
    .param p5, "x3"    # F
    .param p6, "x4"    # F

    .prologue
    .line 98
    invoke-direct/range {p0 .. p6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSingleTap(JJFF)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/org/chromium/content/browser/ContentViewCore;JJFF)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;
    .param p1, "x1"    # J
    .param p3, "x2"    # J
    .param p5, "x3"    # F
    .param p6, "x4"    # F

    .prologue
    .line 98
    invoke-direct/range {p0 .. p6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeLongPress(JJFF)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/org/chromium/content/browser/ContentViewCore;JFFFF)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;
    .param p1, "x1"    # J
    .param p3, "x2"    # F
    .param p4, "x3"    # F
    .param p5, "x4"    # F
    .param p6, "x5"    # F

    .prologue
    .line 98
    invoke-direct/range {p0 .. p6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSelectBetweenCoordinates(JFFFF)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;
    .param p1, "x1"    # Landroid/view/ActionMode;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->showSelectActionBar()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/RenderCoordinates;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/org/chromium/content/browser/ContentViewCore;JFF)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;
    .param p1, "x1"    # J
    .param p3, "x2"    # F
    .param p4, "x3"    # F

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeMoveCaret(JFF)V

    return-void
.end method

.method static synthetic access$2100(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/input/ImeAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->allowTextHandleFadeIn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/input/SelectionHandleController;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hideHandles()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hasFocus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->scrollFocusedEditableNodeIntoView()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    return-void
.end method

.method private addToNavigationHistory(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "history"    # Ljava/lang/Object;
    .param p2, "index"    # I
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "virtualUrl"    # Ljava/lang/String;
    .param p5, "originalUrl"    # Ljava/lang/String;
    .param p6, "title"    # Ljava/lang/String;
    .param p7, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 3095
    new-instance v0, Lcom/android/org/chromium/content/browser/NavigationEntry;

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/org/chromium/content/browser/NavigationEntry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 3097
    .local v0, "entry":Lcom/android/org/chromium/content/browser/NavigationEntry;
    check-cast p1, Lcom/android/org/chromium/content/browser/NavigationHistory;

    .end local p1    # "history":Ljava/lang/Object;
    invoke-virtual {p1, v0}, Lcom/android/org/chromium/content/browser/NavigationHistory;->addEntry(Lcom/android/org/chromium/content/browser/NavigationEntry;)V

    .line 3098
    return-void
.end method

.method private allowTextHandleFadeIn()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2276
    iget-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    if-eqz v1, :cond_1

    .line 2280
    :cond_0
    :goto_0
    return v0

    .line 2278
    :cond_1
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/PopupZoomer;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2280
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private cancelRequestToScrollFocusedEditableNodeIntoView()V
    .locals 1

    .prologue
    .line 1661
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 1662
    return-void
.end method

.method public static convertSPenEventAction(I)I
    .locals 0
    .param p0, "eventActionMasked"    # I

    .prologue
    .line 1159
    packed-switch p0, :pswitch_data_0

    .line 1169
    .end local p0    # "eventActionMasked":I
    :goto_0
    return p0

    .line 1161
    .restart local p0    # "eventActionMasked":I
    :pswitch_0
    const/4 p0, 0x0

    goto :goto_0

    .line 1163
    :pswitch_1
    const/4 p0, 0x1

    goto :goto_0

    .line 1165
    :pswitch_2
    const/4 p0, 0x2

    goto :goto_0

    .line 1167
    :pswitch_3
    const/4 p0, 0x3

    goto :goto_0

    .line 1159
    :pswitch_data_0
    .packed-switch 0xd3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private createImeAdapter(Landroid/content/Context;)Lcom/android/org/chromium/content/browser/input/ImeAdapter;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 541
    new-instance v0, Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    new-instance v2, Lcom/android/org/chromium/content/browser/ContentViewCore$2;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$2;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    invoke-direct {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;-><init>(Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;)V

    return-object v0
.end method

.method private createOffsetMotionEvent(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 3
    .param p1, "src"    # Landroid/view/MotionEvent;

    .prologue
    .line 1809
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1810
    .local v0, "dst":Landroid/view/MotionEvent;
    iget v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mCurrentTouchOffsetX:F

    iget v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mCurrentTouchOffsetY:F

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1811
    return-object v0
.end method

.method private static createRect(IIII)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "right"    # I
    .param p3, "bottom"    # I

    .prologue
    .line 3142
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private createTouchEventSynthesizer()Lcom/android/org/chromium/content/browser/TouchEventSynthesizer;
    .locals 1

    .prologue
    .line 2496
    new-instance v0, Lcom/android/org/chromium/content/browser/TouchEventSynthesizer;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/TouchEventSynthesizer;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    return-object v0
.end method

.method private static detectSPenSupport(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 1136
    const-string v6, "SAMSUNG"

    sget-object v7, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1145
    :cond_0
    :goto_0
    return v5

    .line 1139
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v3

    .line 1140
    .local v3, "infos":[Landroid/content/pm/FeatureInfo;
    move-object v0, v3

    .local v0, "arr$":[Landroid/content/pm/FeatureInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v2, v0, v1

    .line 1141
    .local v2, "info":Landroid/content/pm/FeatureInfo;
    const-string v6, "com.sec.feature.spen_usp"

    iget-object v7, v2, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1142
    const/4 v5, 0x1

    goto :goto_0

    .line 1140
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private filterTapOrPressEvent(III)Z
    .locals 2
    .param p1, "type"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1326
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->offerLongPressToEmbedder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1327
    const/4 v0, 0x1

    .line 1330
    :goto_0
    return v0

    .line 1329
    :cond_0
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-direct {p0, p1, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateForTapOrPress(IFF)V

    .line 1330
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getContentVideoViewClient()Lcom/android/org/chromium/content/browser/ContentVideoViewClient;
    .locals 1

    .prologue
    .line 3215
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewClient;->getContentVideoViewClient()Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    move-result-object v0

    return-object v0
.end method

.method private getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
    .locals 3

    .prologue
    .line 2040
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    if-nez v0, :cond_0

    .line 2041
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/org/chromium/content/browser/ContentViewCore$10;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    .line 2071
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    .line 2074
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    return-object v0
.end method

.method private getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;
    .locals 3

    .prologue
    .line 2012
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    if-nez v0, :cond_0

    .line 2013
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/org/chromium/content/browser/ContentViewCore$9;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    .line 2033
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    .line 2036
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    return-object v0
.end method

.method private hasFocus()Z
    .locals 1

    .prologue
    .line 2619
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocusable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2620
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v0

    goto :goto_0
.end method

.method private hideHandles()V
    .locals 2

    .prologue
    .line 2102
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    if-eqz v0, :cond_0

    .line 2103
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    .line 2105
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    if-eqz v0, :cond_1

    .line 2106
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    .line 2108
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    invoke-interface {v0, v1}, Lcom/android/org/chromium/content/browser/PositionObserver;->removeListener(Lcom/android/org/chromium/content/browser/PositionObserver$Listener;)V

    .line 2109
    return-void
.end method

.method private hidePopups()V
    .locals 0

    .prologue
    .line 1485
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hideSelectPopup()V

    .line 1486
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hideHandles()V

    .line 1487
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hideSelectActionBar()V

    .line 1488
    return-void
.end method

.method private hideSelectPopup()V
    .locals 1

    .prologue
    .line 2475
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/input/SelectPopup;->hide()V

    .line 2476
    :cond_0
    return-void
.end method

.method private initPopupZoomer(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 727
    new-instance v1, Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-direct {v1, p1}, Lcom/android/org/chromium/content/browser/PopupZoomer;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    .line 728
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    new-instance v2, Lcom/android/org/chromium/content/browser/ContentViewCore$6;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$6;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    invoke-virtual {v1, v2}, Lcom/android/org/chromium/content/browser/PopupZoomer;->setOnVisibilityChangedListener(Lcom/android/org/chromium/content/browser/PopupZoomer$OnVisibilityChangedListener;)V

    .line 764
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$7;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$7;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 786
    .local v0, "listener":Lcom/android/org/chromium/content/browser/PopupZoomer$OnTapListener;
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/PopupZoomer;->setOnTapListener(Lcom/android/org/chromium/content/browser/PopupZoomer$OnTapListener;)V

    .line 787
    return-void
.end method

.method private isInsertionHandleShowing()Z
    .locals 1

    .prologue
    .line 2260
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSPenSupported(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1130
    sget-object v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->sIsSPenSupported:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1131
    invoke-static {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->detectSPenSupport(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->sIsSPenSupported:Ljava/lang/Boolean;

    .line 1132
    :cond_0
    sget-object v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->sIsSPenSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private isSelectionHandleShowing()Z
    .locals 1

    .prologue
    .line 2256
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeAddJavascriptInterface(JLjava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V
.end method

.method private native nativeAddStyleSheetByURL(JLjava/lang/String;)V
.end method

.method private native nativeCancelPendingReload(J)V
.end method

.method private native nativeClearHistory(J)V
.end method

.method private native nativeClearSslPreferences(J)V
.end method

.method private native nativeContinuePendingReload(J)V
.end method

.method private native nativeDoubleTap(JJFF)V
.end method

.method private native nativeEvaluateJavaScript(JLjava/lang/String;Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;Z)V
.end method

.method private native nativeExitFullscreen(J)V
.end method

.method private native nativeExtractSmartClipData(JIIII)V
.end method

.method private native nativeFlingCancel(JJ)V
.end method

.method private native nativeFlingStart(JJFFFF)V
.end method

.method private native nativeGetBackgroundColor(J)I
.end method

.method private native nativeGetCurrentRenderProcessId(J)I
.end method

.method private native nativeGetDirectedNavigationHistory(JLjava/lang/Object;ZI)V
.end method

.method private native nativeGetNativeImeAdapter(J)J
.end method

.method private native nativeGetNavigationHistory(JLjava/lang/Object;)I
.end method

.method private native nativeGetOriginalUrlForActiveNavigationEntry(J)Ljava/lang/String;
.end method

.method private native nativeGetURL(J)Ljava/lang/String;
.end method

.method private native nativeGetUseDesktopUserAgent(J)Z
.end method

.method private native nativeGetWebContentsAndroid(J)Lcom/android/org/chromium/content_public/browser/WebContents;
.end method

.method private native nativeInit(JJJLjava/util/HashSet;)J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;)J"
        }
    .end annotation
.end method

.method private native nativeIsIncognito(J)Z
.end method

.method private native nativeIsRenderWidgetHostViewReady(J)Z
.end method

.method private native nativeIsShowingInterstitialPage(J)Z
.end method

.method private native nativeLoadIfNecessary(J)V
.end method

.method private native nativeLoadUrl(JLjava/lang/String;IILjava/lang/String;IILjava/lang/String;[BLjava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method private native nativeLongPress(JJFF)V
.end method

.method private native nativeMoveCaret(JFF)V
.end method

.method private native nativeOnHide(J)V
.end method

.method private native nativeOnJavaContentViewCoreDestroyed(J)V
.end method

.method private native nativeOnShow(J)V
.end method

.method private native nativeOnTouchEvent(JLandroid/view/MotionEvent;JIIIIFFFFIIFFFFIII)Z
.end method

.method private native nativePinchBegin(JJFF)V
.end method

.method private native nativePinchBy(JJFFF)V
.end method

.method private native nativePinchEnd(JJ)V
.end method

.method private native nativeReload(JZ)V
.end method

.method private native nativeReloadIgnoringCache(JZ)V
.end method

.method private native nativeRemoveJavascriptInterface(JLjava/lang/String;)V
.end method

.method private native nativeRequestRestoreLoad(J)V
.end method

.method private native nativeResetGestureDetection(J)V
.end method

.method private native nativeScrollBegin(JJFFFF)V
.end method

.method private native nativeScrollBy(JJFFFF)V
.end method

.method private native nativeScrollEnd(JJ)V
.end method

.method private native nativeScrollFocusedEditableNodeIntoView(J)V
.end method

.method private native nativeSelectBetweenCoordinates(JFFFF)V
.end method

.method private native nativeSelectPopupMenuItems(J[I)V
.end method

.method private native nativeSelectWordAroundCaret(J)V
.end method

.method private native nativeSendMouseMoveEvent(JJFF)I
.end method

.method private native nativeSendMouseWheelEvent(JJFFF)I
.end method

.method private native nativeSendOrientationChangeEvent(JI)V
.end method

.method private native nativeSetAccessibilityEnabled(JZ)V
.end method

.method private native nativeSetAllowJavascriptInterfacesInspection(JZ)V
.end method

.method private native nativeSetBackgroundOpaque(JZ)V
.end method

.method private native nativeSetDoubleTapSupportEnabled(JZ)V
.end method

.method private native nativeSetFocus(JZ)V
.end method

.method private native nativeSetMultiTouchZoomSupportEnabled(JZ)V
.end method

.method private native nativeSetUseDesktopUserAgent(JZZ)V
.end method

.method private native nativeShowImeIfNeeded(J)V
.end method

.method private native nativeShowInterstitialPage(JLjava/lang/String;J)V
.end method

.method private native nativeSingleTap(JJFF)V
.end method

.method private native nativeUpdateTopControlsState(JZZZ)V
.end method

.method private native nativeWasResized(J)V
.end method

.method private offerLongPressToEmbedder()Z
    .locals 1

    .prologue
    .line 3190
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->performLongClick()Z

    move-result v0

    return v0
.end method

.method private onBackgroundColorChanged(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onBackgroundColorChanged(I)V

    .line 867
    return-void
.end method

.method private onDoubleTapEventAck()V
    .locals 0

    .prologue
    .line 1317
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    .line 1318
    return-void
.end method

.method private static onEvaluateJavaScriptResult(Ljava/lang/String;Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V
    .locals 0
    .param p0, "jsonResult"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;

    .prologue
    .line 2585
    invoke-interface {p1, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;->handleJavaScriptResult(Ljava/lang/String;)V

    .line 2586
    return-void
.end method

.method private onFlingCancelEventAck()V
    .locals 1

    .prologue
    .line 1262
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    .line 1263
    return-void
.end method

.method private onFlingStartEventConsumed(II)V
    .locals 3
    .param p1, "vx"    # I
    .param p2, "vy"    # I

    .prologue
    .line 1239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 1240
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    .line 1241
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    .line 1242
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->rewind()V

    .line 1243
    :goto_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollExtent()I

    move-result v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onFlingStartGesture(IIII)V

    goto :goto_0

    .line 1247
    :cond_0
    return-void
.end method

.method private onFlingStartEventHadNoConsumer(II)V
    .locals 1
    .param p1, "vx"    # I
    .param p2, "vy"    # I

    .prologue
    .line 1252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 1253
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->rewind()V

    .line 1254
    :goto_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1255
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onUnhandledFlingStartEvent(II)V

    goto :goto_0

    .line 1257
    :cond_0
    return-void
.end method

.method private onNativeFlingStopped()V
    .locals 1

    .prologue
    .line 3227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 3228
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    if-gtz v0, :cond_0

    .line 3231
    :goto_0
    return-void

    .line 3229
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    .line 3230
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    goto :goto_0
.end method

.method private onPinchBeginEventAck()V
    .locals 1

    .prologue
    .line 1295
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    .line 1296
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    .line 1297
    return-void
.end method

.method private onPinchEndEventAck()V
    .locals 1

    .prologue
    .line 1302
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    .line 1303
    return-void
.end method

.method private onRenderProcessChange()V
    .locals 0

    .prologue
    .line 2600
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->attachImeAdapter()V

    .line 2601
    return-void
.end method

.method private onScrollBeginEventAck()V
    .locals 1

    .prologue
    .line 1268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 1269
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    .line 1270
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;->invokeZoomPicker()V

    .line 1271
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    .line 1272
    return-void
.end method

.method private onScrollEndEventAck()V
    .locals 1

    .prologue
    .line 1287
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    if-nez v0, :cond_0

    .line 1290
    :goto_0
    return-void

    .line 1288
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 1289
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    goto :goto_0
.end method

.method private onScrollUpdateGestureConsumed()V
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;->invokeZoomPicker()V

    .line 1278
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->rewind()V

    .line 1279
    :goto_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1280
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onScrollUpdateGestureConsumed()V

    goto :goto_0

    .line 1282
    :cond_0
    return-void
.end method

.method private onSelectionBoundsChanged(Landroid/graphics/Rect;ILandroid/graphics/Rect;IZ)V
    .locals 11
    .param p1, "anchorRectDip"    # Landroid/graphics/Rect;
    .param p2, "anchorDir"    # I
    .param p3, "focusRectDip"    # Landroid/graphics/Rect;
    .param p4, "focusDir"    # I
    .param p5, "isAnchorFirst"    # Z

    .prologue
    .line 2517
    iget v7, p1, Landroid/graphics/Rect;->left:I

    .line 2518
    .local v7, "x1":I
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 2519
    .local v9, "y1":I
    iget v8, p3, Landroid/graphics/Rect;->left:I

    .line 2520
    .local v8, "x2":I
    iget v10, p3, Landroid/graphics/Rect;->bottom:I

    .line 2522
    .local v10, "y2":I
    if-ne v7, v8, :cond_0

    if-ne v9, v10, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2524
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    if-eqz v0, :cond_1

    .line 2525
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->hide()V

    .line 2527
    :cond_1
    if-eqz p5, :cond_5

    .line 2528
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    int-to-float v1, v7

    int-to-float v4, v9

    invoke-virtual {v0, v1, v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->setLocalDip(FF)V

    .line 2529
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    int-to-float v1, v8

    int-to-float v4, v10

    invoke-virtual {v0, v1, v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->setLocalDip(FF)V

    .line 2535
    :goto_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->isShowing()Z

    move-result v6

    .line 2537
    .local v6, "wereSelectionHandlesShowing":Z
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0, p2, p4}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->onSelectionChanged(II)V

    .line 2538
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    .line 2539
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mHasSelection:Z

    .line 2541
    if-nez v6, :cond_2

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2544
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->performHapticFeedback(I)Z

    .line 2576
    .end local v6    # "wereSelectionHandlesShowing":Z
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2577
    :cond_3
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    invoke-interface {v0, v1}, Lcom/android/org/chromium/content/browser/PositionObserver;->addListener(Lcom/android/org/chromium/content/browser/PositionObserver$Listener;)V

    .line 2579
    :cond_4
    return-void

    .line 2531
    :cond_5
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    int-to-float v1, v8

    int-to-float v4, v10

    invoke-virtual {v0, v1, v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->setLocalDip(FF)V

    .line 2532
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    int-to-float v1, v7

    int-to-float v4, v9

    invoke-virtual {v0, v1, v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->setLocalDip(FF)V

    goto :goto_0

    .line 2548
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z

    .line 2549
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hideSelectActionBar()V

    .line 2550
    if-eqz v7, :cond_9

    if-eqz v9, :cond_9

    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    if-eqz v0, :cond_9

    .line 2552
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    if-eqz v0, :cond_7

    .line 2553
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->hide()V

    .line 2555
    :cond_7
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    int-to-float v1, v7

    int-to-float v4, v9

    invoke-virtual {v0, v1, v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->setLocalDip(FF)V

    .line 2557
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->onCursorPositionChanged()V

    .line 2558
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    .line 2559
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->isWatchingCursor(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2560
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getXPix()F

    move-result v0

    float-to-int v2, v0

    .line 2561
    .local v2, "xPix":I
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getYPix()F

    move-result v0

    float-to-int v3, v0

    .line 2562
    .local v3, "yPix":I
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    move v4, v2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->updateCursor(Landroid/view/View;IIII)V

    .line 2574
    .end local v2    # "xPix":I
    .end local v3    # "yPix":I
    :cond_8
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mHasSelection:Z

    goto :goto_1

    .line 2567
    :cond_9
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    if-eqz v0, :cond_a

    .line 2568
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->hideAndDisallowAutomaticShowing()V

    .line 2570
    :cond_a
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    if-eqz v0, :cond_8

    .line 2571
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->hideAndDisallowAutomaticShowing()V

    goto :goto_2
.end method

.method private onSelectionChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2502
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mLastSelectedText:Ljava/lang/String;

    .line 2503
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onSelectionChanged(Ljava/lang/String;)V

    .line 2504
    return-void
.end method

.method private onSingleTapEventAck(ZII)V
    .locals 1
    .param p1, "consumed"    # Z
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->rewind()V

    .line 1309
    :goto_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1310
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v0}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onSingleTap(ZII)V

    goto :goto_0

    .line 1312
    :cond_0
    return-void
.end method

.method private onSmartClipDataExtracted(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "html"    # Ljava/lang/String;
    .param p3, "clipRect"    # Landroid/graphics/Rect;

    .prologue
    .line 3169
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSmartClipDataListener:Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;

    if-eqz v0, :cond_0

    .line 3170
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSmartClipDataListener:Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;->onSmartClipDataExtracted(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)V

    .line 3172
    :cond_0
    return-void
.end method

.method private resetGestureDetection()V
    .locals 4

    .prologue
    .line 1502
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1504
    :goto_0
    return-void

    .line 1503
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeResetGestureDetection(J)V

    goto :goto_0
.end method

.method private resetScrollInProgress()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3198
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isScrollInProgress()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3208
    :cond_0
    :goto_0
    return-void

    .line 3200
    :cond_1
    iget-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 3201
    .local v1, "touchScrollInProgress":Z
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    .line 3203
    .local v0, "potentiallyActiveFlingCount":I
    iput-boolean v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    .line 3204
    iput v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    .line 3206
    if-eqz v1, :cond_2

    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    .line 3207
    :cond_2
    if-lez v0, :cond_0

    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateGestureStateListener(I)V

    goto :goto_0
.end method

.method private scheduleTextHandleFadeIn()V
    .locals 4

    .prologue
    .line 2285
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2308
    :goto_0
    return-void

    .line 2287
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 2288
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$12;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$12;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    .line 2306
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2307
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mDeferredHandleFadeInRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private scrollFocusedEditableNodeIntoView()V
    .locals 4

    .prologue
    .line 1665
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1670
    :goto_0
    return-void

    .line 1669
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeScrollFocusedEditableNodeIntoView(J)V

    goto :goto_0
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 2437
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onUpdateTitle(Ljava/lang/String;)V

    .line 2438
    return-void
.end method

.method private shouldBlockMediaRequest(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 3220
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->shouldBlockMediaRequest(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private showDisambiguationPopup(Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "targetRect"    # Landroid/graphics/Rect;
    .param p2, "zoomedBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2488
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, p2}, Lcom/android/org/chromium/content/browser/PopupZoomer;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2489
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/PopupZoomer;->show(Landroid/graphics/Rect;)V

    .line 2490
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    .line 2491
    return-void
.end method

.method private showPastePopup(II)V
    .locals 3
    .param p1, "xDip"    # I
    .param p2, "yDip"    # I

    .prologue
    .line 2591
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->setLocalDip(FF)V

    .line 2592
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showHandle()V

    .line 2593
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    .line 2594
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showHandleWithPastePopup()V

    .line 2595
    return-void
.end method

.method private showSelectActionBar()V
    .locals 6

    .prologue
    .line 2112
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-eqz v1, :cond_0

    .line 2113
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v1}, Landroid/view/ActionMode;->invalidate()V

    .line 2231
    :goto_0
    return-void

    .line 2118
    :cond_0
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$11;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$11;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 2217
    .local v0, "actionHandler":Lcom/android/org/chromium/content/browser/SelectActionModeCallback$ActionHandler;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    .line 2219
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2220
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v4, v5}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeIsIncognito(J)Z

    move-result v4

    invoke-virtual {v2, v3, v0, v4}, Lcom/android/org/chromium/content/browser/ContentViewClient;->getSelectActionModeCallback(Landroid/content/Context;Lcom/android/org/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Landroid/view/ActionMode$Callback;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    .line 2224
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z

    .line 2225
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-nez v1, :cond_2

    .line 2227
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->unselect()Z

    goto :goto_0

    .line 2229
    :cond_2
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onContextualActionBarShown()V

    goto :goto_0
.end method

.method private showSelectPopup(Landroid/graphics/Rect;[Ljava/lang/String;[IZ[I)V
    .locals 5
    .param p1, "bounds"    # Landroid/graphics/Rect;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "enabled"    # [I
    .param p4, "multiple"    # Z
    .param p5, "selectedIndices"    # [I

    .prologue
    .line 2451
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 2452
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->selectPopupMenuItems([I)V

    .line 2468
    :goto_0
    return-void

    .line 2456
    :cond_1
    sget-boolean v2, Lcom/android/org/chromium/content/browser/ContentViewCore;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    array-length v2, p2

    array-length v3, p3

    if-eq v2, v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 2457
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2458
    .local v1, "popupItems":Ljava/util/List;, "Ljava/util/List<Lcom/android/org/chromium/content/browser/input/SelectPopupItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_3

    .line 2459
    new-instance v2, Lcom/android/org/chromium/content/browser/input/SelectPopupItem;

    aget-object v3, p2, v0

    aget v4, p3, v0

    invoke-direct {v2, v3, v4}, Lcom/android/org/chromium/content/browser/input/SelectPopupItem;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2458
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2461
    :cond_3
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hidePopups()V

    .line 2462
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/org/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez p4, :cond_4

    .line 2463
    new-instance v2, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    invoke-direct {v2, p0, v1, p1, p5}, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Ljava/util/List;Landroid/graphics/Rect;[I)V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

    .line 2467
    :goto_2
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

    invoke-interface {v2}, Lcom/android/org/chromium/content/browser/input/SelectPopup;->show()V

    goto :goto_0

    .line 2465
    :cond_4
    new-instance v2, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;

    invoke-direct {v2, p0, v1, p4, p5}, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Ljava/util/List;Z[I)V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

    goto :goto_2
.end method

.method private showSelectionHandlesAutomatically()V
    .locals 1

    .prologue
    .line 2509
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->allowAutomaticShowing()V

    .line 2510
    return-void
.end method

.method private startContentIntent(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentUrl"    # Ljava/lang/String;

    .prologue
    .line 2840
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onStartContentIntent(Landroid/content/Context;Ljava/lang/String;)V

    .line 2841
    return-void
.end method

.method private temporarilyHideTextHandles()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2266
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->isDragging()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2267
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->setHandleVisibility(I)V

    .line 2269
    :cond_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->isDragging()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2270
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->setHandleVisibility(I)V

    .line 2272
    :cond_1
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V

    .line 2273
    return-void
.end method

.method private unregisterAccessibilityContentObserver()V
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityScriptInjectionObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 817
    :goto_0
    return-void

    .line 814
    :cond_0
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityScriptInjectionObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 816
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityScriptInjectionObserver:Landroid/database/ContentObserver;

    goto :goto_0
.end method

.method private updateAfterSizeChanged()V
    .locals 3

    .prologue
    .line 1641
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/org/chromium/content/browser/PopupZoomer;->hide(Z)V

    .line 1645
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1646
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1647
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1648
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1650
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1651
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->scrollFocusedEditableNodeIntoView()V

    .line 1653
    :cond_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->cancelRequestToScrollFocusedEditableNodeIntoView()V

    .line 1656
    .end local v0    # "rect":Landroid/graphics/Rect;
    :cond_1
    return-void
.end method

.method private updateForTapOrPress(IFF)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "xPix"    # F
    .param p3, "yPix"    # F

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x5

    .line 1925
    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    if-eq p1, v1, :cond_1

    if-eq p1, v2, :cond_1

    .line 1949
    :cond_0
    :goto_0
    return-void

    .line 1932
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocusableInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1934
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 1937
    :cond_2
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/PopupZoomer;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, p2, p3}, Lcom/android/org/chromium/content/browser/PopupZoomer;->setLastTouch(FF)V

    .line 1939
    :cond_3
    float-to-int v0, p2

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mLastTapX:I

    .line 1940
    float-to-int v0, p3

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mLastTapY:I

    .line 1942
    if-eq p1, v1, :cond_4

    if-ne p1, v2, :cond_5

    .line 1944
    :cond_4
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->allowAutomaticShowing()V

    .line 1945
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->allowAutomaticShowing()V

    goto :goto_0

    .line 1947
    :cond_5
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->allowAutomaticShowing()V

    goto :goto_0
.end method

.method private updateFrameInfo(FFFFFFFFFFFF)V
    .locals 23
    .param p1, "scrollOffsetX"    # F
    .param p2, "scrollOffsetY"    # F
    .param p3, "pageScaleFactor"    # F
    .param p4, "minPageScaleFactor"    # F
    .param p5, "maxPageScaleFactor"    # F
    .param p6, "contentWidth"    # F
    .param p7, "contentHeight"    # F
    .param p8, "viewportWidth"    # F
    .param p9, "viewportHeight"    # F
    .param p10, "controlsOffsetYCss"    # F
    .param p11, "contentOffsetYCss"    # F
    .param p12, "overdrawBottomHeightCss"    # F

    .prologue
    .line 2344
    const-string v1, "ContentViewCore:updateFrameInfo"

    invoke-static {v1}, Lcom/android/org/chromium/base/TraceEvent;->instant(Ljava/lang/String;)V

    .line 2347
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getDeviceScaleFactor()F

    move-result v15

    .line 2348
    .local v15, "deviceScale":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportWidthPix:I

    int-to-float v1, v1

    mul-float v2, v15, p3

    div-float/2addr v1, v2

    move/from16 v0, p6

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p6

    .line 2350
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportHeightPix:I

    int-to-float v1, v1

    mul-float v2, v15, p3

    div-float/2addr v1, v2

    move/from16 v0, p7

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p7

    .line 2352
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move/from16 v0, p11

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->fromDipToPix(F)F

    move-result v11

    .line 2354
    .local v11, "contentOffsetYPix":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getContentWidthCss()F

    move-result v1

    cmpl-float v1, p6, v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getContentHeightCss()F

    move-result v1

    cmpl-float v1, p7, v1

    if-eqz v1, :cond_8

    :cond_0
    const/4 v13, 0x1

    .line 2357
    .local v13, "contentSizeChanged":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getMinPageScaleFactor()F

    move-result v1

    cmpl-float v1, p4, v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getMaxPageScaleFactor()F

    move-result v1

    cmpl-float v1, p5, v1

    if-eqz v1, :cond_9

    :cond_1
    const/16 v21, 0x1

    .line 2360
    .local v21, "scaleLimitsChanged":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getPageScaleFactor()F

    move-result v1

    cmpl-float v1, p3, v1

    if-eqz v1, :cond_a

    const/16 v20, 0x1

    .line 2362
    .local v20, "pageScaleChanged":Z
    :goto_2
    if-nez v20, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollX()F

    move-result v1

    cmpl-float v1, p1, v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollY()F

    move-result v1

    cmpl-float v1, p2, v1

    if-eqz v1, :cond_b

    :cond_2
    const/16 v22, 0x1

    .line 2366
    .local v22, "scrollChanged":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getContentOffsetYPix()F

    move-result v1

    cmpl-float v1, v11, v1

    if-eqz v1, :cond_c

    const/4 v12, 0x1

    .line 2369
    .local v12, "contentOffsetChanged":Z
    :goto_4
    if-nez v13, :cond_3

    if-eqz v22, :cond_d

    :cond_3
    const/16 v16, 0x1

    .line 2370
    .local v16, "needHidePopupZoomer":Z
    :goto_5
    if-nez v21, :cond_4

    if-eqz v22, :cond_e

    :cond_4
    const/16 v18, 0x1

    .line 2371
    .local v18, "needUpdateZoomControls":Z
    :goto_6
    move/from16 v17, v22

    .line 2373
    .local v17, "needTemporarilyHideHandles":Z
    if-eqz v16, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/org/chromium/content/browser/PopupZoomer;->hide(Z)V

    .line 2375
    :cond_5
    if-eqz v22, :cond_6

    .line 2376
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->fromLocalCssToPix(F)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->fromLocalCssToPix(F)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollXPix()F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v5}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollYPix()F

    move-result v5

    float-to-int v5, v5

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->onScrollChanged(IIII)V

    .line 2383
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-virtual/range {v1 .. v11}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->updateFrameInfo(FFFFFFFFFF)V

    .line 2390
    if-nez v22, :cond_7

    if-eqz v12, :cond_f

    .line 2391
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v1}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->rewind()V

    .line 2392
    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v1}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2393
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v1}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollExtent()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onScrollOffsetOrExtentChanged(II)V

    goto :goto_7

    .line 2354
    .end local v12    # "contentOffsetChanged":Z
    .end local v13    # "contentSizeChanged":Z
    .end local v16    # "needHidePopupZoomer":Z
    .end local v17    # "needTemporarilyHideHandles":Z
    .end local v18    # "needUpdateZoomControls":Z
    .end local v20    # "pageScaleChanged":Z
    .end local v21    # "scaleLimitsChanged":Z
    .end local v22    # "scrollChanged":Z
    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 2357
    .restart local v13    # "contentSizeChanged":Z
    :cond_9
    const/16 v21, 0x0

    goto/16 :goto_1

    .line 2360
    .restart local v21    # "scaleLimitsChanged":Z
    :cond_a
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 2362
    .restart local v20    # "pageScaleChanged":Z
    :cond_b
    const/16 v22, 0x0

    goto/16 :goto_3

    .line 2366
    .restart local v22    # "scrollChanged":Z
    :cond_c
    const/4 v12, 0x0

    goto/16 :goto_4

    .line 2369
    .restart local v12    # "contentOffsetChanged":Z
    :cond_d
    const/16 v16, 0x0

    goto/16 :goto_5

    .line 2370
    .restart local v16    # "needHidePopupZoomer":Z
    :cond_e
    const/16 v18, 0x0

    goto/16 :goto_6

    .line 2399
    .restart local v17    # "needTemporarilyHideHandles":Z
    .restart local v18    # "needUpdateZoomControls":Z
    :cond_f
    if-eqz v17, :cond_10

    invoke-direct/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V

    .line 2400
    :cond_10
    if-eqz v18, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;->updateZoomControls()V

    .line 2401
    :cond_11
    if-eqz v12, :cond_12

    invoke-direct/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateHandleScreenPositions()V

    .line 2404
    :cond_12
    mul-float v14, p10, v15

    .line 2405
    .local v14, "controlsOffsetPix":F
    mul-float v19, p12, v15

    .line 2406
    .local v19, "overdrawBottomHeightPix":F
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v1

    move/from16 v0, v19

    invoke-virtual {v1, v14, v11, v0}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onOffsetsForFullscreenChanged(FFF)V

    .line 2409
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    if-eqz v1, :cond_13

    .line 2410
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;->notifyFrameInfoInitialized()V

    .line 2412
    :cond_13
    return-void
.end method

.method private updateHandleScreenPositions()V
    .locals 3

    .prologue
    .line 2088
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2089
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getXPix()F

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mStartHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getYPix()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->setStartHandlePosition(FF)V

    .line 2091
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getXPix()F

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEndHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getYPix()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->setEndHandlePosition(FF)V

    .line 2095
    :cond_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2096
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getXPix()F

    move-result v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandlePoint:Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/RenderCoordinates$NormalizedPoint;->getYPix()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->setHandlePosition(FF)V

    .line 2099
    :cond_1
    return-void
.end method

.method private updateImeAdapter(JILjava/lang/String;IIIIZZ)V
    .locals 9
    .param p1, "nativeImeAdapterAndroid"    # J
    .param p3, "textInputType"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "selectionStart"    # I
    .param p6, "selectionEnd"    # I
    .param p7, "compositionStart"    # I
    .param p8, "compositionEnd"    # I
    .param p9, "showImeIfNeeded"    # Z
    .param p10, "isNonImeChange"    # Z

    .prologue
    .line 2419
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 2420
    invoke-static {}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputTypeNone()I

    move-result v1

    if-eq p3, v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z

    .line 2422
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    move/from16 v0, p9

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->updateKeyboardVisibility(JIZ)V

    .line 2425
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    if-eqz v1, :cond_0

    .line 2426
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    move-object v2, p4

    move v3, p5

    move v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p10

    invoke-virtual/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateState(Ljava/lang/String;IIIIZ)V

    .line 2430
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v1}, Landroid/view/ActionMode;->invalidate()V

    .line 2431
    :cond_1
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 2432
    return-void

    .line 2420
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addGestureStateListener(Lcom/android/org/chromium/content_public/browser/GestureStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    .prologue
    .line 1361
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListeners:Lcom/android/org/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 1362
    return-void
.end method

.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 2754
    const-class v0, Lcom/android/org/chromium/content/browser/JavascriptInterface;

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V

    .line 2755
    return-void
.end method

.method public addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 7
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2801
    .local p3, "requiredAnnotation":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2802
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2803
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeAddJavascriptInterface(JLjava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V

    .line 2805
    :cond_0
    return-void
.end method

.method public attachImeAdapter()V
    .locals 4

    .prologue
    .line 2607
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2608
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeGetNativeImeAdapter(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->attach(J)V

    .line 2610
    :cond_0
    return-void
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/NavigationController;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canGoForward()Z
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/NavigationController;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canGoToOffset(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 1018
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/org/chromium/content_public/browser/NavigationController;->canGoToOffset(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelPendingReload()V
    .locals 4

    .prologue
    .line 1090
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeCancelPendingReload(J)V

    .line 1091
    :cond_0
    return-void
.end method

.method public clearHistory()V
    .locals 4

    .prologue
    .line 1105
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeClearHistory(J)V

    .line 1106
    :cond_0
    return-void
.end method

.method public clearSslPreferences()V
    .locals 4

    .prologue
    .line 2252
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeClearSslPreferences(J)V

    .line 2253
    :cond_0
    return-void
.end method

.method public computeVerticalScrollExtent()I
    .locals 1

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getLastFrameViewportHeightPixInt()I

    move-result v0

    return v0
.end method

.method public computeVerticalScrollOffset()I
    .locals 1

    .prologue
    .line 1896
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollYPixInt()I

    move-result v0

    return v0
.end method

.method public continuePendingReload()V
    .locals 4

    .prologue
    .line 1097
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeContinuePendingReload(J)V

    .line 1098
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 796
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 797
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeOnJavaContentViewCoreDestroyed(J)V

    .line 799
    :cond_0
    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    .line 800
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

    invoke-virtual {v0}, Lcom/android/org/chromium/ui/base/ViewAndroid;->destroy()V

    .line 801
    :cond_1
    iput-wide v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    .line 802
    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentSettings:Lcom/android/org/chromium/content/browser/ContentSettings;

    .line 803
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 804
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRetainedJavaScriptObjects:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 805
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->unregisterAccessibilityContentObserver()V

    .line 806
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListeners:Lcom/android/org/chromium/base/ObserverList;

    invoke-virtual {v0}, Lcom/android/org/chromium/base/ObserverList;->clear()V

    .line 807
    invoke-static {}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->getInstance()Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->removeObserver(Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationObserver;)V

    .line 808
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 1723
    invoke-static {p1}, Lcom/android/org/chromium/content/browser/input/GamepadList;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1730
    :cond_0
    :goto_0
    return v0

    .line 1724
    :cond_1
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->shouldOverrideKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1725
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1728
    :cond_2
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1730
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public evaluateJavaScript(Ljava/lang/String;Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;)V
    .locals 7
    .param p1, "script"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;

    .prologue
    .line 1432
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1434
    :goto_0
    return-void

    .line 1433
    :cond_0
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const/4 v6, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeEvaluateJavaScript(JLjava/lang/String;Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;Z)V

    goto :goto_0
.end method

.method public evaluateJavaScriptEvenIfNotYetNavigated(Ljava/lang/String;)V
    .locals 7
    .param p1, "script"    # Ljava/lang/String;

    .prologue
    .line 1443
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1445
    :goto_0
    return-void

    .line 1444
    :cond_0
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeEvaluateJavaScript(JLjava/lang/String;Lcom/android/org/chromium/content/browser/ContentViewCore$JavaScriptCallback;Z)V

    goto :goto_0
.end method

.method public extractSmartClipData(IIII)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 3146
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 3147
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSmartClipOffsetX:I

    add-int/2addr p1, v0

    .line 3148
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSmartClipOffsetY:I

    add-int/2addr p2, v0

    .line 3149
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeExtractSmartClipData(JIIII)V

    .line 3151
    :cond_0
    return-void
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2903
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    if-eqz v0, :cond_0

    .line 2904
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    .line 2915
    :goto_0
    return-object v0

    .line 2907
    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeAccessibilityAllowed:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeAccessibilityEnabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 2911
    iput-boolean v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeAccessibilityEnabled:Z

    .line 2912
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, v4}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSetAccessibilityEnabled(JZ)V

    .line 2915
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBrowserAccessibilityManager()Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;
    .locals 1

    .prologue
    .line 2892
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    return-object v0
.end method

.method public getContainerView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getContentSettings()Lcom/android/org/chromium/content/browser/ContentSettings;
    .locals 1

    .prologue
    .line 1481
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentSettings:Lcom/android/org/chromium/content/browser/ContentSettings;

    return-object v0
.end method

.method public getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentViewClient:Lcom/android/org/chromium/content/browser/ContentViewClient;

    if-nez v0, :cond_0

    .line 849
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewClient;

    invoke-direct {v0}, Lcom/android/org/chromium/content/browser/ContentViewClient;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentViewClient:Lcom/android/org/chromium/content/browser/ContentViewClient;

    .line 854
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentViewClient:Lcom/android/org/chromium/content/browser/ContentViewClient;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method getDownloadDelegate()Lcom/android/org/chromium/content/browser/ContentViewDownloadDelegate;
    .locals 1

    .prologue
    .line 2008
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mDownloadDelegate:Lcom/android/org/chromium/content/browser/ContentViewDownloadDelegate;

    return-object v0
.end method

.method public getJavascriptInterfaces()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Class;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2742
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    return-object v0
.end method

.method public getNativeContentViewCore()J
    .locals 2

    .prologue
    .line 833
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    return-wide v0
.end method

.method public getNavigationHistory()Lcom/android/org/chromium/content/browser/NavigationHistory;
    .locals 6

    .prologue
    .line 3104
    new-instance v1, Lcom/android/org/chromium/content/browser/NavigationHistory;

    invoke-direct {v1}, Lcom/android/org/chromium/content/browser/NavigationHistory;-><init>()V

    .line 3105
    .local v1, "history":Lcom/android/org/chromium/content/browser/NavigationHistory;
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 3106
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v2, v3, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeGetNavigationHistory(JLjava/lang/Object;)I

    move-result v0

    .line 3107
    .local v0, "currentIndex":I
    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/NavigationHistory;->setCurrentEntryIndex(I)V

    .line 3109
    .end local v0    # "currentIndex":I
    :cond_0
    return-object v1
.end method

.method public getOverdrawBottomHeightPix()I
    .locals 1

    .prologue
    .line 969
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mOverdrawBottomHeightPix:I

    return v0
.end method

.method public getPhysicalBackingHeightPix()I
    .locals 1

    .prologue
    .line 963
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPhysicalBackingHeightPix:I

    return v0
.end method

.method public getPhysicalBackingWidthPix()I
    .locals 1

    .prologue
    .line 957
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPhysicalBackingWidthPix:I

    return v0
.end method

.method public getRenderCoordinates()Lcom/android/org/chromium/content/browser/RenderCoordinates;
    .locals 1

    .prologue
    .line 3137
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    return-object v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1112
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mHasSelection:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mLastSelectedText:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 907
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeGetURL(J)Ljava/lang/String;

    move-result-object v0

    .line 908
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewAndroidDelegate()Lcom/android/org/chromium/ui/base/ViewAndroidDelegate;
    .locals 1

    .prologue
    .line 444
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$1;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$1;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    return-object v0
.end method

.method public getViewportHeightPix()I
    .locals 1

    .prologue
    .line 951
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportHeightPix:I

    return v0
.end method

.method public getViewportSizeOffsetHeightPix()I
    .locals 1

    .prologue
    .line 981
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportSizeOffsetHeightPix:I

    return v0
.end method

.method public getViewportSizeOffsetWidthPix()I
    .locals 1

    .prologue
    .line 975
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportSizeOffsetWidthPix:I

    return v0
.end method

.method public getViewportWidthPix()I
    .locals 1

    .prologue
    .line 945
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportWidthPix:I

    return v0
.end method

.method public getWebContents()Lcom/android/org/chromium/content_public/browser/WebContents;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/NavigationController;->goBack()V

    .line 1043
    :cond_0
    return-void
.end method

.method public goForward()V
    .locals 1

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/NavigationController;->goForward()V

    .line 1050
    :cond_0
    return-void
.end method

.method public goToOffset(I)V
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/org/chromium/content_public/browser/NavigationController;->goToOffset(I)V

    .line 1029
    :cond_0
    return-void
.end method

.method public hideImeIfNeeded()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2328
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->isActive(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2329
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 2332
    :cond_0
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onImeStateChangeRequested(Z)V

    .line 2333
    return-void
.end method

.method public hideSelectActionBar()V
    .locals 1

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 1492
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1493
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;

    .line 1495
    :cond_0
    return-void
.end method

.method public initialize(Landroid/view/ViewGroup;Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLcom/android/org/chromium/ui/base/WindowAndroid;)V
    .locals 9
    .param p1, "containerView"    # Landroid/view/ViewGroup;
    .param p2, "internalDispatcher"    # Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;
    .param p3, "nativeWebContents"    # J
    .param p5, "windowAndroid"    # Lcom/android/org/chromium/ui/base/WindowAndroid;

    .prologue
    const-wide/16 v0, 0x0

    .line 608
    invoke-virtual {p0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setContainerView(Landroid/view/ViewGroup;)V

    .line 610
    new-instance v2, Lcom/android/org/chromium/content/browser/ContentViewCore$3;

    invoke-direct {v2, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$3;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    iput-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    .line 619
    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/android/org/chromium/ui/base/WindowAndroid;->getNativePointer()J

    move-result-wide v6

    .line 621
    .local v6, "windowNativePointer":J
    :goto_0
    const-wide/16 v4, 0x0

    .line 622
    .local v4, "viewAndroidNativePointer":J
    cmp-long v0, v6, v0

    if-eqz v0, :cond_0

    .line 623
    new-instance v0, Lcom/android/org/chromium/ui/base/ViewAndroid;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getViewAndroidDelegate()Lcom/android/org/chromium/ui/base/ViewAndroidDelegate;

    move-result-object v1

    invoke-direct {v0, p5, v1}, Lcom/android/org/chromium/ui/base/ViewAndroid;-><init>(Lcom/android/org/chromium/ui/base/WindowAndroid;Lcom/android/org/chromium/ui/base/ViewAndroidDelegate;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

    .line 624
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

    invoke-virtual {v0}, Lcom/android/org/chromium/ui/base/ViewAndroid;->getNativePointer()J

    move-result-wide v4

    .line 627
    :cond_0
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$4;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$4;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    .line 636
    iget-object v8, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRetainedJavaScriptObjects:Ljava/util/HashSet;

    move-object v1, p0

    move-wide v2, p3

    invoke-direct/range {v1 .. v8}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeInit(JJJLjava/util/HashSet;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    .line 639
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeGetWebContentsAndroid(J)Lcom/android/org/chromium/content_public/browser/WebContents;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    .line 640
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentSettings;

    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {v0, p0, v2, v3}, Lcom/android/org/chromium/content/browser/ContentSettings;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;J)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentSettings:Lcom/android/org/chromium/content/browser/ContentSettings;

    .line 642
    invoke-virtual {p0, p2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setContainerViewInternals(Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;)V

    .line 643
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->reset()V

    .line 644
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->initPopupZoomer(Landroid/content/Context;)V

    .line 645
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->createImeAdapter(Landroid/content/Context;)Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    .line 647
    invoke-static {p0}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->newInstance(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    .line 649
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$5;

    invoke-direct {v0, p0, p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$5;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContentsObserver:Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;

    .line 667
    return-void

    .end local v4    # "viewAndroidNativePointer":J
    .end local v6    # "windowNativePointer":J
    :cond_1
    move-wide v6, v0

    .line 619
    goto :goto_0
.end method

.method public invokeZoomPicker()V
    .locals 1

    .prologue
    .line 2719
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;->invokeZoomPicker()V

    .line 2720
    return-void
.end method

.method public isAlive()Z
    .locals 4

    .prologue
    .line 824
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeviceAccessibilityScriptInjectionEnabled()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2958
    :try_start_0
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_1

    invoke-static {}, Lcom/android/org/chromium/base/CommandLine;->getInstance()Lcom/android/org/chromium/base/CommandLine;

    move-result-object v7

    const-string v8, "enable-accessibility-script-injection"

    invoke-virtual {v7, v8}, Lcom/android/org/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2999
    :cond_0
    :goto_0
    return v6

    .line 2964
    :cond_1
    iget-object v7, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentSettings:Lcom/android/org/chromium/content/browser/ContentSettings;

    invoke-virtual {v7}, Lcom/android/org/chromium/content/browser/ContentSettings;->getJavaScriptEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2968
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "android.permission.INTERNET"

    invoke-virtual {v7, v8}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    .line 2970
    .local v4, "result":I
    if-nez v4, :cond_0

    .line 2974
    const-class v7, Landroid/provider/Settings$Secure;

    const-string v8, "ACCESSIBILITY_SCRIPT_INJECTION"

    invoke-virtual {v7, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 2975
    .local v3, "field":Ljava/lang/reflect/Field;
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2976
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2977
    .local v0, "accessibilityScriptInjection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2979
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    iget-object v7, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityScriptInjectionObserver:Landroid/database/ContentObserver;

    if-nez v7, :cond_2

    .line 2980
    new-instance v1, Lcom/android/org/chromium/content/browser/ContentViewCore$13;

    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v7}, Lcom/android/org/chromium/content/browser/ContentViewCore$13;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/os/Handler;)V

    .line 2986
    .local v1, "contentObserver":Landroid/database/ContentObserver;
    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2990
    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityScriptInjectionObserver:Landroid/database/ContentObserver;

    .line 2993
    .end local v1    # "contentObserver":Landroid/database/ContentObserver;
    :cond_2
    const/4 v7, 0x0

    invoke-static {v2, v0, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-ne v7, v5, :cond_3

    :goto_1
    move v6, v5

    goto :goto_0

    :cond_3
    move v5, v6

    goto :goto_1

    .line 2996
    .end local v0    # "accessibilityScriptInjection":Ljava/lang/String;
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    .end local v3    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "result":I
    :catch_0
    move-exception v5

    goto :goto_0

    .line 2994
    :catch_1
    move-exception v5

    goto :goto_0
.end method

.method public isScrollInProgress()Z
    .locals 1

    .prologue
    .line 1233
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchScrollInProgress:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPotentiallyActiveFlingCount:I

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadUrl(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V
    .locals 18
    .param p1, "params"    # Lcom/android/org/chromium/content/browser/LoadUrlParams;

    .prologue
    .line 877
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 892
    :goto_0
    return-void

    .line 879
    :cond_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v7, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mLoadUrlType:I

    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mTransitionType:I

    invoke-virtual/range {p1 .. p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getReferrer()Lcom/android/org/chromium/content_public/Referrer;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getReferrer()Lcom/android/org/chromium/content_public/Referrer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content_public/Referrer;->getUrl()Ljava/lang/String;

    move-result-object v9

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getReferrer()Lcom/android/org/chromium/content_public/Referrer;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getReferrer()Lcom/android/org/chromium/content_public/Referrer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/content_public/Referrer;->getPolicy()I

    move-result v10

    :goto_2
    move-object/from16 v0, p1

    iget v11, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mUaOverrideOption:I

    invoke-virtual/range {p1 .. p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->getExtraHeadersString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mPostData:[B

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mBaseUrlForDataUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mVirtualUrlForDataUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mCanLoadLocalResources:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/android/org/chromium/content/browser/LoadUrlParams;->mIsRendererInitiated:Z

    move/from16 v17, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v17}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeLoadUrl(JLjava/lang/String;IILjava/lang/String;IILjava/lang/String;[BLjava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    goto :goto_2
.end method

.method public onAccessibilityStateChanged(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 2845
    invoke-virtual {p0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    .line 2846
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    .line 1513
    invoke-static {}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->getInstance()Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0, v1}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->addObserver(Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationObserver;Landroid/content/Context;)V

    .line 1514
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/GamepadList;->onAttachedToWindow(Landroid/content/Context;)V

    .line 1515
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1578
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 1580
    iget v0, p1, Landroid/content/res/Configuration;->keyboard:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 1581
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1582
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v2, v3}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeGetNativeImeAdapter(J)J

    move-result-wide v2

    invoke-static {}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputTypeNone()I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->attach(JI)V

    .line 1585
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputMethodManagerWrapper:Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->restartInput(Landroid/view/View;)V

    .line 1587
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1591
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1592
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 1593
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 4
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->hasTextInputType()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1549
    const/high16 v0, 0x2000000

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 1551
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAdapterInputConnectionFactory:Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mEditable:Landroid/text/Editable;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter$AdapterInputConnectionFactory;->get(Landroid/view/View;Lcom/android/org/chromium/content/browser/input/ImeAdapter;Landroid/text/Editable;Landroid/view/inputmethod/EditorInfo;)Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    .line 1553
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 1523
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setInjectedAccessibility(Z)V

    .line 1524
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hidePopups()V

    .line 1525
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;->dismissZoomPicker()V

    .line 1526
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->unregisterAccessibilityContentObserver()V

    .line 1528
    invoke-static {}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->getInstance()Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->removeObserver(Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationObserver;)V

    .line 1529
    invoke-static {}, Lcom/android/org/chromium/content/browser/input/GamepadList;->onDetachedFromWindow()V

    .line 1530
    return-void
.end method

.method public onFocusChanged(Z)V
    .locals 4
    .param p1, "gainFocus"    # Z

    .prologue
    .line 1689
    if-nez p1, :cond_0

    .line 1690
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->hideImeIfNeeded()V

    .line 1691
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->cancelRequestToScrollFocusedEditableNodeIntoView()V

    .line 1693
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSetFocus(JZ)V

    .line 1694
    :cond_1
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x1

    .line 1769
    invoke-static {p1}, Lcom/android/org/chromium/content/browser/input/GamepadList;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v9

    .line 1794
    :goto_0
    return v1

    .line 1770
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1771
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1794
    :cond_1
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 1773
    :pswitch_0
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    .line 1775
    :cond_2
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v8

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSendMouseWheelEvent(JJFFF)I

    .line 1779
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1782
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1783
    .local v0, "eventFakeMouseMove":Landroid/view/MotionEvent;
    new-instance v1, Lcom/android/org/chromium/content/browser/ContentViewCore$8;

    invoke-direct {v1, p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$8;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/view/MotionEvent;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    .line 1790
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    move v1, v9

    .line 1791
    goto :goto_0

    .line 1771
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    .line 1740
    const-string v1, "onHoverEvent"

    invoke-static {v1}, Lcom/android/org/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 1741
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->createOffsetMotionEvent(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1743
    .local v0, "offset":Landroid/view/MotionEvent;
    :try_start_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    if-eqz v1, :cond_0

    .line 1744
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;->onHoverEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1760
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1761
    const-string v2, "onHoverEvent"

    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    :goto_0
    return v1

    .line 1749
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchExplorationEnabled:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    .line 1760
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1761
    const-string v1, "onHoverEvent"

    invoke-static {v1}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    move v1, v8

    goto :goto_0

    .line 1753
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mFakeMouseMoveRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1754
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 1755
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSendMouseMoveEvent(JJFF)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1760
    :cond_2
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1761
    const-string v1, "onHoverEvent"

    invoke-static {v1}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    move v1, v8

    goto :goto_0

    .line 1760
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1761
    const-string v2, "onHoverEvent"

    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    throw v1
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v3, 0x0

    .line 2931
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 2934
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollXPixInt()I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    .line 2935
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getScrollYPixInt()I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    .line 2939
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getMaxHorizontalScrollPixInt()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2940
    .local v1, "maxScrollXPix":I
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getMaxVerticalScrollPixInt()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2941
    .local v2, "maxScrollYPix":I
    if-gtz v1, :cond_0

    if-lez v2, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 2944
    const/16 v0, 0xf

    .line 2945
    .local v0, "SDK_VERSION_REQUIRED_TO_SET_SCROLL":I
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xf

    if-lt v3, v4, :cond_2

    .line 2946
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    .line 2947
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    .line 2949
    :cond_2
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 2923
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 2924
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 1700
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/PopupZoomer;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 1701
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPopupZoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/PopupZoomer;->hide(Z)V

    .line 1704
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    invoke-interface {v0, p1, p2}, Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;->super_onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method onNativeContentViewCoreDestroyed(J)V
    .locals 3
    .param p1, "nativeContentViewCore"    # J

    .prologue
    .line 713
    sget-boolean v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 714
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    .line 715
    return-void
.end method

.method public onPhysicalBackingSizeChanged(II)V
    .locals 4
    .param p1, "wPix"    # I
    .param p2, "hPix"    # I

    .prologue
    .line 1616
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPhysicalBackingWidthPix:I

    if-ne v0, p1, :cond_1

    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPhysicalBackingHeightPix:I

    if-ne v0, p2, :cond_1

    .line 1624
    :cond_0
    :goto_0
    return-void

    .line 1618
    :cond_1
    iput p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPhysicalBackingWidthPix:I

    .line 1619
    iput p2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPhysicalBackingHeightPix:I

    .line 1621
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1622
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeWasResized(J)V

    goto :goto_0
.end method

.method public onScreenOrientationChanged(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 3235
    invoke-virtual {p0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->sendOrientationChangeEvent(I)V

    .line 3236
    return-void
.end method

.method public onShow()V
    .locals 4

    .prologue
    .line 1451
    sget-boolean v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1452
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeOnShow(J)V

    .line 1453
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    .line 1454
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 4
    .param p1, "wPix"    # I
    .param p2, "hPix"    # I
    .param p3, "owPix"    # I
    .param p4, "ohPix"    # I

    .prologue
    .line 1600
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getViewportWidthPix()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getViewportHeightPix()I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 1609
    :goto_0
    return-void

    .line 1602
    :cond_0
    iput p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportWidthPix:I

    .line 1603
    iput p2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mViewportHeightPix:I

    .line 1604
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1605
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeWasResized(J)V

    .line 1608
    :cond_1
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->updateAfterSizeChanged()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 27
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1177
    const-string v2, "onTouchEvent"

    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 1179
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->cancelRequestToScrollFocusedEditableNodeIntoView()V

    .line 1181
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    .line 1183
    .local v8, "eventAction":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isSPenSupported(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1184
    invoke-static {v8}, Lcom/android/org/chromium/content/browser/ContentViewCore;->convertSPenEventAction(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 1189
    :cond_0
    if-eqz v8, :cond_1

    const/4 v2, 0x1

    if-eq v8, v2, :cond_1

    const/4 v2, 0x3

    if-eq v8, v2, :cond_1

    const/4 v2, 0x2

    if-eq v8, v2, :cond_1

    const/4 v2, 0x5

    if-eq v8, v2, :cond_1

    const/4 v2, 0x6

    if-eq v8, v2, :cond_1

    .line 1195
    const/16 v25, 0x0

    .line 1224
    const-string v2, "onTouchEvent"

    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    :goto_0
    return v25

    .line 1198
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    const/16 v25, 0x0

    .line 1224
    const-string v2, "onTouchEvent"

    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    goto :goto_0

    .line 1201
    :cond_2
    const/16 v26, 0x0

    .line 1202
    .local v26, "offset":Landroid/view/MotionEvent;
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mCurrentTouchOffsetX:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mCurrentTouchOffsetY:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    .line 1203
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->createOffsetMotionEvent(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v26

    .line 1204
    move-object/from16 p1, v26

    .line 1207
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    .line 1208
    .local v9, "pointerCount":I
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v13

    const/4 v2, 0x1

    if-le v9, v2, :cond_6

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v14

    :goto_1
    const/4 v2, 0x1

    if-le v9, v2, :cond_7

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v15

    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v16

    const/4 v2, 0x1

    if-le v9, v2, :cond_8

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v17

    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getTouchMajor()F

    move-result v18

    const/4 v2, 0x1

    if-le v9, v2, :cond_9

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    move-result v19

    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v20

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v21

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v22

    const/4 v2, 0x1

    if-le v9, v2, :cond_a

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v23

    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v24

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v24}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeOnTouchEvent(JLandroid/view/MotionEvent;JIIIIFFFFIIFFFFIII)Z

    move-result v25

    .line 1221
    .local v25, "consumed":Z
    if-eqz v26, :cond_5

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1224
    :cond_5
    const-string v2, "onTouchEvent"

    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1208
    .end local v25    # "consumed":Z
    :cond_6
    const/4 v14, 0x0

    goto :goto_1

    :cond_7
    const/4 v15, 0x0

    goto :goto_2

    :cond_8
    const/16 v17, -0x1

    goto :goto_3

    :cond_9
    const/16 v19, 0x0

    goto :goto_4

    :cond_a
    const/16 v23, 0x0

    goto :goto_5

    .line 1224
    .end local v8    # "eventAction":I
    .end local v9    # "pointerCount":I
    .end local v26    # "offset":Landroid/view/MotionEvent;
    :catchall_0
    move-exception v2

    const-string v3, "onTouchEvent"

    invoke-static {v3}, Lcom/android/org/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    throw v2
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 1685
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->resetGestureDetection()V

    .line 1686
    :cond_0
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "action"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 2868
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->supportsAccessibilityAction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2869
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    .line 2872
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pinchByDelta(F)Z
    .locals 10
    .param p1, "delta"    # F

    .prologue
    .line 2702
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 2712
    :goto_0
    return v1

    .line 2704
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2705
    .local v4, "timeMs":J
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getViewportWidthPix()I

    move-result v1

    div-int/lit8 v0, v1, 0x2

    .line 2706
    .local v0, "xPix":I
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getViewportHeightPix()I

    move-result v1

    div-int/lit8 v9, v1, 0x2

    .line 2708
    .local v9, "yPix":I
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    int-to-float v6, v0

    int-to-float v7, v9

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativePinchBegin(JJFF)V

    .line 2709
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    int-to-float v6, v0

    int-to-float v7, v9

    move-object v1, p0

    move v8, p1

    invoke-direct/range {v1 .. v8}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativePinchBy(JJFFF)V

    .line 2710
    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativePinchEnd(JJ)V

    .line 2712
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public reload(Z)V
    .locals 4
    .param p1, "checkForRepost"    # Z

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->addOrRemoveAccessibilityApisIfNecessary()V

    .line 1071
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1072
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeReload(JZ)V

    .line 1074
    :cond_0
    return-void
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 2813
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mJavaScriptInterfaces:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2814
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2815
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeRemoveJavascriptInterface(JLjava/lang/String;)V

    .line 2817
    :cond_0
    return-void
.end method

.method public selectPopupMenuItems([I)V
    .locals 4
    .param p1, "indices"    # [I

    .prologue
    .line 1980
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1981
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSelectPopupMenuItems(J[I)V

    .line 1983
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectPopup:Lcom/android/org/chromium/content/browser/input/SelectPopup;

    .line 1984
    return-void
.end method

.method sendOrientationChangeEvent(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    .line 1991
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1994
    :goto_0
    return-void

    .line 1993
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSendOrientationChangeEvent(JI)V

    goto :goto_0
.end method

.method public setAccessibilityState(Z)V
    .locals 2
    .param p1, "state"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3023
    if-nez p1, :cond_0

    .line 3024
    invoke-virtual {p0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setInjectedAccessibility(Z)V

    .line 3025
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeAccessibilityAllowed:Z

    .line 3026
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchExplorationEnabled:Z

    .line 3033
    :goto_0
    return-void

    .line 3028
    :cond_0
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->isDeviceAccessibilityScriptInjectionEnabled()Z

    move-result v0

    .line 3029
    .local v0, "useScriptInjection":Z
    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->setInjectedAccessibility(Z)V

    .line 3030
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeAccessibilityAllowed:Z

    .line 3031
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mTouchExplorationEnabled:Z

    goto :goto_0
.end method

.method public setAllowJavascriptInterfacesInspection(Z)V
    .locals 2
    .param p1, "allow"    # Z

    .prologue
    .line 2732
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSetAllowJavascriptInterfacesInspection(JZ)V

    .line 2733
    return-void
.end method

.method public setBrowserAccessibilityManager(Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    .prologue
    .line 2882
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mBrowserAccessibilityManager:Lcom/android/org/chromium/content/browser/accessibility/BrowserAccessibilityManager;

    .line 2883
    return-void
.end method

.method public setContainerView(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "containerView"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 688
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 689
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    invoke-interface {v1, v2}, Lcom/android/org/chromium/content/browser/PositionObserver;->removeListener(Lcom/android/org/chromium/content/browser/PositionObserver$Listener;)V

    .line 691
    iput-object v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lcom/android/org/chromium/content/browser/input/SelectionHandleController;

    .line 692
    iput-object v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    .line 693
    iput-object v3, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mInputConnection:Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;

    .line 696
    :cond_0
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    .line 697
    new-instance v1, Lcom/android/org/chromium/content/browser/ViewPositionObserver;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-direct {v1, v2}, Lcom/android/org/chromium/content/browser/ViewPositionObserver;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    .line 698
    const-string v0, "Web View"

    .line 699
    .local v0, "contentDescription":Ljava/lang/String;
    sget v1, Lcom/android/org/chromium/content/R$string;->accessibility_content_view:I

    if-nez v1, :cond_1

    .line 700
    const-string v1, "ContentViewCore"

    const-string v2, "Setting contentDescription to \'Web View\' as no value was specified."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :goto_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 706
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    .line 707
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 708
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 709
    return-void

    .line 702
    :cond_1
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/org/chromium/content/R$string;->accessibility_content_view:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setContainerViewInternals(Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;)V
    .locals 0
    .param p1, "internalDispatcher"    # Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    .prologue
    .line 723
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerViewInternals:Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;

    .line 724
    return-void
.end method

.method public setContentViewClient(Lcom/android/org/chromium/content/browser/ContentViewClient;)V
    .locals 2
    .param p1, "client"    # Lcom/android/org/chromium/content/browser/ContentViewClient;

    .prologue
    .line 837
    if-nez p1, :cond_0

    .line 838
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The client can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 840
    :cond_0
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mContentViewClient:Lcom/android/org/chromium/content/browser/ContentViewClient;

    .line 841
    return-void
.end method

.method public setInjectedAccessibility(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 3039
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->addOrRemoveAccessibilityApisIfNecessary()V

    .line 3040
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->setScriptEnabled(Z)V

    .line 3041
    return-void
.end method

.method public setSmartClipDataListener(Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;

    .prologue
    .line 3175
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mSmartClipDataListener:Lcom/android/org/chromium/content/browser/ContentViewCore$SmartClipDataListener;

    .line 3176
    return-void
.end method

.method public setZoomControlsDelegate(Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;)V
    .locals 0
    .param p1, "zoomControlsDelegate"    # Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    .prologue
    .line 1966
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mZoomControlsDelegate:Lcom/android/org/chromium/content/browser/ContentViewCore$ZoomControlsDelegate;

    .line 1967
    return-void
.end method

.method public shouldSetAccessibilityFocusOnPageLoad()Z
    .locals 1

    .prologue
    .line 3054
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mShouldSetAccessibilityFocusOnPageLoad:Z

    return v0
.end method

.method public showImeIfNeeded()V
    .locals 4

    .prologue
    .line 2314
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeShowImeIfNeeded(J)V

    .line 2315
    :cond_0
    return-void
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 898
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mWebContents:Lcom/android/org/chromium/content_public/browser/WebContents;

    invoke-interface {v0}, Lcom/android/org/chromium/content_public/browser/WebContents;->stop()V

    .line 899
    :cond_0
    return-void
.end method

.method public supportsAccessibilityAction(I)Z
    .locals 1
    .param p1, "action"    # I

    .prologue
    .line 2854
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mAccessibilityInjector:Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/accessibility/AccessibilityInjector;->supportsAccessibilityAction(I)Z

    move-result v0

    return v0
.end method

.method public updateDoubleTapSupport(Z)V
    .locals 4
    .param p1, "supportsDoubleTap"    # Z

    .prologue
    .line 1975
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1977
    :goto_0
    return-void

    .line 1976
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSetDoubleTapSupportEnabled(JZ)V

    goto :goto_0
.end method

.method updateGestureStateListener(I)V
    .locals 3
    .param p1, "gestureType"    # I

    .prologue
    .line 1373
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v1}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->rewind()V

    .line 1374
    :goto_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v1}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1375
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mGestureStateListenersIterator:Lcom/android/org/chromium/base/ObserverList$RewindableIterator;

    invoke-interface {v1}, Lcom/android/org/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/org/chromium/content_public/browser/GestureStateListener;

    .line 1376
    .local v0, "listener":Lcom/android/org/chromium/content_public/browser/GestureStateListener;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1392
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollExtent()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onScrollStarted(II)V

    goto :goto_0

    .line 1378
    :pswitch_2
    invoke-virtual {v0}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onPinchStarted()V

    goto :goto_0

    .line 1381
    :pswitch_3
    invoke-virtual {v0}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onPinchEnded()V

    goto :goto_0

    .line 1384
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollExtent()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onFlingEndGesture(II)V

    goto :goto_0

    .line 1389
    :pswitch_5
    invoke-virtual {v0}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onFlingCancelGesture()V

    goto :goto_0

    .line 1397
    :pswitch_6
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->computeVerticalScrollExtent()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;->onScrollEnded(II)V

    goto :goto_0

    .line 1405
    .end local v0    # "listener":Lcom/android/org/chromium/content_public/browser/GestureStateListener;
    :cond_0
    return-void

    .line 1376
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public updateMultiTouchZoomSupport(Z)V
    .locals 4
    .param p1, "supportsMultiTouchZoom"    # Z

    .prologue
    .line 1970
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1972
    :goto_0
    return-void

    .line 1971
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSetMultiTouchZoomSupportEnabled(JZ)V

    goto :goto_0
.end method
