.class Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;
.super Ljava/lang/Object;
.source "BindingManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/BindingManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ManagedConnection"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

.field private mInForeground:Z

.field private mWasOomProtected:Z

.field final synthetic this$0:Lcom/android/org/chromium/content/browser/BindingManagerImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/org/chromium/content/browser/BindingManagerImpl;Lcom/android/org/chromium/content/browser/ChildProcessConnection;)V
    .locals 0
    .param p2, "connection"    # Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->this$0:Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .line 117
    return-void
.end method

.method static synthetic access$300(Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->dropBindings()V

    return-void
.end method

.method private addStrongBinding()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .line 73
    .local v0, "connection":Lcom/android/org/chromium/content/browser/ChildProcessConnection;
    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ChildProcessConnection;->addStrongBinding()V

    goto :goto_0
.end method

.method private dropBindings()V
    .locals 2

    .prologue
    .line 108
    sget-boolean v1, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->this$0:Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    # getter for: Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mIsLowMemoryDevice:Z
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->access$100(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .line 110
    .local v0, "connection":Lcom/android/org/chromium/content/browser/ChildProcessConnection;
    if-nez v0, :cond_1

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ChildProcessConnection;->dropOomBindings()V

    goto :goto_0
.end method

.method private removeInitialBinding()V
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .line 58
    .local v0, "connection":Lcom/android/org/chromium/content/browser/ChildProcessConnection;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ChildProcessConnection;->isInitialBindingBound()Z

    move-result v1

    if-nez v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    new-instance v1, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection$1;

    invoke-direct {v1, p0, v0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection$1;-><init>(Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;Lcom/android/org/chromium/content/browser/ChildProcessConnection;)V

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->this$0:Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    # getter for: Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mRemoveInitialBindingDelay:J
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->access$000(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method private removeStrongBinding()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .line 83
    .local v0, "connection":Lcom/android/org/chromium/content/browser/ChildProcessConnection;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ChildProcessConnection;->isStrongBindingBound()Z

    move-result v2

    if-nez v2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    new-instance v1, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection$2;

    invoke-direct {v1, p0, v0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection$2;-><init>(Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;Lcom/android/org/chromium/content/browser/ChildProcessConnection;)V

    .line 96
    .local v1, "doUnbind":Ljava/lang/Runnable;
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->this$0:Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    # getter for: Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mIsLowMemoryDevice:Z
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->access$100(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 99
    :cond_2
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->this$0:Lcom/android/org/chromium/content/browser/BindingManagerImpl;

    # getter for: Lcom/android/org/chromium/content/browser/BindingManagerImpl;->mRemoveStrongBindingDelay:J
    invoke-static {v2}, Lcom/android/org/chromium/content/browser/BindingManagerImpl;->access$200(Lcom/android/org/chromium/content/browser/BindingManagerImpl;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method


# virtual methods
.method clearConnection()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ChildProcessConnection;->isOomProtectedOrWasWhenDied()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mWasOomProtected:Z

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    .line 159
    return-void
.end method

.method isOomProtected()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mConnection:Lcom/android/org/chromium/content/browser/ChildProcessConnection;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ChildProcessConnection;->isOomProtectedOrWasWhenDied()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mWasOomProtected:Z

    goto :goto_0
.end method

.method setInForeground(Z)V
    .locals 1
    .param p1, "nextInForeground"    # Z

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mInForeground:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 125
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->addStrongBinding()V

    .line 130
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->removeInitialBinding()V

    .line 131
    iput-boolean p1, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mInForeground:Z

    .line 132
    return-void

    .line 126
    :cond_1
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->mInForeground:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/BindingManagerImpl$ManagedConnection;->removeStrongBinding()V

    goto :goto_0
.end method
