.class Lcom/android/org/chromium/content/browser/ContentViewCore$3;
.super Ljava/lang/Object;
.source "ContentViewCore.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/PositionObserver$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lcom/android/org/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLcom/android/org/chromium/ui/base/WindowAndroid;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$3;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPositionChanged(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$3;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$700(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$3;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$800(Lcom/android/org/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$3;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->temporarilyHideTextHandles()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$900(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 616
    :cond_1
    return-void
.end method
