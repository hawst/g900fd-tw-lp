.class public Lcom/android/org/chromium/content/browser/input/HandleView;
.super Landroid/view/View;
.source "HandleView.java"


# static fields
.field private static final TEXT_VIEW_HANDLE_ATTRS:[I


# instance fields
.field private mAlpha:F

.field private final mContainer:Landroid/widget/PopupWindow;

.field private final mController:Lcom/android/org/chromium/content/browser/input/CursorController;

.field private mDownPositionX:F

.field private mDownPositionY:F

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mFadeStartTime:J

.field private mHotspotX:F

.field private mHotspotY:F

.field private mIsDragging:Z

.field private mIsInsertionHandle:Z

.field private final mLineOffsetY:I

.field private final mParent:Landroid/view/View;

.field private final mParentPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

.field private final mParentPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

.field private mParentPositionX:I

.field private mParentPositionY:I

.field private mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

.field private mPositionX:I

.field private mPositionY:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchTimer:J

.field private mTouchToWindowOffsetX:F

.field private mTouchToWindowOffsetY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/org/chromium/content/browser/input/HandleView;->TEXT_VIEW_HANDLE_ATTRS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x10102c5
        0x10102c7
        0x10102c6
    .end array-data
.end method

.method constructor <init>(Lcom/android/org/chromium/content/browser/input/CursorController;ILandroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V
    .locals 6
    .param p1, "controller"    # Lcom/android/org/chromium/content/browser/input/CursorController;
    .param p2, "pos"    # I
    .param p3, "parent"    # Landroid/view/View;
    .param p4, "parentPositionObserver"    # Lcom/android/org/chromium/content/browser/PositionObserver;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 56
    iput-boolean v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsInsertionHandle:Z

    .line 63
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTempRect:Landroid/graphics/Rect;

    .line 86
    iput-object p3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    .line 87
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 88
    .local v0, "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mController:Lcom/android/org/chromium/content/browser/input/CursorController;

    .line 89
    new-instance v1, Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    const v3, 0x10102c8

    invoke-direct {v1, v0, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    .line 90
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setSplitTouchEnabled(Z)V

    .line 91
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 92
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 94
    invoke-virtual {p0, p2}, Lcom/android/org/chromium/content/browser/input/HandleView;->setOrientation(I)V

    .line 97
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mLineOffsetY:I

    .line 100
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mAlpha:F

    .line 102
    new-instance v1, Lcom/android/org/chromium/content/browser/input/HandleView$1;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/content/browser/input/HandleView$1;-><init>(Lcom/android/org/chromium/content/browser/input/HandleView;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    .line 108
    iput-object p4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/input/HandleView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/HandleView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/content/browser/input/HandleView;->updateParentPosition(II)V

    return-void
.end method

.method private getContainerPositionX()I
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionX:I

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionX:I

    add-int/2addr v0, v1

    return v0
.end method

.method private getContainerPositionY()I
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionY:I

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    add-int/2addr v0, v1

    return v0
.end method

.method private isPositionVisible()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 213
    iget-boolean v6, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    if-eqz v6, :cond_1

    .line 231
    :cond_0
    :goto_0
    return v4

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTempRect:Landroid/graphics/Rect;

    .line 218
    .local v0, "clip":Landroid/graphics/Rect;
    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 219
    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 220
    iget-object v6, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    iput v6, v0, Landroid/graphics/Rect;->right:I

    .line 221
    iget-object v6, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    iput v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 223
    iget-object v6, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 224
    .local v1, "parent":Landroid/view/ViewParent;
    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    const/4 v7, 0x0

    invoke-interface {v1, v6, v0, v7}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    move v4, v5

    .line 225
    goto :goto_0

    .line 228
    :cond_3
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getContainerPositionX()I

    move-result v6

    iget v7, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    float-to-int v7, v7

    add-int v2, v6, v7

    .line 229
    .local v2, "posX":I
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getContainerPositionY()I

    move-result v6

    iget v7, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotY:F

    float-to-int v7, v7

    add-int v3, v6, v7

    .line 231
    .local v3, "posY":I
    iget v6, v0, Landroid/graphics/Rect;->left:I

    if-lt v2, v6, :cond_4

    iget v6, v0, Landroid/graphics/Rect;->right:I

    if-gt v2, v6, :cond_4

    iget v6, v0, Landroid/graphics/Rect;->top:I

    if-lt v3, v6, :cond_4

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    if-le v3, v6, :cond_0

    :cond_4
    move v4, v5

    goto :goto_0
.end method

.method private onPositionChanged()V
    .locals 6

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getContainerPositionX()I

    move-result v1

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getContainerPositionY()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    goto :goto_0
.end method

.method private showContainer()V
    .locals 5

    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getContainerPositionX()I

    move-result v3

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getContainerPositionY()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 177
    return-void
.end method

.method private updateAlpha()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 383
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mAlpha:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 384
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mFadeStartTime:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x43480000    # 200.0f

    div-float/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mAlpha:F

    .line 386
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mAlpha:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 387
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->invalidate()V

    goto :goto_0
.end method

.method private updateParentPosition(II)V
    .locals 2
    .param p1, "parentPositionX"    # I
    .param p2, "parentPositionY"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    .line 152
    :cond_0
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetX:F

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionX:I

    sub-int v1, p1, v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetX:F

    .line 153
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetY:F

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionY:I

    sub-int v1, p2, v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetY:F

    .line 154
    iput p1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionX:I

    .line 155
    iput p2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionY:I

    .line 156
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->onPositionChanged()V

    .line 157
    return-void
.end method

.method private updatePosition(FF)V
    .locals 5
    .param p1, "rawX"    # F
    .param p2, "rawY"    # F

    .prologue
    .line 335
    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetX:F

    sub-float v2, p1, v2

    iget v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    add-float v0, v2, v3

    .line 336
    .local v0, "newPosX":F
    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetY:F

    sub-float v2, p2, v2

    iget v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotY:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mLineOffsetY:I

    int-to-float v3, v3

    sub-float v1, v2, v3

    .line 338
    .local v1, "newPosY":F
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mController:Lcom/android/org/chromium/content/browser/input/CursorController;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-interface {v2, p0, v3, v4}, Lcom/android/org/chromium/content/browser/input/CursorController;->updatePosition(Lcom/android/org/chromium/content/browser/input/HandleView;II)V

    .line 339
    return-void
.end method


# virtual methods
.method beginFadeIn()V
    .locals 2

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 395
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mAlpha:F

    .line 396
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mFadeStartTime:J

    .line 397
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->setVisibility(I)V

    .line 399
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->onPositionChanged()V

    goto :goto_0
.end method

.method getAdjustedPositionX()I
    .locals 2

    .prologue
    .line 349
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionX:I

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method getAdjustedPositionY()I
    .locals 2

    .prologue
    .line 355
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotY:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getLineAdjustedPositionY()I
    .locals 2

    .prologue
    .line 375
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotY:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mLineOffsetY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method hide()V
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    .line 200
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 201
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    invoke-interface {v0, v1}, Lcom/android/org/chromium/content/browser/PositionObserver;->removeListener(Lcom/android/org/chromium/content/browser/PositionObserver$Listener;)V

    .line 202
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    .line 205
    :cond_0
    return-void
.end method

.method isDragging()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    return v0
.end method

.method moveTo(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 237
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionX:I

    .line 238
    .local v0, "previousPositionX":I
    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    .line 240
    .local v1, "previousPositionY":I
    iput p1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionX:I

    .line 241
    iput p2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    .line 242
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->isPositionVisible()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 243
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 244
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->onPositionChanged()V

    .line 246
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionX:I

    if-ne v0, v2, :cond_0

    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    if-eq v1, v2, :cond_1

    .line 248
    :cond_0
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    .line 254
    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    if-eqz v2, :cond_2

    .line 256
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-eqz v2, :cond_2

    .line 257
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v2}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    .line 263
    :cond_2
    :goto_1
    return-void

    .line 251
    :cond_3
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->show()V

    goto :goto_0

    .line 261
    :cond_4
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->hide()V

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 267
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->updateAlpha()V

    .line 268
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 269
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 270
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/HandleView;->setMeasuredDimension(II)V

    .line 146
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 274
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 313
    :goto_0
    return v2

    .line 276
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDownPositionX:F

    .line 277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDownPositionY:F

    .line 278
    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDownPositionX:F

    iget v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionX:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    iput v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetX:F

    .line 279
    iget v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDownPositionY:F

    iget v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPositionY:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    iput v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchToWindowOffsetY:F

    .line 280
    iput-boolean v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    .line 281
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mController:Lcom/android/org/chromium/content/browser/input/CursorController;

    invoke-interface {v2, p0}, Lcom/android/org/chromium/content/browser/input/CursorController;->beforeStartUpdatingPosition(Lcom/android/org/chromium/content/browser/input/HandleView;)V

    .line 282
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchTimer:J

    :goto_1
    move v2, v3

    .line 313
    goto :goto_0

    .line 287
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    invoke-direct {p0, v2, v4}, Lcom/android/org/chromium/content/browser/input/HandleView;->updatePosition(FF)V

    goto :goto_1

    .line 292
    :pswitch_2
    iget-boolean v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsInsertionHandle:Z

    if-eqz v4, :cond_0

    .line 293
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mTouchTimer:J

    sub-long v0, v4, v6

    .line 294
    .local v0, "delay":J
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v4

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 295
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 297
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v4}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    .line 303
    .end local v0    # "delay":J
    :cond_0
    :goto_2
    iput-boolean v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    goto :goto_1

    .line 299
    .restart local v0    # "delay":J
    :cond_1
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->showPastePopupWindow()V

    goto :goto_2

    .line 307
    .end local v0    # "delay":J
    :pswitch_3
    iput-boolean v2, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsDragging:Z

    goto :goto_1

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method positionAt(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 343
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sub-int v0, p1, v0

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotY:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/HandleView;->moveTo(II)V

    .line 344
    return-void
.end method

.method setOrientation(I)V
    .locals 7
    .param p1, "pos"    # I

    .prologue
    const/4 v3, 0x1

    const/high16 v6, 0x40800000    # 4.0f

    .line 112
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 113
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    sget-object v5, Lcom/android/org/chromium/content/browser/input/HandleView;->TEXT_VIEW_HANDLE_ATTRS:[I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 114
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 115
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 117
    if-ne p1, v3, :cond_0

    :goto_0
    iput-boolean v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsInsertionHandle:Z

    .line 119
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 120
    .local v2, "handleWidth":I
    packed-switch p1, :pswitch_data_0

    .line 133
    :pswitch_0
    int-to-float v3, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iput v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    .line 137
    :goto_1
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotY:F

    .line 139
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->invalidate()V

    .line 140
    return-void

    .line 117
    .end local v2    # "handleWidth":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 122
    .restart local v2    # "handleWidth":I
    :pswitch_1
    mul-int/lit8 v3, v2, 0x3

    int-to-float v3, v3

    div-float/2addr v3, v6

    iput v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    goto :goto_1

    .line 127
    :pswitch_2
    int-to-float v3, v2

    div-float/2addr v3, v6

    iput v3, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mHotspotX:F

    goto :goto_1

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method show()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/PositionObserver;->getPositionX()I

    move-result v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-interface {v1}, Lcom/android/org/chromium/content/browser/PositionObserver;->getPositionY()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/input/HandleView;->updateParentPosition(II)V

    .line 184
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->isPositionVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->hide()V

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mParentPositionListener:Lcom/android/org/chromium/content/browser/PositionObserver$Listener;

    invoke-interface {v0, v1}, Lcom/android/org/chromium/content/browser/PositionObserver;->addListener(Lcom/android/org/chromium/content/browser/PositionObserver$Listener;)V

    .line 189
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 190
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/HandleView;->showContainer()V

    .line 193
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->hide()V

    goto :goto_0
.end method

.method showPastePopupWindow()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mController:Lcom/android/org/chromium/content/browser/input/CursorController;

    check-cast v0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    .line 404
    .local v0, "ihc":Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
    iget-boolean v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mIsInsertionHandle:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->canPaste()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 405
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    if-nez v1, :cond_0

    .line 407
    new-instance v1, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;-><init>(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)V

    iput-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    .line 409
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/HandleView;->mPastePopupWindow:Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;->show()V

    .line 411
    :cond_1
    return-void
.end method
