.class Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;
.super Lcom/android/org/chromium/content_public/browser/GestureStateListener;
.source "AwContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/android_webview/AwContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AwGestureStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/android_webview/AwContents;


# direct methods
.method private constructor <init>(Lcom/android/org/chromium/android_webview/AwContents;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-direct {p0}, Lcom/android/org/chromium/content_public/browser/GestureStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p2, "x1"    # Lcom/android/org/chromium/android_webview/AwContents$1;

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;-><init>(Lcom/android/org/chromium/android_webview/AwContents;)V

    return-void
.end method


# virtual methods
.method public onFlingCancelGesture()V
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->onFlingCancelGesture()V

    .line 492
    return-void
.end method

.method public onPinchEnded()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->unfreezeLayoutRequests()V

    .line 487
    return-void
.end method

.method public onPinchStarted()V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->freezeLayoutRequests()V

    .line 482
    return-void
.end method

.method public onScrollUpdateGestureConsumed()V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollAccessibilityHelper:Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$1100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;->postViewScrolledAccessibilityEventCallback()V

    .line 502
    return-void
.end method

.method public onUnhandledFlingStartEvent(II)V
    .locals 1
    .param p1, "velocityX"    # I
    .param p2, "velocityY"    # I

    .prologue
    .line 496
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwGestureStateListener;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->onUnhandledFlingStartEvent(II)V

    .line 497
    return-void
.end method
