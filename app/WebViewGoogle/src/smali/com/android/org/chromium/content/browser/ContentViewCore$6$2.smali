.class Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;
.super Ljava/lang/Object;
.source "ContentViewCore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore$6;->onPopupZoomerHidden(Lcom/android/org/chromium/content/browser/PopupZoomer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$1:Lcom/android/org/chromium/content/browser/ContentViewCore$6;

.field final synthetic val$zoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 749
    const-class v0, Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore$6;Lcom/android/org/chromium/content/browser/PopupZoomer;)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->this$1:Lcom/android/org/chromium/content/browser/ContentViewCore$6;

    iput-object p2, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->val$zoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 752
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->this$1:Lcom/android/org/chromium/content/browser/ContentViewCore$6;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore$6;->mContainerViewAtCreation:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$6;->access$1300(Lcom/android/org/chromium/content/browser/ContentViewCore$6;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->val$zoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 753
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->this$1:Lcom/android/org/chromium/content/browser/ContentViewCore$6;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore$6;->mContainerViewAtCreation:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$6;->access$1300(Lcom/android/org/chromium/content/browser/ContentViewCore$6;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->val$zoomer:Lcom/android/org/chromium/content/browser/PopupZoomer;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 754
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->this$1:Lcom/android/org/chromium/content/browser/ContentViewCore$6;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore$6;->mContainerViewAtCreation:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore$6;->access$1300(Lcom/android/org/chromium/content/browser/ContentViewCore$6;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 758
    :cond_0
    return-void

    .line 756
    :cond_1
    sget-boolean v0, Lcom/android/org/chromium/content/browser/ContentViewCore$6$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "PopupZoomer should never be hidden without being shown"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
