.class public abstract Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
.super Ljava/lang/Object;
.source "InsertionHandleController.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/input/CursorController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/input/InsertionHandleController$PastePopupMenu;
    }
.end annotation


# instance fields
.field private mAllowAutomaticShowing:Z

.field private final mContext:Landroid/content/Context;

.field private mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

.field private mIsShowing:Z

.field private final mParent:Landroid/view/View;

.field private final mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "positionObserver"    # Lcom/android/org/chromium/content/browser/PositionObserver;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mParent:Landroid/view/View;

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Lcom/android/org/chromium/content/browser/input/HandleView;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/org/chromium/content/browser/input/InsertionHandleController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/input/InsertionHandleController;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mParent:Landroid/view/View;

    return-object v0
.end method

.method private createHandleIfNeeded()V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Lcom/android/org/chromium/content/browser/input/HandleView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mPositionObserver:Lcom/android/org/chromium/content/browser/PositionObserver;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/org/chromium/content/browser/input/HandleView;-><init>(Lcom/android/org/chromium/content/browser/input/CursorController;ILandroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    .line 186
    :cond_0
    return-void
.end method

.method private showHandleIfNeeded()V
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mIsShowing:Z

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mIsShowing:Z

    .line 191
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->show()V

    .line 192
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->setHandleVisibility(I)V

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public allowAutomaticShowing()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mAllowAutomaticShowing:Z

    .line 54
    return-void
.end method

.method public beforeStartUpdatingPosition(Lcom/android/org/chromium/content/browser/input/HandleView;)V
    .locals 0
    .param p1, "handle"    # Lcom/android/org/chromium/content/browser/input/HandleView;

    .prologue
    .line 152
    return-void
.end method

.method public beginHandleFadeIn()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->beginFadeIn()V

    .line 109
    return-void
.end method

.method canPaste()Z
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mContext:Landroid/content/Context;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v0

    return v0
.end method

.method protected abstract getLineHeight()I
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mIsShowing:Z

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->hide()V

    .line 142
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mIsShowing:Z

    .line 144
    :cond_1
    return-void
.end method

.method public hideAndDisallowAutomaticShowing()V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->hide()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mAllowAutomaticShowing:Z

    .line 60
    return-void
.end method

.method public isDragging()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mIsShowing:Z

    return v0
.end method

.method public onCursorPositionChanged()V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mAllowAutomaticShowing:Z

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showHandle()V

    .line 93
    :cond_0
    return-void
.end method

.method public onTouchModeChanged(Z)V
    .locals 0
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 133
    if-nez p1, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->hide()V

    .line 136
    :cond_0
    return-void
.end method

.method protected abstract paste()V
.end method

.method protected abstract setCursorPosition(II)V
.end method

.method public setHandlePosition(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/org/chromium/content/browser/input/HandleView;->positionAt(II)V

    .line 102
    return-void
.end method

.method public setHandleVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/input/HandleView;->setVisibility(I)V

    .line 116
    return-void
.end method

.method public showHandle()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->createHandleIfNeeded()V

    .line 67
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showHandleIfNeeded()V

    .line 68
    return-void
.end method

.method public showHandleWithPastePopup()V
    .locals 0

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showHandle()V

    .line 78
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showPastePopup()V

    .line 79
    return-void
.end method

.method showPastePopup()V
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mIsShowing:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->mHandle:Lcom/android/org/chromium/content/browser/input/HandleView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/HandleView;->showPastePopupWindow()V

    .line 74
    :cond_0
    return-void
.end method

.method public updatePosition(Lcom/android/org/chromium/content/browser/input/HandleView;II)V
    .locals 0
    .param p1, "handle"    # Lcom/android/org/chromium/content/browser/input/HandleView;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 156
    invoke-virtual {p0, p2, p3}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->setCursorPosition(II)V

    .line 157
    return-void
.end method
