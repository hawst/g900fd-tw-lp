.class public Lcom/android/org/chromium/content/browser/ContentVideoView;
.super Landroid/widget/FrameLayout;
.source "ContentVideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/android/org/chromium/ui/base/ViewAndroidDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/content/browser/ContentVideoView$ProgressView;,
        Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;
    }
.end annotation


# instance fields
.field private final mClient:Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

.field private mCurrentState:I

.field private mDuration:I

.field private mErrorButton:Ljava/lang/String;

.field private mErrorTitle:Ljava/lang/String;

.field private final mExitFullscreenRunnable:Ljava/lang/Runnable;

.field private mNativeContentVideoView:J

.field private mPlaybackErrorText:Ljava/lang/String;

.field private mProgressView:Landroid/view/View;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mUnknownErrorText:Ljava/lang/String;

.field private mVideoHeight:I

.field private mVideoLoadingText:Ljava/lang/String;

.field private mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

.field private mVideoWidth:I

.field private mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;


# direct methods
.method protected constructor <init>(Landroid/content/Context;JLcom/android/org/chromium/content/browser/ContentVideoViewClient;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nativeContentVideoView"    # J
    .param p4, "client"    # Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    .prologue
    const/4 v3, 0x0

    .line 149
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 76
    iput v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 140
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentVideoView$1;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/ContentVideoView$1;-><init>(Lcom/android/org/chromium/content/browser/ContentVideoView;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mExitFullscreenRunnable:Ljava/lang/Runnable;

    .line 150
    iput-wide p2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    .line 151
    new-instance v0, Lcom/android/org/chromium/ui/base/ViewAndroid;

    new-instance v1, Lcom/android/org/chromium/ui/base/WindowAndroid;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/org/chromium/ui/base/WindowAndroid;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, p0}, Lcom/android/org/chromium/ui/base/ViewAndroid;-><init>(Lcom/android/org/chromium/ui/base/WindowAndroid;Lcom/android/org/chromium/ui/base/ViewAndroidDelegate;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

    .line 152
    iput-object p4, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mClient:Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    .line 153
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->initResources(Landroid/content/Context;)V

    .line 154
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    invoke-direct {v0, p0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;-><init>(Lcom/android/org/chromium/content/browser/ContentVideoView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    .line 155
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->showContentVideoView()V

    .line 156
    invoke-virtual {p0, v3}, Lcom/android/org/chromium/content/browser/ContentVideoView;->setVisibility(I)V

    .line 157
    return-void
.end method

.method static synthetic access$000(Lcom/android/org/chromium/content/browser/ContentVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoView;

    .prologue
    .line 36
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/content/browser/ContentVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/content/browser/ContentVideoView;

    .prologue
    .line 36
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I

    return v0
.end method

.method private static createContentVideoView(Landroid/content/Context;JLcom/android/org/chromium/content/browser/ContentVideoViewClient;Z)Lcom/android/org/chromium/content/browser/ContentVideoView;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nativeContentVideoView"    # J
    .param p3, "client"    # Lcom/android/org/chromium/content/browser/ContentVideoViewClient;
    .param p4, "legacy"    # Z

    .prologue
    const/4 v1, 0x0

    .line 379
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 381
    invoke-static {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isActivityContext(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 382
    const-string v2, "ContentVideoView"

    const-string v3, "Wrong type of context, can\'t create fullscreen video"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 395
    :cond_0
    :goto_0
    return-object v0

    .line 385
    :cond_1
    const/4 v0, 0x0

    .line 386
    .local v0, "videoView":Lcom/android/org/chromium/content/browser/ContentVideoView;
    if-eqz p4, :cond_2

    .line 387
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    .end local v0    # "videoView":Lcom/android/org/chromium/content/browser/ContentVideoView;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;-><init>(Landroid/content/Context;JLcom/android/org/chromium/content/browser/ContentVideoViewClient;)V

    .line 392
    .restart local v0    # "videoView":Lcom/android/org/chromium/content/browser/ContentVideoView;
    :goto_1
    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->getContentVideoViewClient()Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewClient;->onShowCustomView(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 395
    goto :goto_0

    .line 389
    :cond_2
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentVideoView;

    .end local v0    # "videoView":Lcom/android/org/chromium/content/browser/ContentVideoView;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/org/chromium/content/browser/ContentVideoView;-><init>(Landroid/content/Context;JLcom/android/org/chromium/content/browser/ContentVideoViewClient;)V

    .restart local v0    # "videoView":Lcom/android/org/chromium/content/browser/ContentVideoView;
    goto :goto_1
.end method

.method public static getContentVideoView()Lcom/android/org/chromium/content/browser/ContentVideoView;
    .locals 1

    .prologue
    .line 446
    invoke-static {}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeGetSingletonJavaContentVideoView()Lcom/android/org/chromium/content/browser/ContentVideoView;

    move-result-object v0

    return-object v0
.end method

.method private getNativeViewAndroid()J
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mViewAndroid:Lcom/android/org/chromium/ui/base/ViewAndroid;

    invoke-virtual {v0}, Lcom/android/org/chromium/ui/base/ViewAndroid;->getNativePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method private initResources(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mPlaybackErrorText:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 165
    :cond_0
    sget v0, Lcom/android/org/chromium/content/R$string;->media_player_error_text_invalid_progressive_playback:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mPlaybackErrorText:Ljava/lang/String;

    .line 167
    sget v0, Lcom/android/org/chromium/content/R$string;->media_player_error_text_unknown:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mUnknownErrorText:Ljava/lang/String;

    .line 169
    sget v0, Lcom/android/org/chromium/content/R$string;->media_player_error_button:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mErrorButton:Ljava/lang/String;

    .line 171
    sget v0, Lcom/android/org/chromium/content/R$string;->media_player_error_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mErrorTitle:Ljava/lang/String;

    .line 173
    sget v0, Lcom/android/org/chromium/content/R$string;->media_player_loading_video:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoLoadingText:Ljava/lang/String;

    goto :goto_0
.end method

.method private static isActivityContext(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 401
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_0

    instance-of v0, p0, Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 402
    check-cast p0, Landroid/content/ContextWrapper;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    .line 404
    .restart local p0    # "context":Landroid/content/Context;
    :cond_0
    instance-of v0, p0, Landroid/app/Activity;

    return v0
.end method

.method private native nativeExitFullscreen(JZ)V
.end method

.method private native nativeGetCurrentPosition(J)I
.end method

.method private native nativeGetDurationInMilliSeconds(J)I
.end method

.method private static native nativeGetSingletonJavaContentVideoView()Lcom/android/org/chromium/content/browser/ContentVideoView;
.end method

.method private native nativeGetVideoHeight(J)I
.end method

.method private native nativeGetVideoWidth(J)I
.end method

.method private native nativeIsPlaying(J)Z
.end method

.method private native nativePause(J)V
.end method

.method private native nativePlay(J)V
.end method

.method private native nativeRequestMediaMetadata(J)V
.end method

.method private native nativeSeekTo(JI)V
.end method

.method private native nativeSetSurface(JLandroid/view/Surface;)V
.end method

.method private onExitFullscreen()V
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    .line 425
    return-void
.end method

.method private onPlaybackComplete()V
    .locals 0

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->onCompletion()V

    .line 265
    return-void
.end method

.method private onVideoSizeChanged(II)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 252
    iput p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I

    .line 253
    iput p2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I

    .line 255
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoWidth:I

    iget v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoHeight:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 256
    return-void
.end method


# virtual methods
.method public acquireAnchorView()Landroid/view/View;
    .locals 2

    .prologue
    .line 460
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 461
    .local v0, "anchorView":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->addView(Landroid/view/View;)V

    .line 462
    return-object v0
.end method

.method protected destroyContentVideoView(Z)V
    .locals 2
    .param p1, "nativeViewDestroyed"    # Z

    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    if-eqz v0, :cond_0

    .line 434
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->removeSurfaceView()V

    .line 435
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mClient:Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewClient;->onDestroyContentVideoView()V

    .line 440
    :cond_0
    if-eqz p1, :cond_1

    .line 441
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    .line 443
    :cond_1
    return-void
.end method

.method public exitFullscreen(Z)V
    .locals 4
    .param p1, "relaseMediaPlayer"    # Z

    .prologue
    const-wide/16 v2, 0x0

    .line 415
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->destroyContentVideoView(Z)V

    .line 416
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 417
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeExitFullscreen(JZ)V

    .line 418
    iput-wide v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    .line 420
    :cond_0
    return-void
.end method

.method protected getContentVideoViewClient()Lcom/android/org/chromium/content/browser/ContentVideoViewClient;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mClient:Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    return-object v0
.end method

.method protected getCurrentPosition()I
    .locals 4

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 360
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeGetCurrentPosition(J)I

    move-result v0

    .line 362
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getDuration()I
    .locals 4

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    if-lez v0, :cond_0

    .line 345
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    .line 355
    :goto_0
    return v0

    .line 347
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 348
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeGetDurationInMilliSeconds(J)I

    move-result v0

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    .line 352
    :goto_1
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    goto :goto_0

    .line 350
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    goto :goto_1

    .line 354
    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    .line 355
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    goto :goto_0
.end method

.method protected getSurfaceView()Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    return-object v0
.end method

.method protected isInPlaybackState()Z
    .locals 2

    .prologue
    .line 318
    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 4

    .prologue
    .line 372
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeIsPlaying(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onBufferingUpdate(I)V
    .locals 0
    .param p1, "percent"    # I

    .prologue
    .line 260
    return-void
.end method

.method protected onCompletion()V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 314
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 451
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 452
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    .line 453
    const/4 v0, 0x1

    .line 455
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMediaPlayerError(I)V
    .locals 7
    .param p1, "errorType"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, -0x1

    .line 200
    const-string v2, "ContentVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OnMediaPlayerError: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    if-ne v2, v6, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    if-eq p1, v6, :cond_0

    .line 210
    iput v5, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 220
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 223
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 224
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mPlaybackErrorText:Ljava/lang/String;

    .line 230
    .local v1, "message":Ljava/lang/String;
    :goto_1
    :try_start_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mErrorTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mErrorButton:Ljava/lang/String;

    new-instance v4, Lcom/android/org/chromium/content/browser/ContentVideoView$2;

    invoke-direct {v4, p0}, Lcom/android/org/chromium/content/browser/ContentVideoView$2;-><init>(Lcom/android/org/chromium/content/browser/ContentVideoView;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "ContentVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot show the alert dialog, error message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 226
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mUnknownErrorText:Ljava/lang/String;

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_1
.end method

.method protected onUpdateMediaMetadata(IIIZZZ)V
    .locals 2
    .param p1, "videoWidth"    # I
    .param p2, "videoHeight"    # I
    .param p3, "duration"    # I
    .param p4, "canPause"    # Z
    .param p5, "canSeekBack"    # Z
    .param p6, "canSeekForward"    # Z

    .prologue
    .line 275
    iput p3, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mDuration:I

    .line 276
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 277
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 278
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->onVideoSizeChanged(II)V

    .line 279
    return-void

    .line 277
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected openVideo()V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 303
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 304
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 305
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeRequestMediaMetadata(J)V

    .line 306
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeSetSurface(JLandroid/view/Surface;)V

    .line 310
    :cond_0
    return-void
.end method

.method protected pause()V
    .locals 4

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 334
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativePause(J)V

    .line 336
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 339
    :cond_1
    return-void
.end method

.method public releaseAnchorView(Landroid/view/View;)V
    .locals 0
    .param p1, "anchorView"    # Landroid/view/View;

    .prologue
    .line 472
    invoke-virtual {p0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->removeView(Landroid/view/View;)V

    .line 473
    return-void
.end method

.method public removeSurfaceView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 408
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->removeView(Landroid/view/View;)V

    .line 409
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->removeView(Landroid/view/View;)V

    .line 410
    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    .line 411
    iput-object v1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    .line 412
    return-void
.end method

.method protected seekTo(I)V
    .locals 4
    .param p1, "msec"    # I

    .prologue
    .line 366
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 367
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1, p1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeSeekTo(JI)V

    .line 369
    :cond_0
    return-void
.end method

.method public setAnchorViewPosition(Landroid/view/View;FFFF)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "width"    # F
    .param p5, "height"    # F

    .prologue
    .line 467
    const-string v0, "ContentVideoView"

    const-string v1, "setAnchorViewPosition isn\'t implemented"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    return-void
.end method

.method protected showContentVideoView()V
    .locals 5

    .prologue
    const/16 v4, 0x11

    const/4 v3, -0x2

    .line 178
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 179
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoSurfaceView:Lcom/android/org/chromium/content/browser/ContentVideoView$VideoSurfaceView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mClient:Lcom/android/org/chromium/content/browser/ContentVideoViewClient;

    invoke-interface {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewClient;->getVideoLoadingProgressView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    .line 185
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentVideoView$ProgressView;

    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mVideoLoadingText:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/android/org/chromium/content/browser/ContentVideoView$ProgressView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mProgressView:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    return-void
.end method

.method protected start()V
    .locals 4

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 324
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativePlay(J)V

    .line 326
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mCurrentState:I

    .line 328
    :cond_1
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 283
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 288
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->openVideo()V

    .line 289
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v4, 0x0

    .line 293
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 294
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mNativeContentVideoView:J

    invoke-direct {p0, v0, v1, v4}, Lcom/android/org/chromium/content/browser/ContentVideoView;->nativeSetSurface(JLandroid/view/Surface;)V

    .line 296
    :cond_0
    iput-object v4, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 297
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoView;->mExitFullscreenRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/content/browser/ContentVideoView;->post(Ljava/lang/Runnable;)Z

    .line 298
    return-void
.end method
