.class Lcom/android/org/chromium/content/browser/BatteryStatusManager;
.super Ljava/lang/Object;
.source "BatteryStatusManager.java"


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private mEnabled:Z

.field private final mFilter:Landroid/content/IntentFilter;

.field private mNativePtr:J

.field private final mNativePtrLock:Ljava/lang/Object;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mFilter:Landroid/content/IntentFilter;

    .line 31
    new-instance v0, Lcom/android/org/chromium/content/browser/BatteryStatusManager$1;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/BatteryStatusManager$1;-><init>(Lcom/android/org/chromium/content/browser/BatteryStatusManager;)V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mAppContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static getInstance(Landroid/content/Context;)Lcom/android/org/chromium/content/browser/BatteryStatusManager;
    .locals 1
    .param p0, "appContext"    # Landroid/content/Context;

    .prologue
    .line 52
    new-instance v0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;

    invoke-direct {v0, p0}, Lcom/android/org/chromium/content/browser/BatteryStatusManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private native nativeGotBatteryStatus(JZDDD)V
.end method


# virtual methods
.method protected gotBatteryStatus(ZDDD)V
    .locals 12
    .param p1, "charging"    # Z
    .param p2, "chargingTime"    # D
    .param p4, "dischargingTime"    # D
    .param p6, "level"    # D

    .prologue
    .line 132
    iget-object v10, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v10

    .line 133
    :try_start_0
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 134
    iget-wide v1, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    move-object v0, p0

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v9}, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->nativeGotBatteryStatus(JZDDD)V

    .line 136
    :cond_0
    monitor-exit v10

    .line 137
    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected ignoreBatteryPresentState()Z
    .locals 2

    .prologue
    .line 127
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Galaxy Nexus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method onReceive(Landroid/content/Intent;)V
    .locals 18
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v14, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    const-string v2, "BatteryStatusManager"

    const-string v14, "Unexpected intent."

    invoke-static {v2, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->ignoreBatteryPresentState()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v12, 0x1

    .line 95
    .local v12, "present":Z
    :goto_1
    if-nez v12, :cond_2

    .line 97
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->gotBatteryStatus(ZDDD)V

    goto :goto_0

    .line 92
    .end local v12    # "present":Z
    :cond_1
    const-string v2, "present"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    goto :goto_1

    .line 101
    .restart local v12    # "present":Z
    :cond_2
    const-string v2, "level"

    const/4 v14, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 102
    .local v10, "current":I
    const-string v2, "scale"

    const/4 v14, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 103
    .local v11, "max":I
    int-to-double v14, v10

    int-to-double v0, v11

    move-wide/from16 v16, v0

    div-double v8, v14, v16

    .line 104
    .local v8, "level":D
    const-wide/16 v14, 0x0

    cmpg-double v2, v8, v14

    if-ltz v2, :cond_3

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v8, v14

    if-lez v2, :cond_4

    .line 106
    :cond_3
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 109
    :cond_4
    const-string v2, "status"

    const/4 v14, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 110
    .local v13, "status":I
    const/4 v2, 0x3

    if-eq v13, v2, :cond_5

    const/4 v3, 0x1

    .line 113
    .local v3, "charging":Z
    :goto_2
    const/4 v2, 0x5

    if-ne v13, v2, :cond_6

    const-wide/16 v4, 0x0

    .line 115
    .local v4, "chargingTime":D
    :goto_3
    const-wide/high16 v6, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .local v6, "dischargingTime":D
    move-object/from16 v2, p0

    .line 117
    invoke-virtual/range {v2 .. v9}, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->gotBatteryStatus(ZDDD)V

    goto :goto_0

    .line 110
    .end local v3    # "charging":Z
    .end local v4    # "chargingTime":D
    .end local v6    # "dischargingTime":D
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 113
    .restart local v3    # "charging":Z
    :cond_6
    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    goto :goto_3
.end method

.method start(J)Z
    .locals 5
    .param p1, "nativePtr"    # J

    .prologue
    .line 61
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v1

    .line 62
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iput-wide p1, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    .line 67
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    return v0

    .line 67
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method stop()V
    .locals 4

    .prologue
    .line 76
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-boolean v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 79
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    .line 82
    :cond_0
    monitor-exit v1

    .line 83
    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
