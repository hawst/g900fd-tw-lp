.class public Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "AdapterInputConnection.java"


# instance fields
.field private final mEditable:Landroid/text/Editable;

.field private final mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

.field private final mInternalView:Landroid/view/View;

.field private mLastUpdateCompositionEnd:I

.field private mLastUpdateCompositionStart:I

.field private mLastUpdateSelectionEnd:I

.field private mLastUpdateSelectionStart:I

.field private mNumNestedBatchEdits:I

.field private mSingleLine:Z


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/android/org/chromium/content/browser/input/ImeAdapter;Landroid/text/Editable;Landroid/view/inputmethod/EditorInfo;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "imeAdapter"    # Lcom/android/org/chromium/content/browser/input/ImeAdapter;
    .param p3, "editable"    # Landroid/text/Editable;
    .param p4, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 51
    invoke-direct {p0, p1, v1}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    .line 41
    iput v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    .line 43
    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionStart:I

    .line 44
    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionEnd:I

    .line 45
    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateCompositionStart:I

    .line 46
    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateCompositionEnd:I

    .line 52
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mInternalView:Landroid/view/View;

    .line 53
    iput-object p2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    .line 54
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0, p0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->setInputConnection(Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;)V

    .line 55
    iput-object p3, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    .line 59
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    .line 60
    iput-boolean v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mSingleLine:Z

    .line 61
    const/high16 v0, 0x12000000

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 63
    const/16 v0, 0xa1

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 66
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeText:I

    if-ne v0, v1, :cond_1

    .line 68
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 69
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 109
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    .line 110
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    .line 111
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionStart:I

    .line 112
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionEnd:I

    .line 114
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    iget v1, p4, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    iget v2, p4, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    invoke-static {v0, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 115
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 116
    return-void

    .line 70
    :cond_1
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeTextArea:I

    if-eq v0, v1, :cond_2

    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeContentEditable:I

    if-ne v0, v1, :cond_3

    .line 73
    :cond_2
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    const v1, 0x2c000

    or-int/2addr v0, v1

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 76
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 77
    iput-boolean v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mSingleLine:Z

    goto :goto_0

    .line 78
    :cond_3
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypePassword:I

    if-ne v0, v1, :cond_4

    .line 80
    const/16 v0, 0xe1

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 82
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0

    .line 83
    :cond_4
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeSearch:I

    if-ne v0, v1, :cond_5

    .line 85
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x3

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_0

    .line 86
    :cond_5
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeUrl:I

    if-ne v0, v1, :cond_6

    .line 88
    const/16 v0, 0x11

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 90
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto/16 :goto_0

    .line 91
    :cond_6
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeEmail:I

    if-ne v0, v1, :cond_7

    .line 93
    const/16 v0, 0xd1

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 95
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto/16 :goto_0

    .line 96
    :cond_7
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeTel:I

    if-ne v0, v1, :cond_8

    .line 100
    const/4 v0, 0x3

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 101
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x5

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto/16 :goto_0

    .line 102
    :cond_8
    invoke-virtual {p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getTextInputType()I

    move-result v0

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sTextInputTypeNumber:I

    if-ne v0, v1, :cond_0

    .line 104
    const/16 v0, 0x2002

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 107
    iget v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x5

    iput v0, p4, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto/16 :goto_0
.end method

.method private getInputMethodManagerWrapper()Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->getInputMethodManagerWrapper()Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    move-result-object v0

    return-object v0
.end method

.method private maybePerformEmptyCompositionWorkaround(Ljava/lang/CharSequence;)Z
    .locals 7
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v6, -0x1

    .line 461
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v4

    .line 462
    .local v4, "selectionStart":I
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 463
    .local v3, "selectionEnd":I
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v1

    .line 464
    .local v1, "compositionStart":I
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v0

    .line 465
    .local v0, "compositionEnd":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-ne v4, v3, :cond_0

    if-eq v1, v6, :cond_0

    if-eq v0, v6, :cond_0

    .line 468
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->beginBatchEdit()Z

    .line 469
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->finishComposingText()Z

    .line 470
    iget-object v5, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 471
    .local v2, "selection":I
    sub-int v5, v2, v1

    sub-int v6, v2, v0

    invoke-virtual {p0, v5, v6}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->deleteSurroundingText(II)Z

    .line 472
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->endBatchEdit()Z

    .line 473
    const/4 v5, 0x1

    .line 475
    .end local v2    # "selection":I
    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private updateSelectionIfRequired()V
    .locals 6

    .prologue
    .line 184
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    if-eqz v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 186
    .local v2, "selectionStart":I
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 187
    .local v3, "selectionEnd":I
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v4

    .line 188
    .local v4, "compositionStart":I
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v5

    .line 190
    .local v5, "compositionEnd":I
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionStart:I

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionEnd:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateCompositionStart:I

    if-ne v0, v4, :cond_2

    iget v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateCompositionEnd:I

    if-eq v0, v5, :cond_0

    .line 202
    :cond_2
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getInputMethodManagerWrapper()Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mInternalView:Landroid/view/View;

    invoke-virtual/range {v0 .. v5}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->updateSelection(Landroid/view/View;IIII)V

    .line 204
    iput v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionStart:I

    .line 205
    iput v3, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateSelectionEnd:I

    .line 206
    iput v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateCompositionStart:I

    .line 207
    iput v5, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mLastUpdateCompositionEnd:I

    goto :goto_0
.end method


# virtual methods
.method public beginBatchEdit()Z
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    .line 298
    const/4 v0, 0x1

    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    const/4 v0, 0x1

    .line 228
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->maybePerformEmptyCompositionWorkaround(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    :goto_0
    return v0

    .line 229
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    .line 230
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 231
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_1

    :goto_1
    invoke-virtual {v1, p1, p2, v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->checkCompositionQueueAndCallNative(Ljava/lang/CharSequence;IZ)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public deleteSurroundingText(II)Z
    .locals 4
    .param p1, "beforeLength"    # I
    .param p2, "afterLength"    # I

    .prologue
    .line 321
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 322
    .local v1, "availableBefore":I
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    sub-int v0, v2, v3

    .line 323
    .local v0, "availableAfter":I
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 324
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 325
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    .line 326
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 327
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->deleteSurroundingText(II)Z

    move-result v2

    return v2
.end method

.method public endBatchEdit()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 306
    iget v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    if-nez v1, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 307
    :cond_1
    iget v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    .line 309
    iget v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 310
    :cond_2
    iget v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finishComposingText()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 386
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 394
    :goto_0
    return v2

    .line 390
    :cond_0
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->finishComposingText()Z

    .line 391
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 392
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->finishComposingText()V

    goto :goto_0
.end method

.method public getEditable()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    return-object v0
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .locals 2
    .param p1, "request"    # Landroid/view/inputmethod/ExtractedTextRequest;
    .param p2, "flags"    # I

    .prologue
    .line 282
    new-instance v0, Landroid/view/inputmethod/ExtractedText;

    invoke-direct {v0}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    .line 283
    .local v0, "et":Landroid/view/inputmethod/ExtractedText;
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    .line 284
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iput v1, v0, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    .line 285
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    iput v1, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 286
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    iput v1, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 287
    iget-boolean v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mSingleLine:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput v1, v0, Landroid/view/inputmethod/ExtractedText;->flags:I

    .line 288
    return-object v0

    .line 287
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performContextMenuAction(I)Z
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 261
    packed-switch p1, :pswitch_data_0

    .line 271
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 263
    :pswitch_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->selectAll()Z

    move-result v0

    goto :goto_0

    .line 265
    :pswitch_1
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->cut()Z

    move-result v0

    goto :goto_0

    .line 267
    :pswitch_2
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->copy()Z

    move-result v0

    goto :goto_0

    .line 269
    :pswitch_3
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->paste()Z

    move-result v0

    goto :goto_0

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x102001f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public performEditorAction(I)Z
    .locals 6
    .param p1, "actionCode"    # I

    .prologue
    .line 241
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->restartInput()V

    .line 244
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 245
    .local v2, "timeStampMs":J
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    sget v1, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sEventTypeRawKeyDown:I

    const/16 v4, 0x3d

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sendSyntheticKeyEvent(IJII)Z

    .line 252
    .end local v2    # "timeStampMs":J
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    const/16 v1, 0x42

    const/16 v4, 0x16

    invoke-virtual {v0, v1, v4}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->sendKeyEventWithKeyCode(II)V

    goto :goto_0
.end method

.method restartInput()V
    .locals 2

    .prologue
    .line 416
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->getInputMethodManagerWrapper()Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mInternalView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/input/InputMethodManagerWrapper;->restartInput(Landroid/view/View;)V

    .line 417
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mNumNestedBatchEdits:I

    .line 418
    return-void
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v8, 0x70

    const/16 v7, 0x43

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 341
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v6, :cond_5

    .line 342
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-ne v4, v7, :cond_1

    .line 343
    invoke-virtual {p0, v6, v5}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->deleteSurroundingText(II)Z

    .line 377
    :cond_0
    :goto_0
    return v6

    .line 345
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-ne v4, v8, :cond_2

    .line 346
    invoke-virtual {p0, v5, v6}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->deleteSurroundingText(II)Z

    goto :goto_0

    .line 349
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v3

    .line 350
    .local v3, "unicodeChar":I
    if-eqz v3, :cond_4

    .line 351
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 352
    .local v1, "selectionStart":I
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 353
    .local v0, "selectionEnd":I
    if-le v1, v0, :cond_3

    .line 354
    move v2, v1

    .line 355
    .local v2, "temp":I
    move v1, v0

    .line 356
    move v0, v2

    .line 358
    .end local v2    # "temp":I
    :cond_3
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    int-to-char v5, v3

    invoke-static {v5}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v1, v0, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 376
    .end local v0    # "selectionEnd":I
    .end local v1    # "selectionStart":I
    .end local v3    # "unicodeChar":I
    :cond_4
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v4, p1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    goto :goto_0

    .line 362
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_4

    .line 364
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v5, 0x42

    if-ne v4, v5, :cond_6

    .line 365
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->beginBatchEdit()Z

    .line 366
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->finishComposingText()Z

    .line 367
    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v4, p1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->translateAndSendNativeEvents(Landroid/view/KeyEvent;)Z

    .line 368
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->endBatchEdit()Z

    goto :goto_0

    .line 370
    :cond_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-eq v4, v7, :cond_0

    .line 372
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-ne v4, v8, :cond_4

    goto :goto_0
.end method

.method public setComposingRegion(II)Z
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 426
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v2

    .line 427
    .local v2, "textLength":I
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 428
    .local v0, "a":I
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 429
    .local v1, "b":I
    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 430
    :cond_0
    if-gez v1, :cond_1

    const/4 v1, 0x0

    .line 431
    :cond_1
    if-le v0, v2, :cond_2

    move v0, v2

    .line 432
    :cond_2
    if-le v1, v2, :cond_3

    move v1, v2

    .line 434
    :cond_3
    if-ne v0, v1, :cond_4

    .line 435
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    .line 439
    :goto_0
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 440
    iget-object v3, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v3, v0, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->setComposingRegion(II)Z

    move-result v3

    return v3

    .line 437
    :cond_4
    invoke-super {p0, v0, v1}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    goto :goto_0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->maybePerformEmptyCompositionWorkaround(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 219
    :goto_0
    return v0

    .line 217
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    .line 218
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 219
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->checkCompositionQueueAndCallNative(Ljava/lang/CharSequence;IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public setSelection(II)Z
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 403
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 404
    .local v0, "textLength":I
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-gt p1, v0, :cond_0

    if-le p2, v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 407
    :goto_0
    return v1

    .line 405
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setSelection(II)Z

    .line 406
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    .line 407
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    invoke-virtual {v1, p1, p2}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->setEditableSelectionOffsets(II)Z

    move-result v1

    goto :goto_0
.end method

.method public updateState(Ljava/lang/String;IIIIZ)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "selectionStart"    # I
    .param p3, "selectionEnd"    # I
    .param p4, "compositionStart"    # I
    .param p5, "compositionEnd"    # I
    .param p6, "isNonImeChange"    # Z

    .prologue
    .line 144
    if-nez p6, :cond_0

    .line 169
    :goto_0
    return-void

    .line 147
    :cond_0
    const/16 v2, 0xa0

    const/16 v3, 0x20

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 149
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 150
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 151
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {p4, v2}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 152
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {p5, v2}, Ljava/lang/Math;->min(II)I

    move-result p5

    .line 154
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "prevText":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 157
    .local v1, "textUnchanged":Z
    if-nez v1, :cond_1

    .line 158
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-interface {v2, v3, v4, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 161
    :cond_1
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v2, p2, p3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 163
    if-ne p4, p5, :cond_2

    .line 164
    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    .line 168
    :goto_1
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/input/AdapterInputConnection;->updateSelectionIfRequired()V

    goto :goto_0

    .line 166
    :cond_2
    invoke-super {p0, p4, p5}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    goto :goto_1
.end method
