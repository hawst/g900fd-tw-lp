.class Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$1;
.super Ljava/lang/Object;
.source "SelectPopupDropdown.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Ljava/util/List;Landroid/graphics/Rect;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    .line 42
    new-array v0, v2, [I

    const/4 v1, 0x0

    aput p3, v0, v1

    .line 43
    .local v0, "selectedIndices":[I
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    # getter for: Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->access$000(Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->selectPopupMenuItems([I)V

    .line 44
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    # setter for: Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->mAlreadySelectedItems:Z
    invoke-static {v1, v2}, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->access$102(Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;Z)Z

    .line 45
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;

    invoke-virtual {v1}, Lcom/android/org/chromium/content/browser/input/SelectPopupDropdown;->hide()V

    .line 46
    return-void
.end method
