.class Lcom/android/org/chromium/content/browser/ContentViewCore$9;
.super Lcom/android/org/chromium/content/browser/input/SelectionHandleController;
.source "ContentViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore;->getSelectionHandleController()Lcom/android/org/chromium/content/browser/input/SelectionHandleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;
    .param p3, "x1"    # Lcom/android/org/chromium/content/browser/PositionObserver;

    .prologue
    .line 2014
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, p2, p3}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;-><init>(Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    return-void
.end method


# virtual methods
.method public selectBetweenCoordinates(IIII)V
    .locals 8
    .param p1, "x1"    # I
    .param p2, "y1"    # I
    .param p3, "x2"    # I
    .param p4, "y2"    # I

    .prologue
    .line 2017
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1400(Lcom/android/org/chromium/content/browser/ContentViewCore;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 2018
    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1400(Lcom/android/org/chromium/content/browser/ContentViewCore;)J

    move-result-wide v2

    int-to-float v4, p1

    int-to-float v0, p2

    iget-object v5, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;
    invoke-static {v5}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$200(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getContentOffsetYPix()F

    move-result v5

    sub-float v5, v0, v5

    int-to-float v6, p3

    int-to-float v0, p4

    iget-object v7, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;
    invoke-static {v7}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$200(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getContentOffsetYPix()F

    move-result v7

    sub-float v7, v0, v7

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeSelectBetweenCoordinates(JFFFF)V
    invoke-static/range {v1 .. v7}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1700(Lcom/android/org/chromium/content/browser/ContentViewCore;JFFFF)V

    .line 2022
    :cond_1
    return-void
.end method

.method public showHandles(II)V
    .locals 2
    .param p1, "startDir"    # I
    .param p2, "endDir"    # I

    .prologue
    .line 2026
    invoke-virtual {p0}, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->isShowing()Z

    move-result v0

    .line 2027
    .local v0, "wasShowing":Z
    invoke-super {p0, p1, p2}, Lcom/android/org/chromium/content/browser/input/SelectionHandleController;->showHandles(II)V

    .line 2028
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1800(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$9;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->showSelectActionBar()V
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1900(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 2029
    :cond_1
    return-void
.end method
