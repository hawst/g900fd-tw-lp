.class Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;
.super Ljava/lang/Object;
.source "AwContents.java"

# interfaces
.implements Lcom/android/org/chromium/android_webview/AwViewMethods;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/android_webview/AwContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AwViewMethodsImpl"
.end annotation


# instance fields
.field private final mClipBoundsTemporary:Landroid/graphics/Rect;

.field private mComponentCallbacks:Landroid/content/ComponentCallbacks2;

.field private mLayerType:I

.field final synthetic this$0:Lcom/android/org/chromium/android_webview/AwContents;


# direct methods
.method private constructor <init>(Lcom/android/org/chromium/android_webview/AwContents;)V
    .locals 1

    .prologue
    .line 2206
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2207
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mLayerType:I

    .line 2211
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mClipBoundsTemporary:Landroid/graphics/Rect;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p2, "x1"    # Lcom/android/org/chromium/android_webview/AwContents$1;

    .prologue
    .line 2206
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;-><init>(Lcom/android/org/chromium/android_webview/AwContents;)V

    return-void
.end method

.method private isDpadEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2290
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 2291
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2300
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2297
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2291
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateHardwareAcceleratedFeaturesToggle()V
    .locals 3

    .prologue
    .line 2265
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v1

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$4200(Lcom/android/org/chromium/android_webview/AwContents;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mLayerType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mLayerType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/org/chromium/android_webview/AwSettings;->setEnableSupportedHardwareAcceleratedFeatures(Z)V

    .line 2269
    return-void

    .line 2265
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2283
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->isDpadEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2284
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings;->setSpatialNavigationEnabled(Z)V

    .line 2286
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 5

    .prologue
    .line 2355
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2370
    :cond_0
    :goto_0
    return-void

    .line 2356
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$4200(Lcom/android/org/chromium/android_webview/AwContents;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2357
    const-string v0, "AwContents"

    const-string v1, "onAttachedToWindow called when already attached. Ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2360
    :cond_2
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    const/4 v1, 0x1

    # setter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z
    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$4202(Lcom/android/org/chromium/android_webview/AwContents;Z)Z

    .line 2362
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onAttachedToWindow()V

    .line 2363
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->nativeOnAttachedToWindow(JII)V
    invoke-static {v0, v2, v3, v1, v4}, Lcom/android/org/chromium/android_webview/AwContents;->access$4500(Lcom/android/org/chromium/android_webview/AwContents;JII)V

    .line 2365
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->updateHardwareAcceleratedFeaturesToggle()V

    .line 2367
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mComponentCallbacks:Landroid/content/ComponentCallbacks2;

    if-nez v0, :cond_0

    .line 2368
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents$AwComponentCallbacks;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/org/chromium/android_webview/AwContents$AwComponentCallbacks;-><init>(Lcom/android/org/chromium/android_webview/AwContents;Lcom/android/org/chromium/android_webview/AwContents$1;)V

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mComponentCallbacks:Landroid/content/ComponentCallbacks2;

    .line 2369
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$4700(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mComponentCallbacks:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2350
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2351
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 2273
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    .line 2374
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$4200(Lcom/android/org/chromium/android_webview/AwContents;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2375
    const-string v0, "AwContents"

    const-string v1, "onDetachedFromWindow called when already detached. Ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2394
    :goto_0
    return-void

    .line 2378
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    const/4 v1, 0x0

    # setter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsAttachedToWindow:Z
    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$4202(Lcom/android/org/chromium/android_webview/AwContents;Z)Z

    .line 2379
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->hideAutofillPopup()V

    .line 2380
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2381
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v0

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->nativeOnDetachedFromWindow(J)V
    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$4800(J)V

    .line 2384
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onDetachedFromWindow()V

    .line 2385
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->updateHardwareAcceleratedFeaturesToggle()V

    .line 2387
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mComponentCallbacks:Landroid/content/ComponentCallbacks2;

    if-eqz v0, :cond_2

    .line 2388
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$4700(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mComponentCallbacks:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 2389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mComponentCallbacks:Landroid/content/ComponentCallbacks2;

    .line 2392
    :cond_2
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollAccessibilityHelper:Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$1100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/ScrollAccessibilityHelper;->removePostedCallbacks()V

    .line 2393
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeGLDelegate:Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$4900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;->detachGLFunctor()V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2215
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2216
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getEffectiveBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2243
    :cond_0
    :goto_0
    return-void

    .line 2222
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mClipBoundsTemporary:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2226
    :cond_2
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->syncScrollOffsetFromOnDraw()V

    .line 2227
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->getGlobalVisibleRect()Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$1200(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/graphics/Rect;

    move-result-object v0

    .line 2228
    .local v0, "globalVisibleRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v2}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v5

    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v6

    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v7

    iget v8, v0, Landroid/graphics/Rect;->left:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    iget v10, v0, Landroid/graphics/Rect;->right:I

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    move-object v4, p1

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->nativeOnDraw(JLandroid/graphics/Canvas;ZIIIIII)Z
    invoke-static/range {v1 .. v11}, Lcom/android/org/chromium/android_webview/AwContents;->access$3900(Lcom/android/org/chromium/android_webview/AwContents;JLandroid/graphics/Canvas;ZIIIIII)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2235
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getEffectiveBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2238
    :cond_3
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;

    move-result-object v1

    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v2}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeMaximumHorizontalScrollOffset()I

    move-result v2

    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->computeMaximumVerticalScrollOffset()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->drawEdgeGlows(Landroid/graphics/Canvas;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2241
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->invalidate()V

    goto/16 :goto_0
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2404
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # setter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerViewFocused:Z
    invoke-static {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->access$5102(Lcom/android/org/chromium/android_webview/AwContents;Z)Z

    .line 2405
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onFocusChanged(Z)V

    .line 2406
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2345
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2340
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2278
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 2247
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->onMeasure(II)V

    .line 2248
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 8
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "ow"    # I
    .param p4, "oh"    # I

    .prologue
    .line 2410
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2420
    :goto_0
    return-void

    .line 2411
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->setContainerViewSize(II)V

    .line 2416
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mLayoutSizer:Lcom/android/org/chromium/android_webview/AwLayoutSizer;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$900(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwLayoutSizer;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwLayoutSizer;->onSizeChanged(IIII)V

    .line 2417
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onPhysicalBackingSizeChanged(II)V

    .line 2418
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onSizeChanged(IIII)V

    .line 2419
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->nativeOnSizeChanged(JIIII)V
    invoke-static/range {v1 .. v7}, Lcom/android/org/chromium/android_webview/AwContents;->access$5200(Lcom/android/org/chromium/android_webview/AwContents;JIIII)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 2305
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    move v1, v2

    .line 2335
    :cond_0
    :goto_0
    return v1

    .line 2307
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-nez v3, :cond_2

    .line 2308
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/org/chromium/android_webview/AwSettings;->setSpatialNavigationEnabled(Z)V

    .line 2311
    :cond_2
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v3

    invoke-virtual {v3, v12}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->setProcessingTouchEvent(Z)V

    .line 2312
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 2313
    .local v1, "rv":Z
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mScrollOffsetManager:Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$1000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/org/chromium/android_webview/AwScrollOffsetManager;->setProcessingTouchEvent(Z)V

    .line 2315
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-nez v3, :cond_3

    .line 2316
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 2320
    .local v0, "actionIndex":I
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v4, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v4}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v4

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    float-to-double v6, v6

    iget-object v8, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D
    invoke-static {v8}, Lcom/android/org/chromium/android_webview/AwContents;->access$4300(Lcom/android/org/chromium/android_webview/AwContents;)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    float-to-double v8, v7

    iget-object v7, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mDIPScale:D
    invoke-static {v7}, Lcom/android/org/chromium/android_webview/AwContents;->access$4300(Lcom/android/org/chromium/android_webview/AwContents;)D

    move-result-wide v10

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v7, v8

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->nativeRequestNewHitTestDataAt(JII)V
    invoke-static {v3, v4, v5, v6, v7}, Lcom/android/org/chromium/android_webview/AwContents;->access$4400(Lcom/android/org/chromium/android_webview/AwContents;JII)V

    .line 2325
    .end local v0    # "actionIndex":I
    :cond_3
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2326
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-nez v3, :cond_4

    .line 2327
    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;
    invoke-static {v2}, Lcom/android/org/chromium/android_webview/AwContents;->access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->setShouldPull(Z)V

    goto :goto_0

    .line 2328
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-eq v3, v12, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 2330
    :cond_5
    iget-object v3, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;
    invoke-static {v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->setShouldPull(Z)V

    .line 2331
    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mOverScrollGlow:Lcom/android/org/chromium/android_webview/OverScrollGlow;
    invoke-static {v2}, Lcom/android/org/chromium/android_webview/AwContents;->access$4000(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/OverScrollGlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/OverScrollGlow;->releaseAll()V

    goto/16 :goto_0
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 2
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 2424
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 2425
    .local v0, "viewVisible":Z
    :goto_0
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsViewVisible:Z
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$1300(Lcom/android/org/chromium/android_webview/AwContents;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 2427
    :goto_1
    return-void

    .line 2424
    .end local v0    # "viewVisible":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2426
    .restart local v0    # "viewVisible":Z
    :cond_1
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->setViewVisibilityInternal(Z)V
    invoke-static {v1, v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$5300(Lcom/android/org/chromium/android_webview/AwContents;Z)V

    goto :goto_1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 2398
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # setter for: Lcom/android/org/chromium/android_webview/AwContents;->mWindowFocused:Z
    invoke-static {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->access$5002(Lcom/android/org/chromium/android_webview/AwContents;Z)Z

    .line 2399
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$2100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->onWindowFocusChanged(Z)V

    .line 2400
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 2431
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 2432
    .local v0, "windowVisible":Z
    :goto_0
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mIsWindowVisible:Z
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$1400(Lcom/android/org/chromium/android_webview/AwContents;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 2434
    :goto_1
    return-void

    .line 2431
    .end local v0    # "windowVisible":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2433
    .restart local v0    # "windowVisible":Z
    :cond_1
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->setWindowVisibilityInternal(Z)V
    invoke-static {v1, v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$5400(Lcom/android/org/chromium/android_webview/AwContents;Z)V

    goto :goto_1
.end method

.method public requestFocus()V
    .locals 4

    .prologue
    .line 2252
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2256
    :cond_0
    :goto_0
    return-void

    .line 2253
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$500(Lcom/android/org/chromium/android_webview/AwContents;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mSettings:Lcom/android/org/chromium/android_webview/AwSettings;
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContents;->access$100(Lcom/android/org/chromium/android_webview/AwContents;)Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwSettings;->shouldFocusFirstNode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2254
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->this$0:Lcom/android/org/chromium/android_webview/AwContents;

    # getter for: Lcom/android/org/chromium/android_webview/AwContents;->mNativeAwContents:J
    invoke-static {v1}, Lcom/android/org/chromium/android_webview/AwContents;->access$700(Lcom/android/org/chromium/android_webview/AwContents;)J

    move-result-wide v2

    # invokes: Lcom/android/org/chromium/android_webview/AwContents;->nativeFocusFirstNode(J)V
    invoke-static {v0, v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->access$4100(Lcom/android/org/chromium/android_webview/AwContents;J)V

    goto :goto_0
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .locals 0
    .param p1, "layerType"    # I
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 2260
    iput p1, p0, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->mLayerType:I

    .line 2261
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$AwViewMethodsImpl;->updateHardwareAcceleratedFeaturesToggle()V

    .line 2262
    return-void
.end method
