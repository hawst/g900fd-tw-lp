.class Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;
.super Ljava/lang/Object;
.source "WebContentsImpl.java"

# interfaces
.implements Lcom/android/org/chromium/content_public/browser/WebContents;


# instance fields
.field private mNativeWebContentsAndroid:J

.field private mNavigationController:Lcom/android/org/chromium/content_public/browser/NavigationController;


# direct methods
.method private constructor <init>(JLcom/android/org/chromium/content_public/browser/NavigationController;)V
    .locals 1
    .param p1, "nativeWebContentsAndroid"    # J
    .param p3, "navigationController"    # Lcom/android/org/chromium/content_public/browser/NavigationController;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-wide p1, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNativeWebContentsAndroid:J

    .line 27
    iput-object p3, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNavigationController:Lcom/android/org/chromium/content_public/browser/NavigationController;

    .line 28
    return-void
.end method

.method private static create(JLcom/android/org/chromium/content_public/browser/NavigationController;)Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;
    .locals 2
    .param p0, "nativeWebContentsAndroid"    # J
    .param p2, "navigationController"    # Lcom/android/org/chromium/content_public/browser/NavigationController;

    .prologue
    .line 33
    new-instance v0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;-><init>(JLcom/android/org/chromium/content_public/browser/NavigationController;)V

    return-object v0
.end method

.method private destroy()V
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNativeWebContentsAndroid:J

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNavigationController:Lcom/android/org/chromium/content_public/browser/NavigationController;

    .line 40
    return-void
.end method

.method private getNativePointer()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNativeWebContentsAndroid:J

    return-wide v0
.end method

.method private native nativeGetTitle(J)Ljava/lang/String;
.end method

.method private native nativeGetVisibleURL(J)Ljava/lang/String;
.end method

.method private native nativeStop(J)V
.end method


# virtual methods
.method public getNavigationController()Lcom/android/org/chromium/content_public/browser/NavigationController;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNavigationController:Lcom/android/org/chromium/content_public/browser/NavigationController;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNativeWebContentsAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->nativeGetTitle(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVisibleUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNativeWebContentsAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->nativeGetVisibleURL(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->mNativeWebContentsAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/webcontents/WebContentsImpl;->nativeStop(J)V

    .line 65
    return-void
.end method
