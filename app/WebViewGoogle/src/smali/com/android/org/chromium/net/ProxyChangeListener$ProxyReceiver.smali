.class Lcom/android/org/chromium/net/ProxyChangeListener$ProxyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProxyChangeListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/net/ProxyChangeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProxyReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/net/ProxyChangeListener;


# direct methods
.method private constructor <init>(Lcom/android/org/chromium/net/ProxyChangeListener;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/android/org/chromium/net/ProxyChangeListener$ProxyReceiver;->this$0:Lcom/android/org/chromium/net/ProxyChangeListener;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/org/chromium/net/ProxyChangeListener;Lcom/android/org/chromium/net/ProxyChangeListener$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/org/chromium/net/ProxyChangeListener;
    .param p2, "x1"    # Lcom/android/org/chromium/net/ProxyChangeListener$1;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/android/org/chromium/net/ProxyChangeListener$ProxyReceiver;-><init>(Lcom/android/org/chromium/net/ProxyChangeListener;)V

    return-void
.end method

.method private extractNewProxy(Landroid/content/Intent;)Lcom/android/org/chromium/net/ProxyChangeListener$ProxyConfig;
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 100
    :try_start_0
    const-string v0, "getHost"

    .line 101
    .local v0, "GET_HOST_NAME":Ljava/lang/String;
    const-string v1, "getPort"

    .line 104
    .local v1, "GET_PORT_NAME":Ljava/lang/String;
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x13

    if-gt v11, v12, :cond_0

    .line 105
    const-string v2, "android.net.ProxyProperties"

    .line 106
    .local v2, "className":Ljava/lang/String;
    const-string v10, "proxy"

    .line 112
    .local v10, "proxyInfo":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    invoke-virtual {v11, v10}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 113
    .local v9, "props":Ljava/lang/Object;
    if-nez v9, :cond_1

    .line 114
    const/4 v11, 0x0

    .line 139
    .end local v0    # "GET_HOST_NAME":Ljava/lang/String;
    .end local v1    # "GET_PORT_NAME":Ljava/lang/String;
    .end local v2    # "className":Ljava/lang/String;
    .end local v9    # "props":Ljava/lang/Object;
    .end local v10    # "proxyInfo":Ljava/lang/String;
    :goto_1
    return-object v11

    .line 108
    .restart local v0    # "GET_HOST_NAME":Ljava/lang/String;
    .restart local v1    # "GET_PORT_NAME":Ljava/lang/String;
    :cond_0
    const-string v2, "android.net.ProxyInfo"

    .line 109
    .restart local v2    # "className":Ljava/lang/String;
    const-string v10, "android.intent.extra.PROXY_INFO"

    .restart local v10    # "proxyInfo":Ljava/lang/String;
    goto :goto_0

    .line 117
    .restart local v9    # "props":Ljava/lang/Object;
    :cond_1
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 118
    .local v3, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v11, "getHost"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Class;

    invoke-virtual {v3, v11, v12}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 119
    .local v5, "getHostMethod":Ljava/lang/reflect/Method;
    const-string v11, "getPort"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Class;

    invoke-virtual {v3, v11, v12}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 121
    .local v6, "getPortMethod":Ljava/lang/reflect/Method;
    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v5, v9, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 122
    .local v7, "host":Ljava/lang/String;
    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v6, v9, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 124
    .local v8, "port":I
    new-instance v11, Lcom/android/org/chromium/net/ProxyChangeListener$ProxyConfig;

    invoke-direct {v11, v7, v8}, Lcom/android/org/chromium/net/ProxyChangeListener$ProxyConfig;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 125
    .end local v0    # "GET_HOST_NAME":Ljava/lang/String;
    .end local v1    # "GET_PORT_NAME":Ljava/lang/String;
    .end local v2    # "className":Ljava/lang/String;
    .end local v3    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "getHostMethod":Ljava/lang/reflect/Method;
    .end local v6    # "getPortMethod":Ljava/lang/reflect/Method;
    .end local v7    # "host":Ljava/lang/String;
    .end local v8    # "port":I
    .end local v9    # "props":Ljava/lang/Object;
    .end local v10    # "proxyInfo":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 126
    .local v4, "ex":Ljava/lang/ClassNotFoundException;
    const-string v11, "ProxyChangeListener"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Using no proxy configuration due to exception:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const/4 v11, 0x0

    goto :goto_1

    .line 128
    .end local v4    # "ex":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v4

    .line 129
    .local v4, "ex":Ljava/lang/NoSuchMethodException;
    const-string v11, "ProxyChangeListener"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Using no proxy configuration due to exception:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/4 v11, 0x0

    goto :goto_1

    .line 131
    .end local v4    # "ex":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v4

    .line 132
    .local v4, "ex":Ljava/lang/IllegalAccessException;
    const-string v11, "ProxyChangeListener"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Using no proxy configuration due to exception:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 134
    .end local v4    # "ex":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v4

    .line 135
    .local v4, "ex":Ljava/lang/reflect/InvocationTargetException;
    const-string v11, "ProxyChangeListener"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Using no proxy configuration due to exception:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 137
    .end local v4    # "ex":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v4

    .line 138
    .local v4, "ex":Ljava/lang/NullPointerException;
    const-string v11, "ProxyChangeListener"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Using no proxy configuration due to exception:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const/4 v11, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PROXY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/android/org/chromium/net/ProxyChangeListener$ProxyReceiver;->this$0:Lcom/android/org/chromium/net/ProxyChangeListener;

    invoke-direct {p0, p2}, Lcom/android/org/chromium/net/ProxyChangeListener$ProxyReceiver;->extractNewProxy(Landroid/content/Intent;)Lcom/android/org/chromium/net/ProxyChangeListener$ProxyConfig;

    move-result-object v1

    # invokes: Lcom/android/org/chromium/net/ProxyChangeListener;->proxySettingsChanged(Lcom/android/org/chromium/net/ProxyChangeListener$ProxyConfig;)V
    invoke-static {v0, v1}, Lcom/android/org/chromium/net/ProxyChangeListener;->access$000(Lcom/android/org/chromium/net/ProxyChangeListener;Lcom/android/org/chromium/net/ProxyChangeListener$ProxyConfig;)V

    .line 90
    :cond_0
    return-void
.end method
