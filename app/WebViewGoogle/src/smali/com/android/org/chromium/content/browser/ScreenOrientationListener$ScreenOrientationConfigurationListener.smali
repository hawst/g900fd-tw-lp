.class Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationConfigurationListener;
.super Ljava/lang/Object;
.source "ScreenOrientationListener.java"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationListenerBackend;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/ScreenOrientationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenOrientationConfigurationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;


# direct methods
.method private constructor <init>(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationConfigurationListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;Lcom/android/org/chromium/content/browser/ScreenOrientationListener$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/org/chromium/content/browser/ScreenOrientationListener;
    .param p2, "x1"    # Lcom/android/org/chromium/content/browser/ScreenOrientationListener$1;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationConfigurationListener;-><init>(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationConfigurationListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    # invokes: Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->notifyObservers()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->access$100(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)V

    .line 93
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public startListening()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationConfigurationListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    # getter for: Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->mAppContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->access$000(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 81
    return-void
.end method

.method public stopListening()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ScreenOrientationListener$ScreenOrientationConfigurationListener;->this$0:Lcom/android/org/chromium/content/browser/ScreenOrientationListener;

    # getter for: Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->mAppContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ScreenOrientationListener;->access$000(Lcom/android/org/chromium/content/browser/ScreenOrientationListener;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 86
    return-void
.end method
