.class public abstract Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;
.super Ljava/lang/Object;
.source "WebContentsObserverAndroid.java"


# instance fields
.field private mNativeWebContentsObserverAndroid:J


# direct methods
.method public constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 1
    .param p1, "contentViewCore"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 22
    invoke-virtual {p1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getWebContents()Lcom/android/org/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lcom/android/org/chromium/content_public/browser/WebContents;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/android/org/chromium/content_public/browser/WebContents;)V
    .locals 2
    .param p1, "webContents"    # Lcom/android/org/chromium/content_public/browser/WebContents;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 27
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;->nativeInit(Lcom/android/org/chromium/content_public/browser/WebContents;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;->mNativeWebContentsObserverAndroid:J

    .line 28
    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit(Lcom/android/org/chromium/content_public/browser/WebContents;)J
.end method


# virtual methods
.method public detachFromWebContents()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 178
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;->mNativeWebContentsObserverAndroid:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 179
    iget-wide v0, p0, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;->mNativeWebContentsObserverAndroid:J

    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;->nativeDestroy(J)V

    .line 180
    iput-wide v2, p0, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;->mNativeWebContentsObserverAndroid:J

    .line 182
    :cond_0
    return-void
.end method

.method public didAttachInterstitialPage()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public didChangeThemeColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 171
    return-void
.end method

.method public didCommitProvisionalLoadForFrame(JZLjava/lang/String;I)V
    .locals 0
    .param p1, "frameId"    # J
    .param p3, "isMainFrame"    # Z
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "transitionType"    # I

    .prologue
    .line 124
    return-void
.end method

.method public didDetachInterstitialPage()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "isProvisionalLoad"    # Z
    .param p2, "isMainFrame"    # Z
    .param p3, "errorCode"    # I
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 59
    return-void
.end method

.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 0
    .param p1, "frameId"    # J
    .param p3, "validatedUrl"    # Ljava/lang/String;
    .param p4, "isMainFrame"    # Z

    .prologue
    .line 134
    return-void
.end method

.method public didFirstVisuallyNonEmptyPaint()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public didNavigateAnyFrame(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "baseUrl"    # Ljava/lang/String;
    .param p3, "isReload"    # Z

    .prologue
    .line 89
    return-void
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "baseUrl"    # Ljava/lang/String;
    .param p3, "isNavigationToDifferentPage"    # Z
    .param p4, "isFragmentNavigation"    # Z

    .prologue
    .line 72
    return-void
.end method

.method public didStartLoading(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 40
    return-void
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 0
    .param p1, "frameId"    # J
    .param p3, "parentFrameId"    # J
    .param p5, "isMainFrame"    # Z
    .param p6, "validatedUrl"    # Ljava/lang/String;
    .param p7, "isErrorPage"    # Z
    .param p8, "isIframeSrcdoc"    # Z

    .prologue
    .line 109
    return-void
.end method

.method public didStopLoading(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 48
    return-void
.end method

.method public documentLoadedInFrame(J)V
    .locals 0
    .param p1, "frameId"    # J

    .prologue
    .line 142
    return-void
.end method

.method public navigationEntryCommitted()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public renderProcessGone(Z)V
    .locals 0
    .param p1, "wasOomProtected"    # Z

    .prologue
    .line 32
    return-void
.end method
