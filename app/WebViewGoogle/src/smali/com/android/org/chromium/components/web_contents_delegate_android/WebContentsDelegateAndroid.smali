.class public Lcom/android/org/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;
.super Ljava/lang/Object;
.source "WebContentsDelegateAndroid.java"


# instance fields
.field private mMostRecentProgress:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/org/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->mMostRecentProgress:I

    return-void
.end method

.method private final notifyLoadProgressChanged(D)V
    .locals 3
    .param p1, "progress"    # D

    .prologue
    .line 87
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    iput v0, p0, Lcom/android/org/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->mMostRecentProgress:I

    .line 88
    iget v0, p0, Lcom/android/org/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->mMostRecentProgress:I

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->onLoadProgressChanged(I)V

    .line 89
    return-void
.end method


# virtual methods
.method public activateContents()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public addMessageToConsole(ILjava/lang/String;ILjava/lang/String;)Z
    .locals 1
    .param p1, "level"    # I
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "lineNumber"    # I
    .param p4, "sourceId"    # Ljava/lang/String;

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public closeContents()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public getMostRecentProgress()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/android/org/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->mMostRecentProgress:I

    return v0
.end method

.method public handleKeyboardEvent(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 124
    return-void
.end method

.method public isFullscreenForTabOrPending()Z
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public navigationStateChanged(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 78
    return-void
.end method

.method public onLoadProgressChanged(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 95
    return-void
.end method

.method public onLoadStarted()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onLoadStopped()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public onUpdateUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 113
    return-void
.end method

.method public openNewTab(Ljava/lang/String;Ljava/lang/String;[BIZ)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "extraHeaders"    # Ljava/lang/String;
    .param p3, "postData"    # [B
    .param p4, "disposition"    # I
    .param p5, "isRendererInitiated"    # Z

    .prologue
    .line 58
    return-void
.end method

.method public rendererResponsive()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public rendererUnresponsive()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public showRepostFormWarningDialog(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p1, "contentViewCore"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 147
    return-void
.end method

.method public takeFocus(Z)Z
    .locals 1
    .param p1, "reverse"    # Z

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public toggleFullscreenModeForTab(Z)V
    .locals 0
    .param p1, "enterFullscreen"    # Z

    .prologue
    .line 151
    return-void
.end method

.method public visibleSSLStateChanged()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method
