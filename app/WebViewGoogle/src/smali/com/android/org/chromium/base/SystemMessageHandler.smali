.class Lcom/android/org/chromium/base/SystemMessageHandler;
.super Landroid/os/Handler;
.source "SystemMessageHandler.java"


# instance fields
.field private mDelayedScheduledTimeTicks:J

.field private mMessagePumpDelegateNative:J


# direct methods
.method private constructor <init>(J)V
    .locals 3
    .param p1, "messagePumpDelegateNative"    # J

    .prologue
    const-wide/16 v0, 0x0

    .line 19
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 16
    iput-wide v0, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mMessagePumpDelegateNative:J

    .line 17
    iput-wide v0, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mDelayedScheduledTimeTicks:J

    .line 20
    iput-wide p1, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mMessagePumpDelegateNative:J

    .line 21
    return-void
.end method

.method private static create(J)Lcom/android/org/chromium/base/SystemMessageHandler;
    .locals 2
    .param p0, "messagePumpDelegateNative"    # J

    .prologue
    .line 56
    new-instance v0, Lcom/android/org/chromium/base/SystemMessageHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/org/chromium/base/SystemMessageHandler;-><init>(J)V

    return-object v0
.end method

.method private native nativeDoRunLoopOnce(JJ)V
.end method

.method private removeAllPendingMessages()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/base/SystemMessageHandler;->removeMessages(I)V

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/base/SystemMessageHandler;->removeMessages(I)V

    .line 52
    return-void
.end method

.method private scheduleDelayedWork(JJ)V
    .locals 5
    .param p1, "delayedTimeTicks"    # J
    .param p3, "millis"    # J

    .prologue
    const/4 v4, 0x2

    .line 40
    iget-wide v0, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mDelayedScheduledTimeTicks:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p0, v4}, Lcom/android/org/chromium/base/SystemMessageHandler;->removeMessages(I)V

    .line 43
    :cond_0
    iput-wide p1, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mDelayedScheduledTimeTicks:J

    .line 44
    invoke-virtual {p0, v4, p3, p4}, Lcom/android/org/chromium/base/SystemMessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 45
    return-void
.end method

.method private scheduleWork()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/org/chromium/base/SystemMessageHandler;->sendEmptyMessage(I)Z

    .line 35
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 25
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mDelayedScheduledTimeTicks:J

    .line 28
    :cond_0
    iget-wide v0, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mMessagePumpDelegateNative:J

    iget-wide v2, p0, Lcom/android/org/chromium/base/SystemMessageHandler;->mDelayedScheduledTimeTicks:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/org/chromium/base/SystemMessageHandler;->nativeDoRunLoopOnce(JJ)V

    .line 29
    return-void
.end method
