.class Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;
.super Ljava/lang/Object;
.source "ContentVideoViewLegacy.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->showContentVideoView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mMediaController:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$000(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$FullScreenMediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$2;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # invokes: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->toggleMediaControlsVisiblity()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$100(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V

    .line 147
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
