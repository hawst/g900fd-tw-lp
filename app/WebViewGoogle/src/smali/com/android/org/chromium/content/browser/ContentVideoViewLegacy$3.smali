.class Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;
.super Ljava/lang/Object;
.source "ContentVideoViewLegacy.java"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->openVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canPause()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanPause:Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$200(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Z

    move-result v0

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanSeekBackward:Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$300(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Z

    move-result v0

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCanSeekForward:Z
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$400(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)Z

    move-result v0

    return v0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    # getter for: Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->mCurrentBufferPercentage:I
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->access$500(Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;)I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->getDuration()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->pause()V

    .line 238
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->seekTo(I)V

    .line 243
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy$3;->this$0:Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentVideoViewLegacy;->start()V

    .line 248
    return-void
.end method
