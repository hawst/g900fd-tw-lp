.class Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;
.super Ljava/lang/Object;
.source "AwContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/android_webview/AwContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullScreenTransitionsState"
.end annotation


# instance fields
.field private mFullScreenView:Lcom/android/org/chromium/android_webview/FullScreenView;

.field private final mInitialAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

.field private final mInitialContainerView:Landroid/view/ViewGroup;

.field private final mInitialInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;


# direct methods
.method private constructor <init>(Landroid/view/ViewGroup;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwViewMethods;)V
    .locals 0
    .param p1, "initialContainerView"    # Landroid/view/ViewGroup;
    .param p2, "initialInternalAccessAdapter"    # Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .param p3, "initialAwViewMethods"    # Lcom/android/org/chromium/android_webview/AwViewMethods;

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mInitialContainerView:Landroid/view/ViewGroup;

    .line 273
    iput-object p2, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mInitialInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    .line 274
    iput-object p3, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mInitialAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    .line 275
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/ViewGroup;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwViewMethods;Lcom/android/org/chromium/android_webview/AwContents$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .param p3, "x2"    # Lcom/android/org/chromium/android_webview/AwViewMethods;
    .param p4, "x3"    # Lcom/android/org/chromium/android_webview/AwContents$1;

    .prologue
    .line 263
    invoke-direct {p0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;-><init>(Landroid/view/ViewGroup;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwViewMethods;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->isFullScreen()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;Lcom/android/org/chromium/android_webview/FullScreenView;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;
    .param p1, "x1"    # Lcom/android/org/chromium/android_webview/FullScreenView;

    .prologue
    .line 263
    invoke-direct {p0, p1}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->enterFullScreen(Lcom/android/org/chromium/android_webview/FullScreenView;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/AwViewMethods;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialAwViewMethods()Lcom/android/org/chromium/android_webview/AwViewMethods;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/FullScreenView;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getFullScreenView()Lcom/android/org/chromium/android_webview/FullScreenView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->getInitialInternalAccessDelegate()Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->exitFullScreen()V

    return-void
.end method

.method private enterFullScreen(Lcom/android/org/chromium/android_webview/FullScreenView;)V
    .locals 0
    .param p1, "fullScreenView"    # Lcom/android/org/chromium/android_webview/FullScreenView;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mFullScreenView:Lcom/android/org/chromium/android_webview/FullScreenView;

    .line 279
    return-void
.end method

.method private exitFullScreen()V
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mFullScreenView:Lcom/android/org/chromium/android_webview/FullScreenView;

    .line 283
    return-void
.end method

.method private getFullScreenView()Lcom/android/org/chromium/android_webview/FullScreenView;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mFullScreenView:Lcom/android/org/chromium/android_webview/FullScreenView;

    return-object v0
.end method

.method private getInitialAwViewMethods()Lcom/android/org/chromium/android_webview/AwViewMethods;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mInitialAwViewMethods:Lcom/android/org/chromium/android_webview/AwViewMethods;

    return-object v0
.end method

.method private getInitialContainerView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mInitialContainerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private getInitialInternalAccessDelegate()Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mInitialInternalAccessAdapter:Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;

    return-object v0
.end method

.method private isFullScreen()Z
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContents$FullScreenTransitionsState;->mFullScreenView:Lcom/android/org/chromium/android_webview/FullScreenView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
