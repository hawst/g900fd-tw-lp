.class public Lcom/android/org/chromium/base/library_loader/Linker;
.super Ljava/lang/Object;
.source "Linker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;,
        Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sBaseLoadAddress:J

.field private static sBrowserUsesSharedRelro:Z

.field private static sCurrentLoadAddress:J

.field private static sInBrowserProcess:Z

.field private static sInitialized:Z

.field private static sLoadAtFixedAddressFailed:Z

.field private static sLoadedLibraries:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static sMemoryDeviceConfig:I

.field private static sPrepareLibraryLoadCalled:Z

.field private static sRelroSharingSupported:Z

.field private static sSharedRelros:Landroid/os/Bundle;

.field static sTestRunnerClassName:Ljava/lang/String;

.field private static sWaitForSharedRelros:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 153
    const-class v0, Lcom/android/org/chromium/base/library_loader/Linker;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    .line 188
    sput v2, Lcom/android/org/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    .line 191
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sInitialized:Z

    .line 194
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    .line 197
    sput-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    .line 201
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 205
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 208
    sput-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 211
    sput-wide v4, Lcom/android/org/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    .line 214
    sput-wide v4, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 217
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadAtFixedAddressFailed:Z

    .line 220
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sPrepareLibraryLoadCalled:Z

    .line 296
    sput-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    .line 1051
    sput-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    return-void

    :cond_0
    move v0, v2

    .line 153
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 934
    return-void
.end method

.method private static closeLibInfoMap(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1045
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1046
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;

    invoke-virtual {v2}, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;->close()V

    goto :goto_0

    .line 1048
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    :cond_0
    return-void
.end method

.method private static computeRandomBaseLoadAddress()J
    .locals 20

    .prologue
    .line 607
    const-wide/32 v6, 0x40000000

    .line 610
    .local v6, "baseAddressLimit":J
    const-wide/32 v4, 0x20000000

    .line 614
    .local v4, "baseAddress":J
    const-wide/32 v8, 0x34000000

    .line 617
    .local v8, "baseAddressMax":J
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->nativeGetPageSize()J

    move-result-wide v14

    .line 618
    .local v14, "pageSize":J
    const-wide/32 v16, 0x14000000

    div-long v16, v16, v14

    move-wide/from16 v0, v16

    long-to-int v12, v0

    .line 621
    .local v12, "offsetLimit":I
    const/16 v10, 0x1e

    .line 622
    .local v10, "numBits":I
    :goto_0
    const/4 v13, 0x1

    if-le v10, v13, :cond_0

    .line 623
    const/4 v13, 0x1

    shl-int/2addr v13, v10

    if-gt v13, v12, :cond_2

    .line 634
    :cond_0
    invoke-static {v10}, Lcom/android/org/chromium/base/library_loader/Linker;->getRandomBits(I)I

    move-result v11

    .line 635
    .local v11, "offset":I
    const-wide/16 v2, 0x0

    .line 636
    .local v2, "address":J
    if-ltz v11, :cond_1

    .line 637
    const-wide/32 v16, 0x20000000

    int-to-long v0, v11

    move-wide/from16 v18, v0

    mul-long v18, v18, v14

    add-long v2, v16, v18

    .line 644
    :cond_1
    return-wide v2

    .line 622
    .end local v2    # "address":J
    .end local v11    # "offset":I
    :cond_2
    add-int/lit8 v10, v10, -0x1

    goto :goto_0
.end method

.method private static createBundleFromLibInfoMap(Ljava/util/HashMap;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 1025
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 1026
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1027
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 1030
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    :cond_0
    return-object v0
.end method

.method private static createLibInfoMapFromBundle(Landroid/os/Bundle;)Ljava/util/HashMap;
    .locals 5
    .param p0, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1035
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1036
    .local v3, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1037
    .local v2, "library":Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;

    .line 1038
    .local v1, "libInfo":Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;
    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1040
    .end local v1    # "libInfo":Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;
    .end local v2    # "library":Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public static disableSharedRelros()V
    .locals 2

    .prologue
    .line 528
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 529
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    .line 530
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 531
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 532
    monitor-exit v1

    .line 533
    return-void

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static ensureInitializedLocked()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 224
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 226
    :cond_0
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sInitialized:Z

    if-nez v1, :cond_6

    .line 227
    sput-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    .line 228
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->USE_LINKER:Z

    if-eqz v1, :cond_4

    .line 231
    :try_start_0
    const-string v1, "chromium_android_linker"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->nativeCanUseSharedRelro()Z

    move-result v1

    sput-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    .line 237
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-nez v1, :cond_1

    .line 238
    const-string v1, "chromium_android_linker"

    const-string v4, "This system cannot safely share RELRO sections"

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_1
    sget v1, Lcom/android/org/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    if-nez v1, :cond_2

    .line 244
    invoke-static {}, Lcom/android/org/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    sput v1, Lcom/android/org/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    .line 248
    :cond_2
    packed-switch v2, :pswitch_data_0

    .line 263
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "Unreached"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 232
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 234
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "chromium_android_linker.cr"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :cond_3
    const/4 v1, 0x2

    goto :goto_1

    .line 250
    :pswitch_0
    sput-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 270
    :cond_4
    :goto_2
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-nez v1, :cond_5

    .line 272
    sput-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 273
    sput-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 276
    :cond_5
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sInitialized:Z

    .line 278
    :cond_6
    return-void

    .line 253
    :pswitch_1
    sget v1, Lcom/android/org/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    if-ne v1, v2, :cond_7

    move v1, v2

    :goto_3
    sput-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 255
    sget-boolean v1, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    if-eqz v1, :cond_4

    .line 256
    const-string v1, "chromium_android_linker"

    const-string v4, "Low-memory device: shared RELROs used in all processes"

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_7
    move v1, v3

    .line 253
    goto :goto_3

    .line 259
    :pswitch_2
    const-string v1, "chromium_android_linker"

    const-string v4, "Beware: shared RELROs used in all processes!"

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    sput-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    goto :goto_2

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static finishLibraryLoad()V
    .locals 5

    .prologue
    .line 397
    const-class v3, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v3

    .line 405
    :try_start_0
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    if-nez v2, :cond_1

    .line 440
    :cond_0
    :goto_0
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->ENABLE_LINKER_TESTS:Z

    if-eqz v2, :cond_6

    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_6

    .line 445
    const/4 v1, 0x0

    .line 447
    .local v1, "testRunner":Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    :try_start_1
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "testRunner":Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    check-cast v1, Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    .restart local v1    # "testRunner":Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    :goto_1
    if-eqz v1, :cond_6

    .line 454
    :try_start_2
    sget v2, Lcom/android/org/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    sget-boolean v4, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    invoke-interface {v1, v2, v4}, Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;->runChecks(IZ)Z

    move-result v2

    if-nez v2, :cond_5

    .line 455
    const-string v2, "chromium_android_linker"

    const-string v4, "Linker runtime tests failed in this process!!"

    invoke-static {v2, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 462
    .end local v1    # "testRunner":Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 408
    :cond_1
    :try_start_3
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v2, :cond_2

    .line 411
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/android/org/chromium/base/library_loader/Linker;->createBundleFromLibInfoMap(Ljava/util/HashMap;)Landroid/os/Bundle;

    move-result-object v2

    sput-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 417
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    if-eqz v2, :cond_2

    .line 418
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    invoke-static {v2}, Lcom/android/org/chromium/base/library_loader/Linker;->useSharedRelrosLocked(Landroid/os/Bundle;)V

    .line 422
    :cond_2
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    if-eqz v2, :cond_0

    .line 423
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 426
    :cond_3
    :goto_2
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v2, :cond_4

    .line 428
    :try_start_4
    const-class v2, Lcom/android/org/chromium/base/library_loader/Linker;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 429
    :catch_0
    move-exception v2

    goto :goto_2

    .line 433
    :cond_4
    :try_start_5
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    invoke-static {v2}, Lcom/android/org/chromium/base/library_loader/Linker;->useSharedRelrosLocked(Landroid/os/Bundle;)V

    .line 435
    sget-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    .line 436
    const/4 v2, 0x0

    sput-object v2, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    goto :goto_0

    .line 449
    :catch_1
    move-exception v0

    .line 450
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "chromium_android_linker"

    const-string v4, "Could not extract test runner class name"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 451
    const/4 v1, 0x0

    .restart local v1    # "testRunner":Lcom/android/org/chromium/base/library_loader/Linker$TestRunner;
    goto :goto_1

    .line 458
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    const-string v2, "chromium_android_linker"

    const-string v4, "All linker tests passed!"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_6
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 464
    return-void
.end method

.method public static getBaseLoadAddress()J
    .locals 3

    .prologue
    .line 566
    const-class v2, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v2

    .line 567
    :try_start_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 568
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-nez v0, :cond_0

    .line 569
    const-string v0, "chromium_android_linker"

    const-string v1, "Shared RELRO sections are disabled in this process!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    const-wide/16 v0, 0x0

    monitor-exit v2

    .line 576
    :goto_0
    return-wide v0

    .line 573
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->setupBaseLoadAddressLocked()V

    .line 576
    sget-wide v0, Lcom/android/org/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    monitor-exit v2

    goto :goto_0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static getRandomBits(I)I
    .locals 7
    .param p0, "numBits"    # I

    .prologue
    const/4 v4, -0x1

    .line 655
    sget-boolean v5, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-gtz p0, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 656
    :cond_0
    sget-boolean v5, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    const/16 v5, 0x20

    if-lt p0, v5, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 663
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    new-instance v5, Ljava/io/File;

    const-string v6, "/dev/urandom"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    .local v1, "input":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 671
    .local v3, "result":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    const/4 v5, 0x4

    if-ge v2, v5, :cond_2

    .line 672
    shl-int/lit8 v5, v3, 0x8

    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->read()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    and-int/lit16 v6, v6, 0xff

    or-int v3, v5, v6

    .line 671
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 664
    .end local v1    # "input":Ljava/io/FileInputStream;
    .end local v2    # "n":I
    .end local v3    # "result":I
    :catch_0
    move-exception v0

    .line 665
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "chromium_android_linker"

    const-string v6, "Could not open /dev/urandom"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v3, v4

    .line 691
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return v3

    .line 679
    .restart local v1    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "n":I
    .restart local v3    # "result":I
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 684
    :goto_2
    const/4 v4, 0x1

    shl-int/2addr v4, p0

    add-int/lit8 v4, v4, -0x1

    and-int/2addr v3, v4

    .line 691
    goto :goto_1

    .line 674
    :catch_1
    move-exception v0

    .line 675
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "chromium_android_linker"

    const-string v6, "Could not read /dev/urandom"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 679
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :goto_3
    move v3, v4

    .line 682
    goto :goto_1

    .line 678
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 679
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 682
    :goto_4
    throw v4

    .line 680
    :catch_2
    move-exception v4

    goto :goto_2

    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v5

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    goto :goto_4
.end method

.method public static getSharedRelros()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 509
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 510
    :try_start_0
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-nez v0, :cond_0

    .line 512
    const/4 v0, 0x0

    monitor-exit v1

    .line 517
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    monitor-exit v1

    goto :goto_0

    .line 518
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getTestRunnerClassName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 326
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 327
    :try_start_0
    sget-object v0, Lcom/android/org/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static initServiceProcess(J)V
    .locals 2
    .param p0, "baseLoadAddress"    # J

    .prologue
    .line 546
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 547
    :try_start_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 548
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    .line 549
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 550
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-eqz v0, :cond_0

    .line 551
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 552
    sput-wide p0, Lcom/android/org/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    .line 553
    sput-wide p0, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 555
    :cond_0
    monitor-exit v1

    .line 556
    return-void

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isUsed()Z
    .locals 2

    .prologue
    .line 363
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->USE_LINKER:Z

    if-nez v0, :cond_0

    .line 364
    const/4 v0, 0x0

    .line 370
    :goto_0
    return v0

    .line 366
    :cond_0
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 367
    :try_start_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 370
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    monitor-exit v1

    goto :goto_0

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static loadAtFixedAddressFailed()Z
    .locals 1

    .prologue
    .line 753
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadAtFixedAddressFailed:Z

    return v0
.end method

.method public static loadLibrary(Ljava/lang/String;)V
    .locals 14
    .param p0, "library"    # Ljava/lang/String;

    .prologue
    .line 770
    const-string v3, "chromium_android_linker"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "chromium_android_linker.cr"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 853
    :cond_0
    :goto_0
    return-void

    .line 775
    :cond_1
    const-class v6, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v6

    .line 776
    :try_start_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 782
    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sPrepareLibraryLoadCalled:Z

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 852
    :catchall_0
    move-exception v3

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 784
    :cond_2
    :try_start_1
    invoke-static {p0}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 786
    .local v2, "libName":Ljava/lang/String;
    sget-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    if-nez v3, :cond_3

    .line 787
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    .line 789
    :cond_3
    sget-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 791
    monitor-exit v6

    goto :goto_0

    .line 794
    :cond_4
    new-instance v1, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;

    invoke-direct {v1}, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;-><init>()V

    .line 795
    .local v1, "libInfo":Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;
    const-wide/16 v4, 0x0

    .line 796
    .local v4, "loadAddress":J
    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    if-nez v3, :cond_6

    :cond_5
    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    if-eqz v3, :cond_7

    .line 798
    :cond_6
    sget-wide v4, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 801
    :cond_7
    invoke-static {v2, v4, v5, v1}, Lcom/android/org/chromium/base/library_loader/Linker;->nativeLoadLibrary(Ljava/lang/String;JLcom/android/org/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 802
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to load library: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 803
    .local v0, "errorMessage":Ljava/lang/String;
    const-string v3, "chromium_android_linker"

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    new-instance v3, Ljava/lang/UnsatisfiedLinkError;

    invoke-direct {v3, v0}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v3

    .line 807
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_8
    const-wide/16 v8, 0x0

    cmp-long v3, v4, v8

    if-eqz v3, :cond_9

    iget-wide v8, v1, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;->mLoadAddress:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_9

    .line 808
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadAtFixedAddressFailed:Z

    .line 816
    :cond_9
    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->ENABLE_LINKER_TESTS:Z

    if-eqz v3, :cond_a

    .line 817
    const-string v7, "chromium_android_linker"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%s_LIBRARY_ADDRESS: %s %x"

    const/4 v3, 0x3

    new-array v10, v3, [Ljava/lang/Object;

    const/4 v11, 0x0

    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v3, :cond_d

    const-string v3, "BROWSER"

    :goto_1
    aput-object v3, v10, v11

    const/4 v3, 0x1

    aput-object v2, v10, v3

    const/4 v3, 0x2

    iget-wide v12, v1, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;->mLoadAddress:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v10, v3

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    :cond_a
    sget-boolean v3, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v3, :cond_b

    .line 827
    sget-wide v8, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    invoke-static {v2, v8, v9, v1}, Lcom/android/org/chromium/base/library_loader/Linker;->nativeCreateSharedRelro(Ljava/lang/String;JLcom/android/org/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 828
    const-string v3, "chromium_android_linker"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Could not create shared RELRO for %s at %x"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    const/4 v10, 0x1

    sget-wide v12, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    :cond_b
    sget-wide v8, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-eqz v3, :cond_c

    .line 847
    iget-wide v8, v1, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;->mLoadAddress:J

    iget-wide v10, v1, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;->mLoadSize:J

    add-long/2addr v8, v10

    sput-wide v8, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 850
    :cond_c
    sget-object v3, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 852
    monitor-exit v6

    goto/16 :goto_0

    .line 817
    :cond_d
    const-string v3, "RENDERER"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private static native nativeCanUseSharedRelro()Z
.end method

.method private static native nativeCreateSharedRelro(Ljava/lang/String;JLcom/android/org/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method private static native nativeGetPageSize()J
.end method

.method private static native nativeLoadLibrary(Ljava/lang/String;JLcom/android/org/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method private static native nativeRunCallbackOnUiThread(J)V
.end method

.method private static native nativeUseSharedRelro(Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method public static prepareLibraryLoad()V
    .locals 2

    .prologue
    .line 379
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 380
    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sPrepareLibraryLoadCalled:Z

    .line 382
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_0

    .line 385
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->setupBaseLoadAddressLocked()V

    .line 387
    :cond_0
    monitor-exit v1

    .line 388
    return-void

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setTestRunnerClassName(Ljava/lang/String;)V
    .locals 2
    .param p0, "testRunnerClassName"    # Ljava/lang/String;

    .prologue
    .line 307
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/NativeLibraries;->ENABLE_LINKER_TESTS:Z

    if-nez v0, :cond_0

    .line 316
    :goto_0
    return-void

    .line 312
    :cond_0
    const-class v1, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 313
    :try_start_0
    sget-boolean v0, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/org/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 314
    :cond_1
    :try_start_1
    sput-object p0, Lcom/android/org/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    .line 315
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static setupBaseLoadAddressLocked()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 582
    sget-boolean v2, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const-class v2, Lcom/android/org/chromium/base/library_loader/Linker;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 583
    :cond_0
    sget-wide v2, Lcom/android/org/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_1

    .line 584
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/Linker;->computeRandomBaseLoadAddress()J

    move-result-wide v0

    .line 585
    .local v0, "address":J
    sput-wide v0, Lcom/android/org/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    .line 586
    sput-wide v0, Lcom/android/org/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 587
    cmp-long v2, v0, v6

    if-nez v2, :cond_1

    .line 590
    const-string v2, "chromium_android_linker"

    const-string v3, "Disabling shared RELROs due to bad entropy sources"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    sput-boolean v4, Lcom/android/org/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 592
    sput-boolean v4, Lcom/android/org/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 595
    :cond_1
    return-void
.end method

.method public static useSharedRelros(Landroid/os/Bundle;)V
    .locals 4
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 478
    const/4 v0, 0x0

    .line 479
    .local v0, "clonedBundle":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 480
    const-class v2, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 481
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "clonedBundle":Landroid/os/Bundle;
    const-class v2, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 482
    .restart local v0    # "clonedBundle":Landroid/os/Bundle;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 483
    .local v1, "parcel":Landroid/os/Parcel;
    invoke-virtual {p0, v1, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 484
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 485
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V

    .line 486
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 492
    .end local v1    # "parcel":Landroid/os/Parcel;
    :cond_0
    const-class v3, Lcom/android/org/chromium/base/library_loader/Linker;

    monitor-enter v3

    .line 495
    :try_start_0
    sput-object v0, Lcom/android/org/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 497
    const-class v2, Lcom/android/org/chromium/base/library_loader/Linker;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 498
    monitor-exit v3

    .line 499
    return-void

    .line 498
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static useSharedRelrosLocked(Landroid/os/Bundle;)V
    .locals 8
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 707
    sget-boolean v5, Lcom/android/org/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    const-class v5, Lcom/android/org/chromium/base/library_loader/Linker;

    invoke-static {v5}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 711
    :cond_0
    if-nez p0, :cond_2

    .line 745
    :cond_1
    :goto_0
    return-void

    .line 716
    :cond_2
    sget-boolean v5, Lcom/android/org/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-eqz v5, :cond_1

    .line 721
    sget-object v5, Lcom/android/org/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    if-eqz v5, :cond_1

    .line 727
    invoke-static {p0}, Lcom/android/org/chromium/base/library_loader/Linker;->createLibInfoMapFromBundle(Landroid/os/Bundle;)Ljava/util/HashMap;

    move-result-object v4

    .line 730
    .local v4, "relroMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 731
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 732
    .local v3, "libName":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;

    .line 733
    .local v2, "libInfo":Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;
    invoke-static {v3, v2}, Lcom/android/org/chromium/base/library_loader/Linker;->nativeUseSharedRelro(Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 734
    const-string v5, "chromium_android_linker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not use shared RELRO section for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 741
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;>;"
    .end local v2    # "libInfo":Lcom/android/org/chromium/base/library_loader/Linker$LibInfo;
    .end local v3    # "libName":Ljava/lang/String;
    :cond_4
    sget-boolean v5, Lcom/android/org/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-nez v5, :cond_1

    .line 742
    invoke-static {v4}, Lcom/android/org/chromium/base/library_loader/Linker;->closeLibInfoMap(Ljava/util/HashMap;)V

    goto :goto_0
.end method
