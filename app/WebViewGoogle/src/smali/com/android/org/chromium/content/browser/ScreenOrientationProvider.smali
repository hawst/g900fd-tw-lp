.class Lcom/android/org/chromium/content/browser/ScreenOrientationProvider;
.super Ljava/lang/Object;
.source "ScreenOrientationProvider.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method

.method static create()Lcom/android/org/chromium/content/browser/ScreenOrientationProvider;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/android/org/chromium/content/browser/ScreenOrientationProvider;

    invoke-direct {v0}, Lcom/android/org/chromium/content/browser/ScreenOrientationProvider;-><init>()V

    return-object v0
.end method

.method private getOrientationFromWebScreenOrientations(B)I
    .locals 3
    .param p1, "orientations"    # B

    .prologue
    const/4 v0, -0x1

    .line 26
    packed-switch p1, :pswitch_data_0

    .line 44
    const-string v1, "ScreenOrientationProvider"

    const-string v2, "Trying to lock to unsupported orientation!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :goto_0
    :pswitch_0
    return v0

    .line 30
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 32
    :pswitch_2
    const/16 v0, 0x9

    goto :goto_0

    .line 34
    :pswitch_3
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    :pswitch_4
    const/16 v0, 0x8

    goto :goto_0

    .line 38
    :pswitch_5
    const/4 v0, 0x7

    goto :goto_0

    .line 40
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 42
    :pswitch_7
    const/16 v0, 0xa

    goto :goto_0

    .line 26
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method lockOrientation(B)V
    .locals 3
    .param p1, "orientations"    # B

    .prologue
    .line 57
    invoke-static {}, Lcom/android/org/chromium/base/ApplicationStatus;->getLastTrackedFocusedActivity()Landroid/app/Activity;

    move-result-object v0

    .line 58
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/org/chromium/content/browser/ScreenOrientationProvider;->getOrientationFromWebScreenOrientations(B)I

    move-result v1

    .line 63
    .local v1, "orientation":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 67
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method unlockOrientation()V
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/android/org/chromium/base/ApplicationStatus;->getLastTrackedFocusedActivity()Landroid/app/Activity;

    move-result-object v0

    .line 73
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method
