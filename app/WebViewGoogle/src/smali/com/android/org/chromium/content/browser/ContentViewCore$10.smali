.class Lcom/android/org/chromium/content/browser/ContentViewCore$10;
.super Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
.source "ContentViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore;->getInsertionHandleController()Lcom/android/org/chromium/content/browser/input/InsertionHandleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;
    .param p3, "x1"    # Lcom/android/org/chromium/content/browser/PositionObserver;

    .prologue
    .line 2042
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, p2, p3}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;-><init>(Landroid/view/View;Lcom/android/org/chromium/content/browser/PositionObserver;)V

    return-void
.end method


# virtual methods
.method public getLineHeight()I
    .locals 2

    .prologue
    .line 2061
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$200(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move-result-object v0

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->fromLocalCssToPix(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public paste()V
    .locals 1

    .prologue
    .line 2055
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mImeAdapter:Lcom/android/org/chromium/content/browser/input/ImeAdapter;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$2100(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/input/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/input/ImeAdapter;->paste()Z

    .line 2056
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->hideHandles()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$300(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 2057
    return-void
.end method

.method public setCursorPosition(II)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2047
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1400(Lcom/android/org/chromium/content/browser/ContentViewCore;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2048
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:J
    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$1400(Lcom/android/org/chromium/content/browser/ContentViewCore;)J

    move-result-wide v2

    int-to-float v1, p1

    int-to-float v4, p2

    iget-object v5, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$10;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mRenderCoordinates:Lcom/android/org/chromium/content/browser/RenderCoordinates;
    invoke-static {v5}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$200(Lcom/android/org/chromium/content/browser/ContentViewCore;)Lcom/android/org/chromium/content/browser/RenderCoordinates;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/org/chromium/content/browser/RenderCoordinates;->getContentOffsetYPix()F

    move-result v5

    sub-float/2addr v4, v5

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->nativeMoveCaret(JFF)V
    invoke-static {v0, v2, v3, v1, v4}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$2000(Lcom/android/org/chromium/content/browser/ContentViewCore;JFF)V

    .line 2051
    :cond_0
    return-void
.end method

.method public showHandle()V
    .locals 0

    .prologue
    .line 2067
    invoke-super {p0}, Lcom/android/org/chromium/content/browser/input/InsertionHandleController;->showHandle()V

    .line 2068
    return-void
.end method
