.class Lcom/android/org/chromium/content/browser/input/SelectPopupDialog$1;
.super Ljava/lang/Object;
.source "SelectPopupDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;Ljava/util/List;Z[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;

    iput-object p2, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog$1;->val$listView:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;

    # getter for: Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;->mContentViewCore:Lcom/android/org/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;->access$100(Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;)Lcom/android/org/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog$1;->this$0:Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;

    iget-object v2, p0, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog$1;->val$listView:Landroid/widget/ListView;

    # invokes: Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;->getSelectedIndices(Landroid/widget/ListView;)[I
    invoke-static {v1, v2}, Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;->access$000(Lcom/android/org/chromium/content/browser/input/SelectPopupDialog;Landroid/widget/ListView;)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore;->selectPopupMenuItems([I)V

    .line 54
    return-void
.end method
