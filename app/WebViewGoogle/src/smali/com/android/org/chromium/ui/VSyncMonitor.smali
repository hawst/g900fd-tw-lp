.class public Lcom/android/org/chromium/ui/VSyncMonitor;
.super Ljava/lang/Object;
.source "VSyncMonitor.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/ui/VSyncMonitor$Listener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mChoreographer:Landroid/view/Choreographer;

.field private mGoodStartingPointNano:J

.field private final mHandler:Landroid/os/Handler;

.field private mHaveRequestInFlight:Z

.field private mLastPostedNano:J

.field private mLastVSyncCpuTimeNano:J

.field private mListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

.field private final mRefreshPeriodNano:J

.field private final mSyntheticVSyncRunnable:Ljava/lang/Runnable;

.field private mTriggerNextVSyncCount:I

.field private final mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

.field private final mVSyncRunnableCallback:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/android/org/chromium/ui/VSyncMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/org/chromium/ui/VSyncMonitor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/org/chromium/ui/VSyncMonitor$Listener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/org/chromium/ui/VSyncMonitor;-><init>(Landroid/content/Context;Lcom/android/org/chromium/ui/VSyncMonitor$Listener;Z)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/org/chromium/ui/VSyncMonitor$Listener;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/android/org/chromium/ui/VSyncMonitor$Listener;
    .param p3, "enableJBVSync"    # Z

    .prologue
    const/4 v4, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHandler:Landroid/os/Handler;

    .line 82
    iput-object p2, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

    .line 83
    const-string v1, "window"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRefreshRate()F

    move-result v0

    .line 85
    .local v0, "refreshRate":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    const/high16 v0, 0x42700000    # 60.0f

    .line 86
    :cond_0
    const v1, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v1, v0

    float-to-long v2, v1

    iput-wide v2, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    .line 87
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mTriggerNextVSyncCount:I

    .line 89
    if-eqz p3, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 91
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mChoreographer:Landroid/view/Choreographer;

    .line 92
    new-instance v1, Lcom/android/org/chromium/ui/VSyncMonitor$1;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/ui/VSyncMonitor$1;-><init>(Lcom/android/org/chromium/ui/VSyncMonitor;)V

    iput-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    .line 101
    iput-object v4, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncRunnableCallback:Ljava/lang/Runnable;

    .line 117
    :goto_0
    new-instance v1, Lcom/android/org/chromium/ui/VSyncMonitor$3;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/ui/VSyncMonitor$3;-><init>(Lcom/android/org/chromium/ui/VSyncMonitor;)V

    iput-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mSyntheticVSyncRunnable:Ljava/lang/Runnable;

    .line 126
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->getCurrentNanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mGoodStartingPointNano:J

    .line 127
    return-void

    .line 104
    :cond_1
    iput-object v4, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mChoreographer:Landroid/view/Choreographer;

    .line 105
    iput-object v4, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    .line 106
    new-instance v1, Lcom/android/org/chromium/ui/VSyncMonitor$2;

    invoke-direct {v1, p0}, Lcom/android/org/chromium/ui/VSyncMonitor$2;-><init>(Lcom/android/org/chromium/ui/VSyncMonitor;)V

    iput-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncRunnableCallback:Ljava/lang/Runnable;

    .line 115
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mLastPostedNano:J

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/android/org/chromium/ui/VSyncMonitor;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/ui/VSyncMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 25
    iput-wide p1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mGoodStartingPointNano:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/android/org/chromium/ui/VSyncMonitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/org/chromium/ui/VSyncMonitor;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->getCurrentNanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$200(Lcom/android/org/chromium/ui/VSyncMonitor;JJ)V
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/ui/VSyncMonitor;
    .param p1, "x1"    # J
    .param p3, "x2"    # J

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/org/chromium/ui/VSyncMonitor;->onVSyncCallback(JJ)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/org/chromium/ui/VSyncMonitor;J)J
    .locals 3
    .param p0, "x0"    # Lcom/android/org/chromium/ui/VSyncMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/android/org/chromium/ui/VSyncMonitor;->estimateLastVSyncTime(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private estimateLastVSyncTime(J)J
    .locals 9
    .param p1, "currentTime"    # J

    .prologue
    .line 208
    iget-wide v2, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mGoodStartingPointNano:J

    iget-wide v4, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mGoodStartingPointNano:J

    sub-long v4, p1, v4

    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    div-long/2addr v4, v6

    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    mul-long/2addr v4, v6

    add-long v0, v2, v4

    .line 210
    .local v0, "lastRefreshTime":J
    return-wide v0
.end method

.method private getCurrentNanoTime()J
    .locals 2

    .prologue
    .line 170
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method private isVSyncSignalAvailable()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mChoreographer:Landroid/view/Choreographer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onVSyncCallback(JJ)V
    .locals 5
    .param p1, "frameTimeNanos"    # J
    .param p3, "currentTimeNanos"    # J

    .prologue
    .line 174
    sget-boolean v0, Lcom/android/org/chromium/ui/VSyncMonitor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHaveRequestInFlight:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 175
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHaveRequestInFlight:Z

    .line 176
    iput-wide p3, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mLastVSyncCpuTimeNano:J

    .line 177
    iget v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mTriggerNextVSyncCount:I

    if-ltz v0, :cond_1

    .line 178
    iget v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mTriggerNextVSyncCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mTriggerNextVSyncCount:I

    .line 179
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->postCallback()V

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

    if-eqz v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mListener:Lcom/android/org/chromium/ui/VSyncMonitor$Listener;

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    invoke-interface {v0, p0, v2, v3}, Lcom/android/org/chromium/ui/VSyncMonitor$Listener;->onVSync(Lcom/android/org/chromium/ui/VSyncMonitor;J)V

    .line 184
    :cond_2
    return-void
.end method

.method private postCallback()V
    .locals 2

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHaveRequestInFlight:Z

    if-eqz v0, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHaveRequestInFlight:Z

    .line 189
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->postSyntheticVSync()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->isVSyncSignalAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mChoreographer:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0

    .line 193
    :cond_2
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->postRunnableCallback()V

    goto :goto_0
.end method

.method private postRunnableCallback()V
    .locals 14

    .prologue
    .line 214
    sget-boolean v6, Lcom/android/org/chromium/ui/VSyncMonitor;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->isVSyncSignalAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 215
    :cond_0
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->getCurrentNanoTime()J

    move-result-wide v0

    .line 216
    .local v0, "currentTime":J
    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/ui/VSyncMonitor;->estimateLastVSyncTime(J)J

    move-result-wide v4

    .line 217
    .local v4, "lastRefreshTime":J
    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    add-long/2addr v6, v4

    sub-long v2, v6, v0

    .line 218
    .local v2, "delay":J
    sget-boolean v6, Lcom/android/org/chromium/ui/VSyncMonitor;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    cmp-long v6, v2, v6

    if-lez v6, :cond_2

    :cond_1
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 220
    :cond_2
    add-long v6, v0, v2

    iget-wide v8, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mLastPostedNano:J

    iget-wide v10, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    const-wide/16 v12, 0x2

    div-long/2addr v10, v12

    add-long/2addr v8, v10

    cmp-long v6, v6, v8

    if-gtz v6, :cond_3

    .line 221
    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    add-long/2addr v2, v6

    .line 224
    :cond_3
    add-long v6, v0, v2

    iput-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mLastPostedNano:J

    .line 225
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncRunnableCallback:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 227
    :goto_0
    return-void

    .line 226
    :cond_4
    iget-object v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mVSyncRunnableCallback:Ljava/lang/Runnable;

    const-wide/32 v8, 0xf4240

    div-long v8, v2, v8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private postSyntheticVSync()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x2

    const/4 v2, 0x0

    .line 198
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->getCurrentNanoTime()J

    move-result-wide v0

    .line 201
    .local v0, "currentTime":J
    iget-wide v4, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mLastVSyncCpuTimeNano:J

    sub-long v4, v0, v4

    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    mul-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v2

    .line 202
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/android/org/chromium/ui/VSyncMonitor;->estimateLastVSyncTime(J)J

    move-result-wide v4

    sub-long v4, v0, v4

    iget-wide v6, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    div-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 203
    iget-object v2, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mSyntheticVSyncRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 204
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getVSyncPeriodInMicroseconds()J
    .locals 4

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mRefreshPeriodNano:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public requestUpdate()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/org/chromium/ui/VSyncMonitor;->mTriggerNextVSyncCount:I

    .line 158
    invoke-direct {p0}, Lcom/android/org/chromium/ui/VSyncMonitor;->postCallback()V

    .line 159
    return-void
.end method
