.class public Lcom/android/org/chromium/android_webview/AwContentViewClient;
.super Lcom/android/org/chromium/content/browser/ContentViewClient;
.source "AwContentViewClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/chromium/android_webview/AwContentViewClient$1;,
        Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;
    }
.end annotation


# instance fields
.field private mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

.field private mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

.field private mAwSettings:Lcom/android/org/chromium/android_webview/AwSettings;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/android_webview/AwSettings;Lcom/android/org/chromium/android_webview/AwContents;Landroid/content/Context;)V
    .locals 0
    .param p1, "awContentsClient"    # Lcom/android/org/chromium/android_webview/AwContentsClient;
    .param p2, "awSettings"    # Lcom/android/org/chromium/android_webview/AwSettings;
    .param p3, "awContents"    # Lcom/android/org/chromium/android_webview/AwContents;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/org/chromium/content/browser/ContentViewClient;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    .line 92
    iput-object p2, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    .line 93
    iput-object p3, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    .line 94
    iput-object p4, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mContext:Landroid/content/Context;

    .line 95
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContentViewClient;->areHtmlControlsEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContentsClient;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContentViewClient;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContentViewClient;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/org/chromium/android_webview/AwContentViewClient;)Lcom/android/org/chromium/android_webview/AwContents;
    .locals 1
    .param p0, "x0"    # Lcom/android/org/chromium/android_webview/AwContentViewClient;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    return-object v0
.end method

.method private static areHtmlControlsEnabled()Z
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Lcom/android/org/chromium/base/CommandLine;->getInstance()Lcom/android/org/chromium/base/CommandLine;

    move-result-object v0

    const-string v1, "disable-overlay-fullscreen-video-subtitle"

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getContentVideoViewClient()Lcom/android/org/chromium/content/browser/ContentVideoViewClient;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/org/chromium/android_webview/AwContentViewClient$AwContentVideoViewClient;-><init>(Lcom/android/org/chromium/android_webview/AwContentViewClient;Lcom/android/org/chromium/android_webview/AwContentViewClient$1;)V

    return-object v0
.end method

.method public onBackgroundColorChanged(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onBackgroundColorChanged(I)V

    .line 100
    return-void
.end method

.method public onStartContentIntent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentUrl"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p2}, Lcom/android/org/chromium/android_webview/AwContentsClient;->shouldOverrideUrlLoading(Ljava/lang/String;)Z

    .line 106
    return-void
.end method

.method public onUpdateTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onReceivedTitle(Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public shouldBlockMediaRequest(Ljava/lang/String;)Z
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 125
    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwSettings:Lcom/android/org/chromium/android_webview/AwSettings;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwSettings;->getBlockNetworkLoads()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldOverrideKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentViewClient;->mAwContentsClient:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->shouldOverrideKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
