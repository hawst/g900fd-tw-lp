.class Lcom/android/org/chromium/content/browser/ContentViewCore$2;
.super Ljava/lang/Object;
.source "ContentViewCore.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/input/ImeAdapter$ImeAdapterDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/org/chromium/content/browser/ContentViewCore;->createImeAdapter(Landroid/content/Context;)Lcom/android/org/chromium/content/browser/input/ImeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 542
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$2;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttachedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$2;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # getter for: Lcom/android/org/chromium/content/browser/ContentViewCore;->mContainerView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$000(Lcom/android/org/chromium/content/browser/ContentViewCore;)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public getNewShowKeyboardReceiver()Landroid/os/ResultReceiver;
    .locals 2

    .prologue
    .line 563
    new-instance v0, Lcom/android/org/chromium/content/browser/ContentViewCore$2$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/org/chromium/content/browser/ContentViewCore$2$1;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore$2;Landroid/os/Handler;)V

    return-object v0
.end method

.method public onDismissInput()V
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$2;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onImeStateChangeRequested(Z)V

    .line 554
    return-void
.end method

.method public onImeEvent(Z)V
    .locals 1
    .param p1, "isFinish"    # Z

    .prologue
    .line 545
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$2;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->getContentViewClient()Lcom/android/org/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/org/chromium/content/browser/ContentViewClient;->onImeEvent()V

    .line 546
    if-nez p1, :cond_0

    .line 547
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/ContentViewCore$2;->this$0:Lcom/android/org/chromium/content/browser/ContentViewCore;

    # invokes: Lcom/android/org/chromium/content/browser/ContentViewCore;->hideHandles()V
    invoke-static {v0}, Lcom/android/org/chromium/content/browser/ContentViewCore;->access$300(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 549
    :cond_0
    return-void
.end method
