.class Lcom/android/org/chromium/content/browser/input/InputDialogContainer$FullTimeListener;
.super Ljava/lang/Object;
.source "InputDialogContainer.java"

# interfaces
.implements Lcom/android/org/chromium/content/browser/input/MultiFieldTimePickerDialog$OnMultiFieldTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/content/browser/input/InputDialogContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FullTimeListener"
.end annotation


# instance fields
.field private final mDialogType:I

.field final synthetic this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;


# direct methods
.method constructor <init>(Lcom/android/org/chromium/content/browser/input/InputDialogContainer;I)V
    .locals 0
    .param p2, "dialogType"    # I

    .prologue
    .line 308
    iput-object p1, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$FullTimeListener;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    iput p2, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$FullTimeListener;->mDialogType:I

    .line 310
    return-void
.end method


# virtual methods
.method public onTimeSet(IIII)V
    .locals 10
    .param p1, "hourOfDay"    # I
    .param p2, "minute"    # I
    .param p3, "second"    # I
    .param p4, "milli"    # I

    .prologue
    const/4 v2, 0x0

    .line 314
    iget-object v0, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$FullTimeListener;->this$0:Lcom/android/org/chromium/content/browser/input/InputDialogContainer;

    iget v1, p0, Lcom/android/org/chromium/content/browser/input/InputDialogContainer$FullTimeListener;->mDialogType:I

    move v3, v2

    move v4, v2

    move v5, p1

    move v6, p2

    move v7, p3

    move v8, p4

    move v9, v2

    invoke-virtual/range {v0 .. v9}, Lcom/android/org/chromium/content/browser/input/InputDialogContainer;->setFieldDateTimeValue(IIIIIIIII)V

    .line 315
    return-void
.end method
