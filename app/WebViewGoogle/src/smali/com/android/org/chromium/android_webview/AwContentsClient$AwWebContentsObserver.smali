.class Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;
.super Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;
.source "AwContentsClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/org/chromium/android_webview/AwContentsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AwWebContentsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;


# direct methods
.method public constructor <init>(Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/content/browser/ContentViewCore;)V
    .locals 0
    .param p2, "contentViewCore"    # Lcom/android/org/chromium/content/browser/ContentViewCore;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;->this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;

    .line 60
    invoke-direct {p0, p2}, Lcom/android/org/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lcom/android/org/chromium/content/browser/ContentViewCore;)V

    .line 61
    return-void
.end method


# virtual methods
.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "isProvisionalLoad"    # Z
    .param p2, "isMainFrame"    # Z
    .param p3, "errorCode"    # I
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 76
    if-eqz p2, :cond_1

    .line 77
    const/4 v0, -0x3

    if-eq p3, v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;->this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-static {p3}, Lcom/android/org/chromium/android_webview/ErrorCodeConversionHelper;->convertErrorCode(I)I

    move-result v1

    invoke-virtual {v0, v1, p4, p5}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onReceivedError(ILjava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;->this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p5}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onPageFinished(Ljava/lang/String;)V

    .line 92
    :cond_1
    return-void
.end method

.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 3
    .param p1, "frameId"    # J
    .param p3, "validatedUrl"    # Ljava/lang/String;
    .param p4, "isMainFrame"    # Z

    .prologue
    .line 65
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContentsStatics;->getUnreachableWebDataUrl()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "unreachableWebDataUrl":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 68
    .local v0, "isErrorUrl":Z
    :goto_0
    if-eqz p4, :cond_0

    if-nez v0, :cond_0

    .line 69
    iget-object v2, p0, Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;->this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v2, p3}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onPageFinished(Ljava/lang/String;)V

    .line 71
    :cond_0
    return-void

    .line 66
    .end local v0    # "isErrorUrl":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public didNavigateAnyFrame(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "baseUrl"    # Ljava/lang/String;
    .param p3, "isReload"    # Z

    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;->this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1, p3}, Lcom/android/org/chromium/android_webview/AwContentsClient;->doUpdateVisitedHistory(Ljava/lang/String;Z)V

    .line 107
    return-void
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "baseUrl"    # Ljava/lang/String;
    .param p3, "isNavigationToDifferentPage"    # Z
    .param p4, "isFragmentNavigation"    # Z

    .prologue
    .line 99
    if-eqz p4, :cond_0

    .line 100
    iget-object v0, p0, Lcom/android/org/chromium/android_webview/AwContentsClient$AwWebContentsObserver;->this$0:Lcom/android/org/chromium/android_webview/AwContentsClient;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContentsClient;->onPageFinished(Ljava/lang/String;)V

    .line 102
    :cond_0
    return-void
.end method
