.class Lcom/android/webview/chromium/WebViewChromium;
.super Ljava/lang/Object;
.source "WebViewChromium.java"

# interfaces
.implements Landroid/webkit/WebViewProvider$ScrollDelegate;
.implements Landroid/webkit/WebViewProvider$ViewDelegate;
.implements Landroid/webkit/WebViewProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/webview/chromium/WebViewChromium$InternalAccessAdapter;,
        Lcom/android/webview/chromium/WebViewChromium$WebViewNativeGLDelegate;,
        Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;

.field private static sRecordWholeDocumentEnabledByApi:Z


# instance fields
.field private final mAppTargetSdkVersion:I

.field private mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

.field private mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

.field private mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

.field private mGLfunctor:Lcom/android/webview/chromium/DrawGLFunctor;

.field private final mHitTestResult:Landroid/webkit/WebView$HitTestResult;

.field private mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

.field private mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

.field mWebView:Landroid/webkit/WebView;

.field mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    const-class v0, Lcom/android/webview/chromium/WebViewChromium;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/webview/chromium/WebViewChromium;->$assertionsDisabled:Z

    .line 134
    const-class v0, Lcom/android/webview/chromium/WebViewChromium;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/webview/chromium/WebViewChromium;->TAG:Ljava/lang/String;

    .line 156
    sput-boolean v1, Lcom/android/webview/chromium/WebViewChromium;->sRecordWholeDocumentEnabledByApi:Z

    return-void

    :cond_0
    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)V
    .locals 2
    .param p1, "factory"    # Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;
    .param p2, "webView"    # Landroid/webkit/WebView;
    .param p3, "webViewPrivate"    # Landroid/webkit/WebView$PrivateAccess;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p2, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    .line 166
    iput-object p3, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    .line 167
    new-instance v1, Landroid/webkit/WebView$HitTestResult;

    invoke-direct {v1}, Landroid/webkit/WebView$HitTestResult;-><init>()V

    iput-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mHitTestResult:Landroid/webkit/WebView$HitTestResult;

    .line 168
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    iput v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    .line 169
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    .line 170
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    iput-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    .line 171
    invoke-static {}, Landroid/webkit/WebViewFactory;->getLoadedPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 172
    .local v0, "webViewAssetPath":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    .line 173
    return-void
.end method

.method static synthetic access$000(Lcom/android/webview/chromium/WebViewChromium;)Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;
    .locals 1
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromium;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/webview/chromium/WebViewChromium;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromium;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->initForReal()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/webview/chromium/WebViewChromium;)Lcom/android/org/chromium/android_webview/AwContents;
    .locals 1
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromium;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/webview/chromium/WebViewChromium;)Lcom/android/webview/chromium/DrawGLFunctor;
    .locals 1
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromium;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mGLfunctor:Lcom/android/webview/chromium/DrawGLFunctor;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/webview/chromium/WebViewChromium;Lcom/android/webview/chromium/DrawGLFunctor;)Lcom/android/webview/chromium/DrawGLFunctor;
    .locals 0
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromium;
    .param p1, "x1"    # Lcom/android/webview/chromium/DrawGLFunctor;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewChromium;->mGLfunctor:Lcom/android/webview/chromium/DrawGLFunctor;

    return-object p1
.end method

.method private checkNeedsPost()Z
    .locals 3

    .prologue
    .line 345
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 346
    .local v0, "needsPost":Z
    :goto_0
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v1, :cond_2

    .line 347
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "AwContents must be created if we are not posting!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 345
    .end local v0    # "needsPost":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 350
    .restart local v0    # "needsPost":Z
    :cond_2
    return v0
.end method

.method private checkThread()V
    .locals 2

    .prologue
    .line 355
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->createThreadException()Ljava/lang/RuntimeException;

    move-result-object v0

    .line 357
    .local v0, "threadViolation":Ljava/lang/RuntimeException;
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$3;

    invoke-direct {v1, p0, v0}, Lcom/android/webview/chromium/WebViewChromium$3;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/RuntimeException;)V

    invoke-static {v1}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 363
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->createThreadException()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 365
    .end local v0    # "threadViolation":Ljava/lang/RuntimeException;
    :cond_0
    return-void
.end method

.method static completeWindowCreation(Landroid/webkit/WebView;Landroid/webkit/WebView;)V
    .locals 3
    .param p0, "parent"    # Landroid/webkit/WebView;
    .param p1, "child"    # Landroid/webkit/WebView;

    .prologue
    .line 176
    invoke-virtual {p0}, Landroid/webkit/WebView;->getWebViewProvider()Landroid/webkit/WebViewProvider;

    move-result-object v2

    check-cast v2, Lcom/android/webview/chromium/WebViewChromium;

    iget-object v1, v2, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    .line 177
    .local v1, "parentContents":Lcom/android/org/chromium/android_webview/AwContents;
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 179
    .local v0, "childContents":Lcom/android/org/chromium/android_webview/AwContents;
    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/org/chromium/android_webview/AwContents;->supplyContentsForPopup(Lcom/android/org/chromium/android_webview/AwContents;)V

    .line 180
    return-void

    .line 177
    .end local v0    # "childContents":Lcom/android/org/chromium/android_webview/AwContents;
    :cond_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getWebViewProvider()Landroid/webkit/WebViewProvider;

    move-result-object v2

    check-cast v2, Lcom/android/webview/chromium/WebViewChromium;

    iget-object v0, v2, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    goto :goto_0
.end method

.method private createThreadException()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 340
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Calling View methods on another thread than the UI thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private doesSupportFullscreen(Landroid/webkit/WebChromeClient;)Z
    .locals 8
    .param p1, "client"    # Landroid/webkit/WebChromeClient;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1349
    if-nez p1, :cond_1

    .line 1363
    :cond_0
    :goto_0
    return v1

    .line 1354
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Landroid/webkit/WebChromeClient;

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1358
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onShowCustomView"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/view/View;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Landroid/webkit/WebChromeClient$CustomViewCallback;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 1360
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onHideCustomView"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 1361
    goto :goto_0

    .line 1362
    :catch_0
    move-exception v0

    .line 1363
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    goto :goto_0
.end method

.method static enableSlowWholeDocumentDraw()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/webview/chromium/WebViewChromium;->sRecordWholeDocumentEnabledByApi:Z

    .line 159
    return-void
.end method

.method private static fixupBase(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 626
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "about:blank"

    .end local p0    # "url":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private static fixupData(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 622
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, ""

    .end local p0    # "data":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private static fixupHistory(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 630
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "about:blank"

    .end local p0    # "url":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private static fixupMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 618
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "text/html"

    .end local p0    # "mimeType":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private initForReal()V
    .locals 10

    .prologue
    const/16 v9, 0x13

    const/4 v8, 0x0

    .line 311
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/webview/chromium/WebViewChromium;->resourcesContextWrapper(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v3

    .line 312
    .local v3, "ctx":Landroid/content/Context;
    new-instance v0, Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->getBrowserContext()Lcom/android/org/chromium/android_webview/AwBrowserContext;

    move-result-object v1

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    new-instance v4, Lcom/android/webview/chromium/WebViewChromium$InternalAccessAdapter;

    invoke-direct {v4, p0, v8}, Lcom/android/webview/chromium/WebViewChromium$InternalAccessAdapter;-><init>(Lcom/android/webview/chromium/WebViewChromium;Lcom/android/webview/chromium/WebViewChromium$1;)V

    new-instance v5, Lcom/android/webview/chromium/WebViewChromium$WebViewNativeGLDelegate;

    invoke-direct {v5, p0, v8}, Lcom/android/webview/chromium/WebViewChromium$WebViewNativeGLDelegate;-><init>(Lcom/android/webview/chromium/WebViewChromium;Lcom/android/webview/chromium/WebViewChromium$1;)V

    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    iget-object v7, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    invoke-virtual {v7}, Lcom/android/webview/chromium/ContentSettingsAdapter;->getAwSettings()Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/android/org/chromium/android_webview/AwContents;-><init>(Lcom/android/org/chromium/android_webview/AwBrowserContext;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/android/org/chromium/android_webview/AwContents$InternalAccessDelegate;Lcom/android/org/chromium/android_webview/AwContents$NativeGLDelegate;Lcom/android/org/chromium/android_webview/AwContentsClient;Lcom/android/org/chromium/android_webview/AwSettings;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    .line 316
    iget v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    if-lt v0, v9, :cond_0

    .line 319
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContents;->setShouldDownloadFavicons()V

    .line 322
    :cond_0
    sget-boolean v0, Lcom/android/webview/chromium/WebViewChromium;->sRecordWholeDocumentEnabledByApi:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwContentsStatics;->setRecordFullDocument(Z)V

    .line 325
    iget v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    if-gt v0, v9, :cond_2

    .line 328
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->disableJavascriptInterfacesInspection()V

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getLayerType()I

    move-result v1

    invoke-virtual {v0, v1, v8}, Lcom/android/org/chromium/android_webview/AwContents;->setLayerType(ILandroid/graphics/Paint;)V

    .line 333
    return-void

    .line 322
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isBase64Encoded(Ljava/lang/String;)Z
    .locals 1
    .param p0, "encoding"    # Ljava/lang/String;

    .prologue
    .line 634
    const-string v0, "base64"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private loadUrlOnUiThread(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V
    .locals 2
    .param p1, "loadUrlParams"    # Lcom/android/org/chromium/content/browser/LoadUrlParams;

    .prologue
    .line 679
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 680
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 682
    sget-boolean v0, Lcom/android/webview/chromium/WebViewChromium;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 683
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$16;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$16;-><init>(Lcom/android/webview/chromium/WebViewChromium;Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 692
    :goto_0
    return-void

    .line 691
    :cond_1
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->loadUrl(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    goto :goto_0
.end method

.method private static resourcesContextWrapper(Landroid/content/Context;)Landroid/content/Context;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 278
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$2;

    invoke-direct {v0, p0}, Lcom/android/webview/chromium/WebViewChromium$2;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private runBlockingFuture(Ljava/util/concurrent/FutureTask;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/FutureTask",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<TT;>;"
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->hasStarted()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Must be started before we block!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 184
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "This method should only be called off the UI thread"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    invoke-virtual {v1, p1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 189
    const-wide/16 v2, 0x4

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v2, v3, v1}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Probable deadlock detected due to WebView API being called on incorrect thread while the UI thread is blocked."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 193
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "c":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    new-instance v0, Ljava/util/concurrent/FutureTask;

    invoke-direct {v0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runBlockingFuture(Ljava/util/concurrent/FutureTask;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private runVoidTaskOnUiThreadBlocking(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 202
    new-instance v0, Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 203
    .local v0, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runBlockingFuture(Ljava/util/concurrent/FutureTask;)Ljava/lang/Object;

    .line 204
    return-void
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "interfaceName"    # Ljava/lang/String;

    .prologue
    .line 1385
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1386
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v2, Lcom/android/webview/chromium/WebViewChromium$54;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$54;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1399
    :goto_0
    return-void

    .line 1394
    :cond_0
    const/4 v0, 0x0

    .line 1395
    .local v0, "requiredAnnotation":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    iget v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_1

    .line 1396
    const-class v0, Landroid/webkit/JavascriptInterface;

    .line 1398
    :cond_1
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1, p2, v0}, Lcom/android/org/chromium/android_webview/AwContents;->addPossiblyUnsafeJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public canGoBack()Z
    .locals 3

    .prologue
    .line 750
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 751
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 752
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$20;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$20;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 758
    .local v0, "ret":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 760
    .end local v0    # "ret":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->canGoBack()Z

    move-result v1

    goto :goto_0
.end method

.method public canGoBackOrForward(I)Z
    .locals 3
    .param p1, "steps"    # I

    .prologue
    .line 808
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 809
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 810
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$24;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$24;-><init>(Lcom/android/webview/chromium/WebViewChromium;I)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 816
    .local v0, "ret":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 818
    .end local v0    # "ret":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->canGoBackOrForward(I)Z

    move-result v1

    goto :goto_0
.end method

.method public canGoForward()Z
    .locals 3

    .prologue
    .line 779
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 780
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 781
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$22;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$22;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 787
    .local v0, "ret":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 789
    .end local v0    # "ret":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->canGoForward()Z

    move-result v1

    goto :goto_0
.end method

.method public canZoomIn()Z
    .locals 1

    .prologue
    .line 1454
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455
    const/4 v0, 0x0

    .line 1457
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->canZoomIn()Z

    move-result v0

    goto :goto_0
.end method

.method public canZoomOut()Z
    .locals 1

    .prologue
    .line 1462
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463
    const/4 v0, 0x0

    .line 1465
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->canZoomOut()Z

    move-result v0

    goto :goto_0
.end method

.method public capturePicture()Landroid/graphics/Picture;
    .locals 3

    .prologue
    .line 887
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 888
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 889
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$29;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$29;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Picture;

    .line 897
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->capturePicture()Landroid/graphics/Picture;

    move-result-object v0

    goto :goto_0
.end method

.method public clearCache(Z)V
    .locals 2
    .param p1, "includeDiskFiles"    # Z

    .prologue
    .line 1143
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$43;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$43;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1153
    :goto_0
    return-void

    .line 1152
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->clearCache(Z)V

    goto :goto_0
.end method

.method public clearFormData()V
    .locals 2

    .prologue
    .line 1160
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$44;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$44;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1170
    :goto_0
    return-void

    .line 1169
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->hideAutofillPopup()V

    goto :goto_0
.end method

.method public clearHistory()V
    .locals 2

    .prologue
    .line 1174
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1175
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$45;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$45;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1184
    :goto_0
    return-void

    .line 1183
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->clearHistory()V

    goto :goto_0
.end method

.method public clearMatches()V
    .locals 2

    .prologue
    .line 1300
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1301
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$51;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$51;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1310
    :goto_0
    return-void

    .line 1309
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->clearMatches()V

    goto :goto_0
.end method

.method public clearSslPreferences()V
    .locals 2

    .prologue
    .line 1188
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1189
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$46;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$46;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1198
    :goto_0
    return-void

    .line 1197
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->clearSslPreferences()V

    goto :goto_0
.end method

.method public clearView()V
    .locals 2

    .prologue
    .line 873
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$28;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$28;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 883
    :goto_0
    return-void

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->clearView()V

    goto :goto_0
.end method

.method public computeHorizontalScrollOffset()I
    .locals 3

    .prologue
    .line 2061
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 2062
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2063
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$88;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$88;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2071
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->computeHorizontalScrollOffset()I

    move-result v0

    goto :goto_0
.end method

.method public computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 2046
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 2047
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2048
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$87;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$87;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2056
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->computeHorizontalScrollRange()I

    move-result v0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 2121
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 2122
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$92;

    invoke-direct {v0, p0}, Lcom/android/webview/chromium/WebViewChromium$92;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runVoidTaskOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 2132
    :goto_0
    return-void

    .line 2131
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->computeScroll()V

    goto :goto_0
.end method

.method public computeVerticalScrollExtent()I
    .locals 3

    .prologue
    .line 2106
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 2107
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2108
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$91;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$91;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2116
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->computeVerticalScrollExtent()I

    move-result v0

    goto :goto_0
.end method

.method public computeVerticalScrollOffset()I
    .locals 3

    .prologue
    .line 2091
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 2092
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2093
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$90;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$90;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2101
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->computeVerticalScrollOffset()I

    move-result v0

    goto :goto_0
.end method

.method public computeVerticalScrollRange()I
    .locals 3

    .prologue
    .line 2076
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 2077
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2078
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$89;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$89;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2086
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->computeVerticalScrollRange()I

    move-result v0

    goto :goto_0
.end method

.method public copyBackForwardList()Landroid/webkit/WebBackForwardList;
    .locals 3

    .prologue
    .line 1202
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1203
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1204
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$47;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$47;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebBackForwardList;

    .line 1212
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/webview/chromium/WebBackForwardListChromium;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getNavigationHistory()Lcom/android/org/chromium/content/browser/NavigationHistory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/webview/chromium/WebBackForwardListChromium;-><init>(Lcom/android/org/chromium/content/browser/NavigationHistory;)V

    goto :goto_0
.end method

.method public createPrintDocumentAdapter(Ljava/lang/String;)Landroid/print/PrintDocumentAdapter;
    .locals 2
    .param p1, "documentName"    # Ljava/lang/String;

    .prologue
    .line 2141
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 2142
    new-instance v0, Lcom/android/org/chromium/android_webview/AwPrintDocumentAdapter;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getPdfExporter()Lcom/android/org/chromium/android_webview/AwPdfExporter;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/org/chromium/android_webview/AwPrintDocumentAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwPdfExporter;Ljava/lang/String;)V

    return-object v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$11;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$11;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->destroy()V

    .line 499
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mGLfunctor:Lcom/android/webview/chromium/DrawGLFunctor;

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mGLfunctor:Lcom/android/webview/chromium/DrawGLFunctor;

    invoke-virtual {v0}, Lcom/android/webview/chromium/DrawGLFunctor;->destroy()V

    .line 501
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mGLfunctor:Lcom/android/webview/chromium/DrawGLFunctor;

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1885
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1886
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1887
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$78;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$78;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/KeyEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1895
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public documentHasImages(Landroid/os/Message;)V
    .locals 2
    .param p1, "response"    # Landroid/os/Message;

    .prologue
    .line 1314
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1315
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$52;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$52;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/os/Message;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1324
    :goto_0
    return-void

    .line 1323
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->documentHasImages(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public dumpViewHierarchyWithProperties(Ljava/io/BufferedWriter;I)V
    .locals 0
    .param p1, "out"    # Ljava/io/BufferedWriter;
    .param p2, "level"    # I

    .prologue
    .line 1509
    return-void
.end method

.method public evaluateJavaScript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 1
    .param p1, "script"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 695
    .local p2, "resultCallback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 696
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->evaluateJavaScript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 697
    return-void
.end method

.method public extractSmartClipData(IIII)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 2255
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 2256
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->extractSmartClipData(IIII)V

    .line 2257
    return-void
.end method

.method public findAll(Ljava/lang/String;)I
    .locals 1
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 1237
    invoke-virtual {p0, p1}, Lcom/android/webview/chromium/WebViewChromium;->findAllAsync(Ljava/lang/String;)V

    .line 1238
    const/4 v0, 0x0

    return v0
.end method

.method public findAllAsync(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 1243
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$49;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$49;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1253
    :goto_0
    return-void

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->findAllAsync(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public findHierarchyView(Ljava/lang/String;I)Landroid/view/View;
    .locals 1
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "hashCode"    # I

    .prologue
    .line 1514
    const/4 v0, 0x0

    return-object v0
.end method

.method public findNext(Z)V
    .locals 2
    .param p1, "forwards"    # Z

    .prologue
    .line 1223
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$48;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$48;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1233
    :goto_0
    return-void

    .line 1232
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->findNext(Z)V

    goto :goto_0
.end method

.method public flingScroll(II)V
    .locals 2
    .param p1, "vx"    # I
    .param p2, "vy"    # I

    .prologue
    .line 1427
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$56;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$56;-><init>(Lcom/android/webview/chromium/WebViewChromium;II)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1437
    :goto_0
    return-void

    .line 1436
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->flingScroll(II)V

    goto :goto_0
.end method

.method public freeMemory()V
    .locals 0

    .prologue
    .line 1139
    return-void
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 3

    .prologue
    .line 1553
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1554
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1555
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$60;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$60;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeProvider;

    .line 1564
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    goto :goto_0
.end method

.method public getCertificate()Landroid/net/http/SslCertificate;
    .locals 3

    .prologue
    .line 433
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 434
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$8;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$8;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/http/SslCertificate;

    .line 443
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getCertificate()Landroid/net/http/SslCertificate;

    move-result-object v0

    goto :goto_0
.end method

.method public getContentHeight()I
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1055
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->getContentHeightCss()I

    move-result v0

    goto :goto_0
.end method

.method public getContentWidth()I
    .locals 1

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1062
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->getContentWidthCss()I

    move-result v0

    goto :goto_0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1025
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1026
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$37;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$37;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1035
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getHitTestResult()Landroid/webkit/WebView$HitTestResult;
    .locals 4

    .prologue
    .line 929
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 930
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 931
    new-instance v2, Lcom/android/webview/chromium/WebViewChromium$31;

    invoke-direct {v2, p0}, Lcom/android/webview/chromium/WebViewChromium$31;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v2}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView$HitTestResult;

    .line 943
    :goto_0
    return-object v1

    .line 940
    :cond_0
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/AwContents;->getLastHitTestResult()Lcom/android/org/chromium/android_webview/AwContents$HitTestData;

    move-result-object v0

    .line 941
    .local v0, "data":Lcom/android/org/chromium/android_webview/AwContents$HitTestData;
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mHitTestResult:Landroid/webkit/WebView$HitTestResult;

    iget v3, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->hitTestResultType:I

    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    .line 942
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mHitTestResult:Landroid/webkit/WebView$HitTestResult;

    iget-object v3, v0, Lcom/android/org/chromium/android_webview/AwContents$HitTestData;->hitTestResultExtraData:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setExtra(Ljava/lang/String;)V

    .line 943
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mHitTestResult:Landroid/webkit/WebView$HitTestResult;

    goto :goto_0
.end method

.method public getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "realm"    # Ljava/lang/String;

    .prologue
    .line 473
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 474
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 475
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$10;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 483
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 993
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 994
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 995
    new-instance v2, Lcom/android/webview/chromium/WebViewChromium$35;

    invoke-direct {v2, p0}, Lcom/android/webview/chromium/WebViewChromium$35;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v2}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1005
    :goto_0
    return-object v0

    .line 1003
    :cond_0
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/AwContents;->getOriginalUrl()Ljava/lang/String;

    move-result-object v1

    .line 1004
    .local v1, "url":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1005
    goto :goto_0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 1046
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v0, :cond_0

    const/16 v0, 0x64

    .line 1048
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->getMostRecentProgress()I

    move-result v0

    goto :goto_0
.end method

.method public getScale()F
    .locals 2

    .prologue
    .line 903
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 904
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->getScale()F

    move-result v0

    return v0
.end method

.method public getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;
    .locals 0

    .prologue
    .line 1528
    return-object p0
.end method

.method public getSettings()Landroid/webkit/WebSettings;
    .locals 1

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1011
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1012
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$36;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$36;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1020
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTouchIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1041
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 976
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 977
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 978
    new-instance v2, Lcom/android/webview/chromium/WebViewChromium$34;

    invoke-direct {v2, p0}, Lcom/android/webview/chromium/WebViewChromium$34;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v2}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 988
    :goto_0
    return-object v0

    .line 986
    :cond_0
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/AwContents;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 987
    .local v1, "url":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 988
    goto :goto_0
.end method

.method public getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;
    .locals 0

    .prologue
    .line 1522
    return-object p0
.end method

.method public getVisibleTitleHeight()I
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x0

    return v0
.end method

.method public getZoomControls()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1441
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1442
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1449
    :cond_0
    :goto_0
    return-object v0

    .line 1448
    :cond_1
    sget-object v1, Lcom/android/webview/chromium/WebViewChromium;->TAG:Ljava/lang/String;

    const-string v2, "WebView doesn\'t support getZoomControls"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1449
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->getSettings()Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwSettings;->supportZoom()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public goBack()V
    .locals 2

    .prologue
    .line 765
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$21;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$21;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 775
    :goto_0
    return-void

    .line 774
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->goBack()V

    goto :goto_0
.end method

.method public goBackOrForward(I)V
    .locals 2
    .param p1, "steps"    # I

    .prologue
    .line 823
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$25;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$25;-><init>(Lcom/android/webview/chromium/WebViewChromium;I)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 833
    :goto_0
    return-void

    .line 832
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->goBackOrForward(I)V

    goto :goto_0
.end method

.method public goForward()V
    .locals 2

    .prologue
    .line 794
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$23;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$23;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 804
    :goto_0
    return-void

    .line 803
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->goForward()V

    goto :goto_0
.end method

.method public init(Ljava/util/Map;Z)V
    .locals 10
    .param p2, "privateBrowsing"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "javaScriptInterfaces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/16 v9, 0x13

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 217
    if-eqz p2, :cond_1

    .line 218
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v6, v4}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 219
    const-string v2, "Private browsing is not supported in WebView."

    .line 220
    .local v2, "msg":Ljava/lang/String;
    iget v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    if-lt v6, v9, :cond_0

    .line 221
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Private browsing is not supported in WebView."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 223
    :cond_0
    sget-object v6, Lcom/android/webview/chromium/WebViewChromium;->TAG:Ljava/lang/String;

    const-string v7, "Private browsing is not supported in WebView."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    new-instance v3, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 225
    .local v3, "warningLabel":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/android/webview/chromium/R$string;->webviewchromium_private_browsing_warning:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v6, v3}, Landroid/webkit/WebView;->addView(Landroid/view/View;)V

    .line 235
    .end local v2    # "msg":Ljava/lang/String;
    .end local v3    # "warningLabel":Landroid/widget/TextView;
    :cond_1
    iget v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v7, 0x12

    if-lt v6, v7, :cond_4

    .line 236
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v6, v5}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 237
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 244
    :cond_2
    :goto_0
    iget v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v7, 0x10

    if-ge v6, v7, :cond_5

    move v1, v4

    .line 246
    .local v1, "isAccessFromFileURLsGrantedByDefault":Z
    :goto_1
    iget v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    if-ge v6, v9, :cond_6

    move v0, v4

    .line 249
    .local v0, "areLegacyQuirksEnabled":Z
    :goto_2
    new-instance v6, Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    iget-object v7, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-direct {v6, v7}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;-><init>(Landroid/webkit/WebView;)V

    iput-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    .line 250
    new-instance v6, Lcom/android/webview/chromium/ContentSettingsAdapter;

    new-instance v7, Lcom/android/org/chromium/android_webview/AwSettings;

    iget-object v8, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v8}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8, v1, v0}, Lcom/android/org/chromium/android_webview/AwSettings;-><init>(Landroid/content/Context;ZZ)V

    invoke-direct {v6, v7}, Lcom/android/webview/chromium/ContentSettingsAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwSettings;)V

    iput-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    .line 254
    iget v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    if-gt v6, v9, :cond_3

    .line 255
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    invoke-virtual {v6, v5}, Lcom/android/webview/chromium/ContentSettingsAdapter;->setMixedContentMode(I)V

    .line 257
    iget-object v5, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    invoke-virtual {v5, v4}, Lcom/android/webview/chromium/ContentSettingsAdapter;->setAcceptThirdPartyCookies(Z)V

    .line 258
    iget-object v5, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    invoke-virtual {v5}, Lcom/android/webview/chromium/ContentSettingsAdapter;->getAwSettings()Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/org/chromium/android_webview/AwSettings;->setZeroLayoutHeightDisablesViewportQuirk(Z)V

    .line 261
    :cond_3
    iget-object v4, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v5, Lcom/android/webview/chromium/WebViewChromium$1;

    invoke-direct {v5, p0, p2}, Lcom/android/webview/chromium/WebViewChromium$1;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v4, v5}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 274
    return-void

    .line 238
    .end local v0    # "areLegacyQuirksEnabled":Z
    .end local v1    # "isAccessFromFileURLsGrantedByDefault":Z
    :cond_4
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v6}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->hasStarted()Z

    move-result v6

    if-nez v6, :cond_2

    .line 239
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    if-ne v6, v7, :cond_2

    .line 240
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v6, v4}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    goto :goto_0

    :cond_5
    move v1, v5

    .line 244
    goto :goto_1

    .restart local v1    # "isAccessFromFileURLsGrantedByDefault":Z
    :cond_6
    move v0, v5

    .line 246
    goto :goto_2
.end method

.method public invokeZoomPicker()V
    .locals 2

    .prologue
    .line 915
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$30;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$30;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 925
    :goto_0
    return-void

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->invokeZoomPicker()V

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 3

    .prologue
    .line 1123
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1124
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1125
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$42;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$42;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1131
    .local v0, "ret":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1133
    .end local v0    # "ret":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->isPaused()Z

    move-result v1

    goto :goto_0
.end method

.method public isPrivateBrowsingEnabled()Z
    .locals 1

    .prologue
    .line 838
    const/4 v0, 0x0

    return v0
.end method

.method public loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "encoding"    # Ljava/lang/String;

    .prologue
    .line 639
    invoke-static {p1}, Lcom/android/webview/chromium/WebViewChromium;->fixupData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/android/webview/chromium/WebViewChromium;->fixupMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Lcom/android/webview/chromium/WebViewChromium;->isBase64Encoded(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->createLoadDataParams(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/org/chromium/content/browser/LoadUrlParams;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->loadUrlOnUiThread(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    .line 641
    return-void
.end method

.method public loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "baseUrl"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "encoding"    # Ljava/lang/String;
    .param p5, "historyUrl"    # Ljava/lang/String;

    .prologue
    .line 646
    invoke-static {p2}, Lcom/android/webview/chromium/WebViewChromium;->fixupData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 647
    invoke-static {p3}, Lcom/android/webview/chromium/WebViewChromium;->fixupMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 649
    invoke-static {p1}, Lcom/android/webview/chromium/WebViewChromium;->fixupBase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 650
    invoke-static/range {p5 .. p5}, Lcom/android/webview/chromium/WebViewChromium;->fixupHistory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 652
    const-string v0, "data:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 655
    invoke-static {p4}, Lcom/android/webview/chromium/WebViewChromium;->isBase64Encoded(Ljava/lang/String;)Z

    move-result v2

    .line 656
    .local v2, "isBase64":Z
    if-eqz v2, :cond_0

    const/4 v5, 0x0

    :goto_0
    move-object v0, p2

    move-object v1, p3

    move-object v3, p1

    move-object/from16 v4, p5

    invoke-static/range {v0 .. v5}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->createLoadDataParamsWithBaseUrl(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/org/chromium/content/browser/LoadUrlParams;

    move-result-object v10

    .line 672
    .end local v2    # "isBase64":Z
    .local v10, "loadUrlParams":Lcom/android/org/chromium/content/browser/LoadUrlParams;
    :goto_1
    invoke-direct {p0, v10}, Lcom/android/webview/chromium/WebViewChromium;->loadUrlOnUiThread(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    .line 673
    .end local v10    # "loadUrlParams":Lcom/android/org/chromium/content/browser/LoadUrlParams;
    :goto_2
    return-void

    .restart local v2    # "isBase64":Z
    :cond_0
    move-object v5, p4

    .line 656
    goto :goto_0

    .line 664
    .end local v2    # "isBase64":Z
    :cond_1
    :try_start_0
    const-string v0, "utf-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    const-string v8, "utf-8"

    move-object v4, p3

    move-object v6, p1

    move-object/from16 v7, p5

    invoke-static/range {v3 .. v8}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->createLoadDataParamsWithBaseUrl(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/org/chromium/content/browser/LoadUrlParams;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .restart local v10    # "loadUrlParams":Lcom/android/org/chromium/content/browser/LoadUrlParams;
    goto :goto_1

    .line 667
    .end local v10    # "loadUrlParams":Lcom/android/org/chromium/content/browser/LoadUrlParams;
    :catch_0
    move-exception v9

    .line 668
    .local v9, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v0, Lcom/android/webview/chromium/WebViewChromium;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load data string "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 602
    if-nez p1, :cond_0

    .line 606
    :goto_0
    return-void

    .line 605
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/webview/chromium/WebViewChromium;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 575
    .local p2, "additionalHttpHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "javascript:"

    .line 576
    .local v0, "JAVASCRIPT_SCHEME":Ljava/lang/String;
    iget v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_1

    if-eqz p1, :cond_1

    const-string v2, "javascript:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 578
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 579
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 580
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v3, Lcom/android/webview/chromium/WebViewChromium$15;

    invoke-direct {v3, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$15;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 597
    :goto_0
    return-void

    .line 588
    :cond_0
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    const-string v3, "javascript:"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/org/chromium/android_webview/AwContents;->evaluateJavaScriptEvenIfNotYetNavigated(Ljava/lang/String;)V

    goto :goto_0

    .line 594
    :cond_1
    new-instance v1, Lcom/android/org/chromium/content/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lcom/android/org/chromium/content/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 595
    .local v1, "params":Lcom/android/org/chromium/content/browser/LoadUrlParams;
    if-eqz p2, :cond_2

    invoke-virtual {v1, p2}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setExtraHeaders(Ljava/util/Map;)V

    .line 596
    :cond_2
    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->loadUrlOnUiThread(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    goto :goto_0
.end method

.method public notifyFindDialogDismissed()V
    .locals 2

    .prologue
    .line 1286
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1287
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$50;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$50;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1296
    :goto_0
    return-void

    .line 1295
    :cond_0
    invoke-virtual {p0}, Lcom/android/webview/chromium/WebViewChromium;->clearMatches()V

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1793
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1794
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 1795
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->onAttachedToWindow()V

    .line 1796
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1720
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1721
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$69;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$69;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/content/res/Configuration;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1730
    :goto_0
    return-void

    .line 1729
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 1734
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1735
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736
    const/4 v0, 0x0

    .line 1738
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 1800
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1801
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$73;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$73;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1811
    :goto_0
    return-void

    .line 1810
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1690
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1691
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$68;

    invoke-direct {v0, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$68;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/graphics/Canvas;)V

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runVoidTaskOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 1700
    :goto_0
    return-void

    .line 1699
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "scrollBar"    # Landroid/graphics/drawable/Drawable;
    .param p3, "l"    # I
    .param p4, "t"    # I
    .param p5, "r"    # I
    .param p6, "b"    # I

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/webkit/WebView$PrivateAccess;->super_onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    .line 1656
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->onFinishTemporaryDetach()V

    .line 2040
    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1848
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$76;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/webview/chromium/WebViewChromium$76;-><init>(Lcom/android/webview/chromium/WebViewChromium;ZILandroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1858
    :goto_0
    return-void

    .line 1857
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContents;->onFocusChanged(ZILandroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1930
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1931
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1932
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$81;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$81;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/MotionEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1940
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1915
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1916
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1917
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$80;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$80;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/MotionEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1925
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1585
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1586
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$62;

    invoke-direct {v0, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$62;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runVoidTaskOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 1595
    :goto_0
    return-void

    .line 1594
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 1569
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1570
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1571
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$61;

    invoke-direct {v0, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$61;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runVoidTaskOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 1580
    :goto_0
    return-void

    .line 1579
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 1759
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v1, v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1760
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1761
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$71;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$71;-><init>(Lcom/android/webview/chromium/WebViewChromium;ILandroid/view/KeyEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1770
    :goto_0
    return v0

    .line 1769
    :cond_0
    invoke-static {}, Lcom/android/webview/chromium/UnimplementedWebViewApi;->invoke()V

    goto :goto_0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "repeatCount"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 1743
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v1, v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1744
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1745
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$70;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/webview/chromium/WebViewChromium$70;-><init>(Lcom/android/webview/chromium/WebViewChromium;IILandroid/view/KeyEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1754
    :goto_0
    return v0

    .line 1753
    :cond_0
    invoke-static {}, Lcom/android/webview/chromium/UnimplementedWebViewApi;->invoke()V

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1775
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1776
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1777
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$72;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$72;-><init>(Lcom/android/webview/chromium/WebViewChromium;ILandroid/view/KeyEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1785
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1967
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1968
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$83;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$83;-><init>(Lcom/android/webview/chromium/WebViewChromium;II)V

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromium;->runVoidTaskOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 1978
    :goto_0
    return-void

    .line 1977
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->onMeasure(II)V

    goto :goto_0
.end method

.method public onOverScrolled(IIZZ)V
    .locals 7
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 1661
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1662
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$66;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/webview/chromium/WebViewChromium$66;-><init>(Lcom/android/webview/chromium/WebViewChromium;IIZZ)V

    invoke-virtual {v6, v0}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1671
    :goto_0
    return-void

    .line 1670
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->onContainerViewOverScrolled(IIZZ)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1095
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1096
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$40;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$40;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1105
    :goto_0
    return-void

    .line 1104
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->onPause()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1109
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1110
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$41;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$41;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1119
    :goto_0
    return-void

    .line 1118
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->onResume()V

    goto :goto_0
.end method

.method public onScrollChanged(IIII)V
    .locals 0
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 1881
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "ow"    # I
    .param p4, "oh"    # I

    .prologue
    .line 1867
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1868
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$77;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/webview/chromium/WebViewChromium$77;-><init>(Lcom/android/webview/chromium/WebViewChromium;IIII)V

    invoke-virtual {v6, v0}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1877
    :goto_0
    return-void

    .line 1876
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->onSizeChanged(IIII)V

    goto :goto_0
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2035
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->onStartTemporaryDetach()V

    .line 2036
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1900
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1901
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1902
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$79;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$79;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/MotionEvent;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1910
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1946
    const/4 v0, 0x0

    return v0
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 2
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 1817
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v0, :cond_0

    .line 1829
    :goto_0
    return-void

    .line 1819
    :cond_0
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1820
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$74;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$74;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1828
    :cond_1
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 1833
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$75;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$75;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1843
    :goto_0
    return-void

    .line 1842
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onWindowFocusChanged(Z)V

    goto :goto_0
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 1675
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$67;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$67;-><init>(Lcom/android/webview/chromium/WebViewChromium;I)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1685
    :goto_0
    return-void

    .line 1684
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->onWindowVisibilityChanged(I)V

    goto :goto_0
.end method

.method public overlayHorizontalScrollbar()Z
    .locals 3

    .prologue
    .line 397
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 398
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$6;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$6;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 407
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->overlayHorizontalScrollbar()Z

    move-result v0

    goto :goto_0
.end method

.method public overlayVerticalScrollbar()Z
    .locals 3

    .prologue
    .line 412
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 413
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 414
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$7;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$7;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 422
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->overlayVerticalScrollbar()Z

    move-result v0

    goto :goto_0
.end method

.method public pageDown(Z)Z
    .locals 3
    .param p1, "bottom"    # Z

    .prologue
    .line 858
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 859
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 860
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$27;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$27;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 866
    .local v0, "ret":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 868
    .end local v0    # "ret":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->pageDown(Z)Z

    move-result v1

    goto :goto_0
.end method

.method public pageUp(Z)Z
    .locals 3
    .param p1, "top"    # Z

    .prologue
    .line 843
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 844
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 845
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$26;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$26;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 851
    .local v0, "ret":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 853
    .end local v0    # "ret":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->pageUp(Z)Z

    move-result v1

    goto :goto_0
.end method

.method public pauseTimers()V
    .locals 2

    .prologue
    .line 1067
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$38;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$38;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1077
    :goto_0
    return-void

    .line 1076
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->pauseTimers()V

    goto :goto_0
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 3
    .param p1, "action"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 1599
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1600
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1601
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$63;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$63;-><init>(Lcom/android/webview/chromium/WebViewChromium;ILandroid/os/Bundle;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1612
    :goto_0
    return v0

    .line 1609
    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->supportsAccessibilityAction(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1610
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    .line 1612
    :cond_1
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    invoke-virtual {v1, p1, p2}, Landroid/webkit/WebView$PrivateAccess;->super_performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method public performLongClick()Z
    .locals 1

    .prologue
    .line 1715
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    invoke-virtual {v0}, Landroid/webkit/WebView$PrivateAccess;->super_performLongClick()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postUrl(Ljava/lang/String;[B)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "postData"    # [B

    .prologue
    .line 610
    invoke-static {p1, p2}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->createLoadHttpPostParams(Ljava/lang/String;[B)Lcom/android/org/chromium/content/browser/LoadUrlParams;

    move-result-object v1

    .line 611
    .local v1, "params":Lcom/android/org/chromium/content/browser/LoadUrlParams;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 612
    .local v0, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 613
    invoke-virtual {v1, v0}, Lcom/android/org/chromium/content/browser/LoadUrlParams;->setExtraHeaders(Ljava/util/Map;)V

    .line 614
    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->loadUrlOnUiThread(Lcom/android/org/chromium/content/browser/LoadUrlParams;)V

    .line 615
    return-void
.end method

.method public preDispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2032
    return-void
.end method

.method public reload()V
    .locals 2

    .prologue
    .line 736
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$19;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$19;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 746
    :goto_0
    return-void

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->reload()V

    goto :goto_0
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .locals 2
    .param p1, "interfaceName"    # Ljava/lang/String;

    .prologue
    .line 1403
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1404
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$55;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$55;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1413
    :goto_0
    return-void

    .line 1412
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->removeJavascriptInterface(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "immediate"    # Z

    .prologue
    .line 1983
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1984
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1985
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$84;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/webview/chromium/WebViewChromium$84;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/view/View;Landroid/graphics/Rect;Z)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1993
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContents;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1951
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1952
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1953
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$82;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$82;-><init>(Lcom/android/webview/chromium/WebViewChromium;ILandroid/graphics/Rect;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1962
    :goto_0
    return v0

    .line 1961
    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->requestFocus()V

    .line 1962
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    invoke-virtual {v1, p1, p2}, Landroid/webkit/WebView$PrivateAccess;->super_requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestFocusNodeHref(Landroid/os/Message;)V
    .locals 2
    .param p1, "hrefMsg"    # Landroid/os/Message;

    .prologue
    .line 948
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 949
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$32;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$32;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/os/Message;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 958
    :goto_0
    return-void

    .line 957
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->requestFocusNodeHref(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public requestImageRef(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 962
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$33;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$33;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/os/Message;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 972
    :goto_0
    return-void

    .line 971
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->requestImageRef(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public restorePicture(Landroid/os/Bundle;Ljava/io/File;)Z
    .locals 1
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "src"    # Ljava/io/File;

    .prologue
    .line 547
    const/4 v0, 0x0

    return v0
.end method

.method public restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .locals 3
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 552
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 553
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 554
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$14;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$14;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/os/Bundle;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebBackForwardList;

    .line 564
    :cond_0
    :goto_0
    return-object v0

    .line 562
    :cond_1
    if-eqz p1, :cond_0

    .line 563
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->restoreState(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 564
    invoke-virtual {p0}, Lcom/android/webview/chromium/WebViewChromium;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v0

    goto :goto_0
.end method

.method public resumeTimers()V
    .locals 2

    .prologue
    .line 1081
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$39;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$39;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1091
    :goto_0
    return-void

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->resumeTimers()V

    goto :goto_0
.end method

.method public savePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "username"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 454
    return-void
.end method

.method public savePicture(Landroid/os/Bundle;Ljava/io/File;)Z
    .locals 1
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "dest"    # Ljava/io/File;

    .prologue
    .line 541
    const/4 v0, 0x0

    return v0
.end method

.method public saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 523
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 524
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 525
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$13;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$13;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/os/Bundle;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebBackForwardList;

    .line 535
    :cond_0
    :goto_0
    return-object v0

    .line 533
    :cond_1
    if-eqz p1, :cond_0

    .line 534
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1, p1}, Lcom/android/org/chromium/android_webview/AwContents;->saveState(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/android/webview/chromium/WebViewChromium;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v0

    goto :goto_0
.end method

.method public saveWebArchive(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 701
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/webview/chromium/WebViewChromium;->saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    .line 702
    return-void
.end method

.method public saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V
    .locals 2
    .param p1, "basename"    # Ljava/lang/String;
    .param p2, "autoname"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 707
    .local p3, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$17;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/webview/chromium/WebViewChromium$17;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 717
    :goto_0
    return-void

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/org/chromium/android_webview/AwContents;->saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1999
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$85;

    invoke-direct {v0, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$85;-><init>(Lcom/android/webview/chromium/WebViewChromium;I)V

    invoke-static {v0}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 2009
    :goto_0
    return-void

    .line 2008
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setCertificate(Landroid/net/http/SslCertificate;)V
    .locals 0
    .param p1, "certificate"    # Landroid/net/http/SslCertificate;

    .prologue
    .line 449
    return-void
.end method

.method public setDownloadListener(Landroid/webkit/DownloadListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/webkit/DownloadListener;

    .prologue
    .line 1333
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    invoke-virtual {v0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 1334
    return-void
.end method

.method public setFindListener(Landroid/webkit/WebView$FindListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/webkit/WebView$FindListener;

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    invoke-virtual {v0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->setFindListener(Landroid/webkit/WebView$FindListener;)V

    .line 1219
    return-void
.end method

.method public setFrame(IIII)Z
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1862
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/WebView$PrivateAccess;->super_setFrame(IIII)Z

    move-result v0

    return v0
.end method

.method public setHorizontalScrollbarOverlay(Z)V
    .locals 2
    .param p1, "overlay"    # Z

    .prologue
    .line 369
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$4;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$4;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 379
    :goto_0
    return-void

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setHorizontalScrollbarOverlay(Z)V

    goto :goto_0
.end method

.method public setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "realm"    # Ljava/lang/String;
    .param p3, "username"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    iget-object v6, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$9;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/webview/chromium/WebViewChromium$9;-><init>(Lcom/android/webview/chromium/WebViewChromium;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 469
    :goto_0
    return-void

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/org/chromium/android_webview/AwContents;->setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setInitialScale(I)V
    .locals 2
    .param p1, "scaleInPercent"    # I

    .prologue
    .line 910
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    invoke-virtual {v0}, Lcom/android/webview/chromium/ContentSettingsAdapter;->getAwSettings()Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings;->setInitialPageScale(F)V

    .line 911
    return-void
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .locals 1
    .param p1, "layerType"    # I
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v0, :cond_0

    .line 2026
    :goto_0
    return-void

    .line 2016
    :cond_0
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2017
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium$86;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium$86;-><init>(Lcom/android/webview/chromium/WebViewChromium;ILandroid/graphics/Paint;)V

    invoke-static {v0}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2025
    :cond_1
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1, p2}, Lcom/android/org/chromium/android_webview/AwContents;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 1707
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1708
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 1709
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView$PrivateAccess;->super_setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1710
    return-void
.end method

.method public setMapTrackballToArrowKeys(Z)V
    .locals 0
    .param p1, "setMap"    # Z

    .prologue
    .line 1423
    return-void
.end method

.method public setNetworkAvailable(Z)V
    .locals 2
    .param p1, "networkUp"    # Z

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$12;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$12;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 519
    :goto_0
    return-void

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setNetworkAvailable(Z)V

    goto :goto_0
.end method

.method public setOverScrollMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1621
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-nez v0, :cond_0

    .line 1633
    :goto_0
    return-void

    .line 1623
    :cond_0
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1624
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$64;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$64;-><init>(Lcom/android/webview/chromium/WebViewChromium;I)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1632
    :cond_1
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setOverScrollMode(I)V

    goto :goto_0
.end method

.method public setPictureListener(Landroid/webkit/WebView$PictureListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/webkit/WebView$PictureListener;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1369
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1370
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$53;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$53;-><init>(Lcom/android/webview/chromium/WebViewChromium;Landroid/webkit/WebView$PictureListener;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1381
    :goto_0
    return-void

    .line 1378
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    invoke-virtual {v0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    .line 1379
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    iget v4, p0, Lcom/android/webview/chromium/WebViewChromium;->mAppTargetSdkVersion:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_2

    :goto_2
    invoke-virtual {v3, v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->enableOnNewPicture(ZZ)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public setScrollBarStyle(I)V
    .locals 2
    .param p1, "style"    # I

    .prologue
    .line 1637
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1638
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$65;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$65;-><init>(Lcom/android/webview/chromium/WebViewChromium;I)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 1647
    :goto_0
    return-void

    .line 1646
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setScrollBarStyle(I)V

    goto :goto_0
.end method

.method public setSmartClipResultHandler(Landroid/os/Handler;)V
    .locals 1
    .param p1, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 2262
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 2263
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setSmartClipResultHandler(Landroid/os/Handler;)V

    .line 2264
    return-void
.end method

.method public setVerticalScrollbarOverlay(Z)V
    .locals 2
    .param p1, "overlay"    # Z

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$5;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewChromium$5;-><init>(Lcom/android/webview/chromium/WebViewChromium;Z)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 393
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->setVerticalScrollbarOverlay(Z)V

    goto :goto_0
.end method

.method public setWebChromeClient(Landroid/webkit/WebChromeClient;)V
    .locals 2
    .param p1, "client"    # Landroid/webkit/WebChromeClient;

    .prologue
    .line 1338
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebSettings:Lcom/android/webview/chromium/ContentSettingsAdapter;

    invoke-virtual {v0}, Lcom/android/webview/chromium/ContentSettingsAdapter;->getAwSettings()Lcom/android/org/chromium/android_webview/AwSettings;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/webview/chromium/WebViewChromium;->doesSupportFullscreen(Landroid/webkit/WebChromeClient;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/org/chromium/android_webview/AwSettings;->setFullscreenSupported(Z)V

    .line 1339
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    invoke-virtual {v0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1340
    return-void
.end method

.method public setWebViewClient(Landroid/webkit/WebViewClient;)V
    .locals 1
    .param p1, "client"    # Landroid/webkit/WebViewClient;

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mContentsClientAdapter:Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    invoke-virtual {v0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1329
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 3

    .prologue
    .line 1538
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1539
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1540
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$59;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$59;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1548
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public showFindDialog(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "showIme"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1257
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v2, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1258
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1281
    :cond_0
    :goto_0
    return v1

    .line 1261
    :cond_1
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1265
    new-instance v0, Landroid/webkit/FindActionModeCallback;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/webkit/FindActionModeCallback;-><init>(Landroid/content/Context;)V

    .line 1266
    .local v0, "findAction":Landroid/webkit/FindActionModeCallback;
    if-eqz v0, :cond_0

    .line 1270
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 1271
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/FindActionModeCallback;->setWebView(Landroid/webkit/WebView;)V

    .line 1272
    if-eqz p2, :cond_2

    .line 1273
    invoke-virtual {v0}, Landroid/webkit/FindActionModeCallback;->showSoftInput()V

    .line 1276
    :cond_2
    if-eqz p1, :cond_3

    .line 1277
    invoke-virtual {v0, p1}, Landroid/webkit/FindActionModeCallback;->setText(Ljava/lang/String;)V

    .line 1278
    invoke-virtual {v0}, Landroid/webkit/FindActionModeCallback;->findAll()V

    .line 1281
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method startYourEngine()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    invoke-virtual {v0}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->drainQueue()V

    .line 337
    return-void
.end method

.method public stopLoading()V
    .locals 2

    .prologue
    .line 721
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mRunQueue:Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;

    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$18;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$18;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromium$WebViewChromiumRunQueue;->addTask(Ljava/lang/Runnable;)V

    .line 732
    :goto_0
    return-void

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwContents;->stopLoading()V

    goto :goto_0
.end method

.method public zoomBy(F)Z
    .locals 2
    .param p1, "factor"    # F

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1502
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkThread()V

    .line 1503
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwContents;->zoomBy(F)Z

    move-result v0

    return v0
.end method

.method public zoomIn()Z
    .locals 3

    .prologue
    .line 1470
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1471
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1472
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$57;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$57;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1480
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->zoomIn()Z

    move-result v0

    goto :goto_0
.end method

.method public zoomOut()Z
    .locals 3

    .prologue
    .line 1485
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mFactory:Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startYourEngines(Z)V

    .line 1486
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromium;->checkNeedsPost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1487
    new-instance v1, Lcom/android/webview/chromium/WebViewChromium$58;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromium$58;-><init>(Lcom/android/webview/chromium/WebViewChromium;)V

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromium;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1495
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromium;->mAwContents:Lcom/android/org/chromium/android_webview/AwContents;

    invoke-virtual {v1}, Lcom/android/org/chromium/android_webview/AwContents;->zoomOut()Z

    move-result v0

    goto :goto_0
.end method
