.class public Lcom/android/webview/chromium/WebViewContentsClientAdapter;
.super Lcom/android/org/chromium/android_webview/AwContentsClient;
.source "WebViewContentsClientAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;,
        Lcom/android/webview/chromium/WebViewContentsClientAdapter$AwHttpAuthHandlerAdapter;,
        Lcom/android/webview/chromium/WebViewContentsClientAdapter$ClientCertRequestImpl;,
        Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;,
        Lcom/android/webview/chromium/WebViewContentsClientAdapter$WebResourceRequestImpl;,
        Lcom/android/webview/chromium/WebViewContentsClientAdapter$NullWebViewClient;
    }
.end annotation


# instance fields
.field private mDownloadListener:Landroid/webkit/DownloadListener;

.field private mFindListener:Landroid/webkit/WebView$FindListener;

.field private mOngoingPermissionRequests:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPictureListener:Landroid/webkit/WebView$PictureListener;

.field private mUiThreadHandler:Landroid/os/Handler;

.field private mWebChromeClient:Landroid/webkit/WebChromeClient;

.field private final mWebView:Landroid/webkit/WebView;

.field private mWebViewClient:Landroid/webkit/WebViewClient;


# direct methods
.method constructor <init>(Landroid/webkit/WebView;)V
    .locals 2
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/android/org/chromium/android_webview/AwContentsClient;-><init>()V

    .line 124
    if-nez p1, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "webView can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 131
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$1;-><init>(Lcom/android/webview/chromium/WebViewContentsClientAdapter;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mUiThreadHandler:Landroid/os/Handler;

    .line 159
    return-void
.end method

.method static synthetic access$000(Lcom/android/webview/chromium/WebViewContentsClientAdapter;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/webview/chromium/WebViewContentsClientAdapter;)Landroid/webkit/WebView$PictureListener;
    .locals 1
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewContentsClientAdapter;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    return-object v0
.end method


# virtual methods
.method public doUpdateVisitedHistory(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "isReload"    # Z

    .prologue
    .line 254
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 256
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1, p2}, Landroid/webkit/WebViewClient;->doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    .line 257
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 258
    return-void
.end method

.method public getDefaultVideoPoster()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 980
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 981
    const/4 v2, 0x0

    .line 982
    .local v2, "result":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v3, :cond_0

    .line 984
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v3}, Landroid/webkit/WebChromeClient;->getDefaultVideoPoster()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 986
    :cond_0
    if-nez v2, :cond_1

    .line 989
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/webview/chromium/R$drawable;->ic_media_video_poster:I

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 992
    .local v1, "poster":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 993
    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 994
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 995
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v6, v6, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 997
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "poster":Landroid/graphics/Bitmap;
    :cond_1
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 998
    return-object v2
.end method

.method protected getVideoLoadingProgressView()Landroid/view/View;
    .locals 2

    .prologue
    .line 966
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 968
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v1, :cond_0

    .line 970
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v1}, Landroid/webkit/WebChromeClient;->getVideoLoadingProgressView()Landroid/view/View;

    move-result-object v0

    .line 974
    .local v0, "result":Landroid/view/View;
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 975
    return-object v0

    .line 972
    .end local v0    # "result":Landroid/view/View;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "result":Landroid/view/View;
    goto :goto_0
.end method

.method public getVisitedHistory(Landroid/webkit/ValueCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p1, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<[Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 242
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, p1}, Landroid/webkit/WebChromeClient;->getVisitedHistory(Landroid/webkit/ValueCallback;)V

    .line 246
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 247
    return-void
.end method

.method public handleJsAlert(Ljava/lang/String;Ljava/lang/String;Lcom/android/org/chromium/android_webview/JsResultReceiver;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "receiver"    # Lcom/android/org/chromium/android_webview/JsResultReceiver;

    .prologue
    .line 685
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 686
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_1

    .line 687
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;

    invoke-direct {v0, p3}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;-><init>(Lcom/android/org/chromium/android_webview/JsResultReceiver;)V

    invoke-virtual {v0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;->getPromptResult()Landroid/webkit/JsPromptResult;

    move-result-object v1

    .line 690
    .local v1, "res":Landroid/webkit/JsPromptResult;
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/webkit/WebChromeClient;->onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 691
    new-instance v0, Landroid/webkit/JsDialogHelper;

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Landroid/webkit/JsDialogHelper;-><init>(Landroid/webkit/JsPromptResult;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/JsDialogHelper;->showDialog(Landroid/content/Context;)V

    .line 697
    .end local v1    # "res":Landroid/webkit/JsPromptResult;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 698
    return-void

    .line 695
    :cond_1
    invoke-interface {p3}, Lcom/android/org/chromium/android_webview/JsResultReceiver;->cancel()V

    goto :goto_0
.end method

.method public handleJsBeforeUnload(Ljava/lang/String;Ljava/lang/String;Lcom/android/org/chromium/android_webview/JsResultReceiver;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "receiver"    # Lcom/android/org/chromium/android_webview/JsResultReceiver;

    .prologue
    .line 702
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 703
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_1

    .line 704
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;

    invoke-direct {v0, p3}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;-><init>(Lcom/android/org/chromium/android_webview/JsResultReceiver;)V

    invoke-virtual {v0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;->getPromptResult()Landroid/webkit/JsPromptResult;

    move-result-object v1

    .line 707
    .local v1, "res":Landroid/webkit/JsPromptResult;
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/webkit/WebChromeClient;->onJsBeforeUnload(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 708
    new-instance v0, Landroid/webkit/JsDialogHelper;

    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Landroid/webkit/JsDialogHelper;-><init>(Landroid/webkit/JsPromptResult;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/JsDialogHelper;->showDialog(Landroid/content/Context;)V

    .line 714
    .end local v1    # "res":Landroid/webkit/JsPromptResult;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 715
    return-void

    .line 712
    :cond_1
    invoke-interface {p3}, Lcom/android/org/chromium/android_webview/JsResultReceiver;->cancel()V

    goto :goto_0
.end method

.method public handleJsConfirm(Ljava/lang/String;Ljava/lang/String;Lcom/android/org/chromium/android_webview/JsResultReceiver;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "receiver"    # Lcom/android/org/chromium/android_webview/JsResultReceiver;

    .prologue
    .line 719
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 720
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_1

    .line 721
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;

    invoke-direct {v0, p3}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;-><init>(Lcom/android/org/chromium/android_webview/JsResultReceiver;)V

    invoke-virtual {v0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;->getPromptResult()Landroid/webkit/JsPromptResult;

    move-result-object v1

    .line 724
    .local v1, "res":Landroid/webkit/JsPromptResult;
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/webkit/WebChromeClient;->onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 725
    new-instance v0, Landroid/webkit/JsDialogHelper;

    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Landroid/webkit/JsDialogHelper;-><init>(Landroid/webkit/JsPromptResult;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/JsDialogHelper;->showDialog(Landroid/content/Context;)V

    .line 731
    .end local v1    # "res":Landroid/webkit/JsPromptResult;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 732
    return-void

    .line 729
    :cond_1
    invoke-interface {p3}, Lcom/android/org/chromium/android_webview/JsResultReceiver;->cancel()V

    goto :goto_0
.end method

.method public handleJsPrompt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/org/chromium/android_webview/JsPromptResultReceiver;)V
    .locals 10
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "defaultValue"    # Ljava/lang/String;
    .param p4, "receiver"    # Lcom/android/org/chromium/android_webview/JsPromptResultReceiver;

    .prologue
    .line 737
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 738
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_1

    .line 739
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;

    invoke-direct {v0, p4}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;-><init>(Lcom/android/org/chromium/android_webview/JsPromptResultReceiver;)V

    invoke-virtual {v0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$JsPromptResultReceiverAdapter;->getPromptResult()Landroid/webkit/JsPromptResult;

    move-result-object v5

    .line 742
    .local v5, "res":Landroid/webkit/JsPromptResult;
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebChromeClient;->onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 743
    new-instance v4, Landroid/webkit/JsDialogHelper;

    const/4 v6, 0x3

    move-object v7, p3

    move-object v8, p2

    move-object v9, p1

    invoke-direct/range {v4 .. v9}, Landroid/webkit/JsDialogHelper;-><init>(Landroid/webkit/JsPromptResult;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/webkit/JsDialogHelper;->showDialog(Landroid/content/Context;)V

    .line 749
    .end local v5    # "res":Landroid/webkit/JsPromptResult;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 750
    return-void

    .line 747
    :cond_1
    invoke-interface {p4}, Lcom/android/org/chromium/android_webview/JsPromptResultReceiver;->cancel()V

    goto :goto_0
.end method

.method public onCloseWindow()V
    .locals 2

    .prologue
    .line 430
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 431
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebChromeClient;->onCloseWindow(Landroid/webkit/WebView;)V

    .line 435
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 436
    return-void
.end method

.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 5
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 360
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 362
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v2, :cond_1

    .line 364
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v2, p1}, Landroid/webkit/WebChromeClient;->onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z

    move-result v1

    .line 365
    .local v1, "result":Z
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v0

    .line 366
    .local v0, "message":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v2, "[blocked]"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    const-string v2, "WebViewCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Blocked URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 373
    return v1

    .line 370
    .end local v1    # "result":Z
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "result":Z
    goto :goto_0
.end method

.method public onCreateWindow(ZZ)Z
    .locals 6
    .param p1, "isDialog"    # Z
    .param p2, "isUserGesture"    # Z

    .prologue
    .line 411
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mUiThreadHandler:Landroid/os/Handler;

    const/16 v3, 0x64

    new-instance v4, Landroid/webkit/WebView$WebViewTransport;

    iget-object v5, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v4, v5}, Landroid/webkit/WebView$WebViewTransport;-><init>(Landroid/webkit/WebView;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 413
    .local v0, "m":Landroid/os/Message;
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 415
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v2, :cond_0

    .line 417
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v3, p1, p2, v0}, Landroid/webkit/WebChromeClient;->onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z

    move-result v1

    .line 421
    .local v1, "result":Z
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 422
    return v1

    .line 419
    .end local v1    # "result":Z
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "result":Z
    goto :goto_0
.end method

.method public onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 9
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "contentDisposition"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentLength"    # J

    .prologue
    .line 869
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mDownloadListener:Landroid/webkit/DownloadListener;

    if-eqz v0, :cond_0

    .line 870
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 872
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mDownloadListener:Landroid/webkit/DownloadListener;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-interface/range {v1 .. v7}, Landroid/webkit/DownloadListener;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 877
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 879
    :cond_0
    return-void
.end method

.method public onFindResultReceived(IIZ)V
    .locals 1
    .param p1, "activeMatchOrdinal"    # I
    .param p2, "numberOfMatches"    # I
    .param p3, "isDoneCounting"    # Z

    .prologue
    .line 382
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mFindListener:Landroid/webkit/WebView$FindListener;

    if-nez v0, :cond_0

    .line 387
    :goto_0
    return-void

    .line 383
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 385
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mFindListener:Landroid/webkit/WebView$FindListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebView$FindListener;->onFindResultReceived(IIZ)V

    .line 386
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    goto :goto_0
.end method

.method public onFormResubmission(Landroid/os/Message;Landroid/os/Message;)V
    .locals 2
    .param p1, "dontResend"    # Landroid/os/Message;
    .param p2, "resend"    # Landroid/os/Message;

    .prologue
    .line 857
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 859
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1, p2}, Landroid/webkit/WebViewClient;->onFormResubmission(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V

    .line 860
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 861
    return-void
.end method

.method public onGeolocationPermissionsHidePrompt()V
    .locals 1

    .prologue
    .line 600
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 601
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->onGeolocationPermissionsHidePrompt()V

    .line 605
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 606
    return-void
.end method

.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 1
    .param p1, "origin"    # Ljava/lang/String;
    .param p2, "callback"    # Landroid/webkit/GeolocationPermissions$Callback;

    .prologue
    .line 590
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 591
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebChromeClient;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V

    .line 595
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 596
    return-void
.end method

.method public onHideCustomView()V
    .locals 1

    .prologue
    .line 956
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 957
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 959
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->onHideCustomView()V

    .line 961
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 962
    return-void
.end method

.method public onLoadResource(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 403
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 405
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 406
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 407
    return-void
.end method

.method public onNewPicture(Landroid/graphics/Picture;)V
    .locals 2
    .param p1, "picture"    # Landroid/graphics/Picture;

    .prologue
    .line 394
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    if-nez v0, :cond_0

    .line 399
    :goto_0
    return-void

    .line 395
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 397
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-interface {v0, v1, p1}, Landroid/webkit/WebView$PictureListener;->onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V

    .line 398
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    goto :goto_0
.end method

.method public onPageFinished(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 493
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 495
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 496
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 511
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    if-eqz v0, :cond_0

    .line 512
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$2;-><init>(Lcom/android/webview/chromium/WebViewContentsClientAdapter;)V

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    .line 523
    :cond_0
    return-void
.end method

.method public onPageStarted(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 482
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 484
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 485
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 486
    return-void
.end method

.method public onPermissionRequest(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V
    .locals 3
    .param p1, "permissionRequest"    # Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;

    .prologue
    .line 610
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 611
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v1, :cond_1

    .line 613
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mOngoingPermissionRequests:Ljava/util/WeakHashMap;

    if-nez v1, :cond_0

    .line 614
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mOngoingPermissionRequests:Ljava/util/WeakHashMap;

    .line 617
    :cond_0
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;

    invoke-direct {v0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;-><init>(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V

    .line 618
    .local v0, "adapter":Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mOngoingPermissionRequests:Ljava/util/WeakHashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v1, v0}, Landroid/webkit/WebChromeClient;->onPermissionRequest(Landroid/webkit/PermissionRequest;)V

    .line 625
    .end local v0    # "adapter":Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;
    :goto_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 626
    return-void

    .line 623
    :cond_1
    invoke-virtual {p1}, Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;->deny()V

    goto :goto_0
.end method

.method public onPermissionRequestCanceled(Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;)V
    .locals 3
    .param p1, "permissionRequest"    # Lcom/android/org/chromium/android_webview/permission/AwPermissionRequest;

    .prologue
    .line 630
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 631
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mOngoingPermissionRequests:Ljava/util/WeakHashMap;

    if-eqz v2, :cond_0

    .line 633
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mOngoingPermissionRequests:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 638
    .local v1, "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;>;"
    if-eqz v1, :cond_0

    .line 639
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;

    .line 640
    .local v0, "adapter":Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v2, v0}, Landroid/webkit/WebChromeClient;->onPermissionRequestCanceled(Landroid/webkit/PermissionRequest;)V

    .line 643
    .end local v0    # "adapter":Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;
    .end local v1    # "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/webview/chromium/WebViewContentsClientAdapter$PermissionRequestAdapter;>;"
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 644
    return-void
.end method

.method public onProgressChanged(I)V
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 265
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 266
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    .line 270
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 271
    return-void
.end method

.method public onReceivedClientCertRequest(Lcom/android/org/chromium/android_webview/AwContentsClientBridge$ClientCertificateRequestCallback;[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;I)V
    .locals 6
    .param p1, "callback"    # Lcom/android/org/chromium/android_webview/AwContentsClientBridge$ClientCertificateRequestCallback;
    .param p2, "keyTypes"    # [Ljava/lang/String;
    .param p3, "principals"    # [Ljava/security/Principal;
    .param p4, "host"    # Ljava/lang/String;
    .param p5, "port"    # I

    .prologue
    .line 840
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 841
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$ClientCertRequestImpl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$ClientCertRequestImpl;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClientBridge$ClientCertificateRequestCallback;[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;I)V

    .line 843
    .local v0, "request":Lcom/android/webview/chromium/WebViewContentsClientAdapter$ClientCertRequestImpl;
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewClient;->onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequest;)V

    .line 844
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 845
    return-void
.end method

.method public onReceivedError(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "errorCode"    # I
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 530
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/net/http/ErrorStrings;->getString(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 536
    :cond_1
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 538
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 540
    return-void
.end method

.method public onReceivedHttpAuthRequest(Lcom/android/org/chromium/android_webview/AwHttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "handler"    # Lcom/android/org/chromium/android_webview/AwHttpAuthHandler;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "realm"    # Ljava/lang/String;

    .prologue
    .line 754
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 756
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/android/webview/chromium/WebViewContentsClientAdapter$AwHttpAuthHandlerAdapter;

    invoke-direct {v2, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$AwHttpAuthHandlerAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwHttpAuthHandler;)V

    invoke-virtual {v0, v1, v2, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 759
    return-void
.end method

.method public onReceivedIcon(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 469
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 470
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebChromeClient;->onReceivedIcon(Landroid/webkit/WebView;Landroid/graphics/Bitmap;)V

    .line 474
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 475
    return-void
.end method

.method public onReceivedLoginRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "realm"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "args"    # Ljava/lang/String;

    .prologue
    .line 849
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 851
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 853
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/ValueCallback;Landroid/net/http/SslError;)V
    .locals 3
    .param p2, "error"    # Landroid/net/http/SslError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/net/http/SslError;",
            ")V"
        }
    .end annotation

    .prologue
    .line 763
    .local p1, "callback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/Boolean;>;"
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$3;-><init>(Lcom/android/webview/chromium/WebViewContentsClientAdapter;Landroid/webkit/ValueCallback;)V

    .line 773
    .local v0, "handler":Landroid/webkit/SslErrorHandler;
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 775
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2, v0, p2}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 776
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 777
    return-void
.end method

.method public onReceivedTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 547
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 548
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebChromeClient;->onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 552
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 553
    return-void
.end method

.method public onReceivedTouchIconUrl(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "precomposed"    # Z

    .prologue
    .line 456
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 457
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1, p2}, Landroid/webkit/WebChromeClient;->onReceivedTouchIconUrl(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    .line 461
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 462
    return-void
.end method

.method public onRequestFocus()V
    .locals 2

    .prologue
    .line 443
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 444
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebChromeClient;->onRequestFocus(Landroid/webkit/WebView;)V

    .line 448
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 449
    return-void
.end method

.method public onScaleChangedScaled(FF)V
    .locals 2
    .param p1, "oldScale"    # F
    .param p2, "newScale"    # F

    .prologue
    .line 938
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 940
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1, p2}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    .line 941
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 942
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cb"    # Landroid/webkit/WebChromeClient$CustomViewCallback;

    .prologue
    .line 946
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 947
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-eqz v0, :cond_0

    .line 949
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebChromeClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 951
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 952
    return-void
.end method

.method public onUnhandledKeyEvent(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 349
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 351
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewClient;->onUnhandledKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)V

    .line 352
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 353
    return-void
.end method

.method setDownloadListener(Landroid/webkit/DownloadListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/webkit/DownloadListener;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mDownloadListener:Landroid/webkit/DownloadListener;

    .line 222
    return-void
.end method

.method setFindListener(Landroid/webkit/WebView$FindListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/webkit/WebView$FindListener;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mFindListener:Landroid/webkit/WebView$FindListener;

    .line 226
    return-void
.end method

.method setPictureListener(Landroid/webkit/WebView$PictureListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/webkit/WebView$PictureListener;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    .line 230
    return-void
.end method

.method setWebChromeClient(Landroid/webkit/WebChromeClient;)V
    .locals 0
    .param p1, "client"    # Landroid/webkit/WebChromeClient;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    .line 218
    return-void
.end method

.method setWebViewClient(Landroid/webkit/WebViewClient;)V
    .locals 1
    .param p1, "client"    # Landroid/webkit/WebViewClient;

    .prologue
    .line 209
    if-eqz p1, :cond_0

    .line 210
    iput-object p1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    .line 214
    :goto_0
    return-void

    .line 212
    :cond_0
    new-instance v0, Lcom/android/webview/chromium/WebViewContentsClientAdapter$NullWebViewClient;

    invoke-direct {v0}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$NullWebViewClient;-><init>()V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    goto :goto_0
.end method

.method public shouldInterceptRequest(Lcom/android/org/chromium/android_webview/AwContentsClient$ShouldInterceptRequestParams;)Lcom/android/org/chromium/android_webview/AwWebResourceResponse;
    .locals 8
    .param p1, "params"    # Lcom/android/org/chromium/android_webview/AwContentsClient$ShouldInterceptRequestParams;

    .prologue
    .line 311
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 313
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/android/webview/chromium/WebViewContentsClientAdapter$WebResourceRequestImpl;

    invoke-direct {v2, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$WebResourceRequestImpl;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClient$ShouldInterceptRequestParams;)V

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object v7

    .line 315
    .local v7, "response":Landroid/webkit/WebResourceResponse;
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 316
    if-nez v7, :cond_0

    const/4 v0, 0x0

    .line 323
    :goto_0
    return-object v0

    .line 319
    :cond_0
    invoke-virtual {v7}, Landroid/webkit/WebResourceResponse;->getResponseHeaders()Ljava/util/Map;

    move-result-object v6

    .line 320
    .local v6, "responseHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v6, :cond_1

    .line 321
    new-instance v6, Ljava/util/HashMap;

    .end local v6    # "responseHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 323
    .restart local v6    # "responseHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    new-instance v0, Lcom/android/org/chromium/android_webview/AwWebResourceResponse;

    invoke-virtual {v7}, Landroid/webkit/WebResourceResponse;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Landroid/webkit/WebResourceResponse;->getEncoding()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/webkit/WebResourceResponse;->getData()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v7}, Landroid/webkit/WebResourceResponse;->getStatusCode()I

    move-result v4

    invoke-virtual {v7}, Landroid/webkit/WebResourceResponse;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lcom/android/org/chromium/android_webview/AwWebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;ILjava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public shouldOverrideKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 566
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Lcom/android/org/chromium/content/browser/ContentViewClient;->shouldPropagateKey(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 571
    :goto_0
    return v0

    .line 567
    :cond_0
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 569
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2, p1}, Landroid/webkit/WebViewClient;->shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z

    move-result v0

    .line 570
    .local v0, "result":Z
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Ljava/lang/String;)Z
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 337
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 339
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebViewClient:Landroid/webkit/WebViewClient;

    iget-object v2, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2, p1}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    .line 340
    .local v0, "result":Z
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    .line 341
    return v0
.end method

.method public showFileChooser(Landroid/webkit/ValueCallback;Lcom/android/org/chromium/android_webview/AwContentsClient$FileChooserParams;)V
    .locals 6
    .param p2, "fileChooserParams"    # Lcom/android/org/chromium/android_webview/AwContentsClient$FileChooserParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/org/chromium/android_webview/AwContentsClient$FileChooserParams;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "uploadFileCallback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<[Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 884
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    if-nez v3, :cond_1

    .line 885
    invoke-interface {p1, v5}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 934
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->begin()V

    .line 889
    new-instance v0, Lcom/android/webview/chromium/FileChooserParamsAdapter;

    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, p2, v3}, Lcom/android/webview/chromium/FileChooserParamsAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwContentsClient$FileChooserParams;Landroid/content/Context;)V

    .line 892
    .local v0, "adapter":Lcom/android/webview/chromium/FileChooserParamsAdapter;
    new-instance v1, Lcom/android/webview/chromium/WebViewContentsClientAdapter$4;

    invoke-direct {v1, p0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$4;-><init>(Lcom/android/webview/chromium/WebViewContentsClientAdapter;Landroid/webkit/ValueCallback;)V

    .line 910
    .local v1, "callbackAdapter":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<[Landroid/net/Uri;>;"
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v4, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, v4, v1, v0}, Landroid/webkit/WebChromeClient;->onShowFileChooser(Landroid/webkit/WebView;Landroid/webkit/ValueCallback;Landroid/webkit/WebChromeClient$FileChooserParams;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 913
    iget-object v3, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_2

    .line 915
    invoke-interface {p1, v5}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 918
    :cond_2
    new-instance v2, Lcom/android/webview/chromium/WebViewContentsClientAdapter$5;

    invoke-direct {v2, p0, p1}, Lcom/android/webview/chromium/WebViewContentsClientAdapter$5;-><init>(Lcom/android/webview/chromium/WebViewContentsClientAdapter;Landroid/webkit/ValueCallback;)V

    .line 931
    .local v2, "innerCallback":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Landroid/net/Uri;>;"
    iget-object v4, p0, Lcom/android/webview/chromium/WebViewContentsClientAdapter;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    iget-object v5, p2, Lcom/android/org/chromium/android_webview/AwContentsClient$FileChooserParams;->acceptTypes:Ljava/lang/String;

    iget-boolean v3, p2, Lcom/android/org/chromium/android_webview/AwContentsClient$FileChooserParams;->capture:Z

    if-eqz v3, :cond_3

    const-string v3, "*"

    :goto_1
    invoke-virtual {v4, v2, v5, v3}, Landroid/webkit/WebChromeClient;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    invoke-static {}, Lcom/android/org/chromium/base/TraceEvent;->end()V

    goto :goto_0

    .line 931
    :cond_3
    const-string v3, ""

    goto :goto_1
.end method
