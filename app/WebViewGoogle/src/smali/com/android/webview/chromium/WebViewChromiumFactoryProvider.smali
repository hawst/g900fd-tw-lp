.class public Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;
.super Ljava/lang/Object;
.source "WebViewChromiumFactoryProvider.java"

# interfaces
.implements Landroid/webkit/WebViewFactoryProvider;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

.field private mCookieManager:Lcom/android/webview/chromium/CookieManagerAdapter;

.field private mDevToolsServer:Lcom/android/org/chromium/android_webview/AwDevToolsServer;

.field private mGeolocationPermissions:Lcom/android/webview/chromium/GeolocationPermissionsAdapter;

.field private final mLock:Ljava/lang/Object;

.field private mStarted:Z

.field private mStaticMethods:Landroid/webkit/WebViewFactoryProvider$Statics;

.field private mWebIconDatabase:Lcom/android/webview/chromium/WebIconDatabaseAdapter;

.field private mWebStorage:Lcom/android/webview/chromium/WebStorageAdapter;

.field private mWebViewDatabase:Lcom/android/webview/chromium/WebViewDatabaseAdapter;

.field private mWebViewPrefs:Landroid/content/SharedPreferences;

.field private mWebViewsToStart:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/webview/chromium/WebViewChromium;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x10

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    .line 94
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewsToStart:Ljava/util/ArrayList;

    .line 103
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->setWillOverrideUiThread()V

    .line 105
    const-string v4, "AwBrowserProcess.loadLibrary()"

    invoke-static {v6, v7, v4}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwBrowserProcess;->loadLibrary()V

    .line 107
    invoke-static {v6, v7}, Landroid/os/Trace;->traceEnd(J)V

    .line 109
    const-string v4, "webviewchromium_plat_support"

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 115
    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v4

    const-string v5, "WebViewChromiumPrefs"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewPrefs:Landroid/content/SharedPreferences;

    .line 117
    iget-object v4, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewPrefs:Landroid/content/SharedPreferences;

    const-string v5, "lastVersionCodeUsed"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 118
    .local v3, "lastVersion":I
    invoke-static {}, Landroid/webkit/WebViewFactory;->getLoadedPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v0, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 119
    .local v0, "currentVersion":I
    if-le v3, v0, :cond_0

    .line 122
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v4

    invoke-static {v4}, Lcom/android/org/chromium/base/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, "dataDir":Ljava/lang/String;
    const-string v4, "WebViewChromiumFactoryProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WebView package downgraded from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; deleting contents of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/os/FileUtils;->deleteContents(Ljava/io/File;)Z

    .line 127
    .end local v1    # "dataDir":Ljava/lang/String;
    :cond_0
    if-eq v3, v0, :cond_1

    .line 128
    iget-object v4, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "lastVersionCodeUsed"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    .end local v0    # "currentVersion":I
    .end local v3    # "lastVersion":I
    :cond_1
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "WebViewChromiumFactoryProvider"

    const-string v5, "Not doing version downgrade check as framework is too old."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 72
    invoke-static {}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->syncATraceState()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startChromiumLocked()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->setWebContentsDebuggingEnabled(Z)V

    return-void
.end method

.method private ensureChromiumStartedLocked(Z)V
    .locals 4
    .param p1, "onMainThread"    # Z

    .prologue
    .line 159
    sget-boolean v1, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 161
    :cond_0
    iget-boolean v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    if-eqz v1, :cond_2

    .line 194
    :cond_1
    :goto_0
    return-void

    .line 165
    :cond_2
    if-nez p1, :cond_3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 166
    .local v0, "looper":Landroid/os/Looper;
    :goto_1
    const-string v2, "WebViewChromiumFactoryProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Binding Chromium to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "main"

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " looper "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {v0}, Lcom/android/org/chromium/base/ThreadUtils;->setUiThread(Landroid/os/Looper;)V

    .line 171
    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 172
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->startChromiumLocked()V

    goto :goto_0

    .line 165
    .end local v0    # "looper":Landroid/os/Looper;
    :cond_3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_1

    .line 166
    .restart local v0    # "looper":Landroid/os/Looper;
    :cond_4
    const-string v1, "background"

    goto :goto_2

    .line 178
    :cond_5
    new-instance v1, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider$2;

    invoke-direct {v1, p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider$2;-><init>(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;)V

    invoke-static {v1}, Lcom/android/org/chromium/base/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 186
    :goto_3
    iget-boolean v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    if-nez v1, :cond_1

    .line 189
    :try_start_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 190
    :catch_0
    move-exception v1

    goto :goto_3
.end method

.method private getBrowserContextLocked()Lcom/android/org/chromium/android_webview/AwBrowserContext;
    .locals 2

    .prologue
    .line 287
    sget-boolean v0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 288
    :cond_0
    sget-boolean v0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    if-nez v0, :cond_2

    .line 290
    new-instance v0, Lcom/android/org/chromium/android_webview/AwBrowserContext;

    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewPrefs:Landroid/content/SharedPreferences;

    invoke-direct {v0, v1}, Lcom/android/org/chromium/android_webview/AwBrowserContext;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mBrowserContext:Lcom/android/org/chromium/android_webview/AwBrowserContext;

    return-object v0
.end method

.method private initPlatSupportLibrary()V
    .locals 2

    .prologue
    .line 138
    invoke-static {}, Lcom/android/org/chromium/android_webview/AwContents;->getAwDrawGLFunction()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/webview/chromium/DrawGLFunctor;->setChromiumAwDrawGLFunction(J)V

    .line 139
    invoke-static {}, Lcom/android/webview/chromium/GraphicsUtils;->getDrawSWFunctionTable()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->setAwDrawSWFunctionTable(J)V

    .line 140
    invoke-static {}, Lcom/android/webview/chromium/GraphicsUtils;->getDrawGLFunctionTable()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwContents;->setAwDrawGLFunctionTable(J)V

    .line 141
    return-void
.end method

.method private static initTraceEvent()V
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->syncATraceState()V

    .line 145
    new-instance v0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider$1;

    invoke-direct {v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider$1;-><init>()V

    invoke-static {v0}, Landroid/os/SystemProperties;->addChangeCallback(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method private setUpResources(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 308
    invoke-static {p1}, Lcom/android/webview/chromium/ResourceRewriter;->rewriteRValues(Landroid/content/Context;)V

    .line 310
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwResource;->setResources(Landroid/content/res/Resources;)V

    .line 311
    const/high16 v0, 0x1100000

    const v1, 0x1100001

    invoke-static {v0, v1}, Lcom/android/org/chromium/android_webview/AwResource;->setErrorPageResources(II)V

    .line 313
    const v0, 0x1070005

    invoke-static {v0}, Lcom/android/org/chromium/android_webview/AwResource;->setConfigKeySystemUuidMapping(I)V

    .line 315
    return-void
.end method

.method private setWebContentsDebuggingEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 296
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->getUiThreadLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 297
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Toggling of Web Contents Debugging must be done on the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mDevToolsServer:Lcom/android/org/chromium/android_webview/AwDevToolsServer;

    if-nez v0, :cond_2

    .line 301
    if-nez p1, :cond_1

    .line 305
    :goto_0
    return-void

    .line 302
    :cond_1
    new-instance v0, Lcom/android/org/chromium/android_webview/AwDevToolsServer;

    invoke-direct {v0}, Lcom/android/org/chromium/android_webview/AwDevToolsServer;-><init>()V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mDevToolsServer:Lcom/android/org/chromium/android_webview/AwDevToolsServer;

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mDevToolsServer:Lcom/android/org/chromium/android_webview/AwDevToolsServer;

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwDevToolsServer;->setRemoteDebuggingEnabled(Z)V

    goto :goto_0
.end method

.method private startChromiumLocked()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 197
    sget-boolean v7, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->$assertionsDisabled:Z

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/android/org/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 201
    :cond_1
    iget-object v7, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->notifyAll()V

    .line 203
    iget-boolean v7, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    if-eqz v7, :cond_2

    .line 267
    :goto_0
    return-void

    .line 207
    :cond_2
    sget-boolean v7, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v7, :cond_6

    .line 209
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v4

    .line 210
    .local v4, "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    const-string v7, "/data/local/tmp/webview-command-line"

    invoke-static {v7}, Lcom/android/org/chromium/base/CommandLine;->initFromFile(Ljava/lang/String;)V

    .line 211
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 216
    .end local v4    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    :goto_1
    invoke-static {}, Lcom/android/org/chromium/base/CommandLine;->getInstance()Lcom/android/org/chromium/base/CommandLine;

    move-result-object v1

    .line 221
    .local v1, "cl":Lcom/android/org/chromium/base/CommandLine;
    const-string v7, "enable-dcheck"

    invoke-virtual {v1, v7}, Lcom/android/org/chromium/base/CommandLine;->appendSwitch(Ljava/lang/String;)V

    .line 224
    const-string v7, "disable-webview-gl-mode"

    invoke-virtual {v1, v7}, Lcom/android/org/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 225
    const-string v7, "testing-webview-gl-mode"

    invoke-virtual {v1, v7}, Lcom/android/org/chromium/base/CommandLine;->appendSwitch(Ljava/lang/String;)V

    .line 230
    :cond_3
    new-array v7, v10, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, ""

    aput-object v9, v7, v8

    invoke-static {v7}, Lcom/android/org/chromium/content/browser/ResourceExtractor;->setMandatoryPaksToExtract([Ljava/lang/String;)V

    .line 233
    :try_start_0
    invoke-static {}, Lcom/android/org/chromium/base/library_loader/LibraryLoader;->ensureInitialized()V
    :try_end_0
    .catch Lcom/android/org/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    const/4 v7, 0x3

    const-string v8, "/system/lib/"

    invoke-static {v7, v8}, Lcom/android/org/chromium/base/PathService;->override(ILjava/lang/String;)V

    .line 243
    const/16 v0, 0xbbb

    .line 244
    .local v0, "DIR_RESOURCE_PAKS_ANDROID":I
    const/16 v7, 0xbbb

    const-string v8, "/system/framework/webview/paks"

    invoke-static {v7, v8}, Lcom/android/org/chromium/base/PathService;->override(ILjava/lang/String;)V

    .line 248
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->setUpResources(Landroid/content/Context;)V

    .line 249
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->initPlatSupportLibrary()V

    .line 250
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v7

    invoke-static {v7}, Lcom/android/org/chromium/android_webview/AwBrowserProcess;->start(Landroid/content/Context;)V

    .line 252
    sget-boolean v7, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v7, :cond_4

    .line 253
    invoke-direct {p0, v10}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->setWebContentsDebuggingEnabled(Z)V

    .line 256
    :cond_4
    invoke-static {}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->initTraceEvent()V

    .line 257
    iput-boolean v10, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    .line 259
    iget-object v7, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewsToStart:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    .line 260
    .local v6, "wvc":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/webview/chromium/WebViewChromium;>;"
    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/webview/chromium/WebViewChromium;

    .line 261
    .local v5, "w":Lcom/android/webview/chromium/WebViewChromium;
    if-eqz v5, :cond_5

    .line 262
    invoke-virtual {v5}, Lcom/android/webview/chromium/WebViewChromium;->startYourEngine()V

    goto :goto_2

    .line 213
    .end local v0    # "DIR_RESOURCE_PAKS_ANDROID":I
    .end local v1    # "cl":Lcom/android/org/chromium/base/CommandLine;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "w":Lcom/android/webview/chromium/WebViewChromium;
    .end local v6    # "wvc":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/webview/chromium/WebViewChromium;>;"
    :cond_6
    invoke-static {v11}, Lcom/android/org/chromium/base/CommandLine;->init([Ljava/lang/String;)V

    goto :goto_1

    .line 234
    .restart local v1    # "cl":Lcom/android/org/chromium/base/CommandLine;
    :catch_0
    move-exception v2

    .line 235
    .local v2, "e":Lcom/android/org/chromium/base/library_loader/ProcessInitException;
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Error initializing WebView library"

    invoke-direct {v7, v8, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 265
    .end local v2    # "e":Lcom/android/org/chromium/base/library_loader/ProcessInitException;
    .restart local v0    # "DIR_RESOURCE_PAKS_ANDROID":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_7
    iget-object v7, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewsToStart:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 266
    iput-object v11, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewsToStart:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method private static syncATraceState()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 154
    const-string v2, "debug.atrace.tags.enableflags"

    invoke-static {v2, v4, v5}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 155
    .local v0, "enabledFlags":J
    const-wide/16 v2, 0x10

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/android/org/chromium/base/TraceEvent;->setATraceEnabled(Z)V

    .line 156
    return-void

    .line 155
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createWebView(Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)Landroid/webkit/WebViewProvider;
    .locals 4
    .param p1, "webView"    # Landroid/webkit/WebView;
    .param p2, "privateAccess"    # Landroid/webkit/WebView$PrivateAccess;

    .prologue
    .line 376
    new-instance v0, Lcom/android/webview/chromium/WebViewChromium;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/webview/chromium/WebViewChromium;-><init>(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)V

    .line 378
    .local v0, "wvc":Lcom/android/webview/chromium/WebViewChromium;
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 379
    :try_start_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewsToStart:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewsToStart:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    :cond_0
    monitor-exit v2

    .line 384
    return-object v0

    .line 382
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getBrowserContext()Lcom/android/org/chromium/android_webview/AwBrowserContext;
    .locals 2

    .prologue
    .line 281
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 282
    :try_start_0
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->getBrowserContextLocked()Lcom/android/org/chromium/android_webview/AwBrowserContext;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCookieManager()Landroid/webkit/CookieManager;
    .locals 3

    .prologue
    .line 401
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 402
    :try_start_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mCookieManager:Lcom/android/webview/chromium/CookieManagerAdapter;

    if-nez v0, :cond_1

    .line 403
    iget-boolean v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    if-nez v0, :cond_0

    .line 408
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/org/chromium/content/app/ContentMain;->initApplicationContext(Landroid/content/Context;)V

    .line 410
    :cond_0
    new-instance v0, Lcom/android/webview/chromium/CookieManagerAdapter;

    new-instance v2, Lcom/android/org/chromium/android_webview/AwCookieManager;

    invoke-direct {v2}, Lcom/android/org/chromium/android_webview/AwCookieManager;-><init>()V

    invoke-direct {v0, v2}, Lcom/android/webview/chromium/CookieManagerAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwCookieManager;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mCookieManager:Lcom/android/webview/chromium/CookieManagerAdapter;

    .line 412
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mCookieManager:Lcom/android/webview/chromium/CookieManagerAdapter;

    return-object v0

    .line 412
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getGeolocationPermissions()Landroid/webkit/GeolocationPermissions;
    .locals 3

    .prologue
    .line 389
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 390
    :try_start_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mGeolocationPermissions:Lcom/android/webview/chromium/GeolocationPermissionsAdapter;

    if-nez v0, :cond_0

    .line 391
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->ensureChromiumStartedLocked(Z)V

    .line 392
    new-instance v0, Lcom/android/webview/chromium/GeolocationPermissionsAdapter;

    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->getBrowserContextLocked()Lcom/android/org/chromium/android_webview/AwBrowserContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getGeolocationPermissions()Lcom/android/org/chromium/android_webview/AwGeolocationPermissions;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/webview/chromium/GeolocationPermissionsAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwGeolocationPermissions;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mGeolocationPermissions:Lcom/android/webview/chromium/GeolocationPermissionsAdapter;

    .line 395
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mGeolocationPermissions:Lcom/android/webview/chromium/GeolocationPermissionsAdapter;

    return-object v0

    .line 395
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;
    .locals 2

    .prologue
    .line 319
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStaticMethods:Landroid/webkit/WebViewFactoryProvider$Statics;

    if-nez v0, :cond_0

    .line 324
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->ensureChromiumStartedLocked(Z)V

    .line 325
    new-instance v0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider$3;

    invoke-direct {v0, p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider$3;-><init>(Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStaticMethods:Landroid/webkit/WebViewFactoryProvider$Statics;

    .line 370
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStaticMethods:Landroid/webkit/WebViewFactoryProvider$Statics;

    return-object v0

    .line 370
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getWebIconDatabase()Landroid/webkit/WebIconDatabase;
    .locals 2

    .prologue
    .line 418
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 419
    :try_start_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebIconDatabase:Lcom/android/webview/chromium/WebIconDatabaseAdapter;

    if-nez v0, :cond_0

    .line 420
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->ensureChromiumStartedLocked(Z)V

    .line 421
    new-instance v0, Lcom/android/webview/chromium/WebIconDatabaseAdapter;

    invoke-direct {v0}, Lcom/android/webview/chromium/WebIconDatabaseAdapter;-><init>()V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebIconDatabase:Lcom/android/webview/chromium/WebIconDatabaseAdapter;

    .line 423
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebIconDatabase:Lcom/android/webview/chromium/WebIconDatabaseAdapter;

    return-object v0

    .line 423
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getWebStorage()Landroid/webkit/WebStorage;
    .locals 3

    .prologue
    .line 429
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebStorage:Lcom/android/webview/chromium/WebStorageAdapter;

    if-nez v0, :cond_0

    .line 431
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->ensureChromiumStartedLocked(Z)V

    .line 432
    new-instance v0, Lcom/android/webview/chromium/WebStorageAdapter;

    invoke-static {}, Lcom/android/org/chromium/android_webview/AwQuotaManagerBridge;->getInstance()Lcom/android/org/chromium/android_webview/AwQuotaManagerBridge;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/webview/chromium/WebStorageAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwQuotaManagerBridge;)V

    iput-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebStorage:Lcom/android/webview/chromium/WebStorageAdapter;

    .line 434
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 435
    iget-object v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebStorage:Lcom/android/webview/chromium/WebStorageAdapter;

    return-object v0

    .line 434
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getWebViewDatabase(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 440
    iget-object v2, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 441
    :try_start_0
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewDatabase:Lcom/android/webview/chromium/WebViewDatabaseAdapter;

    if-nez v1, :cond_0

    .line 442
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->ensureChromiumStartedLocked(Z)V

    .line 443
    invoke-direct {p0}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->getBrowserContextLocked()Lcom/android/org/chromium/android_webview/AwBrowserContext;

    move-result-object v0

    .line 444
    .local v0, "browserContext":Lcom/android/org/chromium/android_webview/AwBrowserContext;
    new-instance v1, Lcom/android/webview/chromium/WebViewDatabaseAdapter;

    invoke-virtual {v0}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getFormDatabase()Lcom/android/org/chromium/android_webview/AwFormDatabase;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/android/org/chromium/android_webview/AwBrowserContext;->getHttpAuthDatabase(Landroid/content/Context;)Lcom/android/org/chromium/android_webview/HttpAuthDatabase;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/android/webview/chromium/WebViewDatabaseAdapter;-><init>(Lcom/android/org/chromium/android_webview/AwFormDatabase;Lcom/android/org/chromium/android_webview/HttpAuthDatabase;)V

    iput-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewDatabase:Lcom/android/webview/chromium/WebViewDatabaseAdapter;

    .line 448
    .end local v0    # "browserContext":Lcom/android/org/chromium/android_webview/AwBrowserContext;
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mWebViewDatabase:Lcom/android/webview/chromium/WebViewDatabaseAdapter;

    return-object v1

    .line 448
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method hasStarted()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mStarted:Z

    return v0
.end method

.method startYourEngines(Z)V
    .locals 2
    .param p1, "onMainThread"    # Z

    .prologue
    .line 274
    iget-object v1, p0, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 275
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/webview/chromium/WebViewChromiumFactoryProvider;->ensureChromiumStartedLocked(Z)V

    .line 277
    monitor-exit v1

    .line 278
    return-void

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
