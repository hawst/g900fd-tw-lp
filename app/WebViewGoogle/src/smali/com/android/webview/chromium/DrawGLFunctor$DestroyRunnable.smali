.class final Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;
.super Ljava/lang/Object;
.source "DrawGLFunctor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/webview/chromium/DrawGLFunctor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DestroyRunnable"
.end annotation


# instance fields
.field mNativeDrawGLFunctor:J

.field mViewRootImpl:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(J)V
    .locals 1
    .param p1, "nativeDrawGLFunctor"    # J

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-wide p1, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    .line 92
    return-void
.end method


# virtual methods
.method detachNativeFunctor()V
    .locals 4

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mViewRootImpl:Landroid/view/ViewRootImpl;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mViewRootImpl:Landroid/view/ViewRootImpl;

    iget-wide v2, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewRootImpl;->detachFunctor(J)V

    .line 106
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mViewRootImpl:Landroid/view/ViewRootImpl;

    .line 107
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->detachNativeFunctor()V

    .line 98
    iget-wide v0, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    # invokes: Lcom/android/webview/chromium/DrawGLFunctor;->nativeDestroyGLFunctor(J)V
    invoke-static {v0, v1}, Lcom/android/webview/chromium/DrawGLFunctor;->access$000(J)V

    .line 99
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    .line 100
    return-void
.end method
