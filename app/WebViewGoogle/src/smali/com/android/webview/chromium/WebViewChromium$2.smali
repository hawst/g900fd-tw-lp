.class final Lcom/android/webview/chromium/WebViewChromium$2;
.super Landroid/content/ContextWrapper;
.source "WebViewChromium.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/webview/chromium/WebViewChromium;->resourcesContextWrapper(Landroid/content/Context;)Landroid/content/Context;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 278
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getClassLoader()Ljava/lang/ClassLoader;
    .locals 3

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/android/webview/chromium/WebViewChromium$2;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 282
    .local v0, "appCl":Ljava/lang/ClassLoader;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 283
    .local v1, "webViewCl":Ljava/lang/ClassLoader;
    new-instance v2, Lcom/android/webview/chromium/WebViewChromium$2$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/android/webview/chromium/WebViewChromium$2$1;-><init>(Lcom/android/webview/chromium/WebViewChromium$2;Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;)V

    return-object v2
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 299
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/android/webview/chromium/WebViewChromium$2;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 301
    .local v0, "i":Landroid/view/LayoutInflater;
    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 303
    .end local v0    # "i":Landroid/view/LayoutInflater;
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/webview/chromium/WebViewChromium$2;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method
