.class Lcom/android/webview/chromium/DrawGLFunctor;
.super Ljava/lang/Object;
.source "DrawGLFunctor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

.field private mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/android/webview/chromium/DrawGLFunctor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/webview/chromium/DrawGLFunctor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 5
    .param p1, "viewContext"    # J

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    invoke-static {p1, p2}, Lcom/android/webview/chromium/DrawGLFunctor;->nativeCreateGLFunctor(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;-><init>(J)V

    iput-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    .line 40
    new-instance v0, Lcom/android/org/chromium/content/common/CleanupReference;

    iget-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    invoke-direct {v0, p0, v1}, Lcom/android/org/chromium/content/common/CleanupReference;-><init>(Ljava/lang/Object;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    .line 41
    return-void
.end method

.method static synthetic access$000(J)V
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 30
    invoke-static {p0, p1}, Lcom/android/webview/chromium/DrawGLFunctor;->nativeDestroyGLFunctor(J)V

    return-void
.end method

.method private static native nativeCreateGLFunctor(J)J
.end method

.method private static native nativeDestroyGLFunctor(J)V
.end method

.method private static native nativeSetChromiumAwDrawGLFunction(J)V
.end method

.method public static setChromiumAwDrawGLFunction(J)V
    .locals 0
    .param p0, "functionPointer"    # J

    .prologue
    .line 81
    invoke-static {p0, p1}, Lcom/android/webview/chromium/DrawGLFunctor;->nativeSetChromiumAwDrawGLFunction(J)V

    .line 82
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/android/webview/chromium/DrawGLFunctor;->detach()V

    .line 45
    iget-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    invoke-virtual {v0}, Lcom/android/org/chromium/content/common/CleanupReference;->cleanupNow()V

    .line 47
    iput-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mCleanupReference:Lcom/android/org/chromium/content/common/CleanupReference;

    .line 48
    iput-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    .line 50
    :cond_0
    return-void
.end method

.method public detach()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    invoke-virtual {v0}, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->detachNativeFunctor()V

    .line 54
    return-void
.end method

.method public requestDrawGL(Landroid/view/HardwareCanvas;Landroid/view/ViewRootImpl;Z)Z
    .locals 6
    .param p1, "canvas"    # Landroid/view/HardwareCanvas;
    .param p2, "viewRootImpl"    # Landroid/view/ViewRootImpl;
    .param p3, "waitForCompletion"    # Z

    .prologue
    const/4 v0, 0x1

    .line 58
    iget-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    iget-wide v2, v1, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 59
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requested DrawGL on already destroyed DrawGLFunctor"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    if-nez p2, :cond_2

    .line 63
    const/4 v0, 0x0

    .line 77
    :cond_1
    :goto_0
    return v0

    .line 66
    :cond_2
    iget-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    iput-object p2, v1, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mViewRootImpl:Landroid/view/ViewRootImpl;

    .line 67
    if-nez p1, :cond_3

    .line 68
    iget-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    iget-wide v2, v1, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    invoke-virtual {p2, v2, v3, p3}, Landroid/view/ViewRootImpl;->invokeFunctor(JZ)V

    goto :goto_0

    .line 72
    :cond_3
    iget-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    iget-wide v2, v1, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    invoke-virtual {p1, v2, v3}, Landroid/view/HardwareCanvas;->callDrawGLFunction(J)I

    .line 73
    if-eqz p3, :cond_1

    .line 74
    iget-object v1, p0, Lcom/android/webview/chromium/DrawGLFunctor;->mDestroyRunnable:Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;

    iget-wide v2, v1, Lcom/android/webview/chromium/DrawGLFunctor$DestroyRunnable;->mNativeDrawGLFunctor:J

    invoke-virtual {p2, v2, v3, p3}, Landroid/view/ViewRootImpl;->invokeFunctor(JZ)V

    goto :goto_0
.end method
