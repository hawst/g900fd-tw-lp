.class Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;
.super Ljava/lang/Object;
.source "FileShareCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileShareEvent"
.end annotation


# instance fields
.field actionName:Ljava/lang/String;

.field deviceUDN:Ljava/lang/String;

.field errorCode:I

.field eventItem:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

.field eventType:Ljava/lang/String;

.field requestID:I

.field sessionID:Ljava/lang/String;

.field final synthetic this$1:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p2, "requestID"    # I
    .param p3, "actionName"    # Ljava/lang/String;
    .param p4, "sessionID"    # Ljava/lang/String;
    .param p5, "errorCode"    # I

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->this$1:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->sessionID:Ljava/lang/String;

    .line 84
    iput p2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->requestID:I

    .line 85
    iput-object p3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->actionName:Ljava/lang/String;

    .line 86
    iput p5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->errorCode:I

    .line 87
    const-string v0, "fileShareResponse"

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;Ljava/lang/String;I)V
    .locals 1
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "errorCode"    # I

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->this$1:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->deviceUDN:Ljava/lang/String;

    .line 92
    iput p3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->errorCode:I

    .line 93
    const-string v0, "fileShareSubscribeResponse"

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V
    .locals 1
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "sessionID"    # Ljava/lang/String;
    .param p4, "eventItem"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->this$1:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->deviceUDN:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->sessionID:Ljava/lang/String;

    .line 78
    iput-object p4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventItem:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    .line 79
    const-string v0, "fileShareEvent"

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    .line 80
    return-void
.end method
