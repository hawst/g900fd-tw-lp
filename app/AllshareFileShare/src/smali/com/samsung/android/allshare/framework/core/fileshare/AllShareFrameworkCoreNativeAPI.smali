.class public Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;
.super Ljava/lang/Object;
.source "AllShareFrameworkCoreNativeAPI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native cancel(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I
.end method

.method public static native closeSession(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I
.end method

.method public static native createSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I
.end method

.method public static native getDefaultUserAgent()Ljava/lang/String;
.end method

.method public static native getFileReceiverServiceDescription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)I
.end method

.method public static native getFileShareDevByUDN(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;)I
.end method

.method public static native getFileShareDevList(Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getTransportStatus(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native initializeAllShareFrameworkCore()I
.end method

.method public static native notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native notifyNICRemoved(Ljava/lang/String;)I
.end method

.method public static native refreshFileShareDevList()I
.end method

.method public static native setDefaultUserAgent(Ljava/lang/String;)V
.end method

.method public static native setScreenStatus(I)I
.end method

.method public static native startFileReceiverCP()I
.end method

.method public static native stopFileReceiverCP()I
.end method

.method public static native subscribe(Ljava/lang/String;)I
.end method

.method public static native terminateAllShareFrameworkCore()I
.end method

.method public static native transferItem(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I
.end method

.method public static native transferItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;",
            ">;",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;",
            ")I"
        }
    .end annotation
.end method

.method public static native unsubscribe(Ljava/lang/String;)I
.end method
