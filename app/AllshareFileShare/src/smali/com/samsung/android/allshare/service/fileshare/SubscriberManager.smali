.class public Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;
.super Ljava/lang/Object;
.source "SubscriberManager.java"


# instance fields
.field private mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;>;"
        }
    .end annotation
.end field

.field private mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 55
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->init()V

    .line 57
    return-void
.end method

.method private dumpDeviceEventSubscriptionMap()V
    .locals 9

    .prologue
    .line 333
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 336
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    const-string v7, "===== [ Dump Device Event Subscription Map ] start ====="

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 339
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 340
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 341
    .local v0, "dev_id":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 342
    .local v4, "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--> EVENT:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 344
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 345
    .local v3, "subscriber":Landroid/os/Messenger;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--> Subscriber:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/os/Messenger;->hashCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 349
    .end local v0    # "dev_id":Ljava/lang/String;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    .end local v3    # "subscriber":Landroid/os/Messenger;
    .end local v4    # "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    const-string v7, "===== [ Dump Device Event Subscription Map ] end ====="

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method private dumpGlobalEventSubscriptionMap()V
    .locals 9

    .prologue
    .line 359
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 362
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    const-string v7, "===== [ Dump Global Event Subscription Map ] start ====="

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 365
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 366
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 367
    .local v1, "evt_id":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 368
    .local v4, "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--> EVENT:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 370
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 371
    .local v3, "subscriber":Landroid/os/Messenger;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--> Subscriber:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/os/Messenger;->hashCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 375
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    .end local v1    # "evt_id":Ljava/lang/String;
    .end local v3    # "subscriber":Landroid/os/Messenger;
    .end local v4    # "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    const-string v7, "===== [ Dump Global Event Subscription Map ] end ====="

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    return-void
.end method

.method private existDeviceEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 1
    .param p1, "device_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private existGlobalEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 1
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 105
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 73
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 75
    return-void
.end method

.method private declared-synchronized subscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 5
    .param p1, "device_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    const/4 v4, 0x1

    .line 116
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->existDeviceEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 117
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "subscribeDeviceEvent"

    const-string v3, "Already subscribed event"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :goto_0
    monitor-exit p0

    return v4

    .line 123
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v4, :cond_1

    .line 124
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->dumpDeviceEventSubscriptionMap()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 126
    :cond_1
    :try_start_2
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 127
    .local v0, "subscribers":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;"
    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized subscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 5
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    const/4 v4, 0x1

    .line 170
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->existGlobalEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 171
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "subscribeGlobalEvent"

    const-string v3, "Already subscribed event"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    :goto_0
    monitor-exit p0

    return v4

    .line 176
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v4, :cond_1

    .line 177
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->dumpGlobalEventSubscriptionMap()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 179
    :cond_1
    :try_start_2
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 180
    .local v0, "subscribers":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;"
    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized unsubscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 3
    .param p1, "device_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->existDeviceEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v1, "unsubscribeDeviceEvent"

    const-string v2, "NOT subscribed event"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :goto_0
    monitor-exit p0

    return-void

    .line 152
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->dumpDeviceEventSubscriptionMap()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized unsubscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 3
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->existGlobalEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v1, "unsubscribeGlobalEvent"

    const-string v2, "NOT subscribed event"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    :goto_0
    monitor-exit p0

    return-void

    .line 204
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 206
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->dumpGlobalEventSubscriptionMap()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public fini()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 235
    :cond_0
    return-void
.end method

.method public final getDeviceEventSubscriber(Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 4
    .param p1, "device_id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 296
    if-nez p1, :cond_0

    .line 303
    :goto_0
    return-object v0

    .line 298
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 299
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "getDeviceEventSubscriber"

    const-string v3, "Doesn\'t event subscribers..."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    goto :goto_0
.end method

.method public final getGlobalEventSubscriber(Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 3
    .param p1, "evt_id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v1, "getGlobalEventSubscriber"

    const-string v2, "Doesn\'t event subscribers..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const/4 v0, 0x0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    goto :goto_0
.end method

.method public subscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 247
    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248
    if-nez p3, :cond_0

    .line 249
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "subscribeEvent"

    const-string v3, "Oops~. Couldn\'t find out target device id."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/4 v1, 0x0

    .line 259
    :goto_0
    return v1

    .line 253
    :cond_0
    const-string v1, "BUNDLE_STRING_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "device_id":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->subscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v1

    goto :goto_0

    .line 257
    .end local v0    # "device_id":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->subscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)Z

    .line 259
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public unsubscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 272
    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    if-nez p3, :cond_0

    .line 274
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "unsubscribeEvent"

    const-string v3, "Oops~. Couldn\'t find out target device id."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :goto_0
    return-void

    .line 279
    :cond_0
    const-string v1, "BUNDLE_STRING_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "device_id":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->unsubscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)V

    goto :goto_0

    .line 285
    .end local v0    # "device_id":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->unsubscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)V

    goto :goto_0
.end method
