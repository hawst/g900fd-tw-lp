.class public Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDefines;
.super Ljava/lang/Object;
.source "FileShareDefines.java"


# static fields
.field public static final CANCEL_ACTION_NAME:Ljava/lang/String; = "CancelTransport"

.field public static final CLOSESESSION_ACTION_NAME:Ljava/lang/String; = "CloseSession"

.field public static final CREATESESSION_ACTION_NAME:Ljava/lang/String; = "CreateSession"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "urn:samsung.com:device:FileTransferServer:1"

.field public static final SERVICE_TYPE:Ljava/lang/String; = "urn:samsung.com:service:FileTransport:1"

.field public static final STATUS_CANCELED:Ljava/lang/String; = "CANCELED"

.field public static final STATUS_ERROR:Ljava/lang/String; = "ERROR"

.field public static final STATUS_FINISHED:Ljava/lang/String; = "FINISHED"

.field public static final STATUS_READY_FOR_TRANSPORT:Ljava/lang/String; = "READY_FOR_TRANSPORT"

.field public static final STATUS_TRANSPORTING:Ljava/lang/String; = "TRANSPORTING"

.field public static final TRANSPORT_ITEMS_ACTION_NAME:Ljava/lang/String; = "TransportItems"

.field public static final TRANSPORT_SINGLEITEM_ACTION_NAME:Ljava/lang/String; = "TransportItem"

.field public static final TRANSPORT_STATUS:Ljava/lang/String; = "TransportStatus"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
