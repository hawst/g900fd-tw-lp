.class Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;
.super Ljava/lang/Object;
.source "ServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->finiActionHandlers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 654
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 658
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;"
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$200(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 659
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$200(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 660
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;>;"
    if-eqz v2, :cond_1

    .line 661
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 662
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;

    .line 663
    .local v0, "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 664
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 671
    .end local v0    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;>;"
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$300(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 672
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$300(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 673
    .restart local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;>;"
    if-eqz v2, :cond_3

    .line 674
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 675
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;

    .line 676
    .restart local v0    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 677
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 683
    .end local v0    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;>;"
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 684
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;"
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 685
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;

    .line 686
    .restart local v0    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$000(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "run"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Finalize Action Handler : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->getHandlerID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-interface {v0}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->finiActionHandler()V

    goto :goto_2

    .line 692
    .end local v0    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$200(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 693
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$200(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 694
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # setter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4, v8}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$202(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 697
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$300(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 698
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$300(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 699
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    # setter for: Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4, v8}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->access$302(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 701
    :cond_6
    return-void
.end method
