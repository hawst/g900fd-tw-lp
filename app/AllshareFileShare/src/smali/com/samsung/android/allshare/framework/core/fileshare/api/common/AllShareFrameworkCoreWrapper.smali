.class public Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;
.super Ljava/lang/Object;
.source "AllShareFrameworkCoreWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$ErrorCode;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AllShareFrameworkCoreWrapper"

.field private static instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

.field private static libraryLoaded:Z


# instance fields
.field private isAllShareFrameworkCoreStarted:Z

.field private networkExceptionListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->libraryLoaded:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    .line 51
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    .line 62
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;
    .locals 2

    .prologue
    .line 81
    const-class v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    .line 84
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addNetworkExceptionListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDefaultUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 95
    iget-boolean v3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    if-nez v3, :cond_1

    .line 100
    sget-boolean v3, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->libraryLoaded:Z

    if-nez v3, :cond_0

    .line 104
    :try_start_0
    const-string v3, "asf_fileshare"

    invoke-static {v3}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 106
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->libraryLoaded:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->initializeAllShareFrameworkCore()I

    move-result v1

    .line 118
    .local v1, "result":I
    if-eqz v1, :cond_2

    .line 119
    const-string v2, "AllShareFrameworkCoreWrapper"

    const-string v3, "initialize"

    const-string v4, "Initialize AllShareFrameworkCore failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .end local v1    # "result":I
    :goto_0
    return v1

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v3, 0x0

    sput-boolean v3, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->libraryLoaded:Z

    .line 109
    const-string v3, "AllShareFrameworkCoreWrapper"

    const-string v4, "initialize"

    const-string v5, "loadLibrary failure!"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move v1, v2

    .line 110
    goto :goto_0

    .line 114
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x2

    goto :goto_0

    .line 121
    .restart local v1    # "result":I
    :cond_2
    iput-boolean v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    goto :goto_0
.end method

.method public notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;
    .param p3, "subnetMask"    # Ljava/lang/String;

    .prologue
    .line 201
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public notifyNICRemoved(Ljava/lang/String;)I
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 205
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->notifyNICRemoved(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected onNetworkExceptionNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "exceptionType"    # Ljava/lang/String;
    .param p2, "exceptionValue"    # Ljava/lang/String;
    .param p3, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;

    .line 172
    .local v1, "networkExceptionListener":Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;->onNetworkExceptionNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    .end local v1    # "networkExceptionListener":Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;
    :cond_0
    return-void
.end method

.method public removeNetworkExceptionListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper$INetworkExceptionListener;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setDefaultUserAgent(Ljava/lang/String;)V
    .locals 0
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->setDefaultUserAgent(Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public setScreenStatus(I)I
    .locals 1
    .param p1, "on_off"    # I

    .prologue
    .line 197
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->setScreenStatus(I)I

    move-result v0

    return v0
.end method

.method public terminate()I
    .locals 4

    .prologue
    .line 133
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->terminateAllShareFrameworkCore()I

    move-result v0

    .line 134
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 135
    const-string v1, "AllShareFrameworkCoreWrapper"

    const-string v2, "terminate"

    const-string v3, "Terminate AllShareFrameworkCore failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    .line 138
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 139
    return v0
.end method
