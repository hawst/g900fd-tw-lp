.class public Lcom/samsung/android/allshare/service/fileshare/helper/ActionHandlerLoader;
.super Ljava/lang/Object;
.source "ActionHandlerLoader.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ActionHandlerLoader"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadHandlers(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 14
    .param p0, "manager"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const/high16 v11, 0x7f040000

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v8

    .line 39
    .local v8, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_0
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v10

    if-eq v10, v13, :cond_2

    .line 41
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 42
    .local v7, "name":Ljava/lang/String;
    if-eqz v7, :cond_0

    const-string v10, "ActionHandler"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 44
    const/4 v9, 0x0

    .line 45
    .local v9, "path":Ljava/lang/String;
    const-string v10, "ActionHandler"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 46
    const-string v9, "com.samsung.android.allshare.service.fileshare.handler."

    .line 51
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v10

    if-ge v6, v10, :cond_0

    .line 52
    invoke-interface {v8, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "attribute":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v10, "name"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 54
    invoke-interface {v8, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    .line 55
    .local v5, "handler_name":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 57
    :try_start_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 58
    .local v4, "handler_class":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    const-class v12, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    aput-object v12, v10, v11

    invoke-virtual {v4, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 62
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p0, v10, v11

    invoke-virtual {v1, v10}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;

    .line 67
    .local v3, "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    invoke-interface {v3}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->initActionHandler()V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 51
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .end local v3    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;
    .end local v4    # "handler_class":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "handler_name":Ljava/lang/String;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 68
    .restart local v5    # "handler_name":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 69
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    :try_start_2
    const-string v10, "ActionHandlerLoader"

    const-string v11, "loadHandlers"

    const-string v12, "initActionHandler  Exception"

    invoke-static {v10, v11, v12, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 71
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 80
    .end local v0    # "attribute":Ljava/lang/String;
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .end local v5    # "handler_name":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v9    # "path":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 81
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "ActionHandlerLoader"

    const-string v11, "loadHandlers"

    const-string v12, "loadHandlers Exception2 "

    invoke-static {v10, v11, v12, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 82
    throw v2

    .line 85
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_2
    return-void
.end method
