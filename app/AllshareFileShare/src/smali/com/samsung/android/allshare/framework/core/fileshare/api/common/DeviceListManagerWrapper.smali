.class public Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;
.super Ljava/lang/Object;
.source "DeviceListManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DeviceListManagerWrapper"

.field public static final TYPE_DEVICE_ADD:I = 0x0

.field public static final TYPE_DEVICE_CHANGE:I = 0x2

.field public static final TYPE_DEVICE_INVLID:I = 0x0

.field public static final TYPE_DEVICE_REMOVE:I = 0x1

.field private static instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;


# instance fields
.field private deviceChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/List;

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    .line 76
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    return-object v0
.end method

.method public static onDeviceChandedEvent(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;I)V
    .locals 7
    .param p0, "device"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    .param p1, "changeType"    # I

    .prologue
    const/4 v6, 0x7

    .line 208
    if-nez p0, :cond_1

    .line 224
    :cond_0
    return-void

    .line 212
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    move-result-object v1

    .line 213
    .local v1, "instance":Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;
    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;

    .line 214
    .local v2, "listener":Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;
    if-nez p1, :cond_2

    .line 215
    const-string v3, "DeviceListManagerWrapper"

    const-string v4, "onDeviceChandedEvent"

    const-string v5, ">>>>>>>>onDeviceChandedEvent: Receive device add infomation"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-interface {v2, v6, p0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;->onDeviceAdded(ILcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;)V

    goto :goto_0

    .line 219
    :cond_2
    const-string v3, "DeviceListManagerWrapper"

    const-string v4, "onDeviceChandedEvent"

    const-string v5, "onDeviceChandedEvent: Receive device remove infomation"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-interface {v2, v6, p0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;->onDeviceRemoved(ILcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;)V

    goto :goto_0
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    .locals 6
    .param p1, "deviceType"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 106
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;-><init>()V

    .line 107
    .local v0, "device":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    invoke-static {p2, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->getFileShareDevByUDN(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;)I

    move-result v1

    .line 108
    .local v1, "result":I
    if-eqz v1, :cond_0

    .line 109
    const-string v2, "DeviceListManagerWrapper"

    const-string v3, "getDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get FileShare with type["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] & UUID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] failed!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const/4 v0, 0x0

    .line 113
    .end local v0    # "device":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    :cond_0
    return-object v0
.end method

.method public getDeviceList(I)Ljava/util/ArrayList;
    .locals 6
    .param p1, "deviceType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v0, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;>;"
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->getFileShareDevList(Ljava/util/ArrayList;)I

    move-result v1

    .line 88
    .local v1, "result":I
    if-eqz v1, :cond_0

    .line 89
    const-string v2, "DeviceListManagerWrapper"

    const-string v3, "getDeviceList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get FileShare list with type["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    const-string v2, "DeviceListManagerWrapper"

    const-string v3, "getDeviceList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The device list length is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-object v0
.end method

.method public registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 167
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public removeDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public rescanDevice()I
    .locals 4

    .prologue
    .line 122
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->refreshFileShareDevList()I

    move-result v0

    .line 123
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 124
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "rescanDevice"

    const-string v3, "Rescan FileShare failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    return v0
.end method

.method public rescanDeviceByAppID(Ljava/lang/String;)I
    .locals 4
    .param p1, "appID"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->refreshFileShareDevList()I

    move-result v0

    .line 152
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 153
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "rescanDeviceByAppID"

    const-string v3, "Rescan FileShare device by AppID failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_0
    return v0
.end method

.method public rescanDeviceByType(I)I
    .locals 4
    .param p1, "deviceType"    # I

    .prologue
    .line 136
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->refreshFileShareDevList()I

    move-result v0

    .line 137
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 138
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "rescanDeviceByType"

    const-string v3, "Rescan FileShare device by type failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    return v0
.end method

.method public unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 178
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    return v0
.end method
