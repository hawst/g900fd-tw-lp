.class public Lcom/samsung/android/allshare/framework/core/fileshare/data/common/AllShareJniKey;
.super Ljava/lang/Object;
.source "AllShareJniKey.java"


# static fields
.field public static final BUNDLE_INT_REQUEST_ID:Ljava/lang/String; = "BUNDLE_INT_REQUEST_ID"

.field public static final BUNDLE_INT_TV_ANTENNA_MODE:Ljava/lang/String; = "BUNDLE_INT_TV_ANTENNA_MODE"

.field public static final BUNDLE_INT_TV_ID:Ljava/lang/String; = "BUNDLE_INT_TV_ID"

.field public static final BUNDLE_INT_TV_SATELLITE_ID:Ljava/lang/String; = "BUNDLE_INT_TV_SATELLITE_ID"

.field public static final BUNDLE_INT_TV_UIID:Ljava/lang/String; = "BUNDLE_INT_TV_UIID"

.field public static final BUNDLE_STRING_ID:Ljava/lang/String; = "BUNDLE_STRING_ID"

.field public static final BUNDLE_STRING_IP:Ljava/lang/String; = "BUNDLE_STRING_IP"

.field public static final BUNDLE_STRING_PLAYING_URI:Ljava/lang/String; = "BUNDLE_STRING_PLAYING_URI"

.field public static final BUNDLE_STRING_TV_ALL_START_TIME:Ljava/lang/String; = "BUNDLE_STRING_TV_ALL_START_TIME"

.field public static final BUNDLE_STRING_TV_AV_OFF:Ljava/lang/String; = "BUNDLE_STRING_TV_AV_OFF"

.field public static final BUNDLE_STRING_TV_CHANNEL:Ljava/lang/String; = "BUNDLE_STRING_TV_CHANNEL"

.field public static final BUNDLE_STRING_TV_CHANNEL_LIST:Ljava/lang/String; = "BUNDLE_STRING_TV_CHANNEL_LIST"

.field public static final BUNDLE_STRING_TV_CHANNEL_LIST_PIN:Ljava/lang/String; = "BUNDLE_STRING_TV_CHANNEL_LIST_PIN"

.field public static final BUNDLE_STRING_TV_CHANNEL_LIST_TYPE:Ljava/lang/String; = "BUNDLE_STRING_TV_CHANNEL_LIST_TYPE"

.field public static final BUNDLE_STRING_TV_FORCED_FLAG:Ljava/lang/String; = "BUNDLE_STRING_TV_FORCED_FLAG"

.field public static final BUNDLE_STRING_TV_PIN:Ljava/lang/String; = "BUNDLE_STRING_TV_PIN"

.field public static final BUNDLE_STRING_TV_REMIND_INFORMATION:Ljava/lang/String; = "BUNDLE_STRING_TV_REMIND_INFORMATION"

.field public static final BUNDLE_STRING_TV_RESERVATION_TYPE:Ljava/lang/String; = "BUNDLE_STRING_TV_RESERVATION_TYPE"

.field public static final BUNDLE_STRING_TV_SOURCE:Ljava/lang/String; = "BUNDLE_STRING_TV_SOURCE"

.field public static final BUNDLE_STRING_TV_UID:Ljava/lang/String; = "BUNDLE_STRING_TV_UID"

.field public static final BUNDLE_STRING_TV_VIEW_URL:Ljava/lang/String; = "BUNDLE_STRING_TV_VIEW_URL"

.field public static final BUNDLE_STRING_UDN:Ljava/lang/String; = "BUNDLE_STRING_UDN"

.field public static final BUNDLE_STRING_URL:Ljava/lang/String; = "BUNDLE_STRING_URL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
