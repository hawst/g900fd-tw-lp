.class public abstract Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;
.super Ljava/lang/Object;
.source "AsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "ActionWorker"
.end annotation


# instance fields
.field protected mActionId:Ljava/lang/String;

.field protected mReplyMessenger:Landroid/os/Messenger;

.field protected mResId:J

.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    return-void
.end method


# virtual methods
.method protected response(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 305
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 307
    .local v0, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;->mActionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 308
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgType(I)V

    .line 309
    iget-wide v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;->mResId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgID(J)V

    .line 310
    if-nez p1, :cond_0

    .line 311
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "bundle":Landroid/os/Bundle;
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 313
    .restart local p1    # "bundle":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 314
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;->mReplyMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setMessenger(Landroid/os/Messenger;)V

    .line 316
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->getManager()Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->publishCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 318
    return-void
.end method

.method protected responseFailMsg(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 321
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 322
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;->response(Landroid/os/Bundle;)V

    .line 324
    return-void
.end method

.method public abstract work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end method
