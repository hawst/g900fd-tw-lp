.class Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;
.super Ljava/lang/Object;
.source "FileShareCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubscribeInfo"
.end annotation


# instance fields
.field private deviceUDN:Ljava/lang/String;

.field private errorCode:I

.field private isHeldFlag:Z

.field final synthetic this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Ljava/lang/String;)V
    .locals 1
    .param p2, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 883
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 884
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->deviceUDN:Ljava/lang/String;

    .line 885
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->errorCode:I

    .line 886
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->isHeldFlag:Z

    .line 887
    return-void
.end method


# virtual methods
.method public getDeviceUDN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->deviceUDN:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getErrorCode()I
    .locals 1

    .prologue
    .line 910
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->errorCode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isHeld()Z
    .locals 1

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->isHeldFlag:Z

    return v0
.end method

.method public declared-synchronized setErrorCode(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 890
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->errorCode:I

    .line 893
    const-string v0, "FileShareCPWrapper"

    const-string v1, "SubscribeInfo - setErrorCode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    iget-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->isHeldFlag:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 898
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 899
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->isHeldFlag:Z

    .line 900
    const-string v0, "FileShareCPWrapper"

    const-string v1, "SubscribeInfo"

    const-string v2, "SubscribeInfo wait lock is released !!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 903
    :cond_0
    monitor-exit p0

    return-void

    .line 890
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setHold(Z)V
    .locals 0
    .param p1, "bHeld"    # Z

    .prologue
    .line 914
    iput-boolean p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->isHeldFlag:Z

    .line 915
    return-void
.end method
