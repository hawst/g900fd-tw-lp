.class Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
.super Ljava/lang/Object;
.source "FileShareCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionHandlerInfo"
.end annotation


# instance fields
.field private actionName:Ljava/lang/String;

.field private errorCode:I

.field private isHeldFlag:Z

.field private requestInfo:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

.field private sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

.field final synthetic this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;Ljava/lang/String;)V
    .locals 1
    .param p2, "requestInfo"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    .param p3, "actionName"    # Ljava/lang/String;

    .prologue
    .line 835
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 836
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->requestInfo:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    .line 837
    iput-object p3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->actionName:Ljava/lang/String;

    .line 838
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .line 839
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->isHeldFlag:Z

    .line 840
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->errorCode:I

    .line 841
    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    .prologue
    .line 824
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    .prologue
    .line 824
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->requestInfo:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    .prologue
    .line 824
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->actionName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getErrorCode()I
    .locals 1

    .prologue
    .line 864
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->errorCode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isHeld()Z
    .locals 1

    .prologue
    .line 872
    iget-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->isHeldFlag:Z

    return v0
.end method

.method public declared-synchronized setErrorCode(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 848
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->errorCode:I

    .line 851
    const-string v0, "FileShareCPWrapper"

    const-string v1, "ActionHandlerInfo - setErrorCode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    iget-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->isHeldFlag:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 856
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 857
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->isHeldFlag:Z

    .line 858
    const-string v0, "FileShareCPWrapper"

    const-string v1, "ActionHandlerInfo"

    const-string v2, "ActionHandlerInfo wait lock is released !!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 861
    :cond_0
    monitor-exit p0

    return-void

    .line 848
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setHold(Z)V
    .locals 0
    .param p1, "bHeld"    # Z

    .prologue
    .line 868
    iput-boolean p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->isHeldFlag:Z

    .line 869
    return-void
.end method

.method public setSessionInfo(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)V
    .locals 0
    .param p1, "sessionInfo"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .prologue
    .line 844
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .line 845
    return-void
.end method
