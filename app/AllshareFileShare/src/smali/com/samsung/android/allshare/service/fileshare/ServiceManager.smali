.class public Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
.super Landroid/app/Service;
.source "ServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/fileshare/ServiceManager$ServiceManagerBinder;
    }
.end annotation


# static fields
.field private static final AWAIT_TERMINATION_TIME_SEC:I = 0x9

.field private static final LENGTH_DEVICE_NAME_MAX:I = 0x37

.field private static final TEXT_DEVICE_TYPE_CAMERA:Ljava/lang/String; = "[Camera]"

.field private static final TEXT_DEVICE_TYPE_MOBILE:Ljava/lang/String; = "[Mobile]"

.field private static final TEXT_DEVICE_TYPE_SPC:Ljava/lang/String; = "[HomeSync]"

.field private static final TEXT_DEVICE_TYPE_TABLET:Ljava/lang/String; = "[Tablet]"

.field public static excutorMutex:Ljava/lang/Object;

.field private static mContext:Landroid/content/Context;

.field private static mContextMutex:Ljava/lang/Object;

.field private static mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

.field private static mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

.field private static mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

.field private static mSystemEventListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mSystemEventListenerMutex:Ljava/lang/Object;

.field private static mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;


# instance fields
.field private mAsyncActionHandlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mBinder:Landroid/os/IBinder;

.field private mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

.field private mFlagRunning:Z

.field private mHandlerTimer:Landroid/os/Handler;

.field private mISubscriberStub:Lcom/sec/android/allshare/iface/ISubscriber$Stub;

.field private mRunnableTimer:Ljava/lang/Runnable;

.field private mSyncActionHandlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 80
    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContext:Landroid/content/Context;

    .line 83
    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    .line 86
    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    .line 95
    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    .line 98
    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    .line 101
    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    .line 109
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    .line 111
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    .line 113
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->excutorMutex:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 74
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    .line 89
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    .line 92
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mFlagRunning:Z

    .line 107
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    .line 135
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$ServiceManagerBinder;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$ServiceManagerBinder;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mBinder:Landroid/os/IBinder;

    .line 429
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$1;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

    .line 616
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$2;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    .line 709
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$4;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mISubscriberStub:Lcom/sec/android/allshare/iface/ISubscriber$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->updateFriendlyName()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    return-object p1
.end method

.method private bindServiceManager(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 582
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 584
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 585
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "bindServiceManager"

    const-string v4, "intent.getAction() == null!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :goto_0
    return-object v1

    .line 589
    :cond_0
    const-string v2, "com.samsung.android.allshare.service.fileshare.SUBSCRIBE_SERVICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 590
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "bindServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SubscriberID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "com.sec.android.allshare.iface.subscriber"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mISubscriberStub:Lcom/sec/android/allshare/iface/ISubscriber$Stub;

    goto :goto_0

    .line 594
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "bindServiceManager"

    const-string v4, "bindServiceManager Ignore bind request..."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 466
    if-nez p1, :cond_0

    .line 467
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    const-string v4, "use Build.MODEL"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 524
    :goto_0
    return-object v2

    .line 472
    :cond_0
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 473
    const-string v2, "/"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 474
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remove all \'/\': "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_1
    const-string v2, "line.separator"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 479
    const-string v2, "line.separator"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 480
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "replace line.separator : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_2
    :try_start_0
    const-string v0, "[Mobile]"

    .line 497
    .local v0, "deviceType":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x258

    if-lt v2, v3, :cond_5

    .line 498
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    const-string v4, "set Tablet Device"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    const-string v0, "[Tablet]"

    .line 504
    :goto_1
    if-nez p1, :cond_6

    .line 505
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "use Build.MODEL with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 513
    :cond_3
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x37

    if-lt v2, v3, :cond_4

    .line 514
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    const-string v4, "deviceName length is over LENGTH_DEVICE_NAME_MAX"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const/4 v2, 0x0

    const/16 v3, 0x36

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .end local v0    # "deviceType":Ljava/lang/String;
    :cond_4
    :goto_3
    move-object v2, p1

    .line 524
    goto/16 :goto_0

    .line 501
    .restart local v0    # "deviceType":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    const-string v4, "set default MOBILE Device"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 519
    .end local v0    # "deviceType":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 520
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "checkDeviceName"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 508
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "deviceType":Ljava/lang/String;
    :cond_6
    :try_start_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 509
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object p1

    goto :goto_2
.end method

.method public static displayToast(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "flag_long"    # Z

    .prologue
    .line 1146
    const/4 v0, 0x0

    .line 1148
    .local v0, "length":I
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 1149
    const/4 v0, 0x1

    .line 1152
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p0, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1154
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1156
    return-void
.end method

.method private finiActionHandlers()V
    .locals 2

    .prologue
    .line 648
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager$3;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 704
    return-void
.end method

.method private finiServiceManager()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 292
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->finiActionHandlers()V

    .line 294
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->shutdownAndAwaitTermination()V

    .line 298
    :cond_0
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 299
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 300
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 301
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    .line 303
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    sput-object v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    .line 307
    sput-object v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    .line 309
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->terminate()I

    .line 311
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    if-eqz v0, :cond_2

    .line 312
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->fini()V

    .line 313
    sput-object v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    .line 316
    :cond_2
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    if-eqz v0, :cond_3

    .line 317
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->fini()V

    .line 318
    sput-object v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    .line 321
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "finiServiceManager"

    const-string v2, "Finalize AllShare Service Manager success!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    return-void

    .line 303
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized getContext()Landroid/content/Context;
    .locals 3

    .prologue
    .line 785
    const-class v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 786
    :try_start_1
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContext:Landroid/content/Context;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0

    .line 787
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 785
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getSubscriberManager()Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;
    .locals 1

    .prologue
    .line 797
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    return-object v0
.end method

.method public static getSystemEventMonitor()Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
    .locals 1

    .prologue
    .line 808
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    return-object v0
.end method

.method private initActionHandlers()V
    .locals 4

    .prologue
    .line 637
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/allshare/service/fileshare/helper/ActionHandlerLoader;->loadHandlers(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 642
    :goto_0
    return-void

    .line 638
    :catch_0
    move-exception v0

    .line 639
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "initActionHandlers"

    const-string v3, "initActionHandlers Exception "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 640
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->stopSelf()V

    goto :goto_0
.end method

.method private initServiceManager()V
    .locals 10

    .prologue
    .line 243
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 245
    .local v0, "beginInitServiceTime":J
    new-instance v6, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    sput-object v6, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    .line 246
    new-instance v6, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;-><init>()V

    sput-object v6, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    .line 247
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    .line 248
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    .line 250
    sget-object v7, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 251
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    .line 252
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    new-instance v6, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;-><init>()V

    sput-object v6, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    .line 256
    new-instance v6, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    sput-object v6, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    .line 262
    sget-object v6, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-nez v6, :cond_1

    .line 263
    sget-object v7, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->excutorMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 264
    :try_start_1
    sget-object v6, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-nez v6, :cond_0

    .line 265
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    sput-object v6, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    .line 267
    :cond_0
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 271
    :cond_1
    sget-object v7, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 272
    :try_start_2
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v6

    sget-object v8, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->initialize(Landroid/content/Context;)I

    .line 273
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 276
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->initActionHandlers()V

    .line 279
    new-instance v6, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;-><init>()V

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->addSystemEventListener(Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;)V

    .line 281
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 282
    .local v2, "endInitServiceTime":J
    sub-long v4, v2, v0

    .line 283
    .local v4, "spendInitServiceTime":J
    iget-object v6, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "initServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ">>>>>> spend[--Init--]ServiceTime = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    return-void

    .line 252
    .end local v2    # "endInitServiceTime":J
    .end local v4    # "spendInitServiceTime":J
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 267
    :catchall_1
    move-exception v6

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v6

    .line 273
    :catchall_2
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v6
.end method

.method public static makeStartNotification()Landroid/app/Notification;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1120
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1121
    .local v2, "service_control":Landroid/content/Intent;
    const-string v3, "com.samsung.android.allshare.framework"

    const-string v4, "com.samsung.android.allshare.framework.launcher.AllShareDestroyer"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1123
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5, v2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1126
    .local v1, "pi":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const-string v4, "AllShare Service Works Hard."

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const-string v4, "Experience New Convergence Life."

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const/high16 v4, 0x7f020000

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    const-string v4, "AllShare Service Started."

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 1132
    .local v0, "noti":Landroid/app/Notification;
    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/app/Notification;->flags:I

    .line 1134
    return-object v0
.end method

.method private manageProcessForeground(Z)V
    .locals 4
    .param p1, "start"    # Z

    .prologue
    .line 1172
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 1174
    .local v1, "mAm":Landroid/app/IActivityManager;
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mBinder:Landroid/os/IBinder;

    if-eqz v2, :cond_0

    .line 1175
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mBinder:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1180
    :cond_0
    :goto_0
    return-void

    .line 1177
    :catch_0
    move-exception v0

    .line 1178
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static sendBroadcastMsg(Ljava/lang/String;)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;

    .prologue
    .line 1165
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1166
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1167
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1169
    return-void
.end method

.method private shutdownAndAwaitTermination()V
    .locals 5

    .prologue
    .line 328
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 333
    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 335
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 338
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "shutdownAndAwaitTermination"

    const-string v3, "AsyncActionHandler excutor Pool did not terminate"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :cond_0
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    .line 351
    return-void

    .line 343
    :catch_0
    move-exception v0

    .line 345
    .local v0, "ie":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 347
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private declared-synchronized startServiceManager(Landroid/content/Intent;II)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v9, 0x1

    .line 358
    monitor-enter p0

    :try_start_0
    iget-boolean v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mFlagRunning:Z

    if-ne v8, v9, :cond_0

    .line 359
    iget-object v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v9, "startServiceManager"

    const-string v10, "AllShare Service is ALREADY running..."

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :goto_0
    monitor-exit p0

    return-void

    .line 362
    :cond_0
    :try_start_1
    iget-object v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v9, "startServiceManager"

    const-string v10, "Start AllShare Service..."

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    sget-object v9, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    monitor-enter v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    sput-object v8, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mContext:Landroid/content/Context;

    .line 368
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 371
    :try_start_3
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->initServiceManager()V

    .line 374
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 377
    .local v0, "beginStartServiceTime":J
    sget-object v8, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->start()V

    .line 380
    sget-object v8, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->start()V

    .line 383
    const-string v8, "com.samsung.android.allshare.service.fileshare.ServiceManager.START_COMPLETED"

    invoke-static {v8}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->sendBroadcastMsg(Ljava/lang/String;)V

    .line 386
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mFlagRunning:Z

    .line 388
    sget-object v8, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->startMonitor()V

    .line 392
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->updateFriendlyName()V

    .line 395
    new-instance v4, Landroid/content/IntentFilter;

    const-string v8, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-direct {v4, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 396
    .local v4, "filter":Landroid/content/IntentFilter;
    iget-object v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v4}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 402
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->start()I

    move-result v5

    .line 403
    .local v5, "result":I
    if-eqz v5, :cond_1

    .line 404
    iget-object v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v9, "startServiceManager"

    const-string v10, "start file share cp failed!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 409
    .local v2, "endStartServiceTime":J
    sub-long v6, v2, v0

    .line 410
    .local v6, "spendStartServiceTime":J
    iget-object v8, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v9, "startServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ">>>>>> spend[--Start--]ServiceTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->manageProcessForeground(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 358
    .end local v0    # "beginStartServiceTime":J
    .end local v2    # "endStartServiceTime":J
    .end local v4    # "filter":Landroid/content/IntentFilter;
    .end local v5    # "result":I
    .end local v6    # "spendStartServiceTime":J
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 368
    :catchall_1
    move-exception v8

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private declared-synchronized stopServiceManager()V
    .locals 4

    .prologue
    .line 533
    monitor-enter p0

    :try_start_0
    const-string v1, "com.samsung.android.allshare.service.fileshare.ServiceManager.STOP_COMPLETED"

    invoke-static {v1}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->sendBroadcastMsg(Ljava/lang/String;)V

    .line 534
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "Stop AllShare Service..."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->stop()I

    move-result v0

    .line 537
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 538
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop file share cp failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mFlagRunning:Z

    .line 544
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    if-eqz v1, :cond_1

    .line 545
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->stopMonitor()V

    .line 548
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    if-eqz v1, :cond_2

    .line 549
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->setStopFlag()V

    .line 552
    :cond_2
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    if-eqz v1, :cond_3

    .line 553
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->setStopFlag()V

    .line 556
    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->manageProcessForeground(Z)V

    .line 563
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 571
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->finiServiceManager()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 573
    monitor-exit p0

    return-void

    .line 533
    .end local v0    # "result":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private unbindServiceManager(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 607
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "unbindServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unbind method executed...., ACTION : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    const/4 v0, 0x1

    return v0
.end method

.method private updateFriendlyName()V
    .locals 5

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 454
    .local v0, "name":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->checkDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 460
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "updateFriendlyName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDefaultUserAgent to : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->setDefaultUserAgent(Ljava/lang/String;)V

    .line 462
    return-void
.end method


# virtual methods
.method public addSystemEventListener(Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;

    .prologue
    .line 1068
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 1069
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1070
    monitor-exit v1

    .line 1071
    return-void

    .line 1070
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public allocateCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 860
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    if-eqz v0, :cond_0

    .line 861
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)Z

    .line 867
    :goto_0
    return-void

    .line 863
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "allocateCVMessage"

    const-string v2, "allocateCVMessage NullPointerException AllocateTask is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getActionHandlerMap(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 844
    const-string v0, "ACTION_TYPE_ASYNC"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    .line 850
    :goto_0
    return-object v0

    .line 846
    :cond_0
    const-string v0, "ACTION_TYPE_SYNC"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 847
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    goto :goto_0

    .line 850
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAsyncActionHandlerMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 820
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getSyncActionHandlerMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 832
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1091
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1092
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "notifyEvent2Operator"

    const-string v3, "mSystemEventListenerList is null, FileShare service has stopped"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    :goto_0
    return-void

    .line 1096
    :cond_0
    sget-object v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 1097
    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1098
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1099
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;

    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;->eventNotifyReceived(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 1101
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;>;"
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1079
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 1081
    .local v0, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->publishCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 1082
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-direct {p0, p1, v0, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->startServiceManager(Landroid/content/Intent;II)V

    .line 201
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->bindServiceManager(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 161
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 162
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 187
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 188
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->stopServiceManager()V

    .line 189
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 209
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 211
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 212
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 170
    if-eqz p1, :cond_0

    .line 171
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 172
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "onStartCommand"

    const-string v2, "[ TRACE ] onStartCommand"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->startServiceManager(Landroid/content/Intent;II)V

    .line 177
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "onStartCommand"

    const-string v2, "intent is NULL - Restarted Process"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 221
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    .line 223
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 225
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->unbindServiceManager(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public publishCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 875
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    if-eqz v0, :cond_0

    .line 876
    sget-object v0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)I

    .line 882
    :goto_0
    return-void

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "publishCVMessage"

    const-string v2, "publishCVMessage NullPointerException PublishTask is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerActionHandler(Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;)Z
    .locals 10
    .param p1, "handler"    # Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;

    .prologue
    const/4 v5, 0x0

    .line 891
    if-nez p1, :cond_0

    .line 892
    iget-object v6, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "registerActionHandler"

    const-string v8, "Oops~~. Null Action Handler warning"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    :goto_0
    return v5

    .line 896
    :cond_0
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->getActionType()Ljava/lang/String;

    move-result-object v0

    .line 898
    .local v0, "action_type":Ljava/lang/String;
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->getActionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 899
    .local v1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 900
    iget-object v6, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "registerActionHandler"

    const-string v8, "Oops~~. NULL Action ID list..."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 904
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getActionHandlerMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 905
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;"
    if-nez v4, :cond_2

    .line 906
    iget-object v6, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "registerActionHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "map == null for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 910
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 911
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 912
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 913
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_4

    .line 914
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "registerActionHandler"

    const-string v7, "Oops~~. Action ID failure.."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 918
    :cond_4
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 919
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "registerActionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Oops~~. Couldn\'t register Action handler for duplicated Action ID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->getHandlerID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 925
    :cond_5
    invoke-virtual {v4, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 928
    .end local v2    # "id":Ljava/lang/String;
    :cond_6
    const/4 v5, 0x1

    goto/16 :goto_0
.end method

.method public requestCVAsyncToHandler(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 6
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    const/4 v1, 0x0

    .line 986
    if-nez p2, :cond_0

    .line 1000
    :goto_0
    return v1

    .line 990
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 991
    .local v0, "action_id":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 992
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "requestCVAsyncToHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No such Action Handler with [ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 998
    :cond_1
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->allocateCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 1000
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public requestCVSyncToHandler(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 7
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1012
    if-nez p2, :cond_0

    .line 1013
    const/4 v2, 0x0

    .line 1026
    :goto_0
    return-object v2

    .line 1015
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 1016
    .local v0, "action_id":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1017
    iget-object v3, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v4, "requestCVSyncToHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No such Action Handler with [ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(I)V

    .line 1020
    .local v2, "m":Lcom/sec/android/allshare/iface/CVMessage;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1024
    .end local v2    # "m":Lcom/sec/android/allshare/iface/CVMessage;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/fileshare/handler/SyncActionHandler;

    .line 1026
    .local v1, "handler":Lcom/samsung/android/allshare/service/fileshare/handler/SyncActionHandler;
    invoke-virtual {v1, p2}, Lcom/samsung/android/allshare/service/fileshare/handler/SyncActionHandler;->responseSyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v2

    goto :goto_0
.end method

.method public requestEventSubscription(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 4
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1038
    if-nez p2, :cond_0

    .line 1039
    const/4 v3, 0x0

    .line 1045
    :goto_0
    return v3

    .line 1041
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v1

    .line 1042
    .local v1, "evt":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1043
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v2

    .line 1045
    .local v2, "messenger":Landroid/os/Messenger;
    sget-object v3, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    invoke-virtual {v3, v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->subscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v3

    goto :goto_0
.end method

.method public requestEventUnsubscription(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 4
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1056
    if-nez p2, :cond_0

    .line 1065
    :goto_0
    return-void

    .line 1060
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v1

    .line 1061
    .local v1, "evt":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1062
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v2

    .line 1063
    .local v2, "messenger":Landroid/os/Messenger;
    sget-object v3, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    invoke-virtual {v3, v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->unsubscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setScreenStatus(Z)V
    .locals 2
    .param p1, "on_off"    # Z

    .prologue
    .line 1107
    if-eqz p1, :cond_0

    .line 1108
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    .line 1112
    :goto_0
    return-void

    .line 1110
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    goto :goto_0
.end method

.method public unregisterActionHandler(Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;)V
    .locals 9
    .param p1, "handler"    # Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;

    .prologue
    .line 939
    if-nez p1, :cond_1

    .line 940
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    const-string v7, "Oops~~. Null Action Handler warning"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :cond_0
    :goto_0
    return-void

    .line 944
    :cond_1
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->getActionType()Ljava/lang/String;

    move-result-object v0

    .line 946
    .local v0, "action_type":Ljava/lang/String;
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;->getActionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 947
    .local v1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_2

    .line 948
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    const-string v7, "Oops~~. NULL Action ID list..."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 952
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getActionHandlerMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 953
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;>;"
    if-nez v4, :cond_3

    .line 954
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "map == null for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 958
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 959
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 960
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 961
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_5

    .line 962
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    const-string v7, "Oops~~. Action ID failure.."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 966
    :cond_5
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 967
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Oops~~. Such ActionHandler[ "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ] doesn\'t exist."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 972
    :cond_6
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
