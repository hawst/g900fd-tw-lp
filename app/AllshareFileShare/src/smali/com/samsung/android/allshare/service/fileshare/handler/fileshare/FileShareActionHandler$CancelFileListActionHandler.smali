.class Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelFileListActionHandler;
.super Ljava/lang/Object;
.source "FileShareActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CancelFileListActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelFileListActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;

    .prologue
    .line 290
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelFileListActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V

    return-void
.end method


# virtual methods
.method public handleAction(Lcom/sec/android/allshare/iface/CVMessage;)Landroid/os/Bundle;
    .locals 8
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 293
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 294
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 295
    .local v3, "udn":Ljava/lang/String;
    const-string v4, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 298
    .local v2, "sessionId":Ljava/lang/String;
    const-string v4, "FileShareActionHandler"

    const-string v5, "CancelFileListActionHandler [handleAction]: "

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sessionID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 302
    :cond_0
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-object v0

    .line 306
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->cancel(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 307
    .local v1, "result":I
    if-nez v1, :cond_2

    .line 308
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :goto_1
    const-string v4, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_2
    const-string v4, "FileShareActionHandler"

    const-string v5, "[handleAction]: "

    const-string v6, "CancelFileListActionHandler.handleAction UPnPException Fail to request transfer"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
