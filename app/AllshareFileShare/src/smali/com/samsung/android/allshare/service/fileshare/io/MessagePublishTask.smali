.class public Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;
.super Ljava/lang/Thread;
.source "MessagePublishTask.java"


# static fields
.field private static final THREAD_SLEEP_TIME:I = 0x3e8

.field private static final mActionFINI:Ljava/lang/String; = "com.samsung.android.allshare.framework.io.ACTION_FINI"


# instance fields
.field private mQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/sec/android/allshare/iface/CVMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mStopFlag:Z

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 38
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 43
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mStopFlag:Z

    .line 52
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 53
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mStopFlag:Z

    .line 55
    return-void
.end method

.method private handleEventMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 14
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    const/4 v13, 0x1

    .line 100
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v4

    .line 101
    .local v4, "evt_id":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "BUNDLE_STRING_CATEGORY"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, "category":Ljava/lang/String;
    const/4 v8, 0x0

    .line 104
    .local v8, "subscribers":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;"
    const-string v9, "com.sec.android.allshare.event.GLOBAL"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 105
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getSubscriberManager()Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->getGlobalEventSubscriber(Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v8

    .line 111
    :cond_0
    :goto_0
    if-nez v8, :cond_3

    .line 112
    iget-object v9, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v10, "handleEventMessage"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "No subscriber waiting event : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_1
    return v13

    .line 106
    :cond_2
    const-string v9, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "BUNDLE_STRING_ID"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "dev_id":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getSubscriberManager()Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;

    move-result-object v9

    invoke-virtual {v9, v2}, Lcom/samsung/android/allshare/service/fileshare/SubscriberManager;->getDeviceEventSubscriber(Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v8

    goto :goto_0

    .line 116
    .end local v2    # "dev_id":Ljava/lang/String;
    :cond_3
    invoke-virtual {v8}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 117
    .local v5, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 118
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Messenger;

    .line 120
    .local v7, "subscriber":Landroid/os/Messenger;
    if-eqz v7, :cond_4

    .line 122
    :try_start_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 123
    .local v6, "msg":Landroid/os/Message;
    invoke-virtual {v6}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 124
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v9, "EVT_MSG_KEY"

    invoke-virtual {v0, v9, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 125
    invoke-virtual {v7, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 127
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v6    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v3

    .line 133
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v8, v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 135
    iget-object v9, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v10, "handleEventMessage"

    const-string v11, "handleEventMessage RemoteException"

    invoke-static {v9, v10, v11, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private handlePublishMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 200
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getMsgType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 213
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.allshare.framework.io.ACTION_FINI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v1, "handlePublishMessage"

    const-string v2, "Received finalize message..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :goto_0
    const/4 v0, 0x0

    .line 222
    :goto_1
    return v0

    .line 202
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v1, "handlePublishMessage"

    const-string v2, "Oops~~. Undefined Context Variable type..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 206
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->handleResponseMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z

    goto :goto_2

    .line 209
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v1, "handlePublishMessage"

    const-string v2, "handle EVENT message..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->handleEventMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z

    goto :goto_2

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v1, "handlePublishMessage"

    const-string v2, "Oops~~. Invalid Context Variable type..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private handleResponseMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 7
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v1

    .line 74
    .local v1, "destination":Landroid/os/Messenger;
    if-nez v1, :cond_0

    .line 75
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v5, "handleResponseMessage"

    const-string v6, "Oops~~. invalid destination"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const/4 v4, 0x0

    .line 89
    :goto_0
    return v4

    .line 80
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 81
    .local v3, "msg":Landroid/os/Message;
    invoke-virtual {v3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "RES_MSG_KEY"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 83
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "msg":Landroid/os/Message;
    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    .line 84
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v5, "handleResponseMessage"

    const-string v6, "handleResponseMessage RemoteException "

    invoke-static {v4, v5, v6, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized putQ(Lcom/sec/android/allshare/iface/CVMessage;)I
    .locals 4
    .param p1, "msg"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 255
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    .line 256
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v2, "putQ"

    const-string v3, "putQ InterruptedException "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    const/4 v1, -0x1

    goto :goto_0

    .line 255
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public run()V
    .locals 6

    .prologue
    .line 275
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v4, 0x3e8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/allshare/iface/CVMessage;

    .line 277
    .local v1, "msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v1, :cond_0

    .line 280
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->handlePublishMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    .end local v1    # "msg":Lcom/sec/android/allshare/iface/CVMessage;
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mStopFlag:Z

    if-eqz v2, :cond_0

    .line 291
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 293
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v3, "run"

    const-string v4, "run InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public setStopFlag()V
    .locals 4

    .prologue
    .line 238
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mTag:Ljava/lang/String;

    const-string v2, "setStopFlag"

    const-string v3, "SetStopFlag"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->mStopFlag:Z

    .line 240
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 241
    .local v0, "fin":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v1, "com.samsung.android.allshare.framework.io.ACTION_FINI"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 242
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/io/MessagePublishTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)I

    .line 244
    return-void
.end method
