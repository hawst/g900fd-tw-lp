.class public Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
.super Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;
.source "FileShareActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelFileListActionHandler;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelActionHandler;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;,
        Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FileShareActionHandler"


# instance fields
.field private final mHandlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceManagerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/allshare/service/fileshare/ServiceManager;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mServiceManagerRef:Ljava/lang/ref/WeakReference;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;

    .line 47
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mServiceManagerRef:Ljava/lang/ref/WeakReference;

    .line 48
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mServiceManagerRef:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "reqId"    # J

    .prologue
    .line 71
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 54
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_CANCEL"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->setActionID(Ljava/lang/String;)V

    .line 55
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_RECEIVE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->setActionID(Ljava/lang/String;)V

    .line 56
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->setActionID(Ljava/lang/String;)V

    .line 57
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->setActionID(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_CANCEL"

    new-instance v2, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelActionHandler;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_RECEIVE"

    new-instance v2, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    new-instance v2, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelFileListActionHandler;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$CancelFileListActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    new-instance v2, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 85
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->removeEventListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;)V

    .line 86
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->addEventListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;)V

    .line 81
    return-void
.end method

.method public onTransportStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "transportStatus"    # Ljava/lang/String;

    .prologue
    .line 356
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;

    .line 358
    .local v1, "session":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->deviceUDN:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 359
    :cond_0
    const-string v2, "FileShareActionHandler"

    const-string v3, "[onTransportStatus]: "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onEventReceived::querySessionInfoBySessionID failed! sessionID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :goto_0
    return-void

    .line 364
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 365
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v2, "FINISHED"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 372
    const-string v2, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_COMPLECTED"

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->access$1100(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 374
    iget v2, v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    .line 375
    iget v2, v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    iget-object v3, v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 376
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    :cond_2
    :goto_1
    const-string v2, "FileShareActionHandler"

    const-string v3, "[onTransportStatus]: "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "received status is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 378
    :cond_3
    const-string v2, "CANCELED"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 379
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v2, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_FAILED"

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->access$1100(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 381
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 382
    :cond_4
    const-string v2, "ERROR"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 383
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const-string v2, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_FAILED"

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->access$1100(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 385
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public onTransportingInfo(Ljava/lang/String;Ljava/lang/String;JJI)V
    .locals 7
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "totalSize"    # J
    .param p5, "sendSize"    # J
    .param p7, "status"    # I

    .prologue
    .line 394
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;

    .line 396
    .local v1, "session":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->deviceUDN:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 397
    :cond_0
    const-string v2, "FileShareActionHandler"

    const-string v3, "[onTransportingInfo]: "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onEventReceived::querySessionInfoBySessionID failed! sessionID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :goto_0
    return-void

    .line 402
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 403
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v2, "BUNDLE_LONG_FILE_SIZE"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 408
    const-string v2, "BUNDLE_LONG_FILE_PROGRESS"

    invoke-virtual {v0, v2, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 410
    const-string v2, "FileShareActionHandler"

    const-string v3, "[onTransportingInfo]: "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " get transport status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v2, 0x5

    if-ne v2, p7, :cond_2

    .line 412
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v2, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_FAILED"

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->access$1100(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 415
    :cond_2
    const-string v2, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_PROGRESS"

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->access$1100(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
