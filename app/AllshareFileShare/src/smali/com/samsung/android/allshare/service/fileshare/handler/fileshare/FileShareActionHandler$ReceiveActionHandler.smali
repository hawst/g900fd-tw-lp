.class Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;
.super Ljava/lang/Object;
.source "FileShareActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiveActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V

    return-void
.end method


# virtual methods
.method public handleAction(Lcom/sec/android/allshare/iface/CVMessage;)Landroid/os/Bundle;
    .locals 15
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 132
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v11, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, "id":Ljava/lang/String;
    const-string v11, "BUNDLE_STRING_NAME"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 135
    .local v6, "name":Ljava/lang/String;
    const-string v11, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 137
    .local v7, "path":Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 140
    .local v5, "mSessionId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 142
    .local v10, "time":Ljava/lang/String;
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 143
    :cond_0
    const-string v11, "FileShareActionHandler"

    const-string v12, "[ReceiveActionHandler]: "

    const-string v13, "ReceiveActionHandler>>>time == null || time.isEmpty!.."

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v11, "BUNDLE_ENUM_ERROR"

    sget-object v12, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :goto_0
    return-object v0

    .line 149
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v11

    if-nez v11, :cond_3

    .line 151
    :cond_2
    const-string v11, "FileShareActionHandler"

    const-string v12, "[ReceiveActionHandler]: "

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "ReceiveActionHandler>>>file["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] is not exist or cannot read "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v11, "BUNDLE_ENUM_ERROR"

    sget-object v12, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 158
    .local v2, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v11

    invoke-virtual {v11, v4, v6, v2, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->transfer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v8

    .line 161
    .local v8, "result":I
    if-nez v8, :cond_5

    .line 162
    const-string v11, "BUNDLE_ENUM_ERROR"

    sget-object v12, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v3, 0x0

    .line 164
    .local v3, "firstSessionID":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_4

    .line 165
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "firstSessionID":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 167
    .restart local v3    # "firstSessionID":Ljava/lang/String;
    :cond_4
    new-instance v9, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;

    iget-object v11, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    const/4 v12, 0x0

    invoke-direct {v9, v11, v12}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    .line 168
    .local v9, "session":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    iput-object v4, v9, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->deviceUDN:Ljava/lang/String;

    .line 169
    iput-object v2, v9, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferFiles:Ljava/util/ArrayList;

    .line 170
    iget-object v11, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;
    invoke-static {v11}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->access$900(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v3, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    const-string v11, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v0, v11, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .end local v3    # "firstSessionID":Ljava/lang/String;
    .end local v9    # "session":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    :goto_1
    const-string v11, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {v0, v11, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    :cond_5
    const-string v11, "FileShareActionHandler"

    const-string v12, "[ReceiveActionHandler]: "

    const-string v13, "ReceiveActionHandler.handleAction UPnPException Fail to request transfer"

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v11, "BUNDLE_ENUM_ERROR"

    sget-object v12, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
