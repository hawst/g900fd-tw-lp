.class public Lcom/samsung/android/allshare/service/fileshare/ServiceConfig;
.super Ljava/lang/Object;
.source "ServiceConfig.java"


# static fields
.field public static final ACTION_HANDLERS_LOCATION:Ljava/lang/String; = "com.samsung.android.allshare.service.fileshare.handler."

.field public static final FRIENDLY_NAME:Ljava/lang/String; = "friendlyName"

.field public static final KEY_ATTRIBUTE:Ljava/lang/String; = "attribute"

.field public static final KEY_VALUE:Ljava/lang/String; = "value"

.field public static final NOTIFICATION_ID:I = 0x1001

.field private static final PREFIX:Ljava/lang/String; = "com.samsung.android.allshare.service.fileshare"

.field public static final SUBSCRIPTION_MESSAGE:Ljava/lang/String; = "com.samsung.android.allshare.service.fileshare.SUBSCRIBE_SERVICE"

.field private static final VERSION_MAJOR:Ljava/lang/String; = "2"

.field private static final VERSION_MINOR:Ljava/lang/String; = "0"

.field private static final VERSION_PATCH:Ljava/lang/String; = "0"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getServiceVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "2.0.0"

    return-object v0
.end method
