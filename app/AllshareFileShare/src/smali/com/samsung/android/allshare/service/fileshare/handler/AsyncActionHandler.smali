.class public abstract Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;
.super Ljava/lang/Object;
.source "AsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/fileshare/handler/IAsyncActionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;
    }
.end annotation


# static fields
.field protected static final DEFAULT_WAIT_TIME:J = 0x1f4L

.field public static final GETINFO:Ljava/lang/String; = "GetMediaInfo"

.field public static final GETMUTE:Ljava/lang/String; = "GetMute"

.field public static final GETPOSITION:Ljava/lang/String; = "GetPlayPosition"

.field public static final GETVOL:Ljava/lang/String; = "GetVolume"

.field public static final NEXT:Ljava/lang/String; = "Next"

.field public static final PAUSE:Ljava/lang/String; = "Pause"

.field public static final PLAY:Ljava/lang/String; = "Play"

.field public static final PREVIOUS:Ljava/lang/String; = "Previous"

.field public static final RELTIME:Ljava/lang/String; = "REL_TIME"

.field public static final RESUME:Ljava/lang/String; = "Resume"

.field public static final SEEK:Ljava/lang/String; = "Seek"

.field public static final SETMUTE:Ljava/lang/String; = "SetMute"

.field public static final SETVOL:Ljava/lang/String; = "SetVolume"

.field public static final STOP:Ljava/lang/String; = "Stop"

.field public static excutor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mActionType:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHandlerID:Ljava/lang/String;

.field protected mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

.field protected final mTAG:Ljava/lang/String;

.field protected final mWorkerHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 2
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mTAG:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mContext:Landroid/content/Context;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionType:Ljava/lang/String;

    .line 86
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    .line 255
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$1;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mWorkerHandler:Landroid/os/Handler;

    .line 93
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mContext:Landroid/content/Context;

    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 97
    return-void
.end method

.method private registerHandler()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->registerActionHandler(Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;)Z

    .line 239
    :cond_0
    return-void
.end method

.method private unregisterHandler()V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->unregisterActionHandler(Lcom/samsung/android/allshare/service/fileshare/handler/IActionHandler;)V

    .line 250
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelActionRequest(J)V
    .locals 1
    .param p1, "req_id"    # J

    .prologue
    .line 218
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->cancelRequest(J)V

    .line 220
    return-void
.end method

.method protected abstract cancelRequest(J)V
.end method

.method protected abstract createActionWorker()Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler$ActionWorker;
.end method

.method protected abstract defineHandler()V
.end method

.method public finiActionHandler()V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->finiHandler()V

    .line 154
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->unregisterHandler()V

    .line 156
    return-void
.end method

.method protected abstract finiHandler()V
.end method

.method public getActionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getActionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionType:Ljava/lang/String;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getHandlerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    return-object v0
.end method

.method protected getManager()Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    return-object v0
.end method

.method public initActionHandler()V
    .locals 1

    .prologue
    .line 135
    const-string v0, "ACTION_TYPE_ASYNC"

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionType:Ljava/lang/String;

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->defineHandler()V

    .line 139
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->initHandler()V

    .line 140
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->registerHandler()V

    .line 142
    return-void
.end method

.method protected abstract initHandler()V
.end method

.method public responseAsyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 201
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mWorkerHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 202
    .local v1, "m":Landroid/os/Message;
    if-eqz v1, :cond_0

    .line 203
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 204
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "REQUEST_MESSAGE_DATA"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 205
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 206
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mWorkerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 208
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method protected setActionID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_0
    return-void
.end method

.method protected setHandlerID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 110
    return-void
.end method
