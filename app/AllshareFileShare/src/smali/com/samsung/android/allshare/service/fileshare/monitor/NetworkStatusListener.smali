.class public Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;
.super Ljava/lang/Object;
.source "NetworkStatusListener.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/fileshare/monitor/ISystemEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;
    }
.end annotation


# static fields
.field private static final MAX_SUBMASK_LENGTH:S = 0x20s

.field private static final TAG:Ljava/lang/String; = "NetworkStatusListener"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    return-void
.end method

.method private getNetworkInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;
    .locals 12
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 77
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    .line 78
    .local v1, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 79
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 80
    .local v5, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 84
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    .line 85
    .local v2, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 86
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/InetAddress;

    .line 87
    .local v4, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v4}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v8

    if-nez v8, :cond_1

    .line 92
    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    .line 93
    .local v6, "ip":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, ":"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "%"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 98
    invoke-direct {p0, v6, v5}, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;->getSubnetMask(Ljava/lang/String;Ljava/net/NetworkInterface;)Ljava/lang/String;

    move-result-object v7

    .line 100
    .local v7, "subnetMask":Ljava/lang/String;
    new-instance v8, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;

    invoke-direct {v8, p0, v6, v7}, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;-><init>(Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 108
    .end local v1    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v2    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v4    # "inetAddress":Ljava/net/InetAddress;
    .end local v5    # "intf":Ljava/net/NetworkInterface;
    .end local v6    # "ip":Ljava/lang/String;
    .end local v7    # "subnetMask":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 103
    :catch_0
    move-exception v3

    .line 104
    .local v3, "ex":Ljava/net/SocketException;
    const-string v8, "NetworkStatusListener"

    const-string v9, "getNetworkInfo"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SocketException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .end local v3    # "ex":Ljava/net/SocketException;
    :cond_2
    :goto_1
    const/4 v8, 0x0

    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "NetworkStatusListener"

    const-string v9, "getNetworkInfo"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getSubnetMask(Ljava/lang/String;Ljava/net/NetworkInterface;)Ljava/lang/String;
    .locals 12
    .param p1, "ip"    # Ljava/lang/String;
    .param p2, "networkInterface"    # Ljava/net/NetworkInterface;

    .prologue
    const/4 v7, 0x0

    .line 112
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 113
    :cond_0
    const-string v8, "NetworkStatusListener"

    const-string v9, "getSubnetMask"

    const-string v10, "the parameter is null !"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :goto_0
    return-object v7

    .line 118
    :cond_1
    const/4 v5, 0x0

    .line 119
    .local v5, "networkPrefixLength":I
    invoke-virtual {p2}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v4

    .line 121
    .local v4, "intfAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InterfaceAddress;

    .line 122
    .local v3, "interAddr":Ljava/net/InterfaceAddress;
    invoke-virtual {v3}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 123
    .local v2, "inetAddr":Ljava/net/InetAddress;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 124
    invoke-virtual {v3}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    move-result v5

    .line 129
    .end local v2    # "inetAddr":Ljava/net/InetAddress;
    .end local v3    # "interAddr":Ljava/net/InterfaceAddress;
    :cond_3
    const/16 v8, 0x20

    if-lt v5, v8, :cond_4

    .line 130
    const-string v8, "NetworkStatusListener"

    const-string v9, "getSubnetMask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid subnet mask length: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_4
    const/high16 v6, -0x80000000

    .line 138
    .local v6, "shiftby":I
    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_5

    .line 142
    shr-int/lit8 v6, v6, 0x1

    .line 138
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 147
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v8, v6, 0x18

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    shr-int/lit8 v8, v6, 0x10

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    shr-int/lit8 v8, v6, 0x8

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    and-int/lit16 v8, v6, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0
.end method

.method private notifyNICAdded(Ljava/lang/String;)V
    .locals 6
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 49
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NIC: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;->getNetworkInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;

    move-result-object v0

    .line 52
    .local v0, "info":Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;->ip:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;->subnetMask:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 53
    :cond_0
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    const-string v4, "The network info is null !"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_1
    :goto_0
    return-void

    .line 57
    :cond_2
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;->ip:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener$NetworkInfo;->subnetMask:Ljava/lang/String;

    invoke-virtual {v2, p1, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 59
    .local v1, "result":I
    if-eqz v1, :cond_1

    .line 60
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify to core failed, return value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private notifyNICRemoved(Ljava/lang/String;)V
    .locals 5
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 65
    const-string v1, "NetworkStatusListener"

    const-string v2, "notifyNICRemoved"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NIC: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/AllShareFrameworkCoreWrapper;->notifyNICRemoved(Ljava/lang/String;)I

    move-result v0

    .line 68
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 69
    const-string v1, "NetworkStatusListener"

    const-string v2, "notifyNICRemoved"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notify to core failed, return value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method


# virtual methods
.method public eventNotifyReceived(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 26
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 27
    :cond_0
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "the parameter is null !"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_1
    :goto_0
    return-void

    .line 31
    :cond_2
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "nic":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 33
    :cond_3
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid NIC name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :cond_4
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 39
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;->notifyNICAdded(Ljava/lang/String;)V

    .line 40
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "AllShareEvent.EVENT_WIFI_P2P_CONNECTED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_5
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/NetworkStatusListener;->notifyNICRemoved(Ljava/lang/String;)V

    .line 43
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "AllShareEvent.EVENT_WIFI_P2P_DISCONNECTED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
