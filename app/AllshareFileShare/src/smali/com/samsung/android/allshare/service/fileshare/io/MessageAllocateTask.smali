.class public Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;
.super Ljava/lang/Thread;
.source "MessageAllocateTask.java"


# static fields
.field private static final THREAD_SLEEP_TIME:I = 0x3e8

.field private static final mActionFINI:Ljava/lang/String; = "com.sec.android.allshare.framework.io.ACTION_FINI"


# instance fields
.field private mQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/sec/android/allshare/iface/CVMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

.field private mStopFlag:Z

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    .line 32
    iput-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 35
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mStopFlag:Z

    .line 39
    iput-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 45
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 47
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 48
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mStopFlag:Z

    .line 50
    return-void
.end method

.method private handleAllocateMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 94
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getMsgType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 103
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.allshare.framework.io.ACTION_FINI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v1, "handleAllocateMessage"

    const-string v2, "Received finalize message..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :goto_0
    const/4 v0, 0x0

    .line 112
    :goto_1
    return v0

    .line 96
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v1, "handleAllocateMessage"

    const-string v2, "Oops~~. Undefined Context Variable type..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 100
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->handleRequestMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z

    goto :goto_2

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v1, "handleAllocateMessage"

    const-string v2, "Oops~~. Invalid Context Variable type..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleRequestMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 6
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "action_id":Ljava/lang/String;
    const/4 v1, 0x0

    .line 72
    .local v1, "handler":Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    if-eqz v2, :cond_0

    .line 73
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getAsyncActionHandlerMap()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;
    check-cast v1, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;

    .line 75
    .restart local v1    # "handler":Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;
    :cond_0
    if-nez v1, :cond_1

    .line 76
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v3, "handleRequestMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Oops~~! No such action handler... ==> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const/4 v2, 0x0

    .line 83
    :goto_0
    return v2

    .line 81
    :cond_1
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/AsyncActionHandler;->responseAsyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 83
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized putQ(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 4
    .param p1, "msg"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    const/4 v1, 0x1

    :goto_0
    monitor-exit p0

    return v1

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v2, "putQ"

    const-string v3, "putQ InterruptedException "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    const/4 v1, 0x0

    goto :goto_0

    .line 146
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public run()V
    .locals 6

    .prologue
    .line 166
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v4, 0x3e8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/allshare/iface/CVMessage;

    .line 168
    .local v1, "msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v1, :cond_0

    .line 171
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->handleAllocateMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    .end local v1    # "msg":Lcom/sec/android/allshare/iface/CVMessage;
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mStopFlag:Z

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 184
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v3, "run"

    const-string v4, "run InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public setStopFlag()V
    .locals 4

    .prologue
    .line 129
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v2, "setStopFlag"

    const-string v3, "SetStopFlag"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->mStopFlag:Z

    .line 131
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 132
    .local v0, "fin":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v1, "com.sec.android.allshare.framework.io.ACTION_FINI"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/io/MessageAllocateTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)Z

    .line 135
    return-void
.end method
