.class Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;
.super Landroid/content/BroadcastReceiver;
.source "SystemEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 235
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 238
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive"

    const-string v3, "intent.getAction() == null!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->checkP2PState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$200(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    .line 245
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBroadcastReceiver"

    const-string v3, "WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_2
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 251
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->getAirplaneModeState(Landroid/content/Context;)I
    invoke-static {v2, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$300(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Landroid/content/Context;)I

    move-result v2

    # invokes: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->checkAirplaneModeState(I)V
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$400(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;I)V

    .line 252
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBroadcastReceiver"

    const-string v3, "Intent.ACTION_AIRPLANE_MODE_CHANGED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_3
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->checkScreenState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$500(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    .line 259
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBroadcastReceiver"

    const-string v3, "SCREEN ON or SCREEN OFF or USER_PRESENT"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
