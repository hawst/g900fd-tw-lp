.class public Lcom/samsung/android/allshare/service/fileshare/utility/TimeUtil;
.super Ljava/lang/Object;
.source "TimeUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TimeUtil"

.field public static TIMEOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const v0, 0x88b8

    sput v0, Lcom/samsung/android/allshare/service/fileshare/utility/TimeUtil;->TIMEOUT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertTimeStringToInteger(Ljava/lang/String;)J
    .locals 10
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 97
    if-nez p0, :cond_0

    .line 98
    const-wide/16 v8, -0x1

    .line 115
    :goto_0
    return-wide v8

    .line 100
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v8, 0x5

    if-ne v5, v8, :cond_2

    .line 101
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "00:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 105
    :cond_1
    :goto_1
    const-string v5, ":"

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 106
    .local v1, "indexhrs":I
    const-string v5, ":"

    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 108
    .local v2, "indexmins":I
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "hrs":Ljava/lang/String;
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "mins":Ljava/lang/String;
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {p0, v5, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 112
    .local v4, "secs":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/lit8 v5, v5, 0x3c

    mul-int/lit8 v5, v5, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3c

    add-int/2addr v5, v8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/2addr v5, v8

    int-to-long v6, v5

    .line 115
    .local v6, "timeInSecs":J
    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v6

    goto :goto_0

    .line 102
    .end local v0    # "hrs":Ljava/lang/String;
    .end local v1    # "indexhrs":I
    .end local v2    # "indexmins":I
    .end local v3    # "mins":Ljava/lang/String;
    .end local v4    # "secs":Ljava/lang/String;
    .end local v6    # "timeInSecs":J
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v8, 0x4

    if-ne v5, v8, :cond_1

    .line 103
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "00:0"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public static getTimeLong(Ljava/lang/String;)J
    .locals 13
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    const/16 v12, 0x3c

    const/4 v11, 0x2

    const/4 v10, 0x1

    const-wide/16 v8, -0x1

    .line 62
    if-eqz p0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 64
    const-string v7, "."

    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 65
    .local v2, "index":I
    const/4 v7, -0x1

    if-eq v2, v7, :cond_0

    .line 66
    const/4 v7, 0x0

    invoke-virtual {p0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 68
    :cond_0
    const-string v7, ":"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "timeArray":[Ljava/lang/String;
    array-length v3, v6

    .line 70
    .local v3, "length":I
    const/4 v1, 0x0

    .line 71
    .local v1, "hour":I
    const/4 v4, 0x0

    .line 72
    .local v4, "minute":I
    const/4 v5, 0x0

    .line 73
    .local v5, "second":I
    if-le v3, v11, :cond_2

    .line 74
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 75
    const/4 v7, 0x1

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 76
    const/4 v7, 0x2

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 83
    :goto_0
    if-ltz v1, :cond_1

    if-ltz v4, :cond_1

    if-gt v4, v12, :cond_1

    if-ltz v5, :cond_1

    if-le v5, v12, :cond_3

    .line 92
    .end local v1    # "hour":I
    .end local v2    # "index":I
    .end local v3    # "length":I
    .end local v4    # "minute":I
    .end local v5    # "second":I
    .end local v6    # "timeArray":[Ljava/lang/String;
    :cond_1
    :goto_1
    return-wide v8

    .line 77
    .restart local v1    # "hour":I
    .restart local v2    # "index":I
    .restart local v3    # "length":I
    .restart local v4    # "minute":I
    .restart local v5    # "second":I
    .restart local v6    # "timeArray":[Ljava/lang/String;
    :cond_2
    if-le v3, v10, :cond_1

    .line 78
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 79
    const/4 v7, 0x1

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    .line 86
    :cond_3
    mul-int/lit8 v7, v1, 0x3c

    mul-int/lit8 v7, v7, 0x3c

    mul-int/lit8 v8, v4, 0x3c

    add-int/2addr v7, v8

    add-int/2addr v7, v5

    int-to-long v8, v7

    goto :goto_1

    .line 90
    .end local v1    # "hour":I
    .end local v2    # "index":I
    .end local v3    # "length":I
    .end local v4    # "minute":I
    .end local v5    # "second":I
    .end local v6    # "timeArray":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v7, "TimeUtil"

    const-string v10, "getTimeLong"

    const-string v11, "formatTime NumberFormatException"

    invoke-static {v7, v10, v11, v0}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_stack(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public static getTimeString(J)Ljava/lang/String;
    .locals 12
    .param p0, "millisecond"    # J

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x3c

    .line 26
    cmp-long v6, p0, v10

    if-lez v6, :cond_1

    .line 27
    const-wide/16 v4, 0x0

    .local v4, "ss":J
    const-wide/16 v2, 0x0

    .local v2, "mm":J
    const-wide/16 v0, 0x0

    .line 37
    .local v0, "hh":J
    const-wide/16 v6, 0x3e8

    div-long v4, p0, v6

    .line 38
    cmp-long v6, v4, v10

    if-nez v6, :cond_0

    .line 39
    const-wide/16 v2, 0x0

    .line 40
    const-wide/16 v0, 0x0

    .line 44
    :goto_0
    rem-long/2addr v4, v8

    .line 45
    div-long v0, v2, v8

    .line 46
    rem-long/2addr v2, v8

    .line 49
    const-string v6, "%02d:%02d:%02d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 51
    .end local v0    # "hh":J
    .end local v2    # "mm":J
    .end local v4    # "ss":J
    :goto_1
    return-object v6

    .line 42
    .restart local v0    # "hh":J
    .restart local v2    # "mm":J
    .restart local v4    # "ss":J
    :cond_0
    div-long v2, v4, v8

    goto :goto_0

    .line 51
    .end local v0    # "hh":J
    .end local v2    # "mm":J
    .end local v4    # "ss":J
    :cond_1
    const-string v6, "00:00:00"

    goto :goto_1
.end method
