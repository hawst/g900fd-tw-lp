.class Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;
.super Ljava/lang/Object;
.source "FileShareActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiveFileListActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V

    return-void
.end method


# virtual methods
.method public handleAction(Lcom/sec/android/allshare/iface/CVMessage;)Landroid/os/Bundle;
    .locals 18
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 222
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 223
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v14, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 224
    .local v13, "udn":Ljava/lang/String;
    const-string v14, "BUNDLE_STRING_NAME"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 226
    .local v8, "name":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v7, "mSessionId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v14, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 230
    .local v12, "time":Ljava/lang/String;
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 231
    :cond_0
    const-string v14, "FileShareActionHandler"

    const-string v15, "[handleAction]: "

    const-string v16, "ReceiveFileListActionHandler>>>time == null || time.isEmpty!.."

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v14, "BUNDLE_ENUM_ERROR"

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :goto_0
    return-object v1

    .line 237
    :cond_1
    const-string v14, "BUNDLE_STRING_ARRAYLIST_FILE_PATH"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 240
    .local v4, "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 241
    :cond_2
    const-string v14, "FileShareActionHandler"

    const-string v15, "[handleAction]: "

    const-string v16, "ReceiveFileListActionHandler>>>filePathList == null || filePathList.isEmpty()."

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v14, "BUNDLE_ENUM_ERROR"

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 249
    .local v3, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 250
    .local v9, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v14

    if-nez v14, :cond_5

    .line 254
    :cond_4
    const-string v14, "FileShareActionHandler"

    const-string v15, "[handleAction]: "

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ReceiveFileListActionHandler>>>file["

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "] is not exist or cannot read "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v14, "BUNDLE_ENUM_ERROR"

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :cond_5
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 263
    .end local v2    # "file":Ljava/io/File;
    .end local v9    # "path":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    move-result-object v14

    invoke-virtual {v14, v13, v8, v3, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->transfer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v10

    .line 264
    .local v10, "result":I
    if-nez v10, :cond_8

    .line 265
    const-string v14, "BUNDLE_ENUM_ERROR"

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const/4 v5, 0x0

    .line 267
    .local v5, "firstSessionID":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_7

    .line 268
    const/4 v14, 0x0

    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "firstSessionID":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 270
    .restart local v5    # "firstSessionID":Ljava/lang/String;
    :cond_7
    new-instance v11, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    const/4 v15, 0x0

    invoke-direct {v11, v14, v15}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V

    .line 271
    .local v11, "session":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    iput-object v13, v11, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->deviceUDN:Ljava/lang/String;

    .line 272
    iput-object v3, v11, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferFiles:Ljava/util/ArrayList;

    .line 273
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$ReceiveFileListActionHandler;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mSessionMap:Ljava/util/HashMap;
    invoke-static {v14}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->access$900(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/util/HashMap;

    move-result-object v14

    invoke-virtual {v14, v5, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    const-string v14, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v1, v14, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    .end local v5    # "firstSessionID":Ljava/lang/String;
    .end local v11    # "session":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    :goto_2
    const-string v14, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {v1, v14, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 277
    :cond_8
    const-string v14, "FileShareActionHandler"

    const-string v15, "[handleAction]: "

    const-string v16, "ReceiveFileListActionHandler.handleAction UPnPException Fail to request transfer"

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v14, "BUNDLE_ENUM_ERROR"

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
