.class public Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;
.super Ljava/lang/Object;
.source "FileShareDeviceTransportInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public Id:I

.field public downloadLength:J

.field public length:J

.field public name:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;

.field public sourceURL:Ljava/lang/String;

.field public targetURL:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->name:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->type:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->sourceURL:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->targetURL:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->length:J

    .line 65
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public equalsObj(Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;)Z
    .locals 4
    .param p1, "tempInfo"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->sourceURL:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->sourceURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->targetURL:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->targetURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->type:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->length:J

    iget-wide v2, p1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->length:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKeyValue()I
    .locals 3

    .prologue
    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->Id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public setId(I)V
    .locals 0
    .param p1, "Id"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->Id:I

    .line 41
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->sourceURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->targetURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-wide v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareDeviceTransportInfo;->length:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 74
    return-void
.end method
