.class Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
.super Ljava/lang/Object;
.source "FileShareActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionInfo"
.end annotation


# instance fields
.field deviceUDN:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

.field transferFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field transferredNum:I


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V
    .locals 1

    .prologue
    .line 320
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 325
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$1;

    .prologue
    .line 320
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;-><init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 320
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private notifyEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 328
    iget-object v3, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 330
    .local v1, "size":I
    iget v3, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    if-lt v3, v1, :cond_0

    .line 331
    const-string v3, "FileShareActionHandler"

    const-string v4, "[notifyEvent]: "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "transferredNum >= size! transferredNum="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    .line 335
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferFiles:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->transferredNum:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 337
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_1

    .line 338
    const-string v3, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$SessionInfo;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mServiceManagerRef:Ljava/lang/ref/WeakReference;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->access$1000(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/lang/ref/WeakReference;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 345
    .local v2, "sm":Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    if-nez v2, :cond_2

    .line 346
    const-string v3, "FileShareActionHandler"

    const-string v4, "[notifyEvent]:"

    const-string v5, "ServiceManager is null!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :goto_1
    return-void

    .line 340
    .end local v2    # "sm":Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    :cond_1
    const-string v3, "BUNDLE_STRING_FILE_PATH"

    const-string v4, ""

    invoke-virtual {p2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 349
    .restart local v2    # "sm":Lcom/samsung/android/allshare/service/fileshare/ServiceManager;
    :cond_2
    invoke-virtual {v2, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1
.end method
