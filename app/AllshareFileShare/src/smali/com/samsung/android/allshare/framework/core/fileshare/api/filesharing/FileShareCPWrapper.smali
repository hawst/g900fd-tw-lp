.class public Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
.super Ljava/lang/Object;
.source "FileShareCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPTransportStatusListener;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPResponseListener;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;,
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;
    }
.end annotation


# static fields
.field private static final ACTIONHANDLER_WAIT_TIME:I = 0x7530

.field private static final EVENT_FLAG:Ljava/lang/String; = "fileShareEvent"

.field public static final FILE_FOLDER_TYPE:I = 0x0

.field public static final FILE_ITEM_TYPE:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "FileShareCPWrapper"

.field private static final RES_FLAG:Ljava/lang/String; = "fileShareResponse"

.field private static final SUB_RES_FLAG:Ljava/lang/String; = "fileShareSubscribeResponse"

.field public static instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;


# instance fields
.field private eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

.field public mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mEventListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNoMatchEvenList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;",
            ">;"
        }
    .end annotation
.end field

.field public mResponseListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPResponseListener;",
            ">;"
        }
    .end annotation
.end field

.field public mSessionInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mTransportStatusListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPTransportStatusListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mResponseListener:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mTransportStatusListener:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 51
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 54
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mNoMatchEvenList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    .line 876
    return-void
.end method

.method private closeSession(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 723
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 724
    :cond_0
    const-string v2, "FileShareCPWrapper"

    const-string v3, "closeSession"

    const-string v4, "CloseSession with null input parameters!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    const/4 v1, -0x1

    .line 736
    :cond_1
    :goto_0
    return v1

    .line 728
    :cond_2
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;-><init>()V

    .line 729
    .local v0, "requestInfo":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    invoke-static {p1, p2, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->closeSession(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I

    move-result v1

    .line 730
    .local v1, "result":I
    if-eqz v1, :cond_1

    .line 731
    const-string v2, "FileShareCPWrapper"

    const-string v3, "closeSession"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " close session failed! deviceUdn = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteSessionInfo(Ljava/lang/String;)V
    .locals 4
    .param p1, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 764
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 765
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .line 766
    .local v1, "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->sessionID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 767
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 768
    monitor-exit v3

    .line 772
    .end local v1    # "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :goto_0
    return-void

    .line 771
    :cond_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getFileSize(Ljava/io/File;)J
    .locals 10
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 740
    const-wide/16 v2, 0x0

    .line 741
    .local v2, "size":J
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 742
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 743
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 745
    .local v0, "fileArray":[Ljava/io/File;
    if-nez v0, :cond_0

    move-wide v4, v2

    .line 760
    .end local v0    # "fileArray":[Ljava/io/File;
    .end local v2    # "size":J
    .local v4, "size":J
    :goto_0
    return-wide v4

    .line 749
    .end local v4    # "size":J
    .restart local v0    # "fileArray":[Ljava/io/File;
    .restart local v2    # "size":J
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_2

    .line 750
    aget-object v6, v0, v1

    invoke-direct {p0, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getFileSize(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 749
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 753
    .end local v0    # "fileArray":[Ljava/io/File;
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v2, v6

    :cond_2
    :goto_2
    move-wide v4, v2

    .line 760
    .end local v2    # "size":J
    .restart local v4    # "size":J
    goto :goto_0

    .line 756
    .end local v4    # "size":J
    .restart local v2    # "size":J
    :cond_3
    const-string v6, "FileShareCPWrapper"

    const-string v7, "getFileSize"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File[ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] is not exist!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
    .locals 2

    .prologue
    .line 168
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    if-nez v0, :cond_1

    .line 169
    const-class v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    monitor-enter v1

    .line 170
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    .line 173
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    return-object v0

    .line 173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private notifyTransportStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "transportID"    # Ljava/lang/String;
    .param p3, "transportStatus"    # Ljava/lang/String;

    .prologue
    .line 788
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 789
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;->onTransportStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 791
    :cond_0
    return-void
.end method

.method private notifyTransportingInfo(Ljava/lang/String;Ljava/lang/String;JJI)V
    .locals 9
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "transportID"    # Ljava/lang/String;
    .param p3, "totalSize"    # J
    .param p5, "sendSize"    # J
    .param p7, "status"    # I

    .prologue
    .line 795
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 796
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-interface/range {v1 .. v8}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;->onTransportingInfo(Ljava/lang/String;Ljava/lang/String;JJI)V

    .line 795
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 799
    :cond_0
    return-void
.end method

.method private querySessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    .locals 5
    .param p1, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 775
    const/4 v2, 0x0

    .line 776
    .local v2, "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    iget-object v4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 777
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .line 778
    .local v1, "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->sessionID:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 779
    move-object v2, v1

    .line 783
    .end local v1    # "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_1
    monitor-exit v4

    .line 784
    return-object v2

    .line 783
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private transferSingleFile(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)I
    .locals 8
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "session"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .prologue
    const/4 v3, -0x1

    .line 680
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v4, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->itemsDescriptionList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->itemsDescriptionList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 682
    :cond_0
    const-string v4, "FileShareCPWrapper"

    const-string v5, "transferSingleFile"

    const-string v6, "transferSingleFile with null input parameters!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    :goto_0
    return v3

    .line 687
    :cond_1
    iget-object v4, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->itemsDescriptionList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentTransportIndex:I

    if-gt v4, v5, :cond_2

    .line 688
    const-string v4, "FileShareCPWrapper"

    const-string v5, "transferSingleFile"

    const-string v6, "transferSingleFile with out of index!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 692
    :cond_2
    iget-object v4, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->itemsDescriptionList:Ljava/util/ArrayList;

    iget v5, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentTransportIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;

    .line 694
    .local v1, "itemDescription":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;
    if-nez v1, :cond_3

    .line 695
    const-string v4, "FileShareCPWrapper"

    const-string v5, "transferSingleFile"

    const-string v6, "get itemDescription from session info failed!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 700
    :cond_3
    new-instance v2, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;-><init>()V

    .line 702
    .local v2, "requestInfo":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    const-string v4, "TransportItems"

    invoke-direct {v0, p0, v2, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;Ljava/lang/String;)V

    .line 704
    .local v0, "actionHandler":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    iget-object v4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    iget-object v4, p2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->sessionID:Ljava/lang/String;

    invoke-static {p1, v4, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->transferItem(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I

    move-result v3

    .line 709
    .local v3, "result":I
    if-nez v3, :cond_4

    .line 710
    const-string v4, "FileShareCPWrapper"

    const-string v5, "transferSingleFile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " transfer single file successfully! requestID = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;->requestID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 713
    :cond_4
    const-string v4, "FileShareCPWrapper"

    const-string v5, "transferSingleFile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " transfer single file failed! deviceUdn = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", itemDescription = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    iget-object v4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public addEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)V
    .locals 6
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "eventItem"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    .param p4, "mCurSessInfo"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    .prologue
    const/4 v5, 0x1

    .line 504
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "FINISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 505
    const-string v2, "FileShareCPWrapper"

    const-string v3, "addEvent"

    const-string v4, "Handle Transport finished event when RFT is received!!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    const/4 v1, 0x0

    .line 512
    .local v1, "need_to_finish":Z
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "READY_FOR_TRANSPORT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p4, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isFirstRFT:Z

    if-ne v2, v5, :cond_2

    .line 514
    const/4 v1, 0x1

    .line 517
    :cond_2
    if-ne v1, v5, :cond_3

    .line 518
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;-><init>()V

    .line 519
    .local v0, "finishItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    const-string v2, "TransportStatus"

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->name:Ljava/lang/String;

    .line 520
    const-string v2, "FINISHED"

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    .line 522
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-virtual {v2, p1, p2, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->addEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V

    .line 524
    const-string v2, "FileShareCPWrapper"

    const-string v3, "addEvent"

    const-string v4, "Send finished event to App side first"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    .end local v0    # "finishItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->addEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V

    .line 529
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "READY_FOR_TRANSPORT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p4, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isFirstRFT:Z

    if-nez v2, :cond_0

    .line 531
    iput-boolean v5, p4, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isFirstRFT:Z

    goto :goto_0
.end method

.method public addEventListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    return-void
.end method

.method public addResponseListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPResponseListener;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mResponseListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    return-void
.end method

.method public addTransportStatusListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPTransportStatusListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPTransportStatusListener;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mTransportStatusListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    return-void
.end method

.method public cancel(Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p1, "deviceUdn"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 420
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 421
    :try_start_0
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->querySessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v3

    .line 422
    .local v3, "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-eqz v3, :cond_0

    .line 423
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isCancelled:Z

    .line 425
    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    new-instance v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    invoke-direct {v1}, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;-><init>()V

    .line 430
    .local v1, "requestID":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    const-string v4, "CancelTransport"

    invoke-direct {v0, p0, v1, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;Ljava/lang/String;)V

    .line 432
    .local v0, "actionHandler":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    const/4 v4, -0x1

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->setErrorCode(I)V

    .line 433
    iget-object v4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    invoke-static {p1, p2, v1}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->cancel(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I

    move-result v2

    .line 437
    .local v2, "result":I
    if-nez v2, :cond_1

    .line 438
    const-string v4, "FileShareCPWrapper"

    const-string v5, "cancel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " cancel successfully! transportID = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :goto_0
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->deleteSessionInfo(Ljava/lang/String;)V

    .line 449
    return v2

    .line 425
    .end local v0    # "actionHandler":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    .end local v1    # "requestID":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    .end local v2    # "result":I
    .end local v3    # "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 441
    .restart local v0    # "actionHandler":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    .restart local v1    # "requestID":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    .restart local v2    # "result":I
    .restart local v3    # "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_1
    const-string v4, "FileShareCPWrapper"

    const-string v5, "cancel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " cancel failed! deviceUdn = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v4, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public handleEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V
    .locals 6
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "item"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    .prologue
    .line 537
    if-eqz p3, :cond_0

    const-string v2, "TransportStatus"

    iget-object v3, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->notifyTransportStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "READY_FOR_TRANSPORT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 542
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->querySessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v1

    .line 543
    .local v1, "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-nez v1, :cond_1

    .line 544
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "tempSession is null in STATUS_READY_FOR_TRANSPORT event!!! "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    .end local v1    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_0
    :goto_0
    return-void

    .line 549
    .restart local v1    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_1
    iget-boolean v2, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isCancelled:Z

    if-eqz v2, :cond_2

    .line 550
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "This session has been cancelled already!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 561
    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->transferSingleFile(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)I

    .line 563
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "Success to process STATUS_READY_FOR_TRANSPORT event!!! "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 566
    .end local v1    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_3
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "TRANSPORTING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 567
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "Success to process STATUS_TRANSPORTING event!!! "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 569
    :cond_4
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "FINISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "CANCELED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 571
    :cond_5
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->querySessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v1

    .line 572
    .restart local v1    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-nez v1, :cond_6

    .line 573
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tempSession == null for session "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 578
    :cond_6
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "FINISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-boolean v2, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isSupportTransportItems:Z

    if-nez v2, :cond_7

    iget v2, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentTransportIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentTransportIndex:I

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->itemsDescriptionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 595
    :goto_1
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Success to process "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " event!!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 586
    :cond_7
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->closeSession(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 587
    .local v0, "result":I
    if-eqz v0, :cond_8

    .line 588
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "Calling private function::closeSession failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 592
    :cond_8
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->deleteSessionInfo(Ljava/lang/String;)V

    goto :goto_1

    .line 597
    .end local v0    # "result":I
    .end local v1    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_9
    iget-object v2, p3, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    const-string v3, "ERROR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 598
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->closeSession(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 599
    .restart local v0    # "result":I
    if-eqz v0, :cond_a

    .line 600
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "Calling private function::closeSession failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 605
    :cond_a
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->deleteSessionInfo(Ljava/lang/String;)V

    .line 606
    const-string v2, "FileShareCPWrapper"

    const-string v3, "handleEvent"

    const-string v4, "Success to process ERROR event!!! "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public handleResponse(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "sessionID"    # Ljava/lang/String;
    .param p4, "errorCode"    # I

    .prologue
    .line 619
    const/4 v1, 0x0

    .line 620
    .local v1, "bActionExist":Z
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    .line 621
    .local v0, "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->requestInfo:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$300(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    move-result-object v5

    iget v5, v5, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;->requestID:I

    if-ne v5, p1, :cond_0

    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->actionName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$400(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 622
    const/4 v1, 0x1

    .line 628
    .end local v0    # "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    :cond_1
    if-nez v1, :cond_2

    .line 630
    const-wide/16 v6, 0x32

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    :cond_2
    :goto_0
    const-string v5, "CreateSession"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 637
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    monitor-enter v6

    .line 638
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    .line 639
    .restart local v0    # "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->requestInfo:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$300(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    move-result-object v5

    iget v5, v5, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;->requestID:I

    if-ne v5, p1, :cond_3

    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->actionName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$400(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 641
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v5

    iput-object p3, v5, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->sessionID:Ljava/lang/String;

    .line 642
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->setErrorCode(I)V

    .line 646
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mNoMatchEvenList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    .line 647
    .local v4, "item":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    iget-object v5, v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    invoke-virtual {v5, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 648
    const-string v5, "FileShareCPWrapper"

    const-string v7, "handleResponse"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "add mismatch event,sessionID:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v7, v8}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->deviceUDN:Ljava/lang/String;

    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v7

    invoke-virtual {p0, v5, p3, v4, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->addEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)V

    .line 653
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mNoMatchEvenList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 662
    .end local v0    # "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    .end local v4    # "item":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 631
    :catch_0
    move-exception v2

    .line 632
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 656
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    :cond_5
    :try_start_2
    monitor-exit v6

    .line 671
    .end local v0    # "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    :cond_6
    :goto_2
    return-void

    .line 659
    :cond_7
    const-string v5, "FileShareCPWrapper"

    const-string v7, "handleResponse"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cannot find request id for createSession:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v7, v8}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 665
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    .line 666
    .restart local v0    # "action":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->requestInfo:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$300(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    move-result-object v5

    iget v5, v5, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;->requestID:I

    if-ne v5, p1, :cond_9

    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->actionName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$400(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 667
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public handleSubscribeResponse(Ljava/lang/String;I)V
    .locals 6
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 464
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;

    .line 465
    .local v1, "subscribe":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->getDeviceUDN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 466
    invoke-virtual {v1, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->setErrorCode(I)V

    .line 467
    const-string v2, "FileShareCPWrapper"

    const-string v3, "onFileReceiverCPSubscribeResponseCallback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "subscribe is done with errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    .end local v1    # "subscribe":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;
    :cond_0
    return-void

    .line 471
    .restart local v1    # "subscribe":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;
    :cond_1
    const-string v2, "FileShareCPWrapper"

    const-string v3, "onFileReceiverCPSubscribeResponseCallback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot find udn for subscribe done : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileReceiverCPEventReceived(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 480
    .local p3, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;>;"
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->querySessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v1

    .line 482
    .local v1, "mCurSessInfo":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 484
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    .line 486
    .local v0, "lastItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    const-string v2, "FileShareCPWrapper"

    const-string v3, "onFileReceiverCPEventReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lastItem.value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    if-nez v1, :cond_1

    .line 491
    const-string v2, "FileShareCPWrapper"

    const-string v3, "onFileReceiverCPEventReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "There is no match sessionID with eventItem.sessionID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mNoMatchEvenList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    .end local v0    # "lastItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    :cond_0
    :goto_0
    return-void

    .line 498
    .restart local v0    # "lastItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;
    :cond_1
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->addEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)V

    goto :goto_0
.end method

.method public onFileReceiverCPResponseReceived(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "sessionID"    # Ljava/lang/String;
    .param p4, "errorCode"    # I

    .prologue
    .line 614
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->addResponse(ILjava/lang/String;Ljava/lang/String;I)V

    .line 615
    return-void
.end method

.method public onFileReceiverCPSubscribeResponseCallback(Ljava/lang/String;I)V
    .locals 4
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 453
    if-eqz p2, :cond_0

    .line 454
    const-string v0, "FileShareCPWrapper"

    const-string v1, "onFileReceiverCPSubscribeResponseCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscription is failed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->addSubscribeResponse(Ljava/lang/String;I)V

    .line 461
    return-void

    .line 457
    :cond_0
    const-string v0, "FileShareCPWrapper"

    const-string v1, "onFileReceiverCPSubscribeResponseCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscription is done for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileReceiverDeviceNotified(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 674
    const-string v0, "FileShareCPWrapper"

    const-string v1, "onFileReceiverDeviceNotified"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Success!!! state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", deviceUDN = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", deviceName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    return-void
.end method

.method public onTransportStatusReceived(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 16
    .param p1, "sessionID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 925
    .local p2, "itemsStatus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;>;"
    const-wide/16 v14, 0x0

    .line 926
    .local v14, "totalSize":J
    const-wide/16 v10, 0x0

    .line 927
    .local v10, "sentSize":J
    const/4 v8, 0x0

    .line 928
    .local v8, "status":I
    if-eqz p2, :cond_4

    .line 929
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;

    .line 930
    .local v9, "statusItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;
    iget-wide v2, v9, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;->itemTotalSize:J

    add-long/2addr v14, v2

    .line 931
    iget-wide v2, v9, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;->itemSentSize:J

    add-long/2addr v10, v2

    .line 932
    iget v8, v9, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;->status:I

    .line 933
    goto :goto_0

    .line 935
    .end local v9    # "statusItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareTransportStatus;
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->querySessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v12

    .line 936
    .local v12, "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    if-nez v12, :cond_2

    .line 937
    const-string v1, "FileShareCPWrapper"

    const-string v2, "onTransportStatusReceived"

    const-string v3, "tempSession == null in onTransportStatusReceived callback!!! "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v12    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_1
    :goto_1
    return-void

    .line 943
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v12    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_2
    iget-wide v2, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSentSize:J

    iget-wide v4, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentSentSize:J

    sub-long v4, v10, v4

    add-long/2addr v2, v4

    iput-wide v2, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSentSize:J

    .line 946
    iput-wide v10, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentSentSize:J

    .line 948
    const/4 v1, 0x3

    if-ne v8, v1, :cond_3

    .line 950
    const-wide/16 v2, 0x0

    iput-wide v2, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentSentSize:J

    .line 953
    :cond_3
    iget-object v2, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->deviceUDN:Ljava/lang/String;

    iget-wide v4, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSize:J

    iget-wide v6, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSentSize:J

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v8}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->notifyTransportingInfo(Ljava/lang/String;Ljava/lang/String;JJI)V

    .line 956
    const-string v1, "FileShareCPWrapper"

    const-string v2, "onTransportStatusReceived"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", totalSentSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSentSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", totalSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v12, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const/4 v1, 0x5

    if-ne v1, v8, :cond_1

    .line 961
    const-string v1, "FileShareCPWrapper"

    const-string v2, "onTransportStatusReceived"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", FTD disconnected!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 965
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v12    # "tempSession":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    :cond_4
    const-string v1, "FileShareCPWrapper"

    const-string v2, "onTransportStatusReceived"

    const-string v3, "itemsStatus is null."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public removeEventListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPEventListener;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mEventListener:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 201
    return-void
.end method

.method public removeResponseListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPResponseListener;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mResponseListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 209
    return-void
.end method

.method public removeTransportStatusListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPTransportStatusListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$IFileShareCPTransportStatusListener;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mTransportStatusListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method public start()I
    .locals 4

    .prologue
    .line 220
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->startFileReceiverCP()I

    move-result v0

    .line 221
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 222
    new-instance v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    .line 223
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->start()V

    .line 227
    :goto_0
    return v0

    .line 225
    :cond_0
    const-string v1, "FileShareCPWrapper"

    const-string v2, "start"

    const-string v3, "start failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stop()I
    .locals 5

    .prologue
    .line 231
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->stopFileReceiverCP()I

    move-result v1

    .line 233
    .local v1, "result":I
    if-nez v1, :cond_1

    .line 235
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSessionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 236
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 237
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 238
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mNoMatchEvenList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 243
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    if-eqz v2, :cond_0

    .line 244
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->setRunningStatus(Z)V

    .line 246
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->eventHandlerThread:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_0
    :goto_1
    return v1

    .line 240
    :cond_1
    const-string v2, "FileShareCPWrapper"

    const-string v3, "stop"

    const-string v4, "stop failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "FileShareCPWrapper"

    const-string v3, "stop"

    const-string v4, "eventHandlerThread join failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public transfer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 27
    .param p1, "deviceUdn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 257
    .local p3, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .local p4, "sessionID":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 258
    :cond_0
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "transfer with null input parameters!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const/16 v17, -0x1

    .line 414
    :goto_0
    return v17

    .line 262
    :cond_1
    new-instance v22, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Ljava/lang/String;)V

    .line 263
    .local v22, "subscribeInfo":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    const/4 v5, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->setHold(Z)V

    .line 266
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->subscribe(Ljava/lang/String;)I

    move-result v17

    .line 268
    .local v17, "result":I
    if-eqz v17, :cond_2

    const/16 v5, 0x3aa1

    move/from16 v0, v17

    if-eq v0, v5, :cond_2

    .line 269
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">>> subscribe failed! deviceUdn = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x5

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v26

    add-int/lit8 v26, v26, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 275
    :cond_2
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">>> subscribe successfully! UDN = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/16 v5, 0x3aa1

    move/from16 v0, v17

    if-eq v0, v5, :cond_3

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->isHeld()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 279
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "subscribeInfo wait is locked!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    monitor-enter v22

    .line 283
    const-wide/16 v6, 0x7530

    :try_start_0
    move-object/from16 v0, v22

    invoke-virtual {v0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    :goto_1
    :try_start_1
    monitor-exit v22
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    :cond_3
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SubscribeInfo;->getErrorCode()I

    move-result v17

    .line 292
    const/4 v5, -0x1

    move/from16 v0, v17

    if-ne v0, v5, :cond_5

    .line 293
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "subscribeInfo Wait Response time out!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 305
    new-instance v25, Ljava/lang/StringBuffer;

    const-string v5, "ASF FileShare"

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 308
    .local v25, "transportMsg":Ljava/lang/StringBuffer;
    new-instance v23, Ljava/lang/StringBuffer;

    const-string v5, ""

    move-object/from16 v0, v23

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 309
    .local v23, "transportDescription":Ljava/lang/StringBuffer;
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v24, "transportItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;>;"
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 312
    .local v9, "totalCount":I
    const-wide/16 v10, 0x0

    .line 313
    .local v10, "totalSize":J
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    .line 314
    .local v14, "file":Ljava/io/File;
    if-nez v14, :cond_6

    .line 315
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "get file is null from fileList"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/16 v17, -0x1

    goto/16 :goto_0

    .line 284
    .end local v9    # "totalCount":I
    .end local v10    # "totalSize":J
    .end local v14    # "file":Ljava/io/File;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v23    # "transportDescription":Ljava/lang/StringBuffer;
    .end local v24    # "transportItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;>;"
    .end local v25    # "transportMsg":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v13

    .line 285
    .local v13, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Subscribe Wait exception happened!!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v13}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 288
    .end local v13    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    monitor-exit v22
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 295
    :cond_5
    if-eqz v17, :cond_4

    .line 296
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">>> subscribe failed! deviceUdn = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " error code "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mSubscribeInfoList:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 299
    const/16 v17, -0x1

    goto/16 :goto_0

    .line 319
    .restart local v9    # "totalCount":I
    .restart local v10    # "totalSize":J
    .restart local v14    # "file":Ljava/io/File;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v23    # "transportDescription":Ljava/lang/StringBuffer;
    .restart local v24    # "transportItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;>;"
    .restart local v25    # "transportMsg":Ljava/lang/StringBuffer;
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getFileSize(Ljava/io/File;)J

    move-result-wide v20

    .line 320
    .local v20, "singleSize":J
    const-wide/16 v6, 0x0

    cmp-long v5, v20, v6

    if-gtz v5, :cond_7

    .line 321
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The file["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] size is 0, stop transport!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const/16 v17, -0x1

    goto/16 :goto_0

    .line 326
    :cond_7
    add-long v10, v10, v20

    .line 328
    new-instance v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;

    invoke-direct {v15}, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;-><init>()V

    .line 329
    .local v15, "fileDescrip":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    .line 331
    .local v18, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 332
    const/4 v5, 0x0

    iput v5, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->itemType:I

    .line 336
    :goto_3
    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->itemName:Ljava/lang/String;

    .line 337
    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->sourceURL:Ljava/lang/String;

    .line 338
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/ShareViaWifi/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 340
    iget-object v5, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->itemName:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 341
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->targetURL:Ljava/lang/String;

    .line 342
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->getFileSize(Ljava/io/File;)J

    move-result-wide v6

    iput-wide v6, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->fileLength:J

    .line 344
    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 334
    :cond_8
    const/4 v5, 0x1

    iput v5, v15, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->itemType:I

    goto :goto_3

    .line 347
    .end local v14    # "file":Ljava/io/File;
    .end local v15    # "fileDescrip":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;
    .end local v18    # "sb":Ljava/lang/StringBuffer;
    .end local v20    # "singleSize":J
    :cond_9
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 348
    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;

    iget-object v5, v5, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;->itemName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 351
    :cond_a
    new-instance v12, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;

    invoke-direct {v12}, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;-><init>()V

    .line 353
    .local v12, "requestInfo":Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;
    new-instance v19, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;)V

    .line 354
    .local v19, "session":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->deviceUDN:Ljava/lang/String;

    .line 355
    const/4 v5, 0x0

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->sessionID:Ljava/lang/String;

    .line 356
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->itemsDescriptionList:Ljava/util/ArrayList;

    .line 357
    const/4 v5, 0x0

    move-object/from16 v0, v19

    iput v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentTransportIndex:I

    .line 359
    move-object/from16 v0, v19

    iput-wide v10, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSize:J

    .line 360
    const-wide/16 v6, 0x0

    move-object/from16 v0, v19

    iput-wide v6, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->totalSentSize:J

    .line 361
    const-wide/16 v6, 0x0

    move-object/from16 v0, v19

    iput-wide v6, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->currentSentSize:J

    .line 362
    const/4 v5, 0x0

    move-object/from16 v0, v19

    iput-boolean v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isFirstRFT:Z

    .line 364
    const/4 v5, 0x0

    move-object/from16 v0, v19

    iput-boolean v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isSupportTransportItems:Z

    .line 366
    new-instance v4, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;

    const-string v5, "CreateSession"

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;Ljava/lang/String;)V

    .line 368
    .local v4, "actionHandler":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->setSessionInfo(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;)V

    .line 369
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->setHold(Z)V

    .line 373
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-static/range {v5 .. v12}, Lcom/samsung/android/allshare/framework/core/fileshare/AllShareFrameworkCoreNativeAPI;->createSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;)I

    move-result v17

    .line 377
    if-eqz v17, :cond_b

    .line 378
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">>> create session failed! deviceUdn = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 383
    :cond_b
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">>> create session successfully! requestID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v12, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareRequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    invoke-virtual {v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->isHeld()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_c

    .line 388
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "actionHandler wait is locked!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    monitor-enter v4

    .line 392
    const-wide/16 v6, 0x7530

    :try_start_3
    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 397
    :goto_4
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 400
    :cond_c
    invoke-virtual {v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->getErrorCode()I

    move-result v17

    .line 401
    const/4 v5, -0x1

    move/from16 v0, v17

    if-ne v0, v5, :cond_d

    .line 402
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "Wait Response time out!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->mActionHandlerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 393
    :catch_1
    move-exception v13

    .line 394
    .restart local v13    # "e":Ljava/lang/InterruptedException;
    :try_start_5
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wait exception happened!!! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v13}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 397
    .end local v13    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v5

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v5

    .line 404
    :cond_d
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    invoke-static {v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v5

    if-eqz v5, :cond_e

    .line 405
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "Receive response of create session action!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    # getter for: Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->sessionInfo:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
    invoke-static {v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;->access$200(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$ActionHandlerInfo;)Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->sessionID:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 408
    :cond_e
    const-string v5, "FileShareCPWrapper"

    const-string v6, "transfer"

    const-string v7, "Invalid session info in create session action!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method
