.class public Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;
.super Lcom/samsung/android/allshare/service/fileshare/handler/SyncActionHandler;
.source "DeviceFinderDeviceSyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DeviceFinderDeviceSyncActionHandler"


# instance fields
.field private deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

.field private mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/SyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V

    .line 48
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 54
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 55
    return-void
.end method

.method private formEventBundle(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    .param p3, "changeType"    # Ljava/lang/String;

    .prologue
    .line 345
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 346
    .local v1, "eventBundle":Landroid/os/Bundle;
    invoke-static {p2, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 348
    .local v0, "changedDevice":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.GLOBAL"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v2, "BUNDLE_STRING_TYPE"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v2, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 352
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-object v1
.end method

.method public static generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "boundInterfaceName"    # Ljava/lang/String;

    .prologue
    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceByID(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 175
    .local v2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;>;"
    if-nez v2, :cond_0

    .line 176
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 191
    :goto_0
    return-object v0

    .line 179
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;

    .line 180
    .local v1, "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->UDN:Ljava/lang/String;

    iput-object v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->ID:Ljava/lang/String;

    .line 183
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->ID:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 185
    invoke-static {p2}, Lcom/samsung/android/allshare/Device$DeviceType;->valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 187
    .local v0, "bundleDevice":Landroid/os/Bundle;
    goto :goto_0

    .line 191
    .end local v0    # "bundleDevice":Landroid/os/Bundle;
    .end local v1    # "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method private getDeviceByIPAddress(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 203
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 205
    .local v2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;>;"
    if-nez v2, :cond_0

    .line 206
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 217
    :goto_0
    return-object v0

    .line 209
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;

    .line 210
    .local v1, "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 211
    invoke-static {p2}, Lcom/samsung/android/allshare/Device$DeviceType;->valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 213
    .local v0, "bundleDevice":Landroid/os/Bundle;
    goto :goto_0

    .line 217
    .end local v0    # "bundleDevice":Landroid/os/Bundle;
    .end local v1    # "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method private getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "deviceType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v1

    .line 375
    .local v1, "deviceTypeID":I
    const/4 v0, 0x0

    .line 377
    .local v0, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;>;"
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 379
    return-object v0
.end method

.method private getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;
    .param p3, "deviceIface"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 230
    .local v4, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;>;"
    if-nez v4, :cond_1

    .line 231
    const/4 v1, 0x0

    .line 268
    :cond_0
    return-object v1

    .line 235
    :cond_1
    const/4 v2, 0x0

    .line 236
    .local v2, "deviceDomain":I
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device$DeviceDomain;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 237
    const/16 v2, 0x457

    .line 247
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v1, "deviceBundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    if-eqz p3, :cond_a

    .line 249
    :goto_1
    const/4 v0, 0x0

    .line 250
    .local v0, "bundleDevice":Landroid/os/Bundle;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;

    .line 252
    .local v3, "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    const/16 v6, 0x457

    if-ne v2, v6, :cond_3

    iget-boolean v6, v3, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isLocalDevice:Z

    if-nez v6, :cond_5

    :cond_3
    const/16 v6, 0x8ae

    if-ne v2, v6, :cond_4

    iget-boolean v6, v3, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isLocalDevice:Z

    if-eqz v6, :cond_5

    :cond_4
    if-nez v2, :cond_b

    .line 254
    :cond_5
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_6

    iget-object v6, v3, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    invoke-virtual {p3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 257
    :cond_6
    invoke-static {p2}, Lcom/samsung/android/allshare/Device$DeviceType;->valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 259
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 238
    .end local v0    # "bundleDevice":Landroid/os/Bundle;
    .end local v1    # "deviceBundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v3    # "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_7
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device$DeviceDomain;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 239
    const/16 v2, 0x8ae

    goto :goto_0

    .line 240
    :cond_8
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device$DeviceDomain;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 241
    const/16 v2, 0xd05

    goto :goto_0

    .line 243
    :cond_9
    const/4 v2, 0x0

    goto :goto_0

    .line 248
    .restart local v1    # "deviceBundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_a
    const-string p3, ""

    goto :goto_1

    .line 260
    .restart local v0    # "bundleDevice":Landroid/os/Bundle;
    .restart local v3    # "deviceItem":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_b
    const/16 v6, 0xd05

    if-ne v2, v6, :cond_2

    goto :goto_2
.end method

.method private getDeviceTypeID(Ljava/lang/String;)I
    .locals 2
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    const/16 v0, 0xff

    .line 389
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 390
    const/4 v0, 0x2

    .line 405
    :cond_0
    :goto_0
    return v0

    .line 391
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 392
    const/4 v0, 0x1

    goto :goto_0

    .line 393
    :cond_2
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 394
    const/4 v0, 0x4

    goto :goto_0

    .line 395
    :cond_3
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 396
    const/4 v0, 0x5

    goto :goto_0

    .line 397
    :cond_4
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 398
    const/4 v0, 0x7

    goto :goto_0

    .line 399
    :cond_5
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 400
    const/16 v0, 0x8

    goto :goto_0

    .line 401
    :cond_6
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method private getDeviceTypeStr(I)Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1
    .param p1, "deviceTypeID"    # I

    .prologue
    .line 415
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 416
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 428
    :goto_0
    return-object v0

    .line 417
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 418
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 419
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 420
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 421
    :cond_3
    const/4 v0, 0x5

    if-ne p1, v0, :cond_4

    .line 422
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 423
    :cond_4
    const/4 v0, 0x7

    if-ne p1, v0, :cond_5

    .line 424
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 425
    :cond_5
    const/16 v0, 0x8

    if-ne p1, v0, :cond_6

    .line 426
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 428
    :cond_6
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0
.end method

.method private getEventStrByEventType(IZ)Ljava/lang/String;
    .locals 1
    .param p1, "deviceType"    # I
    .param p2, "isAdded"    # Z

    .prologue
    .line 433
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 434
    const-string v0, "com.sec.android.allshare.event.EVENT_PROVIDER_DISCOVERY"

    .line 446
    :goto_0
    return-object v0

    .line 435
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 436
    const-string v0, "com.sec.android.allshare.event.EVENT_IMAGE_VIEWER_DISCOVERY"

    goto :goto_0

    .line 437
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 438
    const-string v0, "com.sec.android.allshare.event.EVENT_AV_PLAYER_DISCOVERY"

    goto :goto_0

    .line 439
    :cond_2
    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    .line 440
    const-string v0, "com.sec.android.allshare.event.EVENT_SMARTCONTROL_DISCOVERY"

    goto :goto_0

    .line 441
    :cond_3
    const/4 v0, 0x7

    if-ne p1, v0, :cond_4

    .line 442
    const-string v0, "com.sec.android.allshare.event.EVENT_FILERECEIVER_DISCOVERY"

    goto :goto_0

    .line 443
    :cond_4
    const/16 v0, 0x8

    if-ne p1, v0, :cond_5

    .line 444
    const-string v0, "com.sec.android.allshare.event.EVENT_KIES_SYNC_SERVER_DISCOVERY"

    goto :goto_0

    .line 446
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyToServiceManager(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 365
    return-void
.end method

.method private refreshByAppID(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 2
    .param p1, "appID"    # Ljava/lang/String;

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 277
    .local v0, "result":I
    if-eqz p1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->rescanDeviceByAppID(Ljava/lang/String;)I

    move-result v0

    .line 283
    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    :goto_1
    return-object v1

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->rescanDevice()I

    move-result v0

    goto :goto_0

    .line 283
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_1
.end method

.method private refreshByType(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 3
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 290
    const/4 v1, 0x0

    .line 291
    .local v1, "result":I
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 293
    .local v0, "deviceTypeID":I
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->rescanDeviceByType(I)I

    move-result v1

    .line 295
    if-nez v1, :cond_0

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method private registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/allshare/ERROR;"
        }
    .end annotation

    .prologue
    .line 455
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 456
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 468
    :goto_0
    return-object v5

    .line 458
    :cond_0
    if-nez p2, :cond_1

    .line 459
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 461
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 462
    .local v3, "searchTargetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 463
    .local v4, "target":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 464
    .local v0, "deviceType":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 466
    .end local v0    # "deviceType":I
    .end local v4    # "target":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v5, p1, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    .line 468
    .local v2, "result":I
    if-nez v2, :cond_3

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    :cond_3
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method private unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/allshare/ERROR;"
        }
    .end annotation

    .prologue
    .line 472
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 473
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 485
    :goto_0
    return-object v5

    .line 475
    :cond_0
    if-nez p2, :cond_1

    .line 476
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 478
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 479
    .local v3, "searchTargetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 480
    .local v4, "target":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 481
    .local v0, "deviceType":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 483
    .end local v0    # "deviceType":I
    .end local v4    # "target":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v5, p1, v3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    .line 485
    .local v2, "result":I
    if-nez v2, :cond_3

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    :cond_3
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method


# virtual methods
.method protected defineHandler()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 63
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH_TARGET"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 64
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_ID_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 65
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_DOMAIN_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 66
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_TYPE_IFACE_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 67
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 68
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_SOURCE_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 69
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 70
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_UNREGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->removeDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z

    .line 89
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper;->addDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/fileshare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z

    .line 80
    return-void
.end method

.method public onDeviceAdded(ILcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;)V
    .locals 7
    .param p1, "deviceType"    # I
    .param p2, "device"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;

    .prologue
    .line 306
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeStr(I)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    .line 307
    .local v0, "deviceTypeStr":Lcom/samsung/android/allshare/Device$DeviceType;
    const-string v3, "DeviceFinderDeviceSyncActionHandler"

    const-string v4, "[onDeviceAdded]: "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ">>>Receive Device Add Info, deviceNameStr = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", deviceTypeStr = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string v3, "ADDED"

    invoke-direct {p0, v0, p2, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->formEventBundle(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 310
    .local v1, "eventBundle":Landroid/os/Bundle;
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getEventStrByEventType(IZ)Ljava/lang/String;

    move-result-object v2

    .line 312
    .local v2, "eventType":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 313
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->notifyToServiceManager(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 315
    :cond_0
    return-void
.end method

.method public onDeviceRemoved(ILcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;)V
    .locals 7
    .param p1, "deviceType"    # I
    .param p2, "device"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeStr(I)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    .line 326
    .local v0, "deviceTypeStr":Lcom/samsung/android/allshare/Device$DeviceType;
    const-string v3, "DeviceFinderDeviceSyncActionHandler"

    const-string v4, "[onDeviceRemoved]: "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ">>>Receive Device Remove Info, deviceNameStr = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", deviceTypeStr = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v3, "REMOVED"

    invoke-direct {p0, v0, p2, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->formEventBundle(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 329
    .local v1, "eventBundle":Landroid/os/Bundle;
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getEventStrByEventType(IZ)Ljava/lang/String;

    move-result-object v2

    .line 331
    .local v2, "eventType":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 332
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->notifyToServiceManager(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 334
    :cond_0
    return-void
.end method

.method protected responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 15
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 100
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "action":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v11

    .line 102
    .local v11, "reqb":Landroid/os/Bundle;
    const-string v13, "BUNDLE_STRING_NAME"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 103
    .local v10, "name":Ljava/lang/String;
    const-string v13, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 104
    .local v7, "deviceType":Ljava/lang/String;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 105
    .local v3, "b":Landroid/os/Bundle;
    const-string v13, "BUNDLE_STRING_NAME"

    invoke-virtual {v3, v13, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH"

    invoke-virtual {v13, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 108
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "appId":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->refreshByAppID(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_1

    .line 111
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 161
    .end local v2    # "appId":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v13

    return-object v13

    .line 113
    .restart local v2    # "appId":Ljava/lang/String;
    :cond_1
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 114
    .end local v2    # "appId":Ljava/lang/String;
    :cond_2
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH_TARGET"

    invoke-virtual {v13, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 115
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    invoke-direct {p0, v7}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->refreshByType(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_3

    .line 117
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 119
    :cond_3
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 120
    :cond_4
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 121
    const-string v13, "0"

    const/4 v14, 0x0

    invoke-direct {p0, v13, v7, v14}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 122
    .local v6, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v3, v13, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 123
    .end local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_5
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_ID_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 125
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "ID":Ljava/lang/String;
    invoke-direct {p0, v0, v7}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceByID(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 127
    .local v5, "device":Landroid/os/Bundle;
    const-string v13, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v3, v13, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 128
    .end local v0    # "ID":Ljava/lang/String;
    .end local v5    # "device":Landroid/os/Bundle;
    :cond_6
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_TYPE_IFACE_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 130
    const-string v13, "BUNDLE_STRING_BOUND_INTERFACE"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "boundInterface":Ljava/lang/String;
    const-string v13, "0"

    invoke-direct {p0, v13, v7, v4}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 132
    .restart local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v3, v13, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 133
    .end local v4    # "boundInterface":Ljava/lang/String;
    .end local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_7
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_SOURCE_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 135
    const-string v13, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 136
    .local v9, "ipAddress":Ljava/lang/String;
    invoke-direct {p0, v9, v7}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceByIPAddress(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 137
    .restart local v5    # "device":Landroid/os/Bundle;
    const-string v13, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v3, v13, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_0

    .line 138
    .end local v5    # "device":Landroid/os/Bundle;
    .end local v9    # "ipAddress":Ljava/lang/String;
    :cond_8
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_DOMAIN_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 140
    const-string v13, "BUNDLE_ENUM_DEVICE_DOMAIN"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 141
    .local v8, "domain":Ljava/lang/String;
    const/4 v13, 0x0

    invoke-direct {p0, v8, v7, v13}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 142
    .restart local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v3, v13, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 143
    .end local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v8    # "domain":Ljava/lang/String;
    :cond_9
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 144
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 145
    .restart local v2    # "appId":Ljava/lang/String;
    const-string v13, "BUNDLE_STRINGARRAYLIST_DEVICE_TYPE_LIST"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 147
    .local v12, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v12}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_a

    .line 148
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 150
    :cond_a
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 151
    .end local v2    # "appId":Ljava/lang/String;
    .end local v12    # "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_UNREGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 152
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 153
    .restart local v2    # "appId":Ljava/lang/String;
    const-string v13, "BUNDLE_STRINGARRAYLIST_DEVICE_TYPE_LIST"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 155
    .restart local v12    # "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v12}, Lcom/samsung/android/allshare/service/fileshare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_c

    .line 156
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 158
    :cond_c
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
