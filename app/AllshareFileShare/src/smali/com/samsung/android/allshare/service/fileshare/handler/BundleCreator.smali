.class public Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;
.super Ljava/lang/Object;
.source "BundleCreator.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BundleCreator"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    return-void
.end method

.method public static createDeviceBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;
    .locals 12
    .param p0, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;

    .prologue
    const/16 v11, 0x78

    const/4 v10, 0x0

    .line 45
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 46
    :cond_0
    const/4 v0, 0x0

    .line 162
    :cond_1
    :goto_0
    return-object v0

    .line 50
    :cond_2
    const/4 v5, 0x0

    .line 51
    .local v5, "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v1, 0x0

    .line 52
    .local v1, "deviceIconUrl":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 53
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->createIconBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 54
    if-eqz v5, :cond_7

    .line 55
    const-string v6, "BundleCreator"

    const-string v7, "createDeviceBundle"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "createDeviceBundle---->iconList.size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const/4 v4, 0x0

    .line 60
    .local v4, "icon":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 61
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "icon":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;
    check-cast v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;

    .line 62
    .restart local v4    # "icon":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;
    if-eqz v4, :cond_6

    iget v6, v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->height:I

    if-ne v6, v11, :cond_6

    iget v6, v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->width:I

    if-ne v6, v11, :cond_6

    iget v6, v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->depth:I

    const/16 v7, 0x18

    if-ne v6, v7, :cond_6

    iget-object v6, v4, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->mimeType:Ljava/lang/String;

    const-string v7, "image/png"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 64
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;

    iget-object v1, v6, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->URL:Ljava/lang/String;

    .line 77
    .end local v3    # "i":I
    .end local v4    # "icon":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;
    :cond_3
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->IPAddr:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-static {v10}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->getHostAddress(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 78
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isLocalDevice:Z

    .line 81
    :cond_4
    iget-boolean v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isLocalDevice:Z

    if-eqz v6, :cond_8

    .line 82
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device$DeviceDomain;->name()Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "deviceLocation":Ljava/lang/String;
    :goto_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->name:Ljava/lang/String;

    if-eqz v6, :cond_9

    .line 90
    const-string v6, "BUNDLE_STRING_DEVICE_NAME"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->name:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_4
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->ID:Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 95
    const-string v6, "BUNDLE_STRING_DEVICE_MODELNAME"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->ID:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :goto_5
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    if-eqz v6, :cond_b

    .line 100
    const-string v6, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_6
    const-string v6, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device$DeviceType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->UDN:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 107
    const-string v6, "BUNDLE_STRING_ID"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :goto_7
    const-string v6, "BUNDLE_ENUM_DEVICE_DOMAIN"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->IPAddr:Ljava/lang/String;

    if-eqz v6, :cond_d

    .line 113
    const-string v6, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :goto_8
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->modelName:Ljava/lang/String;

    if-eqz v6, :cond_e

    .line 118
    const-string v6, "BUNDLE_STRING_DEVICE_MODELNAME"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->modelName:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :goto_9
    if-eqz v5, :cond_f

    .line 124
    const-string v6, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICONLIST"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 130
    :goto_a
    if-eqz v1, :cond_5

    .line 131
    const-string v6, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICON"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 136
    :cond_5
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 137
    const-string v6, "BUNDLE_BOOLEAN_SEARCHABLE"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isSearchable:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 138
    const-string v6, "BUNDLE_BOOLEAN_RECEIVERABLE"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isDownloadable:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 67
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "deviceLocation":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "icon":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;

    iget-object v1, v6, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->URL:Ljava/lang/String;

    .line 60
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 71
    .end local v3    # "i":I
    .end local v4    # "icon":Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;
    :cond_7
    const-string v6, "BundleCreator"

    const-string v7, "createDeviceBundle"

    const-string v8, "createDeviceBundle---->iconList == null"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 84
    :cond_8
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device$DeviceDomain;->name()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "deviceLocation":Ljava/lang/String;
    goto/16 :goto_3

    .line 92
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_9
    const-string v6, "BUNDLE_STRING_DEVICE_NAME"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 97
    :cond_a
    const-string v6, "BUNDLE_STRING_DEVICE_MODELNAME"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 103
    :cond_b
    const-string v6, "BUNDLE_STRING_BOUND_INTERFACE"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 109
    :cond_c
    const-string v6, "BUNDLE_STRING_ID"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 115
    :cond_d
    const-string v6, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 120
    :cond_e
    const-string v6, "BUNDLE_STRING_DEVICE_MODELNAME"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 126
    :cond_f
    const-string v6, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICONLIST"

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_a

    .line 139
    :cond_10
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 140
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_PLAYLIST_PLAYER"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isSupportImagePlayList:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 142
    const-string v6, "BUNDLE_BOOLEAN_SEEKABLE"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isSeekable:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    const-string v6, "BUNDLE_STRING_DEVICE_ID"

    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->deviceID:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v6, "BUNDLE_BOOLEAN_NAVIGATE_IN_PAUSE"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isNavigateInPause:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    const-string v6, "BUNDLE_BOOLEAN_AUTO_SLIDE_SHOW"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isSlideshowSupport:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 152
    :cond_11
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 153
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_AUDIO_PLAYLIST_PLAYER"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isSupportAudioPlayList:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 155
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_VIDEO_PLAYLIST_PLAYER"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isSupportVideoPlayList:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 157
    :cond_12
    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 158
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_TVCONTROLLER"

    iget-boolean v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/Device;->isTVControlable:Z

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method private static createIconBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;)Landroid/os/Bundle;
    .locals 3
    .param p0, "iconItem"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;

    .prologue
    .line 177
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 179
    .local v0, "iconBundle":Landroid/os/Bundle;
    const-string v1, "ICON_URI"

    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->URL:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 180
    const-string v1, "ICON_DEPTH"

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->depth:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    const-string v1, "ICON_WIDTH"

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->width:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v1, "ICON_HEIGHT"

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->height:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 183
    const-string v1, "ICON_MIMETYPE"

    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-object v0
.end method

.method private static createIconBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    .local p0, "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v2, "iconbundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 169
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;

    invoke-static {v3}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->createIconBundle(Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DeviceIcon;)Landroid/os/Bundle;

    move-result-object v1

    .line 170
    .local v1, "iconBundle":Landroid/os/Bundle;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    .end local v1    # "iconBundle":Landroid/os/Bundle;
    :cond_0
    return-object v2
.end method

.method public static final getHostAddress(I)Ljava/lang/String;
    .locals 10
    .param p0, "n"    # I

    .prologue
    .line 189
    const/4 v4, 0x0

    .line 191
    .local v4, "hostAddrCnt":I
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v6

    .line 192
    .local v6, "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    if-nez v6, :cond_0

    .line 193
    const-string v3, ""

    .line 212
    .end local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_0
    return-object v3

    .line 194
    .restart local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 195
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 196
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 197
    .local v1, "addrs":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 198
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 199
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/fileshare/handler/BundleCreator;->isUsableAddress(Ljava/net/InetAddress;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 201
    if-ge v4, p0, :cond_2

    .line 202
    add-int/lit8 v4, v4, 0x1

    .line 203
    goto :goto_1

    .line 205
    :cond_2
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 206
    .local v3, "host":Ljava/lang/String;
    goto :goto_0

    .line 209
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "host":Ljava/lang/String;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v2

    .line 210
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "BundleCreator"

    const-string v8, "getHostAddress"

    const-string v9, ""

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->w_stack(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 212
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v3, ""

    goto :goto_0
.end method

.method private static isUsableAddress(Ljava/net/InetAddress;)Z
    .locals 1
    .param p0, "addr"    # Ljava/net/InetAddress;

    .prologue
    .line 216
    instance-of v0, p0, Ljava/net/Inet6Address;

    if-eqz v0, :cond_0

    .line 217
    const/4 v0, 0x0

    .line 219
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
