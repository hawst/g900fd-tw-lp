.class Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;
.super Ljava/lang/Thread;
.source "FileShareCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandlerThread"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;
    }
.end annotation


# static fields
.field private static final THREAD_SLEEP_TIME:I = 0x3e8


# instance fields
.field private eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;",
            ">;"
        }
    .end annotation
.end field

.field isRunning:Z

.field final synthetic this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->isRunning:Z

    .line 99
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
    .param p2, "x1"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;)V

    return-void
.end method


# virtual methods
.method public addEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V
    .locals 4
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "eventItem"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    .prologue
    .line 142
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "FileShareCPWrapper"

    const-string v2, "addEvent"

    const-string v3, "putQ InterruptedException "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public addResponse(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "sessionID"    # Ljava/lang/String;
    .param p4, "errorCode"    # I

    .prologue
    .line 150
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v7, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    return-void

    .line 151
    :catch_0
    move-exception v6

    .line 152
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v0, "FileShareCPWrapper"

    const-string v1, "addResponse"

    const-string v2, "putQ InterruptedException "

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public addSubscribeResponse(Ljava/lang/String;I)V
    .locals 4
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 158
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v2, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "FileShareCPWrapper"

    const-string v2, "addSubscribeResponse"

    const-string v3, "putQ InterruptedException "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 105
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->isRunning:Z

    .line 107
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->isRunning:Z

    if-eqz v2, :cond_4

    .line 110
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v4, 0x3e8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;

    .line 112
    .local v1, "event":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;
    if-eqz v1, :cond_0

    .line 116
    const-string v2, "fileShareResponse"

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    iget v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->requestID:I

    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->actionName:Ljava/lang/String;

    iget-object v5, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->sessionID:Ljava/lang/String;

    iget v6, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->errorCode:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->handleResponse(ILjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    .end local v1    # "event":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "FileShareCPWrapper"

    const-string v3, "run"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "event":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;
    :cond_1
    :try_start_1
    const-string v2, "fileShareEvent"

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->deviceUDN:Ljava/lang/String;

    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->sessionID:Ljava/lang/String;

    iget-object v5, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventItem:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->handleEvent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareEventItem;)V

    goto :goto_0

    .line 121
    :cond_2
    const-string v2, "fileShareSubscribeResponse"

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 122
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->deviceUDN:Ljava/lang/String;

    iget v4, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->errorCode:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;->handleSubscribeResponse(Ljava/lang/String;I)V

    goto :goto_0

    .line 124
    :cond_3
    const-string v2, "FileShareCPWrapper"

    const-string v3, "run"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid event type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;->eventType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 131
    .end local v1    # "event":Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread$FileShareEvent;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->eventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 132
    const-string v2, "FileShareCPWrapper"

    const-string v3, "run"

    const-string v4, "EventHandlerThread finished."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->i_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public setRunningStatus(Z)V
    .locals 0
    .param p1, "runStatus"    # Z

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$EventHandlerThread;->isRunning:Z

    .line 138
    return-void
.end method
