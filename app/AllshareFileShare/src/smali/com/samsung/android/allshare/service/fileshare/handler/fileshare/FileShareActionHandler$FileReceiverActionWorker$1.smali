.class Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;
.super Ljava/lang/Object;
.source "FileShareActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 100
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "actionid":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->mHandlerMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;->access$500(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;

    .line 102
    .local v4, "handler":Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;
    if-nez v4, :cond_0

    .line 103
    const-string v5, "FileShareActionHandler"

    const-string v6, "[run]: "

    const-string v7, "Null pointer Error!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 108
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-interface {v4, v5}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$IHandleAction;->handleAction(Lcom/sec/android/allshare/iface/CVMessage;)Landroid/os/Bundle;

    move-result-object v2

    .line 109
    .local v2, "bundle":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;->response(Landroid/os/Bundle;)V
    invoke-static {v5, v2}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;->access$600(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 112
    .end local v2    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 113
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "FileShareActionHandler"

    const-string v6, "[run]: "

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 120
    .local v1, "b":Landroid/os/Bundle;
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v5, p0, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;->response(Landroid/os/Bundle;)V
    invoke-static {v5, v1}, Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;->access$700(Lcom/samsung/android/allshare/service/fileshare/handler/fileshare/FileShareActionHandler$FileReceiverActionWorker;Landroid/os/Bundle;)V

    goto :goto_0

    .line 114
    .end local v1    # "b":Landroid/os/Bundle;
    :catch_1
    move-exception v3

    .line 115
    .local v3, "e":Ljava/lang/Error;
    const-string v5, "FileShareActionHandler"

    const-string v6, "[run]: "

    const-string v7, "Error"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/fileshare/data/common/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
