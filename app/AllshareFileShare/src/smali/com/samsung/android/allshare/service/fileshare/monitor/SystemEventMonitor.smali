.class public Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
.super Ljava/lang/Object;
.source "SystemEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$3;
    }
.end annotation


# static fields
.field private static final AIRPLANE_MODE_OFF:I = 0x0

.field private static final AIRPLANE_MODE_ON:I = 0x1


# instance fields
.field private handler:Landroid/os/Handler;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mIsP2pAvailable:Z

.field private mP2pInterfaceName:Ljava/lang/String;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

.field private mStarted:Z

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/fileshare/ServiceManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 57
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mPowerManager:Landroid/os/PowerManager;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    .line 61
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    .line 183
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$1;-><init>(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    .line 231
    new-instance v0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$2;-><init>(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 315
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 71
    iput-object p1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->init()V

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyByHandler(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->checkP2PState(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Landroid/content/Context;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->getAirplaneModeState(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->checkAirplaneModeState(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->checkScreenState(Landroid/content/Intent;)V

    return-void
.end method

.method private checkAirplaneModeState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 136
    packed-switch p1, :pswitch_data_0

    .line 150
    :goto_0
    return-void

    .line 138
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkAirplaneModeState"

    const-string v2, "AIRPLANE MODE: OFF"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_OFF"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkAirplaneModeState"

    const-string v2, "AIRPLANE MODE: ON"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 147
    const-string v0, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_ON"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkP2PState(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 105
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    .line 106
    .local v1, "state":Landroid/net/NetworkInfo$State;
    const-string v2, "networkInfo"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 109
    .local v0, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    .line 114
    :goto_0
    sget-object v2, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 129
    :goto_1
    return-void

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    goto :goto_0

    .line 116
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v3, "checkP2PState"

    const-string v4, "WIFI P2P STATE: CONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 120
    const-string v2, "com.sec.android.allshare.event.EVENT_WIFI_P2P_ENABLED"

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v3, "checkP2PState"

    const-string v4, "WIFI P2P STATE: DISCONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 126
    const-string v2, "com.sec.android.allshare.event.EVENT_WIFI_P2P_DISABLED"

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_1

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkScreenState(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 159
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkScreenState"

    const-string v2, "SCREEN STATE: ON"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v0, "com.sec.android.allshare.event.EVENT_SCREEN_ON"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkScreenState"

    const-string v2, "SCREEN STATE: OFF"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "com.sec.android.allshare.event.EVENT_SCREEN_OFF"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :cond_2
    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkScreenState"

    const-string v2, "SCREEN STATE: USER_PRESENT"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "com.sec.android.allshare.event.EVENT_USER_PRESENT"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAirplaneModeState(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "airplane_mode_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 80
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 82
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mPowerManager:Landroid/os/PowerManager;

    .line 85
    const-string v0, "p2p-wlan0-0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    .line 90
    return-void
.end method

.method private notifyByHandler(Ljava/lang/String;)V
    .locals 4
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 197
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 198
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "com.sec.android.allshare.event.EVENT_PARAMETER_KEY1"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 200
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 203
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "notifyByHandler"

    const-string v3, "AllShareEvent.EVENT_WIFI_P2P_CONNECTED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 209
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "notifyByHandler"

    const-string v3, "AllShareEvent.EVENT_WIFI_P2P_DISCONNECTED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_1
    const-string v1, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_ON"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_OFF"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 214
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 215
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "notifyByHandler"

    const-string v3, "AllShareEvent.EVENT_AIRPLANE_MODE_ON Or AllShareEvent.EVENT_AIRPLANE_MODE_OFF"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_3
    const-string v1, "com.sec.android.allshare.event.EVENT_USER_PRESENT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 220
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/fileshare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 221
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "notifyByHandler"

    const-string v3, "AllShareEvent.EVENT_USER_PRESENT"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/service/fileshare/utility/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_4
    return-void
.end method

.method private notifyEvent(Ljava/lang/String;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 178
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 179
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 180
    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 181
    return-void
.end method


# virtual methods
.method public fini()V
    .locals 2

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    if-eqz v0, :cond_0

    .line 271
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 272
    :cond_0
    return-void
.end method

.method public isNetworkAvailable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 321
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScreenOn()Z
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method public startMonitor()V
    .locals 3

    .prologue
    .line 278
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    if-eqz v1, :cond_0

    .line 302
    :goto_0
    return-void

    .line 281
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 284
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 285
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 286
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 287
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 290
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 293
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 297
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 299
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 301
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    goto :goto_0
.end method

.method public stopMonitor()V
    .locals 2

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    if-nez v0, :cond_0

    .line 313
    :goto_0
    return-void

    .line 311
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/service/fileshare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    goto :goto_0
.end method
