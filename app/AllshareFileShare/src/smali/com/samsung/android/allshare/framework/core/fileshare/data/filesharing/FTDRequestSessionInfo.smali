.class public Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDRequestSessionInfo;
.super Ljava/lang/Object;
.source "FTDRequestSessionInfo.java"


# instance fields
.field count:I

.field requestStatus:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDSessionAcceptInfo;

.field totalSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setAcceptInfo(Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDSessionAcceptInfo;)V
    .locals 0
    .param p1, "requestStatus"    # Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDSessionAcceptInfo;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDRequestSessionInfo;->requestStatus:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDSessionAcceptInfo;

    .line 19
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDRequestSessionInfo;->count:I

    .line 23
    return-void
.end method

.method public declared-synchronized setRequestStatus(Z)V
    .locals 1
    .param p1, "isAccept"    # Z

    .prologue
    .line 13
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDRequestSessionInfo;->requestStatus:Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDSessionAcceptInfo;

    iput-boolean p1, v0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDSessionAcceptInfo;->isAccept:Z

    .line 14
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    monitor-exit p0

    return-void

    .line 13
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FTDRequestSessionInfo;->totalSize:J

    .line 27
    return-void
.end method
