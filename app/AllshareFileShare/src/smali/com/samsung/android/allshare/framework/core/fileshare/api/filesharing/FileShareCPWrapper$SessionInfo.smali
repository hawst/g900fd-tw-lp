.class Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;
.super Ljava/lang/Object;
.source "FileShareCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionInfo"
.end annotation


# instance fields
.field currentSentSize:J

.field currentTransportIndex:I

.field deviceUDN:Ljava/lang/String;

.field isCancelled:Z

.field isFirstRFT:Z

.field isSupportTransportItems:Z

.field itemsDescriptionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/fileshare/data/filesharing/FileShareItemDescription;",
            ">;"
        }
    .end annotation
.end field

.field sessionID:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

.field totalSentSize:J

.field totalSize:J


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 801
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->this$0:Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810
    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isCancelled:Z

    .line 821
    iput-boolean v0, p0, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;->isFirstRFT:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;
    .param p2, "x1"    # Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$1;

    .prologue
    .line 801
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper$SessionInfo;-><init>(Lcom/samsung/android/allshare/framework/core/fileshare/api/filesharing/FileShareCPWrapper;)V

    return-void
.end method
