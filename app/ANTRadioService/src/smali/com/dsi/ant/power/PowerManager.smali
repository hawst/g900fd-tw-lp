.class public Lcom/dsi/ant/power/PowerManager;
.super Ljava/lang/Object;
.source "PowerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/power/PowerManager$CanEnableState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/dsi/ant/power/PowerManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/power/PowerManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static canEnable()Z
    .locals 2

    .prologue
    .line 101
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterProvider;->getBuiltInChipPowerControl()Lcom/dsi/ant/adapter/BuiltInPowerControl;

    move-result-object v0

    .line 103
    .local v0, "powercontrol":Lcom/dsi/ant/adapter/BuiltInPowerControl;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->canEnable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    const/4 v1, 0x1

    .line 106
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static disable()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 3

    .prologue
    .line 87
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterProvider;->getBuiltInChipPowerControl()Lcom/dsi/ant/adapter/BuiltInPowerControl;

    move-result-object v0

    .line 89
    .local v0, "powerControl":Lcom/dsi/ant/adapter/BuiltInPowerControl;
    if-nez v0, :cond_0

    .line 90
    sget-object v1, Lcom/dsi/ant/power/PowerManager;->TAG:Ljava/lang/String;

    const-string v2, "Attempting to disable on a non-existent adapter"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->INVALID:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 93
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v1

    goto :goto_0
.end method

.method public static enable()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterProvider;->getBuiltInChipPowerControl()Lcom/dsi/ant/adapter/BuiltInPowerControl;

    move-result-object v0

    .line 53
    .local v0, "powerControl":Lcom/dsi/ant/adapter/BuiltInPowerControl;
    if-nez v0, :cond_0

    .line 54
    sget-object v1, Lcom/dsi/ant/power/PowerManager;->TAG:Ljava/lang/String;

    const-string v2, "Attempting to enable a non-existent adapter"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->INVALID:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 57
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v1

    goto :goto_0
.end method

.method public static onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 2
    .param p0, "state"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 115
    sget-object v0, Lcom/dsi/ant/power/PowerManager;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Receiving adapter state change to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 116
    return-void
.end method

.method public static tryEnable()Lcom/dsi/ant/power/PowerManager$CanEnableState;
    .locals 3

    .prologue
    .line 67
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->getBuiltInChipPowerControl()Lcom/dsi/ant/adapter/BuiltInPowerControl;

    move-result-object v1

    .line 70
    .local v1, "powercontrol":Lcom/dsi/ant/adapter/BuiltInPowerControl;
    sget-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->NO_ADAPTER:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 72
    .local v0, "canEnableState":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {v1}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->tryEnable()Lcom/dsi/ant/power/PowerManager$CanEnableState;

    move-result-object v0

    .line 75
    :cond_0
    return-object v0
.end method
