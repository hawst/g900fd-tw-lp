.class public final enum Lcom/dsi/ant/power/PowerManager$CanEnableState;
.super Ljava/lang/Enum;
.source "PowerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/power/PowerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CanEnableState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/power/PowerManager$CanEnableState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/power/PowerManager$CanEnableState;

.field public static final enum AIRPLANE_MODE_BLOCKING_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

.field public static final enum CAN_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

.field public static final enum ERROR_RECOVERY_COOLDOWN:Lcom/dsi/ant/power/PowerManager$CanEnableState;

.field public static final enum NO_ADAPTER:Lcom/dsi/ant/power/PowerManager$CanEnableState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;

    const-string v1, "CAN_ENABLE"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/power/PowerManager$CanEnableState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->CAN_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 32
    new-instance v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;

    const-string v1, "AIRPLANE_MODE_BLOCKING_ENABLE"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/power/PowerManager$CanEnableState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->AIRPLANE_MODE_BLOCKING_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 35
    new-instance v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;

    const-string v1, "ERROR_RECOVERY_COOLDOWN"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/power/PowerManager$CanEnableState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->ERROR_RECOVERY_COOLDOWN:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 38
    new-instance v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;

    const-string v1, "NO_ADAPTER"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/power/PowerManager$CanEnableState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->NO_ADAPTER:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/power/PowerManager$CanEnableState;

    sget-object v1, Lcom/dsi/ant/power/PowerManager$CanEnableState;->CAN_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/power/PowerManager$CanEnableState;->AIRPLANE_MODE_BLOCKING_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/power/PowerManager$CanEnableState;->ERROR_RECOVERY_COOLDOWN:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/power/PowerManager$CanEnableState;->NO_ADAPTER:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->$VALUES:[Lcom/dsi/ant/power/PowerManager$CanEnableState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/power/PowerManager$CanEnableState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/power/PowerManager$CanEnableState;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/dsi/ant/power/PowerManager$CanEnableState;->$VALUES:[Lcom/dsi/ant/power/PowerManager$CanEnableState;

    invoke-virtual {v0}, [Lcom/dsi/ant/power/PowerManager$CanEnableState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/power/PowerManager$CanEnableState;

    return-object v0
.end method
