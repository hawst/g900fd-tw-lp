.class public final enum Lcom/dsi/ant/service/NotificationID;
.super Ljava/lang/Enum;
.source "NotificationID.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/service/NotificationID;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/service/NotificationID;

.field public static final enum RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/dsi/ant/service/NotificationID;

    const-string v1, "RequestForceClaimWholeChipInterface"

    invoke-direct {v0, v1}, Lcom/dsi/ant/service/NotificationID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    .line 15
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/service/NotificationID;

    const/4 v1, 0x0

    sget-object v2, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/service/NotificationID;->$VALUES:[Lcom/dsi/ant/service/NotificationID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/service/NotificationID;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/dsi/ant/service/NotificationID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/service/NotificationID;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/service/NotificationID;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/dsi/ant/service/NotificationID;->$VALUES:[Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v0}, [Lcom/dsi/ant/service/NotificationID;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/service/NotificationID;

    return-object v0
.end method
