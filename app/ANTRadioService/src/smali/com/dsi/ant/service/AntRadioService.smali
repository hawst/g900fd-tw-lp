.class public Lcom/dsi/ant/service/AntRadioService;
.super Landroid/app/Service;
.source "AntRadioService.java"


# static fields
.field private static sAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

.field private static sAntManager:Lcom/dsi/ant/legacy/AntManager;

.field private static sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

.field private static sChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

.field private static sClaimManager:Lcom/dsi/ant/legacy/ClaimManager;

.field private static sContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/dsi/ant/service/AntRadioService;->sContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    return-object v0
.end method

.method public static getAntManager()Lcom/dsi/ant/legacy/AntManager;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sAntManager:Lcom/dsi/ant/legacy/AntManager;

    return-object v0
.end method

.method public static getBindingManager()Lcom/dsi/ant/legacy/BindingManager;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

    return-object v0
.end method

.method public static getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    return-object v0
.end method

.method public static getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sClaimManager:Lcom/dsi/ant/legacy/ClaimManager;

    return-object v0
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 142
    const-string v1, "com.dsi.ant.bind.AntService"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    new-instance v0, Lcom/dsi/ant/ipc/aidl/AntServiceWrapperAidl;

    invoke-direct {v0}, Lcom/dsi/ant/ipc/aidl/AntServiceWrapperAidl;-><init>()V

    .line 155
    .local v0, "binder":Landroid/os/IBinder;
    :goto_0
    return-object v0

    .line 152
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_0
    sget-object v1, Lcom/dsi/ant/service/AntRadioService;->sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/legacy/BindingManager;->doBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    .restart local v0    # "binder":Landroid/os/IBinder;
    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 93
    sput-object p0, Lcom/dsi/ant/service/AntRadioService;->sContext:Landroid/content/Context;

    .line 95
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 97
    new-instance v0, Lcom/dsi/ant/legacy/AntManager;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/AntManager;-><init>()V

    sput-object v0, Lcom/dsi/ant/service/AntRadioService;->sAntManager:Lcom/dsi/ant/legacy/AntManager;

    new-instance v0, Lcom/dsi/ant/legacy/BindingManager;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/BindingManager;-><init>()V

    sput-object v0, Lcom/dsi/ant/service/AntRadioService;->sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

    new-instance v0, Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-direct {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;-><init>()V

    sput-object v0, Lcom/dsi/ant/service/AntRadioService;->sChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    new-instance v0, Lcom/dsi/ant/legacy/ClaimManager;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/ClaimManager;-><init>()V

    sput-object v0, Lcom/dsi/ant/service/AntRadioService;->sClaimManager:Lcom/dsi/ant/legacy/ClaimManager;

    new-instance v0, Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-direct {v0}, Lcom/dsi/ant/adapter/AdapterProvider;-><init>()V

    sput-object v0, Lcom/dsi/ant/service/AntRadioService;->sAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    .line 98
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 114
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider;->stopChipDetectors()V

    .line 116
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sAntManager:Lcom/dsi/ant/legacy/AntManager;

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->destroy()V

    sput-object v1, Lcom/dsi/ant/service/AntRadioService;->sAntManager:Lcom/dsi/ant/legacy/AntManager;

    sput-object v1, Lcom/dsi/ant/service/AntRadioService;->sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

    sput-object v1, Lcom/dsi/ant/service/AntRadioService;->sClaimManager:Lcom/dsi/ant/legacy/ClaimManager;

    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider;->destroy()V

    sput-object v1, Lcom/dsi/ant/service/AntRadioService;->sAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->destroy()V

    sput-object v1, Lcom/dsi/ant/service/AntRadioService;->sChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .line 118
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 120
    sput-object v1, Lcom/dsi/ant/service/AntRadioService;->sContext:Landroid/content/Context;

    .line 122
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 123
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 163
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/BindingManager;->doBind(Landroid/content/Intent;)Landroid/os/IBinder;

    .line 164
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 169
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 171
    sget-object v0, Lcom/dsi/ant/service/AntRadioService;->sBindingManager:Lcom/dsi/ant/legacy/BindingManager;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/BindingManager;->doUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
