.class final Lcom/dsi/ant/legacy/BindingManager$2;
.super Lcom/dsi/ant/IAnt_6$Stub;
.source "BindingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/legacy/BindingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/legacy/BindingManager;


# direct methods
.method constructor <init>(Lcom/dsi/ant/legacy/BindingManager;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/dsi/ant/legacy/BindingManager$2;->this$0:Lcom/dsi/ant/legacy/BindingManager;

    invoke-direct {p0}, Lcom/dsi/ant/IAnt_6$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final ANTAddChannelId(BIBBB)Z
    .locals 6
    .param p1, "channelNumber"    # B
    .param p2, "deviceNumber"    # I
    .param p3, "deviceType"    # B
    .param p4, "txType"    # B
    .param p5, "listIndex"    # B

    .prologue
    .line 265
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/legacy/AntManager;->ANTAddChannelId(BIBBB)Z

    move-result v0

    return v0
.end method

.method public final ANTAssignChannel(BBB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "channelType"    # B
    .param p3, "networkNumber"    # B

    .prologue
    .line 225
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/dsi/ant/legacy/AntManager;->ANTAssignChannel(BBB)Z

    move-result v0

    return v0
.end method

.method public final ANTCloseChannel(B)Z
    .locals 1
    .param p1, "channelNumber"    # B

    .prologue
    .line 290
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/AntManager;->ANTCloseChannel(B)Z

    move-result v0

    return v0
.end method

.method public final ANTConfigEventBuffering(IIII)Z
    .locals 1
    .param p1, "screenOnFlushTimerInterval"    # I
    .param p2, "screenOnFlushBufferThreshold"    # I
    .param p3, "screenOffFlushTimerInterval"    # I
    .param p4, "screenOffFlushBufferThreshold"    # I

    .prologue
    .line 275
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dsi/ant/legacy/AntManager;->ANTConfigEventBuffering(IIII)Z

    move-result v0

    return v0
.end method

.method public final ANTConfigList(BBB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "listSize"    # B
    .param p3, "exclude"    # B

    .prologue
    .line 270
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/dsi/ant/legacy/AntManager;->ANTConfigList(BBB)Z

    move-result v0

    return v0
.end method

.method public final ANTDisableEventBuffering()Z
    .locals 1

    .prologue
    .line 280
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->ANTDisableEventBuffering()Z

    move-result v0

    return v0
.end method

.method public final ANTOpenChannel(B)Z
    .locals 1
    .param p1, "channelNumber"    # B

    .prologue
    .line 285
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/AntManager;->ANTOpenChannel(B)Z

    move-result v0

    return v0
.end method

.method public final ANTRequestMessage(BB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "messageID"    # B

    .prologue
    .line 295
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTRequestMessage(BB)Z

    move-result v0

    return v0
.end method

.method public final ANTResetSystem()Z
    .locals 1

    .prologue
    .line 215
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->ANTResetSystem()Z

    move-result v0

    return v0
.end method

.method public final ANTSendAcknowledgedData(B[B)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "txBuffer"    # [B

    .prologue
    .line 305
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSendAcknowledgedData(B[B)Z

    move-result v0

    return v0
.end method

.method public final ANTSendBroadcastData(B[B)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "txBuffer"    # [B

    .prologue
    .line 300
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSendBroadcastData(B[B)Z

    move-result v0

    return v0
.end method

.method public final ANTSendBurstTransfer(B[B)I
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "data"    # [B

    .prologue
    .line 315
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSendBurstTransfer(B[B)I

    move-result v0

    return v0
.end method

.method public final ANTSendBurstTransferPacket(B[B)Z
    .locals 1
    .param p1, "control"    # B
    .param p2, "txBuffer"    # [B

    .prologue
    .line 310
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSendBurstTransferPacket(B[B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelId(BIBB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "deviceNumber"    # I
    .param p3, "deviceType"    # B
    .param p4, "txType"    # B

    .prologue
    .line 230
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dsi/ant/legacy/AntManager;->ANTSetChannelId(BIBB)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelPeriod(BI)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "channelPeriod"    # I

    .prologue
    .line 235
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSetChannelPeriod(BI)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelRFFreq(BB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "radioFrequency"    # B

    .prologue
    .line 240
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSetChannelRFFreq(BB)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelSearchTimeout(BB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "searchTimeout"    # B

    .prologue
    .line 245
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSetChannelSearchTimeout(BB)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelTxPower(BB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "txPower"    # B

    .prologue
    .line 260
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSetChannelTxPower(BB)Z

    move-result v0

    return v0
.end method

.method public final ANTSetLowPriorityChannelSearchTimeout(BB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "searchTimeout"    # B

    .prologue
    .line 250
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSetLowPriorityChannelSearchTimeout(BB)Z

    move-result v0

    return v0
.end method

.method public final ANTSetProximitySearch(BB)Z
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "searchThreshold"    # B

    .prologue
    .line 255
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/legacy/AntManager;->ANTSetProximitySearch(BB)Z

    move-result v0

    return v0
.end method

.method public final ANTTransmitBurst(B[BIZ)I
    .locals 1
    .param p1, "channelNumber"    # B
    .param p2, "txBuffer"    # [B
    .param p3, "initialPacket"    # I
    .param p4, "containsEndOfBurst"    # Z

    .prologue
    .line 320
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dsi/ant/legacy/AntManager;->ANTTransmitBurst(B[BIZ)I

    move-result v0

    return v0
.end method

.method public final ANTTxMessage([B)Z
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 210
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTUnassignChannel(B)Z
    .locals 1
    .param p1, "channelNumber"    # B

    .prologue
    .line 220
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/AntManager;->ANTUnassignChannel(B)Z

    move-result v0

    return v0
.end method

.method public final claimInterface()Z
    .locals 1

    .prologue
    .line 335
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->claimInterface()Z

    move-result v0

    return v0
.end method

.method public final disable()Z
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->disable()Z

    move-result v0

    return v0
.end method

.method public final enable()Z
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->enable()Z

    move-result v0

    return v0
.end method

.method public final getServiceLibraryVersionCode()I
    .locals 1

    .prologue
    .line 325
    invoke-static {}, Lcom/dsi/ant/legacy/AntManager;->getServiceLibraryVersionCode()I

    move-result v0

    return v0
.end method

.method public final getServiceLibraryVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    invoke-static {}, Lcom/dsi/ant/legacy/AntManager;->getServiceLibraryVersionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hasClaimedInterface()Z
    .locals 1

    .prologue
    .line 355
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->hasClaimedInterface()Z

    move-result v0

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->isAdapterEnabled()Z

    move-result v0

    return v0
.end method

.method public final releaseInterface()Z
    .locals 1

    .prologue
    .line 350
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->releaseInterface()Z

    move-result v0

    return v0
.end method

.method public final requestForceClaimInterface(Ljava/lang/String;)Z
    .locals 1
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 340
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/ClaimManager;->requestForceClaimInterface(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final stopRequestForceClaimInterface()Z
    .locals 1

    .prologue
    .line 345
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->stopRequestForceClaimInterface()Z

    move-result v0

    return v0
.end method
