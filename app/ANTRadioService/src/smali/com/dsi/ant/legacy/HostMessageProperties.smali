.class public final Lcom/dsi/ant/legacy/HostMessageProperties;
.super Ljava/lang/Object;
.source "HostMessageProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;
    }
.end annotation


# static fields
.field private static final table:[Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    const/16 v0, 0x100

    new-array v0, v0, [Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;

    sput-object v0, Lcom/dsi/ant/legacy/HostMessageProperties;->table:[Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;

    .line 58
    const/16 v0, 0x41

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 59
    const/16 v0, 0x42

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 60
    const/16 v0, 0x51

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 61
    const/16 v0, 0x43

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 62
    const/16 v0, 0x44

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 63
    const/16 v0, 0x45

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 64
    const/16 v0, 0x46

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 65
    const/16 v0, 0x47

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 66
    const/16 v0, 0x59

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 67
    const/16 v0, 0x5a

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 68
    const/16 v0, 0x60

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 69
    const/16 v0, 0x63

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 70
    const/16 v0, 0x65

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 71
    const/16 v0, 0x66

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 72
    const/16 v0, 0x68

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 73
    const/16 v0, 0x6d

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 74
    const/16 v0, 0x6e

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 75
    const/16 v0, 0x70

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 76
    const/16 v0, 0x71

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 77
    const/16 v0, 0x75

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 78
    const/16 v0, -0x39

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 82
    const/16 v0, 0x4a

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 83
    const/16 v0, 0x4b

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 84
    const/16 v0, 0x4c

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 85
    const/16 v0, 0x5b

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 86
    const/16 v0, 0x4d

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 87
    const/16 v0, -0x3b

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 90
    const/16 v0, 0x4e

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 91
    const/16 v0, 0x4f

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 92
    const/16 v0, 0x50

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 95
    const/16 v0, 0x53

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 96
    const/16 v0, 0x48

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 99
    const/16 v0, 0x5d

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 100
    const/16 v0, 0x5e

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 101
    const/16 v0, 0x5f

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 104
    const/16 v0, 0x74

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 105
    const/16 v0, 0x72

    invoke-static {v0, v2}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 106
    const/16 v0, 0x78

    invoke-static {v0, v1}, Lcom/dsi/ant/legacy/HostMessageProperties;->set(BZ)V

    .line 107
    return-void
.end method

.method public static get(B)Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;
    .locals 2
    .param p0, "index"    # B

    .prologue
    .line 49
    sget-object v0, Lcom/dsi/ant/legacy/HostMessageProperties;->table:[Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;

    and-int/lit16 v1, p0, 0xff

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static final set(BZ)V
    .locals 3
    .param p0, "id"    # B
    .param p1, "hasResponse"    # Z

    .prologue
    .line 37
    new-instance v0, Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;-><init>()V

    .line 38
    .local v0, "entry":Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;
    iput-boolean p1, v0, Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;->hasResponse:Z

    .line 39
    sget-object v1, Lcom/dsi/ant/legacy/HostMessageProperties;->table:[Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;

    and-int/lit16 v2, p0, 0xff

    aput-object v0, v1, v2

    .line 40
    return-void
.end method
