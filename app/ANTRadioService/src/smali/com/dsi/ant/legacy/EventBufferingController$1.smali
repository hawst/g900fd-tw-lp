.class final Lcom/dsi/ant/legacy/EventBufferingController$1;
.super Landroid/content/BroadcastReceiver;
.source "EventBufferingController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/legacy/EventBufferingController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/legacy/EventBufferingController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/legacy/EventBufferingController;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 368
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 370
    .local v0, "ANTAction":Ljava/lang/String;
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # getter for: Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$000(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received screen event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 372
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # getter for: Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$100(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 374
    :try_start_0
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 376
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # getter for: Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$000(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 377
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    const/4 v4, 0x1

    # setter for: Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOff:Z
    invoke-static {v2, v4}, Lcom/dsi/ant/legacy/EventBufferingController;->access$202(Lcom/dsi/ant/legacy/EventBufferingController;Z)Z

    .line 384
    :cond_0
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # getter for: Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$300(Lcom/dsi/ant/legacy/EventBufferingController;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 388
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # invokes: Lcom/dsi/ant/legacy/EventBufferingController;->configureEventBuffering()[B
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$400(Lcom/dsi/ant/legacy/EventBufferingController;)[B

    move-result-object v1

    .line 389
    .local v1, "response":[B
    if-eqz v1, :cond_1

    const/4 v2, 0x2

    aget-byte v2, v1, v2

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1

    const/4 v2, 0x4

    aget-byte v2, v1, v2

    if-eqz v2, :cond_2

    .line 392
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # getter for: Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$000(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Problem configuring event buffering on screen event. response:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    .end local v1    # "response":[B
    :cond_2
    return-void

    .line 379
    :cond_3
    :try_start_1
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 381
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    # getter for: Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/dsi/ant/legacy/EventBufferingController;->access$000(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 382
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$1;->this$0:Lcom/dsi/ant/legacy/EventBufferingController;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOff:Z
    invoke-static {v2, v4}, Lcom/dsi/ant/legacy/EventBufferingController;->access$202(Lcom/dsi/ant/legacy/EventBufferingController;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 384
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
