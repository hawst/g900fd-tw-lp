.class public Lcom/dsi/ant/legacy/ClaimManager;
.super Ljava/lang/Object;
.source "ClaimManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mOwnsInterface:I

.field private mPidRequestingClaimInterface:I

.field private final mShowForceClaimInterfacePopup_LOCK:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/dsi/ant/legacy/ClaimManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mShowForceClaimInterfacePopup_LOCK:Ljava/lang/Object;

    .line 52
    iput v1, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 53
    iput v1, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    .line 56
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ClaimManager;->cleanClaimState()V

    .line 57
    return-void
.end method

.method private interfaceNotInUse()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 468
    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    .line 470
    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 471
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v1, "ReleaseInterface: ANT Interface Released"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->releaseAdapterClaim()V

    .line 480
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v1, "ReleaseInterface: ANT Adapter Released"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :cond_0
    return-void
.end method

.method private releaseInterface(I)Z
    .locals 8
    .param p1, "callingPid"    # I

    .prologue
    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 393
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReleaseInterface: Calling PID = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const/4 v2, 0x0

    .line 397
    .local v2, "released":Z
    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-ne v7, v3, :cond_0

    .line 399
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 461
    :goto_0
    return v2

    .line 403
    :cond_0
    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-eq v3, p1, :cond_1

    if-ne v6, p1, :cond_5

    .line 408
    :cond_1
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 410
    const/4 v2, 0x1

    .line 412
    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    if-ne v7, v3, :cond_2

    .line 416
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->interfaceNotInUse()V

    .line 453
    :goto_1
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    goto :goto_0

    .line 422
    :cond_2
    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    if-ne v6, v3, :cond_3

    .line 426
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->interfaceNotInUse()V

    .line 447
    :goto_2
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ClaimManager;->clearRequestClaimInterfaceNotification()V

    .line 450
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    goto :goto_1

    .line 432
    :cond_3
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 433
    .local v0, "callingIdentity":J
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/AntManager;->ANTResetSystem()Z

    move-result v3

    if-nez v3, :cond_4

    .line 435
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v4, "ReleaseInterface: Could not reset ANT"

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->interfaceNotInUse()V

    .line 444
    :goto_3
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_2

    .line 441
    :cond_4
    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    iput v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 442
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v4, "ReleaseInterface: ANT Interface owner switched"

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 457
    .end local v0    # "callingIdentity":J
    :cond_5
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v4, "ReleaseInterface: Do not have permission to release ANT interface"

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestForceClaimInterface(Ljava/lang/String;I)Z
    .locals 8
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "callingPid"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 178
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ForceClaimInterface: Calling PID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    if-eq v3, v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ClaimManager;->clearRequestClaimInterfaceNotification()V

    .line 183
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 187
    :cond_0
    iget v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-ne v3, v0, :cond_3

    .line 188
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/dsi/ant/legacy/AntManager;->acquireAdapterClaim(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 192
    iput p2, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 193
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    .line 194
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    .line 227
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 199
    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v0

    if-nez v0, :cond_2

    .line 201
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 204
    iput p2, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 205
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    goto :goto_0

    .line 207
    :cond_2
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->getAdapter()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v0

    if-nez v0, :cond_4

    .line 209
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 217
    :cond_3
    iget v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-ne v0, p2, :cond_4

    .line 219
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 223
    :cond_4
    iget-object v2, p0, Lcom/dsi/ant/legacy/ClaimManager;->mShowForceClaimInterfacePopup_LOCK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    iput p2, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v0, "notification"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f040002

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    new-instance v6, Landroid/app/Notification;

    const v7, 0x7f020001

    invoke-direct {v6, v7, v1, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v1, v6, Landroid/app/Notification;->defaults:I

    or-int/lit8 v1, v1, 0x17

    iput v1, v6, Landroid/app/Notification;->defaults:I

    const/4 v1, -0x2

    if-ne v1, p2, :cond_5

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f040004

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    invoke-direct {v4, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "com.dsi.ant.service.notification.forceclaim.appName"

    invoke-virtual {v4, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "com.dsi.ant.service.notification.forceclaim.pid"

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v5, 0x0

    const/high16 v7, 0x8000000

    invoke-static {v3, v5, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v6, v3, p1, v1, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    sget-object v1, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v1}, Lcom/dsi/ant/service/NotificationID;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_5
    :try_start_1
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f040003

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_1
.end method

.method private sendInterfaceClaimedIntent()V
    .locals 3

    .prologue
    .line 134
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dsi.ant.intent.action.ANT_INTERFACE_CLAIMED_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, "intentANT":Landroid/content/Intent;
    const-string v1, "com.dsi.ant.intent.ANT_INTERFACE_CLAIMED_PID"

    iget v2, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 136
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 137
    return-void
.end method


# virtual methods
.method public final claimInterface()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 141
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 143
    .local v0, "callingPid":I
    sget-object v3, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ClaimInterface: Calling PID = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, -0x1

    iget v4, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-ne v3, v4, :cond_3

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/dsi/ant/legacy/AntManager;->acquireAdapterClaim(Z)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v2, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v2, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/AntManager;->getAdapter()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v3, "ClaimInterface: adapter is null."

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v3, "ClaimInterface: adapter in use by other interface."

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-ne v3, v0, :cond_4

    sget-object v2, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v3, "ClaimInterface: Interface already claimed by another process"

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0
.end method

.method public final cleanClaimState()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 64
    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 66
    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    .line 67
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ClaimManager;->clearRequestClaimInterfaceNotification()V

    .line 68
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->releaseAdapterClaim()V

    .line 69
    return-void
.end method

.method public final clearInterfaceClaimWhenInitialized()V
    .locals 2

    .prologue
    .line 76
    iget-object v1, p0, Lcom/dsi/ant/legacy/ClaimManager;->mShowForceClaimInterfacePopup_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 82
    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    .line 85
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ClaimManager;->clearRequestClaimInterfaceNotification()V

    .line 86
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    .line 87
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final clearRequestClaimInterfaceNotification()V
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 127
    sget-object v1, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v1}, Lcom/dsi/ant/service/NotificationID;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 129
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    .line 130
    return-void
.end method

.method public final forceClaimInterface(I)V
    .locals 2
    .param p1, "callingPid"    # I

    .prologue
    const/4 v1, -0x2

    .line 332
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 334
    if-ne v1, p1, :cond_1

    .line 336
    invoke-direct {p0, v1}, Lcom/dsi/ant/legacy/ClaimManager;->releaseInterface(I)Z

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/legacy/AntManager;->acquireAdapterClaim(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 340
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 342
    iput p1, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 343
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    .line 344
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    goto :goto_0

    .line 349
    :cond_2
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v0

    if-nez v0, :cond_3

    .line 351
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 353
    iput p1, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    .line 354
    invoke-direct {p0}, Lcom/dsi/ant/legacy/ClaimManager;->sendInterfaceClaimedIntent()V

    goto :goto_0

    .line 358
    :cond_3
    sget-object v0, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    const-string v1, "ForceClaimInterface: Could not change ANT interface claim, interface not claimed."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iget v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 360
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->releaseAdapterClaim()V

    goto :goto_0
.end method

.method public final forceReleaseInterface()V
    .locals 1

    .prologue
    .line 370
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/dsi/ant/legacy/ClaimManager;->releaseInterface(I)Z

    .line 371
    return-void
.end method

.method public final hasClaimedInterface()Z
    .locals 2

    .prologue
    .line 148
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    iget v1, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInterfaceClaimed()Z
    .locals 2

    .prologue
    .line 379
    iget v0, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNotPermitted()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 99
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    iget v3, p0, Lcom/dsi/ant/legacy/ClaimManager;->mOwnsInterface:I

    if-eq v3, v2, :cond_0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    if-ne v3, v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final releaseInterface()Z
    .locals 2

    .prologue
    .line 318
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 320
    .local v0, "callingPid":I
    invoke-direct {p0, v0}, Lcom/dsi/ant/legacy/ClaimManager;->releaseInterface(I)Z

    move-result v1

    return v1
.end method

.method public final requestForceClaimInterface(Ljava/lang/String;)Z
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 163
    .local v0, "callingPid":I
    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/legacy/ClaimManager;->requestForceClaimInterface(Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public final requestForceUnClaim()Z
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/legacy/ClaimManager;->requestForceClaimInterface(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final stopRequestForceClaimInterface()Z
    .locals 5

    .prologue
    .line 295
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 297
    .local v0, "callingPid":I
    sget-object v2, Lcom/dsi/ant/legacy/ClaimManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "StopRequestForceClaimInterface: Calling PID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v1, 0x0

    .line 301
    .local v1, "result":Z
    iget v2, p0, Lcom/dsi/ant/legacy/ClaimManager;->mPidRequestingClaimInterface:I

    if-ne v0, v2, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ClaimManager;->clearRequestClaimInterfaceNotification()V

    .line 304
    const/4 v1, 0x1

    .line 307
    :cond_0
    return v1
.end method
