.class public Lcom/dsi/ant/legacy/BindingManager;
.super Ljava/lang/Object;
.source "BindingManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBinder:Lcom/dsi/ant/IAnt$Stub;

.field private final mBinder_6:Lcom/dsi/ant/IAnt_6$Stub;

.field private mIAnt6ConnectionInUse:Z

.field private mIAntConnectionInUse:Z

.field private final mNotificationResultBinder:Lcom/dsi/ant/service/INotificationResult$Stub;

.field private final mServiceSettingsBinder:Lcom/dsi/ant/IServiceSettings$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/dsi/ant/legacy/BindingManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/dsi/ant/legacy/BindingManager$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/BindingManager$1;-><init>(Lcom/dsi/ant/legacy/BindingManager;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mBinder:Lcom/dsi/ant/IAnt$Stub;

    .line 191
    new-instance v0, Lcom/dsi/ant/legacy/BindingManager$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/BindingManager$2;-><init>(Lcom/dsi/ant/legacy/BindingManager;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mBinder_6:Lcom/dsi/ant/IAnt_6$Stub;

    .line 361
    new-instance v0, Lcom/dsi/ant/legacy/BindingManager$3;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/BindingManager$3;-><init>(Lcom/dsi/ant/legacy/BindingManager;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mNotificationResultBinder:Lcom/dsi/ant/service/INotificationResult$Stub;

    .line 376
    new-instance v0, Lcom/dsi/ant/legacy/BindingManager$4;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/BindingManager$4;-><init>(Lcom/dsi/ant/legacy/BindingManager;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mServiceSettingsBinder:Lcom/dsi/ant/IServiceSettings$Stub;

    .line 36
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAntConnectionInUse:Z

    .line 37
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAnt6ConnectionInUse:Z

    .line 38
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final doBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 468
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/IAnt;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "Bind: IAnt"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mBinder:Lcom/dsi/ant/IAnt$Stub;

    .line 475
    .local v0, "binder":Landroid/os/IBinder;
    iput-boolean v3, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAntConnectionInUse:Z

    .line 501
    :goto_0
    return-object v0

    .line 477
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/IAnt_6;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 479
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "Bind: IAnt_6"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mBinder_6:Lcom/dsi/ant/IAnt_6$Stub;

    .line 482
    .restart local v0    # "binder":Landroid/os/IBinder;
    iput-boolean v3, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAnt6ConnectionInUse:Z

    goto :goto_0

    .line 484
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/IServiceSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "Bind: IServiceSettings"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mServiceSettingsBinder:Lcom/dsi/ant/IServiceSettings$Stub;

    .restart local v0    # "binder":Landroid/os/IBinder;
    goto :goto_0

    .line 490
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/service/INotificationResult;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 492
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "Bind: INotificationResult"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mNotificationResultBinder:Lcom/dsi/ant/service/INotificationResult$Stub;

    .restart local v0    # "binder":Landroid/os/IBinder;
    goto :goto_0

    .line 498
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_3
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/dsi/ant/adapter/AdapterProvider;->bindTransportSpecificIntent(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    .restart local v0    # "binder":Landroid/os/IBinder;
    goto :goto_0
.end method

.method public final doUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 506
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 508
    const/4 v0, 0x0

    .line 510
    .local v0, "antConnectionWasInUse":Z
    iget-boolean v1, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAntConnectionInUse:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAnt6ConnectionInUse:Z

    if-eqz v1, :cond_1

    .line 512
    :cond_0
    const/4 v0, 0x1

    .line 515
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/IAnt;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 517
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "onUnbind: IAnt"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    iput-boolean v3, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAntConnectionInUse:Z

    .line 536
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/dsi/ant/legacy/BindingManager;->hasApplicationBound()Z

    move-result v1

    if-nez v1, :cond_3

    .line 540
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "onUnbind: ANT HAL connection no longer in use"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/ClaimManager;->cleanClaimState()V

    .line 546
    :cond_3
    const/4 v1, 0x1

    return v1

    .line 521
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/IAnt_6;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 523
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "onUnbind: IAnt_6"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iput-boolean v3, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAnt6ConnectionInUse:Z

    goto :goto_0

    .line 527
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/IServiceSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 529
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "onUnbind: IServiceSettings"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/service/INotificationResult;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 533
    sget-object v1, Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;

    const-string v2, "onUnbind: INotificationResult"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final hasApplicationBound()Z
    .locals 1

    .prologue
    .line 550
    iget-boolean v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAntConnectionInUse:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/legacy/BindingManager;->mIAnt6ConnectionInUse:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
