.class final Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;
.super Ljava/lang/Object;
.source "EventBufferingController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/legacy/EventBufferingController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "BufferSettings"
.end annotation


# instance fields
.field final flushBufferInterval:S

.field final flushTimerInterval:S


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-short v0, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    iput-short v0, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    .line 58
    return-void
.end method

.method constructor <init>(SS)V
    .locals 0
    .param p1, "timerInterval"    # S
    .param p2, "bufferInterval"    # S

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-short p2, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    .line 68
    iput-short p1, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    .line 69
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    if-ne p0, p1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v1

    .line 90
    :cond_1
    instance-of v3, p1, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    if-nez v3, :cond_2

    move v1, v2

    .line 92
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 94
    check-cast v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 95
    .local v0, "other":Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;
    iget-short v3, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    iget-short v4, v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    if-ne v3, v4, :cond_3

    iget-short v3, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    iget-short v4, v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    .line 98
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 74
    iget-short v1, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    add-int/lit8 v0, v1, 0x1f

    .line 77
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-short v2, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    add-int/2addr v1, v2

    .line 78
    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BufferSettings [flushTimerInterval="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v1, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flushBufferInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
