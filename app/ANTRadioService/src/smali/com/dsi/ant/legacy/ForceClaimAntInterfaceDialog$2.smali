.class final Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;
.super Ljava/lang/Object;
.source "ForceClaimAntInterfaceDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;


# direct methods
.method constructor <init>(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;->this$0:Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 180
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 182
    const-string v0, "ForceClaimAntInterfaceDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "App Name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;->this$0:Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    # getter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingAppName:Ljava/lang/String;
    invoke-static {v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$400(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", PID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;->this$0:Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    # getter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I
    invoke-static {v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$500(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Force = YES"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;->this$0:Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sendResult(Z)V

    .line 185
    return-void
.end method
