.class public final Lcom/dsi/ant/legacy/AntManager;
.super Ljava/lang/Object;
.source "AntManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/legacy/AntManager$2;
    }
.end annotation


# static fields
.field private static mBurstAntPacket:[B


# instance fields
.field private mActionQueue:Landroid/os/Handler;

.field mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

.field private final mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

.field private mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

.field private final mAdapterState_LOCK:Ljava/lang/Object;

.field private mBurstBuffer:[B

.field private mCanContinueBurst:Z

.field private mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

.field private mFilterSequenceNumberErrors:Z

.field private mIgnoreRxMessages:Z

.field private mNumCombinedBurstPackets:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0xb

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    return-void

    :array_0
    .array-data 1
        0x9t
        0x50t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-object v0, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mBurstBuffer:[B

    .line 45
    const/16 v0, 0x8

    iput v0, p0, Lcom/dsi/ant/legacy/AntManager;->mNumCombinedBurstPackets:I

    .line 50
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/AntManager;->mFilterSequenceNumberErrors:Z

    .line 52
    iput-boolean v2, p0, Lcom/dsi/ant/legacy/AntManager;->mCanContinueBurst:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mActionQueue:Landroid/os/Handler;

    .line 78
    new-instance v0, Lcom/dsi/ant/legacy/AntManager$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/AntManager$1;-><init>(Lcom/dsi/ant/legacy/AntManager;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    .line 186
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/EventBufferingController;-><init>(Lcom/dsi/ant/legacy/AntManager;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    .line 188
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/AntManager;->mFilterSequenceNumberErrors:Z

    .line 189
    iput-boolean v2, p0, Lcom/dsi/ant/legacy/AntManager;->mCanContinueBurst:Z

    .line 190
    iput-boolean v2, p0, Lcom/dsi/ant/legacy/AntManager;->mIgnoreRxMessages:Z

    .line 192
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.dsi.ant.service.AntManager"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "SERIAL_DEBUG"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/util/LogAnt;->setSerialDebug(Z)V

    const-string v1, "DEBUG_LEVEL"

    sget v2, Lcom/dsi/ant/util/LogAnt;->DEBUG_LEVEL_DEFAULT:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/util/LogAnt;->setDebugLevel(I)V

    .line 193
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/legacy/AntManager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/AntManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/legacy/AntManager;)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/AntManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    return-object v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/legacy/AntManager;Lcom/dsi/ant/adapter/AntAdapterState;)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/legacy/AntManager;
    .param p1, "x1"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    return-object p1
.end method

.method private getResultWhileInitialising([B)I
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 1059
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/AntManager;->isAdapterEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1066
    const/4 v0, 0x0

    .line 1074
    .local v0, "result":I
    :goto_0
    return v0

    .line 1071
    .end local v0    # "result":I
    :cond_0
    array-length v0, p1

    .restart local v0    # "result":I
    goto :goto_0
.end method

.method private getResultWhileInitialising()Z
    .locals 2

    .prologue
    .line 1034
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/AntManager;->isAdapterEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1041
    const/4 v0, 0x1

    .line 1049
    .local v0, "result":Z
    :goto_0
    return v0

    .line 1046
    .end local v0    # "result":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "result":Z
    goto :goto_0
.end method

.method public static getServiceLibraryVersionCode()I
    .locals 1

    .prologue
    .line 1020
    const/16 v0, 0x75f8

    return v0
.end method

.method public static getServiceLibraryVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1025
    sget-object v0, Lcom/dsi/ant/Version;->ANT_LIBRARY_VERSION_NAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final ANTAddChannelId(BIBBB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "deviceNumber"    # I
    .param p3, "deviceType"    # B
    .param p4, "txType"    # B
    .param p5, "listIndex"    # B

    .prologue
    .line 728
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTAddChannelId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 730
    invoke-static {p1, p2, p3, p4, p5}, Lcom/dsi/ant/AntMessageBuilder;->getANTAddChannelId(BIBBB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTAssignChannel(BBB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "channelType"    # B
    .param p3, "networkNumber"    # B

    .prologue
    .line 665
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTAssignChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 667
    invoke-static {p1, p2, p3}, Lcom/dsi/ant/AntMessageBuilder;->getANTAssignChannel(BBB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTCloseChannel(B)Z
    .locals 2
    .param p1, "channelNumber"    # B

    .prologue
    .line 815
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTCloseChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 817
    invoke-static {p1}, Lcom/dsi/ant/AntMessageBuilder;->getANTCloseChannel(B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTConfigEventBuffering(IIII)Z
    .locals 8
    .param p1, "screenOnFlushTimerInterval"    # I
    .param p2, "screenOnFlushBufferThreshold"    # I
    .param p3, "screenOffFlushTimerInterval"    # I
    .param p4, "screenOffFlushBufferThreshold"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 742
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 744
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 773
    :cond_0
    :goto_0
    return v1

    .line 751
    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v3

    if-nez v3, :cond_2

    .line 753
    invoke-direct {p0}, Lcom/dsi/ant/legacy/AntManager;->getResultWhileInitialising()Z

    move-result v1

    goto :goto_0

    .line 759
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    new-instance v4, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    int-to-short v5, p1

    int-to-short v6, p2

    invoke-direct {v4, v5, v6}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>(SS)V

    new-instance v5, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    int-to-short v6, p3

    int-to-short v7, p4

    invoke-direct {v5, v6, v7}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>(SS)V

    invoke-virtual {v3, v4, v5}, Lcom/dsi/ant/legacy/EventBufferingController;->ANTConfigEventBuffering(Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;)[B

    move-result-object v0

    .line 764
    .local v0, "response":[B
    if-eqz v0, :cond_3

    aget-byte v3, v0, v2

    const/16 v4, 0x40

    if-ne v3, v4, :cond_3

    const/4 v3, 0x4

    aget-byte v3, v0, v3

    if-nez v3, :cond_3

    move v1, v2

    .line 768
    .local v1, "result":Z
    :cond_3
    if-nez v1, :cond_0

    .line 769
    const-string v2, "ANTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received bad response for event buffering message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final ANTConfigList(BBB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "listSize"    # B
    .param p3, "exclude"    # B

    .prologue
    .line 735
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTConfigList: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 737
    invoke-static {p1, p2, p3}, Lcom/dsi/ant/AntMessageBuilder;->getANTConfigList(BBB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTDisableEventBuffering()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 778
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 780
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 803
    :cond_0
    :goto_0
    return v1

    .line 787
    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v3

    if-nez v3, :cond_2

    .line 789
    invoke-direct {p0}, Lcom/dsi/ant/legacy/AntManager;->getResultWhileInitialising()Z

    move-result v1

    goto :goto_0

    .line 792
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/EventBufferingController;->ANTDisableEventBuffering()[B

    move-result-object v0

    .line 794
    .local v0, "response":[B
    if-eqz v0, :cond_3

    aget-byte v3, v0, v2

    const/16 v4, 0x40

    if-ne v3, v4, :cond_3

    const/4 v3, 0x4

    aget-byte v3, v0, v3

    if-nez v3, :cond_3

    move v1, v2

    .line 798
    .local v1, "result":Z
    :cond_3
    if-nez v1, :cond_0

    .line 799
    const-string v2, "ANTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received bad response for event buffering message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final ANTOpenChannel(B)Z
    .locals 2
    .param p1, "channelNumber"    # B

    .prologue
    .line 808
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTOpenChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 810
    invoke-static {p1}, Lcom/dsi/ant/AntMessageBuilder;->getANTOpenChannel(B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTRequestMessage(BB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "messageID"    # B

    .prologue
    .line 822
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTRequestMessage: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 824
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTRequestMessage(BB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTResetSystem()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 633
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 635
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 653
    :cond_0
    :goto_0
    return v0

    .line 642
    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v1

    if-nez v1, :cond_2

    .line 644
    invoke-direct {p0}, Lcom/dsi/ant/legacy/AntManager;->getResultWhileInitialising()Z

    move-result v0

    goto :goto_0

    .line 647
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v1, :cond_3

    .line 649
    const-string v1, "ANTManager"

    const-string v2, "Adapter is null!"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 653
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ANTSendAcknowledgedData(B[B)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "txBuffer"    # [B

    .prologue
    .line 836
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSendAcknowledgedData: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 838
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSendAcknowledgedData(B[B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSendBroadcastData(B[B)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "txBuffer"    # [B

    .prologue
    .line 829
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSendBroadcastData: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 831
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSendBroadcastData(B[B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSendBurstTransfer(B[B)I
    .locals 3
    .param p1, "channelNumber"    # B
    .param p2, "data"    # [B

    .prologue
    const/4 v2, 0x1

    .line 862
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 866
    :try_start_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 868
    array-length v0, p2

    .line 897
    :goto_0
    return v0

    .line 873
    :cond_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v0

    if-nez v0, :cond_1

    .line 875
    invoke-direct {p0, p2}, Lcom/dsi/ant/legacy/AntManager;->getResultWhileInitialising([B)I

    move-result v0

    goto :goto_0

    .line 878
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v0, :cond_2

    .line 880
    const-string v0, "ANTManager"

    const-string v1, "Adapter is null!"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    array-length v0, p2

    goto :goto_0

    .line 884
    :cond_2
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/adapter/AdapterHandle;->txBurst(I[B)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 886
    const/4 v0, 0x0

    goto :goto_0

    .line 890
    :cond_3
    array-length v0, p2
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 896
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 897
    invoke-virtual {p0, p1, p2, v2, v2}, Lcom/dsi/ant/legacy/AntManager;->ANTTransmitBurst(B[BIZ)I

    move-result v0

    goto :goto_0
.end method

.method public final ANTSendBurstTransferPacket(B[B)Z
    .locals 1
    .param p1, "control"    # B
    .param p2, "txBuffer"    # [B

    .prologue
    .line 843
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 845
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSendBurstTransferPacket(B[B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelId(BIBB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "deviceNumber"    # I
    .param p3, "deviceType"    # B
    .param p4, "txType"    # B

    .prologue
    .line 672
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 674
    invoke-static {p1, p2, p3, p4}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetChannelId(BIBB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelPeriod(BI)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "channelPeriod"    # I

    .prologue
    .line 679
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelPeriod: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 681
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetChannelPeriod(BI)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelRFFreq(BB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "radioFrequency"    # B

    .prologue
    .line 686
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelRFFreq: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 688
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetChannelRFFreq(BB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelSearchTimeout(BB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "searchTimeout"    # B

    .prologue
    .line 693
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelSearchTimeout: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 695
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetChannelSearchTimeout(BB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetChannelTxPower(BB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "txPower"    # B

    .prologue
    .line 714
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelTxPower: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 716
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetChannelTxPower(BB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetLowPriorityChannelSearchTimeout(BB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "searchTimeout"    # B

    .prologue
    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetLowPriorityChannelSearchTimeout: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 702
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetLowPriorityChannelSearchTimeout(BB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTSetProximitySearch(BB)Z
    .locals 2
    .param p1, "channelNumber"    # B
    .param p2, "searchThreshold"    # B

    .prologue
    .line 707
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetProximitySearch: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 709
    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetProximitySearch(BB)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final ANTTransmitBurst(B[BIZ)I
    .locals 17
    .param p1, "channelNumber"    # B
    .param p2, "txBuffer"    # [B
    .param p3, "initialPacket"    # I
    .param p4, "containsEndOfBurst"    # Z

    .prologue
    .line 912
    const-string v13, "ANTManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "ANTTransmitBurst: "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/dsi/ant/legacy/AntManager;->mCanContinueBurst:Z

    .line 916
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 918
    move-object/from16 v0, p2

    array-length v6, v0

    .line 1007
    :goto_0
    return v6

    .line 923
    :cond_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v13

    invoke-virtual {v13}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v13

    if-nez v13, :cond_1

    .line 925
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/dsi/ant/legacy/AntManager;->getResultWhileInitialising([B)I

    move-result v6

    goto :goto_0

    .line 928
    :cond_1
    move-object/from16 v0, p2

    array-length v6, v0

    .line 929
    .local v6, "bytesLeftToSend":I
    const/4 v5, 0x0

    .line 931
    .local v5, "bytesBuffered":I
    int-to-double v13, v6

    const-wide/high16 v15, 0x4020000000000000L    # 8.0

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v13

    double-to-int v2, v13

    .line 932
    .local v2, "antPacketsLeft":I
    const/16 v7, 0x8

    .line 934
    .local v7, "currentAntPacketDataBytes":I
    const/4 v8, 0x1

    .line 937
    .local v8, "doBurst":Z
    move/from16 v9, p3

    .line 938
    .local v9, "hciPacketCount":I
    const/4 v13, 0x4

    move/from16 v0, p3

    if-gt v0, v13, :cond_8

    .line 939
    add-int/lit8 v13, p3, -0x1

    int-to-byte v13, v13

    mul-int/lit8 v13, v13, 0x20

    int-to-byte v12, v13

    .line 943
    .local v12, "ucSeq":B
    :goto_1
    const-string v13, "ANTManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "ANTTransmitBurst: Sending "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " bytes, in "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ANT packets."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/dsi/ant/legacy/AntManager;->mNumCombinedBurstPackets:I

    invoke-static {v2, v13}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 949
    .local v11, "numCombinedAntPackets":I
    const/4 v10, 0x0

    .line 950
    .local v10, "internalAntPacketNumber":I
    const/4 v4, 0x0

    .line 952
    .local v4, "burstBufferOffset":I
    const-string v13, "ANTManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "ANTTransmitBurst: Combining "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ANT packets inside 1 HCI packet"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    sget-object v13, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    array-length v13, v13

    mul-int v3, v11, v13

    .line 955
    .local v3, "bufferLength":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/legacy/AntManager;->mBurstBuffer:[B

    array-length v13, v13

    if-eq v13, v3, :cond_3

    .line 958
    new-array v13, v3, [B

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/dsi/ant/legacy/AntManager;->mBurstBuffer:[B

    .line 960
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/legacy/AntManager;->mBurstBuffer:[B

    const/4 v14, 0x0

    invoke-static {v13, v14}, Ljava/util/Arrays;->fill([BB)V

    .line 964
    :cond_4
    const/16 v13, 0x8

    if-gt v6, v13, :cond_6

    .line 966
    if-eqz p4, :cond_5

    .line 968
    or-int/lit8 v13, v12, -0x80

    int-to-byte v12, v13

    .line 970
    :cond_5
    move v7, v6

    .line 971
    const/4 v8, 0x0

    .line 974
    :cond_6
    sget-object v13, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    const/4 v14, 0x2

    or-int v15, p1, v12

    int-to-byte v15, v15

    aput-byte v15, v13, v14

    .line 975
    sget-object v13, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    const/4 v14, 0x3

    move-object/from16 v0, p2

    invoke-static {v0, v5, v13, v14, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 976
    sget-object v13, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/dsi/ant/legacy/AntManager;->mBurstBuffer:[B

    sget-object v16, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v13, v14, v15, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 979
    const/16 v13, 0x60

    if-ne v12, v13, :cond_9

    .line 980
    const/16 v12, 0x20

    .line 984
    :goto_2
    sub-int/2addr v6, v7

    .line 985
    add-int/2addr v5, v7

    .line 987
    add-int/lit8 v10, v10, 0x1

    .line 988
    sget-object v13, Lcom/dsi/ant/legacy/AntManager;->mBurstAntPacket:[B

    array-length v13, v13

    add-int/2addr v4, v13

    .line 990
    if-lt v10, v11, :cond_4

    .line 992
    sub-int/2addr v2, v11

    .line 994
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "ANTTransmitBurst: Sending HCI packet "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 996
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/dsi/ant/legacy/AntManager;->mCanContinueBurst:Z

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/legacy/AntManager;->mBurstBuffer:[B

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v13

    if-nez v13, :cond_a

    .line 998
    :cond_7
    const/4 v8, 0x0

    .line 999
    const-string v13, "ANTManager"

    const-string v14, "ANTTransmitBurst: Aborting burst due to transmit error"

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    :goto_3
    if-nez v8, :cond_2

    goto/16 :goto_0

    .line 941
    .end local v3    # "bufferLength":I
    .end local v4    # "burstBufferOffset":I
    .end local v10    # "internalAntPacketNumber":I
    .end local v11    # "numCombinedAntPackets":I
    .end local v12    # "ucSeq":B
    :cond_8
    add-int/lit8 v13, p3, 0x1

    rem-int/lit8 v13, v13, 0x3

    int-to-byte v13, v13

    add-int/lit8 v13, v13, 0x1

    mul-int/lit8 v13, v13, 0x20

    int-to-byte v12, v13

    .restart local v12    # "ucSeq":B
    goto/16 :goto_1

    .line 982
    .restart local v3    # "bufferLength":I
    .restart local v4    # "burstBufferOffset":I
    .restart local v10    # "internalAntPacketNumber":I
    .restart local v11    # "numCombinedAntPackets":I
    :cond_9
    add-int/lit8 v13, v12, 0x20

    int-to-byte v12, v13

    goto :goto_2

    .line 1003
    :cond_a
    add-int/lit8 v9, v9, 0x1

    goto :goto_3
.end method

.method public final ANTTxMessage([B)Z
    .locals 7
    .param p1, "message"    # [B

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 570
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 571
    const/4 v1, 0x0

    .line 573
    .local v1, "writeSuccess":Z
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 628
    :goto_0
    return v3

    .line 580
    :cond_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v4

    if-nez v4, :cond_1

    .line 582
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempt to transmit message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " while service is initialising"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 585
    invoke-direct {p0}, Lcom/dsi/ant/legacy/AntManager;->getResultWhileInitialising()Z

    move-result v3

    goto :goto_0

    .line 588
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v4, :cond_2

    .line 590
    const-string v2, "ANTManager"

    const-string v4, "Adapter is null!"

    invoke-static {v2, v4}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 594
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 595
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v5}, Lcom/dsi/ant/adapter/AdapterHandle;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v5

    sget-object v6, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v5, v6, :cond_3

    iget-object v5, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v5}, Lcom/dsi/ant/adapter/AdapterHandle;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v5

    sget-object v6, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v5, v6, :cond_4

    .line 597
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Attempt to transmit message "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " while adapter state is INITIALIZING or ENABLING."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 603
    monitor-exit v4

    move v3, v2

    goto :goto_0

    .line 606
    :cond_4
    const/4 v5, 0x1

    aget-byte v5, p1, v5

    invoke-static {v5}, Lcom/dsi/ant/legacy/HostMessageProperties;->get(B)Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;

    move-result-object v0

    .line 609
    .local v0, "info":Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;
    if-nez v0, :cond_6

    .line 611
    const-string v2, "ANTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Transmitting unknown message id 0x"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    aget-byte v5, p1, v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :cond_5
    iget-object v2, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v2, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->txData([B)Z

    move-result v1

    .line 626
    :goto_1
    monitor-exit v4

    move v3, v1

    .line 628
    goto/16 :goto_0

    .line 616
    :cond_6
    iget-boolean v5, v0, Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;->hasResponse:Z

    if-eqz v5, :cond_5

    .line 618
    iget-object v5, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v5, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->txCommand([B)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 620
    if-eqz v5, :cond_7

    move v1, v2

    .line 621
    :goto_2
    goto :goto_1

    :cond_7
    move v1, v3

    .line 620
    goto :goto_2

    .line 626
    .end local v0    # "info":Lcom/dsi/ant/legacy/HostMessageProperties$PropertiesTableEntry;
    :catchall_0
    move-exception v2

    monitor-exit v4

    throw v2
.end method

.method public final ANTUnassignChannel(B)Z
    .locals 2
    .param p1, "channelNumber"    # B

    .prologue
    .line 658
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTUnassignChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 660
    invoke-static {p1}, Lcom/dsi/ant/AntMessageBuilder;->getANTUnassignChannel(B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/AntManager;->ANTTxMessage([B)Z

    move-result v0

    return v0
.end method

.method public final acquireAdapterClaim(Z)Z
    .locals 4
    .param p1, "force"    # Z

    .prologue
    const/4 v0, 0x0

    .line 373
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 374
    iget-object v2, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 375
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v1, :cond_0

    .line 376
    const-string v1, "ANTManager"

    const-string v3, "No adapter to upgrade claim on."

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    monitor-exit v2

    .line 400
    :goto_0
    return v0

    .line 384
    :cond_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/ClaimManager;->isInterfaceClaimed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 401
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 390
    :cond_1
    if-eqz p1, :cond_3

    .line 391
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->forceClaimAdapter()Z

    move-result v0

    .line 396
    .local v0, "result":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 397
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/EventBufferingController;->start()V

    .line 398
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/EventBufferingController;->ANTSetDefaultEventBuffering()[B

    .line 400
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 393
    .end local v0    # "result":Z
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->strongClaimAdapter()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .restart local v0    # "result":Z
    goto :goto_1
.end method

.method final applyANTEventBufferingSettings(SS)[B
    .locals 2
    .param p1, "flushTimerInterval"    # S
    .param p2, "flushBufferInterval"    # S

    .prologue
    .line 196
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 198
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-static {p1, p2}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetEventBuffering(SS)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/adapter/AdapterHandle;->txCommand([B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/EventBufferingController;->stop()V

    .line 1013
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    .line 1015
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/AntManager;->mIgnoreRxMessages:Z

    .line 1016
    return-void
.end method

.method public final disable()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 517
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 519
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 565
    :cond_0
    :goto_0
    return v1

    .line 532
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 538
    :try_start_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v4, :cond_3

    .line 540
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v4, :cond_2

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v4, :cond_2

    .line 545
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-interface {v1, v4}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 546
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-interface {v1, v4}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 548
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    goto :goto_0

    .line 550
    :cond_3
    monitor-exit v3

    .line 553
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v3, :cond_4

    .line 555
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    .line 558
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eqz v3, :cond_6

    .line 560
    invoke-static {}, Lcom/dsi/ant/power/PowerManager;->disable()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .line 561
    .local v0, "result":Lcom/dsi/ant/adapter/AntAdapterState;
    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v3, :cond_0

    :cond_5
    move v1, v2

    goto :goto_0

    .line 550
    .end local v0    # "result":Lcom/dsi/ant/adapter/AntAdapterState;
    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1

    .line 564
    :cond_6
    const-string v2, "ANTManager"

    const-string v3, "disable(): No Default Adapter."

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final enable()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 451
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 453
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/ClaimManager;->isNotPermitted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 506
    :cond_0
    :goto_0
    return v1

    .line 465
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 469
    :try_start_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isServiceInitialised()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v4, :cond_3

    .line 471
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v4, :cond_2

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v4, :cond_2

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v4, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v4, :cond_2

    .line 480
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-interface {v1, v4}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 482
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-interface {v1, v4}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 484
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-interface {v1, v4}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 486
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    goto :goto_0

    .line 488
    :cond_3
    monitor-exit v3

    .line 491
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v3, :cond_4

    .line 493
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    .line 496
    :cond_4
    invoke-static {}, Lcom/dsi/ant/power/PowerManager;->canEnable()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 498
    const-string v3, "ANTManager"

    const-string v4, "enable(): enable the default adapter."

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-static {}, Lcom/dsi/ant/power/PowerManager;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .line 500
    .local v0, "result":Lcom/dsi/ant/adapter/AntAdapterState;
    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v3, :cond_0

    :cond_5
    move v1, v2

    goto :goto_0

    .line 488
    .end local v0    # "result":Lcom/dsi/ant/adapter/AntAdapterState;
    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1

    .line 505
    :cond_6
    const-string v2, "ANTManager"

    const-string v3, "enable(): No Default Adapter."

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final getAdapter()Lcom/dsi/ant/adapter/AdapterHandle;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    return-object v0
.end method

.method public final getNumCombinedBurstPackets()I
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Lcom/dsi/ant/legacy/AntManager;->mNumCombinedBurstPackets:I

    return v0
.end method

.method public final isAdapterEnabled()Z
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 438
    .local v0, "currentState":Lcom/dsi/ant/adapter/AntAdapterState;
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v0, :cond_0

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v1, v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final releaseAdapterClaim()V
    .locals 3

    .prologue
    .line 348
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 349
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 350
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/dsi/ant/adapter/AdapterHandle;->releaseClaim(Z)V

    .line 353
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager;->mEventBufferingController:Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/EventBufferingController;->stop()V

    .line 355
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final sendAntMessage([B)V
    .locals 10
    .param p1, "message"    # [B

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 258
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 260
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 262
    .local v0, "context":Landroid/content/Context;
    iget-boolean v4, p0, Lcom/dsi/ant/legacy/AntManager;->mIgnoreRxMessages:Z

    if-eqz v4, :cond_0

    .line 264
    const-string v4, "ANTManager"

    const-string v5, "After being destroyed, a message was received and ignored"

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :goto_0
    return-void

    .line 269
    :cond_0
    if-eqz p1, :cond_1

    array-length v4, p1

    if-lez v4, :cond_1

    aget-byte v4, p1, v7

    if-nez v4, :cond_2

    .line 271
    :cond_1
    const-string v4, "ANTManager"

    const-string v5, "sendAntMessage: received an empty message, ignoring message."

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_2
    aget-byte v4, p1, v7

    array-length v5, p1

    add-int/lit8 v5, v5, -0x2

    if-eq v4, v5, :cond_3

    .line 277
    const-string v4, "ANTManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sendAntMessage: received message size("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v6, p1

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") does not match message size byte("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-byte v6, p1, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "). Dropping message"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    :cond_3
    aget-byte v4, p1, v6

    const/16 v5, 0x40

    if-ne v4, v5, :cond_6

    .line 287
    aget-byte v4, p1, v8

    if-ne v4, v6, :cond_5

    .line 289
    aget-byte v4, p1, v9

    packed-switch v4, :pswitch_data_0

    .line 335
    :cond_4
    :goto_1
    :pswitch_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.dsi.ant.intent.action.ANT_RX_MESSAGE_ACTION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 336
    .local v3, "intentANTMesg":Landroid/content/Intent;
    const-string v4, "com.dsi.ant.intent.ANT_MESSAGE"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 337
    const-string v4, "com.dsi.ant.rx.intent.STATUS"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 338
    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 340
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    goto :goto_0

    .line 295
    .end local v3    # "intentANTMesg":Landroid/content/Intent;
    :pswitch_1
    iput-boolean v7, p0, Lcom/dsi/ant/legacy/AntManager;->mFilterSequenceNumberErrors:Z

    goto :goto_1

    .line 299
    :pswitch_2
    iput-boolean v6, p0, Lcom/dsi/ant/legacy/AntManager;->mFilterSequenceNumberErrors:Z

    .line 300
    iput-boolean v7, p0, Lcom/dsi/ant/legacy/AntManager;->mCanContinueBurst:Z

    goto :goto_1

    .line 304
    :cond_5
    aget-byte v4, p1, v8

    const/16 v5, 0x50

    if-ne v4, v5, :cond_4

    .line 307
    iput-boolean v6, p0, Lcom/dsi/ant/legacy/AntManager;->mCanContinueBurst:Z

    .line 310
    aget-byte v4, p1, v9

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 312
    iget-boolean v4, p0, Lcom/dsi/ant/legacy/AntManager;->mFilterSequenceNumberErrors:Z

    if-eqz v4, :cond_4

    .line 314
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    goto/16 :goto_0

    .line 320
    :cond_6
    aget-byte v4, p1, v6

    const/16 v5, 0x6f

    if-ne v4, v5, :cond_4

    .line 322
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 324
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 325
    .local v1, "enabledState":Lcom/dsi/ant/adapter/AntAdapterState;
    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v4, v1, :cond_4

    .line 328
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getBindingManager()Lcom/dsi/ant/legacy/BindingManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/legacy/BindingManager;->hasApplicationBound()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 329
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.dsi.ant.intent.action.ANT_RESET"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    .local v2, "intentANT":Landroid/content/Intent;
    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final setDefaultAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V
    .locals 4
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 221
    const-string v1, "ANTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Using adapter: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v2, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 225
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eqz v1, :cond_0

    .line 226
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 229
    :cond_0
    if-eqz p1, :cond_3

    .line 230
    iput-object p1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 231
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    invoke-virtual {v1, v3}, Lcom/dsi/ant/adapter/AdapterHandle;->assignAdapterReceiver(Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;)V

    .line 234
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/ClaimManager;->isInterfaceClaimed()Z

    move-result v1

    .line 235
    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dsi/ant/legacy/AntManager;->acquireAdapterClaim(Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 238
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/legacy/ClaimManager;->forceReleaseInterface()V

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapter:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .line 242
    .local v0, "tempState":Lcom/dsi/ant/adapter/AntAdapterState;
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    invoke-interface {v1, v0}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 254
    .end local v0    # "tempState":Lcom/dsi/ant/adapter/AntAdapterState;
    :cond_2
    :goto_0
    monitor-exit v2

    return-void

    .line 249
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v3, :cond_2

    .line 251
    iget-object v1, p0, Lcom/dsi/ant/legacy/AntManager;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-interface {v1, v3}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final setNumCombinedBurstPackets(I)Z
    .locals 1
    .param p1, "numPackets"    # I

    .prologue
    .line 406
    const/4 v0, 0x0

    .line 408
    .local v0, "result":Z
    if-lez p1, :cond_0

    .line 410
    iput p1, p0, Lcom/dsi/ant/legacy/AntManager;->mNumCombinedBurstPackets:I

    .line 411
    const/4 v0, 0x1

    .line 414
    :cond_0
    return v0
.end method
