.class Lcom/dsi/ant/legacy/EventBufferingController;
.super Ljava/lang/Object;
.source "EventBufferingController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mAntManager:Lcom/dsi/ant/legacy/AntManager;

.field private final mEventBufferingConfig_LOCK:Ljava/lang/Object;

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private mScreenOff:Z

.field private mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

.field private final mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

.field private mStarted:Z

.field private mTriggerUpdateOnScreenEvent:Z


# direct methods
.method constructor <init>(Lcom/dsi/ant/legacy/AntManager;)V
    .locals 2
    .param p1, "antManager"    # Lcom/dsi/ant/legacy/AntManager;

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mStarted:Z

    .line 140
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    .line 363
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/legacy/EventBufferingController$1;-><init>(Lcom/dsi/ant/legacy/EventBufferingController;)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 153
    const-class v0, Lcom/dsi/ant/legacy/EventBufferingController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    .line 155
    invoke-direct {p0}, Lcom/dsi/ant/legacy/EventBufferingController;->clearInternalState()V

    .line 157
    iput-object p1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mAntManager:Lcom/dsi/ant/legacy/AntManager;

    .line 158
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mIntentFilter:Landroid/content/IntentFilter;

    .line 159
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/EventBufferingController;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/legacy/EventBufferingController;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/EventBufferingController;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/dsi/ant/legacy/EventBufferingController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/legacy/EventBufferingController;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOff:Z

    return p1
.end method

.method static synthetic access$300(Lcom/dsi/ant/legacy/EventBufferingController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/EventBufferingController;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z

    return v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/legacy/EventBufferingController;)[B
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/EventBufferingController;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dsi/ant/legacy/EventBufferingController;->configureEventBuffering()[B

    move-result-object v0

    return-object v0
.end method

.method private applyEventBufferingSettings(Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;)[B
    .locals 4
    .param p1, "bufferSettings"    # Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .prologue
    .line 322
    iget-object v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 324
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mStarted:Z

    if-nez v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Attempted to use the event buffering controller while it was stopped."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const/4 v0, 0x0

    monitor-exit v1

    .line 333
    :goto_0
    return-object v0

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Applying buffer settings: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 333
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mAntManager:Lcom/dsi/ant/legacy/AntManager;

    iget-short v2, p1, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushTimerInterval:S

    iget-short v3, p1, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->flushBufferInterval:S

    invoke-virtual {v0, v2, v3}, Lcom/dsi/ant/legacy/AntManager;->applyANTEventBufferingSettings(SS)[B

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private clearInternalState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOff:Z

    .line 217
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z

    .line 219
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-direct {v0, v1, v1}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>(SS)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 221
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-direct {v0, v1, v1}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>(SS)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 223
    return-void
.end method

.method private configureEventBuffering()[B
    .locals 3

    .prologue
    .line 294
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 296
    iget-object v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 298
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOff:Z

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Updating flush interval/threshold [Screen Off]"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-direct {p0, v0}, Lcom/dsi/ant/legacy/EventBufferingController;->applyEventBufferingSettings(Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;)[B

    move-result-object v0

    monitor-exit v1

    .line 308
    :goto_0
    return-object v0

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Updating flush interval/threshold [Screen On]"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-direct {p0, v0}, Lcom/dsi/ant/legacy/EventBufferingController;->applyEventBufferingSettings(Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;)[B

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method final ANTConfigEventBuffering(Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;)[B
    .locals 3
    .param p1, "screenOnSettings"    # Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;
    .param p2, "screenOffSettings"    # Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .prologue
    .line 259
    iget-object v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 261
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Changing event buffering settings."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Screen off settings: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nScreen on settings: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 265
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 268
    :cond_0
    iput-object p2, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 269
    iput-object p1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 271
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z

    .line 282
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/dsi/ant/legacy/EventBufferingController;->configureEventBuffering()[B

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 278
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final ANTDisableEventBuffering()[B
    .locals 3

    .prologue
    .line 347
    iget-object v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Disabling all buffering."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 352
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z

    .line 356
    invoke-direct {p0}, Lcom/dsi/ant/legacy/EventBufferingController;->configureEventBuffering()[B

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ANTSetDefaultEventBuffering()[B
    .locals 4

    .prologue
    .line 232
    iget-object v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Setting default buffer settings"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>(SS)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 238
    new-instance v0, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;-><init>(SS)V

    iput-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOffSettings:Lcom/dsi/ant/legacy/EventBufferingController$BufferSettings;

    .line 241
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mTriggerUpdateOnScreenEvent:Z

    .line 243
    invoke-direct {p0}, Lcom/dsi/ant/legacy/EventBufferingController;->configureEventBuffering()[B

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final start()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 169
    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 171
    :try_start_0
    iget-boolean v3, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mStarted:Z

    if-nez v3, :cond_0

    .line 173
    iget-object v3, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v4, "Starting"

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mStarted:Z

    .line 176
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    iget-object v5, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 178
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 180
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOff:Z

    .line 182
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 180
    .restart local v0    # "pm":Landroid/os/PowerManager;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 182
    .end local v0    # "pm":Landroid/os/PowerManager;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final stop()V
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mEventBufferingConfig_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 193
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mStarted:Z

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->TAG:Ljava/lang/String;

    const-string v2, "Stopping"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :try_start_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/legacy/EventBufferingController;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :goto_0
    :try_start_2
    invoke-direct {p0}, Lcom/dsi/ant/legacy/EventBufferingController;->clearInternalState()V

    .line 207
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
