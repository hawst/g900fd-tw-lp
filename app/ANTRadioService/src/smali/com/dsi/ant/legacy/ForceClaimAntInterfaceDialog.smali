.class public Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;
.super Landroid/app/Activity;
.source "ForceClaimAntInterfaceDialog.java"


# static fields
.field private static mButtonNo:Landroid/widget/Button;

.field private static mButtonYes:Landroid/widget/Button;

.field private static mNotificationManager:Landroid/app/NotificationManager;

.field private static mResultSent:Z

.field private static sINotificationResultConnection:Landroid/content/ServiceConnection;

.field private static sNotificationResultConnected:Z

.field private static sNotificationResultReceiver:Lcom/dsi/ant/service/INotificationResult;


# instance fields
.field private mRequestingAppName:Ljava/lang/String;

.field private mRequestingPid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultReceiver:Lcom/dsi/ant/service/INotificationResult;

    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z

    .line 59
    new-instance v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$1;

    invoke-direct {v0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sINotificationResultConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/dsi/ant/service/INotificationResult;)Lcom/dsi/ant/service/INotificationResult;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/service/INotificationResult;

    .prologue
    .line 31
    sput-object p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultReceiver:Lcom/dsi/ant/service/INotificationResult;

    return-object p0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 31
    sput-boolean p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z

    return p0
.end method

.method static synthetic access$200()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonYes:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonNo:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingAppName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;

    .prologue
    .line 31
    iget v0, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 142
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 144
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 146
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->setContentView(I)V

    .line 148
    const v2, 0x7f050001

    invoke-virtual {p0, v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    sput-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonYes:Landroid/widget/Button;

    .line 149
    const v2, 0x7f050002

    invoke-virtual {p0, v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    sput-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonNo:Landroid/widget/Button;

    .line 151
    sget-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonYes:Landroid/widget/Button;

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 152
    sget-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonNo:Landroid/widget/Button;

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 154
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    sget-boolean v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z

    if-nez v2, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/dsi/ant/service/INotificationResult;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sINotificationResultConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    const-string v3, "ForceClaimAntInterfaceDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sIAntConnection onServiceConnected: Bound with ANT Service Settings: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :goto_0
    sput-boolean v7, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mResultSent:Z

    .line 157
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingAppName:Ljava/lang/String;

    .line 158
    iput v6, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    .line 160
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    sput-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mNotificationManager:Landroid/app/NotificationManager;

    .line 162
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 163
    .local v1, "startIntent":Landroid/content/Intent;
    const-string v2, "com.dsi.ant.service.notification.forceclaim.appName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingAppName:Ljava/lang/String;

    .line 164
    const-string v2, "com.dsi.ant.service.notification.forceclaim.pid"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    .line 166
    iget v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    if-eq v6, v2, :cond_2

    .line 168
    const/high16 v2, 0x7f050000

    invoke-virtual {p0, v2}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    .local v0, "notificationText":Landroid/widget/TextView;
    iget v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_1

    .line 171
    const v2, 0x7f040006

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 176
    :goto_1
    sget-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonYes:Landroid/widget/Button;

    new-instance v3, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;

    invoke-direct {v3, p0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$2;-><init>(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    sget-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonNo:Landroid/widget/Button;

    new-instance v3, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$3;

    invoke-direct {v3, p0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$3;-><init>(Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    .end local v0    # "notificationText":Landroid/widget/TextView;
    :goto_2
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 207
    return-void

    .line 154
    .end local v1    # "startIntent":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 173
    .restart local v0    # "notificationText":Landroid/widget/TextView;
    .restart local v1    # "startIntent":Landroid/content/Intent;
    :cond_1
    const v2, 0x7f040005

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 202
    .end local v0    # "notificationText":Landroid/widget/TextView;
    :cond_2
    sget-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v3, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v3}, Lcom/dsi/ant/service/NotificationID;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 203
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->finish()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 239
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 241
    sget-boolean v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mResultSent:Z

    if-nez v0, :cond_0

    .line 243
    invoke-virtual {p0, v1}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sendResult(Z)V

    .line 245
    :cond_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    sget-boolean v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sINotificationResultConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->unbindService(Landroid/content/ServiceConnection;)V

    sput-boolean v1, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z

    :cond_1
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 247
    const/4 v0, 0x0

    sput-object v0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mNotificationManager:Landroid/app/NotificationManager;

    .line 249
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 251
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 252
    return-void
.end method

.method final sendResult(Z)V
    .locals 4
    .param p1, "agreed"    # Z

    .prologue
    .line 213
    const/4 v1, -0x1

    iget v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    if-eq v1, v2, :cond_1

    .line 217
    :try_start_0
    sget-boolean v1, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z

    if-eqz v1, :cond_0

    .line 219
    sget-object v1, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultReceiver:Lcom/dsi/ant/service/INotificationResult;

    iget v2, p0, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mRequestingPid:I

    invoke-interface {v1, p1, v2}, Lcom/dsi/ant/service/INotificationResult;->forceClaimUserResponse(ZI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :cond_0
    sget-object v1, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v2, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v2}, Lcom/dsi/ant/service/NotificationID;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 230
    :goto_0
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->finish()V

    .line 233
    const/4 v1, 0x1

    sput-boolean v1, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mResultSent:Z

    .line 235
    :cond_1
    return-void

    .line 222
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v1, "ForceClaimAntInterfaceDialog"

    const-string v2, "Error sending selection to ANT Radio Service"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v1, "ForceClaimAntInterfaceDialog"

    const-string v2, "sendResult: Error communicating with service"

    invoke-static {v1, v2, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    sget-object v1, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v2, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v2}, Lcom/dsi/ant/service/NotificationID;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v3, Lcom/dsi/ant/service/NotificationID;->RequestForceClaimWholeChipInterface:Lcom/dsi/ant/service/NotificationID;

    invoke-virtual {v3}, Lcom/dsi/ant/service/NotificationID;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 230
    invoke-virtual {p0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->finish()V

    .line 229
    throw v1
.end method
