.class final Lcom/dsi/ant/legacy/AntManager$1;
.super Ljava/lang/Object;
.source "AntManager.java"

# interfaces
.implements Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/legacy/AntManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/legacy/AntManager;


# direct methods
.method constructor <init>(Lcom/dsi/ant/legacy/AntManager;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClaimLost()V
    .locals 2

    .prologue
    .line 179
    const-string v0, "ANTManager"

    const-string v1, "Lost claim on default adapter."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->forceReleaseInterface()V

    .line 181
    return-void
.end method

.method public final onRxData([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 174
    iget-object v0, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/AntManager;->sendAntMessage([B)V

    .line 175
    return-void
.end method

.method public final onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 7
    .param p1, "newAdapterState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 82
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 84
    .local v0, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    # getter for: Lcom/dsi/ant/legacy/AntManager;->mAdapterState_LOCK:Ljava/lang/Object;
    invoke-static {v3}, Lcom/dsi/ant/legacy/AntManager;->access$000(Lcom/dsi/ant/legacy/AntManager;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 92
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    # getter for: Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;
    invoke-static {v3}, Lcom/dsi/ant/legacy/AntManager;->access$100(Lcom/dsi/ant/legacy/AntManager;)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v3

    sget-object v5, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v3, v5, :cond_0

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne p1, v3, :cond_0

    .line 94
    monitor-exit v4

    .line 169
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    # getter for: Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;
    invoke-static {v3}, Lcom/dsi/ant/legacy/AntManager;->access$100(Lcom/dsi/ant/legacy/AntManager;)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v2

    .line 106
    .local v2, "previousState":Lcom/dsi/ant/adapter/AntAdapterState;
    iget-object v3, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    # setter for: Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;
    invoke-static {v3, p1}, Lcom/dsi/ant/legacy/AntManager;->access$102(Lcom/dsi/ant/legacy/AntManager;Lcom/dsi/ant/adapter/AntAdapterState;)Lcom/dsi/ant/adapter/AntAdapterState;

    .line 108
    const-string v3, "ANTManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Adapter state changed to "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    # getter for: Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;
    invoke-static {v6}, Lcom/dsi/ant/legacy/AntManager;->access$100(Lcom/dsi/ant/legacy/AntManager;)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getBindingManager()Lcom/dsi/ant/legacy/BindingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/legacy/BindingManager;->hasApplicationBound()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    sget-object v3, Lcom/dsi/ant/legacy/AntManager$2;->$SwitchMap$com$dsi$ant$adapter$AntAdapterState:[I

    iget-object v5, p0, Lcom/dsi/ant/legacy/AntManager$1;->this$0:Lcom/dsi/ant/legacy/AntManager;

    # getter for: Lcom/dsi/ant/legacy/AntManager;->mAdapterStateSentToApp:Lcom/dsi/ant/adapter/AntAdapterState;
    invoke-static {v5}, Lcom/dsi/ant/legacy/AntManager;->access$100(Lcom/dsi/ant/legacy/AntManager;)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/adapter/AntAdapterState;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 169
    :cond_1
    :goto_1
    :pswitch_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .end local v2    # "previousState":Lcom/dsi/ant/adapter/AntAdapterState;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 116
    .restart local v2    # "previousState":Lcom/dsi/ant/adapter/AntAdapterState;
    :pswitch_1
    :try_start_1
    const-string v3, "ANTManager"

    const-string v5, "Adapter is in error state."

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v2, v3, :cond_1

    .line 119
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.action.ANT_DISABLED"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 120
    .local v1, "intentANT":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 126
    .end local v1    # "intentANT":Landroid/content/Intent;
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.action.ANT_DISABLED"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 127
    .restart local v1    # "intentANT":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 132
    .end local v1    # "intentANT":Landroid/content/Intent;
    :pswitch_3
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.action.ANT_DISABLING"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    .restart local v1    # "intentANT":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 138
    .end local v1    # "intentANT":Landroid/content/Intent;
    :pswitch_4
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.action.ANT_ENABLING"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 139
    .restart local v1    # "intentANT":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 149
    .end local v1    # "intentANT":Landroid/content/Intent;
    :pswitch_5
    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v2, v3, :cond_1

    .line 151
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.action.ANT_ENABLED"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    .restart local v1    # "intentANT":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 165
    .end local v1    # "intentANT":Landroid/content/Intent;
    :pswitch_6
    const-string v3, "ANTManager"

    const-string v5, "Invalid state changed value"

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
