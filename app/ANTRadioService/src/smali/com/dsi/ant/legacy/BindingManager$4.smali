.class final Lcom/dsi/ant/legacy/BindingManager$4;
.super Lcom/dsi/ant/IServiceSettings$Stub;
.source "BindingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/legacy/BindingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/legacy/BindingManager;


# direct methods
.method constructor <init>(Lcom/dsi/ant/legacy/BindingManager;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/dsi/ant/legacy/BindingManager$4;->this$0:Lcom/dsi/ant/legacy/BindingManager;

    invoke-direct {p0}, Lcom/dsi/ant/IServiceSettings$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDebugLevel()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getDebugLevel()I

    move-result v0

    return v0
.end method

.method public final getNumCombinedBurstPackets()I
    .locals 1

    .prologue
    .line 385
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/AntManager;->getNumCombinedBurstPackets()I

    move-result v0

    return v0
.end method

.method public final getSerialLogging()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 411
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v0

    return v0
.end method

.method public final resetAnt()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 434
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider;->triggerResetForBuiltInChip()Z

    move-result v0

    return v0
.end method

.method public final setDebugLevel(I)V
    .locals 3
    .param p1, "debugLevel"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 417
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getDebugLevel()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 419
    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->setDebugLevel(I)V

    .line 420
    # getter for: Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/legacy/BindingManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Debug logging level set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.dsi.ant.service.AntManager"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "DEBUG_LEVEL"

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getDebugLevel()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 424
    :cond_0
    return-void
.end method

.method public final setNumCombinedBurstPackets(I)Z
    .locals 1
    .param p1, "numPackets"    # I

    .prologue
    .line 380
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/legacy/AntManager;->setNumCombinedBurstPackets(I)Z

    move-result v0

    return v0
.end method

.method public final setSerialLogging(Z)V
    .locals 3
    .param p1, "doLogging"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 391
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 393
    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->setSerialDebug(Z)V

    .line 394
    if-eqz p1, :cond_1

    .line 396
    # getter for: Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/legacy/BindingManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Serial logging enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :goto_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.dsi.ant.service.AntManager"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SERIAL_DEBUG"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 406
    :cond_0
    return-void

    .line 400
    :cond_1
    # getter for: Lcom/dsi/ant/legacy/BindingManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/legacy/BindingManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Serial logging disabled."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
