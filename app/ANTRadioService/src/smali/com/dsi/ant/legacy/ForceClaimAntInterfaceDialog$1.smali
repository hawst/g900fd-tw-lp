.class final Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog$1;
.super Ljava/lang/Object;
.source "ForceClaimAntInterfaceDialog.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "pClassName"    # Landroid/content/ComponentName;
    .param p2, "pService"    # Landroid/os/IBinder;

    .prologue
    const/4 v1, 0x1

    .line 68
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 70
    invoke-static {p2}, Lcom/dsi/ant/service/INotificationResult$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/service/INotificationResult;

    move-result-object v0

    # setter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultReceiver:Lcom/dsi/ant/service/INotificationResult;
    invoke-static {v0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$002(Lcom/dsi/ant/service/INotificationResult;)Lcom/dsi/ant/service/INotificationResult;

    .line 71
    # setter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z
    invoke-static {v1}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$102(Z)Z

    .line 73
    # getter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonYes:Landroid/widget/Button;
    invoke-static {}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$200()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 74
    # getter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonNo:Landroid/widget/Button;
    invoke-static {}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$300()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 75
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "pClassName"    # Landroid/content/ComponentName;

    .prologue
    .line 81
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 83
    const/4 v0, 0x0

    # setter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultReceiver:Lcom/dsi/ant/service/INotificationResult;
    invoke-static {v0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$002(Lcom/dsi/ant/service/INotificationResult;)Lcom/dsi/ant/service/INotificationResult;

    .line 84
    const/4 v0, 0x0

    # setter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->sNotificationResultConnected:Z
    invoke-static {v0}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$102(Z)Z

    .line 86
    # getter for: Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->mButtonNo:Landroid/widget/Button;
    invoke-static {}, Lcom/dsi/ant/legacy/ForceClaimAntInterfaceDialog;->access$300()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    .line 87
    return-void
.end method
