.class public final enum Lcom/dsi/ant/channel/ChannelNotAvailableReason;
.super Ljava/lang/Enum;
.source "ChannelNotAvailableReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/ChannelNotAvailableReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum ALL_CHANNELS_IN_USE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum ALL_CHANNELS_IN_USE_LEGACY:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum ANT_DISABLED_AIRPLANE_MODE_ON:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum NETWORK_NOT_AVAILABLE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum NO_ADAPTERS_EXIST:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum NO_CHANNELS_MATCH_CRITERIA:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum RELEASE_PROCESSING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum SERVICE_INITIALIZING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field public static final enum UNKNOWN:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

.field private static final sValues:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 22
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->UNKNOWN:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 33
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "SERVICE_INITIALIZING"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->SERVICE_INITIALIZING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 52
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "NO_ADAPTERS_EXIST"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_ADAPTERS_EXIST:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 63
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "ANT_DISABLED_AIRPLANE_MODE_ON"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ANT_DISABLED_AIRPLANE_MODE_ON:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 73
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "ALL_CHANNELS_IN_USE_LEGACY"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE_LEGACY:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 81
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "ALL_CHANNELS_IN_USE"

    const/4 v2, 0x5

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 87
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "NO_CHANNELS_MATCH_CRITERIA"

    const/4 v2, 0x6

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_CHANNELS_MATCH_CRITERIA:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 95
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "RELEASE_PROCESSING"

    const/4 v2, 0x7

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->RELEASE_PROCESSING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 104
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    const-string v1, "NETWORK_NOT_AVAILABLE"

    const/16 v2, 0x8

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NETWORK_NOT_AVAILABLE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->UNKNOWN:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->SERVICE_INITIALIZING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_ADAPTERS_EXIST:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ANT_DISABLED_AIRPLANE_MODE_ON:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE_LEGACY:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_CHANNELS_MATCH_CRITERIA:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->RELEASE_PROCESSING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NETWORK_NOT_AVAILABLE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->$VALUES:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 110
    invoke-static {}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->values()[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->sValues:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 131
    sget-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->UNKNOWN:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 133
    .local v0, "code":Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->sValues:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 134
    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->sValues:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 135
    sget-object v2, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->sValues:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    aget-object v0, v2, v1

    .line 140
    :cond_0
    return-object v0

    .line 134
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 133
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->$VALUES:[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/ChannelNotAvailableReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    return-object v0
.end method


# virtual methods
.method public final getRawValue()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->mRawValue:I

    return v0
.end method
