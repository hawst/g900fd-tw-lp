.class public final Lcom/dsi/ant/channel/ChannelsController;
.super Ljava/lang/Object;
.source "ChannelsController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ChannelsController$1;,
        Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;,
        Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;,
        Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

.field private mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

.field private mBackgroundScanStateChange_Lock:Ljava/lang/Object;

.field private final mBufferVotes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/dsi/ant/channel/EventBufferSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final mCapabilities:Lcom/dsi/ant/channel/Capabilities;

.field private final mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

.field private mChannelShutdownPool:Ljava/util/concurrent/ExecutorService;

.field private mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

.field private mEventBufferChange_Lock:Ljava/lang/Object;

.field private mEventBufferSettings:Lcom/dsi/ant/channel/EventBufferSettings;

.field private mHasUseOfAdapter:Z

.field private mLibConfig:Lcom/dsi/ant/message/LibConfig;

.field private mLibConfigChange_Lock:Ljava/lang/Object;

.field private mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

.field private mNetworkController:Lcom/dsi/ant/channel/NetworkController;

.field private mTransferController:Lcom/dsi/ant/channel/TransferController;

.field public final versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;)V
    .locals 6
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "channelListener"    # Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mBufferVotes:Ljava/util/HashMap;

    .line 315
    iput-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .line 320
    iput-boolean v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    .line 325
    iput-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelShutdownPool:Ljava/util/concurrent/ExecutorService;

    .line 334
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigChange_Lock:Ljava/lang/Object;

    .line 335
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferChange_Lock:Ljava/lang/Object;

    .line 336
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanStateChange_Lock:Ljava/lang/Object;

    .line 347
    iput-object p2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    .line 348
    iput-object p1, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    .line 352
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelShutdownPool:Ljava/util/concurrent/ExecutorService;

    .line 354
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterHandle;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v2

    if-nez v2, :cond_0

    .line 355
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The passed adapter was not fully initialized."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 358
    :cond_0
    new-instance v2, Lcom/dsi/ant/message/LibConfig;

    invoke-direct {v2}, Lcom/dsi/ant/message/LibConfig;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    .line 359
    new-instance v2, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-direct {v2}, Lcom/dsi/ant/channel/EventBufferSettings;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferSettings:Lcom/dsi/ant/channel/EventBufferSettings;

    .line 361
    new-instance v2, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/AdapterHandle;->getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;-><init>(Lcom/dsi/ant/message/fromant/AntVersionMessage;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    .line 362
    new-instance v2, Lcom/dsi/ant/channel/Capabilities;

    invoke-direct {v2}, Lcom/dsi/ant/channel/Capabilities;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    .line 363
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasRxMessageTimestamp()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->supportRxMessageTimestamp(Z)V

    .line 364
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasTimeFlushedEventBuffering()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->supportEventBuffering(Z)V

    .line 365
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasBackgroundScanning()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->supportBackgroundScanning(Z)V

    .line 366
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasFrequencyAgility()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->supportFrequencyAgility(Z)V

    .line 367
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasRssi()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->supportRssi(Z)V

    .line 368
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasWildcardListId()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->supportWildcardIdList(Z)V

    .line 370
    new-instance v2, Lcom/dsi/ant/channel/TransferController;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-direct {v2, p0, v3, v4}, Lcom/dsi/ant/channel/TransferController;-><init>(Lcom/dsi/ant/channel/ChannelsController;Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;

    .line 371
    new-instance v2, Lcom/dsi/ant/channel/NetworkController;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-direct {v2, v3}, Lcom/dsi/ant/channel/NetworkController;-><init>(Lcom/dsi/ant/adapter/AdapterHandle;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mNetworkController:Lcom/dsi/ant/channel/NetworkController;

    .line 374
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterHandle;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getNumberOfChannels()I

    move-result v1

    .line 375
    .local v1, "numChannels":I
    new-array v2, v1, [Lcom/dsi/ant/message/LibConfig;

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    .line 376
    new-array v2, v1, [Lcom/dsi/ant/channel/AntChannelImpl;

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .line 378
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 380
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    new-instance v3, Lcom/dsi/ant/message/LibConfig;

    invoke-direct {v3}, Lcom/dsi/ant/message/LibConfig;-><init>()V

    aput-object v3, v2, v0

    .line 381
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    new-instance v3, Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    invoke-direct {v3, p0, v0, v4}, Lcom/dsi/ant/channel/AntChannelImpl;-><init>(Lcom/dsi/ant/channel/ChannelsController;ILcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;)V

    aput-object v3, v2, v0

    .line 378
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 384
    :cond_1
    new-instance v2, Lcom/dsi/ant/channel/BackgroundScanController;

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-direct {v2, v3, v4}, Lcom/dsi/ant/channel/BackgroundScanController;-><init>([Lcom/dsi/ant/channel/AntChannelImpl;Lcom/dsi/ant/adapter/AdapterHandle;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    .line 386
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Initialized with "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " channels."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 388
    new-instance v2, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;

    invoke-direct {v2, p0, v5}, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;-><init>(Lcom/dsi/ant/channel/ChannelsController;B)V

    invoke-virtual {p1, v2}, Lcom/dsi/ant/adapter/AdapterHandle;->assignAdapterReceiver(Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;)V

    .line 389
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/ChannelsController;)[Lcom/dsi/ant/channel/AntChannelImpl;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ChannelsController;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/ChannelsController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ChannelsController;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/BackgroundScanController;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ChannelsController;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/TransferController;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ChannelsController;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ChannelsController;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    return-object v0
.end method

.method private applyBufferVotes()Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    .locals 13

    .prologue
    const/4 v7, 0x0

    .line 480
    const v2, 0x9fff6

    .line 481
    .local v2, "minTimeMilliseconds":I
    const v1, 0xffff

    .line 483
    .local v1, "minBuffer":I
    iget-object v8, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferChange_Lock:Ljava/lang/Object;

    monitor-enter v8

    .line 485
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/channel/ChannelsController;->mBufferVotes:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 487
    iget-object v6, p0, Lcom/dsi/ant/channel/ChannelsController;->mBufferVotes:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/dsi/ant/channel/EventBufferSettings;

    .line 489
    invoke-virtual {v6}, Lcom/dsi/ant/channel/EventBufferSettings;->getEventBufferTime()I

    move-result v5

    .line 490
    .local v5, "voteBufferTimeMilliseconds":I
    if-ge v5, v2, :cond_0

    .line 491
    move v2, v5

    goto :goto_0

    .line 495
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v5    # "voteBufferTimeMilliseconds":I
    :cond_1
    const/4 v2, 0x0

    .line 498
    :cond_2
    div-int/lit8 v3, v2, 0xa

    .line 500
    .local v3, "rawMinTime":I
    if-nez v3, :cond_3

    .line 501
    const/4 v1, 0x0

    .line 504
    :cond_3
    new-instance v6, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;

    sget-object v9, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->LOW_PRIORITY:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-direct {v6, v9, v1, v3}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;-><init>(Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;II)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->getRawMessage(II)[B

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/channel/ChannelsController;->txCommand([B)[B

    move-result-object v6

    .line 507
    invoke-static {v6}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .line 513
    .local v4, "response":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v6

    sget-object v9, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    if-ne v6, v9, :cond_6

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v6

    const/16 v9, 0x74

    if-ne v6, v9, :cond_6

    .line 515
    iget-object v6, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferSettings:Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/EventBufferSettings;->getEventBufferTime()I

    move-result v6

    if-eq v6, v2, :cond_6

    .line 516
    iget-object v6, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferSettings:Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/EventBufferSettings;->setEventBufferTime(I)V

    .line 517
    iget-object v6, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/Capabilities;->hasEventBuffering()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v9, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferChange_Lock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v10, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    array-length v11, v10

    move v6, v7

    :goto_1
    if-ge v6, v11, :cond_5

    aget-object v7, v10, v6

    iget-object v7, v7, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v7, :cond_4

    iget-object v12, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferSettings:Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-interface {v7, v12}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_5
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521
    :cond_6
    :try_start_2
    monitor-exit v8

    return-object v4

    .line 517
    :catchall_0
    move-exception v6

    monitor-exit v9

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 522
    .end local v3    # "rawMinTime":I
    .end local v4    # "response":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    :catchall_1
    move-exception v6

    monitor-exit v8

    throw v6
.end method

.method private numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I
    .locals 1
    .param p1, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 417
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/Capabilities;->numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final acquireChannel(Lcom/dsi/ant/channel/SelectedNetwork;)Lcom/dsi/ant/channel/AntChannelImpl;
    .locals 7
    .param p1, "whichNetwork"    # Lcom/dsi/ant/channel/SelectedNetwork;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .local v0, "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 395
    .local v1, "channel":Lcom/dsi/ant/channel/AntChannelImpl;
    monitor-enter v1

    .line 396
    :try_start_0
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannelImpl;->isAvailable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 397
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Acquired "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    sget-object v4, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_ACQUIRED:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iput-object v4, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 399
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mNetworkController:Lcom/dsi/ant/channel/NetworkController;

    invoke-virtual {v4, p1}, Lcom/dsi/ant/channel/NetworkController;->tryUseNetwork(Lcom/dsi/ant/channel/SelectedNetwork;)I

    move-result v4

    iput v4, v1, Lcom/dsi/ant/channel/AntChannelImpl;->networkNumber:I

    .line 401
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    if-eqz v4, :cond_0

    .line 402
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    invoke-interface {v4}, Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;->onChannelAcquired()V

    .line 404
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    .end local v1    # "channel":Lcom/dsi/ant/channel/AntChannelImpl;
    :goto_1
    return-object v1

    .line 406
    .restart local v1    # "channel":Lcom/dsi/ant/channel/AntChannelImpl;
    :cond_1
    monitor-exit v1

    .line 394
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 406
    :catchall_0
    move-exception v4

    monitor-exit v1

    throw v4

    .line 409
    .end local v1    # "channel":Lcom/dsi/ant/channel/AntChannelImpl;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method final adapterRemoved()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 467
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->releaseChannel(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    iget-boolean v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    invoke-interface {v0, v1}, Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;->onChannelAvailable(Z)V

    .line 469
    return-void
.end method

.method public final addBackgroundScanningChannel$13462e()V
    .locals 2

    .prologue
    .line 934
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanStateChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 935
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/BackgroundScanController;->addBackgroundScanningChannel$13462e()V

    .line 936
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final canClaimAdapter()Z
    .locals 1

    .prologue
    .line 454
    iget-boolean v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterHandle;->isAdapterClaimed()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clearBackgroundScanInProgress(I)V
    .locals 2
    .param p1, "channelNumber"    # I

    .prologue
    .line 928
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanStateChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 929
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/BackgroundScanController;->clearBackgroundScanInProgress(I)V

    .line 930
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final destroy()V
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelShutdownPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 848
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelShutdownPool:Ljava/util/concurrent/ExecutorService;

    .line 849
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 887
    if-ne p0, p1, :cond_1

    .line 888
    const/4 v1, 0x1

    .line 900
    :cond_0
    :goto_0
    return v1

    .line 891
    :cond_1
    if-eqz p1, :cond_0

    .line 895
    instance-of v2, p1, Lcom/dsi/ant/channel/ChannelsController;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 899
    check-cast v0, Lcom/dsi/ant/channel/ChannelsController;

    .line 900
    .local v0, "other":Lcom/dsi/ant/channel/ChannelsController;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/channel/ChannelsController;->usesAdapterHandle(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v1

    goto :goto_0
.end method

.method public final getBackgroundScanState()Lcom/dsi/ant/channel/BackgroundScanState;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/BackgroundScanController;->getBackgroundScanState()Lcom/dsi/ant/channel/BackgroundScanState;

    move-result-object v0

    return-object v0
.end method

.method public final getBurstState()Lcom/dsi/ant/channel/BurstState;
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/TransferController;->getBurstState()Lcom/dsi/ant/channel/BurstState;

    move-result-object v0

    return-object v0
.end method

.method public final getCapabilities()Lcom/dsi/ant/channel/Capabilities;
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    return-object v0
.end method

.method public final getEventBufferSettings()Lcom/dsi/ant/channel/EventBufferSettings;
    .locals 1

    .prologue
    .line 946
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferSettings:Lcom/dsi/ant/channel/EventBufferSettings;

    return-object v0
.end method

.method public final getLibConfig()Lcom/dsi/ant/message/LibConfig;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    return-object v0
.end method

.method public final getNumChannelsAvailable()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 439
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ChannelsController;->canClaimAdapter()Z

    move-result v4

    if-nez v4, :cond_1

    .line 450
    :cond_0
    return v1

    .line 442
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    if-eqz v4, :cond_0

    .line 444
    const/4 v1, 0x0

    .line 446
    .local v1, "i":I
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .local v0, "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 447
    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannelImpl;->isAvailable()Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v1, v1, 0x1

    .line 446
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final getTxPowerSetting(B)B
    .locals 1
    .param p1, "powerLevel"    # B

    .prologue
    .line 962
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->getCorrectedTxPowerSetting(B)B

    move-result v0

    return v0
.end method

.method public final hasCapabilities(Lcom/dsi/ant/channel/Capabilities;)Z
    .locals 1
    .param p1, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 413
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/Capabilities;->hasCapabilities(Lcom/dsi/ant/channel/Capabilities;)Z

    move-result v0

    return v0
.end method

.method public final hasChannelReleasing()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 421
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    if-nez v4, :cond_1

    .line 427
    :cond_0
    :goto_0
    return v3

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .local v0, "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 424
    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannelImpl;->isReleasing()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    .line 423
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 877
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterHandle;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0xd9

    .line 882
    return v0
.end method

.method final isNetworkAvailable(Lcom/dsi/ant/channel/SelectedNetwork;)Z
    .locals 1
    .param p1, "whichNetwork"    # Lcom/dsi/ant/channel/SelectedNetwork;

    .prologue
    .line 565
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mNetworkController:Lcom/dsi/ant/channel/NetworkController;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/NetworkController;->isNetworkAvailable(Lcom/dsi/ant/channel/SelectedNetwork;)Z

    move-result v0

    return v0
.end method

.method final isPreferred(Lcom/dsi/ant/channel/ChannelsController;Lcom/dsi/ant/channel/Capabilities;)Z
    .locals 6
    .param p1, "otherChannelController"    # Lcom/dsi/ant/channel/ChannelsController;
    .param p2, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 611
    if-nez p1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return v1

    .line 615
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Comparing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 617
    if-eqz p2, :cond_2

    .line 618
    invoke-direct {p0, p2}, Lcom/dsi/ant/channel/ChannelsController;->numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v3

    invoke-direct {p1, p2}, Lcom/dsi/ant/channel/ChannelsController;->numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 621
    invoke-direct {p0, p2}, Lcom/dsi/ant/channel/ChannelsController;->numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v3

    invoke-direct {p1, p2}, Lcom/dsi/ant/channel/ChannelsController;->numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v4

    if-ge v3, v4, :cond_2

    move v1, v2

    .line 623
    goto :goto_0

    .line 629
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 632
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    iget-object v5, p1, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v3, v4, v5}, Lcom/dsi/ant/adapter/AdapterProvider;->getHandlesPriority(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterHandle;)I

    move-result v0

    .line 635
    .local v0, "adapterComparison":I
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<comparison> Adapter comparison result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 637
    if-eq v0, v1, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method final notifyAcknowledgedMessageComplete()V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/TransferController;->notifyAcknowledgedMessageComplete()V

    .line 873
    return-void
.end method

.method final releaseAdapterClaimIfNotInUse()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 664
    const/4 v0, 0x1

    .line 665
    .local v0, "doRelease":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 666
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    aget-object v3, v2, v1

    monitor-enter v3

    .line 667
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    sget-object v4, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-eq v2, v4, :cond_2

    .line 668
    const/4 v0, 0x0

    .line 669
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 674
    :cond_0
    if-eqz v0, :cond_1

    .line 680
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v2, v5}, Lcom/dsi/ant/adapter/AdapterHandle;->releaseClaim(Z)V

    .line 689
    iput-boolean v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    .line 691
    :cond_1
    return-void

    .line 671
    :cond_2
    monitor-exit v3

    .line 665
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 671
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method final releaseChannel(Lcom/dsi/ant/channel/AntChannelImpl;Z)V
    .locals 3
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannelImpl;
    .param p2, "performCleanup"    # Z

    .prologue
    .line 577
    monitor-enter p1

    .line 579
    :try_start_0
    iget-object v1, p1, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    sget-object v2, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_ACQUIRED:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v1, v2, :cond_1

    .line 581
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Releasing channel "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 582
    sget-object v1, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iput-object v1, p1, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 584
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mNetworkController:Lcom/dsi/ant/channel/NetworkController;

    iget v2, p1, Lcom/dsi/ant/channel/AntChannelImpl;->networkNumber:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/NetworkController;->onChannelReleased(I)V

    .line 586
    iget-object v0, p1, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .line 587
    .local v0, "eventHandler":Lcom/dsi/ant/channel/IAntChannelEventHandler;
    if-eqz v0, :cond_0

    .line 588
    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 591
    :cond_0
    if-eqz p2, :cond_1

    .line 592
    new-instance v1, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;

    invoke-direct {v1, p0, p1}, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;-><init>(Lcom/dsi/ant/channel/ChannelsController;Lcom/dsi/ant/channel/AntChannelImpl;)V

    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelShutdownPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 595
    .end local v0    # "eventHandler":Lcom/dsi/ant/channel/IAntChannelEventHandler;
    :cond_1
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p1

    throw v1
.end method

.method public final removeBackgroundScanningChannel$13462e()V
    .locals 2

    .prologue
    .line 940
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanStateChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 941
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/BackgroundScanController;->removeBackgroundScanningChannel$13462e()V

    .line 942
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final revokeEventBufferingvote(B)V
    .locals 3
    .param p1, "channel"    # B

    .prologue
    .line 650
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mCapabilities:Lcom/dsi/ant/channel/Capabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/Capabilities;->hasEventBuffering()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Channel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " revokes event buffering vote."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 652
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 653
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBufferVotes:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 654
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    invoke-direct {p0}, Lcom/dsi/ant/channel/ChannelsController;->applyBufferVotes()Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .line 657
    :cond_0
    return-void

    .line 654
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V
    .locals 5
    .param p1, "burstState"    # Lcom/dsi/ant/channel/BurstState;

    .prologue
    .line 813
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .local v0, "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 814
    iget-object v1, v4, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .line 816
    .local v1, "eventHandler":Lcom/dsi/ant/channel/IAntAdapterEventHandler;
    if-eqz v1, :cond_0

    .line 817
    invoke-interface {v1, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    .line 813
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 820
    .end local v1    # "eventHandler":Lcom/dsi/ant/channel/IAntAdapterEventHandler;
    :cond_1
    return-void
.end method

.method public final setBackgroundScanInProgress(I)Z
    .locals 2
    .param p1, "channelNumber"    # I

    .prologue
    .line 922
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanStateChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 923
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/BackgroundScanController;->setBackgroundScanInProgress(I)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 924
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final setEventBufferVote(BLcom/dsi/ant/channel/EventBufferSettings;)Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    .locals 3
    .param p1, "channel"    # B
    .param p2, "eventBufferSettings"    # Lcom/dsi/ant/channel/EventBufferSettings;

    .prologue
    .line 835
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Channel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has event buffering vote "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 837
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mEventBufferChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 838
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mBufferVotes:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 841
    invoke-direct {p0}, Lcom/dsi/ant/channel/ChannelsController;->applyBufferVotes()Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-result-object v0

    return-object v0

    .line 839
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final setHasUseOfAdapter(Z)V
    .locals 2
    .param p1, "claimed"    # Z

    .prologue
    .line 458
    iput-boolean p1, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    .line 459
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    iget-boolean v1, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    invoke-interface {v0, v1}, Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;->onChannelAvailable(Z)V

    .line 460
    return-void
.end method

.method final setLibConfigVote(ILcom/dsi/ant/message/LibConfig;)Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    .locals 10
    .param p1, "channelNumber"    # I
    .param p2, "vote"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    const/4 v3, 0x0

    .line 750
    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigChange_Lock:Ljava/lang/Object;

    monitor-enter v4

    .line 751
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    aput-object p2, v5, p1

    .line 753
    new-instance v0, Lcom/dsi/ant/message/LibConfig;

    invoke-direct {v0}, Lcom/dsi/ant/message/LibConfig;-><init>()V

    .line 756
    .local v0, "config":Lcom/dsi/ant/message/LibConfig;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 757
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lcom/dsi/ant/message/LibConfig;->getEnableChannelIdOutput()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 758
    invoke-virtual {v0}, Lcom/dsi/ant/message/LibConfig;->setEnableChannelIdOutput$1385ff()V

    .line 761
    :cond_0
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lcom/dsi/ant/message/LibConfig;->getEnableRssiOutput()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 762
    invoke-virtual {v0}, Lcom/dsi/ant/message/LibConfig;->setEnableRssiOutput$1385ff()V

    .line 765
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigVotes:[Lcom/dsi/ant/message/LibConfig;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lcom/dsi/ant/message/LibConfig;->getEnableRxTimestampOutput()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 766
    invoke-virtual {v0}, Lcom/dsi/ant/message/LibConfig;->setEnableRxTimestampOutput$1385ff()V

    .line 756
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 770
    :cond_3
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    new-instance v6, Lcom/dsi/ant/message/fromhost/LibConfigMessage;

    invoke-direct {v6, v0}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;-><init>(Lcom/dsi/ant/message/LibConfig;)V

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->getRawMessage(II)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/dsi/ant/adapter/AdapterHandle;->txCommand([B)[B

    move-result-object v2

    .line 772
    .local v2, "result":[B
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v0, v5}, Lcom/dsi/ant/message/LibConfig;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 773
    iput-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    .line 775
    iget-object v5, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfigChange_Lock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v6, p0, Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    array-length v7, v6

    :goto_1
    if-ge v3, v7, :cond_5

    aget-object v8, v6, v3

    iget-object v8, v8, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v8, :cond_4

    iget-object v9, p0, Lcom/dsi/ant/channel/ChannelsController;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    invoke-interface {v8, v9}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 777
    :cond_6
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 779
    invoke-static {v2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    return-object v3

    .line 775
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v5

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 777
    .end local v0    # "config":Lcom/dsi/ant/message/LibConfig;
    .end local v1    # "i":I
    .end local v2    # "result":[B
    :catchall_1
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method final strongClaimAdapter()Z
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterHandle;->strongClaimAdapter()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    return v0
.end method

.method final txAcknowledgedMessage([B)Z
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 868
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/TransferController;->txAcknowledgedMessage([B)Z

    move-result v0

    return v0
.end method

.method final txBurst(I[B)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 1
    .param p1, "channelNumber"    # I
    .param p2, "data"    # [B

    .prologue
    .line 864
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/channel/TransferController;->txBurst(I[B)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    return-object v0
.end method

.method final txCommand([B)[B
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 856
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->txCommand([B)[B

    move-result-object v0

    return-object v0
.end method

.method final txData([B)Z
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 860
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->txData([B)Z

    move-result v0

    return v0
.end method

.method final unhandledMessageType(Lcom/dsi/ant/message/fromant/AntMessageFromAnt;)V
    .locals 3
    .param p1, "antMessage"    # Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring unhandled message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public final usesAdapterHandle(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    .locals 1
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 918
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final weakClaimAdapter()Z
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterHandle;->weakClaimAdapter()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ChannelsController;->mHasUseOfAdapter:Z

    return v0
.end method
