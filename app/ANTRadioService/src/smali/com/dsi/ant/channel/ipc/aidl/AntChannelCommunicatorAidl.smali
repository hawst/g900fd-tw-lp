.class public Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
.super Ljava/lang/Object;
.source "AntChannelCommunicatorAidl.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$2;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

.field final mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

.field private mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

.field mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

.field final mChannelEventHandlerChange_Lock:Ljava/lang/Object;

.field private mEventHandlerThread_Lock:Ljava/lang/Object;

.field private mEventReceiverMessenger:Landroid/os/Messenger;

.field private mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

.field private mReceiveThread:Landroid/os/HandlerThread;

.field private final mReferenceToken:Landroid/os/Binder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    .line 757
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;)V
    .locals 2
    .param p1, "acquiredChannelAidl"    # Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    .prologue
    const/4 v1, 0x0

    .line 513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    .line 357
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    .line 359
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventHandlerThread_Lock:Ljava/lang/Object;

    .line 364
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    .line 369
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 623
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    .line 514
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    .line 516
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->setupIpcEventReceiverThread()Z

    .line 522
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReferenceToken:Landroid/os/Binder;

    .line 534
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/BurstState;)V
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/BurstState;

    .prologue
    .line 65
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/message/LibConfig;)V
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    .line 65
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/BackgroundScanState;)V
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/BackgroundScanState;

    .prologue
    .line 65
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/EventBufferSettings;)V
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/EventBufferSettings;

    .prologue
    .line 65
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setupIpcEventReceiverThread()Z
    .locals 5

    .prologue
    .line 390
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventHandlerThread_Lock:Ljava/lang/Object;

    monitor-enter v2

    .line 391
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    monitor-exit v2

    .line 413
    :goto_0
    return v0

    .line 393
    :cond_0
    new-instance v1, Landroid/os/HandlerThread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Receive thread"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    .line 394
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 396
    new-instance v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;-><init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    const/4 v0, 0x0

    .line 401
    .local v0, "addSuccess":Z
    :try_start_1
    new-instance v1, Landroid/os/Messenger;

    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    invoke-direct {v1, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 402
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    invoke-interface {v1, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->addEventReceiver(Landroid/os/Messenger;)Z

    move-result v0

    .line 404
    if-nez v0, :cond_1

    .line 405
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->teardownIpcEventReceiverThread()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 414
    .end local v0    # "addSuccess":Z
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 409
    .restart local v0    # "addSuccess":Z
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 410
    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    const-string v3, "Could not setup IPC Event receiver with remote service."

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private teardownIpcEventReceiverThread()V
    .locals 3

    .prologue
    .line 422
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventHandlerThread_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 423
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 440
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    .line 428
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    invoke-static {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->access$400(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;)V

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    invoke-interface {v0, v2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->removeEventReceiver(Landroid/os/Messenger;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    :goto_1
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 440
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 437
    :catch_0
    move-exception v0

    :try_start_3
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    const-string v2, "Could not remove IPC Event receiver with remote service."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 747
    const/4 v0, 0x0

    return v0
.end method

.method public final onChannelDeathMessage()V
    .locals 2

    .prologue
    .line 211
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 216
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->teardownIpcEventReceiverThread()V

    .line 218
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onRxAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 203
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 207
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 752
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 754
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 755
    return-void
.end method
