.class final Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;
.super Ljava/lang/Object;
.source "NetworkKeyStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/NetworkKeyStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NetworkKeyEntry"
.end annotation


# instance fields
.field public keyTypeEncrypted:[B

.field public networkKeyHashEncrypted:[B

.field public networkNameEncrypted:[B

.field public shortNetworkKeyEncrypted:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "networkKeyType"    # Ljava/lang/String;
    .param p2, "networkKeyHash"    # Ljava/lang/String;
    .param p3, "shortNetworkKey"    # Ljava/lang/String;
    .param p4, "networkName"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->makeByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->keyTypeEncrypted:[B

    .line 56
    invoke-static {p2}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->makeByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->networkKeyHashEncrypted:[B

    .line 57
    invoke-static {p3}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->makeByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->shortNetworkKeyEncrypted:[B

    .line 58
    invoke-static {p4}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->makeByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->networkNameEncrypted:[B

    .line 59
    return-void
.end method

.method private static makeByteArray(Ljava/lang/String;)[B
    .locals 4
    .param p0, "inputString"    # Ljava/lang/String;

    .prologue
    .line 62
    const-string v3, "\\|"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "hexSubString":[Ljava/lang/String;
    array-length v3, v1

    new-array v0, v3, [B

    .line 65
    .local v0, "byteArray":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 66
    aget-object v3, v1, v2

    invoke-static {v3}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->byteValue()B

    move-result v3

    aput-byte v3, v0, v2

    .line 65
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 68
    :cond_0
    return-object v0
.end method
