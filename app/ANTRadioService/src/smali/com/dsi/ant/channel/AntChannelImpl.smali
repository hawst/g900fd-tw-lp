.class public final Lcom/dsi/ant/channel/AntChannelImpl;
.super Ljava/lang/Object;
.source "AntChannelImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/AntChannelImpl$1;,
        Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;
    }
.end annotation


# instance fields
.field public final TAG:Ljava/lang/String;

.field public final controller:Lcom/dsi/ant/channel/ChannelsController;

.field public isBackgroundScanningChannel:Z

.field mAckInProgress:Z

.field mAckInProgressData:[B

.field mAckInProgressUpdate_LOCK:Ljava/lang/Object;

.field public mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

.field mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

.field private mAntAdapterEventHandlerChange_LOCK:Ljava/lang/Object;

.field mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

.field mAntChannelEventHandlerChange_LOCK:Ljava/lang/Object;

.field private final mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

.field public final mChannelNumber:I

.field volatile mCloseSeen:Z

.field public mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/dsi/ant/message/fromhost/MessageFromHostType;",
            ">;"
        }
    .end annotation
.end field

.field final mWaitCloseLock:Ljava/lang/Object;

.field public networkNumber:I


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/ChannelsController;ILcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;)V
    .locals 3
    .param p1, "controller"    # Lcom/dsi/ant/channel/ChannelsController;
    .param p2, "channelNum"    # I
    .param p3, "listener"    # Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    .prologue
    const/4 v2, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mWaitCloseLock:Ljava/lang/Object;

    .line 90
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandlerChange_LOCK:Ljava/lang/Object;

    .line 98
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandlerChange_LOCK:Ljava/lang/Object;

    .line 107
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 111
    iput-boolean v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    .line 121
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    .line 157
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    .line 158
    int-to-byte v0, p2

    iput v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Channel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    .line 162
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 163
    iput-object p3, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    .line 165
    iput-boolean v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    .line 166
    return-void
.end method

.method public static createResult(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 3
    .param p0, "responseMessage"    # Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    if-ne v1, v2, :cond_0

    .line 242
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 247
    .local v0, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_0
    return-object v0

    .line 244
    .end local v0    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :cond_0
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)V

    .restart local v0    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_0
.end method


# virtual methods
.method public final burstTransfer([B)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 603
    sget-object v1, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v1, v2, :cond_0

    .line 604
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 624
    :goto_0
    return-object v0

    .line 607
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 608
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-eqz v1, :cond_1

    .line 609
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v3, "Ack in progress, can\'t burst"

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 615
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 613
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    .line 614
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    .line 615
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 617
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v1, v2, p1}, Lcom/dsi/ant/channel/ChannelsController;->txBurst(I[B)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 619
    .local v0, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 620
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    .line 621
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 622
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final cleanUp()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 679
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    new-instance v1, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;

    invoke-direct {v1, v7, v7}, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;-><init>(IZ)V

    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    sget-object v3, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 680
    .local v0, "configurationResponse":Lcom/dsi/ant/channel/ipc/ServiceResult;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_0

    .line 681
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reset inclusion/exclusion list to defaults failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :cond_0
    sget-object v4, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->INVALID:Lcom/dsi/ant/message/ChannelState;

    const/4 v3, 0x0

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-direct {v1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>()V

    new-instance v5, Lcom/dsi/ant/message/fromhost/RequestMessage;

    sget-object v6, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-direct {v5, v6}, Lcom/dsi/ant/message/fromhost/RequestMessage;-><init>(Lcom/dsi/ant/message/fromant/MessageFromAntType;)V

    invoke-virtual {p0, v5, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->processRequestResponse(Lcom/dsi/ant/message/fromhost/RequestMessage;Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v5

    invoke-virtual {v1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    invoke-static {v5, v1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;[B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v1

    instance-of v5, v1, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    if-eqz v5, :cond_7

    check-cast v1, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    :goto_0
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    :goto_1
    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    .line 746
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    int-to-byte v2, v2

    new-instance v3, Lcom/dsi/ant/message/LibConfig;

    invoke-direct {v3}, Lcom/dsi/ant/message/LibConfig;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/ChannelsController;->setLibConfigVote(ILcom/dsi/ant/message/LibConfig;)Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .line 747
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/ChannelsController;->revokeEventBufferingvote(B)V

    .line 748
    :goto_3
    return-void

    .line 690
    :pswitch_0
    iput-boolean v7, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mCloseSeen:Z

    .line 693
    :goto_4
    new-instance v1, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;

    invoke-direct {v1}, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;-><init>()V

    invoke-virtual {p0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->close(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntCommandFailureReason;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 694
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 697
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-eqz v1, :cond_2

    .line 698
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 708
    :cond_2
    :goto_5
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 706
    :catch_0
    move-exception v1

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 711
    :cond_3
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_4

    .line 712
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Close failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    :cond_4
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mWaitCloseLock:Ljava/lang/Object;

    monitor-enter v2

    .line 717
    :goto_6
    :try_start_3
    iget-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mCloseSeen:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-nez v1, :cond_5

    .line 719
    :try_start_4
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mWaitCloseLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_6

    .line 721
    :catch_1
    move-exception v1

    :try_start_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 722
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    .line 725
    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_5
    monitor-exit v2

    .line 730
    :pswitch_1
    new-instance v1, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v1}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>()V

    invoke-virtual {p0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 732
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_1

    .line 733
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unassign failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    move-object v1, v2

    goto/16 :goto_1

    :cond_7
    move-object v1, v3

    goto/16 :goto_0

    .line 685
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final close(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 4
    .param p1, "antMessage"    # Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    .prologue
    .line 515
    const/4 v0, 0x0

    .line 516
    .local v0, "isTransferInProgress":Z
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 517
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-eqz v2, :cond_0

    .line 518
    const/4 v0, 0x1

    .line 520
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 524
    if-eqz v0, :cond_1

    .line 525
    new-instance v1, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 532
    .local v1, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_0
    return-object v1

    .line 520
    .end local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 527
    :cond_1
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v1

    .restart local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_0
.end method

.method public final isAvailable()Z
    .locals 2

    .prologue
    .line 926
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFrequencyAllowed(I)Z
    .locals 4
    .param p1, "frequency"    # I

    .prologue
    .line 261
    const/4 v0, 0x1

    .line 263
    .local v0, "allowed":Z
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v1

    .line 264
    .local v1, "channelCapabilities":Lcom/dsi/ant/channel/Capabilities;
    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities;->getRfFrequencyMin()I

    move-result v2

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities;->getRfFrequencyMax()I

    move-result v3

    invoke-static {p1, v2, v3}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x39

    if-ne p1, v2, :cond_1

    iget v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->networkNumber:I

    sget-object v3, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/PredefinedNetwork;->getRawValue()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 268
    :cond_0
    const/4 v0, 0x0

    .line 270
    :cond_1
    return v0
.end method

.method public final isReleasing()Z
    .locals 2

    .prologue
    .line 935
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final processRequestResponse(Lcom/dsi/ant/message/fromhost/RequestMessage;Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 5
    .param p1, "requestMessage"    # Lcom/dsi/ant/message/fromhost/RequestMessage;
    .param p2, "responseMessage"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 546
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v3, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    iget v4, p0, Lcom/dsi/ant/channel/AntChannelImpl;->networkNumber:I

    invoke-virtual {p1, v3, v4}, Lcom/dsi/ant/message/fromhost/RequestMessage;->getRawMessage(II)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/ChannelsController;->txCommand([B)[B

    move-result-object v2

    .line 548
    invoke-static {v2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    .line 550
    .local v0, "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    if-nez v0, :cond_0

    .line 551
    new-instance v1, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 565
    .end local v0    # "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .local v1, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_0
    return-object v1

    .line 552
    .end local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    .restart local v0    # "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_0
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne v2, v3, :cond_1

    .line 553
    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .line 554
    .end local v0    # "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannelImpl;->createResult(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v1

    .line 555
    .restart local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_0

    .end local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    .restart local v0    # "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_1
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne v2, v3, :cond_2

    .line 556
    new-instance v1, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .restart local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_0

    .line 559
    .end local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :cond_2
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageId()I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->setMessageId(I)V

    .line 560
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->setMessageContent([B)V

    .line 562
    sget-object v1, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .restart local v1    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_0
.end method

.method public final releaseChannel(Z)V
    .locals 1
    .param p1, "performCleanup"    # Z

    .prologue
    .line 904
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v0, p0, p1}, Lcom/dsi/ant/channel/ChannelsController;->releaseChannel(Lcom/dsi/ant/channel/AntChannelImpl;Z)V

    .line 905
    return-void
.end method

.method public final setAdapterEventHandler(Lcom/dsi/ant/channel/IAntAdapterEventHandler;)V
    .locals 2
    .param p1, "eventHandler"    # Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .prologue
    .line 665
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandlerChange_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 666
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .line 667
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final setBroadcastData([B)Z
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 569
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 571
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    int-to-byte v1, v1

    invoke-static {v1, p1}, Lcom/dsi/ant/AntMessageBuilder;->getANTSendBroadcastData(B[B)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/ChannelsController;->txData([B)Z

    move-result v0

    goto :goto_0
.end method

.method public final setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V
    .locals 2
    .param p1, "eventHandler"    # Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .prologue
    .line 655
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandlerChange_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 656
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .line 657
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final setLibConfig(Lcom/dsi/ant/message/LibConfig;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 2
    .param p1, "libConfig"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    .line 634
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v0, v1, :cond_0

    .line 635
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 638
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v0, v1, p1}, Lcom/dsi/ant/channel/ChannelsController;->setLibConfigVote(ILcom/dsi/ant/message/LibConfig;)Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-result-object v0

    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannelImpl;->createResult(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final setTransmitPower(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 4
    .param p1, "antMessage"    # Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    .prologue
    .line 446
    move-object v1, p1

    check-cast v1, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;

    .line 449
    .local v1, "transmitMessage":Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v2, v2, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->needsTxPowerRemap()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;->getMessageContent()[B

    move-result-object v2

    .line 451
    const/4 v3, 0x1

    aget-byte v0, v2, v3

    .line 454
    .local v0, "powerLevel":B
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/channel/ChannelsController;->getTxPowerSetting(B)B

    move-result v0

    .line 457
    new-instance v1, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;

    .end local v1    # "transmitMessage":Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;-><init>(ILcom/dsi/ant/channel/Capabilities;)V

    .line 460
    .end local v0    # "powerLevel":B
    .restart local v1    # "transmitMessage":Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;
    :cond_0
    invoke-virtual {p0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v2

    return-object v2
.end method

.method public final startAcknowledgedTransfer([B)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 3
    .param p1, "data"    # [B

    .prologue
    .line 575
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v0, v1, :cond_0

    .line 576
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 599
    :goto_0
    return-object v0

    .line 579
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 580
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-eqz v0, :cond_1

    .line 581
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v2}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 586
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 584
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    int-to-byte v0, v0

    invoke-static {v0, p1}, Lcom/dsi/ant/AntMessageBuilder;->getANTSendAcknowledgedData(B[B)[B

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    .line 585
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    .line 586
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/ChannelsController;->txAcknowledgedMessage([B)Z

    move-result v0

    .line 590
    if-eqz v0, :cond_2

    .line 591
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    goto :goto_0

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 595
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    .line 596
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 597
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 599
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    goto :goto_0

    .line 597
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 909
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "channel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 3
    .param p1, "antMessage"    # Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    .prologue
    .line 311
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 313
    .local v0, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    iget-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 314
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    .line 315
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v2, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelsController;->removeBackgroundScanningChannel$13462e()V

    .line 318
    :cond_0
    return-object v0
.end method

.method public final writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 2
    .param p1, "antMessage"    # Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    .prologue
    .line 299
    iget v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    iget v1, p0, Lcom/dsi/ant/channel/AntChannelImpl;->networkNumber:I

    invoke-virtual {p1, v0, v1}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getRawMessage(II)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage([B)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    return-object v0
.end method

.method public final writeMessage([B)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 3
    .param p1, "rawMessage"    # [B

    .prologue
    .line 303
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/ChannelsController;->txCommand([B)[B

    move-result-object v0

    .line 305
    invoke-static {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    .line 307
    if-nez v0, :cond_0

    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne v1, v2, :cond_1

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannelImpl;->createResult(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    goto :goto_0
.end method
