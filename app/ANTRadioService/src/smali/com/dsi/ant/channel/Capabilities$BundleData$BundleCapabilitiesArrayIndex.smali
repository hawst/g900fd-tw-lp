.class final enum Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;
.super Ljava/lang/Enum;
.source "Capabilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BundleCapabilitiesArrayIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

.field public static final enum EVENT_BUFFERING:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

.field public static final enum NUMBER_OF_BUNDLE_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

.field public static final enum RSSI:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

.field public static final enum WILDCARD_ID_LIST:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 404
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    const-string v1, "RSSI"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->RSSI:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    .line 405
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    const-string v1, "WILDCARD_ID_LIST"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->WILDCARD_ID_LIST:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    .line 406
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    const-string v1, "EVENT_BUFFERING"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->EVENT_BUFFERING:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    .line 407
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    const-string v1, "NUMBER_OF_BUNDLE_CAPABILITIES"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->NUMBER_OF_BUNDLE_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    .line 402
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->RSSI:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->WILDCARD_ID_LIST:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->EVENT_BUFFERING:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->NUMBER_OF_BUNDLE_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 402
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 402
    const-class v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;
    .locals 1

    .prologue
    .line 402
    sget-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    return-object v0
.end method
