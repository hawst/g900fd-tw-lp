.class Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;
.super Landroid/os/Handler;
.source "AntChannelProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/AntChannelProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BroadcastChannelStateChangeHandler"
.end annotation


# instance fields
.field private HANDLER_TAG:Ljava/lang/String;

.field private mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

.field private final mDestroy_LOCK:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/dsi/ant/channel/AntChannelProviderImpl;)V
    .locals 1
    .param p1, "channelProvider"    # Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .prologue
    .line 62
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 55
    const-class v0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->HANDLER_TAG:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mDestroy_LOCK:Ljava/lang/Object;

    .line 63
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .line 64
    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    .line 67
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 70
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 87
    :goto_0
    return-void

    .line 79
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 84
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->HANDLER_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Internal message with unknown id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 81
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v2}, Lcom/dsi/ant/message/MessageUtils;->booleanFromNumber(I)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->access$000(Lcom/dsi/ant/channel/AntChannelProviderImpl;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
