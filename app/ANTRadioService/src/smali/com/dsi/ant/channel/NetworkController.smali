.class final Lcom/dsi/ant/channel/NetworkController;
.super Ljava/lang/Object;
.source "NetworkController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;,
        Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;
    }
.end annotation


# static fields
.field private static final PUBLIC_LONG_NETWORK_KEY:Lcom/dsi/ant/channel/NetworkKey;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

.field private mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

.field private final mNetworkSlotChannelUsageUpdate_LOCK:Ljava/lang/Object;

.field private mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-class v0, Lcom/dsi/ant/channel/NetworkController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/NetworkController;->TAG:Ljava/lang/String;

    .line 110
    new-instance v0, Lcom/dsi/ant/channel/NetworkKey;

    const/16 v1, 0x10

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/NetworkKey;-><init>([B)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkController;->PUBLIC_LONG_NETWORK_KEY:Lcom/dsi/ant/channel/NetworkKey;

    return-void

    nop

    :array_0
    .array-data 1
        -0x25t
        -0x4at
        0x5ct
        0x7dt
        -0xft
        0x1dt
        0x52t
        0x37t
        -0x6ft
        0x27t
        -0x27t
        -0x39t
        -0x24t
        0x43t
        -0x1et
        0x28t
    .end array-data
.end method

.method public constructor <init>(Lcom/dsi/ant/adapter/AdapterHandle;)V
    .locals 2
    .param p1, "adapterHandle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    const/4 v1, 0x0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    sget-object v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->UNKNOWN:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkController;->mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    .line 138
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkController;->mNetworkSlotChannelUsageUpdate_LOCK:Ljava/lang/Object;

    .line 143
    iput-object p1, p0, Lcom/dsi/ant/channel/NetworkController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 144
    new-instance v0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;-><init>(Lcom/dsi/ant/channel/NetworkController;B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    .line 148
    iget-object v0, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iput v1, v0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    .line 149
    return-void
.end method

.method private setNetworkKey(ILcom/dsi/ant/channel/NetworkKey;I)V
    .locals 5
    .param p1, "networkIndex"    # I
    .param p2, "inputNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;
    .param p3, "networkSlot"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 308
    sget-object v2, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->UNKNOWN:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    iget-object v3, p0, Lcom/dsi/ant/channel/NetworkController;->mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    if-eq v2, v3, :cond_0

    sget-object v2, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->LONG_128_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    iget-object v3, p0, Lcom/dsi/ant/channel/NetworkController;->mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    if-ne v2, v3, :cond_1

    .line 312
    :cond_0
    new-instance v2, Lcom/dsi/ant/message/fromhost/SetLongNetworkKeyMessage;

    invoke-virtual {p2}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dsi/ant/message/fromhost/SetLongNetworkKeyMessage;-><init>([B)V

    invoke-direct {p0, v2, p3}, Lcom/dsi/ant/channel/NetworkController;->writeNetworkMessage(Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 314
    sget-object v2, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->LONG_128_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    iput-object v2, p0, Lcom/dsi/ant/channel/NetworkController;->mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    .line 342
    :goto_0
    return-void

    .line 330
    :cond_1
    sget-object v2, Lcom/dsi/ant/channel/NetworkKeyStore;->CONVERSION_TABLE:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt p1, v2, :cond_2

    .line 332
    const/16 v2, 0x8

    new-array v0, v2, [B

    .line 333
    .local v0, "rawShortNetworkKey":[B
    invoke-virtual {p2}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v2

    array-length v3, v0

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    new-instance v1, Lcom/dsi/ant/channel/NetworkKey;

    invoke-direct {v1, v0}, Lcom/dsi/ant/channel/NetworkKey;-><init>([B)V

    .line 340
    .end local v0    # "rawShortNetworkKey":[B
    .local v1, "shortNetworkKey":Lcom/dsi/ant/channel/NetworkKey;
    :goto_1
    new-instance v2, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;-><init>([B)V

    invoke-direct {p0, v2, p3}, Lcom/dsi/ant/channel/NetworkController;->writeNetworkMessage(Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 342
    sget-object v2, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->SHORT_64_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    iput-object v2, p0, Lcom/dsi/ant/channel/NetworkController;->mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    goto :goto_0

    .line 336
    .end local v1    # "shortNetworkKey":Lcom/dsi/ant/channel/NetworkKey;
    :cond_2
    invoke-static {p1, p2}, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->getShortKeyFromTable(ILcom/dsi/ant/channel/NetworkKey;)Lcom/dsi/ant/channel/NetworkKey;

    move-result-object v1

    .restart local v1    # "shortNetworkKey":Lcom/dsi/ant/channel/NetworkKey;
    goto :goto_1

    .line 346
    :cond_3
    sget-object v2, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->UNKNOWN:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    iput-object v2, p0, Lcom/dsi/ant/channel/NetworkController;->mKnownNetworkKeyLength:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    .line 349
    new-instance v2, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v3, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NETWORK_NOT_AVAILABLE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v2, v3}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v2
.end method

.method private tryUseNetwork(Lcom/dsi/ant/channel/NetworkKey;I)I
    .locals 5
    .param p1, "networkKey"    # Lcom/dsi/ant/channel/NetworkKey;
    .param p2, "networkIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    .line 214
    const/4 v0, -0x1

    .line 217
    .local v0, "networkSlotNumber":I
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    invoke-virtual {v1, p2}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->isNetworkSlotFree(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    sget-object v1, Lcom/dsi/ant/channel/NetworkController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempting to set Network "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on network slot "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v3, v3, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/channel/NetworkController;->mNetworkSlotChannelUsageUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 233
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->isNetworkSlotInUse()Z

    move-result v1

    if-nez v1, :cond_2

    .line 240
    const/4 v1, 0x0

    sget-object v3, Lcom/dsi/ant/channel/NetworkController;->PUBLIC_LONG_NETWORK_KEY:Lcom/dsi/ant/channel/NetworkKey;

    iget-object v4, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v4, v4, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    invoke-direct {p0, v1, v3, v4}, Lcom/dsi/ant/channel/NetworkController;->setNetworkKey(ILcom/dsi/ant/channel/NetworkKey;I)V

    .line 244
    if-eqz p2, :cond_1

    .line 246
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v1, v1, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    invoke-direct {p0, p2, p1, v1}, Lcom/dsi/ant/channel/NetworkController;->setNetworkKey(ILcom/dsi/ant/channel/NetworkKey;I)V

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iput p2, v1, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkIndex:I

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v3, v1, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->mChannelCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->mChannelCount:I

    .line 251
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v0, v1, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    .line 254
    sget-object v1, Lcom/dsi/ant/channel/NetworkController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully started using Network "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on network slot "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v2, v2, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 257
    :cond_3
    return v0

    .line 251
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private writeNetworkMessage(Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;I)Z
    .locals 6
    .param p1, "antMessage"    # Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
    .param p2, "networkSlot"    # I

    .prologue
    .line 364
    iget-object v4, p0, Lcom/dsi/ant/channel/NetworkController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    const/4 v5, 0x0

    invoke-virtual {p1, v5, p2}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getRawMessage(II)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/adapter/AdapterHandle;->txCommand([B)[B

    move-result-object v1

    .line 366
    .local v1, "rawResponse":[B
    const/4 v3, 0x0

    .line 367
    .local v3, "result":Z
    if-eqz v1, :cond_0

    .line 369
    invoke-static {v1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    .line 371
    .local v0, "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v4

    sget-object v5, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne v4, v5, :cond_0

    move-object v2, v0

    .line 372
    check-cast v2, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .line 373
    .local v2, "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    sget-object v4, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 374
    const/4 v3, 0x1

    .line 378
    .end local v0    # "commandResponse":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .end local v2    # "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    :cond_0
    return v3
.end method


# virtual methods
.method final isNetworkAvailable(Lcom/dsi/ant/channel/SelectedNetwork;)Z
    .locals 3
    .param p1, "whichNetwork"    # Lcom/dsi/ant/channel/SelectedNetwork;

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 269
    .local v0, "result":Z
    iget-boolean v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->isPredefined:Z

    if-eqz v1, :cond_2

    .line 270
    iget-object v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->PUBLIC:Lcom/dsi/ant/channel/PredefinedNetwork;

    if-ne v1, v2, :cond_1

    .line 271
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v2, p1, Lcom/dsi/ant/channel/SelectedNetwork;->networkIndex:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->isNetworkSlotFree(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const/4 v0, 0x1

    :cond_0
    :goto_0
    move v1, v0

    .line 289
    :goto_1
    return v1

    .line 275
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NETWORK_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 282
    const/4 v1, 0x0

    goto :goto_1

    .line 285
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v2, p1, Lcom/dsi/ant/channel/SelectedNetwork;->networkIndex:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->isNetworkSlotFree(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final onChannelReleased(I)V
    .locals 3
    .param p1, "networkSlotNumber"    # I

    .prologue
    .line 159
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mNetworkSlotChannelUsageUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v0, v0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkSlotNumber:I

    if-ne v0, p1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/dsi/ant/channel/NetworkController;->mSingleNetworkSlot:Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;

    iget v2, v0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->mChannelCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->mChannelCount:I

    .line 165
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final tryUseNetwork(Lcom/dsi/ant/channel/SelectedNetwork;)I
    .locals 3
    .param p1, "selectedNetwork"    # Lcom/dsi/ant/channel/SelectedNetwork;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    .line 178
    iget-boolean v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->isPredefined:Z

    if-eqz v1, :cond_2

    .line 182
    iget-object v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->PUBLIC:Lcom/dsi/ant/channel/PredefinedNetwork;

    if-ne v1, v2, :cond_1

    .line 185
    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NETWORK_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    sget-object v1, Lcom/dsi/ant/channel/NetworkController;->PUBLIC_LONG_NETWORK_KEY:Lcom/dsi/ant/channel/NetworkKey;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/dsi/ant/channel/NetworkController;->tryUseNetwork(Lcom/dsi/ant/channel/NetworkKey;I)I

    move-result v0

    .line 201
    .local v0, "networkSlotNumber":I
    :goto_0
    return v0

    .line 191
    .end local v0    # "networkSlotNumber":I
    :cond_0
    iget-object v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/PredefinedNetwork;->getRawValue()I

    move-result v0

    .restart local v0    # "networkSlotNumber":I
    goto :goto_0

    .line 195
    .end local v0    # "networkSlotNumber":I
    :cond_1
    iget-object v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/PredefinedNetwork;->getRawValue()I

    move-result v0

    .restart local v0    # "networkSlotNumber":I
    goto :goto_0

    .line 199
    .end local v0    # "networkSlotNumber":I
    :cond_2
    iget-object v1, p1, Lcom/dsi/ant/channel/SelectedNetwork;->networkKey:Lcom/dsi/ant/channel/NetworkKey;

    iget v2, p1, Lcom/dsi/ant/channel/SelectedNetwork;->networkIndex:I

    invoke-direct {p0, v1, v2}, Lcom/dsi/ant/channel/NetworkController;->tryUseNetwork(Lcom/dsi/ant/channel/NetworkKey;I)I

    move-result v0

    .restart local v0    # "networkSlotNumber":I
    goto :goto_0
.end method
