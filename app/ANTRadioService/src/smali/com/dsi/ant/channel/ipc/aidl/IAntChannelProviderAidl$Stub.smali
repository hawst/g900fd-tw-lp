.class public abstract Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;
.super Landroid/os/Binder;
.source "IAntChannelProviderAidl.java"

# interfaces
.implements Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p0, p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 165
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v7, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 51
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1

    .line 52
    sget-object v7, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/Capabilities;

    .line 58
    .local v1, "_arg1":Lcom/dsi/ant/channel/Capabilities;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_2

    .line 59
    sget-object v7, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/Capabilities;

    .line 65
    .local v2, "_arg2":Lcom/dsi/ant/channel/Capabilities;
    :goto_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v3, "_arg3":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v4

    .line 67
    .local v4, "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v4, :cond_0

    invoke-interface {v4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 69
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    invoke-virtual {v3, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 55
    .end local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    .end local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    .end local v3    # "_arg3":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_1

    .line 62
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_2

    .line 80
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    .end local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    :sswitch_2
    const-string v7, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_3

    .line 85
    sget-object v7, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    .line 88
    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_4

    .line 92
    sget-object v7, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    .line 95
    :cond_4
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 99
    .restart local v3    # "_arg3":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->acquireChannelKey$2ece064f()Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v4

    .line 100
    .restart local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 101
    if-eqz v4, :cond_5

    invoke-interface {v4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_5
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 102
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    invoke-virtual {v3, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 113
    .end local v3    # "_arg3":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :sswitch_3
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_6

    .line 116
    sget-object v5, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities;

    .line 121
    .local v0, "_arg0":Lcom/dsi/ant/channel/Capabilities;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v4

    .line 122
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 119
    .end local v0    # "_arg0":Lcom/dsi/ant/channel/Capabilities;
    .end local v4    # "_result":I
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_3

    .line 128
    .end local v0    # "_arg0":Lcom/dsi/ant/channel/Capabilities;
    :sswitch_4
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->isLegacyInterfaceInUse()Z

    move-result v4

    .line 130
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 131
    if-eqz v4, :cond_7

    move v5, v6

    :goto_4
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_7
    move v5, v7

    goto :goto_4

    .line 136
    .end local v4    # "_result":Z
    :sswitch_5
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_8

    .line 139
    sget-object v5, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 145
    .local v0, "_arg0":Landroid/os/Message;
    :goto_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 146
    .local v1, "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;

    move-result-object v4

    .line 147
    .local v4, "_result":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    if-eqz v4, :cond_9

    .line 149
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 150
    invoke-virtual {v4, p3, v6}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 155
    :goto_6
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    invoke-virtual {v1, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 142
    .end local v0    # "_arg0":Landroid/os/Message;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Message;
    goto :goto_5

    .line 153
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    .restart local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    :cond_9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
