.class final Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;
.super Ljava/lang/Object;
.source "NetworkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/NetworkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "NetworkSlotInfo"
.end annotation


# instance fields
.field mChannelCount:I

.field public networkIndex:I

.field public networkSlotNumber:I

.field final synthetic this$0:Lcom/dsi/ant/channel/NetworkController;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/channel/NetworkController;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->this$0:Lcom/dsi/ant/channel/NetworkController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/channel/NetworkController;B)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/channel/NetworkController;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;-><init>(Lcom/dsi/ant/channel/NetworkController;)V

    return-void
.end method


# virtual methods
.method public final isNetworkSlotFree(I)Z
    .locals 1
    .param p1, "networkIndex"    # I

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->isNetworkSlotInUse()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->networkIndex:I

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNetworkSlotInUse()Z
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/dsi/ant/channel/NetworkController$NetworkSlotInfo;->mChannelCount:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
