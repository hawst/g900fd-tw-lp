.class public Lcom/dsi/ant/channel/BackgroundScanController;
.super Ljava/lang/Object;
.source "BackgroundScanController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/BackgroundScanController$1;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

.field private mBackgroundScanChannelTimeout:Z

.field private mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

.field mBackgroundScanningChannel:I

.field private mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;


# direct methods
.method public constructor <init>([Lcom/dsi/ant/channel/AntChannelImpl;Lcom/dsi/ant/adapter/AdapterHandle;)V
    .locals 1
    .param p1, "channels"    # [Lcom/dsi/ant/channel/AntChannelImpl;
    .param p2, "adapterHandle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-class v0, Lcom/dsi/ant/channel/BackgroundScanController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->TAG:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanChannelTimeout:Z

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanningChannel:I

    .line 69
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BackgroundScanState;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    .line 70
    iput-object p1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .line 71
    iput-object p2, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 72
    return-void
.end method

.method private sendBackgroundScanStateChange()V
    .locals 5

    .prologue
    .line 229
    iget-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .local v0, "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 230
    iget-object v1, v4, Lcom/dsi/ant/channel/AntChannelImpl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .line 232
    .local v1, "eventHandler":Lcom/dsi/ant/channel/IAntAdapterEventHandler;
    if-eqz v1, :cond_0

    .line 233
    iget-object v4, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-interface {v1, v4}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V

    .line 229
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 236
    .end local v1    # "eventHandler":Lcom/dsi/ant/channel/IAntAdapterEventHandler;
    :cond_1
    return-void
.end method


# virtual methods
.method public final addBackgroundScanningChannel$13462e()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/BackgroundScanState;->isConfigured()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/BackgroundScanState;->setConfigured(Z)V

    .line 197
    invoke-direct {p0}, Lcom/dsi/ant/channel/BackgroundScanController;->sendBackgroundScanStateChange()V

    .line 199
    :cond_0
    return-void
.end method

.method public final clearBackgroundScanInProgress(I)V
    .locals 2
    .param p1, "channelNumber"    # I

    .prologue
    .line 181
    iget v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanningChannel:I

    if-ne p1, v0, :cond_0

    .line 182
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanningChannel:I

    .line 183
    iget-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/BackgroundScanState;->setInProgress(Z)V

    .line 185
    invoke-direct {p0}, Lcom/dsi/ant/channel/BackgroundScanController;->sendBackgroundScanStateChange()V

    .line 187
    :cond_0
    return-void
.end method

.method public final getBackgroundScanState()Lcom/dsi/ant/channel/BackgroundScanState;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    return-object v0
.end method

.method public final handleBackgroundScanData(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z
    .locals 6
    .param p1, "channelNumber"    # I
    .param p2, "antMessage"    # Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    const/4 v0, 0x1

    .line 88
    .local v0, "passMessageToChannel":Z
    sget-object v3, Lcom/dsi/ant/channel/BackgroundScanController$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 96
    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :goto_0
    return v0

    .line 90
    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :pswitch_0
    check-cast p2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    sget-object v3, Lcom/dsi/ant/channel/BackgroundScanController$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    :goto_1
    move v0, v1

    .line 92
    :goto_2
    goto :goto_0

    .line 90
    :pswitch_1
    iput-boolean v1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanChannelTimeout:Z

    iget-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->TAG:Ljava/lang/String;

    const-string v3, "Background scanning timeout received."

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_2

    :pswitch_2
    iget-boolean v3, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanChannelTimeout:Z

    if-ne v3, v1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Attempting to reopen background scanning channel #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " after timeout event."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    new-instance v3, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;

    invoke-direct {v3}, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;-><init>()V

    iget-object v4, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    aget-object v4, v4, p1

    iget v4, v4, Lcom/dsi/ant/channel/AntChannelImpl;->networkNumber:I

    invoke-virtual {v3, p1, v4}, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;->getRawMessage(II)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/dsi/ant/adapter/AdapterHandle;->txCommand([B)[B

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/dsi/ant/channel/BackgroundScanController;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failure Response Code: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v2

    goto :goto_2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanController;->clearBackgroundScanInProgress(I)V

    goto :goto_1

    .line 94
    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :pswitch_3
    check-cast p2, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    if-ne v3, v4, :cond_2

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v3

    const/16 v4, 0x4b

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanChannelTimeout:Z

    if-eqz v3, :cond_2

    iput-boolean v2, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanChannelTimeout:Z

    move v0, v2

    :goto_3
    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_3

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 90
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final removeBackgroundScanningChannel$13462e()V
    .locals 6

    .prologue
    .line 207
    iget-object v4, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/BackgroundScanState;->isConfigured()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 208
    const/4 v1, 0x0

    .line 210
    .local v1, "foundConfiguredChannel":Z
    iget-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;

    .local v0, "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 211
    iget-boolean v4, v4, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v4, :cond_2

    .line 212
    const/4 v1, 0x1

    .line 217
    :cond_0
    if-nez v1, :cond_1

    .line 218
    iget-object v4, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/BackgroundScanState;->setConfigured(Z)V

    .line 220
    invoke-direct {p0}, Lcom/dsi/ant/channel/BackgroundScanController;->sendBackgroundScanStateChange()V

    .line 223
    .end local v0    # "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    .end local v1    # "foundConfiguredChannel":Z
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    return-void

    .line 210
    .restart local v0    # "arr$":[Lcom/dsi/ant/channel/AntChannelImpl;
    .restart local v1    # "foundConfiguredChannel":Z
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final setBackgroundScanInProgress(I)Z
    .locals 3
    .param p1, "channelNumber"    # I

    .prologue
    .line 162
    const/4 v0, 0x0

    .line 164
    .local v0, "canSet":Z
    iget-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/BackgroundScanState;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    iput p1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanningChannel:I

    .line 166
    iget-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanController;->mBackgroundScanState:Lcom/dsi/ant/channel/BackgroundScanState;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/BackgroundScanState;->setInProgress(Z)V

    .line 168
    const/4 v0, 0x1

    .line 170
    invoke-direct {p0}, Lcom/dsi/ant/channel/BackgroundScanController;->sendBackgroundScanStateChange()V

    .line 173
    :cond_0
    return v0
.end method
