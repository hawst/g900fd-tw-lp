.class public final Lcom/dsi/ant/channel/AntChannel;
.super Ljava/lang/Object;
.source "AntChannel.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/AntChannel;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile mAllowChannelEvents:Z

.field private final mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

.field private final mChannelEventDispatcher:Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/AntChannel;->TAG:Ljava/lang/String;

    .line 1032
    new-instance v0, Lcom/dsi/ant/channel/AntChannel$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/AntChannel$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/AntChannel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;)V
    .locals 2
    .param p1, "communicator"    # Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;-><init>(Lcom/dsi/ant/channel/AntChannel;B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mChannelEventDispatcher:Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;

    .line 74
    iput-boolean v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    .line 89
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel communicator provided  was null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    .line 95
    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/AntChannel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/AntChannel;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    return v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1016
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    instance-of v0, v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1026
    :goto_0
    return-void

    .line 1024
    :cond_0
    sget-object v0, Lcom/dsi/ant/channel/AntChannel;->TAG:Ljava/lang/String;

    const-string v1, "Could not parcel, unknown IPC communicator type"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
