.class public final Lcom/dsi/ant/channel/AntChannelProviderImpl;
.super Ljava/lang/Object;
.source "AntChannelProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/AntChannelProviderImpl$2;,
        Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

.field private final mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

.field private final mChannelsControllerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/channel/ChannelsController;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceIsInitialised:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mServiceIsInitialised:Z

    .line 112
    new-instance v0, Lcom/dsi/ant/channel/AntChannelProviderImpl$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/AntChannelProviderImpl$1;-><init>(Lcom/dsi/ant/channel/AntChannelProviderImpl;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    .line 99
    new-instance v0, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;-><init>(Lcom/dsi/ant/channel/AntChannelProviderImpl;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/AntChannelProviderImpl;Z)V
    .locals 4
    .param p0, "x0"    # Lcom/dsi/ant/channel/AntChannelProviderImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v0

    invoke-virtual {p0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isLegacyInterfaceInUse()Z

    move-result v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.action.CHANNEL_PROVIDER_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_NUM_CHANNELS_AVAILABLE"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_NEW_CHANNELS_AVAILABLE"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_LEGACY_INTERFACE_IN_USE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v1, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<Intent> ACTION_CHANNEL_PROVIDER_STATE_CHANGED sent, number of channels available: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/AntChannelProviderImpl;)Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    return-object v0
.end method

.method private acquireChannel(Lcom/dsi/ant/channel/SelectedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;
    .locals 14
    .param p1, "whichNetwork"    # Lcom/dsi/ant/channel/SelectedNetwork;
    .param p2, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "libraryVersionCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    .line 140
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Acquiring channel..."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Requested network="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 142
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Required capabilites="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 143
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Desired capabilites="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 145
    iget-boolean v12, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mServiceIsInitialised:Z

    if-nez v12, :cond_0

    .line 146
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Service is not initialized"

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v13, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->SERVICE_INITIALIZING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v12, v13}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 151
    :cond_0
    const/4 v9, 0x0

    .line 153
    .local v9, "preferredChannelController":Lcom/dsi/ant/channel/ChannelsController;
    const/4 v8, 0x0

    .line 155
    .local v8, "preferredChannel":Lcom/dsi/ant/channel/AntChannelImpl;
    const/4 v3, 0x0

    .line 156
    .local v3, "foundAdapter":Z
    const/4 v4, 0x0

    .line 157
    .local v4, "foundAdapterWithCapabilities":Z
    const/4 v5, 0x0

    .line 158
    .local v5, "foundFreeChannelWithCapabilities":Z
    const/4 v6, 0x0

    .line 160
    .local v6, "foundReleasingChannelWithCapabilities":Z
    const/4 v11, 0x0

    .line 162
    .local v11, "wholeChipInterfaceInUse":Z
    iget-object v13, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    monitor-enter v13

    .line 163
    :try_start_0
    iget-object v12, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_6

    .line 164
    const/4 v3, 0x1

    .line 166
    iget-object v12, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/ChannelsController;

    .line 167
    .local v2, "controller":Lcom/dsi/ant/channel/ChannelsController;
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/dsi/ant/channel/ChannelsController;->hasCapabilities(Lcom/dsi/ant/channel/Capabilities;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 168
    const/4 v4, 0x1

    .line 170
    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->canClaimAdapter()Z

    move-result v12

    if-nez v12, :cond_2

    .line 171
    const/4 v11, 0x1

    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->weakClaimAdapter()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->getNumChannelsAvailable()I

    move-result v12

    if-lez v12, :cond_5

    .line 177
    const/4 v5, 0x1

    .line 179
    invoke-virtual {v2, p1}, Lcom/dsi/ant/channel/ChannelsController;->isNetworkAvailable(Lcom/dsi/ant/channel/SelectedNetwork;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 181
    if-nez v9, :cond_3

    .line 182
    move-object v9, v2

    goto :goto_0

    .line 184
    :cond_3
    move-object/from16 v0, p3

    invoke-virtual {v9, v2, v0}, Lcom/dsi/ant/channel/ChannelsController;->isPreferred(Lcom/dsi/ant/channel/ChannelsController;Lcom/dsi/ant/channel/Capabilities;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 185
    invoke-virtual {v9}, Lcom/dsi/ant/channel/ChannelsController;->releaseAdapterClaimIfNotInUse()V

    .line 188
    move-object v9, v2

    goto :goto_0

    .line 190
    :cond_4
    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->releaseAdapterClaimIfNotInUse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 209
    .end local v2    # "controller":Lcom/dsi/ant/channel/ChannelsController;
    .end local v7    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v12

    monitor-exit v13

    throw v12

    .line 194
    .restart local v2    # "controller":Lcom/dsi/ant/channel/ChannelsController;
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_5
    :try_start_1
    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->hasChannelReleasing()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 195
    const/4 v6, 0x1

    goto :goto_0

    .line 201
    .end local v2    # "controller":Lcom/dsi/ant/channel/ChannelsController;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_6
    if-eqz v9, :cond_7

    .line 203
    invoke-virtual {v9}, Lcom/dsi/ant/channel/ChannelsController;->strongClaimAdapter()Z

    .line 206
    invoke-virtual {v9, p1}, Lcom/dsi/ant/channel/ChannelsController;->acquireChannel(Lcom/dsi/ant/channel/SelectedNetwork;)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v8

    .line 209
    :cond_7
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    if-nez v8, :cond_e

    .line 212
    if-nez v3, :cond_9

    .line 214
    invoke-static {}, Lcom/dsi/ant/power/PowerManager;->tryEnable()Lcom/dsi/ant/power/PowerManager$CanEnableState;

    move-result-object v10

    .line 215
    .local v10, "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_ADAPTERS_EXIST:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 217
    .local v1, "channelNotAvailableReason":Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl$2;->$SwitchMap$com$dsi$ant$power$PowerManager$CanEnableState:[I

    invoke-virtual {v10}, Lcom/dsi/ant/power/PowerManager$CanEnableState;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 241
    :goto_1
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    invoke-direct {v12, v1}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 219
    :pswitch_0
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Adapter currently disabled, attempting auto-enable."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->SERVICE_INITIALIZING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 222
    goto :goto_1

    .line 224
    :pswitch_1
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Adapter is disabled because airplane mode is on."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const v12, 0x9dd2

    move/from16 v0, p4

    if-lt v0, v12, :cond_8

    .line 228
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ANT_DISABLED_AIRPLANE_MODE_ON:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    goto :goto_1

    .line 231
    :cond_8
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_ADAPTERS_EXIST:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 233
    goto :goto_1

    .line 235
    :pswitch_2
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Can\'t enable adapter during error recovery cooldown"

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 238
    :pswitch_3
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "No adapter."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 242
    .end local v1    # "channelNotAvailableReason":Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    .end local v10    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    :cond_9
    if-nez v4, :cond_a

    .line 243
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "No adapter with capabilities."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v13, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_CHANNELS_MATCH_CRITERIA:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v12, v13}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 247
    :cond_a
    if-nez v5, :cond_d

    .line 248
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "No channel that matched the given criteria."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    if-eqz v6, :cond_b

    .line 251
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "..but one is releasing"

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v13, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->RELEASE_PROCESSING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v12, v13}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 257
    :cond_b
    if-eqz v11, :cond_c

    .line 259
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/dsi/ant/legacy/ClaimManager;->requestForceUnClaim()Z

    .line 261
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v13, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE_LEGACY:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v12, v13}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 265
    :cond_c
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v13, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v12, v13}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 269
    :cond_d
    sget-object v12, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "No free network slot."

    invoke-static {v12, v13}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    new-instance v12, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    sget-object v13, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NETWORK_NOT_AVAILABLE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-direct {v12, v13}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V

    throw v12

    .line 276
    :cond_e
    return-object v8

    .line 217
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;
    .locals 2
    .param p1, "whichNetwork"    # I
    .param p2, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "libraryVersionCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    .line 281
    new-instance v0, Lcom/dsi/ant/channel/SelectedNetwork;

    invoke-static {p1}, Lcom/dsi/ant/channel/PredefinedNetwork;->create(I)Lcom/dsi/ant/channel/PredefinedNetwork;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/SelectedNetwork;-><init>(Lcom/dsi/ant/channel/PredefinedNetwork;)V

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->acquireChannel(Lcom/dsi/ant/channel/SelectedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    return-object v0
.end method

.method public final acquireChannel(Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;
    .locals 2
    .param p1, "networkKey"    # Lcom/dsi/ant/channel/NetworkKey;
    .param p2, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "libraryVersionCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;
        }
    .end annotation

    .prologue
    .line 289
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    .line 291
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Received network key that is null or has invalid length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_1
    new-instance v0, Lcom/dsi/ant/channel/SelectedNetwork;

    invoke-direct {v0, p1}, Lcom/dsi/ant/channel/SelectedNetwork;-><init>(Lcom/dsi/ant/channel/NetworkKey;)V

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->acquireChannel(Lcom/dsi/ant/channel/SelectedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    return-object v0
.end method

.method public final addAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V
    .locals 3
    .param p1, "adapterHandle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 331
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 333
    :try_start_0
    new-instance v0, Lcom/dsi/ant/channel/ChannelsController;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    invoke-direct {v0, p1, v1}, Lcom/dsi/ant/channel/ChannelsController;-><init>(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;)V

    .line 336
    .local v0, "controller":Lcom/dsi/ant/channel/ChannelsController;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    iget-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mServiceIsInitialised:Z

    if-nez v1, :cond_0

    .line 341
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mServiceIsInitialised:Z

    .line 342
    sget-object v1, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 345
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/adapter/AdapterHandle;->hasClaim()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/ChannelsController;->setHasUseOfAdapter(Z)V

    .line 346
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .end local v0    # "controller":Lcom/dsi/ant/channel/ChannelsController;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->destroy()V

    .line 427
    return-void
.end method

.method public final getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I
    .locals 5
    .param p1, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 390
    const/4 v1, 0x0

    .line 392
    .local v1, "i":I
    iget-object v4, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 393
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ChannelsController;

    .line 394
    .local v0, "channelsController":Lcom/dsi/ant/channel/ChannelsController;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelsController;->canClaimAdapter()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/ChannelsController;->hasCapabilities(Lcom/dsi/ant/channel/Capabilities;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 396
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelsController;->getNumChannelsAvailable()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    .line 399
    .end local v0    # "channelsController":Lcom/dsi/ant/channel/ChannelsController;
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    return v1

    .line 399
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public final isLegacyInterfaceInUse()Z
    .locals 4

    .prologue
    .line 408
    const/4 v1, 0x0

    .line 410
    .local v1, "wholeChipInterfaceInUse":Z
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 411
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/ChannelsController;

    .line 413
    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->canClaimAdapter()Z

    move-result v2

    if-nez v2, :cond_0

    .line 414
    const/4 v1, 0x1

    .line 418
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    return v1

    .line 418
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public final isServiceInitialised()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mServiceIsInitialised:Z

    return v0
.end method

.method public final removeAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V
    .locals 8
    .param p1, "adapterHandle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 355
    const/4 v0, 0x0

    .line 357
    .local v0, "controller":Lcom/dsi/ant/channel/ChannelsController;
    iget-object v4, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 358
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/ChannelsController;

    .line 359
    .local v2, "possibleController":Lcom/dsi/ant/channel/ChannelsController;
    invoke-virtual {v2, p1}, Lcom/dsi/ant/channel/ChannelsController;->usesAdapterHandle(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 360
    move-object v0, v2

    .line 365
    .end local v2    # "possibleController":Lcom/dsi/ant/channel/ChannelsController;
    :cond_1
    if-eqz v0, :cond_3

    .line 366
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mChannelsControllerList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 368
    invoke-virtual {p1}, Lcom/dsi/ant/adapter/AdapterHandle;->hasClaim()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 369
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelsController;->adapterRemoved()V

    .line 370
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 376
    :cond_2
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelsController;->destroy()V

    .line 377
    sget-object v3, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Removed controller "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 379
    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public final setServiceInitialised()V
    .locals 1

    .prologue
    .line 318
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getClaimManager()Lcom/dsi/ant/legacy/ClaimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/legacy/ClaimManager;->clearInterfaceClaimWhenInitialized()V

    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->mServiceIsInitialised:Z

    .line 321
    sget-object v0, Lcom/dsi/ant/channel/AntChannelProviderImpl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 322
    return-void
.end method
