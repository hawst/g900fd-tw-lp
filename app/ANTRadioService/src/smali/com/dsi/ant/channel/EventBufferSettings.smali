.class public final Lcom/dsi/ant/channel/EventBufferSettings;
.super Ljava/lang/Object;
.source "EventBufferSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/EventBufferSettings$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/EventBufferSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;

.field public static final DISABLE_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;


# instance fields
.field private mBufferTimeMilliseconds:I

.field private mBundleData:Lcom/dsi/ant/channel/EventBufferSettings$BundleData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/dsi/ant/channel/EventBufferSettings;

    const/16 v1, 0x7d0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/EventBufferSettings;-><init>(I)V

    sput-object v0, Lcom/dsi/ant/channel/EventBufferSettings;->DEFAULT_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;

    .line 58
    new-instance v0, Lcom/dsi/ant/channel/EventBufferSettings;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/EventBufferSettings;-><init>(I)V

    sput-object v0, Lcom/dsi/ant/channel/EventBufferSettings;->DISABLE_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;

    .line 249
    new-instance v0, Lcom/dsi/ant/channel/EventBufferSettings$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/EventBufferSettings$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/EventBufferSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    const/16 v0, 0x7d0

    invoke-direct {p0, v0}, Lcom/dsi/ant/channel/EventBufferSettings;-><init>(I)V

    .line 73
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2
    .param p1, "maxTimeMilliseconds"    # I

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    new-instance v0, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBundleData:Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    .line 83
    const v0, 0x9fff6

    invoke-static {p1, v1, v0}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Buffer time out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iput p1, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    .line 88
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    new-instance v1, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;-><init>(B)V

    iput-object v1, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBundleData:Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 200
    .local v0, "version":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    .line 203
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v2, "com.dsi.ant.channel.eventbuffersettings.bundledata"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBundleData:Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    .line 206
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/EventBufferSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 122
    if-ne p0, p1, :cond_1

    .line 140
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 126
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 127
    goto :goto_0

    .line 130
    :cond_2
    instance-of v2, p1, Lcom/dsi/ant/channel/EventBufferSettings;

    if-nez v2, :cond_3

    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_3
    check-cast p1, Lcom/dsi/ant/channel/EventBufferSettings;

    .line 136
    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p1, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    iget v3, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 137
    goto :goto_0
.end method

.method public final getEventBufferTime()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    add-int/lit16 v0, v0, 0xd9

    .line 116
    return v0
.end method

.method public final setEventBufferTime(I)V
    .locals 0
    .param p1, "timeMilliseconds"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Event Buffer Settings:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 148
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, " -Buffer Time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 227
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 230
    iget v0, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBufferTimeMilliseconds:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.dsi.ant.channel.eventbuffersettings.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/EventBufferSettings;->mBundleData:Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 236
    :cond_0
    return-void
.end method
