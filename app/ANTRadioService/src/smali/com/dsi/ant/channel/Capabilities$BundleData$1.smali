.class final Lcom/dsi/ant/channel/Capabilities$BundleData$1;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/Capabilities$BundleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 431
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    new-instance v1, Lcom/dsi/ant/channel/Capabilities$BundleData;

    invoke-direct {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;-><init>()V

    sget-object v2, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->RSSI:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v2

    aget-boolean v2, v0, v2

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v1, v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$002(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    sget-object v2, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->WILDCARD_ID_LIST:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v2

    aget-boolean v2, v0, v2

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v1, v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$102(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    sget-object v2, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->EVENT_BUFFERING:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v2

    aget-boolean v0, v0, v2

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v1, v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$202(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v1, v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$302(Lcom/dsi/ant/channel/Capabilities$BundleData;I)I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v1, v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$402(Lcom/dsi/ant/channel/Capabilities$BundleData;I)I

    return-object v1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 431
    const/4 v0, 0x0

    return-object v0
.end method
