.class final Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;
.super Ljava/lang/Object;
.source "ServiceResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/ServiceResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BundleData"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mChannelExists:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 225
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->mChannelExists:Z

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>()V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x1

    .line 221
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 222
    iget-boolean v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->mChannelExists:Z

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    return-void

    .line 222
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
