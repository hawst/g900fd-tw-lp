.class public final enum Lcom/dsi/ant/channel/AntCommandFailureReason;
.super Ljava/lang/Enum;
.source "AntCommandFailureReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/AntCommandFailureReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum BACKGROUND_SCAN_IN_USE:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum CANCELLED:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field public static final enum UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field private static final sValues:[Lcom/dsi/ant/channel/AntCommandFailureReason;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 21
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 25
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "IO_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 33
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "CHANNEL_RESPONSE"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 37
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "CHANNEL_RELEASING"

    invoke-direct {v0, v1, v6, v6}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 41
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "INVALID_REQUEST"

    invoke-direct {v0, v1, v7, v7}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 49
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "TRANSFER_IN_PROGRESS"

    invoke-direct {v0, v1, v8, v8}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 59
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "TRANSFER_FAILED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 63
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "BACKGROUND_SCAN_IN_USE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->BACKGROUND_SCAN_IN_USE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 67
    new-instance v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-string v1, "CANCELLED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/AntCommandFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->CANCELLED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/dsi/ant/channel/AntCommandFailureReason;

    const/4 v1, 0x0

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v2, v0, v1

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->BACKGROUND_SCAN_IN_USE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->CANCELLED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->$VALUES:[Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 73
    invoke-static {}, Lcom/dsi/ant/channel/AntCommandFailureReason;->values()[Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->sValues:[Lcom/dsi/ant/channel/AntCommandFailureReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/channel/AntCommandFailureReason;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/channel/AntCommandFailureReason;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 95
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 97
    .local v0, "code":Lcom/dsi/ant/channel/AntCommandFailureReason;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->sValues:[Lcom/dsi/ant/channel/AntCommandFailureReason;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 98
    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->sValues:[Lcom/dsi/ant/channel/AntCommandFailureReason;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 99
    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->sValues:[Lcom/dsi/ant/channel/AntCommandFailureReason;

    aget-object v0, v2, v1

    .line 104
    :cond_0
    return-object v0

    .line 98
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 97
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/AntCommandFailureReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/AntCommandFailureReason;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/AntCommandFailureReason;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->$VALUES:[Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/AntCommandFailureReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/AntCommandFailureReason;

    return-object v0
.end method


# virtual methods
.method public final getRawValue()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/dsi/ant/channel/AntCommandFailureReason;->mRawValue:I

    return v0
.end method
