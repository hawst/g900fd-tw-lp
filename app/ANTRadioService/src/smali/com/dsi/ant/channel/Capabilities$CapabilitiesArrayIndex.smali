.class final enum Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;
.super Ljava/lang/Enum;
.source "Capabilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CapabilitiesArrayIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

.field public static final enum BACKGROUND_SCANNING:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

.field public static final enum EXTENDED_ASSIGN:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

.field public static final enum FREQUENCY_AGILITY:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

.field public static final enum NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

.field public static final enum RX_MESSAGE_TIMESTAMP:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 485
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    const-string v1, "RX_MESSAGE_TIMESTAMP"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->RX_MESSAGE_TIMESTAMP:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    .line 486
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    const-string v1, "EXTENDED_ASSIGN"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->EXTENDED_ASSIGN:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    .line 487
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    const-string v1, "BACKGROUND_SCANNING"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->BACKGROUND_SCANNING:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    .line 488
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    const-string v1, "FREQUENCY_AGILITY"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->FREQUENCY_AGILITY:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    .line 489
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    const-string v1, "NUMBER_OF_CAPABILITIES"

    invoke-direct {v0, v1, v6}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    .line 483
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->RX_MESSAGE_TIMESTAMP:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->EXTENDED_ASSIGN:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->BACKGROUND_SCANNING:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->FREQUENCY_AGILITY:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 483
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 483
    const-class v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    return-object v0
.end method
