.class final synthetic Lcom/dsi/ant/channel/AntChannelImpl$1;
.super Ljava/lang/Object;
.source "AntChannelImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/AntChannelImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$dsi$ant$message$ChannelState:[I

.field static final synthetic $SwitchMap$com$dsi$ant$message$EventCode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 823
    invoke-static {}, Lcom/dsi/ant/message/EventCode;->values()[Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    :try_start_0
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_COMPLETED:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_FAILED:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    sget-object v1, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    sget-object v1, Lcom/dsi/ant/message/EventCode;->UNKNOWN:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    .line 685
    :goto_3
    invoke-static {}, Lcom/dsi/ant/message/ChannelState;->values()[Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    :try_start_4
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
