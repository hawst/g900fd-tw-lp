.class final Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;
.super Ljava/lang/Object;
.source "AntChannelImplWrapperAidl.java"

# interfaces
.implements Lcom/dsi/ant/channel/IAntAdapterEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;


# direct methods
.method constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)V
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V
    .locals 3
    .param p1, "newBackgroundScanState"    # Lcom/dsi/ant/channel/BackgroundScanState;

    .prologue
    .line 596
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 597
    .local v0, "data":Landroid/os/Bundle;
    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/BackgroundScanState;Landroid/os/Bundle;)V
    invoke-static {p1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$900(Lcom/dsi/ant/channel/BackgroundScanState;Landroid/os/Bundle;)V

    .line 599
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->BACKGROUND_SCAN_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    .line 600
    return-void
.end method

.method public final onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V
    .locals 3
    .param p1, "newBurstState"    # Lcom/dsi/ant/channel/BurstState;

    .prologue
    .line 588
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 589
    .local v0, "data":Landroid/os/Bundle;
    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/BurstState;Landroid/os/Bundle;)V
    invoke-static {p1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$800(Lcom/dsi/ant/channel/BurstState;Landroid/os/Bundle;)V

    .line 591
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->BURST_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    .line 592
    return-void
.end method

.method public final onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V
    .locals 3
    .param p1, "newEventBufferSettings"    # Lcom/dsi/ant/channel/EventBufferSettings;

    .prologue
    .line 604
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 605
    .local v0, "data":Landroid/os/Bundle;
    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V
    invoke-static {p1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$1000(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V

    .line 607
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->EVENT_BUFFER_SETTINGS_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    .line 608
    return-void
.end method

.method public final onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V
    .locals 3
    .param p1, "newLibConfig"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    .line 580
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 581
    .local v0, "data":Landroid/os/Bundle;
    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V
    invoke-static {p1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$700(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V

    .line 583
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->LIB_CONFIG_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    .line 584
    return-void
.end method
