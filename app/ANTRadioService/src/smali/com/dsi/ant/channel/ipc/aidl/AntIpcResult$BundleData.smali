.class final Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;
.super Ljava/lang/Object;
.source "AntIpcResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BundleData"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mBindersReturned:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field public mBundleReturned:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBundleReturned:Landroid/os/Bundle;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBindersReturned:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;-><init>()V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBundleReturned:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 115
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBindersReturned:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBinderList(Ljava/util/List;)V

    .line 118
    return-void
.end method
