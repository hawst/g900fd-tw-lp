.class final Lcom/dsi/ant/channel/SelectedNetwork;
.super Ljava/lang/Object;
.source "SelectedNetwork.java"


# instance fields
.field public final isPredefined:Z

.field private final mInvalidKeyNetworkIDGenerator:Ljava/util/Random;

.field public networkIndex:I

.field public final networkKey:Lcom/dsi/ant/channel/NetworkKey;

.field public final predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/NetworkKey;)V
    .locals 4
    .param p1, "networkKey"    # Lcom/dsi/ant/channel/NetworkKey;

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/channel/SelectedNetwork;->mInvalidKeyNetworkIDGenerator:Ljava/util/Random;

    .line 61
    iput-boolean v1, p0, Lcom/dsi/ant/channel/SelectedNetwork;->isPredefined:Z

    .line 62
    iput-object p1, p0, Lcom/dsi/ant/channel/SelectedNetwork;->networkKey:Lcom/dsi/ant/channel/NetworkKey;

    .line 63
    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->INVALID:Lcom/dsi/ant/channel/PredefinedNetwork;

    iput-object v2, p0, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 67
    invoke-static {p1}, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->getIndexWithNetworkKey(Lcom/dsi/ant/channel/NetworkKey;)I

    move-result v0

    .line 69
    .local v0, "tableIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 71
    iput v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->networkIndex:I

    .line 74
    invoke-static {v0, p1}, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->getNetworkKeyType(ILcom/dsi/ant/channel/NetworkKey;)Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    move-result-object v2

    .line 76
    sget-object v3, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->PUBLIC:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ANTPLUS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ANTFS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_3

    .line 77
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Network keys matching predefined networks are not allowed."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/channel/SelectedNetwork;->mInvalidKeyNetworkIDGenerator:Ljava/util/Random;

    invoke-virtual {p1}, Lcom/dsi/ant/channel/NetworkKey;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Random;->setSeed(J)V

    .line 85
    iget-object v1, p0, Lcom/dsi/ant/channel/SelectedNetwork;->mInvalidKeyNetworkIDGenerator:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    sget-object v2, Lcom/dsi/ant/channel/NetworkKeyStore;->CONVERSION_TABLE:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/dsi/ant/channel/SelectedNetwork;->networkIndex:I

    .line 88
    :cond_3
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/PredefinedNetwork;)V
    .locals 1
    .param p1, "predefinedNetwork"    # Lcom/dsi/ant/channel/PredefinedNetwork;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->mInvalidKeyNetworkIDGenerator:Ljava/util/Random;

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->isPredefined:Z

    .line 44
    iput-object p1, p0, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 47
    sget-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->PUBLIC:Lcom/dsi/ant/channel/PredefinedNetwork;

    if-ne p1, v0, :cond_0

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->networkIndex:I

    .line 51
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->networkKey:Lcom/dsi/ant/channel/NetworkKey;

    .line 52
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->isPredefined:Z

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->predefinedNetwork:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/SelectedNetwork;->networkKey:Lcom/dsi/ant/channel/NetworkKey;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/NetworkKey;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
