.class public Lcom/dsi/ant/channel/ipc/ServiceResult;
.super Ljava/lang/Object;
.source "ServiceResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/ServiceResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final INVALID_REQUEST_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

.field public static final NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

.field public static final SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;


# instance fields
.field private mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

.field private mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

.field private mDetailMessage:Ljava/lang/String;

.field private mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field private mSuccess:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Z)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 44
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Z)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 48
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->INVALID_REQUEST_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 319
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 131
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/message/MessageUtils;->booleanFromNumber(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/channel/AntCommandFailureReason;->create(I)Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    const-class v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    const/4 v0, 0x1

    if-le v1, v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.dsi.ant.channel.ipc.serviceresult.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V
    .locals 3
    .param p1, "failureReason"    # Lcom/dsi/ant/channel/AntCommandFailureReason;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    iput-object v2, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 73
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v0, p1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel Response failure type only valid when the response is provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANT Service responded with failure reason: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 79
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 80
    iput-object v2, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)V
    .locals 2
    .param p1, "responseMessage"    # Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 90
    if-nez p1, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel Response failure type only valid when the response is provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANT Adapter responded with failure code: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v1, "<null>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 96
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 97
    new-instance v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-direct {v0, p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 98
    return-void

    .line 95
    :cond_1
    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getRawResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "channelExists"    # Z

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 55
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    iput-boolean p1, v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->mChannelExists:Z

    .line 57
    if-eqz p1, :cond_0

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 59
    const-string v0, "Success"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 65
    :goto_0
    return-void

    .line 61
    :cond_0
    const-string v0, "Channel Does Not Exist"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return v0
.end method

.method public final getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    return-object v0
.end method

.method public final isSuccess()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final writeTo(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "error"    # Landroid/os/Bundle;

    .prologue
    .line 123
    const-string v0, "com.dsi.ant.serviceerror"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 124
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 259
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 262
    iget-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailureReason;->getRawValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 265
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.dsi.ant.channel.ipc.serviceresult.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 268
    :cond_0
    return-void

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
