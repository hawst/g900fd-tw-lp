.class public final enum Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;
.super Ljava/lang/Enum;
.source "AntChannelImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/AntChannelImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AcquiredState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

.field public static final enum ACQUIREDSTATE_ACQUIRED:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

.field public static final enum ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

.field public static final enum ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    new-instance v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    const-string v1, "ACQUIREDSTATE_ACQUIRED"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_ACQUIRED:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 144
    new-instance v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    const-string v1, "ACQUIREDSTATE_AVAILABLE"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 145
    new-instance v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    const-string v1, "ACQUIREDSTATE_RELEASING"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 142
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    sget-object v1, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_ACQUIRED:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->$VALUES:[Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    const-class v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->$VALUES:[Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    return-object v0
.end method
