.class final Lcom/dsi/ant/channel/Capabilities$1;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/Capabilities;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 536
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    new-array v1, v1, [Z

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    new-instance v2, Lcom/dsi/ant/channel/Capabilities;

    invoke-direct {v2}, Lcom/dsi/ant/channel/Capabilities;-><init>()V

    sget-object v3, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->RX_MESSAGE_TIMESTAMP:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v1, v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z
    invoke-static {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->access$502(Lcom/dsi/ant/channel/Capabilities;Z)Z

    sget-object v3, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->BACKGROUND_SCANNING:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v1, v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z
    invoke-static {v2, v3}, Lcom/dsi/ant/channel/Capabilities;->access$602(Lcom/dsi/ant/channel/Capabilities;Z)Z

    sget-object v3, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->FREQUENCY_AGILITY:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v1, v1, v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z
    invoke-static {v2, v1}, Lcom/dsi/ant/channel/Capabilities;->access$702(Lcom/dsi/ant/channel/Capabilities;Z)Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I
    invoke-static {v2, v1}, Lcom/dsi/ant/channel/Capabilities;->access$802(Lcom/dsi/ant/channel/Capabilities;I)I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/channel/Capabilities$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.dsi.ant.channel.capabilities.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities$BundleData;

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;
    invoke-static {v2, v0}, Lcom/dsi/ant/channel/Capabilities;->access$902(Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities$BundleData;)Lcom/dsi/ant/channel/Capabilities$BundleData;

    :cond_0
    return-object v2
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 536
    new-array v0, p1, [Lcom/dsi/ant/channel/Capabilities;

    return-object v0
.end method
