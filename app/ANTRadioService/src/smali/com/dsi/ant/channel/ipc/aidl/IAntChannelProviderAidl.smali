.class public interface abstract Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;
.super Ljava/lang/Object;
.source "IAntChannelProviderAidl.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;
    }
.end annotation


# virtual methods
.method public abstract acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract acquireChannelKey$2ece064f()Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isLegacyInterfaceInUse()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
