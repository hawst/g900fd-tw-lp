.class final Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;
.super Ljava/lang/Object;
.source "AntChannelImplWrapperAidl.java"

# interfaces
.implements Lcom/dsi/ant/channel/IAntChannelEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;


# direct methods
.method constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)V
    .locals 0

    .prologue
    .line 529
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChannelDeath()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 545
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->CHANNEL_DEATH:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    invoke-static {v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$400(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;)V

    .line 547
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$500(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$500(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->setAdapterEventHandler(Lcom/dsi/ant/channel/IAntAdapterEventHandler;)V

    .line 549
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$500(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 552
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-static {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$600(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)V

    .line 553
    return-void
.end method

.method public final onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 3
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 537
    new-instance v0, Landroid/os/Bundle;

    const-class v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 538
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.data.antmessageparcel"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 540
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->RX_ANT_MESSAGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    .line 541
    return-void
.end method
