.class final Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData$1;
.super Ljava/lang/Object;
.source "ServiceResult.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(B)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->booleanFromNumber(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->mChannelExists:Z

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method
