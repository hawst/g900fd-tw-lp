.class public Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
.super Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;
.source "AntChannelImplWrapperAidl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$3;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

.field private mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

.field private mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

.field private final mChannelDeathNotifiers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;",
            ">;"
        }
    .end annotation
.end field

.field private mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;

.field private final mDeadMessengers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field

.field private final mEventReceivers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field

.field private final mEventReceiversRemove_LOCK:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/AntChannelImpl;)V
    .locals 2
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannelImpl;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;-><init>()V

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceiversRemove_LOCK:Ljava/lang/Object;

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;

    .line 529
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$1;-><init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .line 576
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$2;-><init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .line 126
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mDeadMessengers:Ljava/util/List;

    .line 128
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    .line 130
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 131
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->setAdapterEventHandler(Lcom/dsi/ant/channel/IAntAdapterEventHandler;)V

    .line 132
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/EventBufferSettings;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;)V
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$500(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Lcom/dsi/ant/channel/AntChannelImpl;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    return-object v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)V
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/message/LibConfig;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$800(Lcom/dsi/ant/channel/BurstState;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/BurstState;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/BurstState;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$900(Lcom/dsi/ant/channel/BackgroundScanState;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/BackgroundScanState;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/BackgroundScanState;Landroid/os/Bundle;)V

    return-void
.end method

.method private sendEvent(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "event"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 495
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 496
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 498
    .local v0, "eventMessage":Landroid/os/Message;
    invoke-virtual {p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->getRawValue()I

    move-result v3

    iput v3, v0, Landroid/os/Message;->what:I

    .line 499
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 501
    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceiversRemove_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 502
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    .local v2, "messenger":Landroid/os/Messenger;
    :try_start_1
    invoke-virtual {v2, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 508
    :catch_0
    move-exception v3

    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mDeadMessengers:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 521
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "messenger":Landroid/os/Messenger;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 517
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mDeadMessengers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 518
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    iget-object v5, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mDeadMessengers:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 519
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mDeadMessengers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 521
    :cond_1
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 527
    .end local v0    # "eventMessage":Landroid/os/Message;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private static writeToBundle(Lcom/dsi/ant/channel/BackgroundScanState;Landroid/os/Bundle;)V
    .locals 1
    .param p0, "backgroundScanState"    # Lcom/dsi/ant/channel/BackgroundScanState;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 567
    const-class v0, Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 568
    const-string v0, "com.dsi.ant.channel.data.backgroundscanstate"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569
    return-void
.end method

.method private static writeToBundle(Lcom/dsi/ant/channel/BurstState;Landroid/os/Bundle;)V
    .locals 1
    .param p0, "burstState"    # Lcom/dsi/ant/channel/BurstState;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 562
    const-class v0, Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 563
    const-string v0, "com.dsi.ant.channel.data.burststate"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 564
    return-void
.end method

.method private static writeToBundle(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V
    .locals 1
    .param p0, "eventBufferSettings"    # Lcom/dsi/ant/channel/EventBufferSettings;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 572
    const-class v0, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 573
    const-string v0, "com.dsi.ant.channel.data.eventbuffersettings"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 574
    return-void
.end method

.method private static writeToBundle(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V
    .locals 1
    .param p0, "libConfig"    # Lcom/dsi/ant/message/LibConfig;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 557
    const-class v0, Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 558
    const-string v0, "com.dsi.ant.channel.data.libconfig"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 559
    return-void
.end method


# virtual methods
.method public final acknowledgedTransfer([BLandroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 409
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 411
    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    .line 412
    return-void
.end method

.method public final addDeathNotifier(Landroid/os/IBinder;)V
    .locals 3
    .param p1, "deathNotifierBinder"    # Landroid/os/IBinder;

    .prologue
    .line 446
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 448
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;

    new-instance v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;

    invoke-direct {v2, p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;-><init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Landroid/os/IBinder;)V

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Added death notifier "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " from PID "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 467
    :goto_1
    return-void

    .line 455
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    .line 457
    if-eqz v0, :cond_0

    .line 458
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->releaseChannel()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 461
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1

    .line 465
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    goto :goto_1
.end method

.method public final addEventReceiver(Landroid/os/Messenger;)Z
    .locals 1
    .param p1, "eventReceiver"    # Landroid/os/Messenger;

    .prologue
    .line 613
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 615
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final burstTransfer([BLandroid/os/Bundle;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 417
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/AntChannelImpl;->burstTransfer([B)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 419
    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 426
    :goto_0
    return-void

    .line 423
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 424
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final cancelTransfer(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "error"    # Landroid/os/Bundle;

    .prologue
    .line 432
    :try_start_0
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 434
    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    :goto_0
    return-void

    .line 438
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 439
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final getCapabilities()Lcom/dsi/ant/channel/Capabilities;
    .locals 1

    .prologue
    .line 628
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v0, v0, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelsController;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 635
    :goto_0
    return-object v0

    .line 632
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 635
    new-instance v0, Lcom/dsi/ant/channel/Capabilities;

    invoke-direct {v0}, Lcom/dsi/ant/channel/Capabilities;-><init>()V

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "message"    # Landroid/os/Message;
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 656
    :try_start_0
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$3;->$SwitchMap$com$dsi$ant$channel$ipc$aidl$AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat:[I

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->create(I)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v0, :pswitch_data_0

    .line 684
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :goto_1
    return-object v0

    .line 658
    :pswitch_0
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.dsi.ant.channel.data.libconfig"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/LibConfig;

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/channel/AntChannelImpl;->setLibConfig(Lcom/dsi/ant/message/LibConfig;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 680
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 681
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0

    .line 660
    :pswitch_1
    :try_start_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.dsi.ant.channel.data.eventbuffersettings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/EventBufferSettings;

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    sget-object v2, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v2, v3, :cond_0

    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    :goto_2
    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_1

    :cond_0
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/channel/Capabilities;->hasEventBuffering()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    goto :goto_2

    :cond_1
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v1, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    int-to-byte v1, v1

    invoke-virtual {v2, v1, v0}, Lcom/dsi/ant/channel/ChannelsController;->setEventBufferVote(BLcom/dsi/ant/channel/EventBufferSettings;)Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-result-object v0

    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannelImpl;->createResult(Lcom/dsi/ant/message/fromant/ChannelResponseMessage;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    goto :goto_2

    .line 662
    :pswitch_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v1, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelsController;->getLibConfig()Lcom/dsi/ant/message/LibConfig;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V

    sget-object v1, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v1, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 664
    :pswitch_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v1, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelsController;->getBurstState()Lcom/dsi/ant/channel/BurstState;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/BurstState;Landroid/os/Bundle;)V

    sget-object v1, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v1, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 666
    :pswitch_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v1, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelsController;->getBackgroundScanState()Lcom/dsi/ant/channel/BackgroundScanState;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/BackgroundScanState;Landroid/os/Bundle;)V

    sget-object v1, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v1, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 668
    :pswitch_5
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/channel/Capabilities;->hasEventBuffering()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v1, Lcom/dsi/ant/channel/EventBufferSettings;->DISABLE_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;

    :goto_3
    invoke-static {v1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->writeToBundle(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V

    sget-object v1, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v1, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelsController;->getEventBufferSettings()Lcom/dsi/ant/channel/EventBufferSettings;

    move-result-object v1

    goto :goto_3

    .line 673
    :pswitch_6
    const-string v0, "ANT Radio Service"

    const-string v1, "Received unknown request from app"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 656
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final releaseChannel()V
    .locals 2

    .prologue
    .line 642
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 644
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannelImpl;->releaseChannel(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    :goto_0
    return-void

    .line 648
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    goto :goto_0
.end method

.method public final removeDeathNotifier(Landroid/os/IBinder;)V
    .locals 4
    .param p1, "deathNotifierBinder"    # Landroid/os/IBinder;

    .prologue
    .line 472
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 473
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;

    .line 474
    .local v0, "chanRef":Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;
    if-nez v0, :cond_0

    .line 475
    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Attempt to remove non-existant death notifier "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 477
    monitor-exit v2

    .line 487
    .end local v0    # "chanRef":Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;
    :goto_0
    return-void

    .line 479
    .restart local v0    # "chanRef":Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;
    :cond_0
    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Removed death notifier "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 480
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->cancelDeathWatch()V

    .line 481
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .end local v0    # "chanRef":Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2

    throw v1
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    .line 485
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v1}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    goto :goto_0
.end method

.method public final removeEventReceiver(Landroid/os/Messenger;)Z
    .locals 1
    .param p1, "eventReceiver"    # Landroid/os/Messenger;

    .prologue
    .line 620
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mEventReceivers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 622
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final requestResponse(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .locals 7
    .param p1, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 343
    new-instance v4, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v4, v5}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 344
    new-instance v3, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-direct {v3}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>()V

    .line 347
    .local v3, "requestedResponse":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :try_start_0
    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->createAntMessage$5f6f8477(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/message/fromhost/RequestMessage;

    .line 350
    .local v2, "requestMessage":Lcom/dsi/ant/message/fromhost/RequestMessage;
    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    sget-object v5, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_RELEASING:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iget-object v6, v4, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-ne v5, v6, :cond_0

    new-instance v4, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v4, v5}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 352
    :goto_0
    invoke-virtual {v4, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    .line 369
    .end local v2    # "requestMessage":Lcom/dsi/ant/message/fromhost/RequestMessage;
    :goto_1
    return-object v3

    .line 350
    .restart local v2    # "requestMessage":Lcom/dsi/ant/message/fromhost/RequestMessage;
    :cond_0
    invoke-virtual {v4, v2, v3}, Lcom/dsi/ant/channel/AntChannelImpl;->processRequestResponse(Lcom/dsi/ant/message/fromhost/RequestMessage;Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    goto :goto_0

    .line 353
    .end local v2    # "requestMessage":Lcom/dsi/ant/message/fromhost/RequestMessage;
    :catch_0
    move-exception v1

    .line 356
    .local v1, "npe":Ljava/lang/NullPointerException;
    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v4}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 357
    sget-object v4, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v4, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_1

    .line 358
    .end local v1    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 362
    .local v0, "exception":Ljava/lang/Exception;
    sget-object v4, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    const-string v5, "Request response crashed"

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v4}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 366
    sget-object v4, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v4, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final setBroadcastData([BLandroid/os/Bundle;)V
    .locals 2
    .param p1, "payload"    # [B
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 377
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/channel/AntChannelImpl;->setBroadcastData([B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 383
    .local v0, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_0
    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    .line 390
    .end local v0    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_1
    return-void

    .line 380
    :cond_0
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v0    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_0

    .line 387
    .end local v0    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v1}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 388
    sget-object v1, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v1, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final startAcknowledgedTransfer([BLandroid/os/Bundle;)V
    .locals 1
    .param p1, "payload"    # [B
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 395
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/AntChannelImpl;->startAcknowledgedTransfer([B)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 397
    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :goto_0
    return-void

    .line 401
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 402
    sget-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .param p2, "error"    # Landroid/os/Bundle;

    .prologue
    .line 137
    sget-object v6, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Received message over IPC. "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 139
    const/4 v4, 0x0

    .line 142
    .local v4, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannelImpl;->isReleasing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 143
    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RELEASING:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    invoke-virtual {v6, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    .line 339
    .end local v4    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_0
    return-void

    .line 147
    .restart local v4    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :cond_0
    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->createAntMessage$5f6f8477(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    move-result-object v2

    .line 149
    .local v2, "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    if-eqz v2, :cond_14

    .line 150
    sget-object v6, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$3;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 330
    .end local v2    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .end local v4    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_1
    :pswitch_0
    if-nez v4, :cond_1

    sget-object v4, Lcom/dsi/ant/channel/ipc/ServiceResult;->INVALID_REQUEST_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 332
    :cond_1
    invoke-virtual {v4, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 336
    :catch_0
    move-exception v6

    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v6}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 337
    sget-object v6, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v6, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0

    .line 152
    .restart local v2    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .restart local v4    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :pswitch_1
    :try_start_1
    move-object v0, v2

    check-cast v0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;

    move-object v3, v0

    .line 154
    .local v3, "dataMessage":Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;->getPayload()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->setBroadcastData([B)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 155
    sget-object v4, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    goto :goto_1

    .line 157
    :cond_2
    new-instance v5, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v6, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v5, v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .end local v4    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    .local v5, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    move-object v4, v5

    .line 159
    .local v4, "result":Ljava/lang/Object;
    goto :goto_1

    .line 161
    .end local v3    # "dataMessage":Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;
    .end local v5    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    .local v4, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :pswitch_2
    move-object v0, v2

    check-cast v0, Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;

    move-object v1, v0

    .line 163
    .local v1, "ackMessage":Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;->getPayload()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->startAcknowledgedTransfer([B)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 164
    goto :goto_1

    .line 172
    .end local v1    # "ackMessage":Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;
    :pswitch_3
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 173
    goto :goto_1

    .line 176
    :pswitch_4
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 177
    goto :goto_1

    .line 179
    :pswitch_5
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    move-object v0, v2

    check-cast v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;

    move-object v6, v0

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->getExtendedAssignment()Lcom/dsi/ant/message/ExtendedAssignment;

    move-result-object v6

    iget-object v7, v8, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/ChannelsController;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v9

    invoke-virtual {v6}, Lcom/dsi/ant/message/ExtendedAssignment;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v9}, Lcom/dsi/ant/channel/Capabilities;->hasExtendedAssign()Z

    move-result v7

    if-nez v7, :cond_3

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    .line 180
    :goto_2
    goto :goto_1

    .line 179
    :cond_3
    iget-boolean v7, v6, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    if-eqz v7, :cond_5

    invoke-virtual {v9}, Lcom/dsi/ant/channel/Capabilities;->hasBackgroundScanning()Z

    move-result v7

    if-nez v7, :cond_4

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    goto :goto_2

    :cond_4
    const/4 v7, 0x1

    iput-boolean v7, v8, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    :cond_5
    iget-boolean v6, v6, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    if-eqz v6, :cond_6

    invoke-virtual {v9}, Lcom/dsi/ant/channel/Capabilities;->hasFrequencyAgility()Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    goto :goto_2

    :cond_6
    invoke-virtual {v8, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v6

    if-nez v6, :cond_8

    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v9, "Assign aborted as assign failed"

    invoke-static {v6, v9}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    iput-boolean v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    :cond_7
    move-object v4, v7

    goto :goto_2

    :cond_8
    iget-boolean v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v6, :cond_9

    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v10, v8, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v6}, Lcom/dsi/ant/channel/ChannelsController;->addBackgroundScanningChannel$13462e()V

    :cond_9
    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v10, "Setting high priority search timeout to default"

    invoke-static {v6, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;

    sget-object v10, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DEFAULT:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-direct {v6, v10}, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;-><init>(Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V

    iget-object v10, v8, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    sget-object v11, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v10, v11}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v6}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v10

    if-nez v10, :cond_a

    iget-object v7, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v9, "Assign aborted as reset high priority search failed"

    invoke-static {v7, v9}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v7}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>()V

    invoke-virtual {v8, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-object v4, v6

    goto :goto_2

    :cond_a
    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v10, "Setting low priority search timeout to default"

    invoke-static {v6, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;

    sget-object v10, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->DEFAULT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-direct {v6, v10}, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;-><init>(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    iget-object v10, v8, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    sget-object v11, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOW_PRIORITY_SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v10, v11}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v6}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v10

    if-nez v10, :cond_b

    iget-object v7, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v9, "Assign aborted as reset low priority search failed"

    invoke-static {v7, v9}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v7}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>()V

    invoke-virtual {v8, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-object v4, v6

    goto/16 :goto_2

    :cond_b
    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v10, "Setting proximity search to default"

    invoke-static {v6, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;

    const/4 v10, 0x0

    invoke-direct {v6, v10}, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;-><init>(I)V

    iget-object v10, v8, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    sget-object v11, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->PROXIMITY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v10, v11}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v6}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v10

    if-nez v10, :cond_c

    iget-object v7, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v9, "Assign aborted as reset proximity search failed"

    invoke-static {v7, v9}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v7}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>()V

    invoke-virtual {v8, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-object v4, v6

    goto/16 :goto_2

    :cond_c
    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v6, v6, Lcom/dsi/ant/channel/ChannelsController;->versionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v6}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->needsTxPowerRemap()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v10, "Setting channel transmit power to default"

    invoke-static {v6, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;

    const/4 v10, 0x3

    invoke-direct {v6, v10, v9}, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;-><init>(ILcom/dsi/ant/channel/Capabilities;)V

    iget-object v9, v8, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    sget-object v10, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_CHANNEL_TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v9, v10}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v6}, Lcom/dsi/ant/channel/AntChannelImpl;->setTransmitPower(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v9

    if-nez v9, :cond_7

    iget-object v7, v8, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v9, "Assign aborted as reset transmit power failed"

    invoke-static {v7, v9}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v7}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>()V

    invoke-virtual {v8, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-object v4, v6

    goto/16 :goto_2

    .line 182
    :pswitch_6
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 183
    goto/16 :goto_1

    .line 185
    :pswitch_7
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 186
    goto/16 :goto_1

    .line 188
    :pswitch_8
    iget-object v7, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    move-object v0, v2

    check-cast v0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;

    move-object v6, v0

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->getRfFrequency()I

    move-result v6

    invoke-virtual {v7, v6}, Lcom/dsi/ant/channel/AntChannelImpl;->isFrequencyAllowed(I)Z

    move-result v6

    if-nez v6, :cond_d

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    .line 189
    :goto_3
    goto/16 :goto_1

    .line 188
    :cond_d
    invoke-virtual {v7, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    goto :goto_3

    .line 193
    :pswitch_9
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->close(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 194
    goto/16 :goto_1

    .line 212
    :pswitch_a
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 213
    goto/16 :goto_1

    .line 215
    :pswitch_b
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 216
    goto/16 :goto_1

    .line 228
    :pswitch_c
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 229
    goto/16 :goto_1

    .line 235
    :pswitch_d
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 236
    goto/16 :goto_1

    .line 242
    :pswitch_e
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 243
    goto/16 :goto_1

    .line 248
    :pswitch_f
    iget-object v7, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    move-object v0, v2

    check-cast v0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;

    move-object v6, v0

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->getFrequencyOne()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannelImpl;->isFrequencyAllowed(I)Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->getFrequencyTwo()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannelImpl;->isFrequencyAllowed(I)Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->getFrequencyThree()I

    move-result v6

    invoke-virtual {v7, v6}, Lcom/dsi/ant/channel/AntChannelImpl;->isFrequencyAllowed(I)Z

    move-result v6

    if-nez v6, :cond_f

    :cond_e
    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    .line 249
    :goto_4
    goto/16 :goto_1

    .line 248
    :cond_f
    invoke-virtual {v7, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    goto :goto_4

    .line 254
    :pswitch_10
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    check-cast v2, Lcom/dsi/ant/message/fromhost/LibConfigMessage;

    .end local v2    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->getLibConfig()Lcom/dsi/ant/message/LibConfig;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/dsi/ant/channel/AntChannelImpl;->setLibConfig(Lcom/dsi/ant/message/LibConfig;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 255
    goto/16 :goto_1

    .line 260
    .restart local v2    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    :pswitch_11
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-boolean v7, v6, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v7, :cond_10

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    .line 261
    :goto_5
    goto/16 :goto_1

    .line 260
    :cond_10
    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    goto :goto_5

    .line 263
    :pswitch_12
    iget-object v7, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-boolean v6, v7, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v6, :cond_11

    iget-object v6, v7, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v8, v7, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v6, v8}, Lcom/dsi/ant/channel/ChannelsController;->setBackgroundScanInProgress(I)Z

    move-result v6

    if-nez v6, :cond_11

    iget-object v6, v7, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->BACKGROUND_SCAN_IN_USE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    .line 264
    :goto_6
    goto/16 :goto_1

    .line 263
    :cond_11
    invoke-virtual {v7, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v6

    iget-boolean v8, v7, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v8, :cond_12

    invoke-virtual {v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v8

    if-nez v8, :cond_12

    iget-object v8, v7, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget v7, v7, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-virtual {v8, v7}, Lcom/dsi/ant/channel/ChannelsController;->clearBackgroundScanInProgress(I)V

    :cond_12
    move-object v4, v6

    goto :goto_6

    .line 269
    :pswitch_13
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 270
    goto/16 :goto_1

    .line 278
    :pswitch_14
    iget-object v7, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    iget-boolean v6, v7, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v6, :cond_13

    move-object v0, v2

    check-cast v0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;

    move-object v6, v0

    sget-object v8, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v8}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->getRawValue()I

    move-result v8

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->getRawSearchTimeout()I

    move-result v6

    if-eq v8, v6, :cond_13

    new-instance v6, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v6, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    move-object v4, v6

    .line 279
    :goto_7
    goto/16 :goto_1

    .line 278
    :cond_13
    invoke-virtual {v7, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    goto :goto_7

    .line 287
    :pswitch_15
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->setTransmitPower(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 288
    goto/16 :goto_1

    .line 314
    :pswitch_16
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v6, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->unassign(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v4

    .line 315
    goto/16 :goto_1

    .line 317
    :pswitch_17
    sget-object v6, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "writeMessage received unknown message type, ID: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto/16 :goto_1

    .line 322
    :cond_14
    sget-object v6, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;

    const-string v7, "Could not create AntMessageFromHost (null), writing raw message"

    invoke-static {v6, v7}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v6, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v7

    array-length v8, v7

    add-int/lit8 v9, v8, 0x2

    new-array v9, v9, [B

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageId()I

    move-result v10

    const/4 v11, 0x0

    invoke-static {v8, v9, v11}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    const/4 v11, 0x1

    invoke-static {v10, v9, v11}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-static {v7, v10, v9, v11, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v6, v9}, Lcom/dsi/ant/channel/AntChannelImpl;->writeMessage([B)Lcom/dsi/ant/channel/ipc/ServiceResult;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto/16 :goto_1

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method
