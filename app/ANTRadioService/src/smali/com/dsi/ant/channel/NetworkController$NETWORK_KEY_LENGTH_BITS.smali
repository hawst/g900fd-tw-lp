.class final enum Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;
.super Ljava/lang/Enum;
.source "NetworkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/NetworkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "NETWORK_KEY_LENGTH_BITS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

.field public static final enum LONG_128_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

.field public static final enum SHORT_64_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

.field public static final enum UNKNOWN:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->UNKNOWN:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    .line 39
    new-instance v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    const-string v1, "SHORT_64_BITS"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->SHORT_64_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    .line 40
    new-instance v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    const-string v1, "LONG_128_BITS"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->LONG_128_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    sget-object v1, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->UNKNOWN:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->SHORT_64_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->LONG_128_BITS:Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->$VALUES:[Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->$VALUES:[Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/NetworkController$NETWORK_KEY_LENGTH_BITS;

    return-object v0
.end method
