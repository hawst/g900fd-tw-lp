.class public final enum Lcom/dsi/ant/channel/PredefinedNetwork;
.super Ljava/lang/Enum;
.source "PredefinedNetwork.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/PredefinedNetwork;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/PredefinedNetwork;

.field public static final enum ANT_FS:Lcom/dsi/ant/channel/PredefinedNetwork;

.field public static final enum ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

.field public static final enum INVALID:Lcom/dsi/ant/channel/PredefinedNetwork;

.field public static final enum PUBLIC:Lcom/dsi/ant/channel/PredefinedNetwork;

.field private static final sValues:[Lcom/dsi/ant/channel/PredefinedNetwork;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/dsi/ant/channel/PredefinedNetwork;

    const-string v1, "INVALID"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/dsi/ant/channel/PredefinedNetwork;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->INVALID:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 24
    new-instance v0, Lcom/dsi/ant/channel/PredefinedNetwork;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v4, v3}, Lcom/dsi/ant/channel/PredefinedNetwork;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->PUBLIC:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 34
    new-instance v0, Lcom/dsi/ant/channel/PredefinedNetwork;

    const-string v1, "ANT_PLUS"

    invoke-direct {v0, v1, v5, v4}, Lcom/dsi/ant/channel/PredefinedNetwork;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 37
    new-instance v0, Lcom/dsi/ant/channel/PredefinedNetwork;

    const-string v1, "ANT_FS"

    invoke-direct {v0, v1, v6, v5}, Lcom/dsi/ant/channel/PredefinedNetwork;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_FS:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/channel/PredefinedNetwork;

    sget-object v1, Lcom/dsi/ant/channel/PredefinedNetwork;->INVALID:Lcom/dsi/ant/channel/PredefinedNetwork;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/PredefinedNetwork;->PUBLIC:Lcom/dsi/ant/channel/PredefinedNetwork;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_FS:Lcom/dsi/ant/channel/PredefinedNetwork;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->$VALUES:[Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 42
    invoke-static {}, Lcom/dsi/ant/channel/PredefinedNetwork;->values()[Lcom/dsi/ant/channel/PredefinedNetwork;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->sValues:[Lcom/dsi/ant/channel/PredefinedNetwork;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/channel/PredefinedNetwork;->mRawValue:I

    return-void
.end method

.method static create(I)Lcom/dsi/ant/channel/PredefinedNetwork;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 51
    sget-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->INVALID:Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 53
    .local v0, "code":Lcom/dsi/ant/channel/PredefinedNetwork;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->sValues:[Lcom/dsi/ant/channel/PredefinedNetwork;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 54
    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->sValues:[Lcom/dsi/ant/channel/PredefinedNetwork;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/channel/PredefinedNetwork;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 55
    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->sValues:[Lcom/dsi/ant/channel/PredefinedNetwork;

    aget-object v0, v2, v1

    .line 60
    :cond_0
    return-object v0

    .line 54
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 53
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/PredefinedNetwork;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/PredefinedNetwork;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/PredefinedNetwork;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->$VALUES:[Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/PredefinedNetwork;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/PredefinedNetwork;

    return-object v0
.end method


# virtual methods
.method final getRawValue()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/dsi/ant/channel/PredefinedNetwork;->mRawValue:I

    return v0
.end method
