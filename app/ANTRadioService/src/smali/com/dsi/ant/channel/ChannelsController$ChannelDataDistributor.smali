.class final Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;
.super Ljava/lang/Object;
.source "ChannelsController.java"

# interfaces
.implements Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ChannelsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ChannelDataDistributor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/channel/ChannelsController;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/channel/ChannelsController;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/channel/ChannelsController;B)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/channel/ChannelsController;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;-><init>(Lcom/dsi/ant/channel/ChannelsController;)V

    return-void
.end method

.method private passToChannel(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)V
    .locals 7
    .param p1, "channelNumber"    # I
    .param p2, "antMessage"    # Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 205
    iget-object v2, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v2}, Lcom/dsi/ant/channel/ChannelsController;->access$000(Lcom/dsi/ant/channel/ChannelsController;)[Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v2

    aget-object v1, v2, p1

    .line 206
    .local v1, "currentChannel":Lcom/dsi/ant/channel/AntChannelImpl;
    monitor-enter v1

    .line 207
    :try_start_0
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    sget-object v3, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    if-eq v2, v3, :cond_0

    .line 208
    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v3

    if-ne v2, v3, :cond_7

    move-object v0, p2

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-object v2, v0

    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v6

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v3

    if-ne v6, v3, :cond_1

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mIgnoreReponseList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    .line 212
    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_0
    :goto_0
    :pswitch_0
    monitor-exit v1

    return-void

    .line 208
    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_1
    sget-object v3, Lcom/dsi/ant/message/ResponseCode;->UNKNOWN:Lcom/dsi/ant/message/ResponseCode;

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v6

    if-eq v3, v6, :cond_0

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v3

    sget-object v6, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v6

    if-ne v3, v6, :cond_4

    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v6, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-nez v6, :cond_2

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v4, "Received ack command response when mAckInProgress == FALSE"

    invoke-static {v2, v4}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212
    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :catchall_1
    move-exception v2

    monitor-exit v1

    throw v2

    .line 208
    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_2
    const/16 v6, 0x1f

    :try_start_3
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getRawResponseCode()I

    move-result v2

    if-ne v6, v2, :cond_e

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_6

    :goto_1
    :try_start_4
    monitor-exit v3

    if-eqz v4, :cond_3

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->notifyAcknowledgedMessageComplete()V

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/ChannelsController;->txData([B)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 v2, 0x0

    :try_start_5
    iput-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_4
    :goto_2
    :try_start_6
    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandlerChange_LOCK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    if-eqz v2, :cond_5

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAntChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    const/4 v4, 0x0

    new-instance v5, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-direct {v5, p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v2, v4, v5}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    :cond_5
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v2

    :try_start_8
    monitor-exit v3

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_6
    :try_start_9
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v4, "Received ack command response TRANSFER_IN_PROGRESS when mAckInProgressData == null"

    invoke-static {v2, v4}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_0

    :catchall_3
    move-exception v2

    :try_start_a
    monitor-exit v3

    throw v2

    :cond_7
    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v3

    if-ne v2, v3, :cond_c

    move-object v0, p2

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    move-object v2, v0

    sget-object v3, Lcom/dsi/ant/channel/AntChannelImpl$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_2

    :pswitch_1
    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    iget-boolean v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    if-eqz v2, :cond_8

    move v5, v4

    :cond_8
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    :goto_3
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    if-eqz v5, :cond_4

    :try_start_c
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->notifyAcknowledgedMessageComplete()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_2

    :cond_9
    :try_start_d
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Transfer result "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContentString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " received when mAckInProgress == false"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    goto :goto_3

    :catchall_4
    move-exception v2

    :try_start_e
    monitor-exit v3

    throw v2

    :pswitch_2
    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :try_start_f
    iget-boolean v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    if-eqz v2, :cond_d

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->TAG:Ljava/lang/String;

    const-string v5, "Channel Closed Event while waiting for Ack - may be stuck IN_PROGRESS until unassign. RELEASING CHANNEL"

    invoke-static {v2, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgress:Z

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressUpdate_LOCK:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    move v2, v4

    :goto_4
    monitor-exit v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    if-eqz v2, :cond_b

    :try_start_10
    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mAckInProgressData:[B

    if-eqz v2, :cond_a

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->controller:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ChannelsController;->notifyAcknowledgedMessageComplete()V

    :cond_a
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannelImpl;->releaseChannel(Z)V

    :cond_b
    iget-object v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mWaitCloseLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    const/4 v2, 0x1

    :try_start_11
    iput-boolean v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mCloseSeen:Z

    iget-object v2, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mWaitCloseLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    goto/16 :goto_2

    :catchall_5
    move-exception v2

    :try_start_12
    monitor-exit v3

    throw v2

    :catchall_6
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_c
    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v3

    if-ne v2, v3, :cond_4

    move-object v0, p2

    check-cast v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isLastMessage()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getSequenceNumber()I

    move-result v3

    if-nez v3, :cond_4

    new-instance p2, Lcom/dsi/ant/message/fromant/GeneratedAcknowledgedDataMessage;

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    iget v3, v1, Lcom/dsi/ant/channel/AntChannelImpl;->mChannelNumber:I

    invoke-direct {p2, v3, v2}, Lcom/dsi/ant/message/fromant/GeneratedAcknowledgedDataMessage;-><init>(ILcom/dsi/ant/message/fromant/BurstTransferDataMessage;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto/16 :goto_2

    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_d
    move v2, v5

    goto :goto_4

    :cond_e
    move v4, v5

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final onClaimLost()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/ChannelsController;->setHasUseOfAdapter(Z)V

    .line 224
    return-void
.end method

.method public final onRxData([B)V
    .locals 7
    .param p1, "data"    # [B

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    const/4 v3, -0x3

    .line 147
    if-nez p1, :cond_1

    .line 181
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 149
    :cond_1
    invoke-static {p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    .line 151
    .local v0, "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    if-eqz v0, :cond_0

    .line 153
    sget-object v4, Lcom/dsi/ant/channel/ChannelsController$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    iget-object v4, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/channel/ChannelsController;->unhandledMessageType(Lcom/dsi/ant/message/fromant/AntMessageFromAnt;)V

    move v1, v3

    .line 155
    .local v1, "channelNumber":I
    :goto_1
    :pswitch_1
    :sswitch_0
    packed-switch v1, :pswitch_data_1

    .line 171
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v3}, Lcom/dsi/ant/channel/ChannelsController;->access$000(Lcom/dsi/ant/channel/ChannelsController;)[Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v3

    array-length v3, v3

    if-le v1, v3, :cond_3

    .line 172
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/channel/ChannelsController;->access$100(Lcom/dsi/ant/channel/ChannelsController;)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Received serial message for non existant channel "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ". "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    .end local v1    # "channelNumber":I
    :pswitch_2
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v3

    aget-byte v3, v3, v6

    and-int/lit8 v3, v3, 0x1f

    shr-int/lit8 v1, v3, 0x0

    goto :goto_1

    :pswitch_3
    move-object v3, v0

    check-cast v3, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v3

    aget-byte v3, v3, v6

    and-int/lit8 v3, v3, 0x1f

    shr-int/lit8 v1, v3, 0x0

    goto :goto_1

    :sswitch_1
    const/4 v1, -0x2

    goto :goto_1

    :pswitch_4
    move v1, v3

    goto :goto_1

    .line 166
    .restart local v1    # "channelNumber":I
    :pswitch_5
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v3}, Lcom/dsi/ant/channel/ChannelsController;->access$000(Lcom/dsi/ant/channel/ChannelsController;)[Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v3

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 167
    invoke-direct {p0, v2, v0}, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->passToChannel(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)V

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 172
    .end local v2    # "i":I
    :cond_2
    const-string v3, ""

    goto :goto_2

    .line 175
    :cond_3
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mChannels:[Lcom/dsi/ant/channel/AntChannelImpl;
    invoke-static {v3}, Lcom/dsi/ant/channel/ChannelsController;->access$000(Lcom/dsi/ant/channel/ChannelsController;)[Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v3

    aget-object v3, v3, v1

    iget-boolean v3, v3, Lcom/dsi/ant/channel/AntChannelImpl;->isBackgroundScanningChannel:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mBackgroundScanController:Lcom/dsi/ant/channel/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/channel/ChannelsController;->access$200(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/BackgroundScanController;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Lcom/dsi/ant/channel/BackgroundScanController;->handleBackgroundScanData(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z

    move-result v3

    :goto_4
    if-eqz v3, :cond_0

    .line 176
    invoke-direct {p0, v1, v0}, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->passToChannel(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)V

    goto/16 :goto_0

    .line 175
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelDataDistributor;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mTransferController:Lcom/dsi/ant/channel/TransferController;
    invoke-static {v3}, Lcom/dsi/ant/channel/ChannelsController;->access$300(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/TransferController;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Lcom/dsi/ant/channel/TransferController;->handleBurstData(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z

    move-result v3

    goto :goto_4

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 155
    :pswitch_data_1
    .packed-switch -0x3
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 153
    :sswitch_data_0
    .sparse-switch
        0x46 -> :sswitch_1
        0x47 -> :sswitch_0
        0x5b -> :sswitch_0
        0x66 -> :sswitch_0
        0x68 -> :sswitch_0
        0x6d -> :sswitch_0
        0x6e -> :sswitch_1
        0x74 -> :sswitch_1
        0x76 -> :sswitch_1
        0x78 -> :sswitch_0
        0x79 -> :sswitch_0
        0x7b -> :sswitch_0
        0x7c -> :sswitch_0
        0x7d -> :sswitch_0
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
        0x83 -> :sswitch_0
        0xc7 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 0
    .param p1, "newAdapterState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 219
    return-void
.end method
