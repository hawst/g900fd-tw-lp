.class Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;
.super Ljava/lang/Object;
.source "NetworkKeyStoreDecryptor.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static decryptByteStream([B[B)[B
    .locals 6
    .param p0, "key"    # [B
    .param p1, "inputBytes"    # [B

    .prologue
    .line 183
    const/4 v2, 0x0

    .line 185
    .local v2, "result":[B
    :try_start_0
    const-string v4, "AES"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 186
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, p0, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 187
    .local v3, "secretKey":Ljavax/crypto/SecretKey;
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 188
    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v2

    .line 210
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v3    # "secretKey":Ljavax/crypto/SecretKey;
    :goto_0
    return-object v2

    .line 189
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljavax/crypto/BadPaddingException;
    sget-object v4, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 193
    .end local v1    # "e":Ljavax/crypto/BadPaddingException;
    :catch_1
    move-exception v1

    .line 196
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v4, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    const-string v5, "Cipher algorithm not found, private networks not supported."

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v1

    .line 200
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    sget-object v4, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    const-string v5, "Cipher algorithm not found, private networks not supported."

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 201
    .end local v1    # "e":Ljavax/crypto/NoSuchPaddingException;
    :catch_3
    move-exception v1

    .line 204
    .local v1, "e":Ljava/security/InvalidKeyException;
    sget-object v4, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 205
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_4
    move-exception v1

    .line 206
    .local v1, "e":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v4, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    const-string v5, ""

    invoke-static {v4, v5, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 207
    .end local v1    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_5
    move-exception v1

    .line 208
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    const-string v5, ""

    invoke-static {v4, v5, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static getIndexWithNetworkKey(Lcom/dsi/ant/channel/NetworkKey;)I
    .locals 8
    .param p0, "inputNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;

    .prologue
    .line 81
    const/4 v2, -0x1

    .line 83
    .local v2, "index":I
    invoke-virtual {p0}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v5

    .line 92
    .local v5, "rawNetworkKey":[B
    :try_start_0
    const-string v6, "SHA-1"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 98
    .local v4, "networkKeyHash":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v6, Lcom/dsi/ant/channel/NetworkKeyStore;->CONVERSION_TABLE:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 99
    sget-object v6, Lcom/dsi/ant/channel/NetworkKeyStore;->CONVERSION_TABLE:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;

    .line 101
    .local v3, "keyEntry":Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;
    iget-object v6, v3, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->networkKeyHashEncrypted:[B

    invoke-static {v5, v6}, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->decryptByteStream([B[B)[B

    move-result-object v0

    .line 106
    .local v0, "cipherNetworkKeyHash":[B
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 107
    move v2, v1

    .end local v0    # "cipherNetworkKeyHash":[B
    .end local v3    # "keyEntry":Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;
    :cond_0
    move v6, v2

    .line 111
    .end local v1    # "i":I
    .end local v4    # "networkKeyHash":[B
    :goto_1
    return v6

    .line 94
    :catch_0
    move-exception v6

    sget-object v6, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->TAG:Ljava/lang/String;

    const-string v7, "Hash algorithm not found, private networks not supported."

    invoke-static {v6, v7}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v6, -0x1

    goto :goto_1

    .line 98
    .restart local v0    # "cipherNetworkKeyHash":[B
    .restart local v1    # "i":I
    .restart local v3    # "keyEntry":Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;
    .restart local v4    # "networkKeyHash":[B
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static getNetworkKeyType(ILcom/dsi/ant/channel/NetworkKey;)Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;
    .locals 3
    .param p0, "keystoreIndex"    # I
    .param p1, "inputNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;

    .prologue
    .line 124
    sget-object v1, Lcom/dsi/ant/channel/NetworkKeyStore;->CONVERSION_TABLE:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;

    .line 126
    .local v0, "keyEntry":Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;
    invoke-virtual {p1}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v1

    .line 127
    iget-object v2, v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->keyTypeEncrypted:[B

    invoke-static {v1, v2}, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->decryptByteStream([B[B)[B

    move-result-object v1

    .line 132
    const/4 v2, 0x0

    aget-byte v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->createFromRawValue(I)Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    move-result-object v1

    return-object v1
.end method

.method static getShortKeyFromTable(ILcom/dsi/ant/channel/NetworkKey;)Lcom/dsi/ant/channel/NetworkKey;
    .locals 4
    .param p0, "keystoreIndex"    # I
    .param p1, "inputNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;

    .prologue
    .line 62
    sget-object v2, Lcom/dsi/ant/channel/NetworkKeyStore;->CONVERSION_TABLE:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;

    .line 64
    .local v0, "keyEntry":Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;
    invoke-virtual {p1}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v2

    .line 65
    iget-object v3, v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyEntry;->shortNetworkKeyEncrypted:[B

    invoke-static {v2, v3}, Lcom/dsi/ant/channel/NetworkKeyStoreDecryptor;->decryptByteStream([B[B)[B

    move-result-object v1

    .line 68
    .local v1, "result":[B
    new-instance v2, Lcom/dsi/ant/channel/NetworkKey;

    invoke-direct {v2, v1}, Lcom/dsi/ant/channel/NetworkKey;-><init>([B)V

    return-object v2
.end method
