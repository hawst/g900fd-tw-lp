.class public final Lcom/dsi/ant/channel/Capabilities;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;,
        Lcom/dsi/ant/channel/Capabilities$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/Capabilities;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBackgroundScanning:Z

.field private mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

.field private mFrequencyAgility:Z

.field private mMaxOutputPowerLevelSetting:I

.field private mRxMessageTimestamp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/Capabilities$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    .line 37
    iput-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    .line 38
    iput-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    .line 39
    const/4 v0, 0x3

    iput v0, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    .line 382
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData;

    invoke-direct {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    .line 47
    return-void
.end method

.method static synthetic access$502(Lcom/dsi/ant/channel/Capabilities;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    return p1
.end method

.method static synthetic access$602(Lcom/dsi/ant/channel/Capabilities;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    return p1
.end method

.method static synthetic access$702(Lcom/dsi/ant/channel/Capabilities;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    return p1
.end method

.method static synthetic access$802(Lcom/dsi/ant/channel/Capabilities;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    return p1
.end method

.method static synthetic access$902(Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities$BundleData;)Lcom/dsi/ant/channel/Capabilities$BundleData;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities;
    .param p1, "x1"    # Lcom/dsi/ant/channel/Capabilities$BundleData;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    return-object p1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 494
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 311
    if-ne p0, p1, :cond_1

    .line 337
    :cond_0
    :goto_0
    return v1

    .line 315
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 316
    goto :goto_0

    .line 319
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/channel/Capabilities;

    if-nez v3, :cond_3

    move v1, v2

    .line 320
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 323
    check-cast v0, Lcom/dsi/ant/channel/Capabilities;

    .line 325
    .local v0, "other":Lcom/dsi/ant/channel/Capabilities;
    iget-boolean v3, v0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, v0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, v0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-ne v3, v4, :cond_4

    iget-object v3, v0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v3}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, v0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v3}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, v0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v3}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, v0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v3}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$300(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$300(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, v0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v3}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$400(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$400(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v4

    if-ne v3, v4, :cond_4

    iget v3, v0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    iget v4, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 334
    goto :goto_0
.end method

.method public final getMaxOutputPowerLevelSetting()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    return v0
.end method

.method public final getRfFrequencyMax()I
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$400(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v0

    return v0
.end method

.method public final getRfFrequencyMin()I
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$300(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v0

    return v0
.end method

.method public final hasBackgroundScanning()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    return v0
.end method

.method public final hasCapabilities(Lcom/dsi/ant/channel/Capabilities;)Z
    .locals 3
    .param p1, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229
    if-nez p1, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v0

    .line 233
    :cond_1
    iget-boolean v2, p1, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-eqz v2, :cond_2

    .line 234
    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 237
    :cond_2
    iget-boolean v2, p1, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-eqz v2, :cond_3

    .line 238
    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    .line 241
    :cond_3
    iget-boolean v2, p1, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-eqz v2, :cond_4

    .line 242
    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 245
    :cond_4
    iget-object v2, p1, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 246
    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 249
    :cond_5
    iget-object v2, p1, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 250
    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    .line 253
    :cond_6
    iget-object v2, p1, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 254
    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hasEventBuffering()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v0

    return v0
.end method

.method public final hasExtendedAssign()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFrequencyAgility()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    return v0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 293
    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0xd9

    .line 297
    .local v0, "result":I
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 298
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v4, v1

    .line 299
    mul-int/lit8 v4, v0, 0x1f

    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v4, v1

    .line 300
    mul-int/lit8 v4, v0, 0x1f

    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v4, v1

    .line 301
    mul-int/lit8 v1, v0, 0x1f

    iget-object v4, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v4

    if-eqz v4, :cond_5

    :goto_5
    add-int v0, v1, v2

    .line 302
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$300(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v2

    add-int v0, v1, v2

    .line 303
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$400(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v2

    add-int/2addr v1, v2

    .line 305
    return v1

    .end local v0    # "result":I
    :cond_0
    move v1, v3

    .line 293
    goto :goto_0

    .restart local v0    # "result":I
    :cond_1
    move v1, v3

    .line 297
    goto :goto_1

    :cond_2
    move v1, v3

    .line 298
    goto :goto_2

    :cond_3
    move v1, v3

    .line 299
    goto :goto_3

    :cond_4
    move v1, v3

    .line 300
    goto :goto_4

    :cond_5
    move v2, v3

    .line 301
    goto :goto_5
.end method

.method public final numberOfCapabilities(Lcom/dsi/ant/channel/Capabilities;)I
    .locals 2
    .param p1, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 271
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 273
    :cond_1
    const/4 v0, 0x0

    .line 275
    .local v0, "numCapabilities":I
    iget-boolean v1, p1, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 277
    :cond_2
    iget-boolean v1, p1, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-eqz v1, :cond_3

    add-int/lit8 v0, v0, 0x1

    .line 279
    :cond_3
    iget-boolean v1, p1, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    .line 281
    :cond_4
    iget-object v1, p1, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_5

    add-int/lit8 v0, v0, 0x1

    .line 283
    :cond_5
    iget-object v1, p1, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_6

    add-int/lit8 v0, v0, 0x1

    .line 285
    :cond_6
    iget-object v1, p1, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final supportBackgroundScanning(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    .line 94
    return-void
.end method

.method public final supportEventBuffering(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 168
    iget-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v0, p1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$202(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    .line 169
    return-void
.end method

.method public final supportFrequencyAgility(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    .line 112
    return-void
.end method

.method public final supportRssi(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v0, p1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$002(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    .line 130
    return-void
.end method

.method public final supportRxMessageTimestamp(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    .line 65
    return-void
.end method

.method public final supportWildcardIdList(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v0, p1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$102(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    .line 149
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Capabilities:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 345
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    if-eqz v1, :cond_0

    .line 346
    const-string v1, " -Rx Message Timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    :cond_0
    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    if-eqz v1, :cond_1

    .line 350
    const-string v1, " -Background Scanning"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    :cond_1
    iget-boolean v1, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    if-eqz v1, :cond_2

    .line 354
    const-string v1, " -Frequency Agility"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 358
    const-string v1, " -RSSI"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 362
    const-string v1, " -Wildcards in ID Lists"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    :cond_4
    iget-object v1, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 366
    const-string v1, " -Event Buffering"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    :cond_5
    const/4 v1, 0x3

    iget v2, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    if-eq v1, v2, :cond_6

    .line 370
    const-string v1, "  Max Transmit Power Level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373
    :cond_6
    const-string v1, " -RF Frequency Range: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$300(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    # getter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v2}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$400(Lcom/dsi/ant/channel/Capabilities$BundleData;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MHz offset of 2400 MHz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 499
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 500
    sget-object v0, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v0

    new-array v0, v0, [Z

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->RX_MESSAGE_TIMESTAMP:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->EXTENDED_ASSIGN:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v1

    invoke-virtual {p0}, Lcom/dsi/ant/channel/Capabilities;->hasExtendedAssign()Z

    move-result v2

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->BACKGROUND_SCANNING:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->FREQUENCY_AGILITY:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    iget v0, p0, Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 502
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.dsi.ant.channel.capabilities.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 505
    :cond_0
    return-void
.end method
