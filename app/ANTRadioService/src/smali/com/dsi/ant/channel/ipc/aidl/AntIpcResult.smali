.class public Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
.super Ljava/lang/Object;
.source "AntIpcResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBundleData:Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->mBundleData:Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v1, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;-><init>(B)V

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->mBundleData:Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 162
    .local v0, "data":Landroid/os/Bundle;
    const-class v1, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 163
    const-string v1, "com.dsi.ant.channel.antipcresult.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->mBundleData:Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    .line 164
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final addBinder(Landroid/os/IBinder;)V
    .locals 1
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->mBundleData:Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    iget-object v0, v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBindersReturned:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    .local v0, "out":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.antipcresult.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->mBundleData:Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 157
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 158
    return-void
.end method
