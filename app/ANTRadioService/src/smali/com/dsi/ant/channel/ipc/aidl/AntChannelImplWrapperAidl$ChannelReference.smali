.class final Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;
.super Ljava/lang/Object;
.source "AntChannelImplWrapperAidl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ChannelReference"
.end annotation


# instance fields
.field private final mDeathNotifierBinder:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;Landroid/os/IBinder;)V
    .locals 2
    .param p2, "deathNotifierBinder"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 77
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->mDeathNotifierBinder:Landroid/os/IBinder;

    .line 79
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->mDeathNotifierBinder:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 80
    return-void
.end method


# virtual methods
.method public final binderDied()V
    .locals 4

    .prologue
    .line 89
    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$000()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ref "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->mDeathNotifierBinder:Landroid/os/IBinder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " died."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 91
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->mDeathNotifierBinder:Landroid/os/IBinder;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiersChange_Lock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$200(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 93
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->mDeathNotifierBinder:Landroid/os/IBinder;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->mChannelDeathNotifiers:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    .line 95
    .local v0, "noRefs":Z
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    if-eqz v0, :cond_0

    .line 98
    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "All refs dead, releasing channel."

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->this$0:Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->releaseChannel()V

    .line 103
    .end local v0    # "noRefs":Z
    :cond_0
    return-void

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final cancelDeathWatch()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl$ChannelReference;->mDeathNotifierBinder:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 84
    return-void
.end method
