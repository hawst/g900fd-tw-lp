.class public final Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;
.super Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;
.source "ChannelProviderWrapperAidl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl$1;
    }
.end annotation


# instance fields
.field private final mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/AntChannelProviderImpl;)V
    .locals 0
    .param p1, "channelProvider"    # Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    .line 41
    return-void
.end method

.method private acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;
    .locals 4
    .param p1, "whichNetwork"    # I
    .param p2, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "err"    # Landroid/os/Bundle;

    .prologue
    .line 47
    :try_start_0
    new-instance v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, p3, v3}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;-><init>(Lcom/dsi/ant/channel/AntChannelImpl;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-object v1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    const-string v1, "error"

    invoke-virtual {p4, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 52
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getDesiredCapabilities(Landroid/os/Bundle;)Lcom/dsi/ant/channel/Capabilities;
    .locals 1
    .param p0, "parameters"    # Landroid/os/Bundle;

    .prologue
    .line 146
    const-class v0, Lcom/dsi/ant/channel/Capabilities;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 147
    const-string v0, "com.dsi.channel.data.desiredcapabilities"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities;

    return-object v0
.end method

.method private static getRequiredCapabilities(Landroid/os/Bundle;)Lcom/dsi/ant/channel/Capabilities;
    .locals 1
    .param p0, "parameters"    # Landroid/os/Bundle;

    .prologue
    .line 140
    const-class v0, Lcom/dsi/ant/channel/Capabilities;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 141
    const-string v0, "com.dsi.channel.data.requiredcapabilities"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "x2"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "x3"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    move-result-object v0

    return-object v0
.end method

.method public final acquireChannelKey$2ece064f()Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unused legacy method for private network key support."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I
    .locals 1
    .param p1, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v0

    return v0
.end method

.method public final handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    .locals 12
    .param p1, "message"    # Landroid/os/Message;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 80
    new-instance v7, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;

    invoke-direct {v7}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;-><init>()V

    .line 81
    .local v7, "returnData":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 82
    .local v5, "parameters":Landroid/os/Bundle;
    const/4 v6, 0x0

    .line 83
    .local v6, "requiredCapabilities":Lcom/dsi/ant/channel/Capabilities;
    const/4 v1, 0x0

    .line 84
    .local v1, "desiredCapabilities":Lcom/dsi/ant/channel/Capabilities;
    const/4 v3, 0x0

    .line 86
    .local v3, "libraryVersionCode":I
    const-class v9, Lcom/dsi/ant/channel/NetworkKey;

    invoke-virtual {v9}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 88
    sget-object v9, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl$1;->$SwitchMap$com$dsi$ant$channel$ipc$aidl$AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat:[I

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-static {v10}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->createFromRawValue(I)Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    move-result-object v10

    invoke-virtual {v10}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 135
    :goto_0
    return-object v7

    .line 92
    :pswitch_0
    :try_start_0
    const-string v9, "com.dsi.channel.data.networkkey"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/channel/NetworkKey;

    .line 94
    .local v4, "networkKey":Lcom/dsi/ant/channel/NetworkKey;
    invoke-static {v5}, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->getRequiredCapabilities(Landroid/os/Bundle;)Lcom/dsi/ant/channel/Capabilities;

    move-result-object v6

    .line 95
    invoke-static {v5}, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->getDesiredCapabilities(Landroid/os/Bundle;)Lcom/dsi/ant/channel/Capabilities;

    move-result-object v1

    .line 97
    const-string v9, "com.dsi.channel.data.versioncode"

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 102
    iget-object v9, p0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-virtual {v9, v4, v6, v1, v3}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->acquireChannel(Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    .line 104
    .local v0, "acquiredChannel":Lcom/dsi/ant/channel/AntChannelImpl;
    new-instance v9, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-direct {v9, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;-><init>(Lcom/dsi/ant/channel/AntChannelImpl;)V

    invoke-virtual {v9}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->addBinder(Landroid/os/IBinder;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    .end local v0    # "acquiredChannel":Lcom/dsi/ant/channel/AntChannelImpl;
    .end local v4    # "networkKey":Lcom/dsi/ant/channel/NetworkKey;
    :catch_0
    move-exception v2

    .line 107
    .local v2, "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    const-string v9, "error"

    invoke-virtual {p2, v9, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 113
    .end local v2    # "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    :pswitch_1
    :try_start_1
    const-string v9, "com.dsi.channel.data.predefinednetwork"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 115
    .local v8, "whichNetwork":I
    invoke-static {v5}, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->getRequiredCapabilities(Landroid/os/Bundle;)Lcom/dsi/ant/channel/Capabilities;

    move-result-object v6

    .line 116
    invoke-static {v5}, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->getDesiredCapabilities(Landroid/os/Bundle;)Lcom/dsi/ant/channel/Capabilities;

    move-result-object v1

    .line 118
    const-string v9, "com.dsi.channel.data.versioncode"

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 123
    iget-object v9, p0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-virtual {v9, v8, v6, v1, v3}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;I)Lcom/dsi/ant/channel/AntChannelImpl;

    move-result-object v0

    .line 125
    .restart local v0    # "acquiredChannel":Lcom/dsi/ant/channel/AntChannelImpl;
    new-instance v9, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;

    invoke-direct {v9, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;-><init>(Lcom/dsi/ant/channel/AntChannelImpl;)V

    invoke-virtual {v9}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelImplWrapperAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->addBinder(Landroid/os/IBinder;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 127
    .end local v0    # "acquiredChannel":Lcom/dsi/ant/channel/AntChannelImpl;
    .end local v8    # "whichNetwork":I
    :catch_1
    move-exception v2

    .line 128
    .restart local v2    # "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    const-string v9, "error"

    invoke-virtual {p2, v9, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 132
    .end local v2    # "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    :pswitch_2
    const-string v9, "Ant Radio Service - Channel Provider"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Received unknown request from app: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, p1, Landroid/os/Message;->what:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final isLegacyInterfaceInUse()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->isLegacyInterfaceInUse()Z

    move-result v0

    return v0
.end method
