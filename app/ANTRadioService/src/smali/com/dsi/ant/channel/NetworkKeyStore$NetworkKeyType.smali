.class public final enum Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;
.super Ljava/lang/Enum;
.source "NetworkKeyStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/NetworkKeyStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkKeyType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

.field public static final enum ANTFS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

.field public static final enum ANTPLUS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

.field public static final enum PRIVATE:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

.field public static final enum PUBLIC:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

.field private static final sValues:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    const-string v1, "PRIVATE"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->PRIVATE:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    .line 84
    new-instance v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->PUBLIC:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    .line 89
    new-instance v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    const-string v1, "ANTFS"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ANTFS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    .line 94
    new-instance v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    const-string v1, "ANTPLUS"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ANTPLUS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    .line 75
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    sget-object v1, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->PRIVATE:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->PUBLIC:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ANTFS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ANTPLUS:Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->$VALUES:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    .line 96
    invoke-static {}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->values()[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->sValues:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static createFromRawValue(I)Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;
    .locals 4
    .param p0, "rawValue"    # I

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 101
    .local v0, "code":Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->sValues:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 102
    sget-object v2, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->sValues:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->ordinal()I

    move-result v2

    if-ne v2, p0, :cond_1

    .line 103
    sget-object v2, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->sValues:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    aget-object v0, v2, v1

    .line 108
    :cond_0
    if-nez v0, :cond_2

    .line 110
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Given raw value is invalid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 101
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    :cond_2
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 75
    const-class v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->$VALUES:[Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/NetworkKeyStore$NetworkKeyType;

    return-object v0
.end method
