.class final Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;
.super Ljava/lang/Object;
.source "ChannelsController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ChannelsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ChannelCleanupTask"
.end annotation


# instance fields
.field private final mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

.field final synthetic this$0:Lcom/dsi/ant/channel/ChannelsController;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/ChannelsController;Lcom/dsi/ant/channel/AntChannelImpl;)V
    .locals 0
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannelImpl;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    iput-object p2, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    .line 237
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/dsi/ant/channel/ChannelsController;->access$100(Lcom/dsi/ant/channel/ChannelsController;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cleaning up channel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 243
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelImpl;->cleanUp()V

    .line 245
    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    monitor-enter v1

    .line 246
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    sget-object v2, Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;->ACQUIREDSTATE_AVAILABLE:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    iput-object v2, v0, Lcom/dsi/ant/channel/AntChannelImpl;->mAcquiredState:Lcom/dsi/ant/channel/AntChannelImpl$AcquiredState;

    .line 247
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelsController;->releaseAdapterClaimIfNotInUse()V

    .line 251
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;
    invoke-static {v0}, Lcom/dsi/ant/channel/ChannelsController;->access$400(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->mChannelAcquiredStateListener:Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;
    invoke-static {v0}, Lcom/dsi/ant/channel/ChannelsController;->access$400(Lcom/dsi/ant/channel/ChannelsController;)Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;->onChannelAvailable(Z)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->this$0:Lcom/dsi/ant/channel/ChannelsController;

    # getter for: Lcom/dsi/ant/channel/ChannelsController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/dsi/ant/channel/ChannelsController;->access$100(Lcom/dsi/ant/channel/ChannelsController;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "...channel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/dsi/ant/channel/ChannelsController$ChannelCleanupTask;->mChannel:Lcom/dsi/ant/channel/AntChannelImpl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cleaned up"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 255
    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
