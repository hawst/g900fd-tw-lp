.class final Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
.super Landroid/os/Handler;
.source "AntChannelCommunicatorAidl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AntIpcEventReceiver"
.end annotation


# instance fields
.field private mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

.field private final mDestroy_LOCK:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Landroid/os/Looper;)V
    .locals 1
    .param p1, "outerChannelCommunicator"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 92
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 80
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mDestroy_LOCK:Ljava/lang/Object;

    .line 94
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    .line 95
    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;)V
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    .prologue
    .line 74
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 109
    iget-object v9, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v9

    .line 110
    :try_start_0
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    if-nez v8, :cond_0

    monitor-exit v9

    .line 198
    :goto_0
    return-void

    .line 112
    :cond_0
    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {v8}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->create(I)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    move-result-object v7

    .line 114
    .local v7, "whatMessage":Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    sget-object v8, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$2;->$SwitchMap$com$dsi$ant$channel$ipc$aidl$AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat:[I

    invoke-virtual {v7}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->ordinal()I

    move-result v10

    aget v8, v8, v10

    packed-switch v8, :pswitch_data_0

    .line 198
    :cond_1
    :goto_1
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .end local v7    # "whatMessage":Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8

    .line 117
    .restart local v7    # "whatMessage":Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    :pswitch_0
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 118
    .local v1, "messageBundle":Landroid/os/Bundle;
    const-class v8, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 119
    const-string v8, "com.dsi.ant.channel.data.antmessageparcel"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 122
    .local v0, "antParcel":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    invoke-static {v0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v2

    .line 125
    .local v2, "messageType":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    sget-object v8, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-eq v2, v8, :cond_1

    .line 130
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {v8, v2, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->onRxAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_1

    .line 135
    .end local v0    # "antParcel":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .end local v1    # "messageBundle":Landroid/os/Bundle;
    .end local v2    # "messageType":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :pswitch_1
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {v8}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->onChannelDeathMessage()V

    goto :goto_1

    .line 140
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 141
    .restart local v1    # "messageBundle":Landroid/os/Bundle;
    const-class v8, Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 142
    const-string v8, "com.dsi.ant.channel.data.burststate"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/channel/BurstState;

    .line 144
    .local v4, "newBurstState":Lcom/dsi/ant/channel/BurstState;
    if-eqz v4, :cond_1

    .line 145
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-static {v8, v4}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->access$000(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/BurstState;)V

    goto :goto_1

    .line 153
    .end local v1    # "messageBundle":Landroid/os/Bundle;
    .end local v4    # "newBurstState":Lcom/dsi/ant/channel/BurstState;
    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 154
    .restart local v1    # "messageBundle":Landroid/os/Bundle;
    const-class v8, Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 155
    const-string v8, "com.dsi.ant.channel.data.libconfig"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/dsi/ant/message/LibConfig;

    .line 157
    .local v6, "newLibConfig":Lcom/dsi/ant/message/LibConfig;
    if-eqz v6, :cond_1

    .line 158
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-static {v8, v6}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/message/LibConfig;)V

    goto :goto_1

    .line 166
    .end local v1    # "messageBundle":Landroid/os/Bundle;
    .end local v6    # "newLibConfig":Lcom/dsi/ant/message/LibConfig;
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 167
    .restart local v1    # "messageBundle":Landroid/os/Bundle;
    const-class v8, Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 168
    const-string v8, "com.dsi.ant.channel.data.backgroundscanstate"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/channel/BackgroundScanState;

    .line 171
    .local v3, "newBackgroundScanState":Lcom/dsi/ant/channel/BackgroundScanState;
    if-eqz v3, :cond_1

    .line 172
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-static {v8, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->access$200(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/BackgroundScanState;)V

    goto/16 :goto_1

    .line 180
    .end local v1    # "messageBundle":Landroid/os/Bundle;
    .end local v3    # "newBackgroundScanState":Lcom/dsi/ant/channel/BackgroundScanState;
    :pswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 181
    .restart local v1    # "messageBundle":Landroid/os/Bundle;
    const-class v8, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 182
    const-string v8, "com.dsi.ant.channel.data.eventbuffersettings"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/dsi/ant/channel/EventBufferSettings;

    .line 184
    .local v5, "newEventBufferState":Lcom/dsi/ant/channel/EventBufferSettings;
    if-eqz v5, :cond_1

    .line 185
    iget-object v8, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->mChannelCommunicator:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-static {v8, v5}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/EventBufferSettings;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
