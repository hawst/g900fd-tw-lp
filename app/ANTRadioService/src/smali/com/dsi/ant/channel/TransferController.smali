.class public Lcom/dsi/ant/channel/TransferController;
.super Ljava/lang/Object;
.source "TransferController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/TransferController$1;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAcknowledgedMessagesInProgress:I

.field private mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

.field private mBurstState:Lcom/dsi/ant/channel/BurstState;

.field private mBurstingChannel:I

.field private mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

.field private mFailGoToSearchReceived:Z

.field private final mFirmwareCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

.field private final mQueuedAcknowledgedMessages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

.field private mTransferState_Lock:Ljava/lang/Object;

.field private mTxStartReceived:Z

.field private mWaitingForChannelPeriod:Z


# direct methods
.method public constructor <init>(Lcom/dsi/ant/channel/ChannelsController;Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;)V
    .locals 2
    .param p1, "channelsController"    # Lcom/dsi/ant/channel/ChannelsController;
    .param p2, "adapterHandle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p3, "firmwareCapabilities"    # Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mQueuedAcknowledgedMessages:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    .line 68
    iput-boolean v1, p0, Lcom/dsi/ant/channel/TransferController;->mFailGoToSearchReceived:Z

    .line 73
    iput v1, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    .line 77
    iput-object p3, p0, Lcom/dsi/ant/channel/TransferController;->mFirmwareCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    .line 78
    iput-object p1, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    .line 79
    iput-object p2, p0, Lcom/dsi/ant/channel/TransferController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 81
    new-instance v0, Lcom/dsi/ant/channel/BurstState;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BurstState;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dsi/ant/channel/TransferController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/TransferController;->TAG:Ljava/lang/String;

    .line 84
    return-void
.end method

.method private clearBurstTransmitInProgress()V
    .locals 3

    .prologue
    .line 203
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/dsi/ant/channel/BurstState;->setTransmitInProgress(Z)V

    .line 206
    iget-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v2, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    .line 207
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setBurstTransmitInProgress()Z
    .locals 4

    .prologue
    .line 188
    const/4 v0, 0x0

    .line 190
    .local v0, "canStart":Z
    iget-object v2, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v2

    .line 191
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/BurstState;->isTransmitInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/dsi/ant/channel/BurstState;->setTransmitInProgress(Z)V

    .line 193
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v3, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v1, v3}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    .line 197
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    return v0

    .line 197
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private waitForBurstResult()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 373
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 375
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    if-eqz v0, :cond_1

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 379
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final getBurstState()Lcom/dsi/ant/channel/BurstState;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    return-object v0
.end method

.method public final handleBurstData(ILcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z
    .locals 8
    .param p1, "channelNumber"    # I
    .param p2, "antMessage"    # Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 219
    const/4 v1, 0x0

    .line 221
    .local v1, "processed":Z
    iget-object v6, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v6

    .line 222
    :try_start_0
    iget v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    if-ne p1, v5, :cond_0

    .line 223
    iget-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    if-eqz v5, :cond_1

    .line 224
    sget-object v5, Lcom/dsi/ant/channel/TransferController$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_0

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :goto_0
    move v5, v4

    :goto_1
    if-eqz v5, :cond_0

    .line 228
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    .line 229
    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 246
    :cond_0
    :goto_2
    monitor-exit v6

    .line 248
    if-nez v1, :cond_7

    :goto_3
    return v3

    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :pswitch_0
    move v5, v3

    .line 224
    goto :goto_1

    :pswitch_1
    move v5, v3

    goto :goto_1

    :pswitch_2
    check-cast p2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    sget-object v5, Lcom/dsi/ant/channel/TransferController$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    move v5, v3

    goto :goto_1

    .line 231
    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v5}, Lcom/dsi/ant/channel/BurstState;->isTransmitInProgress()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 232
    sget-object v5, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v7

    if-ne v5, v7, :cond_4

    .line 233
    move-object v0, p2

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-object v2, v0

    .line 235
    .local v2, "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v5

    sget-object v7, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v7}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v7

    if-ne v5, v7, :cond_0

    .line 238
    iget v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getRawResponseCode()I

    move-result v5

    const/16 v7, 0x1f

    if-ne v7, v5, :cond_3

    new-instance v5, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v5, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    iput-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 239
    :cond_2
    :goto_4
    const/4 v1, 0x1

    goto :goto_2

    .line 238
    :cond_3
    iget-boolean v7, p0, Lcom/dsi/ant/channel/TransferController;->mTxStartReceived:Z

    if-nez v7, :cond_2

    packed-switch v5, :pswitch_data_2

    goto :goto_4

    :pswitch_4
    new-instance v5, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v5, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    iput-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 246
    .end local v2    # "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :catchall_0
    move-exception v3

    monitor-exit v6

    throw v3

    .line 241
    .restart local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    :cond_4
    :try_start_1
    sget-object v5, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v7

    if-ne v5, v7, :cond_0

    .line 242
    check-cast p2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    .end local p2    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    iget v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    sget-object v5, Lcom/dsi/ant/channel/TransferController$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    invoke-virtual {p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_3

    :cond_5
    :goto_5
    const/4 v1, 0x0

    goto/16 :goto_2

    :pswitch_5
    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/BurstState;->setProcessing(Z)V

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v7, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    sget-object v5, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    iput-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    goto :goto_5

    :pswitch_6
    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/BurstState;->setProcessing(Z)V

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v7, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    new-instance v5, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v5, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    iput-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    iget-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mFailGoToSearchReceived:Z

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    goto :goto_5

    :cond_6
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    goto :goto_5

    :pswitch_7
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mTxStartReceived:Z

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/BurstState;->setProcessing(Z)V

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v7, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    goto :goto_5

    :pswitch_8
    iget v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    if-ne p1, v5, :cond_5

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/BurstState;->setProcessing(Z)V

    invoke-direct {p0}, Lcom/dsi/ant/channel/TransferController;->clearBurstTransmitInProgress()V

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v7, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v5, v7}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    new-instance v5, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v7, Lcom/dsi/ant/channel/AntCommandFailureReason;->CANCELLED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v5, v7}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    iput-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    goto :goto_5

    :pswitch_9
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/channel/TransferController;->mFailGoToSearchReceived:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :cond_7
    move v3, v4

    .line 248
    goto/16 :goto_3

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 238
    :pswitch_data_2
    .packed-switch 0x20
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 242
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final notifyAcknowledgedMessageComplete()V
    .locals 2

    .prologue
    .line 403
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 404
    :try_start_0
    iget v0, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    .line 405
    iget v0, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    if-nez v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 409
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final txAcknowledgedMessage([B)Z
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 383
    iget-object v2, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v2

    .line 384
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mFirmwareCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasBurstBufferBug()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 388
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mQueuedAcknowledgedMessages:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    const/4 v0, 0x1

    monitor-exit v2

    .line 396
    :goto_0
    return v0

    .line 391
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/channel/ChannelsController;->txData([B)Z

    move-result v0

    .line 392
    .local v0, "success":Z
    if-eqz v0, :cond_1

    .line 394
    iget v1, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    .line 396
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 398
    .end local v0    # "success":Z
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final txBurst(I[B)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 7
    .param p1, "channelNumber"    # I
    .param p2, "data"    # [B

    .prologue
    .line 94
    array-length v4, p2

    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v5}, Lcom/dsi/ant/channel/BurstState;->getMaxBurstSize()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 95
    new-instance v2, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v2, v4}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 184
    .local v2, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_0
    return-object v2

    .line 96
    .end local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/channel/TransferController;->setBurstTransmitInProgress()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 97
    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v5

    .line 99
    :try_start_0
    iput p1, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    .line 100
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 101
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/channel/TransferController;->mTxStartReceived:Z

    .line 102
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    .line 103
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/channel/TransferController;->mFailGoToSearchReceived:Z

    .line 105
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mFirmwareCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v4}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasBurstBufferBug()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-lez v4, :cond_1

    .line 109
    :goto_1
    :try_start_1
    iget v4, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    if-lez v4, :cond_1

    .line 111
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 116
    :catch_0
    move-exception v4

    :try_start_2
    new-instance v4, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v6, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v4, v6}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 119
    :cond_1
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 123
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mAdapterHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v4, p1, p2}, Lcom/dsi/ant/adapter/AdapterHandle;->txBurst(I[B)Z

    move-result v3

    .line 125
    .local v3, "written":Z
    iget-object v5, p0, Lcom/dsi/ant/channel/TransferController;->mTransferState_Lock:Ljava/lang/Object;

    monitor-enter v5

    .line 128
    if-eqz v3, :cond_2

    .line 133
    :try_start_3
    invoke-direct {p0}, Lcom/dsi/ant/channel/TransferController;->waitForBurstResult()V

    .line 134
    iget-object v2, p0, Lcom/dsi/ant/channel/TransferController;->mResult:Lcom/dsi/ant/channel/ipc/ServiceResult;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150
    .restart local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :goto_2
    const/4 v4, -0x1

    :try_start_4
    iput v4, p0, Lcom/dsi/ant/channel/TransferController;->mBurstingChannel:I

    .line 151
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/channel/TransferController;->mTxStartReceived:Z

    .line 152
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/channel/TransferController;->mWaitingForChannelPeriod:Z

    .line 153
    invoke-direct {p0}, Lcom/dsi/ant/channel/TransferController;->clearBurstTransmitInProgress()V

    .line 158
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mFirmwareCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v4}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hasBurstBufferBug()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mQueuedAcknowledgedMessages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 161
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mQueuedAcknowledgedMessages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 163
    .local v1, "mesg":[B
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    invoke-virtual {v4, v1}, Lcom/dsi/ant/channel/ChannelsController;->txData([B)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 165
    iget v4, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/dsi/ant/channel/TransferController;->mAcknowledgedMessagesInProgress:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 177
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mesg":[B
    .end local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 119
    .end local v3    # "written":Z
    :catchall_1
    move-exception v4

    monitor-exit v5

    throw v4

    .line 141
    .restart local v3    # "written":Z
    :cond_2
    :try_start_5
    invoke-direct {p0}, Lcom/dsi/ant/channel/TransferController;->waitForBurstResult()V

    .line 142
    new-instance v2, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v2, v4}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .restart local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_2

    .line 147
    .end local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    :catch_1
    move-exception v4

    :try_start_6
    new-instance v2, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->IO_ERROR:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v2, v4}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .restart local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto :goto_2

    .line 173
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mQueuedAcknowledgedMessages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 176
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v4, p0, Lcom/dsi/ant/channel/TransferController;->mChannelsController:Lcom/dsi/ant/channel/ChannelsController;

    iget-object v6, p0, Lcom/dsi/ant/channel/TransferController;->mBurstState:Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v4, v6}, Lcom/dsi/ant/channel/ChannelsController;->sendBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    .line 177
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 178
    .end local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    .end local v3    # "written":Z
    :cond_5
    new-instance v2, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v2, v4}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .restart local v2    # "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    goto/16 :goto_0
.end method
