.class final Lcom/dsi/ant/channel/AntChannelProviderImpl$1;
.super Ljava/lang/Object;
.source "AntChannelProviderImpl.java"

# interfaces
.implements Lcom/dsi/ant/channel/ChannelsController$ChannelAcquiredStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/AntChannelProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/channel/AntChannelProviderImpl;


# direct methods
.method constructor <init>(Lcom/dsi/ant/channel/AntChannelProviderImpl;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$1;->this$0:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChannelAcquired()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$1;->this$0:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    # getter for: Lcom/dsi/ant/channel/AntChannelProviderImpl;->mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;
    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->access$100(Lcom/dsi/ant/channel/AntChannelProviderImpl;)Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2}, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 129
    return-void
.end method

.method public final onChannelAvailable(Z)V
    .locals 4
    .param p1, "isNewChannelAvailable"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProviderImpl$1;->this$0:Lcom/dsi/ant/channel/AntChannelProviderImpl;

    # getter for: Lcom/dsi/ant/channel/AntChannelProviderImpl;->mBroadcastChannelStateChangeCallbackHandler:Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;
    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->access$100(Lcom/dsi/ant/channel/AntChannelProviderImpl;)Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;

    move-result-object v3

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v1, v0, v2}, Lcom/dsi/ant/channel/AntChannelProviderImpl$BroadcastChannelStateChangeHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 122
    return-void

    :cond_0
    move v0, v2

    .line 118
    goto :goto_0
.end method
