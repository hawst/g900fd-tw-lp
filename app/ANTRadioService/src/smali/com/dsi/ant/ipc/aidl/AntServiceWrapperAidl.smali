.class public final Lcom/dsi/ant/ipc/aidl/AntServiceWrapperAidl;
.super Lcom/dsi/ant/ipc/aidl/IAntServiceAidl$Stub;
.source "AntServiceWrapperAidl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/ipc/aidl/AntServiceWrapperAidl$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dsi/ant/ipc/aidl/IAntServiceAidl$Stub;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public final getComponent(I)Landroid/os/IBinder;
    .locals 3
    .param p1, "component"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x0

    .line 27
    .local v0, "binder":Landroid/os/IBinder;
    sget-object v1, Lcom/dsi/ant/ipc/aidl/AntServiceWrapperAidl$1;->$SwitchMap$com$dsi$ant$AntService$Component:[I

    invoke-static {p1}, Lcom/dsi/ant/AntService$Component;->create(I)Lcom/dsi/ant/AntService$Component;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/AntService$Component;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 30
    :goto_0
    return-object v0

    .line 29
    :pswitch_0
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;

    .end local v0    # "binder":Landroid/os/IBinder;
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/ChannelProviderWrapperAidl;-><init>(Lcom/dsi/ant/channel/AntChannelProviderImpl;)V

    .restart local v0    # "binder":Landroid/os/IBinder;
    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
