.class public final Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;
.super Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
.source "BroadcastDataMessageFromHost.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mPayload:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "payload"    # [B

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;-><init>()V

    .line 32
    array-length v0, p1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Broadcast payload data length invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;->mPayload:[B

    .line 37
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x0

    .line 48
    const/16 v1, 0x9

    new-array v0, v1, [B

    .line 52
    .local v0, "messageContent":[B
    invoke-static {p1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 53
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;->mPayload:[B

    const/4 v2, 0x1

    const/16 v3, 0x8

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final getPayload()[B
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;->mPayload:[B

    return-object v0
.end method
