.class public final Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "SearchTimeoutMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mSearchTimeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    .locals 1
    .param p1, "searchTimeout"    # Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 41
    invoke-virtual {p1}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->getRawValue()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->mSearchTimeout:I

    .line 42
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v0, 0x1

    .line 44
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 45
    invoke-static {p1, v0, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->mSearchTimeout:I

    .line 47
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 4
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v3, 0x1

    .line 61
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 63
    .local v0, "content":[B
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 64
    iget v1, p0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->mSearchTimeout:I

    int-to-long v1, v1

    invoke-static {v1, v2, v0, v3, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 67
    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final getRawSearchTimeout()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->mSearchTimeout:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, "Search Timeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget v1, p0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;->mSearchTimeout:I

    mul-int/lit16 v1, v1, 0x9c4

    invoke-static {v1}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->create(I)Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
