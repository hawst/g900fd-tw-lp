.class public final Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;
.super Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
.source "BurstTransferDataMessageFromHost.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mPayload:[B

.field public final sequenceNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 2
    .param p1, "sequenceNumberByte"    # I
    .param p2, "payload"    # [B

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;-><init>()V

    .line 58
    array-length v0, p2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Burst packet payload data length invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    int-to-byte v0, p1

    and-int/lit16 v0, v0, 0xe0

    shr-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->sequenceNumber:I

    .line 65
    iput-object p2, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->mPayload:[B

    .line 66
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    const/16 v1, 0x9

    new-array v0, v1, [B

    .line 101
    .local v0, "content":[B
    iget v1, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->sequenceNumber:I

    shl-int/lit8 v1, v1, 0x5

    add-int/2addr v1, p1

    .line 103
    int-to-long v1, v1

    invoke-static {v1, v2, v0, v4, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 104
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->mPayload:[B

    const/16 v2, 0x8

    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final getPayload()[B
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->mPayload:[B

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string v1, "Sequence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    iget v1, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->sequenceNumber:I

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    iget v1, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->sequenceNumber:I

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    const-string v1, " (FIRST)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    const/16 v1, 0x80

    iget v2, p0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;->sequenceNumber:I

    invoke-static {v1, v2}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, " (LAST)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 122
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
