.class public final Lcom/dsi/ant/message/Timestamp;
.super Ljava/lang/Object;
.source "Timestamp.java"


# instance fields
.field private mRxTimestamp:I


# direct methods
.method public constructor <init>([BI)V
    .locals 2
    .param p1, "messageContent"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    add-int/lit8 v0, p2, 0x0

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    .line 40
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    if-ne p0, p1, :cond_1

    .line 88
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 74
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 75
    goto :goto_0

    .line 78
    :cond_2
    instance-of v2, p1, Lcom/dsi/ant/message/Timestamp;

    if-nez v2, :cond_3

    move v0, v1

    .line 79
    goto :goto_0

    .line 82
    :cond_3
    check-cast p1, Lcom/dsi/ant/message/Timestamp;

    .line 84
    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p1, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    iget v3, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 85
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    add-int/lit16 v0, v0, 0xd9

    .line 64
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Timestamp:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, " Rx="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    iget v1, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
