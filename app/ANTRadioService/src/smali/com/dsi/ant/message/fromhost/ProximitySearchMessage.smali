.class public final Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ProximitySearchMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mSearchThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->PROXIMITY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "searchThreshold"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 52
    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Search threshold outside valid range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iput p1, p0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->mSearchThreshold:I

    .line 57
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 1
    .param p1, "messageContent"    # [B

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;-><init>(I)V

    .line 61
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumbe"    # I

    .prologue
    const/4 v4, 0x1

    .line 75
    const/4 v1, 0x5

    new-array v0, v1, [B

    .line 77
    .local v0, "content":[B
    int-to-long v1, p1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v4, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 79
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->mSearchThreshold:I

    invoke-static {v1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 81
    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v1, "Search threshold bin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->mSearchThreshold:I

    if-nez v1, :cond_0

    .line 97
    const-string v1, "[Proximity search disabled]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 99
    :cond_0
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;->mSearchThreshold:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
