.class public final Lcom/dsi/ant/message/LibConfig;
.super Ljava/lang/Object;
.source "LibConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;,
        Lcom/dsi/ant/message/LibConfig$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/message/LibConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBundleData:Lcom/dsi/ant/message/LibConfig$BundleData;

.field private mEnableChannelIdOutput:Z

.field private mEnableRssiOutput:Z

.field private mEnableRxTimestampOutput:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 320
    new-instance v0, Lcom/dsi/ant/message/LibConfig$1;

    invoke-direct {v0}, Lcom/dsi/ant/message/LibConfig$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/message/LibConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, v0, v0, v0}, Lcom/dsi/ant/message/LibConfig;-><init>(ZZZ)V

    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v1, Lcom/dsi/ant/message/LibConfig$BundleData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/LibConfig$BundleData;-><init>(B)V

    iput-object v1, p0, Lcom/dsi/ant/message/LibConfig;->mBundleData:Lcom/dsi/ant/message/LibConfig$BundleData;

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 256
    .local v0, "version":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    sget-object v2, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v2

    if-le v1, v2, :cond_1

    :goto_0
    new-array v1, v1, [Z

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    sget-object v2, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->CHANNEL_ID:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v2

    aget-boolean v2, v1, v2

    iput-boolean v2, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    sget-object v2, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RSSI:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v2

    aget-boolean v2, v1, v2

    iput-boolean v2, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    sget-object v2, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RX_TIMESTAMP:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v2}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v2

    aget-boolean v1, v1, v2

    iput-boolean v1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    .line 258
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/message/LibConfig$BundleData;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v2, "com.dsi.ant.message.libconfig.bundledata"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/message/LibConfig$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/message/LibConfig;->mBundleData:Lcom/dsi/ant/message/LibConfig$BundleData;

    .line 261
    :cond_0
    return-void

    .line 256
    :cond_1
    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v1

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/LibConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(ZZZ)V
    .locals 2
    .param p1, "enableChannelIdOutput"    # Z
    .param p2, "enableRssiOutput"    # Z
    .param p3, "enableRxTimestampOutput"    # Z

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v0, Lcom/dsi/ant/message/LibConfig$BundleData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/LibConfig$BundleData;-><init>(B)V

    iput-object v0, p0, Lcom/dsi/ant/message/LibConfig;->mBundleData:Lcom/dsi/ant/message/LibConfig$BundleData;

    .line 71
    iput-boolean p1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    .line 72
    iput-boolean p2, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    .line 73
    iput-boolean p3, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    .line 74
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154
    if-ne p0, p1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v1

    .line 158
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 159
    goto :goto_0

    .line 162
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/message/LibConfig;

    if-nez v3, :cond_3

    move v1, v2

    .line 163
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 166
    check-cast v0, Lcom/dsi/ant/message/LibConfig;

    .line 168
    .local v0, "other":Lcom/dsi/ant/message/LibConfig;
    iget-boolean v3, v0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    iget-boolean v4, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, v0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    iget-boolean v4, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, v0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    iget-boolean v4, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 171
    goto :goto_0
.end method

.method public final getEnableChannelIdOutput()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    return v0
.end method

.method public final getEnableRssiOutput()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    return v0
.end method

.method public final getEnableRxTimestampOutput()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    return v0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 141
    iget-boolean v1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0xd9

    .line 145
    .local v0, "result":I
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 146
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int/2addr v1, v2

    .line 148
    return v1

    .end local v0    # "result":I
    :cond_0
    move v1, v3

    .line 141
    goto :goto_0

    .restart local v0    # "result":I
    :cond_1
    move v1, v3

    .line 145
    goto :goto_1

    :cond_2
    move v2, v3

    .line 146
    goto :goto_2
.end method

.method public final setEnableChannelIdOutput$1385ff()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    return-void
.end method

.method public final setEnableRssiOutput$1385ff()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    return-void
.end method

.method public final setEnableRxTimestampOutput$1385ff()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Lib Config: Enabled extended data:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 182
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    if-eqz v1, :cond_0

    .line 183
    const-string v1, " -Channel Id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_0
    iget-boolean v1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    if-eqz v1, :cond_1

    .line 187
    const-string v1, " -RSSI"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_1
    iget-boolean v1, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    if-eqz v1, :cond_2

    .line 191
    const-string v1, " -Rx Timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 292
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 294
    sget-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v0}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v0

    new-array v0, v0, [Z

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->CHANNEL_ID:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/message/LibConfig;->mEnableChannelIdOutput:Z

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RSSI:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRssiOutput:Z

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RX_TIMESTAMP:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/message/LibConfig;->mEnableRxTimestampOutput:Z

    aput-boolean v2, v0, v1

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->ordinal()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 296
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.dsi.ant.message.libconfig.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/message/LibConfig;->mBundleData:Lcom/dsi/ant/message/LibConfig$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 299
    :cond_0
    return-void
.end method
