.class public abstract Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.super Lcom/dsi/ant/message/AntMessage;
.source "AntMessageFromHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/AntMessageFromHost$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    .line 266
    return-void
.end method

.method public static createAntMessage$5f6f8477(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .locals 6
    .param p0, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 106
    invoke-static {p0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->create$4a3c7ec6(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v1

    .line 108
    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/message/fromhost/AntMessageFromHost$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    sget-object v3, Lcom/dsi/ant/message/fromhost/DataMessageFromHost$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;

    invoke-static {v2}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getStandardPayload([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;-><init>([B)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;

    invoke-static {v2}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getStandardPayload([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;-><init>([B)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;

    invoke-static {v2, v5}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v1

    invoke-static {v2}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getStandardPayload([B)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;-><init>(I[B)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;-><init>([B)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;-><init>([B)V

    goto :goto_0

    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;-><init>([B)V

    goto :goto_0

    :pswitch_8
    new-instance v0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;-><init>([B)V

    goto :goto_0

    :pswitch_9
    new-instance v0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;-><init>([B)V

    goto :goto_0

    :pswitch_a
    new-instance v0, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;-><init>(B)V

    goto :goto_0

    :pswitch_b
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;-><init>([B)V

    goto :goto_0

    :pswitch_c
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;-><init>([B)V

    goto :goto_0

    :pswitch_d
    new-instance v0, Lcom/dsi/ant/message/fromhost/CrystalEnableMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/CrystalEnableMessage;-><init>(B)V

    goto :goto_0

    :pswitch_e
    new-instance v0, Lcom/dsi/ant/message/fromhost/CwInitMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/CwInitMessage;-><init>(B)V

    goto :goto_0

    :pswitch_f
    new-instance v0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;-><init>([B)V

    goto :goto_0

    :pswitch_10
    new-instance v0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;-><init>([B)V

    goto :goto_0

    :pswitch_11
    new-instance v0, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;-><init>([B)V

    goto :goto_0

    :pswitch_12
    new-instance v0, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;-><init>(B)V

    goto/16 :goto_0

    :pswitch_13
    new-instance v0, Lcom/dsi/ant/message/fromhost/OpenRxScanModeMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/OpenRxScanModeMessage;-><init>(B)V

    goto/16 :goto_0

    :pswitch_14
    new-instance v0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_15
    new-instance v0, Lcom/dsi/ant/message/fromhost/RequestMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/RequestMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_16
    new-instance v0, Lcom/dsi/ant/message/fromhost/ResetSystemMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/ResetSystemMessage;-><init>(B)V

    goto/16 :goto_0

    :pswitch_17
    new-instance v0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_18
    new-instance v0, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_19
    sget-object v0, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message Type not a network key message"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1a
    new-instance v0, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;

    const/16 v1, 0x8

    invoke-static {v2, v1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKey([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_1b
    new-instance v0, Lcom/dsi/ant/message/fromhost/SetLongNetworkKeyMessage;

    const/16 v1, 0x10

    invoke-static {v2, v1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKey([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/SetLongNetworkKeyMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_1c
    new-instance v0, Lcom/dsi/ant/message/fromhost/SleepMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/SleepMessage;-><init>(B)V

    goto/16 :goto_0

    :pswitch_1d
    new-instance v0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;

    invoke-direct {v0, v2}, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;-><init>([B)V

    goto/16 :goto_0

    :pswitch_1e
    new-instance v0, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v0, v5}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>(B)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method


# virtual methods
.method public final getMessageContent()[B
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p0, v0, v0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContent(II)[B

    move-result-object v0

    return-object v0
.end method

.method public abstract getMessageContent(II)[B
.end method

.method public final getMessageId()I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v0

    return v0
.end method

.method public abstract getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
.end method

.method public final getRawMessage(II)[B
    .locals 6
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v5, 0x0

    .line 83
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContent(II)[B

    move-result-object v0

    .line 84
    .local v0, "messageContent":[B
    array-length v2, v0

    .line 85
    .local v2, "messageLength":I
    add-int/lit8 v4, v2, 0x2

    new-array v3, v4, [B

    .line 86
    .local v3, "rawMessage":[B
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v1

    .line 88
    .local v1, "messageId":I
    invoke-static {v2, v3, v5}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 89
    const/4 v4, 0x1

    invoke-static {v1, v3, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 90
    const/4 v4, 0x2

    invoke-static {v0, v5, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final toStringHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
