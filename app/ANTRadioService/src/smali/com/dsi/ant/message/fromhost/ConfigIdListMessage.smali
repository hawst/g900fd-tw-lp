.class public final Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ConfigIdListMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mExclude:Z

.field private final mListSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2
    .param p1, "listSize"    # I
    .param p2, "exclude"    # Z

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 57
    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "List Index out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    iput p1, p0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->mListSize:I

    .line 63
    iput-boolean p2, p0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->mExclude:Z

    .line 64
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v0, 0x1

    .line 67
    invoke-static {p1, v0, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {p1}, Lcom/dsi/ant/message/MessageUtils;->booleanFromByte$49634b8b([B)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;-><init>(IZ)V

    .line 69
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 91
    const/4 v3, 0x3

    new-array v0, v3, [B

    .line 93
    .local v0, "content":[B
    invoke-static {p1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 94
    iget v3, p0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->mListSize:I

    int-to-long v3, v3

    invoke-static {v3, v4, v0, v1, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 96
    iget-boolean v3, p0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->mExclude:Z

    const/4 v4, 0x2

    if-eqz v3, :cond_0

    :goto_0
    int-to-byte v1, v1

    aput-byte v1, v0, v4

    .line 98
    return-object v0

    :cond_0
    move v1, v2

    .line 96
    goto :goto_0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, "Size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->mListSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget-boolean v1, p0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;->mExclude:Z

    if-eqz v1, :cond_0

    .line 115
    const-string v1, "Exclude"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 117
    :cond_0
    const-string v1, "Include"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
