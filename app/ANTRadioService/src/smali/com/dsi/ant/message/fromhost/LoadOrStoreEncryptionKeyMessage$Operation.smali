.class public final enum Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
.super Ljava/lang/Enum;
.source "LoadOrStoreEncryptionKeyMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Operation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

.field public static final enum LOAD:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

.field public static final enum STORE:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

.field public static final enum UNKNOWN:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

.field private static final sValues:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    const-string v1, "LOAD"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->LOAD:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    .line 39
    new-instance v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    const-string v1, "STORE"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->STORE:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    .line 42
    new-instance v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    const-string v1, "UNKNOWN"

    const v2, 0xffff

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->UNKNOWN:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    sget-object v1, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->LOAD:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->STORE:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->UNKNOWN:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->$VALUES:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    .line 47
    invoke-static {}, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->values()[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->sValues:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 70
    sget-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->UNKNOWN:Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    .line 72
    .local v0, "code":Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->sValues:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 73
    sget-object v2, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->sValues:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 74
    sget-object v2, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->sValues:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    aget-object v0, v2, v1

    .line 79
    :cond_0
    return-object v0

    .line 73
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 72
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->$VALUES:[Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    return-object v0
.end method
