.class public Lcom/dsi/ant/message/fromant/ChannelEventMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "ChannelEventMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# instance fields
.field private final mEventCode:Lcom/dsi/ant/message/EventCode;

.field private final mRawEventCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 57
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mMessageContent:[B

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mRawEventCode:I

    .line 59
    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mRawEventCode:I

    invoke-static {v0}, Lcom/dsi/ant/message/EventCode;->create(I)Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mEventCode:Lcom/dsi/ant/message/EventCode;

    .line 60
    return-void
.end method


# virtual methods
.method public final getEventCode()Lcom/dsi/ant/message/EventCode;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mEventCode:Lcom/dsi/ant/message/EventCode;

    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string v1, "Event Code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mEventCode:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->mRawEventCode:I

    invoke-static {v2}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
