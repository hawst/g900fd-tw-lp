.class public abstract Lcom/dsi/ant/message/fromant/DataMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "DataMessage.java"


# instance fields
.field private mPayload:[B


# direct methods
.method protected constructor <init>([B)V
    .locals 1
    .param p1, "messageContent"    # [B

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    .line 48
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x8

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/DataMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, "Payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    if-nez v2, :cond_0

    new-array v2, v6, [B

    iput-object v2, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mMessageContent:[B

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    invoke-static {v2}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-static {p0}, Lcom/dsi/ant/message/ExtendedData;->hasExtendedData(Lcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    new-instance v1, Lcom/dsi/ant/message/ExtendedData;

    invoke-direct {v1, p0}, Lcom/dsi/ant/message/ExtendedData;-><init>(Lcom/dsi/ant/message/fromant/DataMessage;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/ExtendedData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
