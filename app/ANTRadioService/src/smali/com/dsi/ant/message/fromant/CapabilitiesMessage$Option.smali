.class public final enum Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
.super Ljava/lang/Enum;
.source "CapabilitiesMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Option"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_ADVANCED_BURST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_EVENT_BUFFERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_EVENT_FILTERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_EXT_ASSIGN_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_EXT_MESSAGE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_FS_ANTFS_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_HIGH_DUTY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_LED_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_LOW_PRIORITY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NETWORK_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NO_ACKD_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NO_BURST_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NO_RECEIVE_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NO_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NO_TRANSMIT_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_NO_TRANSMIT_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_PER_CHANNEL_TX_POWER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_PROX_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_SCAN_MODE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_SCRIPT_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_SEARCH_LIST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_SELECTIVE_DATA_UPDATES_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum CAPABILITIES_SERIAL_NUMBER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

.field public static final enum NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NO_RECEIVE_CHANNELS"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_RECEIVE_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 32
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NO_TRANSMIT_CHANNELS"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_TRANSMIT_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 33
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NO_RECEIVE_MESSAGES"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 34
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NO_TRANSMIT_MESSAGES"

    invoke-direct {v0, v1, v6}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_TRANSMIT_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 35
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NO_ACKD_MESSAGES"

    invoke-direct {v0, v1, v7}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_ACKD_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 36
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NO_BURST_MESSAGES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_BURST_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 37
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_NETWORK_ENABLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NETWORK_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 38
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_SERIAL_NUMBER_ENABLED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SERIAL_NUMBER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 39
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_PER_CHANNEL_TX_POWER_ENABLED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_PER_CHANNEL_TX_POWER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 40
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_LOW_PRIORITY_SEARCH_ENABLED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_LOW_PRIORITY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 41
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_SCRIPT_ENABLED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SCRIPT_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 42
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_SEARCH_LIST_ENABLED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SEARCH_LIST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 43
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_LED_ENABLED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_LED_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 44
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_EXT_MESSAGE_ENABLED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EXT_MESSAGE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 45
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_SCAN_MODE_ENABLED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SCAN_MODE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 46
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_PROX_SEARCH_ENABLED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_PROX_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 47
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_EXT_ASSIGN_ENABLED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EXT_ASSIGN_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 48
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_FS_ANTFS_ENABLED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_FS_ANTFS_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 49
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_ADVANCED_BURST_ENABLED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_ADVANCED_BURST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 50
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_EVENT_BUFFERING_ENABLED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EVENT_BUFFERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 51
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_EVENT_FILTERING_ENABLED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EVENT_FILTERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 52
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_HIGH_DUTY_SEARCH_ENABLED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_HIGH_DUTY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 53
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "CAPABILITIES_SELECTIVE_DATA_UPDATES_ENABLED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SELECTIVE_DATA_UPDATES_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 54
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    const-string v1, "NUMBER_OF_CAPABILITIES"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .line 30
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_RECEIVE_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_TRANSMIT_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_TRANSMIT_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_ACKD_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_BURST_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NETWORK_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SERIAL_NUMBER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_PER_CHANNEL_TX_POWER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_LOW_PRIORITY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SCRIPT_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SEARCH_LIST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_LED_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EXT_MESSAGE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SCAN_MODE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_PROX_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EXT_ASSIGN_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_FS_ANTFS_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_ADVANCED_BURST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EVENT_BUFFERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EVENT_FILTERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_HIGH_DUTY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SELECTIVE_DATA_UPDATES_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->$VALUES:[Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->$VALUES:[Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    return-object v0
.end method
