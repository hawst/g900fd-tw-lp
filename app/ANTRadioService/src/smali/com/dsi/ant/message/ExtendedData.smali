.class public final Lcom/dsi/ant/message/ExtendedData;
.super Ljava/lang/Object;
.source "ExtendedData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/ExtendedData$1;
    }
.end annotation


# instance fields
.field private mChannelId:Lcom/dsi/ant/message/ChannelId;

.field private mRssi:Lcom/dsi/ant/message/Rssi;

.field private mTimestamp:Lcom/dsi/ant/message/Timestamp;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/message/fromant/DataMessage;)V
    .locals 4
    .param p1, "dataMessage"    # Lcom/dsi/ant/message/fromant/DataMessage;

    .prologue
    const/16 v0, 0xa

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    .line 48
    iput-object v1, p0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    .line 49
    iput-object v1, p0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    .line 57
    invoke-static {p1}, Lcom/dsi/ant/message/ExtendedData;->hasExtendedData(Lcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 58
    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v1

    const/16 v2, 0x9

    invoke-static {v1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v2

    const/16 v3, 0x80

    invoke-static {v3, v2}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/dsi/ant/message/ChannelId;

    invoke-direct {v3, v1, v0}, Lcom/dsi/ant/message/ChannelId;-><init>([BI)V

    iput-object v3, p0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    const/16 v0, 0xe

    :cond_0
    const/16 v3, 0x40

    invoke-static {v3, v2}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Lcom/dsi/ant/message/Rssi;

    invoke-direct {v3, v1, v0}, Lcom/dsi/ant/message/Rssi;-><init>([BI)V

    iput-object v3, p0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    add-int/lit8 v0, v0, 0x3

    :cond_1
    const/16 v3, 0x20

    invoke-static {v3, v2}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lcom/dsi/ant/message/Timestamp;

    invoke-direct {v2, v1, v0}, Lcom/dsi/ant/message/Timestamp;-><init>([BI)V

    iput-object v2, p0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    .line 62
    :cond_2
    return-void
.end method

.method private hasChannelId()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasExtendedData(Lcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z
    .locals 3
    .param p0, "antMessage"    # Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 102
    .local v0, "extendedDataPresent":Z
    sget-object v1, Lcom/dsi/ant/message/ExtendedData$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 107
    :pswitch_0
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v1

    array-length v1, v1

    const/16 v2, 0x9

    if-le v1, v2, :cond_0

    .line 108
    const/4 v0, 0x1

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private hasRssi()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasTimestamp()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 190
    if-ne p0, p1, :cond_1

    .line 228
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 195
    goto :goto_0

    .line 198
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/message/ExtendedData;

    if-nez v3, :cond_3

    move v1, v2

    .line 199
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 202
    check-cast v0, Lcom/dsi/ant/message/ExtendedData;

    .line 204
    .local v0, "other":Lcom/dsi/ant/message/ExtendedData;
    invoke-direct {v0}, Lcom/dsi/ant/message/ExtendedData;->hasChannelId()Z

    move-result v3

    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasChannelId()Z

    move-result v4

    if-ne v3, v4, :cond_4

    invoke-direct {v0}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v3

    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v4

    if-ne v3, v4, :cond_4

    invoke-direct {v0}, Lcom/dsi/ant/message/ExtendedData;->hasTimestamp()Z

    move-result v3

    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasTimestamp()Z

    move-result v4

    if-eq v3, v4, :cond_5

    :cond_4
    move v1, v2

    .line 207
    goto :goto_0

    .line 210
    :cond_5
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasChannelId()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 211
    iget-object v3, p0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    iget-object v4, v0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 212
    goto :goto_0

    .line 216
    :cond_6
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 217
    iget-object v3, p0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    iget-object v4, v0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/message/Rssi;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 218
    goto :goto_0

    .line 222
    :cond_7
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasTimestamp()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    iget-object v3, p0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    iget-object v4, v0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/message/Timestamp;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 224
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 170
    const/4 v0, 0x7

    .line 173
    .local v0, "result":I
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasChannelId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0xd9

    .line 176
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    invoke-virtual {v2}, Lcom/dsi/ant/message/Rssi;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasTimestamp()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 181
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    invoke-virtual {v2}, Lcom/dsi/ant/message/Timestamp;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 184
    :cond_2
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Extended data:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 236
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasChannelId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/ExtendedData;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v2}, Lcom/dsi/ant/message/ChannelId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/ExtendedData;->mRssi:Lcom/dsi/ant/message/Rssi;

    invoke-virtual {v2}, Lcom/dsi/ant/message/Rssi;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    :cond_1
    invoke-direct {p0}, Lcom/dsi/ant/message/ExtendedData;->hasTimestamp()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 245
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/ExtendedData;->mTimestamp:Lcom/dsi/ant/message/Timestamp;

    invoke-virtual {v2}, Lcom/dsi/ant/message/Timestamp;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
