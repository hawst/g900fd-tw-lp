.class public final Lcom/dsi/ant/message/fromant/GeneratedChannelEventMessage;
.super Lcom/dsi/ant/message/fromant/ChannelEventMessage;
.source "GeneratedChannelEventMessage.java"


# direct methods
.method public constructor <init>(ILcom/dsi/ant/message/EventCode;)V
    .locals 4
    .param p1, "channelNumber"    # I
    .param p2, "eventCode"    # Lcom/dsi/ant/message/EventCode;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {v3, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    const/16 v1, 0x40

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    invoke-static {v2, v0, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    invoke-virtual {p2}, Lcom/dsi/ant/message/EventCode;->getRawValue()I

    move-result v1

    const/4 v2, 0x4

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>([B)V

    .line 33
    return-void
.end method
