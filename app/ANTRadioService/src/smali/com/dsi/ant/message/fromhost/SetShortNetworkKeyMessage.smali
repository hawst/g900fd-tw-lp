.class public final Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;
.super Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
.source "SetShortNetworkKeyMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "networkKey"    # [B

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;-><init>([B)V

    .line 38
    return-void
.end method


# virtual methods
.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method protected final getNetworkKeyLengthBytes()I
    .locals 1

    .prologue
    .line 45
    const/16 v0, 0x8

    return v0
.end method
