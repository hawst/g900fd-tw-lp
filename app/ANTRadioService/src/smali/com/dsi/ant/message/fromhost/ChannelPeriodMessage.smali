.class public final Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ChannelPeriodMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mChannelPeriod:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_PERIOD:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 58
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    .line 60
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    .line 120
    const/4 v1, 0x3

    new-array v0, v1, [B

    .line 122
    .local v0, "content":[B
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 123
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    int-to-long v1, v1

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v0, v3, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 126
    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, "Channel period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    const-wide/high16 v3, 0x40e0000000000000L    # 32768.0

    int-to-double v5, v2

    div-double v2, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Hz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
