.class public final Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "ChannelResponseMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# instance fields
.field private mInitiatingMessageId:I

.field private mRawResponseCode:I

.field private mResponseCode:Lcom/dsi/ant/message/ResponseCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v2, 0x1

    .line 61
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 63
    iput-object p1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mMessageContent:[B

    invoke-static {p1, v2, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mInitiatingMessageId:I

    const/4 v0, 0x2

    invoke-static {p1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    invoke-static {v0}, Lcom/dsi/ant/message/ResponseCode;->create(I)Lcom/dsi/ant/message/ResponseCode;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mResponseCode:Lcom/dsi/ant/message/ResponseCode;

    .line 64
    return-void
.end method


# virtual methods
.method public final getInitiatingMessageId()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mInitiatingMessageId:I

    return v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public final getRawResponseCode()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    return v0
.end method

.method public final getResponseCode()Lcom/dsi/ant/message/ResponseCode;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mResponseCode:Lcom/dsi/ant/message/ResponseCode;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 107
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string v1, "Response to="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget v1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mInitiatingMessageId:I

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, "code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    iget-object v1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mResponseCode:Lcom/dsi/ant/message/ResponseCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget v1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
