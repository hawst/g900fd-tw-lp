.class public final Lcom/dsi/ant/message/ExtendedAssignment;
.super Ljava/lang/Object;
.source "ExtendedAssignment.java"


# instance fields
.field public mEnableBackgroundScanning:Z

.field public mEnableFrequencyAgility:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    .line 36
    iput-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    .line 44
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "extendedAssignmentByte"    # I

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    .line 36
    iput-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    .line 55
    const/4 v0, 0x1

    invoke-static {v0, p1}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    .line 58
    const/4 v0, 0x4

    invoke-static {v0, p1}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    .line 60
    return-void
.end method


# virtual methods
.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Extended Assignment="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/dsi/ant/message/ExtendedAssignment;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120
    iget-boolean v1, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    if-eqz v1, :cond_0

    .line 121
    const-string v1, " -Background Scanning"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-boolean v1, p0, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    if-eqz v1, :cond_1

    .line 125
    const-string v1, " -Frequency Agility"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 128
    :cond_2
    const-string v1, "[Not Enabled]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
