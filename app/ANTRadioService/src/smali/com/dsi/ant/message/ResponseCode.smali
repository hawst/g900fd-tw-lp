.class public final enum Lcom/dsi/ant/message/ResponseCode;
.super Ljava/lang/Enum;
.source "ResponseCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/ResponseCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/ResponseCode;

.field public static final enum CHANNEL_ID_NOT_SET:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum CHANNEL_IN_WRONG_STATE:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum CHANNEL_NOT_OPENED:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum INVALID_LIST_ID:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum INVALID_MESSAGE:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum INVALID_PARAMETER_PROVIDED:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum MESSAGE_SIZE_EXCEEDS_LIMIT:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

.field public static final enum UNKNOWN:Lcom/dsi/ant/message/ResponseCode;

.field private static final sValues:[Lcom/dsi/ant/message/ResponseCode;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "RESPONSE_NO_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    .line 26
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "CHANNEL_IN_WRONG_STATE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_IN_WRONG_STATE:Lcom/dsi/ant/message/ResponseCode;

    .line 29
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "CHANNEL_NOT_OPENED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_NOT_OPENED:Lcom/dsi/ant/message/ResponseCode;

    .line 32
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "CHANNEL_ID_NOT_SET"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_ID_NOT_SET:Lcom/dsi/ant/message/ResponseCode;

    .line 36
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "MESSAGE_SIZE_EXCEEDS_LIMIT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->MESSAGE_SIZE_EXCEEDS_LIMIT:Lcom/dsi/ant/message/ResponseCode;

    .line 39
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "INVALID_MESSAGE"

    const/4 v2, 0x5

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->INVALID_MESSAGE:Lcom/dsi/ant/message/ResponseCode;

    .line 42
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "INVALID_LIST_ID"

    const/4 v2, 0x6

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->INVALID_LIST_ID:Lcom/dsi/ant/message/ResponseCode;

    .line 46
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "INVALID_PARAMETER_PROVIDED"

    const/4 v2, 0x7

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->INVALID_PARAMETER_PROVIDED:Lcom/dsi/ant/message/ResponseCode;

    .line 53
    new-instance v0, Lcom/dsi/ant/message/ResponseCode;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x8

    const v3, 0xffff

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->UNKNOWN:Lcom/dsi/ant/message/ResponseCode;

    .line 19
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/dsi/ant/message/ResponseCode;

    sget-object v1, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_IN_WRONG_STATE:Lcom/dsi/ant/message/ResponseCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_NOT_OPENED:Lcom/dsi/ant/message/ResponseCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_ID_NOT_SET:Lcom/dsi/ant/message/ResponseCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/message/ResponseCode;->MESSAGE_SIZE_EXCEEDS_LIMIT:Lcom/dsi/ant/message/ResponseCode;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->INVALID_MESSAGE:Lcom/dsi/ant/message/ResponseCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->INVALID_LIST_ID:Lcom/dsi/ant/message/ResponseCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->INVALID_PARAMETER_PROVIDED:Lcom/dsi/ant/message/ResponseCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->UNKNOWN:Lcom/dsi/ant/message/ResponseCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->$VALUES:[Lcom/dsi/ant/message/ResponseCode;

    .line 58
    invoke-static {}, Lcom/dsi/ant/message/ResponseCode;->values()[Lcom/dsi/ant/message/ResponseCode;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/ResponseCode;->sValues:[Lcom/dsi/ant/message/ResponseCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/ResponseCode;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/ResponseCode;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 82
    sget-object v0, Lcom/dsi/ant/message/ResponseCode;->UNKNOWN:Lcom/dsi/ant/message/ResponseCode;

    .line 84
    .local v0, "code":Lcom/dsi/ant/message/ResponseCode;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->sValues:[Lcom/dsi/ant/message/ResponseCode;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 85
    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->sValues:[Lcom/dsi/ant/message/ResponseCode;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/message/ResponseCode;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 86
    sget-object v2, Lcom/dsi/ant/message/ResponseCode;->sValues:[Lcom/dsi/ant/message/ResponseCode;

    aget-object v0, v2, v1

    .line 91
    :cond_0
    return-object v0

    .line 85
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 84
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/ResponseCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/dsi/ant/message/ResponseCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ResponseCode;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/ResponseCode;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/message/ResponseCode;->$VALUES:[Lcom/dsi/ant/message/ResponseCode;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/ResponseCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/ResponseCode;

    return-object v0
.end method


# virtual methods
.method public final getRawValue()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/dsi/ant/message/ResponseCode;->mRawValue:I

    return v0
.end method
