.class public final Lcom/dsi/ant/message/fromhost/AssignChannelMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "AssignChannelMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

.field private final mRawChannelType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 67
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 68
    invoke-static {p1, v0, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    .line 70
    array-length v0, p1

    if-le v0, v2, :cond_0

    .line 72
    new-instance v0, Lcom/dsi/ant/message/ExtendedAssignment;

    invoke-static {p1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/ExtendedAssignment;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    new-instance v0, Lcom/dsi/ant/message/ExtendedAssignment;

    invoke-direct {v0}, Lcom/dsi/ant/message/ExtendedAssignment;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    goto :goto_0
.end method


# virtual methods
.method public final getExtendedAssignment()Lcom/dsi/ant/message/ExtendedAssignment;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    return-object v0
.end method

.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    const/4 v3, 0x4

    new-array v0, v3, [B

    .line 111
    .local v0, "content":[B
    invoke-static {p1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 112
    iget v3, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    invoke-static {v3, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 113
    const/4 v3, 0x2

    invoke-static {p2, v0, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 114
    iget-object v3, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    iget-boolean v4, v3, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableBackgroundScanning:Z

    if-eqz v4, :cond_1

    :goto_0
    iget-boolean v2, v3, Lcom/dsi/ant/message/ExtendedAssignment;->mEnableFrequencyAgility:Z

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x4

    int-to-byte v1, v1

    :cond_0
    const/4 v2, 0x3

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 117
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string v1, "Channel Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    invoke-static {v2}, Lcom/dsi/ant/message/ChannelType;->create(I)Lcom/dsi/ant/message/ChannelType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/ChannelType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
