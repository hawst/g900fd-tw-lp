.class public final enum Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
.super Ljava/lang/Enum;
.source "ConfigureEventBufferMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BufferEvents"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

.field public static final enum ALL:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

.field public static final enum LOW_PRIORITY:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

.field public static final enum NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

.field private static final sValues:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 88
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    const-string v1, "LOW_PRIORITY"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->LOW_PRIORITY:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 91
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->ALL:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 94
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    const-string v1, "NONE"

    const v2, 0xffff

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 85
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->LOW_PRIORITY:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->ALL:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->$VALUES:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 99
    invoke-static {}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->values()[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->sValues:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 102
    iput p3, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->mRawValue:I

    .line 103
    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
    .locals 4
    .param p0, "rawValue"    # I

    .prologue
    .line 125
    sget-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 127
    .local v0, "code":Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->sValues:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 128
    sget-object v2, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->sValues:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    aget-object v2, v2, v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    sget-object v2, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->sValues:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    aget-object v0, v2, v1

    .line 134
    :cond_0
    return-object v0

    .line 127
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 85
    const-class v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->$VALUES:[Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    return-object v0
.end method


# virtual methods
.method public final getRawValue()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->mRawValue:I

    return v0
.end method
