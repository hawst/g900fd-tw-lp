.class public final Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ConfigureEventBufferMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
    }
.end annotation


# static fields
.field public static final DISABLE_EVENT_BUFFERING:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;

.field private static final EVENTS_DISABLE_EVENT_BUFFERING:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

.field private final mBufferFlushSize:I

.field private final mBufferFlushTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_BUFFER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 64
    sget-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->LOW_PRIORITY:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->EVENTS_DISABLE_EVENT_BUFFERING:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 79
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;

    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->EVENTS_DISABLE_EVENT_BUFFERING:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-direct {v0, v1, v2, v2}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;-><init>(Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;II)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->DISABLE_EVENT_BUFFERING:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;II)V
    .locals 3
    .param p1, "events"    # Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;
    .param p2, "bufferSize"    # I
    .param p3, "bufferTime"    # I

    .prologue
    const v2, 0xffff

    const/4 v1, 0x0

    .line 163
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 165
    sget-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    if-eq v0, p1, :cond_1

    .line 167
    invoke-static {p2, v1, v2}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Buffer size out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    invoke-static {p3, v1, v2}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_1

    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Buffer time out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_1
    iput-object p1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 177
    iput p2, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushSize:I

    .line 178
    iput p3, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    .line 179
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x2

    .line 181
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 182
    invoke-static {p1, v0, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->create(I)Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    .line 184
    invoke-static {p1, v2, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushSize:I

    .line 186
    const/4 v0, 0x4

    invoke-static {p1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    .line 188
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 8
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 235
    const/4 v1, 0x6

    new-array v0, v1, [B

    .line 238
    .local v0, "content":[B
    invoke-static {v2, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 240
    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    iget-object v2, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    if-ne v1, v2, :cond_0

    .line 243
    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->EVENTS_DISABLE_EVENT_BUFFERING:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->getRawValue()I

    move-result v1

    invoke-static {v1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 246
    invoke-static {v6, v7, v0, v3, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 249
    invoke-static {v6, v7, v0, v3, v5}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 259
    :goto_0
    return-object v0

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->getRawValue()I

    move-result v1

    invoke-static {v1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 253
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushSize:I

    int-to-long v1, v1

    invoke-static {v1, v2, v0, v3, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 255
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    int-to-long v1, v1

    invoke-static {v1, v2, v0, v3, v5}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    goto :goto_0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 265
    sget-object v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0xffff

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 272
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    sget-object v1, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;->NONE:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    iget-object v2, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushSize:I

    if-nez v1, :cond_1

    .line 275
    :cond_0
    const-string v1, "Disable Event Buffering"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 277
    :cond_1
    const-string v1, "Buffered events="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferEvents:Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage$BufferEvents;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string v1, "Buffer flush size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushSize:I

    if-ne v3, v1, :cond_2

    .line 281
    const-string v1, "Max"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :goto_1
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string v1, "Buffer flush time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    if-nez v1, :cond_3

    .line 289
    const-string v1, "[Disable timer]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 283
    :cond_2
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 291
    :cond_3
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    if-ne v3, v1, :cond_4

    .line 292
    const-string v1, "Max"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 294
    :cond_4
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;->mBufferFlushTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
