.class public final Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "AddChannelIdToListMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mChannelId:Lcom/dsi/ant/message/ChannelId;

.field private final mListIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADD_CHANNEL_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method private constructor <init>(Lcom/dsi/ant/message/ChannelId;I)V
    .locals 2
    .param p1, "channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p2, "listIndex"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 52
    if-ltz p2, :cond_0

    const/16 v0, 0x7c

    if-le p2, v0, :cond_1

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "List Index out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    iput-object p1, p0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    .line 58
    iput p2, p0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->mListIndex:I

    .line 59
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v2, 0x1

    .line 62
    new-instance v0, Lcom/dsi/ant/message/ChannelId;

    invoke-direct {v0, p1, v2}, Lcom/dsi/ant/message/ChannelId;-><init>([BI)V

    const/4 v1, 0x5

    invoke-static {p1, v1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;-><init>(Lcom/dsi/ant/message/ChannelId;I)V

    .line 65
    return-void
.end method


# virtual methods
.method public final getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    const/4 v1, 0x6

    new-array v0, v1, [B

    .line 89
    .local v0, "content":[B
    invoke-static {p1, v0, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 90
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelId;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    iget v1, p0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->mListIndex:I

    int-to-long v1, v1

    const/4 v3, 0x5

    invoke-static {v1, v2, v0, v4, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 95
    return-object v0
.end method

.method public final getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, "List Index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;->mListIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
