.class public final Lcom/dsi/ant/message/ChannelId;
.super Ljava/lang/Object;
.source "ChannelId.java"


# instance fields
.field private final mDeviceNumber:I

.field private final mDeviceType:I

.field private final mPair:Z

.field private final mTransmissionType:I


# direct methods
.method public constructor <init>([BI)V
    .locals 2
    .param p1, "messageContent"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    add-int/lit8 v0, p2, 0x0

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    .line 106
    add-int/lit8 v0, p2, 0x2

    aget-byte v1, p1, v0

    and-int/lit8 v1, v1, 0x7f

    shr-int/lit8 v1, v1, 0x0

    iput v1, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    .line 109
    const/16 v1, 0x80

    invoke-static {v1, p1, v0}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    .line 112
    add-int/lit8 v0, p2, 0x3

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    .line 114
    return-void
.end method


# virtual methods
.method public final getMessageContent()[B
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 226
    const/4 v2, 0x4

    new-array v0, v2, [B

    .line 228
    .local v0, "content":[B
    iget v4, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    iget-boolean v2, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x80

    :goto_0
    add-int v1, v4, v2

    .line 230
    .local v1, "deviceTypeWithPiaringBit":I
    iget v2, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    int-to-long v4, v2

    invoke-static {v4, v5, v0, v7, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 232
    int-to-long v2, v1

    invoke-static {v2, v3, v0, v6, v7}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 234
    iget v2, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    int-to-long v2, v2

    const/4 v4, 0x3

    invoke-static {v2, v3, v0, v6, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 237
    return-object v0

    .end local v1    # "deviceTypeWithPiaringBit":I
    :cond_0
    move v2, v3

    .line 228
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Channel ID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 245
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, " Device number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    iget v1, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 247
    const-string v1, ", Pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    iget-boolean v1, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 249
    const-string v1, ", Device Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    iget v1, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 251
    const-string v1, ", Transmission Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    iget v1, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 254
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
