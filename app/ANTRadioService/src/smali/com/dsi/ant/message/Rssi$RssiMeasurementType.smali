.class public final enum Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
.super Ljava/lang/Enum;
.source "Rssi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/message/Rssi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RssiMeasurementType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/Rssi$RssiMeasurementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

.field public static final enum DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

.field public static final enum UNKNOWN:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;


# instance fields
.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    const-string v1, "DBM"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v3, v2}, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    .line 27
    new-instance v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    const-string v1, "UNKNOWN"

    const/high16 v2, -0x80000000

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->UNKNOWN:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    .line 21
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    sget-object v1, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->UNKNOWN:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->$VALUES:[Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->mType:I

    return-void
.end method

.method public static convertToRssiMeasurementType(I)Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 50
    sget-object v1, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    iget v1, v1, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->mType:I

    if-ne p0, v1, :cond_0

    .line 53
    sget-object v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    .line 58
    .local v0, "measurementType":Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    :goto_0
    return-object v0

    .line 55
    .end local v0    # "measurementType":Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    :cond_0
    sget-object v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->UNKNOWN:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    .restart local v0    # "measurementType":Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->$VALUES:[Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    return-object v0
.end method
