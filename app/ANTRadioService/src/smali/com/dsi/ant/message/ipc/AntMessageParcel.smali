.class public Lcom/dsi/ant/message/ipc/AntMessageParcel;
.super Lcom/dsi/ant/message/AntMessage;
.source "AntMessageParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/message/ipc/AntMessageParcel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mMessageContent:[B

.field protected mMessageId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcom/dsi/ant/message/ipc/AntMessageParcel$1;

    invoke-direct {v0}, Lcom/dsi/ant/message/ipc/AntMessageParcel$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    .line 62
    iput v1, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageId:I

    .line 63
    const/4 v0, 0x1

    new-array v0, v0, [B

    aput-byte v1, v0, v1

    iput-object v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    .line 64
    return-void
.end method

.method private constructor <init>(I[B)V
    .locals 0
    .param p1, "messageId"    # I
    .param p2, "messageContent"    # [B

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    .line 74
    iput p1, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageId:I

    .line 75
    iput-object p2, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    .line 97
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageId:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    iget-object v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 98
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/AntMessage;)V
    .locals 2
    .param p1, "antMessage"    # Lcom/dsi/ant/message/AntMessage;

    .prologue
    .line 85
    invoke-virtual {p1}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v0

    invoke-virtual {p1}, Lcom/dsi/ant/message/AntMessage;->getMessageContent()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(I[B)V

    .line 86
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public final getMessageContent()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    return-object v0
.end method

.method public final getMessageId()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageId:I

    return v0
.end method

.method public final setMessageContent([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 121
    iput-object p1, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    return-void
.end method

.method public final setMessageId(I)V
    .locals 0
    .param p1, "messageId"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageId:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 136
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget-object v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    iget-object v0, p0, Lcom/dsi/ant/message/ipc/AntMessageParcel;->mMessageContent:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 141
    return-void
.end method
