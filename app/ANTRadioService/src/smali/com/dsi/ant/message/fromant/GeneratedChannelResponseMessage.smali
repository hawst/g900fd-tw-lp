.class public final Lcom/dsi/ant/message/fromant/GeneratedChannelResponseMessage;
.super Lcom/dsi/ant/message/fromant/ChannelEventMessage;
.source "GeneratedChannelResponseMessage.java"


# direct methods
.method public constructor <init>(IILcom/dsi/ant/message/ResponseCode;)V
    .locals 4
    .param p1, "channelNumber"    # I
    .param p2, "initiatingMessageID"    # I
    .param p3, "responseCode"    # Lcom/dsi/ant/message/ResponseCode;

    .prologue
    const/4 v3, 0x3

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {v3, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    const/16 v1, 0x40

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    int-to-byte v1, p2

    invoke-static {v1, v0, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    invoke-virtual {p3}, Lcom/dsi/ant/message/ResponseCode;->getRawValue()I

    move-result v1

    const/4 v2, 0x4

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>([B)V

    .line 38
    return-void
.end method
