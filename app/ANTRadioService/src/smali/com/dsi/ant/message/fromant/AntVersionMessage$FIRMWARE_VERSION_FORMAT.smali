.class final enum Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;
.super Ljava/lang/Enum;
.source "AntVersionMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/message/fromant/AntVersionMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "FIRMWARE_VERSION_FORMAT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

.field public static final enum BAD_FORMAT:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

.field public static final enum VERSION:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

.field public static final enum VERSION_MODULE:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    const-string v1, "BAD_FORMAT"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->BAD_FORMAT:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    .line 39
    new-instance v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    const-string v1, "VERSION"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    .line 40
    new-instance v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    const-string v1, "VERSION_MODULE"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION_MODULE:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    sget-object v1, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->BAD_FORMAT:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION_MODULE:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->$VALUES:[Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->$VALUES:[Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    return-object v0
.end method
