.class public abstract Lcom/dsi/ant/message/AntMessage;
.super Ljava/lang/Object;
.source "AntMessage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 64
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/dsi/ant/message/AntMessage;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/dsi/ant/message/AntMessage;

    .line 67
    .local v0, "antMessage":Lcom/dsi/ant/message/AntMessage;
    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v1

    invoke-virtual {v0}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageContent()[B

    move-result-object v1

    invoke-virtual {v0}, Lcom/dsi/ant/message/AntMessage;->getMessageContent()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const/4 v1, 0x1

    .line 72
    .end local v0    # "antMessage":Lcom/dsi/ant/message/AntMessage;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract getMessageContent()[B
.end method

.method public final getMessageContentString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageContent()[B

    move-result-object v0

    invoke-static {v0}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getMessageId()I
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v1

    add-int/lit16 v0, v1, 0xd9

    .line 82
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageContent()[B

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    .line 84
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
