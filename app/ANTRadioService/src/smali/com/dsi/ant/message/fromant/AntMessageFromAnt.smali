.class public abstract Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.super Lcom/dsi/ant/message/AntMessage;
.source "AntMessageFromAnt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromant/AntMessageFromAnt$1;
    }
.end annotation


# instance fields
.field protected mMessageContent:[B


# direct methods
.method protected constructor <init>([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    iput-object p1, p0, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->mMessageContent:[B

    return-void
.end method

.method public static createAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;[B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .locals 3
    .param p0, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p1, "messageContent"    # [B

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 121
    .local v0, "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    sget-object v1, Lcom/dsi/ant/message/fromant/AntMessageFromAnt$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 151
    :goto_0
    return-object v0

    .line 123
    :pswitch_0
    new-instance v0, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;-><init>([B)V

    .line 124
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 126
    :pswitch_1
    new-instance v0, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;-><init>([B)V

    .line 127
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 129
    :pswitch_2
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>([B)V

    .line 130
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 132
    :pswitch_3
    new-instance v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>([B)V

    .line 133
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 135
    :pswitch_4
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;-><init>([B)V

    .line 136
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 138
    :pswitch_5
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>([B)V

    .line 139
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 141
    :pswitch_6
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;-><init>([B)V

    .line 142
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 144
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;-><init>([B)V

    .line 145
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 147
    :pswitch_8
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;-><init>([B)V

    .line 148
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 150
    :pswitch_9
    new-instance v0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/SerialNumberMessage;-><init>([B)V

    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .locals 10
    .param p0, "rawMessage"    # [B

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 83
    invoke-static {p0, v7, v8}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v4

    long-to-int v3, v4

    .line 85
    .local v3, "messageLength":I
    array-length v4, p0

    add-int/lit8 v4, v4, -0x2

    if-eq v4, v3, :cond_0

    .line 86
    const-string v4, "Received message of length %d, expected %d. Contents: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    array-length v6, p0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    add-int/lit8 v6, v3, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {p0}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "info":Ljava/lang/String;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 92
    .end local v0    # "info":Ljava/lang/String;
    :cond_0
    invoke-static {p0, v8}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v2

    .line 93
    .local v2, "messageId":I
    new-array v1, v3, [B

    .line 94
    .local v1, "messageContent":[B
    invoke-static {p0, v9, v1, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    invoke-static {v2, v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(I[B)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v4

    .line 98
    invoke-static {v4, v1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;[B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public final getMessageContent()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->mMessageContent:[B

    return-object v0
.end method

.method public final getMessageId()I
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->getMessageId()I

    move-result v0

    return v0
.end method

.method public abstract getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final toStringHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/AntMessage;->getMessageId()I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
