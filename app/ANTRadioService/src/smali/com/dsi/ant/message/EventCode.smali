.class public final enum Lcom/dsi/ant/message/EventCode;
.super Ljava/lang/Enum;
.source "EventCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/EventCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/EventCode;

.field public static final enum CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

.field public static final enum CHANNEL_COLLISION:Lcom/dsi/ant/message/EventCode;

.field public static final enum RX_FAIL:Lcom/dsi/ant/message/EventCode;

.field public static final enum RX_FAIL_GO_TO_SEARCH:Lcom/dsi/ant/message/EventCode;

.field public static final enum RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

.field public static final enum TRANSFER_RX_FAILED:Lcom/dsi/ant/message/EventCode;

.field public static final enum TRANSFER_TX_COMPLETED:Lcom/dsi/ant/message/EventCode;

.field public static final enum TRANSFER_TX_FAILED:Lcom/dsi/ant/message/EventCode;

.field public static final enum TRANSFER_TX_START:Lcom/dsi/ant/message/EventCode;

.field public static final enum TX:Lcom/dsi/ant/message/EventCode;

.field public static final enum UNKNOWN:Lcom/dsi/ant/message/EventCode;

.field private static final sValues:[Lcom/dsi/ant/message/EventCode;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 22
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "RX_SEARCH_TIMEOUT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    .line 25
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "RX_FAIL"

    invoke-direct {v0, v1, v4, v5}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->RX_FAIL:Lcom/dsi/ant/message/EventCode;

    .line 28
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "TX"

    invoke-direct {v0, v1, v5, v6}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->TX:Lcom/dsi/ant/message/EventCode;

    .line 30
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "TRANSFER_RX_FAILED"

    invoke-direct {v0, v1, v6, v7}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->TRANSFER_RX_FAILED:Lcom/dsi/ant/message/EventCode;

    .line 33
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "TRANSFER_TX_COMPLETED"

    invoke-direct {v0, v1, v7, v8}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_COMPLETED:Lcom/dsi/ant/message/EventCode;

    .line 39
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "TRANSFER_TX_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_FAILED:Lcom/dsi/ant/message/EventCode;

    .line 42
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "CHANNEL_CLOSED"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    .line 45
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "RX_FAIL_GO_TO_SEARCH"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->RX_FAIL_GO_TO_SEARCH:Lcom/dsi/ant/message/EventCode;

    .line 51
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "CHANNEL_COLLISION"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->CHANNEL_COLLISION:Lcom/dsi/ant/message/EventCode;

    .line 57
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "TRANSFER_TX_START"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_START:Lcom/dsi/ant/message/EventCode;

    .line 61
    new-instance v0, Lcom/dsi/ant/message/EventCode;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xa

    const v3, 0xffff

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/EventCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/EventCode;->UNKNOWN:Lcom/dsi/ant/message/EventCode;

    .line 19
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/dsi/ant/message/EventCode;

    const/4 v1, 0x0

    sget-object v2, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/dsi/ant/message/EventCode;->RX_FAIL:Lcom/dsi/ant/message/EventCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TX:Lcom/dsi/ant/message/EventCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TRANSFER_RX_FAILED:Lcom/dsi/ant/message/EventCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_COMPLETED:Lcom/dsi/ant/message/EventCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_FAILED:Lcom/dsi/ant/message/EventCode;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/message/EventCode;->RX_FAIL_GO_TO_SEARCH:Lcom/dsi/ant/message/EventCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/message/EventCode;->CHANNEL_COLLISION:Lcom/dsi/ant/message/EventCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/message/EventCode;->TRANSFER_TX_START:Lcom/dsi/ant/message/EventCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/message/EventCode;->UNKNOWN:Lcom/dsi/ant/message/EventCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/message/EventCode;->$VALUES:[Lcom/dsi/ant/message/EventCode;

    .line 66
    invoke-static {}, Lcom/dsi/ant/message/EventCode;->values()[Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/EventCode;->sValues:[Lcom/dsi/ant/message/EventCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/EventCode;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/EventCode;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 90
    sget-object v0, Lcom/dsi/ant/message/EventCode;->UNKNOWN:Lcom/dsi/ant/message/EventCode;

    .line 92
    .local v0, "code":Lcom/dsi/ant/message/EventCode;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/EventCode;->sValues:[Lcom/dsi/ant/message/EventCode;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 93
    sget-object v2, Lcom/dsi/ant/message/EventCode;->sValues:[Lcom/dsi/ant/message/EventCode;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/message/EventCode;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 94
    sget-object v2, Lcom/dsi/ant/message/EventCode;->sValues:[Lcom/dsi/ant/message/EventCode;

    aget-object v0, v2, v1

    .line 99
    :cond_0
    return-object v0

    .line 93
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 92
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/EventCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/dsi/ant/message/EventCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/EventCode;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/EventCode;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/message/EventCode;->$VALUES:[Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/EventCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/EventCode;

    return-object v0
.end method


# virtual methods
.method public final getRawValue()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/dsi/ant/message/EventCode;->mRawValue:I

    return v0
.end method
