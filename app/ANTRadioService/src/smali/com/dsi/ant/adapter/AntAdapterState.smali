.class public final enum Lcom/dsi/ant/adapter/AntAdapterState;
.super Ljava/lang/Enum;
.source "AntAdapterState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/AntAdapterState$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/adapter/AntAdapterState;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/adapter/AntAdapterState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final enum DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final enum ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final enum ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final enum ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final enum INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

.field public static final enum INVALID:Lcom/dsi/ant/adapter/AntAdapterState;

.field private static final sValues:[Lcom/dsi/ant/adapter/AntAdapterState;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "INVALID"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->INVALID:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 29
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5, v4}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 32
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v6, v5}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 35
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "DISABLING"

    invoke-direct {v0, v1, v7, v6}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 38
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "ENABLING"

    invoke-direct {v0, v1, v8, v7}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 41
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "INITIALIZING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 44
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState;

    const-string v1, "ENABLED"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/adapter/AntAdapterState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 19
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->INVALID:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->$VALUES:[Lcom/dsi/ant/adapter/AntAdapterState;

    .line 49
    invoke-static {}, Lcom/dsi/ant/adapter/AntAdapterState;->values()[Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->sValues:[Lcom/dsi/ant/adapter/AntAdapterState;

    .line 121
    new-instance v0, Lcom/dsi/ant/adapter/AntAdapterState$1;

    invoke-direct {v0}, Lcom/dsi/ant/adapter/AntAdapterState$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/adapter/AntAdapterState;->mRawValue:I

    return-void
.end method

.method public static getAntAdapterState(I)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 58
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->INVALID:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 60
    .local v0, "code":Lcom/dsi/ant/adapter/AntAdapterState;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->sValues:[Lcom/dsi/ant/adapter/AntAdapterState;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 61
    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->sValues:[Lcom/dsi/ant/adapter/AntAdapterState;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/dsi/ant/adapter/AntAdapterState;->mRawValue:I

    if-ne p0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    .line 62
    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->sValues:[Lcom/dsi/ant/adapter/AntAdapterState;

    aget-object v0, v2, v1

    .line 67
    :cond_0
    return-object v0

    .line 61
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 60
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/adapter/AntAdapterState;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->$VALUES:[Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v0}, [Lcom/dsi/ant/adapter/AntAdapterState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/adapter/AntAdapterState;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState$2;->$SwitchMap$com$dsi$ant$adapter$AntAdapterState:[I

    iget v2, p0, Lcom/dsi/ant/adapter/AntAdapterState;->mRawValue:I

    invoke-static {v2}, Lcom/dsi/ant/adapter/AntAdapterState;->getAntAdapterState(I)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AntAdapterState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 103
    const-string v0, "UNKNOWN"

    .line 107
    .local v0, "result":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 84
    .end local v0    # "result":Ljava/lang/String;
    :pswitch_0
    const-string v0, "ERROR"

    .line 85
    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 87
    .end local v0    # "result":Ljava/lang/String;
    :pswitch_1
    const-string v0, "DISABLED"

    .line 88
    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 90
    .end local v0    # "result":Ljava/lang/String;
    :pswitch_2
    const-string v0, "DISABLING"

    .line 91
    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 93
    .end local v0    # "result":Ljava/lang/String;
    :pswitch_3
    const-string v0, "ENABLING"

    .line 94
    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 96
    .end local v0    # "result":Ljava/lang/String;
    :pswitch_4
    const-string v0, "INITIALIZING"

    .line 97
    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 99
    .end local v0    # "result":Ljava/lang/String;
    :pswitch_5
    const-string v0, "ENABLED"

    .line 100
    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 118
    iget v0, p0, Lcom/dsi/ant/adapter/AntAdapterState;->mRawValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    return-void
.end method
