.class final Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;
.super Ljava/lang/Object;
.source "AntDeviceInterfaceCapabilityVersions.java"


# static fields
.field public static final backgroundScanningExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final eventBufferingExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final extendedAssignExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final extendedRXTimeStampExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final frequencyAgilityExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final ignoreTransmissionTypeExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final rssiExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final timeFlushedEventBufferingExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final wildcardListIdExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-array v0, v5, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.15B00"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AUM9.99B99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->extendedRXTimeStampExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 36
    new-array v0, v4, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.09B"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->backgroundScanningExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 44
    new-array v0, v6, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.09B"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-19.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-89.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->extendedAssignExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 55
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2USB9.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AUM9.99B99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AJK1.99RAF"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-19.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-89.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->eventBufferingExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 68
    new-array v0, v4, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.06B00"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->timeFlushedEventBufferingExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 77
    new-array v0, v4, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.09B"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->ignoreTransmissionTypeExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 86
    new-array v0, v6, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.09B"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AUM9.99B99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "ATL1.11B00"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->frequencyAgilityExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 95
    new-array v0, v7, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AJK2.99RAF"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2USB9.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-19.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-89.99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->rssiExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 107
    new-array v0, v7, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AJK1.03RAF"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2USB1.04"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-11.06"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AP2-81.06"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->wildcardListIdExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    return-void
.end method
