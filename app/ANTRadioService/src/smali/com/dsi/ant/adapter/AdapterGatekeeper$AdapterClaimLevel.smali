.class final enum Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;
.super Ljava/lang/Enum;
.source "AdapterGatekeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/AdapterGatekeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AdapterClaimLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

.field public static final enum NONE:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

.field public static final enum STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

.field public static final enum WEAK:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->NONE:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    .line 38
    new-instance v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->WEAK:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    .line 42
    new-instance v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    sget-object v1, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->NONE:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->WEAK:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->$VALUES:[Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->$VALUES:[Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-virtual {v0}, [Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    return-object v0
.end method
