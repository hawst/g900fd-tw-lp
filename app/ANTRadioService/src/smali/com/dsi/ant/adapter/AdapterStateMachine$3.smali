.class final Lcom/dsi/ant/adapter/AdapterStateMachine$3;
.super Ljava/lang/Object;
.source "AdapterStateMachine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/adapter/AdapterStateMachine;->startDisable(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

.field final synthetic val$delayDisable:Z


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/AdapterStateMachine;Z)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    iput-boolean p2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->val$delayDisable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 881
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/Adapter;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v3, v4, :cond_3

    .line 882
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 883
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v3

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-static {}, Lcom/dsi/ant/AntMessageBuilder;->getANTResetSystem()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([B)[B

    move-result-object v2

    .line 885
    .local v2, "resetResult":[B
    if-eqz v2, :cond_0

    const/4 v3, 0x1

    aget-byte v3, v2, v3

    const/16 v4, 0x6f

    if-eq v3, v4, :cond_2

    .line 889
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v3

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v3}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v3

    if-eqz v3, :cond_1

    .line 890
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad reset response during disable: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    .line 893
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$702$7ec78ff6(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/util/concurrent/Future;

    .line 894
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 896
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mStopErrorRecovery:Z
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$800(Lcom/dsi/ant/adapter/AdapterStateMachine;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 900
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Giving up attempt recovery, disable failed and leaving adapter in error state."

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    .end local v2    # "resetResult":[B
    :cond_1
    :goto_0
    return-void

    .line 906
    .restart local v2    # "resetResult":[B
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 910
    .end local v2    # "resetResult":[B
    :cond_3
    iget-boolean v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->val$delayDisable:Z

    if-eqz v3, :cond_4

    .line 912
    const-wide/16 v3, 0x1388

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 918
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v3

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/PowerControl;->canDisable()Z

    move-result v0

    .line 920
    .local v0, "chipCanDisable":Z
    if-eqz v0, :cond_5

    .line 921
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v3

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v3}, Lcom/dsi/ant/chip/IAntChip;->disable()I

    move-result v1

    .line 923
    .local v1, "disableResult":I
    packed-switch v1, :pswitch_data_0

    .line 937
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v3, v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    .line 944
    .end local v1    # "disableResult":I
    :cond_5
    :goto_2
    :pswitch_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$702$7ec78ff6(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/util/concurrent/Future;

    .line 946
    if-nez v0, :cond_6

    .line 949
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$900(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 950
    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$900(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 951
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 954
    :cond_6
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 914
    .end local v0    # "chipCanDisable":Z
    :catch_0
    move-exception v3

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Disable delay unexpectedly interrupted."

    invoke-static {v3, v4}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 930
    .restart local v0    # "chipCanDisable":Z
    .restart local v1    # "disableResult":I
    :pswitch_1
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    const-string v4, "Chip ignored disable command."

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    goto :goto_2

    .line 934
    :pswitch_2
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$3;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->deferDisable()V

    goto :goto_2

    .line 951
    .end local v1    # "disableResult":I
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 923
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
