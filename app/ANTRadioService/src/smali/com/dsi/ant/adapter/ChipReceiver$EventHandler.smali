.class final Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;
.super Landroid/os/Handler;
.source "ChipReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/ChipReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventHandler"
.end annotation


# instance fields
.field private final mDestroy_LOCK:Ljava/lang/Object;

.field private mReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

.field private final values:[Lcom/dsi/ant/chip/ChipCallbackMessageIds;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/ChipReceiver;Landroid/os/Looper;)V
    .locals 1
    .param p1, "receiver"    # Lcom/dsi/ant/adapter/ChipReceiver;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 104
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 97
    invoke-static {}, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->values()[Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->values:[Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    .line 99
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mDestroy_LOCK:Ljava/lang/Object;

    .line 105
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    .line 106
    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    .line 109
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    .line 111
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 112
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 117
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    if-nez v0, :cond_0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :goto_0
    return-void

    .line 121
    :cond_0
    :try_start_1
    sget-object v0, Lcom/dsi/ant/adapter/ChipReceiver$1;->$SwitchMap$com$dsi$ant$chip$ChipCallbackMessageIds:[I

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->values:[Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    iget v3, p1, Landroid/os/Message;->what:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->ordinal()I

    move-result v2

    aget v0, v0, v2
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 138
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 123
    :pswitch_0
    :try_start_3
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v2}, Lcom/dsi/ant/adapter/ChipReceiver;->access$000(Lcom/dsi/ant/adapter/ChipReceiver;I)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->mReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "com.dsi.ant.service.hardwareinterface.chip.key.DATA_BYTE_ARRAY"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/adapter/ChipReceiver;->access$100(Lcom/dsi/ant/adapter/ChipReceiver;[B)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
