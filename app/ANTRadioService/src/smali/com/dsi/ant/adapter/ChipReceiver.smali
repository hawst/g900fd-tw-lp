.class final Lcom/dsi/ant/adapter/ChipReceiver;
.super Ljava/lang/Object;
.source "ChipReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/ChipReceiver$1;,
        Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;
    }
.end annotation


# static fields
.field private static final sEventThread:Landroid/os/HandlerThread;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private final mEventHandler:Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "adapter event thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 48
    sput-object v0, Lcom/dsi/ant/adapter/ChipReceiver;->sEventThread:Landroid/os/HandlerThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setDaemon(Z)V

    .line 49
    sget-object v0, Lcom/dsi/ant/adapter/ChipReceiver;->sEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 50
    const-class v0, Lcom/dsi/ant/adapter/ChipReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dsi/ant/adapter/ChipReceiver;->sEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " started."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;

    sget-object v1, Lcom/dsi/ant/adapter/ChipReceiver;->sEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;-><init>(Lcom/dsi/ant/adapter/ChipReceiver;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mEventHandler:Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/adapter/ChipReceiver;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->TAG:Ljava/lang/String;

    .line 71
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/ChipReceiver;I)V
    .locals 3
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChipReceiver;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received chip state change to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/dsi/ant/chip/AntChipState;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onChipStateUpdate(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/adapter/ChipReceiver;[B)V
    .locals 3
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChipReceiver;
    .param p1, "x1"    # [B

    .prologue
    .line 32
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ANTSerial"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - Rx         - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/MessageTrafficControl;->handleResponseMessage([B)V

    return-void
.end method


# virtual methods
.method final init()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    new-instance v1, Landroid/os/Messenger;

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mEventHandler:Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;

    invoke-direct {v1, v2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-interface {v0, v1}, Lcom/dsi/ant/chip/IAntChip;->setCallback(Landroid/os/Messenger;)V

    .line 80
    return-void
.end method

.method final shutdown()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/dsi/ant/chip/IAntChip;->setCallback(Landroid/os/Messenger;)V

    .line 87
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->mEventHandler:Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipReceiver$EventHandler;->destroy()V

    .line 89
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipReceiver;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 90
    return-void
.end method
