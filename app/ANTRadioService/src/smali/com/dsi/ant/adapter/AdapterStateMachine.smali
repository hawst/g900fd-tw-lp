.class final Lcom/dsi/ant/adapter/AdapterStateMachine;
.super Ljava/lang/Object;
.source "AdapterStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/AdapterStateMachine$4;,
        Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;
    }
.end annotation


# static fields
.field protected static final sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final MAX_RECOVERY_ATTEMPTS:I

.field private final RESET_RETRIES:I

.field private final TAG:Ljava/lang/String;

.field private final mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private final mAdapterStateChangeLock:Ljava/lang/Object;

.field private final mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

.field private mChipStateChange_LOCK:Ljava/lang/Object;

.field private mDisableDeferred:Z

.field private mDisableFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field protected mEnableDeferred:Z

.field private volatile mErrorString:Ljava/lang/String;

.field private mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

.field private mPreviousChipState:I

.field private mRecoveryAttempts:I

.field private final mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

.field private mState:Lcom/dsi/ant/adapter/AntAdapterState;

.field private mStateChangedSignal:Ljava/lang/Object;

.field private mStopErrorRecovery:Z

.field private volatile sEnableTimeStampMilliseconds:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/AdapterStateMachine;->sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    const/4 v1, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipStateChange_LOCK:Ljava/lang/Object;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 68
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableDeferred:Z

    .line 74
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mEnableDeferred:Z

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mErrorString:Ljava/lang/String;

    .line 125
    const/4 v0, 0x5

    iput v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->RESET_RETRIES:I

    .line 130
    const/4 v0, 0x2

    iput v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->MAX_RECOVERY_ATTEMPTS:I

    .line 135
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->sEnableTimeStampMilliseconds:J

    .line 189
    new-instance v0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterStateMachine$1;-><init>(Lcom/dsi/ant/adapter/AdapterStateMachine;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    .line 226
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 228
    new-instance v0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;-><init>(Lcom/dsi/ant/adapter/AdapterStateMachine;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    .line 229
    new-instance v0, Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    invoke-direct {v0, p1, v1}, Lcom/dsi/ant/adapter/ChipInitialiser;-><init>(Lcom/dsi/ant/adapter/Adapter;Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    .line 230
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/AdapterStateMachine;)Z
    .locals 7
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 36
    const/4 v0, 0x0

    move v3, v1

    :goto_0
    const/4 v4, 0x5

    if-gt v3, v4, :cond_3

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "<init> resetting, attempt: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-static {}, Lcom/dsi/ant/AntMessageBuilder;->getANTResetSystem()[B

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([BZ)[B

    move-result-object v0

    if-eqz v0, :cond_0

    aget-byte v4, v0, v1

    const/16 v5, 0x6f

    if-eq v4, v5, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bad response to reset: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " attempt: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/5"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    :goto_1
    if-nez v1, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad response to reset: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    :cond_2
    return v1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method static synthetic access$100(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/ChipInitialiser;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/adapter/AdapterStateMachine;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    return-object v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    return-object v0
.end method

.method static synthetic access$702$7ec78ff6(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/util/concurrent/Future;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/adapter/AdapterStateMachine;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStopErrorRecovery:Z

    return v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    return-object v0
.end method

.method private resetErrorRecoveryState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1027
    iput v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRecoveryAttempts:I

    .line 1028
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStopErrorRecovery:Z

    .line 1029
    return-void
.end method

.method private startDisable(Z)V
    .locals 4
    .param p1, "delayDisable"    # Z

    .prologue
    const/4 v0, 0x0

    .line 868
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableFuture:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    .line 957
    :goto_0
    return-void

    .line 870
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipStateChange_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 871
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableDeferred:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableDeferred:Z

    .line 873
    :cond_1
    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v2, v3, :cond_2

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v2, v3, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_3

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 957
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 875
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v2, "Starting disable process."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 877
    sget-object v0, Lcom/dsi/ant/adapter/AdapterStateMachine;->sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/dsi/ant/adapter/AdapterStateMachine$3;

    invoke-direct {v2, p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine$3;-><init>(Lcom/dsi/ant/adapter/AdapterStateMachine;Z)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableFuture:Ljava/util/concurrent/Future;

    .line 957
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private startEnable()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 758
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mEnableDeferred:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mEnableDeferred:Z

    .line 760
    :cond_0
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v2, :cond_1

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v1, v2, :cond_1

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v1, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_3

    .line 784
    :goto_0
    return-void

    .line 762
    :cond_3
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 763
    sget-object v0, Lcom/dsi/ant/adapter/AdapterStateMachine;->sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/dsi/ant/adapter/AdapterStateMachine$2;

    invoke-direct {v1, p0}, Lcom/dsi/ant/adapter/AdapterStateMachine$2;-><init>(Lcom/dsi/ant/adapter/AdapterStateMachine;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private undefinedAdapterState()V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "Adapter is in an undefined state"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    return-void
.end method


# virtual methods
.method final deferDisable()V
    .locals 3

    .prologue
    .line 965
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 966
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v2, :cond_0

    monitor-exit v1

    .line 969
    :goto_0
    return-void

    .line 968
    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->serviceDisable()V

    .line 969
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final deferEnable()V
    .locals 3

    .prologue
    .line 791
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 792
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v2, :cond_0

    monitor-exit v1

    .line 804
    :goto_0
    return-void

    .line 794
    :cond_0
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    packed-switch v0, :pswitch_data_0

    .line 804
    :goto_1
    :pswitch_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 797
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v2, "Enable deferred, calling startEnable"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startEnable()V

    goto :goto_1

    .line 802
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v2, "Deferring enable."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mEnableDeferred:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 794
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method final disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 3
    .param p1, "delayDisable"    # Z

    .prologue
    .line 819
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v1, :cond_0

    .line 820
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "Disable ignored when chip is already disabled"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 858
    :goto_0
    return-object v0

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Disable request in state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 826
    :try_start_0
    sget-object v0, Lcom/dsi/ant/adapter/AdapterStateMachine$4;->$SwitchMap$com$dsi$ant$adapter$AntAdapterState:[I

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AntAdapterState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 858
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 859
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 834
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 839
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->resetChannelStatus$1385ff()V

    .line 840
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startDisable(Z)V

    goto :goto_1

    .line 843
    :pswitch_3
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v0}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v0

    if-eqz v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->resetChannelStatus$1385ff()V

    .line 845
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startDisable(Z)V

    goto :goto_1

    .line 847
    :cond_1
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 849
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 850
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_1

    .line 854
    :pswitch_4
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedAdapterState()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 826
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method final disableErrorRecovery()V
    .locals 1

    .prologue
    .line 1022
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStopErrorRecovery:Z

    .line 1023
    const/4 v0, 0x2

    iput v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRecoveryAttempts:I

    .line 1024
    return-void
.end method

.method final enable()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 4

    .prologue
    .line 697
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v1, :cond_0

    .line 698
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "Enable ignored when chip is already enabled"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 741
    :goto_0
    return-object v0

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enable request in state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 704
    :try_start_0
    sget-object v0, Lcom/dsi/ant/adapter/AdapterStateMachine$4;->$SwitchMap$com$dsi$ant$adapter$AntAdapterState:[I

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AntAdapterState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 741
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 742
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 713
    :pswitch_1
    const/4 v0, 0x2

    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v2, v2, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v2}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 716
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 722
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->sEnableTimeStampMilliseconds:J

    .line 727
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/PowerControl;->canEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 730
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startEnable()V

    goto :goto_1

    .line 733
    :cond_2
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->setServiceInitialised()V

    goto :goto_1

    .line 737
    :pswitch_3
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedAdapterState()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 704
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method final getChipStateChangeLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipStateChange_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method final getLastEnableTimeStamp()J
    .locals 2

    .prologue
    .line 750
    iget-wide v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->sEnableTimeStampMilliseconds:J

    return-wide v0
.end method

.method final getState()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    return-object v0
.end method

.method final init()V
    .locals 4

    .prologue
    .line 237
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 241
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v0}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 242
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    packed-switch v0, :pswitch_data_0

    .line 258
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    .line 261
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Initialized with state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    monitor-exit v1

    return-void

    .line 246
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 249
    :pswitch_1
    :try_start_1
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_0

    .line 252
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 255
    :pswitch_3
    const-string v0, "Chip in error state when adapter created."

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method final onChipStateUpdate(I)V
    .locals 6
    .param p1, "newChipState"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x5

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 292
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v2

    .line 296
    if-ne v4, p1, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/PowerControl;->canEnable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 300
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/PowerControl;->canDisable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 303
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startDisable(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 475
    :try_start_1
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    :try_start_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 304
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 481
    :goto_0
    return-void

    .line 308
    :cond_0
    :try_start_4
    iget v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-ne v3, p1, :cond_1

    .line 309
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Chip sent repeated state update for state "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 475
    :try_start_5
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 478
    :try_start_6
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 311
    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 314
    :cond_1
    packed-switch p1, :pswitch_data_0

    :try_start_8
    invoke-virtual {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    :cond_2
    :goto_1
    if-nez v0, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid chip state change from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    invoke-static {v4}, Lcom/dsi/ant/chip/AntChipState;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/dsi/ant/chip/AntChipState;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_3
    if-nez v0, :cond_6

    .line 475
    :try_start_9
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 478
    :try_start_a
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 314
    :try_start_b
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    goto :goto_1

    :pswitch_1
    :try_start_c
    iget v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-ne v3, v5, :cond_2

    :cond_4
    move v0, v1

    goto :goto_1

    :pswitch_2
    iget v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    move v0, v1

    goto :goto_1

    :pswitch_3
    iget v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-ne v3, v5, :cond_2

    :cond_5
    move v0, v1

    goto :goto_1

    :pswitch_4
    move v0, v1

    goto :goto_1

    :pswitch_5
    move v0, v1

    goto :goto_1

    .line 316
    :cond_6
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-ne v0, v1, :cond_7

    if-nez p1, :cond_7

    .line 318
    const-string v0, "Enable failed to complete successfully."

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 475
    :try_start_d
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 478
    :try_start_e
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .line 319
    :try_start_f
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0

    .line 322
    :cond_7
    :try_start_10
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v1, :cond_8

    .line 323
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "ignoring chip state change while adapter in "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " state."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 475
    :try_start_11
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 478
    :try_start_12
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    .line 325
    :try_start_13
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_0

    .line 328
    :cond_8
    if-ne p1, v5, :cond_9

    .line 329
    :try_start_14
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Chip signalled error while in the "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    invoke-static {v1}, Lcom/dsi/ant/chip/AntChipState;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " state. Adapter was "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 475
    :try_start_15
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 478
    :try_start_16
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_8

    .line 331
    :try_start_17
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto/16 :goto_0

    .line 334
    :cond_9
    :try_start_18
    sget-object v0, Lcom/dsi/ant/adapter/AdapterStateMachine$4;->$SwitchMap$com$dsi$ant$adapter$AntAdapterState:[I

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AntAdapterState;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    packed-switch v0, :pswitch_data_1

    .line 475
    :cond_a
    :goto_2
    :pswitch_6
    :try_start_19
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 478
    :try_start_1a
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_9

    .line 481
    :try_start_1b
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    goto/16 :goto_0

    .line 336
    :pswitch_7
    packed-switch p1, :pswitch_data_2

    .line 359
    :try_start_1c
    invoke-virtual {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    goto :goto_2

    .line 475
    :catchall_1
    move-exception v0

    :try_start_1d
    iput p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 478
    :try_start_1e
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 479
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_2

    .line 475
    :try_start_1f
    throw v0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    .line 341
    :pswitch_8
    :try_start_20
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 342
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_2

    .line 345
    :pswitch_9
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_2

    .line 349
    :pswitch_a
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 351
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 353
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 356
    :pswitch_b
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_2

    .line 364
    :pswitch_c
    packed-switch p1, :pswitch_data_3

    .line 383
    invoke-virtual {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    goto :goto_2

    .line 373
    :pswitch_d
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 375
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 377
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 380
    :pswitch_e
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_2

    .line 388
    :pswitch_f
    packed-switch p1, :pswitch_data_4

    .line 407
    invoke-virtual {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    goto :goto_2

    .line 393
    :pswitch_10
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 394
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto :goto_2

    .line 401
    :pswitch_11
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableDeferred:Z

    if-eqz v0, :cond_a

    .line 402
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "Doing deferred disable."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startDisable(Z)V

    goto/16 :goto_2

    .line 413
    :pswitch_12
    packed-switch p1, :pswitch_data_5

    .line 439
    invoke-virtual {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    goto/16 :goto_2

    .line 418
    :pswitch_13
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 419
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 420
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 421
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto/16 :goto_2

    .line 424
    :pswitch_14
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 425
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->DISABLING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    goto/16 :goto_2

    .line 432
    :pswitch_15
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 433
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 434
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Chip state change to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/dsi/ant/chip/AntChipState;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " while adapter was "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 444
    :pswitch_16
    packed-switch p1, :pswitch_data_6

    .line 465
    invoke-virtual {p0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedChipState(I)V

    goto/16 :goto_2

    .line 446
    :pswitch_17
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mEnableDeferred:Z

    if-eqz v0, :cond_a

    .line 447
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "Doing deferred enable."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startEnable()V

    goto/16 :goto_2

    .line 458
    :pswitch_18
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V

    .line 459
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_2

    .line 462
    :pswitch_19
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Hard reset occured on chip while adapter was "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 470
    :pswitch_1a
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->undefinedAdapterState()V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    goto/16 :goto_2

    .line 479
    :catchall_2
    move-exception v0

    :try_start_21
    monitor-exit v1

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_5
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_6
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_7
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_8
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_9
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 334
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_12
        :pswitch_16
        :pswitch_1a
    .end packed-switch

    .line 336
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    .line 364
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_e
        :pswitch_d
        :pswitch_6
        :pswitch_6
    .end packed-switch

    .line 388
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_6
        :pswitch_11
        :pswitch_6
        :pswitch_10
    .end packed-switch

    .line 413
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_13
        :pswitch_15
        :pswitch_6
        :pswitch_14
        :pswitch_13
    .end packed-switch

    .line 444
    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_17
        :pswitch_6
        :pswitch_18
        :pswitch_6
        :pswitch_19
    .end packed-switch
.end method

.method final onError(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 557
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 559
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mErrorString:Ljava/lang/String;

    .line 560
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 561
    return-void
.end method

.method final reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 3

    .prologue
    .line 492
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reinitialize request in state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const/4 v0, 0x2

    iget v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 503
    :goto_0
    return-object v0

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 496
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v2, :cond_2

    .line 497
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->restartInit()V

    .line 503
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 498
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v2, :cond_1

    .line 499
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 500
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method final serviceDisable()V
    .locals 3

    .prologue
    .line 973
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 974
    :try_start_0
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    packed-switch v0, :pswitch_data_0

    .line 984
    :goto_0
    :pswitch_0
    monitor-exit v1

    return-void

    .line 977
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v2, "Disable deferred, calling startDisable"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->startDisable(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 984
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 982
    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v2, "Deferring disable."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableDeferred:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 974
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method final setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 4
    .param p1, "adapterState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 571
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 572
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, p1, :cond_1

    .line 573
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adapter state changed from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v2, :cond_0

    .line 576
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 579
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    .line 580
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/PowerControl;->onStateChanged(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 582
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 584
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, v1, :cond_2

    .line 590
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->setServiceInitialised()V

    .line 594
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRecoveryAttempts:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_3

    .line 595
    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRecoveryAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRecoveryAttempts:I

    .line 596
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error recovery attempt #"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRecoveryAttempts:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 616
    :cond_2
    :goto_0
    return-void

    .line 582
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 600
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1

    .line 601
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 602
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 604
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStopErrorRecovery:Z

    if-nez v0, :cond_4

    .line 608
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->disableErrorRecovery()V

    .line 609
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "Recovery attempt to enable failed, trying to disable adapter."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    goto :goto_0

    .line 602
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 612
    :cond_4
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "No recovery attempt is made. Error recovery is no longer enabled"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final shutdown()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 269
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableDeferred:Z

    .line 270
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mEnableDeferred:Z

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableFuture:Ljava/util/concurrent/Future;

    .line 273
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mRequestHandler:Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->destroy()V

    .line 275
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 276
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->cancelInit()V

    .line 277
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->shutdown()V

    .line 278
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 280
    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final undefinedChipState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 627
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Chip is in an undefined state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    return-void
.end method

.method final waitForChipDisableOrAdapterError()V
    .locals 2

    .prologue
    .line 1006
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mDisableFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mPreviousChipState:I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ERROR:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq v0, v1, :cond_0

    .line 1008
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    monitor-enter v1

    .line 1010
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mStateChangedSignal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1014
    :goto_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1016
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method final waitForInit()V
    .locals 1

    .prologue
    .line 994
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->waitForInit()V

    .line 995
    return-void
.end method
