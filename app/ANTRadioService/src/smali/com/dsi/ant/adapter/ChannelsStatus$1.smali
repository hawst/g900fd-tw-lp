.class final Lcom/dsi/ant/adapter/ChannelsStatus$1;
.super Landroid/content/BroadcastReceiver;
.source "ChannelsStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/ChannelsStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/adapter/ChannelsStatus;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/ChannelsStatus;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChannelsStatus$1;->this$0:Lcom/dsi/ant/adapter/ChannelsStatus;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 51
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "ANTAction":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 55
    .local v2, "bundle":Landroid/os/Bundle;
    if-nez v2, :cond_0

    .line 106
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v5, "OVERRIDE_SOURCE_PACKAGE"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "appName":Ljava/lang/String;
    const-string v5, "OVERRIDE_REQUEST_ID"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 65
    .local v3, "requestId":Ljava/lang/String;
    iget-object v5, p0, Lcom/dsi/ant/adapter/ChannelsStatus$1;->this$0:Lcom/dsi/ant/adapter/ChannelsStatus;

    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$000(Lcom/dsi/ant/adapter/ChannelsStatus;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 67
    .local v4, "requestIdsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "com.dsi.ant.statenotifier.action.OVERRIDE_ACTIVE_ENABLE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 68
    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$100()Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Override request "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " received"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 70
    if-nez v4, :cond_1

    .line 71
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "requestIdsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .restart local v4    # "requestIdsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/dsi/ant/adapter/ChannelsStatus$1;->this$0:Lcom/dsi/ant/adapter/ChannelsStatus;

    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$000(Lcom/dsi/ant/adapter/ChannelsStatus;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    :cond_1
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 76
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/dsi/ant/adapter/ChannelsStatus$1;->this$0:Lcom/dsi/ant/adapter/ChannelsStatus;

    # invokes: Lcom/dsi/ant/adapter/ChannelsStatus;->updateStateNotification()V
    invoke-static {v5}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$200(Lcom/dsi/ant/adapter/ChannelsStatus;)V

    goto :goto_0

    .line 79
    :cond_3
    const-string v5, "com.dsi.ant.statenotifier.action.OVERRIDE_ACTIVE_CANCEL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 80
    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$100()Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cancel override "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " received"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 82
    if-eqz v4, :cond_2

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 83
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 85
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 86
    iget-object v5, p0, Lcom/dsi/ant/adapter/ChannelsStatus$1;->this$0:Lcom/dsi/ant/adapter/ChannelsStatus;

    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$000(Lcom/dsi/ant/adapter/ChannelsStatus;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 90
    :cond_4
    const-string v5, "com.dsi.ant.statenotifier.action.OVERRIDE_ACTIVE_CANCEL_ALL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 91
    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$100()Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "All notification override from "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " are cancelled"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 93
    if-eqz v4, :cond_2

    .line 94
    iget-object v5, p0, Lcom/dsi/ant/adapter/ChannelsStatus$1;->this$0:Lcom/dsi/ant/adapter/ChannelsStatus;

    # getter for: Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/dsi/ant/adapter/ChannelsStatus;->access$000(Lcom/dsi/ant/adapter/ChannelsStatus;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
