.class final Lcom/dsi/ant/adapter/ChipInitialiser;
.super Ljava/lang/Object;
.source "ChipInitialiser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;,
        Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;
    }
.end annotation


# static fields
.field private static final sInitProcessChangeLock:Ljava/lang/Object;

.field private static final sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private mInitFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

.field public mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 255
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/ChipInitialiser;->sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 268
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dsi/ant/adapter/ChipInitialiser;->sInitProcessChangeLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/adapter/Adapter;Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;
    .param p2, "initReceiver"    # Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitFuture:Ljava/util/concurrent/Future;

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    .line 285
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 286
    iput-object p2, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    .line 287
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChipInitialiser;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChipInitialiser;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    return-object v0
.end method


# virtual methods
.method final cancelInit()V
    .locals 3

    .prologue
    .line 313
    sget-object v1, Lcom/dsi/ant/adapter/ChipInitialiser;->sInitProcessChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 314
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 319
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    const-string v2, "Cancelling init process."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->cancel()V

    .line 319
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final cleanup()V
    .locals 2

    .prologue
    .line 353
    sget-object v1, Lcom/dsi/ant/adapter/ChipInitialiser;->sInitProcessChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 354
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    .line 355
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final restartInit()V
    .locals 2

    .prologue
    .line 324
    sget-object v1, Lcom/dsi/ant/adapter/ChipInitialiser;->sInitProcessChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->restartInit()V

    .line 330
    :goto_0
    monitor-exit v1

    return-void

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/ChipInitialiser;->startInit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final shutdown()V
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    .line 363
    return-void
.end method

.method final startInit()V
    .locals 3

    .prologue
    .line 296
    sget-object v1, Lcom/dsi/ant/adapter/ChipInitialiser;->sInitProcessChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 297
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    const-string v2, "An init process is already in progress, not starting a new init process."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    monitor-exit v1

    .line 305
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    const-string v2, "Starting init process."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    new-instance v0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;-><init>(Lcom/dsi/ant/adapter/ChipInitialiser;B)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    .line 304
    sget-object v0, Lcom/dsi/ant/adapter/ChipInitialiser;->sLongOperationThreadPool:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitProcess:Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitFuture:Ljava/util/concurrent/Future;

    .line 305
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final waitForInit()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitFuture:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 337
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitFuture:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 345
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitFuture:Ljava/util/concurrent/Future;

    goto :goto_0

    .line 339
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    const-string v1, "waitForInit() was cancelled."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 341
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    const-string v1, "waitForInit() was interrupted."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 343
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;

    const-string v1, "Unknown error in the initprocess threadpool."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
