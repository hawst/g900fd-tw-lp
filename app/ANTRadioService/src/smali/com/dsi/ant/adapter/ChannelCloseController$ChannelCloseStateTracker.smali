.class final Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;
.super Ljava/lang/Object;
.source "ChannelCloseController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/ChannelCloseController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChannelCloseStateTracker"
.end annotation


# instance fields
.field volatile isClosing:Z

.field mChannelNumber:I

.field stateLock:Ljava/lang/Object;

.field volatile statusRequestsPending:I

.field final synthetic this$0:Lcom/dsi/ant/adapter/ChannelCloseController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/ChannelCloseController;I)V
    .locals 1
    .param p2, "channelNumber"    # I

    .prologue
    const/4 v0, 0x0

    .line 80
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    .line 74
    iput v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->stateLock:Ljava/lang/Object;

    .line 81
    iput p2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->mChannelNumber:I

    .line 82
    return-void
.end method

.method private sendRequestStatusMessage()Z
    .locals 4

    .prologue
    .line 113
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    # getter for: Lcom/dsi/ant/adapter/ChannelCloseController;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v0}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$000(Lcom/dsi/ant/adapter/ChannelCloseController;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v0

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    iget v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->mChannelNumber:I

    int-to-byte v2, v2

    const/16 v3, 0x52

    invoke-static {v2, v3}, Lcom/dsi/ant/AntMessageBuilder;->getANTRequestMessage(BB)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txData([B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    .line 119
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final isClosing()Z
    .locals 2

    .prologue
    .line 86
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 88
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final processChannelMessage([B)Z
    .locals 10
    .param p1, "message"    # [B

    .prologue
    const-wide/16 v8, 0x19

    const/16 v5, 0x52

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 141
    aget-byte v3, p1, v1

    if-eq v3, v5, :cond_0

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    if-nez v3, :cond_0

    .line 235
    :goto_0
    return v1

    .line 146
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->stateLock:Ljava/lang/Object;

    monitor-enter v3

    .line 152
    const/4 v4, 0x1

    :try_start_0
    aget-byte v4, p1, v4

    if-ne v4, v5, :cond_6

    .line 155
    iget v4, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    if-gtz v4, :cond_4

    .line 157
    .local v1, "forwardMessage":Z
    :goto_1
    iget v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    if-lez v2, :cond_1

    .line 158
    iget v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->statusRequestsPending:I

    .line 160
    :cond_1
    if-nez v1, :cond_2

    .line 162
    # getter for: Lcom/dsi/ant/adapter/ChannelCloseController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$200()Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Message "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " consumed, will not be forwarded to the app."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 165
    :cond_2
    iget-boolean v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    if-eqz v2, :cond_3

    .line 167
    const/4 v2, 0x3

    aget-byte v2, p1, v2

    and-int/lit8 v2, v2, 0x3

    .line 170
    sget-object v4, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v4}, Lcom/dsi/ant/message/ChannelState;->getRawValue()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 172
    iget-boolean v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    if-eqz v2, :cond_3

    .line 174
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    # getter for: Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J
    invoke-static {v2}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$100(Lcom/dsi/ant/adapter/ChannelCloseController;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-gtz v2, :cond_5

    .line 177
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    # setter for: Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J
    invoke-static {v2, v4, v5}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$102(Lcom/dsi/ant/adapter/ChannelCloseController;J)J

    .line 178
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    const-wide/16 v4, 0x19

    invoke-virtual {v2, p0, v4, v5}, Lcom/dsi/ant/adapter/ChannelCloseController;->scheduleQueryTask(Ljava/lang/Runnable;J)V

    .line 235
    :cond_3
    :goto_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 236
    .end local v1    # "forwardMessage":Z
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_4
    move v1, v2

    .line 155
    goto :goto_1

    .line 184
    .restart local v1    # "forwardMessage":Z
    :cond_5
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    # getter for: Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J
    invoke-static {v2}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$100(Lcom/dsi/ant/adapter/ChannelCloseController;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v2, v4, v8

    if-ltz v2, :cond_3

    .line 189
    new-instance v0, Lcom/dsi/ant/message/fromant/GeneratedChannelEventMessage;

    iget v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->mChannelNumber:I

    sget-object v4, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    invoke-direct {v0, v2, v4}, Lcom/dsi/ant/message/fromant/GeneratedChannelEventMessage;-><init>(ILcom/dsi/ant/message/EventCode;)V

    .line 191
    .local v0, "channelClosedEvent":Lcom/dsi/ant/message/fromant/GeneratedChannelEventMessage;
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    # getter for: Lcom/dsi/ant/adapter/ChannelCloseController;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v2}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$000(Lcom/dsi/ant/adapter/ChannelCloseController;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/GeneratedChannelEventMessage;->getMessageContent()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    .line 192
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    .line 194
    # getter for: Lcom/dsi/ant/adapter/ChannelCloseController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$200()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_2

    .line 203
    .end local v0    # "channelClosedEvent":Lcom/dsi/ant/message/fromant/GeneratedChannelEventMessage;
    .end local v1    # "forwardMessage":Z
    :cond_6
    iget-boolean v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    if-eqz v2, :cond_a

    .line 205
    const/4 v2, 0x1

    aget-byte v2, p1, v2

    const/16 v4, 0x40

    if-ne v2, v4, :cond_9

    .line 207
    const/4 v2, 0x3

    aget-byte v2, p1, v2

    if-eq v2, v1, :cond_7

    .line 209
    monitor-exit v3

    goto/16 :goto_0

    .line 212
    :cond_7
    const/4 v2, 0x4

    aget-byte v2, p1, v2

    const/4 v4, 0x7

    if-ne v2, v4, :cond_8

    .line 214
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    .line 215
    .restart local v1    # "forwardMessage":Z
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    goto :goto_2

    .line 219
    .end local v1    # "forwardMessage":Z
    :cond_8
    const/4 v1, 0x1

    .restart local v1    # "forwardMessage":Z
    goto :goto_2

    .line 225
    .end local v1    # "forwardMessage":Z
    :cond_9
    invoke-direct {p0}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->sendRequestStatusMessage()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    :cond_a
    const/4 v1, 0x1

    .restart local v1    # "forwardMessage":Z
    goto :goto_2
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 94
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 96
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    iget-object v0, v0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->sendRequestStatusMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    const-wide/16 v2, 0x172

    invoke-virtual {v0, p0, v2, v3}, Lcom/dsi/ant/adapter/ChannelCloseController;->scheduleQueryTask(Ljava/lang/Runnable;J)V

    .line 108
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final setIsClosing(Z)V
    .locals 4
    .param p1, "newValue"    # Z

    .prologue
    .line 127
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    iput-boolean p1, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    .line 131
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing:Z

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->this$0:Lcom/dsi/ant/adapter/ChannelCloseController;

    const-wide/16 v2, 0x0

    # setter for: Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J
    invoke-static {v0, v2, v3}, Lcom/dsi/ant/adapter/ChannelCloseController;->access$102(Lcom/dsi/ant/adapter/ChannelCloseController;J)J

    .line 136
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
