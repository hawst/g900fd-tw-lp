.class public Lcom/dsi/ant/adapter/AdapterProvider;
.super Ljava/lang/Object;
.source "AdapterProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;,
        Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;,
        Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field private hardwareListAccess_LOCK:Ljava/lang/Object;

.field private mAdapterInstanceCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

.field private mChipDetectorsStarted:Z

.field private final mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

.field private mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/dsi/ant/chip/IAntChipDetector;",
            ">;"
        }
    .end annotation
.end field

.field private final mHardwareList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/dsi/ant/chip/IAntChipDetector;",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/adapter/Adapter;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mPowerManagerStateChangeCallbackHandler:Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    const-class v0, Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->hardwareListAccess_LOCK:Ljava/lang/Object;

    .line 59
    iput-boolean v2, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipDetectorsStarted:Z

    .line 160
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    .line 208
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    .line 232
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mAdapterInstanceCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 235
    new-instance v0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;-><init>(Lcom/dsi/ant/adapter/AdapterProvider;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mPowerManagerStateChangeCallbackHandler:Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;

    .line 237
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 239
    invoke-direct {p0}, Lcom/dsi/ant/adapter/AdapterProvider;->startChipDetectors()V

    .line 242
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;

    invoke-direct {v1, p0, v2}, Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;-><init>(Lcom/dsi/ant/adapter/AdapterProvider;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 243
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/AdapterProvider;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipDetectorsStarted:Z

    return v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->hardwareListAccess_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/adapter/AdapterProvider;)Lcom/dsi/ant/chip/IAntChipDetector;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/adapter/AdapterProvider;)Lcom/dsi/ant/adapter/Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    return-object v0
.end method

.method private addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V
    .locals 3
    .param p1, "chipDetector"    # Lcom/dsi/ant/chip/IAntChipDetector;

    .prologue
    .line 386
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-eqz v0, :cond_0

    .line 388
    sget-object v0, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v1, "Multiple Built-in ANT Services, ignoring"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :goto_0
    return-void

    .line 392
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    .line 394
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private addExternalChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V
    .locals 2
    .param p1, "chipDetector"    # Lcom/dsi/ant/chip/IAntChipDetector;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    return-void
.end method

.method private static hasLibrary(Ljava/lang/String;)Z
    .locals 1
    .param p0, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 588
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private startChipDetectors()V
    .locals 14

    .prologue
    .line 414
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 416
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipDetectorsStarted:Z

    .line 418
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 419
    .local v5, "pm":Landroid/content/pm/PackageManager;
    new-instance v11, Landroid/content/Intent;

    const-string v12, "com.dsi.ant.intent.request.SERVICE_INFO"

    invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v12, 0x80

    invoke-virtual {v5, v11, v12}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v8

    .line 423
    .local v8, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " responses for com.dsi.ant.intent.request.SERVICE_INFO"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 425
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 429
    .local v7, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 430
    .local v9, "serviceInterfaceInfo":Landroid/content/pm/ServiceInfo;
    invoke-virtual {v7, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 432
    .local v10, "serviceLabel":Ljava/lang/CharSequence;
    if-eqz v9, :cond_e

    iget-object v11, v9, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-eqz v11, :cond_e

    .line 434
    iget-object v11, v9, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string v12, "ANT_AdapterType"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 437
    .local v0, "adapterType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 439
    const-string v11, "localsocket"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 441
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Local Socket ANT Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    new-instance v11, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    invoke-direct {v11}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;-><init>()V

    invoke-direct {p0, v11}, Lcom/dsi/ant/adapter/AdapterProvider;->addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto :goto_0

    .line 445
    :cond_1
    const-string v11, "built-in"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 447
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Built-in ANT Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    new-instance v11, Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-direct {v11}, Lcom/dsi/ant/chip/hal/AntAidlInterface;-><init>()V

    invoke-direct {p0, v11}, Lcom/dsi/ant/adapter/AdapterProvider;->addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto :goto_0

    .line 451
    :cond_2
    const-string v11, "tcpsocket"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 453
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "TCP Socket ANT Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    new-instance v1, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    invoke-direct {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;-><init>()V

    .line 457
    .local v1, "emulatorChipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    invoke-direct {p0, v1}, Lcom/dsi/ant/adapter/AdapterProvider;->addExternalChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto/16 :goto_0

    .line 459
    .end local v1    # "emulatorChipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    :cond_3
    const-string v11, "remote"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 461
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 464
    iget-object v11, v9, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string v12, "ANT_HardwareType"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 467
    .local v2, "hardwareType":Ljava/lang/String;
    const/4 v4, 0x0

    .line 469
    .local v4, "isBuiltIn":Z
    const-string v11, "built-in"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 472
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Remote ANT Chip Provider Service that claims to be for a built-in chip detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const/4 v4, 0x1

    .line 521
    :goto_1
    new-instance v6, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    iget-object v11, v9, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v12, v9, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v6, v11, v12}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    .local v6, "remoteChipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    if-eqz v4, :cond_c

    .line 525
    invoke-direct {p0, v6}, Lcom/dsi/ant/adapter/AdapterProvider;->addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto/16 :goto_0

    .line 478
    .end local v6    # "remoteChipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    :cond_4
    const-string v11, "network"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 481
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT Network Bridge Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 483
    :cond_5
    const-string v11, "usb"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 486
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT USB Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 488
    :cond_6
    const-string v11, "usb accessory"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 491
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT USB Accessory Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 493
    :cond_7
    const-string v11, "sdio"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 496
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT SDIO Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 498
    :cond_8
    const-string v11, "bluetooth"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 501
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT Bluetooth Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 503
    :cond_9
    const-string v11, "ble"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 506
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT BLE Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 508
    :cond_a
    const-string v11, "wifi"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 511
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ANT Wi-Fi Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 515
    :cond_b
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown ANT Chip Provider Service detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 527
    .restart local v6    # "remoteChipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    :cond_c
    invoke-direct {p0, v6}, Lcom/dsi/ant/adapter/AdapterProvider;->addExternalChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto/16 :goto_0

    .line 532
    .end local v2    # "hardwareType":Ljava/lang/String;
    .end local v4    # "isBuiltIn":Z
    .end local v6    # "remoteChipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    :cond_d
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown ANT Chip Provider Service IPC type detected: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 536
    .end local v0    # "adapterType":Ljava/lang/String;
    :cond_e
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Ignored service with bad meta-data: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 540
    .end local v7    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v9    # "serviceInterfaceInfo":Landroid/content/pm/ServiceInfo;
    .end local v10    # "serviceLabel":Ljava/lang/CharSequence;
    :cond_f
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v12, "Checking for service libraries."

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string v11, "com.dsi.ant.antradio_library"

    invoke-static {v11}, Lcom/dsi/ant/adapter/AdapterProvider;->hasLibrary(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 544
    const-string v11, "com.dsi.ant.isemulator_library"

    invoke-static {v11}, Lcom/dsi/ant/adapter/AdapterProvider;->hasLibrary(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 546
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v12, "TCP Socket ANT Service detected"

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    new-instance v11, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    invoke-direct {v11}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;-><init>()V

    invoke-direct {p0, v11}, Lcom/dsi/ant/adapter/AdapterProvider;->addExternalChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    .line 576
    :cond_10
    :goto_2
    iget-object v11, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-nez v11, :cond_11

    iget-object v11, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v11}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_11

    .line 577
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v11

    invoke-virtual {v11}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->setServiceInitialised()V

    .line 579
    :cond_11
    return-void

    .line 550
    :cond_12
    const-string v11, "com.sonyericsson.suquashi"

    invoke-static {v11}, Lcom/dsi/ant/adapter/AdapterProvider;->hasLibrary(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 552
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v12, "Local Socket ANT Service detected"

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    new-instance v11, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    invoke-direct {v11}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;-><init>()V

    invoke-direct {p0, v11}, Lcom/dsi/ant/adapter/AdapterProvider;->addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto :goto_2

    .line 556
    :cond_13
    const-string v11, "com.htc.sunny2"

    invoke-static {v11}, Lcom/dsi/ant/adapter/AdapterProvider;->hasLibrary(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 558
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v12, "BTIPS ANT Service detected"

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    new-instance v11, Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-direct {v11}, Lcom/dsi/ant/chip/hal/AntAidlInterface;-><init>()V

    invoke-direct {p0, v11}, Lcom/dsi/ant/adapter/AdapterProvider;->addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto :goto_2

    .line 563
    :cond_14
    iget-object v11, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-nez v11, :cond_10

    .line 566
    sget-object v11, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v12, "Built-in ANT Service detected"

    invoke-static {v11, v12}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    new-instance v11, Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-direct {v11}, Lcom/dsi/ant/chip/hal/AntAidlInterface;-><init>()V

    invoke-direct {p0, v11}, Lcom/dsi/ant/adapter/AdapterProvider;->addBuiltinChipDetector(Lcom/dsi/ant/chip/IAntChipDetector;)V

    goto :goto_2
.end method


# virtual methods
.method public final bindTransportSpecificIntent(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 680
    const/4 v0, 0x0

    .line 682
    .local v0, "binder":Landroid/os/IBinder;
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/chip/IAntChipDetector;

    .line 684
    invoke-interface {v2, p1}, Lcom/dsi/ant/chip/IAntChipDetector;->bindDetectorSpecificInterface(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    .line 685
    goto :goto_0

    .line 687
    :cond_0
    return-object v0
.end method

.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 284
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mPowerManagerStateChangeCallbackHandler:Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->destroy()V

    .line 286
    iput-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mAdapterInstanceCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 287
    iput-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    .line 288
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 290
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 291
    return-void
.end method

.method final getAdapterInstanceCounter()I
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mAdapterInstanceCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method public final getBuiltInChipPowerControl()Lcom/dsi/ant/adapter/BuiltInPowerControl;
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 325
    :cond_0
    const/4 v0, 0x0

    .line 328
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    check-cast v0, Lcom/dsi/ant/adapter/BuiltInPowerControl;

    goto :goto_0
.end method

.method public final getHandlesPriority(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterHandle;)I
    .locals 9
    .param p1, "lhs"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "rhs"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    const/4 v6, 0x1

    const/4 v7, -0x1

    .line 295
    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-eqz v5, :cond_1

    .line 296
    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v8, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/adapter/Adapter;

    .line 297
    .local v0, "chip":Lcom/dsi/ant/adapter/Adapter;
    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v6

    .line 315
    .end local v0    # "chip":Lcom/dsi/ant/adapter/Adapter;
    :goto_0
    return v5

    .line 299
    .restart local v0    # "chip":Lcom/dsi/ant/adapter/Adapter;
    :cond_0
    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/dsi/ant/adapter/AdapterHandle;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v7

    .line 300
    goto :goto_0

    .line 305
    .end local v0    # "chip":Lcom/dsi/ant/adapter/Adapter;
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/chip/IAntChipDetector;

    .line 306
    .local v1, "detector":Lcom/dsi/ant/chip/IAntChipDetector;
    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 307
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/adapter/Adapter;

    .line 308
    .local v2, "externalChip":Lcom/dsi/ant/adapter/Adapter;
    invoke-virtual {v2}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/dsi/ant/adapter/AdapterHandle;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v6

    .line 309
    goto :goto_0

    .line 310
    :cond_4
    invoke-virtual {v2}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/dsi/ant/adapter/AdapterHandle;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v7

    .line 311
    goto :goto_0

    .line 315
    .end local v1    # "detector":Lcom/dsi/ant/chip/IAntChipDetector;
    .end local v2    # "externalChip":Lcom/dsi/ant/adapter/Adapter;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-virtual {p1}, Lcom/dsi/ant/adapter/AdapterHandle;->hashCode()I

    move-result v5

    invoke-virtual {p2}, Lcom/dsi/ant/adapter/AdapterHandle;->hashCode()I

    move-result v6

    sub-int/2addr v5, v6

    goto :goto_0
.end method

.method public final hasBuiltInChipDetector()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final onStateChanged(Lcom/dsi/ant/adapter/Adapter;Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;
    .param p2, "newState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 350
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v0, p2, :cond_0

    .line 352
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->addAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V

    .line 358
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mPowerManagerStateChangeCallbackHandler:Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 360
    return-void

    .line 355
    :cond_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->removeAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V

    goto :goto_0
.end method

.method public final setDefaultAdapter(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 4
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    const/4 v0, 0x0

    .line 625
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    if-ne v1, p1, :cond_0

    .line 649
    :goto_0
    return-void

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    if-eqz v1, :cond_1

    .line 632
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/dsi/ant/adapter/PowerControl;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    .line 635
    :cond_1
    if-eqz p1, :cond_2

    .line 637
    sget-object v1, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Default Adapter changed to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 647
    :goto_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    if-nez v2, :cond_3

    :goto_2
    invoke-virtual {v1, v0}, Lcom/dsi/ant/legacy/AntManager;->setDefaultAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V

    goto :goto_0

    .line 640
    :cond_2
    sget-object v1, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v2, "Default Adapter changed to null"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    goto :goto_1

    .line 647
    :cond_3
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getWholeChipHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v0

    goto :goto_2
.end method

.method public final stopChipDetectors()V
    .locals 8

    .prologue
    .line 252
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->hardwareListAccess_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 253
    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipDetectorsStarted:Z

    .line 255
    sget-object v3, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 256
    sget-object v3, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v5, 0x4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v5, v6, v7}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v5, 0x4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v5, v6, v7}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    const-string v5, "Chip init process did not terminate."

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    :cond_0
    :goto_0
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {p0, v3}, Lcom/dsi/ant/adapter/AdapterProvider;->setDefaultAdapter(Lcom/dsi/ant/adapter/Adapter;)V

    .line 261
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/chip/IAntChipDetector;

    .line 265
    .local v0, "detector":Lcom/dsi/ant/chip/IAntChipDetector;
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/adapter/Adapter;

    .line 267
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/dsi/ant/adapter/Adapter;->shutdown(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 279
    .end local v0    # "detector":Lcom/dsi/ant/chip/IAntChipDetector;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 256
    :catch_0
    move-exception v3

    :try_start_3
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mChipInitExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 271
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-eqz v3, :cond_3

    .line 272
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v3}, Lcom/dsi/ant/chip/IAntChipDetector;->shutdown()V

    .line 275
    :cond_3
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/chip/IAntChipDetector;

    .line 277
    invoke-interface {v3}, Lcom/dsi/ant/chip/IAntChipDetector;->shutdown()V

    goto :goto_2

    .line 279
    :cond_4
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final triggerResetForBuiltInChip()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 691
    const/4 v0, 0x0

    .line 693
    .local v0, "resetTriggered":Z
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-nez v1, :cond_0

    .line 703
    :goto_0
    return v2

    .line 695
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider;->hardwareListAccess_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 697
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 698
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/Adapter;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v1

    .line 700
    sget-object v4, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v1, v4, :cond_2

    const/4 v0, 0x1

    .line 702
    :cond_1
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v0

    .line 703
    goto :goto_0

    :cond_2
    move v0, v2

    .line 700
    goto :goto_1

    .line 702
    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1
.end method

.method public final updateDefaultAdapter()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 653
    const/4 v1, 0x0

    .line 655
    .local v1, "chip":Lcom/dsi/ant/adapter/Adapter;
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    if-eqz v4, :cond_1

    .line 658
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 659
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "chip":Lcom/dsi/ant/adapter/Adapter;
    check-cast v1, Lcom/dsi/ant/adapter/Adapter;

    .line 675
    .restart local v1    # "chip":Lcom/dsi/ant/adapter/Adapter;
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/dsi/ant/adapter/AdapterProvider;->setDefaultAdapter(Lcom/dsi/ant/adapter/Adapter;)V

    .line 676
    return-void

    .line 664
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mExternalAntChipDetectors:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/chip/IAntChipDetector;

    .line 666
    .local v2, "chipDetector":Lcom/dsi/ant/chip/IAntChipDetector;
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/dsi/ant/adapter/Adapter;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 672
    :catch_0
    move-exception v4

    goto :goto_1
.end method
