.class public final Lcom/dsi/ant/adapter/AdapterGatekeeper;
.super Ljava/lang/Object;
.source "AdapterGatekeeper.java"

# interfaces
.implements Lcom/dsi/ant/adapter/Adapter$AdapterReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/AdapterGatekeeper$1;,
        Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

.field private final mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

.field private final mClaimedControl_LOCK:Ljava/lang/Object;

.field private final mPerChannelHandle:Lcom/dsi/ant/adapter/AdapterHandle;

.field private final mWholeChipHandle:Lcom/dsi/ant/adapter/AdapterHandle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->NONE:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    .line 65
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl_LOCK:Ljava/lang/Object;

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 98
    if-nez p1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null argument received."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 104
    new-instance v0, Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterHandle;-><init>(Lcom/dsi/ant/adapter/AdapterGatekeeper;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mWholeChipHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 105
    new-instance v0, Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterHandle;-><init>(Lcom/dsi/ant/adapter/AdapterGatekeeper;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mPerChannelHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 106
    return-void
.end method

.method private claimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)Z
    .locals 3
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "requestedClaimLevel"    # Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    .prologue
    .line 160
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is trying to claim "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 161
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is currently "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " claimed by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 166
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$1;->$SwitchMap$com$dsi$ant$adapter$AdapterGatekeeper$AdapterClaimLevel:[I

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 194
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-ne v0, p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    monitor-exit v1

    return v0

    .line 168
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "No other claims on "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " got claim."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 170
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->setClaimedByHandle(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 175
    :pswitch_1
    :try_start_1
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-ne v0, p1, :cond_0

    .line 177
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " weak claim on "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to strong claim."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 179
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->setClaimedByHandle(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)V

    goto :goto_0

    .line 182
    :cond_0
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " already has weak claim on "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    goto/16 :goto_0

    .line 186
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-ne v0, p1, :cond_1

    .line 187
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " already has strong claim on "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    goto/16 :goto_0

    .line 189
    :cond_1
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is currently strong claimed by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 166
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private isMessageAllowed(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    .locals 2
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 426
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 429
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    sget-object v1, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 432
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return v0

    .line 429
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 432
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method private setClaimedByHandle(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)V
    .locals 2
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "claimlevel"    # Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 269
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    .line 270
    iput-object p2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 273
    return-void

    .line 272
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 349
    if-ne p0, p1, :cond_1

    .line 369
    :cond_0
    :goto_0
    return v1

    .line 352
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 353
    goto :goto_0

    .line 355
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/adapter/AdapterGatekeeper;

    if-nez v3, :cond_3

    move v1, v2

    .line 356
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 359
    check-cast v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;

    .line 361
    .local v0, "other":Lcom/dsi/ant/adapter/AdapterGatekeeper;
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    if-eqz v3, :cond_4

    .line 362
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v4, v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/Adapter;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 363
    goto :goto_0

    .line 365
    :cond_4
    iget-object v3, v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    if-eqz v3, :cond_0

    move v1, v2

    .line 366
    goto :goto_0
.end method

.method public final forceClaimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    .locals 4
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 207
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is trying to force claim "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 209
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 211
    :try_start_0
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is currently "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " claimed by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 213
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eq v0, p1, :cond_0

    .line 214
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Releasing claim on "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 216
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterHandle;->getAdapterReceiver()Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    move-result-object v0

    invoke-interface {v0}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onClaimLost()V

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 221
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->waitForInit()V

    .line 223
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdapterHandle "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has force claimed control over "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->setClaimedByHandle(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)V

    .line 225
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    const/4 v0, 0x1

    return v0

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v0

    return-object v0
.end method

.method final getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;

    move-result-object v0

    return-object v0
.end method

.method public final getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mPerChannelHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    return-object v0
.end method

.method final getState()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method public final getWholeChipHandle()Lcom/dsi/ant/adapter/AdapterHandle;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mWholeChipHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    return-object v0
.end method

.method public final hasClaim(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    .locals 1
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0xd9

    .line 344
    return v0

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method final isAdapterClaimed()Z
    .locals 2

    .prologue
    .line 414
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 415
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 416
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onRxData([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 377
    .local v0, "receiver":Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 378
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapterClaimLevel:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    sget-object v3, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    if-ne v1, v3, :cond_0

    .line 380
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->getAdapterReceiver()Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    move-result-object v0

    .line 383
    :cond_0
    if-eqz v0, :cond_1

    .line 384
    invoke-interface {v0, p1}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onRxData([B)V

    .line 386
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 2
    .param p1, "newAdapterState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 395
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mWholeChipHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->getAdapterReceiver()Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    move-result-object v0

    .line 396
    .local v0, "receiver":Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;
    if-eqz v0, :cond_0

    .line 397
    invoke-interface {v0, p1}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 399
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mPerChannelHandle:Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterHandle;->getAdapterReceiver()Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    move-result-object v0

    .line 400
    if-eqz v0, :cond_1

    .line 401
    invoke-interface {v0, p1}, Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 403
    :cond_1
    return-void
.end method

.method final reinitialize(Lcom/dsi/ant/adapter/AdapterHandle;)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 2
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 312
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->isMessageAllowed(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->getState()Lcom/dsi/ant/adapter/AntAdapterState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    :goto_0
    return-object v0

    .line 314
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public final releaseClaim(Lcom/dsi/ant/adapter/AdapterHandle;)V
    .locals 4
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 237
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is trying to release claim on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 239
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 240
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimedControl:Lcom/dsi/ant/adapter/AdapterHandle;

    if-ne p1, v0, :cond_0

    .line 241
    const/4 v0, 0x0

    sget-object v2, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->NONE:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-direct {p0, v0, v2}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->setClaimedByHandle(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)V

    .line 242
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdapterHandle "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has released control of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final strongClaimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    .locals 1
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 148
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->STRONG:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->claimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Gatekeeper <"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final txBurst(Lcom/dsi/ant/adapter/AdapterHandle;I[B)Z
    .locals 2
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "channel"    # I
    .param p3, "data"    # [B

    .prologue
    .line 299
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 301
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->isMessageAllowed(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 303
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, p2, p3}, Lcom/dsi/ant/adapter/Adapter;->txBurst(I[B)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 305
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method final txCommand(Lcom/dsi/ant/adapter/AdapterHandle;[B)[B
    .locals 2
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "message"    # [B

    .prologue
    .line 277
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 279
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->isMessageAllowed(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 281
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/adapter/Adapter;->txCommand([B)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method final txData(Lcom/dsi/ant/adapter/AdapterHandle;[B)Z
    .locals 2
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;
    .param p2, "message"    # [B

    .prologue
    .line 288
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 290
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->isMessageAllowed(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 292
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/adapter/Adapter;->txData([B)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 294
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterGatekeeper;->mClaimAccess_LOCK:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public final weakClaimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)Z
    .locals 1
    .param p1, "handle"    # Lcom/dsi/ant/adapter/AdapterHandle;

    .prologue
    .line 139
    sget-object v0, Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;->WEAK:Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->claimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;Lcom/dsi/ant/adapter/AdapterGatekeeper$AdapterClaimLevel;)Z

    move-result v0

    return v0
.end method
