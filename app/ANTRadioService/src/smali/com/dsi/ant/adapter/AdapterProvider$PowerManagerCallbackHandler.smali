.class Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;
.super Landroid/os/Handler;
.source "AdapterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/AdapterProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PowerManagerCallbackHandler"
.end annotation


# instance fields
.field private HANDLER_TAG:Ljava/lang/String;

.field private mAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

.field private final mDestroy_LOCK:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/AdapterProvider;)V
    .locals 1
    .param p1, "adapterProvider"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 174
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 167
    const-class v0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->HANDLER_TAG:Ljava/lang/String;

    .line 169
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->mDestroy_LOCK:Ljava/lang/Object;

    .line 175
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->mAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    .line 176
    return-void
.end method


# virtual methods
.method final destroy()V
    .locals 2

    .prologue
    .line 180
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 181
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->mAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    .line 182
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 183
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 188
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->mAdapterProvider:Lcom/dsi/ant/adapter/AdapterProvider;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 199
    :goto_0
    return-void

    .line 191
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 196
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterProvider$PowerManagerCallbackHandler;->HANDLER_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Internal message with unknown id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 193
    :pswitch_0
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-static {v0}, Lcom/dsi/ant/power/PowerManager;->onStateChange(Lcom/dsi/ant/adapter/AntAdapterState;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
