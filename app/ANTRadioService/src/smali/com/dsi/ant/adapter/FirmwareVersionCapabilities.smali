.class public Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
.super Ljava/lang/Object;
.source "FirmwareVersionCapabilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$1;,
        Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBackgroundScanning:Z

.field private final mDelayAirplaneModeDisable:Z

.field private final mEventBuffering:Z

.field private final mExtendedAssign:Z

.field private final mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field private final mFrequencyAgility:Z

.field private final mHasBurstBufferBug:Z

.field private final mIgnoresTransmissionType:Z

.field private final mNeedsTxPowerRemap:Z

.field private final mRXMessageTimestamp:Z

.field private final mRequiresTxPowerInitialise:Z

.field private final mRssi:Z

.field private final mTimeFlushedEventBuffering:Z

.field private final mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

.field private final mUsesLongNetworkKeys:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final mWildcardIdList:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/fromant/AntVersionMessage;)V
    .locals 4
    .param p1, "firmwareVersionMessage"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mUsesLongNetworkKeys:Z

    .line 170
    iput-object p1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 172
    iget-object v2, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->UNKNOWN:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->getVersionString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AP2USB"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->AP2_USB:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    :cond_0
    :goto_0
    iput-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    .line 174
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    sget-object v2, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->WILINK_6_7:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    if-ne v0, v2, :cond_9

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRequiresTxPowerInitialise:Z

    .line 178
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    if-eqz v0, :cond_b

    .line 179
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v2, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->backgroundScanningExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v2}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    .line 182
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v2, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->ignoreTransmissionTypeExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v2}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mIgnoresTransmissionType:Z

    .line 185
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v2, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->eventBufferingExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v2}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    .line 188
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    if-eqz v0, :cond_a

    .line 189
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->timeFlushedEventBufferingExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    .line 195
    :goto_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->extendedRXTimeStampExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    .line 198
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->extendedAssignExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mExtendedAssign:Z

    .line 201
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->frequencyAgilityExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    .line 204
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->rssiExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    .line 207
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCapabilityVersions;->wildcardListIdExclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    .line 210
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->txPowerRemapInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstInclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    .line 213
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->burstBufferBugInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstInclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    .line 216
    iget-object v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFirmwareVersionMessage:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->delayAirplaneModeDisableInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-static {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionAgainstInclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    .line 233
    :goto_3
    return-void

    .line 172
    :cond_1
    const-string v3, "AUM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->BROADCOM:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_2
    const-string v3, "257"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->C7:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_3
    const-string v3, "AVX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->STE:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_4
    const-string v3, "AJK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->USB:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_5
    const-string v3, "AOV"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->WILINK_6_7:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_6
    const-string v3, "ATL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->WILINK_8:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_7
    const-string v3, "AP2-1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "AP2-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_8
    sget-object v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->AP2:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 174
    goto/16 :goto_1

    .line 192
    :cond_a
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    goto/16 :goto_2

    .line 220
    :cond_b
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    .line 221
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    .line 222
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    .line 223
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mIgnoresTransmissionType:Z

    .line 224
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    .line 225
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    .line 226
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mExtendedAssign:Z

    .line 227
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    .line 228
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    .line 229
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    .line 230
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    .line 231
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    goto :goto_3
.end method

.method private static checkVersionAgainstExclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z
    .locals 2
    .param p0, "firmwareVersion"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .param p1, "firmwareVersions"    # [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    const/4 v0, 0x1

    .line 525
    invoke-static {p0, p1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->listHasComparableElements(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v0

    .line 527
    :goto_0
    if-eqz v1, :cond_1

    .line 531
    :goto_1
    return v0

    .line 525
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 531
    :cond_1
    invoke-static {p0, p1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionIsGreaterOrEqual(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    goto :goto_1
.end method

.method private static checkVersionAgainstInclusionList$4d473d3e(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z
    .locals 1
    .param p0, "firmwareVersion"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .param p1, "firmwareVersions"    # [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    .line 548
    invoke-static {p0, p1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->listHasComparableElements(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    .line 550
    if-nez v0, :cond_0

    .line 551
    const/4 v0, 0x0

    .line 554
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->checkVersionIsLesserOrEqual(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v0

    goto :goto_0
.end method

.method private static checkVersionIsGreaterOrEqual(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z
    .locals 8
    .param p0, "firmwareVersion"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .param p1, "firmwareVersions"    # [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    const/4 v4, 0x0

    .line 587
    move-object v0, p1

    .local v0, "arr$":[Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :try_start_0
    array-length v2, p1

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 588
    .local v3, "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    invoke-virtual {p0, v3}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->isComparable(Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->compareTo(Lcom/dsi/ant/message/fromant/AntVersionMessage;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-ltz v5, :cond_1

    .line 589
    const/4 v4, 0x1

    .line 595
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :cond_0
    :goto_1
    return v4

    .line 587
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 594
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :catch_0
    move-exception v5

    sget-object v5, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Illegal argument "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " detected in firmware comparision."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static checkVersionIsLesserOrEqual(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z
    .locals 8
    .param p0, "firmwareVersion"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .param p1, "firmwareVersions"    # [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    const/4 v4, 0x0

    .line 611
    move-object v0, p1

    .local v0, "arr$":[Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :try_start_0
    array-length v2, p1

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 612
    .local v3, "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    invoke-virtual {p0, v3}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->isComparable(Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->compareTo(Lcom/dsi/ant/message/fromant/AntVersionMessage;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-gtz v5, :cond_1

    .line 613
    const/4 v4, 0x1

    .line 619
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :cond_0
    :goto_1
    return v4

    .line 611
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 618
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :catch_0
    move-exception v5

    sget-object v5, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Illegal argument "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " detected in firmware comparision."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static getValidPowerLevel(B[B)B
    .locals 3
    .param p0, "requestedPowerLevel"    # B
    .param p1, "powerLevels"    # [B

    .prologue
    .line 488
    array-length v0, p1

    .line 491
    .local v0, "length":I
    if-gez p0, :cond_0

    .line 492
    const/4 v2, 0x0

    aget-byte v1, p1, v2

    .line 499
    .local v1, "validPowerLevel":B
    :goto_0
    return v1

    .line 493
    .end local v1    # "validPowerLevel":B
    :cond_0
    if-lt p0, v0, :cond_1

    .line 494
    add-int/lit8 v2, v0, -0x1

    aget-byte v1, p1, v2

    .restart local v1    # "validPowerLevel":B
    goto :goto_0

    .line 496
    .end local v1    # "validPowerLevel":B
    :cond_1
    aget-byte v1, p1, p0

    .restart local v1    # "validPowerLevel":B
    goto :goto_0
.end method

.method private static listHasComparableElements(Lcom/dsi/ant/message/fromant/AntVersionMessage;[Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z
    .locals 5
    .param p0, "firmwareVersion"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .param p1, "firmwareVersions"    # [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    .line 567
    move-object v0, p1

    .local v0, "arr$":[Lcom/dsi/ant/message/fromant/AntVersionMessage;
    array-length v2, p1

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 568
    .local v3, "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    invoke-virtual {p0, v3}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->isComparable(Lcom/dsi/ant/message/fromant/AntVersionMessage;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 569
    const/4 v4, 0x1

    .line 572
    .end local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :goto_1
    return v4

    .line 567
    .restart local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 572
    .end local v3    # "version":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 363
    if-ne p0, p1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v1

    .line 366
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 367
    goto :goto_0

    .line 369
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    if-nez v3, :cond_3

    move v1, v2

    .line 370
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 373
    check-cast v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    .line 375
    .local v0, "other":Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    iget-object v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    iget-object v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    if-ne v3, v4, :cond_4

    invoke-virtual {p0}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hashCode()I

    move-result v3

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_5

    :cond_4
    move v1, v2

    .line 377
    goto :goto_0

    .line 380
    :cond_5
    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRequiresTxPowerInitialise:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRequiresTxPowerInitialise:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mIgnoresTransmissionType:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mIgnoresTransmissionType:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mExtendedAssign:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mExtendedAssign:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    if-ne v3, v4, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    iget-boolean v4, v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    if-eq v3, v4, :cond_0

    :cond_6
    move v1, v2

    .line 393
    goto :goto_0
.end method

.method public final getCorrectedTxPowerSetting(B)B
    .locals 4
    .param p1, "powerSetting"    # B

    .prologue
    .line 461
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    if-nez v1, :cond_0

    .line 479
    .end local p1    # "powerSetting":B
    :goto_0
    return p1

    .line 464
    .restart local p1    # "powerSetting":B
    :cond_0
    sget-object v1, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$1;->$SwitchMap$com$dsi$ant$adapter$FirmwareVersionCapabilities$ChipType:[I

    iget-object v2, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 474
    sget-object v1, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "There is no corresponding tx power settings to be corrected for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    move v0, p1

    .local v0, "mappedPowerLevel":B
    :goto_1
    move p1, v0

    .line 479
    goto :goto_0

    .line 466
    .end local v0    # "mappedPowerLevel":B
    :pswitch_0
    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->WILINK_TX_POWER_LEVEL_REMAP:[B

    invoke-static {p1, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->getValidPowerLevel(B[B)B

    move-result v0

    .line 468
    .restart local v0    # "mappedPowerLevel":B
    goto :goto_1

    .line 470
    .end local v0    # "mappedPowerLevel":B
    :pswitch_1
    sget-object v1, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->BROADCOM_TX_POWER_LEVEL_REMAP:[B

    invoke-static {p1, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->getValidPowerLevel(B[B)B

    move-result v0

    .line 472
    .restart local v0    # "mappedPowerLevel":B
    goto :goto_1

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final hasBackgroundScanning()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    return v0
.end method

.method public final hasBurstBufferBug()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    return v0
.end method

.method public final hasFrequencyAgility()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    return v0
.end method

.method public final hasRssi()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    return v0
.end method

.method public final hasRxMessageTimestamp()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    return v0
.end method

.method public final hasTimeFlushedEventBuffering()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    return v0
.end method

.method public final hasWildcardListId()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 341
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRequiresTxPowerInitialise:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0xd9

    .line 345
    .local v0, "result":I
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 346
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mIgnoresTransmissionType:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v4, v1

    .line 347
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v4, v1

    .line 348
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v4, v1

    .line 349
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    if-eqz v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v4, v1

    .line 350
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mExtendedAssign:Z

    if-eqz v1, :cond_6

    move v1, v2

    :goto_6
    add-int v0, v4, v1

    .line 351
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    if-eqz v1, :cond_7

    move v1, v2

    :goto_7
    add-int v0, v4, v1

    .line 352
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    if-eqz v1, :cond_8

    move v1, v2

    :goto_8
    add-int v0, v4, v1

    .line 353
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    if-eqz v1, :cond_9

    move v1, v2

    :goto_9
    add-int v0, v4, v1

    .line 354
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    if-eqz v1, :cond_a

    move v1, v2

    :goto_a
    add-int v0, v4, v1

    .line 355
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    if-eqz v1, :cond_b

    move v1, v2

    :goto_b
    add-int v0, v4, v1

    .line 356
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    if-eqz v4, :cond_c

    :goto_c
    add-int/2addr v1, v2

    .line 358
    return v1

    .end local v0    # "result":I
    :cond_0
    move v1, v3

    .line 341
    goto :goto_0

    .restart local v0    # "result":I
    :cond_1
    move v1, v3

    .line 345
    goto :goto_1

    :cond_2
    move v1, v3

    .line 346
    goto :goto_2

    :cond_3
    move v1, v3

    .line 347
    goto :goto_3

    :cond_4
    move v1, v3

    .line 348
    goto :goto_4

    :cond_5
    move v1, v3

    .line 349
    goto :goto_5

    :cond_6
    move v1, v3

    .line 350
    goto :goto_6

    :cond_7
    move v1, v3

    .line 351
    goto :goto_7

    :cond_8
    move v1, v3

    .line 352
    goto :goto_8

    :cond_9
    move v1, v3

    .line 353
    goto :goto_9

    :cond_a
    move v1, v3

    .line 354
    goto :goto_a

    :cond_b
    move v1, v3

    .line 355
    goto :goto_b

    :cond_c
    move v2, v3

    .line 356
    goto :goto_c
.end method

.method public final needsTxPowerRemap()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mNeedsTxPowerRemap:Z

    return v0
.end method

.method public final requiresDelayForAirplaneModeDisable()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    return v0
.end method

.method public final requiresTxPowerReset()Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRequiresTxPowerInitialise:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 400
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Firmware Version Capabilities ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mType:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities$ChipType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "):"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRequiresTxPowerInitialise:Z

    if-eqz v1, :cond_0

    .line 403
    const-string v1, " -Requires Tx Power Initialise"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    :cond_0
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mEventBuffering:Z

    if-eqz v1, :cond_1

    .line 407
    const-string v1, " -Event Buffering"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_1
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mTimeFlushedEventBuffering:Z

    if-eqz v1, :cond_2

    .line 411
    const-string v1, " -Time Flushed Event Buffering"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    :cond_2
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mBackgroundScanning:Z

    if-eqz v1, :cond_3

    .line 415
    const-string v1, " -Background Scanning"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    :cond_3
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mIgnoresTransmissionType:Z

    if-eqz v1, :cond_4

    .line 419
    const-string v1, " -Ignores Transmission Type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    :cond_4
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRXMessageTimestamp:Z

    if-eqz v1, :cond_5

    .line 423
    const-string v1, " -Rx Message Timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    :cond_5
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mExtendedAssign:Z

    if-eqz v1, :cond_6

    .line 427
    const-string v1, " -Extended Assign"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    :cond_6
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mFrequencyAgility:Z

    if-eqz v1, :cond_7

    .line 431
    const-string v1, " -Frequency Agility"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :cond_7
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mRssi:Z

    if-eqz v1, :cond_8

    .line 435
    const-string v1, " -RSSI"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    :cond_8
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mWildcardIdList:Z

    if-eqz v1, :cond_9

    .line 439
    const-string v1, " -Wildcard Support in ID List"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    :cond_9
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mHasBurstBufferBug:Z

    if-eqz v1, :cond_a

    .line 443
    const-string v1, " -Flushes burst buffer on ack result."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    :cond_a
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->mDelayAirplaneModeDisable:Z

    if-eqz v1, :cond_b

    .line 447
    const-string v1, " -Requires delay for Airplane mode disables"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
