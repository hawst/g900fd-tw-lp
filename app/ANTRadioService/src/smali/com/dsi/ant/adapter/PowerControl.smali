.class abstract Lcom/dsi/ant/adapter/PowerControl;
.super Ljava/lang/Object;
.source "PowerControl.java"


# instance fields
.field protected final mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private volatile mIsInitialized:Z


# direct methods
.method protected constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mIsInitialized:Z

    .line 33
    iput-object p1, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 34
    return-void
.end method


# virtual methods
.method abstract canDisable()Z
.end method

.method abstract canEnable()Z
.end method

.method disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 2
    .param p1, "delayDisable"    # Z

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/PowerControl;->canDisable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .line 53
    .local v0, "result":Lcom/dsi/ant/adapter/AntAdapterState;
    :goto_0
    return-object v0

    .line 50
    .end local v0    # "result":Lcom/dsi/ant/adapter/AntAdapterState;
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/Adapter;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .restart local v0    # "result":Lcom/dsi/ant/adapter/AntAdapterState;
    goto :goto_0
.end method

.method enable()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/PowerControl;->canEnable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .line 72
    .local v0, "result":Lcom/dsi/ant/adapter/AntAdapterState;
    :goto_0
    return-object v0

    .line 69
    .end local v0    # "result":Lcom/dsi/ant/adapter/AntAdapterState;
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/Adapter;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .restart local v0    # "result":Lcom/dsi/ant/adapter/AntAdapterState;
    goto :goto_0
.end method

.method init()V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mIsInitialized:Z

    .line 125
    return-void
.end method

.method protected final onStateChanged(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 2
    .param p1, "newState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/Adapter;->onStateChanged(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 117
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v0, v1, p1}, Lcom/dsi/ant/adapter/AdapterProvider;->onStateChanged(Lcom/dsi/ant/adapter/Adapter;Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 118
    return-void
.end method

.method saveChipState(Z)V
    .locals 0
    .param p1, "doEnable"    # Z

    .prologue
    .line 108
    return-void
.end method

.method shutdown(Z)V
    .locals 1
    .param p1, "doDisable"    # Z

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mIsInitialized:Z

    .line 90
    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->disableErrorRecovery()V

    .line 97
    iget-object v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->serviceDisable()V

    .line 99
    iget-object v0, p0, Lcom/dsi/ant/adapter/PowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->waitForChipDisableOrAdapterError()V

    .line 101
    :cond_0
    return-void
.end method
