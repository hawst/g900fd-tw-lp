.class final Lcom/dsi/ant/adapter/AdapterStateMachine$1;
.super Ljava/lang/Object;
.source "AdapterStateMachine.java"

# interfaces
.implements Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/AdapterStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/AdapterStateMachine;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final errorOccured(Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # invokes: Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V
    invoke-static {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$200(Lcom/dsi/ant/adapter/AdapterStateMachine;)V

    .line 197
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method public final onInitCompleted()V
    .locals 4

    .prologue
    .line 202
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$100(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/ChipInitialiser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/ChipInitialiser;->cleanup()V

    .line 204
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapterStateChangeLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$300(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 205
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$400(Lcom/dsi/ant/adapter/AdapterStateMachine;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "...Initialized"

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mState:Lcom/dsi/ant/adapter/AntAdapterState;
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$500(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v1

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne v1, v3, :cond_0

    .line 208
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    sget-object v3, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v1, v3}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 212
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # invokes: Lcom/dsi/ant/adapter/AdapterStateMachine;->resetErrorRecoveryState()V
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$200(Lcom/dsi/ant/adapter/AdapterStateMachine;)V

    .line 216
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v1

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v1}, Lcom/dsi/ant/adapter/MessageTrafficControl;->getResetType()B

    move-result v0

    .line 217
    .local v0, "resetType":B
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$1;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v1

    invoke-static {v0}, Lcom/dsi/ant/AntMessageBuilder;->getANTStartupMessage(B)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    .line 220
    .end local v0    # "resetType":B
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
