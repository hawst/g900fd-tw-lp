.class final Lcom/dsi/ant/adapter/BuiltInPowerControl$1;
.super Landroid/content/BroadcastReceiver;
.source "BuiltInPowerControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/BuiltInPowerControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/adapter/BuiltInPowerControl;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/BuiltInPowerControl;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl$1;->this$0:Lcom/dsi/ant/adapter/BuiltInPowerControl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 188
    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isAirplaneModeOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 190
    iget-object v2, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl$1;->this$0:Lcom/dsi/ant/adapter/BuiltInPowerControl;

    iget-object v2, v2, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    const-string v3, "Airplane mode is activated and ANT is in the sensitivity list, disabling ANT"

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v2, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl$1;->this$0:Lcom/dsi/ant/adapter/BuiltInPowerControl;

    iget-object v2, v2, Lcom/dsi/ant/adapter/BuiltInPowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/Adapter;->getFirmwareVersionCapabilities()Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    move-result-object v0

    .line 197
    .local v0, "firmwareCapabilities":Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->requiresDelayForAirplaneModeDisable()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 198
    .local v1, "shouldDelayDisable":Z
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl$1;->this$0:Lcom/dsi/ant/adapter/BuiltInPowerControl;

    invoke-virtual {v2, v1}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    .line 200
    .end local v0    # "firmwareCapabilities":Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    .end local v1    # "shouldDelayDisable":Z
    :cond_1
    return-void

    .line 197
    .restart local v0    # "firmwareCapabilities":Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
