.class public Lcom/dsi/ant/adapter/ChannelsStatus;
.super Ljava/lang/Object;
.source "ChannelsStatus.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mChannelStatus:[I

.field private mIsEnabled:Z

.field private mOverrideActiveApplications:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOverrideReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/adapter/ChannelsStatus;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1, "isEnabled"    # Z

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mIsEnabled:Z

    .line 45
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;

    .line 48
    new-instance v1, Lcom/dsi/ant/adapter/ChannelsStatus$1;

    invoke-direct {v1, p0}, Lcom/dsi/ant/adapter/ChannelsStatus$1;-><init>(Lcom/dsi/ant/adapter/ChannelsStatus;)V

    iput-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideReceiver:Landroid/content/BroadcastReceiver;

    .line 125
    iput-boolean p1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mIsEnabled:Z

    .line 127
    const/16 v1, 0x8

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    .line 129
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dsi/ant/adapter/ChannelsStatus;->resetChannelStatus(Z)V

    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 132
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.dsi.ant.statenotifier.action.OVERRIDE_ACTIVE_ENABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v1, "com.dsi.ant.statenotifier.action.OVERRIDE_ACTIVE_CANCEL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v1, "com.dsi.ant.statenotifier.action.OVERRIDE_ACTIVE_CANCEL_ALL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/ChannelsStatus;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChannelsStatus;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/adapter/ChannelsStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChannelsStatus;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/dsi/ant/adapter/ChannelsStatus;->updateStateNotification()V

    return-void
.end method

.method private static sendStateNotification(I)V
    .locals 3
    .param p0, "state"    # I

    .prologue
    .line 224
    sget-object v1, Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending ACTION_NOTIFICATION_STATE_CHANGED Intent.  State="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 226
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dsi.ant.action.NOTIFICATION_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "intentStatus":Landroid/content/Intent;
    const-string v1, "ANT_STATE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 228
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 230
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    return-void
.end method

.method private updateStateNotification()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 207
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mIsEnabled:Z

    if-nez v1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideActiveApplications:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 210
    invoke-static {v3}, Lcom/dsi/ant/adapter/ChannelsStatus;->sendStateNotification(I)V

    goto :goto_0

    :cond_1
    move v1, v0

    .line 215
    :goto_1
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    aget v2, v2, v0

    if-ne v2, v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-lez v1, :cond_4

    .line 216
    invoke-static {v3}, Lcom/dsi/ant/adapter/ChannelsStatus;->sendStateNotification(I)V

    goto :goto_0

    .line 218
    :cond_4
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/dsi/ant/adapter/ChannelsStatus;->sendStateNotification(I)V

    goto :goto_0
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    .line 188
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mOverrideReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 190
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mIsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/dsi/ant/adapter/ChannelsStatus;->sendStateNotification(I)V

    .line 191
    :cond_0
    return-void
.end method

.method public final resetChannelStatus(Z)V
    .locals 3
    .param p1, "notify"    # Z

    .prologue
    .line 175
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    const/4 v2, 0x2

    aput v2, v1, v0

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    if-eqz p1, :cond_1

    .line 182
    invoke-direct {p0}, Lcom/dsi/ant/adapter/ChannelsStatus;->updateStateNotification()V

    .line 184
    :cond_1
    return-void
.end method

.method public final setChannelActive(I)V
    .locals 3
    .param p1, "channel"    # I

    .prologue
    const/4 v1, 0x1

    .line 148
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    array-length v0, v0

    if-ge p1, v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    aput v1, v0, p1

    .line 152
    iget-boolean v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mIsEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/dsi/ant/adapter/ChannelsStatus;->sendStateNotification(I)V

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    sget-object v0, Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Active channel is outside range: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final setChannelInactive(I)V
    .locals 3
    .param p1, "channel"    # I

    .prologue
    .line 162
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelsStatus;->mChannelStatus:[I

    const/4 v1, 0x2

    aput v1, v0, p1

    .line 165
    invoke-direct {p0}, Lcom/dsi/ant/adapter/ChannelsStatus;->updateStateNotification()V

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_0
    sget-object v0, Lcom/dsi/ant/adapter/ChannelsStatus;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inactive channel is outside range: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
