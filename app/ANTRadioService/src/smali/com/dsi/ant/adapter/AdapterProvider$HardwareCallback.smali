.class final Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;
.super Ljava/lang/Object;
.source "AdapterProvider.java"

# interfaces
.implements Lcom/dsi/ant/chip/IAntChipDetectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/AdapterProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HardwareCallback"
.end annotation


# instance fields
.field private final mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

.field final synthetic this$0:Lcom/dsi/ant/adapter/AdapterProvider;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/adapter/AdapterProvider;Lcom/dsi/ant/chip/IAntChipDetector;)V
    .locals 0
    .param p2, "hardware"    # Lcom/dsi/ant/chip/IAntChipDetector;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    .line 68
    return-void
.end method


# virtual methods
.method public final adapterGone(Lcom/dsi/ant/chip/IAntChip;)V
    .locals 8
    .param p1, "chip"    # Lcom/dsi/ant/chip/IAntChip;

    .prologue
    .line 99
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mChipDetectorsStarted:Z
    invoke-static {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->access$000(Lcom/dsi/ant/adapter/AdapterProvider;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 137
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->hardwareListAccess_LOCK:Ljava/lang/Object;
    invoke-static {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->access$100(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 102
    :try_start_0
    sget-object v4, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Hardware "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " reports chip "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " as gone."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v3, 0x0

    .line 107
    .local v3, "match":Lcom/dsi/ant/adapter/Adapter;
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;
    invoke-static {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->access$300(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/util/Map;

    move-result-object v4

    iget-object v6, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/adapter/Adapter;

    .line 109
    .local v0, "adapter":Lcom/dsi/ant/adapter/Adapter;
    iget-object v4, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 111
    move-object v3, v0

    .line 115
    .end local v0    # "adapter":Lcom/dsi/ant/adapter/Adapter;
    :cond_2
    if-nez v3, :cond_3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 137
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "match":Lcom/dsi/ant/adapter/Adapter;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 117
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "match":Lcom/dsi/ant/adapter/Adapter;
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;
    invoke-static {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->access$300(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/util/Map;

    move-result-object v4

    iget-object v6, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 119
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAntManager()Lcom/dsi/ant/legacy/AntManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/legacy/AntManager;->getAdapter()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v1

    .line 122
    .local v1, "adapterHandle":Lcom/dsi/ant/adapter/AdapterHandle;
    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/Adapter;->getWholeChipHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v4

    if-ne v1, v4, :cond_4

    .line 125
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;
    invoke-static {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->access$200(Lcom/dsi/ant/adapter/AdapterProvider;)Lcom/dsi/ant/chip/IAntChipDetector;

    move-result-object v4

    if-nez v4, :cond_4

    .line 126
    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/dsi/ant/adapter/AdapterProvider;->setDefaultAdapter(Lcom/dsi/ant/adapter/Adapter;)V

    .line 131
    :cond_4
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/Adapter;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->removeAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)V

    .line 135
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/Adapter;->shutdown(Z)V

    .line 137
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final newAdapter(Lcom/dsi/ant/chip/IAntChip;)V
    .locals 6
    .param p1, "chip"    # Lcom/dsi/ant/chip/IAntChip;

    .prologue
    .line 73
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mChipDetectorsStarted:Z
    invoke-static {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->access$000(Lcom/dsi/ant/adapter/AdapterProvider;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->hardwareListAccess_LOCK:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->access$100(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 76
    :try_start_0
    sget-object v2, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Hardware "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has made chip "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " available."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mBuiltinAntChipDetector:Lcom/dsi/ant/chip/IAntChipDetector;
    invoke-static {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->access$200(Lcom/dsi/ant/adapter/AdapterProvider;)Lcom/dsi/ant/chip/IAntChipDetector;

    move-result-object v4

    if-ne v2, v4, :cond_2

    const/4 v1, 0x1

    .line 80
    .local v1, "isBuiltInAdapter":Z
    :goto_1
    new-instance v0, Lcom/dsi/ant/adapter/Adapter;

    invoke-direct {v0, p1, v1}, Lcom/dsi/ant/adapter/Adapter;-><init>(Lcom/dsi/ant/chip/IAntChip;Z)V

    .line 81
    .local v0, "adapter":Lcom/dsi/ant/adapter/Adapter;
    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter;->startup()V

    .line 83
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;
    invoke-static {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->access$300(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/util/Map;

    move-result-object v2

    iget-object v4, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->mHardware:Lcom/dsi/ant/chip/IAntChipDetector;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mDefaultAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->access$400(Lcom/dsi/ant/adapter/AdapterProvider;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v2

    if-nez v2, :cond_1

    .line 87
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->updateDefaultAdapter()V

    .line 92
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .end local v0    # "adapter":Lcom/dsi/ant/adapter/Adapter;
    .end local v1    # "isBuiltInAdapter":Z
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 78
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
