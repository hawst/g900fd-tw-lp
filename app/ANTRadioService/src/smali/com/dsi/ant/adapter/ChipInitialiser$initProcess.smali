.class final Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;
.super Ljava/lang/Object;
.source "ChipInitialiser.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/ChipInitialiser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "initProcess"
.end annotation


# instance fields
.field private volatile mCancelled:Z

.field private volatile mPerformInitialisation:Z

.field final synthetic this$0:Lcom/dsi/ant/adapter/ChipInitialiser;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/adapter/ChipInitialiser;)V
    .locals 1

    .prologue
    .line 57
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/adapter/ChipInitialiser;B)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/adapter/ChipInitialiser;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;-><init>(Lcom/dsi/ant/adapter/ChipInitialiser;)V

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 68
    return-void
.end method

.method public final restartInit()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    .line 72
    iput-boolean v0, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 73
    return-void
.end method

.method public final run()V
    .locals 15

    .prologue
    const/16 v14, 0x6f

    const/16 v13, 0x40

    const/4 v12, 0x4

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 82
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    .line 84
    :cond_0
    :goto_0
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    if-eqz v7, :cond_16

    .line 85
    iput-boolean v11, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    .line 86
    iput-boolean v11, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 88
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Initializing..."

    invoke-static {v7, v8}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-eqz v7, :cond_1

    .line 91
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 95
    :cond_1
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/adapter/Adapter;->getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;

    move-result-object v7

    .line 97
    const/4 v4, 0x0

    .local v4, "firmwareVersionMessage":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    if-nez v7, :cond_3

    .line 98
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 100
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    iget-object v7, v7, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    const/16 v8, 0x3e

    invoke-static {v11, v8}, Lcom/dsi/ant/AntMessageBuilder;->getANTRequestMessage(BB)[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([B)[B

    move-result-object v5

    .line 103
    .local v5, "rawVersionMessage":[B
    if-nez v5, :cond_2

    .line 105
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    const-string v8, "Serial error while requesting version message."

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 106
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 109
    :cond_2
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-nez v7, :cond_3

    .line 110
    invoke-static {v5}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    .line 112
    .local v0, "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    if-nez v0, :cond_5

    .line 113
    aget-byte v7, v5, v10

    if-ne v7, v14, :cond_4

    .line 114
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    .line 115
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 120
    :goto_1
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 144
    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .end local v5    # "rawVersionMessage":[B
    :cond_3
    :goto_2
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-eqz v7, :cond_8

    .line 145
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 118
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .restart local v5    # "rawVersionMessage":[B
    :cond_4
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    const-string v8, "Invalid response while requesting version message."

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    goto :goto_1

    .line 121
    :cond_5
    sget-object v7, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v8

    if-ne v7, v8, :cond_6

    .line 122
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    move-object v4, v0

    .line 124
    check-cast v4, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 125
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/dsi/ant/adapter/Adapter;->setFirmwareVersion(Lcom/dsi/ant/message/fromant/AntVersionMessage;)V

    .line 127
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "<init> adapter firmware version determined: \n"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_2

    .line 128
    :cond_6
    aget-byte v7, v5, v10

    if-ne v7, v13, :cond_7

    aget-byte v7, v5, v12

    const/16 v8, 0x28

    if-ne v7, v8, :cond_7

    .line 130
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 132
    new-instance v4, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .end local v4    # "firmwareVersionMessage":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    const-string v7, ""

    invoke-direct {v4, v7}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    .line 135
    .restart local v4    # "firmwareVersionMessage":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/dsi/ant/adapter/Adapter;->setFirmwareVersion(Lcom/dsi/ant/message/fromant/AntVersionMessage;)V

    goto :goto_2

    .line 137
    :cond_7
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unexpected response to version request: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 139
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    goto/16 :goto_2

    .line 149
    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .end local v5    # "rawVersionMessage":[B
    :cond_8
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/adapter/Adapter;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v1

    .line 151
    .local v1, "capabilities":Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
    if-nez v1, :cond_a

    .line 152
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 154
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    iget-object v7, v7, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    const/16 v8, 0x54

    invoke-static {v11, v8}, Lcom/dsi/ant/AntMessageBuilder;->getANTRequestMessage(BB)[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([B)[B

    move-result-object v2

    .line 157
    .local v2, "capabilitiesRequestResult":[B
    if-nez v2, :cond_9

    .line 159
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    const-string v8, "Serial error while requesting capabilities message."

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 160
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 163
    :cond_9
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-nez v7, :cond_a

    .line 164
    invoke-static {v2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    .line 166
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    if-nez v0, :cond_c

    .line 167
    aget-byte v7, v2, v10

    if-ne v7, v14, :cond_b

    .line 168
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    .line 169
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 174
    :goto_3
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 192
    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .end local v2    # "capabilitiesRequestResult":[B
    :cond_a
    :goto_4
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/adapter/Adapter;->setupChannelClosedController()V

    .line 194
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-eqz v7, :cond_e

    .line 195
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto/16 :goto_0

    .line 172
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .restart local v2    # "capabilitiesRequestResult":[B
    :cond_b
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    const-string v8, "Invalid response while requesting capabilities message."

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    goto :goto_3

    .line 175
    :cond_c
    aget-byte v7, v2, v10

    const/16 v8, 0x54

    if-ne v7, v8, :cond_d

    .line 176
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    move-object v1, v0

    .line 178
    check-cast v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .line 179
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/dsi/ant/adapter/Adapter;->setCapabilities(Lcom/dsi/ant/message/fromant/CapabilitiesMessage;)V

    .line 181
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "<init> adapter capabilities determined: \n"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_4

    .line 183
    :cond_d
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unexpected response to capabilities request: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 185
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    goto :goto_4

    .line 199
    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .end local v2    # "capabilitiesRequestResult":[B
    :cond_e
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/adapter/Adapter;->getFirmwareVersionCapabilities()Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    move-result-object v3

    .line 201
    .local v3, "firmwareVersionCapabilities":Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;->requiresTxPowerReset()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 202
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 203
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    iget-object v7, v7, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-static {}, Lcom/dsi/ant/AntMessageBuilder;->getANTSetTxPower$25251d6()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([B)[B

    move-result-object v6

    .line 206
    .local v6, "result":[B
    if-nez v6, :cond_10

    .line 207
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    const-string v8, "Serial error while resetting tx power."

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 208
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 223
    .end local v6    # "result":[B
    :cond_f
    :goto_5
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-eqz v7, :cond_13

    .line 224
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto/16 :goto_0

    .line 209
    .restart local v6    # "result":[B
    :cond_10
    aget-byte v7, v6, v10

    if-ne v7, v13, :cond_11

    aget-byte v7, v6, v12

    if-nez v7, :cond_11

    .line 211
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_5

    .line 212
    :cond_11
    aget-byte v7, v6, v10

    if-ne v7, v14, :cond_12

    .line 213
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mPerformInitialisation:Z

    .line 214
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 215
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_5

    .line 217
    :cond_12
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unexpected response to tx power reset command: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 219
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    goto :goto_5

    .line 228
    .end local v6    # "result":[B
    :cond_13
    sget-object v7, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_ADVANCED_BURST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    invoke-virtual {v1, v7}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 229
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 231
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$100(Lcom/dsi/ant/adapter/ChipInitialiser;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v7

    iget-object v7, v7, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-static {}, Lcom/dsi/ant/AntMessageBuilder;->getANTAdvanceBurstConfigurationMessage$480d76b0()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([B)[B

    move-result-object v6

    .line 234
    .restart local v6    # "result":[B
    if-nez v6, :cond_15

    .line 235
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    const-string v8, "Serial error while configuring advanced burst."

    invoke-interface {v7, v8}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->errorOccured(Ljava/lang/String;)V

    .line 236
    iput-boolean v10, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    .line 243
    .end local v6    # "result":[B
    :cond_14
    :goto_6
    iget-boolean v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->mCancelled:Z

    if-eqz v7, :cond_0

    .line 244
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto/16 :goto_0

    .line 237
    .restart local v6    # "result":[B
    :cond_15
    aget-byte v7, v6, v10

    if-ne v7, v13, :cond_14

    aget-byte v7, v6, v12

    if-eqz v7, :cond_14

    .line 239
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    # getter for: Lcom/dsi/ant/adapter/ChipInitialiser;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/dsi/ant/adapter/ChipInitialiser;->access$000(Lcom/dsi/ant/adapter/ChipInitialiser;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "<init> Configuring advanced burst with default settings failed."

    invoke-static {v7, v8}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 247
    .end local v1    # "capabilities":Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
    .end local v3    # "firmwareVersionCapabilities":Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    .end local v4    # "firmwareVersionMessage":Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .end local v6    # "result":[B
    :cond_16
    iget-object v7, p0, Lcom/dsi/ant/adapter/ChipInitialiser$initProcess;->this$0:Lcom/dsi/ant/adapter/ChipInitialiser;

    iget-object v7, v7, Lcom/dsi/ant/adapter/ChipInitialiser;->mInitReceiver:Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;

    invoke-interface {v7}, Lcom/dsi/ant/adapter/ChipInitialiser$ChipInitReceiver;->onInitCompleted()V

    .line 248
    return-void
.end method
