.class final Lcom/dsi/ant/adapter/AdapterStateMachine$2;
.super Ljava/lang/Object;
.source "AdapterStateMachine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/adapter/AdapterStateMachine;->startEnable()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/AdapterStateMachine;)V
    .locals 0

    .prologue
    .line 763
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$2;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$2;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mAdapter:Lcom/dsi/ant/adapter/Adapter;
    invoke-static {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$600(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/Adapter;

    move-result-object v0

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v0}, Lcom/dsi/ant/chip/IAntChip;->enable()I

    move-result v0

    .line 768
    packed-switch v0, :pswitch_data_0

    .line 775
    :goto_0
    :pswitch_0
    return-void

    .line 770
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$2;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    const-string v1, "Chip ignored enable command."

    invoke-virtual {v0, v1}, Lcom/dsi/ant/adapter/AdapterStateMachine;->onError(Ljava/lang/String;)V

    goto :goto_0

    .line 774
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$2;->this$0:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->deferEnable()V

    goto :goto_0

    .line 768
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
