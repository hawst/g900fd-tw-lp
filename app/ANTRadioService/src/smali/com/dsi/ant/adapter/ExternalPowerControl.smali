.class public Lcom/dsi/ant/adapter/ExternalPowerControl;
.super Lcom/dsi/ant/adapter/PowerControl;
.source "ExternalPowerControl.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/PowerControl;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/adapter/ExternalPowerControl;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/ExternalPowerControl;->TAG:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public final canDisable()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public final canEnable()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method

.method final init()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/dsi/ant/adapter/PowerControl;->init()V

    .line 48
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/ExternalPowerControl;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 49
    return-void
.end method
