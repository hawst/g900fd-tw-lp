.class final Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;
.super Ljava/lang/Object;
.source "AdapterProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/AdapterProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ChipInitProcess"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/adapter/AdapterProvider;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/adapter/AdapterProvider;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/adapter/AdapterProvider;B)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/adapter/AdapterProvider;

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;-><init>(Lcom/dsi/ant/adapter/AdapterProvider;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 220
    sget-object v2, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 221
    iget-object v2, p0, Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    # getter for: Lcom/dsi/ant/adapter/AdapterProvider;->mHardwareList:Ljava/util/Map;
    invoke-static {v2}, Lcom/dsi/ant/adapter/AdapterProvider;->access$300(Lcom/dsi/ant/adapter/AdapterProvider;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/chip/IAntChipDetector;

    .line 223
    .local v0, "hardware":Lcom/dsi/ant/chip/IAntChipDetector;
    new-instance v2, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;

    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterProvider$ChipInitProcess;->this$0:Lcom/dsi/ant/adapter/AdapterProvider;

    invoke-direct {v2, v3, v0}, Lcom/dsi/ant/adapter/AdapterProvider$HardwareCallback;-><init>(Lcom/dsi/ant/adapter/AdapterProvider;Lcom/dsi/ant/chip/IAntChipDetector;)V

    invoke-interface {v0, v2}, Lcom/dsi/ant/chip/IAntChipDetector;->init(Lcom/dsi/ant/chip/IAntChipDetectedListener;)V

    goto :goto_0

    .line 226
    .end local v0    # "hardware":Lcom/dsi/ant/chip/IAntChipDetector;
    :cond_0
    sget-object v2, Lcom/dsi/ant/adapter/AdapterProvider;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 227
    return-void
.end method
