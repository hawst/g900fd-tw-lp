.class Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;
.super Landroid/os/Handler;
.source "AdapterStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/adapter/AdapterStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestHandler"
.end annotation


# instance fields
.field private HANDLER_TAG:Ljava/lang/String;

.field private final mDestroy_LOCK:Ljava/lang/Object;

.field private mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/AdapterStateMachine;)V
    .locals 1
    .param p1, "stateMachine"    # Lcom/dsi/ant/adapter/AdapterStateMachine;

    .prologue
    .line 153
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 146
    const-class v0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->HANDLER_TAG:Ljava/lang/String;

    .line 148
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mDestroy_LOCK:Ljava/lang/Object;

    .line 154
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    .line 155
    return-void
.end method


# virtual methods
.method final destroy()V
    .locals 2

    .prologue
    .line 158
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    .line 160
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 161
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 166
    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 181
    :goto_0
    return-void

    .line 169
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 178
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->HANDLER_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Internal message with unknown id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_1
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 171
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    sget-object v2, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/adapter/AdapterStateMachine;->setAdapterState(Lcom/dsi/ant/adapter/AntAdapterState;)V

    .line 173
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-static {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$000(Lcom/dsi/ant/adapter/AdapterStateMachine;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterStateMachine$RequestHandler;->mStateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    # getter for: Lcom/dsi/ant/adapter/AdapterStateMachine;->mChipInitialiser:Lcom/dsi/ant/adapter/ChipInitialiser;
    invoke-static {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->access$100(Lcom/dsi/ant/adapter/AdapterStateMachine;)Lcom/dsi/ant/adapter/ChipInitialiser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipInitialiser;->startInit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
