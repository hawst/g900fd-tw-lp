.class public final Lcom/dsi/ant/adapter/ChannelCloseController;
.super Ljava/lang/Object;
.source "ChannelCloseController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field channelStateQueryHandler:Landroid/os/Handler;

.field handlerLock:Ljava/lang/Object;

.field private mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

.field private volatile mFirstCloseStatusSeen:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 240
    const-class v0, Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/ChannelCloseController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->handlerLock:Ljava/lang/Object;

    .line 258
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J

    .line 261
    iput-object p1, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 262
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/ChannelCloseController;)Lcom/dsi/ant/adapter/Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChannelCloseController;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/adapter/ChannelCloseController;)J
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChannelCloseController;

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/adapter/ChannelCloseController;J)J
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/adapter/ChannelCloseController;
    .param p1, "x1"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mFirstCloseStatusSeen:J

    return-wide p1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/dsi/ant/adapter/ChannelCloseController;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final initializeController(Lcom/dsi/ant/message/fromant/CapabilitiesMessage;)V
    .locals 5
    .param p1, "capabilities"    # Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .prologue
    .line 291
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->handlerLock:Ljava/lang/Object;

    monitor-enter v3

    .line 293
    :try_start_0
    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getNumberOfChannels()I

    move-result v2

    new-array v2, v2, [Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    iput-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    .line 294
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 295
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    new-instance v4, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    invoke-direct {v4, p0, v1}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;-><init>(Lcom/dsi/ant/adapter/ChannelCloseController;I)V

    aput-object v4, v2, v1

    .line 294
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 298
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "ChannelCloseQueryHandler"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 299
    .local v0, "ht":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 300
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    .line 301
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .end local v0    # "ht":Landroid/os/HandlerThread;
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public final onChannelMessage([B)Z
    .locals 5
    .param p1, "message"    # [B

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 411
    aget-byte v2, p1, v1

    sparse-switch v2, :sswitch_data_0

    .line 443
    :goto_0
    return v1

    .line 420
    :sswitch_0
    const/4 v2, 0x4

    aget-byte v2, p1, v2

    if-ne v2, v1, :cond_0

    .line 421
    aget-byte v0, p1, v4

    .line 422
    .local v0, "channelNumber":I
    iget-object v2, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->setIsClosing(Z)V

    .line 424
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v1, v1, v0

    const-wide/16 v2, 0x172

    invoke-virtual {p0, v1, v2, v3}, Lcom/dsi/ant/adapter/ChannelCloseController;->scheduleQueryTask(Ljava/lang/Runnable;J)V

    .line 435
    .end local v0    # "channelNumber":I
    :cond_0
    :sswitch_1
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-byte v2, p1, v4

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->processChannelMessage([B)Z

    move-result v1

    goto :goto_0

    .line 439
    :sswitch_2
    aget-byte v1, p1, v4

    and-int/lit8 v0, v1, 0x1f

    .line 441
    .restart local v0    # "channelNumber":I
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->processChannelMessage([B)Z

    move-result v1

    goto :goto_0

    .line 411
    nop

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x4e -> :sswitch_1
        0x4f -> :sswitch_1
        0x50 -> :sswitch_2
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method final processChannelStateCommand([B)[B
    .locals 6
    .param p1, "message"    # [B

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    .line 316
    aget-byte v1, p1, v5

    .line 318
    .local v1, "msgId":B
    sparse-switch v1, :sswitch_data_0

    .line 360
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v3, v3, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v3, p1, v5}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([BZ)[B

    move-result-object v2

    .line 363
    .local v2, "response":[B
    :goto_0
    return-object v2

    .line 331
    .end local v2    # "response":[B
    :sswitch_0
    aget-byte v0, p1, v4

    .line 339
    .local v0, "channelNumber":I
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->isClosing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 341
    new-instance v3, Lcom/dsi/ant/message/fromant/GeneratedChannelResponseMessage;

    aget-byte v4, p1, v5

    sget-object v5, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_IN_WRONG_STATE:Lcom/dsi/ant/message/ResponseCode;

    invoke-direct {v3, v0, v4, v5}, Lcom/dsi/ant/message/fromant/GeneratedChannelResponseMessage;-><init>(IILcom/dsi/ant/message/ResponseCode;)V

    .line 345
    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/GeneratedChannelResponseMessage;->getMessageContent()[B

    move-result-object v2

    .line 346
    .restart local v2    # "response":[B
    goto :goto_0

    .line 348
    .end local v2    # "response":[B
    :cond_1
    const/16 v3, 0x4c

    if-ne v1, v3, :cond_0

    .line 352
    aget-byte v3, p1, v4

    iget-object v4, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v4, v4, v3

    invoke-virtual {v4, v5}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->setIsClosing(Z)V

    iget-object v4, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v4, v4, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v4, p1, v5}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([BZ)[B

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v4, 0x4

    aget-byte v4, v2, v4

    sget-object v5, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    invoke-virtual {v5}, Lcom/dsi/ant/message/ResponseCode;->getRawValue()I

    move-result v5

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v3, v4, v3

    const-wide/16 v4, 0x172

    invoke-virtual {p0, v3, v4, v5}, Lcom/dsi/ant/adapter/ChannelCloseController;->scheduleQueryTask(Ljava/lang/Runnable;J)V

    .line 353
    .restart local v2    # "response":[B
    :goto_1
    goto :goto_0

    .line 352
    .end local v2    # "response":[B
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    aget-object v3, v4, v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->setIsClosing(Z)V

    goto :goto_1

    .line 318
    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x4b -> :sswitch_0
        0x4c -> :sswitch_0
    .end sparse-switch
.end method

.method final scheduleQueryTask(Ljava/lang/Runnable;J)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "delayMillis"    # J

    .prologue
    .line 306
    iget-object v1, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->handlerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 308
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 311
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final shutdown()V
    .locals 6

    .prologue
    .line 268
    iget-object v4, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->handlerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 270
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    if-eqz v3, :cond_1

    .line 272
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 275
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 276
    iget-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->quit()V

    .line 278
    iget-object v0, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->mChannelStateControllerList:[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;

    .local v0, "arr$":[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 279
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;->setIsClosing(Z)V

    .line 278
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 282
    .end local v0    # "arr$":[Lcom/dsi/ant/adapter/ChannelCloseController$ChannelCloseStateTracker;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/adapter/ChannelCloseController;->channelStateQueryHandler:Landroid/os/Handler;

    .line 284
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method
