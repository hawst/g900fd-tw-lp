.class public final Lcom/dsi/ant/adapter/AdapterHandle;
.super Ljava/lang/Object;
.source "AdapterHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

.field private final mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/dsi/ant/adapter/AdapterHandle;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/adapter/AdapterHandle;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/adapter/AdapterGatekeeper;)V
    .locals 0
    .param p1, "gatekeeper"    # Lcom/dsi/ant/adapter/AdapterGatekeeper;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    .line 60
    return-void
.end method


# virtual methods
.method public final assignAdapterReceiver(Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;)V
    .locals 2
    .param p1, "receiver"    # Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null argument received."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    .line 83
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    if-ne p0, p1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v1

    .line 152
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 153
    goto :goto_0

    .line 155
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/adapter/AdapterHandle;

    if-nez v3, :cond_3

    move v1, v2

    .line 156
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 159
    check-cast v0, Lcom/dsi/ant/adapter/AdapterHandle;

    .line 161
    .local v0, "other":Lcom/dsi/ant/adapter/AdapterHandle;
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    if-eqz v3, :cond_4

    .line 162
    iget-object v3, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    iget-object v4, v0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 163
    goto :goto_0

    .line 165
    :cond_4
    iget-object v3, v0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    if-eqz v3, :cond_0

    move v1, v2

    .line 166
    goto :goto_0
.end method

.method public final forceClaimAdapter()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->forceClaimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v0

    return v0
.end method

.method public final getAdapterReceiver()Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mAdapterReceiver:Lcom/dsi/ant/adapter/AdapterHandle$AdapterHandleReceiver;

    return-object v0
.end method

.method public final getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    move-result-object v0

    return-object v0
.end method

.method public final getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;

    move-result-object v0

    return-object v0
.end method

.method public final getState()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method public final hasClaim()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->hasClaim(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0xd9

    .line 144
    return v0

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final isAdapterClaimed()Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->isAdapterClaimed()Z

    move-result v0

    return v0
.end method

.method public final reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->reinitialize(Lcom/dsi/ant/adapter/AdapterHandle;)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method public final releaseClaim(Z)V
    .locals 1
    .param p1, "sendReset"    # Z

    .prologue
    .line 116
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->reinitialize(Lcom/dsi/ant/adapter/AdapterHandle;)Lcom/dsi/ant/adapter/AntAdapterState;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->releaseClaim(Lcom/dsi/ant/adapter/AdapterHandle;)V

    .line 119
    return-void
.end method

.method public final strongClaimAdapter()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->strongClaimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Handle<"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final txBurst(I[B)Z
    .locals 1
    .param p1, "channel"    # I
    .param p2, "data"    # [B

    .prologue
    .line 189
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0, p1, p2}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->txBurst(Lcom/dsi/ant/adapter/AdapterHandle;I[B)Z

    move-result v0

    return v0
.end method

.method public final txCommand([B)[B
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 179
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0, p1}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->txCommand(Lcom/dsi/ant/adapter/AdapterHandle;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final txData([B)Z
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 184
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0, p1}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->txData(Lcom/dsi/ant/adapter/AdapterHandle;[B)Z

    move-result v0

    return v0
.end method

.method public final weakClaimAdapter()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/dsi/ant/adapter/AdapterHandle;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->weakClaimAdapter(Lcom/dsi/ant/adapter/AdapterHandle;)Z

    move-result v0

    return v0
.end method
