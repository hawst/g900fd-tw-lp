.class public Lcom/dsi/ant/adapter/BuiltInPowerControl;
.super Lcom/dsi/ant/adapter/PowerControl;
.source "BuiltInPowerControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/BuiltInPowerControl$2;
    }
.end annotation


# static fields
.field private static final AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;


# instance fields
.field final TAG:Ljava/lang/String;

.field private final mAirplaneReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 57
    const/4 v1, 0x0

    .line 60
    .local v1, "temp":Ljava/lang/String;
    :try_start_0
    const-class v2, Landroid/provider/Settings$System;

    const-string v3, "AIRPLANE_MODE_TOGGLEABLE_RADIOS"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 61
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    sput-object v1, Lcom/dsi/ant/adapter/BuiltInPowerControl;->AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;

    .line 66
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 4
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    const/4 v3, 0x0

    .line 120
    invoke-direct {p0, p1}, Lcom/dsi/ant/adapter/PowerControl;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    .line 183
    new-instance v0, Lcom/dsi/ant/adapter/BuiltInPowerControl$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/BuiltInPowerControl$1;-><init>(Lcom/dsi/ant/adapter/BuiltInPowerControl;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->mAirplaneReceiver:Landroid/content/BroadcastReceiver;

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/adapter/BuiltInPowerControl;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    .line 124
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "com.dsi.ant.service.AntManager"

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "airplaneToggleInit"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    const-string v3, "Setting up default airplane mode settings."

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "airplaneToggleInit"

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "airplane_mode_radios"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Current AIRPLANE_MODE_RADIOS_STRING: <"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_6

    const-string v0, ""

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    :cond_0
    const-string v1, "ant"

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "New AIRPLANE_MODE_RADIOS_STRING: <"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_radios"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    sget-object v0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;

    if-eqz v0, :cond_a

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/adapter/BuiltInPowerControl;->AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Current AIRPLANE_MODE_TOGGLEABLE_RADIOS is: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_8

    const-string v0, ""

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_2
    const-string v1, "ant"

    :cond_3
    :goto_3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/dsi/ant/adapter/BuiltInPowerControl;->AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AIRPLANE_MODE_TOGGLEABLE_RADIOS set to: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_4

    const-string v1, ""

    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_5
    :goto_4
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->mAirplaneReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 125
    return-void

    :cond_6
    move-object v0, v1

    .line 124
    goto/16 :goto_0

    :cond_7
    const-string v0, "ant"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ant"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    goto :goto_2

    :cond_9
    const-string v0, "ant"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ant"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    goto :goto_4
.end method

.method public static final isAirplaneModeOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 148
    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isInAirplaneModeRadiosSetting()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isInAirplaneToggleableRadiosSetting()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private static final isInAirplaneModeRadiosSetting()Z
    .locals 3

    .prologue
    .line 129
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_radios"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "airplaneModeRadios":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "ant"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static final isInAirplaneToggleableRadiosSetting()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 138
    sget-object v2, Lcom/dsi/ant/adapter/BuiltInPowerControl;->AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 143
    .local v0, "toggleableRadios":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 140
    .end local v0    # "toggleableRadios":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/adapter/BuiltInPowerControl;->AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    .restart local v0    # "toggleableRadios":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "ant"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final canDisable()Z
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    return v0
.end method

.method public final canEnable()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 288
    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isAirplaneModeOn()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isInAirplaneModeRadiosSetting()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isInAirplaneToggleableRadiosSetting()Z

    move-result v2

    :goto_0
    if-nez v2, :cond_2

    .line 289
    iget-object v1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    const-string v2, "Enable request ignored, Airplane mode is on and ANT is not in the toggleable radios list."

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :goto_1
    return v0

    .line 288
    :cond_0
    invoke-static {}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->isInAirplaneToggleableRadiosSetting()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 295
    goto :goto_1
.end method

.method public final disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1
    .param p1, "delayDisable"    # Z

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->saveChipState(Z)V

    .line 302
    invoke-super {p0, p1}, Lcom/dsi/ant/adapter/PowerControl;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method public final enable()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->saveChipState(Z)V

    .line 309
    invoke-super {p0}, Lcom/dsi/ant/adapter/PowerControl;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method final init()V
    .locals 1

    .prologue
    .line 326
    invoke-super {p0}, Lcom/dsi/ant/adapter/PowerControl;->init()V

    .line 328
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->canEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 344
    :goto_0
    return-void

    .line 331
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->disable(Z)Lcom/dsi/ant/adapter/AntAdapterState;

    .line 335
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->setServiceInitialised()V

    goto :goto_0
.end method

.method final saveChipState(Z)V
    .locals 4
    .param p1, "doEnable"    # Z

    .prologue
    .line 212
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "PowerControlPref"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 213
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 215
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Saving adapter state, enabled: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v1, "CHIP_STATE_KEY"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 218
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 220
    return-void
.end method

.method final shutdown(Z)V
    .locals 2
    .param p1, "doDisable"    # Z

    .prologue
    .line 314
    :try_start_0
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->mAirplaneReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :goto_0
    invoke-super {p0, p1}, Lcom/dsi/ant/adapter/PowerControl;->shutdown(Z)V

    .line 318
    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 319
    return-void

    .line 314
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0
.end method

.method public final tryEnable()Lcom/dsi/ant/power/PowerManager$CanEnableState;
    .locals 10

    .prologue
    .line 246
    iget-object v6, p0, Lcom/dsi/ant/adapter/BuiltInPowerControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v6, v6, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v6}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getLastEnableTimeStamp()J

    move-result-wide v3

    .line 248
    .local v3, "lastEnableTimeStamp":J
    sget-object v6, Lcom/dsi/ant/power/PowerManager$CanEnableState;->NO_ADAPTER:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 250
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 252
    .local v1, "currentTimeMillis":J
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->canEnable()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 254
    sub-long v6, v1, v3

    const-wide/32 v8, 0xea60

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;->enable()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    .line 259
    .local v0, "adapterState":Lcom/dsi/ant/adapter/AntAdapterState;
    sget-object v6, Lcom/dsi/ant/adapter/BuiltInPowerControl$2;->$SwitchMap$com$dsi$ant$adapter$AntAdapterState:[I

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AntAdapterState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 267
    sget-object v5, Lcom/dsi/ant/power/PowerManager$CanEnableState;->NO_ADAPTER:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 277
    .end local v0    # "adapterState":Lcom/dsi/ant/adapter/AntAdapterState;
    .local v5, "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    :goto_0
    return-object v5

    .line 263
    .end local v5    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    .restart local v0    # "adapterState":Lcom/dsi/ant/adapter/AntAdapterState;
    :pswitch_0
    sget-object v5, Lcom/dsi/ant/power/PowerManager$CanEnableState;->CAN_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .line 264
    .restart local v5    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    goto :goto_0

    .line 271
    .end local v0    # "adapterState":Lcom/dsi/ant/adapter/AntAdapterState;
    .end local v5    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    :cond_0
    sget-object v5, Lcom/dsi/ant/power/PowerManager$CanEnableState;->ERROR_RECOVERY_COOLDOWN:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .restart local v5    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    goto :goto_0

    .line 274
    .end local v5    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    :cond_1
    sget-object v5, Lcom/dsi/ant/power/PowerManager$CanEnableState;->AIRPLANE_MODE_BLOCKING_ENABLE:Lcom/dsi/ant/power/PowerManager$CanEnableState;

    .restart local v5    # "result":Lcom/dsi/ant/power/PowerManager$CanEnableState;
    goto :goto_0

    .line 259
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
