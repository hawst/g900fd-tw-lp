.class final Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;
.super Ljava/lang/Object;
.source "AntDeviceInterfaceCheckVersions.java"


# static fields
.field static final BROADCOM_TX_POWER_LEVEL_REMAP:[B

.field static final WILINK_TX_POWER_LEVEL_REMAP:[B

.field public static final burstBufferBugInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final delayAirplaneModeDisableInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field public static final txPowerRemapInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->WILINK_TX_POWER_LEVEL_REMAP:[B

    .line 33
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->BROADCOM_TX_POWER_LEVEL_REMAP:[B

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AOV1.15B00"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AUM1.03B00"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->txPowerRemapInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 54
    new-array v0, v4, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AUM1.03B13"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->burstBufferBugInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 67
    new-array v0, v4, [Lcom/dsi/ant/message/fromant/AntVersionMessage;

    new-instance v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    const-string v2, "AUM9.99B99"

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/adapter/AntDeviceInterfaceCheckVersions;->delayAirplaneModeDisableInclusionList:[Lcom/dsi/ant/message/fromant/AntVersionMessage;

    return-void

    .line 23
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
        0x2t
    .end array-data

    .line 33
    nop

    :array_1
    .array-data 1
        0x1t
        0x2t
        0x3t
        0x3t
        0x3t
    .end array-data
.end method
