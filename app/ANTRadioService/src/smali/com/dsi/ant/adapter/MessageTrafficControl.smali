.class final Lcom/dsi/ant/adapter/MessageTrafficControl;
.super Ljava/lang/Object;
.source "MessageTrafficControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/MessageTrafficControl$1;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mAdapter:Lcom/dsi/ant/adapter/Adapter;

.field private volatile mBurstPacketSequenceNumber:B

.field private mCanContinueBurst:Z

.field private final mCommandLock:Ljava/lang/Object;

.field private volatile mReceivedResponse:[B

.field private volatile mRequestedMessage:B

.field private mResetType:B

.field private final mResponseCondition:Ljava/util/concurrent/locks/Condition;

.field private final mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private mSentMessage:[B

.field private mShutdown:Z

.field private mTxBurstSupported:Z

.field private mTxInterrupted:Z


# direct methods
.method constructor <init>(Lcom/dsi/ant/adapter/Adapter;)V
    .locals 3
    .param p1, "adapter"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCommandLock:Ljava/lang/Object;

    .line 81
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 86
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseCondition:Ljava/util/concurrent/locks/Condition;

    .line 91
    iput-object v2, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B

    .line 98
    iput-byte v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mRequestedMessage:B

    .line 108
    iput-byte v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    .line 113
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCanContinueBurst:Z

    .line 119
    iput-object v2, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    .line 124
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxInterrupted:Z

    .line 129
    iput-boolean v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mShutdown:Z

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxBurstSupported:Z

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    .line 150
    iput-object p1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    .line 151
    return-void
.end method

.method private attemptRecovery()V
    .locals 2

    .prologue
    .line 803
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v1, "Attempting recovery."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v0}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 807
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v1, "<recovery> Chip not enabled, recovery not needed."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    :goto_0
    return-void

    .line 811
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 813
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v0, v0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    goto :goto_0
.end method

.method private clearRequestedMessage()V
    .locals 1

    .prologue
    .line 171
    iget-byte v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mRequestedMessage:B

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 173
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mRequestedMessage:B

    .line 175
    :cond_0
    return-void
.end method

.method private sendConcatenatedBurst(I[B)Z
    .locals 17
    .param p1, "channelNumber"    # I
    .param p2, "data"    # [B

    .prologue
    .line 401
    move-object/from16 v0, p2

    array-length v4, v0

    .line 402
    .local v4, "bytesLeftToSend":I
    const/4 v6, 0x0

    .line 403
    .local v6, "dataOffset":I
    const/4 v8, 0x0

    .line 405
    .local v8, "hciPacketCount":I
    move-object/from16 v0, p2

    array-length v13, v0

    int-to-double v13, v13

    const-wide/high16 v15, 0x4020000000000000L    # 8.0

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v13

    double-to-int v1, v13

    .line 407
    .local v1, "antPacketsLeft":I
    const/4 v12, 0x0

    .line 408
    .local v12, "ucSeq":B
    const/4 v7, 0x1

    .line 409
    .local v7, "doBurst":Z
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCanContinueBurst:Z

    .line 411
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "txConcatenatedBurst: Sending "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    array-length v15, v0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " bytes, composing "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ANT packets."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_0
    const/4 v10, 0x0

    .line 418
    .local v10, "numAntPacketsCombined":I
    const/16 v13, 0x8

    invoke-static {v1, v13}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 420
    .local v11, "numAntPacketsCombinedTarget":I
    const/4 v2, 0x0

    .line 422
    .local v2, "burstBufferOffset":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "ANTTransmitBurst: Combining next "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ANT packets inside 1 HCI packet"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    mul-int/lit8 v13, v11, 0xb

    new-array v9, v13, [B

    .line 430
    .local v9, "mBurstBuffer":[B
    :cond_1
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "burstBufferOffset":I
    .local v3, "burstBufferOffset":I
    const/16 v13, 0x9

    aput-byte v13, v9, v2

    .line 431
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "burstBufferOffset":I
    .restart local v2    # "burstBufferOffset":I
    const/16 v13, 0x50

    aput-byte v13, v9, v3

    .line 434
    const/16 v13, 0x8

    if-gt v4, v13, :cond_2

    .line 436
    or-int/lit8 v13, v12, -0x80

    int-to-byte v12, v13

    .line 437
    add-int v13, v2, v4

    array-length v14, v9

    const/4 v15, 0x0

    invoke-static {v9, v13, v14, v15}, Ljava/util/Arrays;->fill([BIIB)V

    .line 441
    :cond_2
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "burstBufferOffset":I
    .restart local v3    # "burstBufferOffset":I
    or-int v13, p1, v12

    int-to-byte v13, v13

    aput-byte v13, v9, v2

    .line 444
    const/16 v13, 0x8

    invoke-static {v4, v13}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 445
    .local v5, "dataCopyLength":I
    move-object/from16 v0, p2

    invoke-static {v0, v6, v9, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 448
    sub-int/2addr v4, v5

    .line 449
    add-int/2addr v6, v5

    .line 450
    add-int v2, v3, v5

    .line 451
    .end local v3    # "burstBufferOffset":I
    .restart local v2    # "burstBufferOffset":I
    add-int/lit8 v10, v10, 0x1

    .line 454
    and-int/lit8 v13, v12, 0x60

    const/16 v14, 0x60

    if-ne v13, v14, :cond_5

    .line 455
    const/16 v12, 0x20

    .line 460
    :goto_0
    if-ge v10, v11, :cond_3

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCanContinueBurst:Z

    if-nez v13, :cond_1

    .line 462
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCanContinueBurst:Z

    if-eqz v13, :cond_7

    .line 464
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "ANTTransmitBurst: Sending HCI packet "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 465
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txData([B)Z

    move-result v13

    if-nez v13, :cond_6

    .line 467
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "ANTTransmitBurst: Aborting burst due to transmit error, still had "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ANT packets left"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const/4 v7, 0x0

    .line 482
    :goto_1
    if-lez v1, :cond_4

    if-nez v7, :cond_0

    .line 485
    :cond_4
    if-nez v1, :cond_8

    const/4 v13, 0x1

    :goto_2
    return v13

    .line 457
    :cond_5
    add-int/lit8 v13, v12, 0x20

    int-to-byte v12, v13

    goto :goto_0

    .line 472
    :cond_6
    sub-int/2addr v1, v10

    goto :goto_1

    .line 477
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "ANTTransmitBurst: mCanContinueBurst set to false, Aborting burst, still had "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ANT packets left"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const/4 v7, 0x0

    goto :goto_1

    .line 485
    :cond_8
    const/4 v13, 0x0

    goto :goto_2
.end method


# virtual methods
.method final getResetType()B
    .locals 1

    .prologue
    .line 756
    iget-byte v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResetType:B

    return v0
.end method

.method final handleResponseMessage([B)V
    .locals 17
    .param p1, "message"    # [B

    .prologue
    .line 520
    const/4 v6, 0x0

    .line 521
    .local v6, "doReinitialize":Z
    const/4 v5, 0x0

    .line 524
    .local v5, "doRecovery":Z
    const/4 v7, 0x1

    .line 525
    .local v7, "forwardMessage":Z
    const/4 v9, 0x0

    .line 527
    .local v9, "isAdvancedBurstMessage":Z
    const/4 v3, 0x0

    .line 529
    .local v3, "burstPackets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 533
    :try_start_0
    move-object/from16 v0, p1

    array-length v10, v0

    add-int/lit8 v10, v10, -0x2

    const/4 v11, 0x0

    aget-byte v11, p1, v11

    if-eq v10, v11, :cond_1

    .line 536
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v11, "Received message of length %d, expected %d. Contents: %s, performing error recovery."

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    array-length v14, v0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const/4 v14, 0x0

    aget-byte v14, p1, v14

    add-int/lit8 v14, v14, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-static/range {p1 .. p1}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V

    .line 544
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->interruptCommandTx()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    const/4 v5, 0x1

    .line 634
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 636
    if-eqz v6, :cond_12

    .line 637
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v10, v10, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v10}, Lcom/dsi/ant/adapter/AdapterStateMachine;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 644
    :cond_0
    :goto_0
    sget-object v10, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v11, v11, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v11}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v11

    if-eq v10, v11, :cond_10

    const/16 v10, 0x6f

    const/4 v11, 0x1

    aget-byte v11, p1, v11

    if-eq v10, v11, :cond_10

    .line 647
    if-eqz v7, :cond_10

    .line 648
    if-eqz v9, :cond_f

    .line 649
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 651
    .local v2, "burstPacket":[B
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v10, v2}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    goto :goto_1

    .line 550
    .end local v2    # "burstPacket":[B
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v10, v10, Lcom/dsi/ant/adapter/Adapter;->mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/dsi/ant/adapter/ChannelCloseController;->onChannelMessage([B)Z

    move-result v7

    .line 554
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    if-eqz v10, :cond_3

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    const/16 v11, 0x4d

    if-ne v10, v11, :cond_3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    const/4 v11, 0x3

    aget-byte v10, v10, v11

    const/4 v11, 0x1

    aget-byte v11, p1, v11

    if-ne v10, v11, :cond_3

    .line 558
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V

    .line 559
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B

    .line 560
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v10}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 634
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 636
    if-eqz v6, :cond_13

    .line 637
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v10, v10, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v10}, Lcom/dsi/ant/adapter/AdapterStateMachine;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 644
    :cond_2
    :goto_2
    sget-object v10, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v11, v11, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v11}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v11

    if-eq v10, v11, :cond_10

    const/16 v10, 0x6f

    const/4 v11, 0x1

    aget-byte v11, p1, v11

    if-eq v10, v11, :cond_10

    .line 647
    if-eqz v7, :cond_10

    .line 648
    if-eqz v9, :cond_14

    .line 649
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 651
    .restart local v2    # "burstPacket":[B
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v10, v2}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    goto :goto_3

    .line 565
    .end local v2    # "burstPacket":[B
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_3
    const/4 v10, 0x1

    :try_start_2
    aget-byte v10, p1, v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sparse-switch v10, :sswitch_data_0

    .line 634
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 636
    if-eqz v6, :cond_15

    .line 637
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v10, v10, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v10}, Lcom/dsi/ant/adapter/AdapterStateMachine;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 644
    :cond_5
    :goto_5
    sget-object v10, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v11, v11, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v11}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v11

    if-eq v10, v11, :cond_10

    const/16 v10, 0x6f

    const/4 v11, 0x1

    aget-byte v11, p1, v11

    if-eq v10, v11, :cond_10

    .line 647
    if-eqz v7, :cond_10

    .line 648
    if-eqz v9, :cond_16

    .line 649
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 651
    .restart local v2    # "burstPacket":[B
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v10, v2}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    goto :goto_6

    .line 567
    .end local v2    # "burstPacket":[B
    .end local v8    # "i$":Ljava/util/Iterator;
    :sswitch_0
    :try_start_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v11, "Startup message received."

    invoke-static {v10, v11}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const/4 v10, 0x2

    aget-byte v10, p1, v10

    move-object/from16 v0, p0

    iput-byte v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResetType:B

    .line 571
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v10}, Lcom/dsi/ant/adapter/Adapter;->resetChannelStatus$1385ff()V

    .line 573
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V

    .line 577
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B

    .line 578
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v10}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 579
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    if-eqz v10, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    const/16 v11, 0x4a

    if-eq v10, v11, :cond_4

    .line 581
    :cond_6
    const/4 v6, 0x1

    goto :goto_4

    .line 585
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 586
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V

    .line 587
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->interruptCommandTx()V

    .line 588
    const/4 v5, 0x1

    .line 589
    goto/16 :goto_4

    .line 591
    :sswitch_2
    invoke-static/range {p1 .. p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v11

    invoke-virtual {v11}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v10

    sget-object v12, Lcom/dsi/ant/adapter/MessageTrafficControl$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {v10}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v10

    aget v10, v12, v10

    packed-switch v10, :pswitch_data_0

    .line 594
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    if-eqz v10, :cond_9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    const/4 v11, 0x3

    aget-byte v11, p1, v11

    if-ne v10, v11, :cond_9

    .line 597
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V

    .line 598
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 599
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B

    .line 600
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v10}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 634
    :catchall_0
    move-exception v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v11}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 636
    if-eqz v6, :cond_11

    .line 637
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v11, v11, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v11}, Lcom/dsi/ant/adapter/AdapterStateMachine;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    .line 644
    :cond_8
    :goto_8
    sget-object v11, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v12, v12, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v12}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v12

    if-eq v11, v12, :cond_e

    const/16 v11, 0x6f

    const/4 v12, 0x1

    aget-byte v12, p1, v12

    if-eq v11, v12, :cond_e

    .line 647
    if-eqz v7, :cond_e

    .line 648
    if-eqz v9, :cond_d

    .line 649
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 651
    .restart local v2    # "burstPacket":[B
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v11, v2}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    goto :goto_9

    .line 591
    .end local v2    # "burstPacket":[B
    .end local v8    # "i$":Ljava/util/Iterator;
    :pswitch_0
    :try_start_4
    move-object v0, v11

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    move-object v10, v0

    invoke-virtual {v10}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v10

    sget-object v12, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    if-ne v10, v12, :cond_7

    invoke-virtual {v11}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    and-int/lit8 v10, v10, 0x1f

    shr-int/lit8 v10, v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v11, v10}, Lcom/dsi/ant/adapter/Adapter;->setChannelStatusInactive(I)V

    goto/16 :goto_7

    :pswitch_1
    move-object v0, v11

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-object v10, v0

    invoke-virtual {v10}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v12

    sget-object v13, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v13}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v13

    if-ne v12, v13, :cond_7

    invoke-virtual {v10}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v10

    sget-object v12, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;

    if-ne v10, v12, :cond_7

    invoke-virtual {v11}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    and-int/lit8 v10, v10, 0x1f

    shr-int/lit8 v10, v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v11, v10}, Lcom/dsi/ant/adapter/Adapter;->setChannelStatusActive(I)V

    goto/16 :goto_7

    .line 607
    :cond_9
    const/4 v10, 0x3

    aget-byte v10, p1, v10

    const/16 v11, 0x50

    if-ne v10, v11, :cond_4

    .line 608
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCanContinueBurst:Z

    goto/16 :goto_4

    .line 614
    :sswitch_3
    const/4 v10, 0x2

    aget-byte v10, p1, v10

    and-int/lit8 v10, v10, -0x20

    int-to-byte v10, v10

    .line 615
    if-nez v10, :cond_4

    .line 619
    const/16 v10, 0x20

    move-object/from16 v0, p0

    iput-byte v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    goto/16 :goto_4

    .line 624
    :sswitch_4
    const/4 v9, 0x1

    .line 627
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v10, 0x2

    aget-byte v10, p1, v10

    and-int/lit8 v10, v10, 0x1f

    int-to-byte v13, v10

    const/4 v10, 0x0

    const/16 v11, -0x80

    const/4 v12, 0x2

    aget-byte v12, p1, v12

    and-int/lit8 v12, v12, -0x80

    if-ne v11, v12, :cond_17

    const/4 v10, 0x1

    move v12, v10

    :goto_a
    move-object/from16 v0, p1

    array-length v10, v0

    add-int/lit8 v11, v10, -0x3

    const/4 v10, 0x3

    :goto_b
    move-object/from16 v0, p1

    array-length v14, v0

    if-ge v10, v14, :cond_c

    const/16 v14, 0xb

    new-array v14, v14, [B

    const/4 v15, 0x0

    const/16 v16, 0x9

    aput-byte v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x50

    aput-byte v16, v14, v15

    const/16 v15, 0x8

    if-gt v11, v15, :cond_a

    if-eqz v12, :cond_a

    move-object/from16 v0, p0

    iget-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    or-int/lit8 v15, v15, -0x80

    int-to-byte v15, v15

    move-object/from16 v0, p0

    iput-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    const/4 v15, 0x2

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    move/from16 v16, v0

    or-int v16, v16, v13

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v16, v0

    aput-byte v16, v14, v15

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    :goto_c
    const/4 v15, 0x3

    const/16 v16, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v10, v14, v15, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v11, v11, -0x8

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v10, 0x8

    goto :goto_b

    :cond_a
    const/4 v15, 0x2

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    move/from16 v16, v0

    or-int v16, v16, v13

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v16, v0

    aput-byte v16, v14, v15

    move-object/from16 v0, p0

    iget-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    and-int/lit8 v15, v15, -0x20

    const/16 v16, 0x60

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    const/16 v15, 0x20

    move-object/from16 v0, p0

    iput-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    goto :goto_c

    :cond_b
    move-object/from16 v0, p0

    iget-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    add-int/lit8 v15, v15, 0x20

    int-to-byte v15, v15

    move-object/from16 v0, p0

    iput-byte v15, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mBurstPacketSequenceNumber:B

    goto :goto_c

    :cond_c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Advanced burst packet is split into "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " standard burst packets."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v3    # "burstPackets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .local v4, "burstPackets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    move-object v3, v4

    .end local v4    # "burstPackets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .restart local v3    # "burstPackets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    goto/16 :goto_4

    .line 655
    :cond_d
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    .line 634
    :cond_e
    throw v10

    .line 655
    :cond_f
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    :goto_d
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/dsi/ant/adapter/Adapter;->onMessageReceived([B)V

    .line 659
    :cond_10
    return-void

    .line 638
    :cond_11
    if-eqz v5, :cond_8

    .line 639
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V

    goto/16 :goto_8

    .line 638
    :cond_12
    if-eqz v5, :cond_0

    .line 639
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V

    goto/16 :goto_0

    .line 638
    :cond_13
    if-eqz v5, :cond_2

    .line 639
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V

    goto/16 :goto_2

    .line 655
    :cond_14
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    goto :goto_d

    .line 638
    :cond_15
    if-eqz v5, :cond_5

    .line 639
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V

    goto/16 :goto_5

    .line 655
    :cond_16
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    goto :goto_d

    :cond_17
    move v12, v10

    goto/16 :goto_a

    .line 565
    :sswitch_data_0
    .sparse-switch
        -0x52 -> :sswitch_1
        0x40 -> :sswitch_2
        0x50 -> :sswitch_3
        0x6f -> :sswitch_0
        0x72 -> :sswitch_4
    .end sparse-switch

    .line 591
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final interruptCommandTx()V
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 497
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v1, "Interrupting command transmit."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxInterrupted:Z

    .line 499
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 503
    return-void

    .line 502
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method final shutdown()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 160
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->interruptCommandTx()V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mShutdown:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    iget-object v0, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 164
    return-void

    .line 163
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method final txBurst(I[B)Z
    .locals 4
    .param p1, "channel"    # I
    .param p2, "data"    # [B

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 359
    .local v0, "success":Z
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxBurstSupported:Z

    if-eqz v1, :cond_2

    .line 362
    :try_start_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    const-string v1, "ANTSerial"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - Tx burst   - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v1, p1, p2}, Lcom/dsi/ant/chip/IAntChip;->txBurst(I[B)Z

    move-result v0

    .line 368
    if-nez v0, :cond_2

    .line 369
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 370
    const-string v1, "ANTSerial"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - Tx burst failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_1
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxBurstSupported:Z

    if-nez v1, :cond_4

    .line 387
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 388
    iget-object v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v2, "Adapter doesn\'t support txBurst(), performing packet concatenation in MessageWriter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/adapter/MessageTrafficControl;->sendConcatenatedBurst(I[B)Z

    move-result v0

    .line 393
    :cond_4
    return v0

    .line 382
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxBurstSupported:Z

    goto :goto_0
.end method

.method final txCommand([B)[B
    .locals 3
    .param p1, "message"    # [B

    .prologue
    const/4 v2, 0x1

    .line 182
    aget-byte v1, p1, v2

    sparse-switch v1, :sswitch_data_0

    .line 198
    invoke-virtual {p0, p1, v2}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([BZ)[B

    move-result-object v0

    .line 202
    .local v0, "response":[B
    :goto_0
    return-object v0

    .line 195
    .end local v0    # "response":[B
    :sswitch_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/adapter/ChannelCloseController;->processChannelStateCommand([B)[B

    move-result-object v0

    .line 196
    .restart local v0    # "response":[B
    goto :goto_0

    .line 182
    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x42 -> :sswitch_0
        0x4b -> :sswitch_0
        0x4c -> :sswitch_0
    .end sparse-switch
.end method

.method final txCommand([BZ)[B
    .locals 11
    .param p1, "message"    # [B
    .param p2, "recoveryIfNecessary"    # Z

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x0

    .line 217
    const/4 v0, 0x0

    .line 220
    .local v0, "doRecovery":Z
    const/4 v2, 0x0

    .line 222
    .local v2, "receivedResponse":[B
    iget-object v6, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mCommandLock:Ljava/lang/Object;

    monitor-enter v6

    .line 223
    :try_start_0
    iget-object v7, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v7, v7, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v7}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getChipStateChangeLock()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 224
    :try_start_1
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 228
    :try_start_2
    iget-boolean v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mShutdown:Z

    if-eqz v8, :cond_0

    .line 229
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v9, "Ignoring command, instance shutdown."

    invoke-static {v8, v9}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 230
    const/4 v8, 0x0

    :try_start_3
    iput-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    .line 318
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 321
    :try_start_4
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 330
    :goto_0
    return-object v5

    .line 321
    :catchall_0
    move-exception v5

    :try_start_6
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 324
    :catchall_1
    move-exception v5

    :try_start_7
    monitor-exit v7

    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 325
    :catchall_2
    move-exception v5

    monitor-exit v6

    throw v5

    .line 233
    :cond_0
    const/4 v8, 0x0

    :try_start_8
    iput-boolean v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxInterrupted:Z

    .line 236
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v8, v8, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v8}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v8

    if-eq v9, v8, :cond_1

    .line 237
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Chip state is not ENABLED, not sending command. Current state is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v10, v10, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v10}, Lcom/dsi/ant/chip/IAntChip;->getChipState()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 239
    const/4 v8, 0x0

    :try_start_9
    iput-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    .line 318
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 321
    :try_start_a
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    monitor-exit v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    monitor-exit v6
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_0

    :catchall_3
    move-exception v5

    :try_start_c
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 242
    :cond_1
    const/4 v5, 0x1

    :try_start_d
    aget-byte v5, p1, v5

    const/16 v8, 0x4d

    if-ne v5, v8, :cond_2

    .line 243
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Requested message set to 0x"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x3

    aget-byte v8, p1, v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 245
    const/4 v5, 0x3

    aget-byte v5, p1, v5

    iput-byte v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mRequestedMessage:B

    .line 248
    :cond_2
    const/4 v5, 0x1

    aget-byte v5, p1, v5

    const/16 v8, 0x46

    if-eq v5, v8, :cond_3

    const/4 v5, 0x1

    aget-byte v5, p1, v5

    const/16 v8, 0x76

    if-ne v5, v8, :cond_9

    .line 250
    :cond_3
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 253
    const/4 v5, 0x2

    new-array v1, v5, [B

    .line 255
    .local v1, "maskedMessage":[B
    const/4 v5, 0x1

    const/4 v8, 0x0

    array-length v9, v1

    invoke-static {p1, v5, v1, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 257
    const-string v5, "ANTSerial"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - Tx command - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    .end local v1    # "maskedMessage":[B
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v5, v5, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v5, p1}, Lcom/dsi/ant/chip/IAntChip;->txMessage([B)Z

    move-result v5

    if-nez v5, :cond_a

    .line 270
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 271
    const-string v5, "ANTSerial"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - Tx command failed"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_5
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v8, "Failed to send message!"

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/4 v0, 0x1

    .line 301
    :cond_6
    if-nez v0, :cond_7

    .line 302
    iget-boolean v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxInterrupted:Z

    if-eqz v5, :cond_c

    .line 303
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v8, "Wait for response was interrupted."

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_7
    :goto_2
    iget-object v2, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 317
    const/4 v5, 0x0

    :try_start_e
    iput-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    .line 318
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 321
    :try_start_f
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 324
    monitor-exit v7
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 325
    :try_start_10
    monitor-exit v6
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 327
    if-eqz v0, :cond_8

    if-eqz p2, :cond_8

    .line 328
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V

    :cond_8
    move-object v5, v2

    .line 330
    goto/16 :goto_0

    .line 264
    :cond_9
    :try_start_11
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 265
    const-string v5, "ANTSerial"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - Tx command - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    goto :goto_1

    .line 323
    :catchall_4
    move-exception v5

    .line 317
    const/4 v8, 0x0

    :try_start_12
    iput-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    .line 318
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->clearRequestedMessage()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    .line 321
    :try_start_13
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 278
    :cond_a
    :try_start_14
    iput-object p1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mSentMessage:[B

    .line 279
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B

    .line 283
    sget-object v5, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v8, v8, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v8}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v8

    if-ne v5, v8, :cond_b

    const-wide/32 v3, 0x3b9aca00

    .line 286
    .local v3, "timeLeft":J
    :goto_3
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 288
    :goto_4
    const-wide/16 v8, 0x0

    cmp-long v5, v3, v8

    if-lez v5, :cond_6

    iget-boolean v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxInterrupted:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    if-nez v5, :cond_6

    .line 290
    :try_start_15
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v5, v3, v4}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_15
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_0
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    move-result-wide v3

    goto :goto_4

    .line 283
    .end local v3    # "timeLeft":J
    :cond_b
    const-wide v3, 0x9502f900L

    goto :goto_3

    .line 292
    .restart local v3    # "timeLeft":J
    :catch_0
    move-exception v5

    :try_start_16
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v8, "Command transmit thread interrupted, no longer waiting for response."

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 296
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mTxInterrupted:Z

    goto :goto_4

    .line 304
    .end local v3    # "timeLeft":J
    :cond_c
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mReceivedResponse:[B

    if-nez v5, :cond_d

    .line 305
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    const-string v8, "Timed out waiting for response."

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 308
    :cond_d
    iget-object v5, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Response received. "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_4

    goto/16 :goto_2

    .line 321
    :catchall_5
    move-exception v5

    :try_start_17
    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5

    :catchall_6
    move-exception v5

    iget-object v8, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mResponseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1
.end method

.method final txData([B)Z
    .locals 4
    .param p1, "message"    # [B

    .prologue
    .line 338
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    const-string v1, "ANTSerial"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - Tx data    - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/dsi/ant/util/LogAnt;->getHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    iget-object v1, v1, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v1, p1}, Lcom/dsi/ant/chip/IAntChip;->txMessage([B)Z

    move-result v0

    .line 344
    .local v0, "success":Z
    if-nez v0, :cond_2

    .line 345
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->getSerialDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ANTSerial"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dsi/ant/adapter/MessageTrafficControl;->mAdapter:Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - Tx data failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :cond_1
    invoke-direct {p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->attemptRecovery()V

    .line 350
    :cond_2
    return v0
.end method
