.class final Lcom/dsi/ant/adapter/Adapter;
.super Ljava/lang/Object;
.source "Adapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;,
        Lcom/dsi/ant/adapter/Adapter$AdapterReceiver;
    }
.end annotation


# instance fields
.field final TAG:Ljava/lang/String;

.field final chip:Lcom/dsi/ant/chip/IAntChip;

.field final chipReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

.field private mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

.field private mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

.field final mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

.field private mChannelsStatus:Lcom/dsi/ant/adapter/ChannelsStatus;

.field private mFirmwareVersion:Lcom/dsi/ant/message/fromant/AntVersionMessage;

.field private mFirmwareVersionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

.field private final mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

.field private final mId:I

.field final messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

.field final powerControl:Lcom/dsi/ant/adapter/PowerControl;

.field final stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/chip/IAntChip;Z)V
    .locals 3
    .param p1, "chip"    # Lcom/dsi/ant/chip/IAntChip;
    .param p2, "isBuiltIn"    # Z

    .prologue
    const/4 v1, 0x0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterProvider;->getAdapterInstanceCounter()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/adapter/Adapter;->mId:I

    .line 85
    iput-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .line 90
    iput-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersion:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 95
    iput-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dsi/ant/adapter/Adapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dsi/ant/adapter/Adapter;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    .line 111
    new-instance v0, Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/MessageTrafficControl;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    .line 114
    new-instance v0, Lcom/dsi/ant/adapter/ChipReceiver;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/ChipReceiver;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->chipReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    .line 117
    new-instance v0, Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterStateMachine;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    .line 190
    if-nez p1, :cond_0

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IAntChip given to constuctor was null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    .line 196
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Initialized, isBuiltIn="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 198
    if-eqz p2, :cond_1

    new-instance v0, Lcom/dsi/ant/adapter/BuiltInPowerControl;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/BuiltInPowerControl;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    :goto_0
    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    .line 201
    new-instance v0, Lcom/dsi/ant/adapter/ChannelsStatus;

    invoke-direct {v0, p2}, Lcom/dsi/ant/adapter/ChannelsStatus;-><init>(Z)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelsStatus:Lcom/dsi/ant/adapter/ChannelsStatus;

    .line 204
    new-instance v0, Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    .line 205
    new-instance v0, Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    .line 206
    new-instance v0, Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/ChannelCloseController;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

    .line 208
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Created around chip instance: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    return-void

    .line 198
    :cond_1
    new-instance v0, Lcom/dsi/ant/adapter/ExternalPowerControl;

    invoke-direct {v0, p0}, Lcom/dsi/ant/adapter/ExternalPowerControl;-><init>(Lcom/dsi/ant/adapter/Adapter;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/dsi/ant/adapter/Adapter;)Lcom/dsi/ant/adapter/AdapterGatekeeper;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/adapter/Adapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    return-object v0
.end method

.method private getChipName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v0}, Lcom/dsi/ant/chip/IAntChip;->getChipName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHardwareType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->chip:Lcom/dsi/ant/chip/IAntChip;

    invoke-interface {v0}, Lcom/dsi/ant/chip/IAntChip;->getHardwareType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 426
    if-ne p0, p1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v1

    .line 430
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 431
    goto :goto_0

    .line 434
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/adapter/Adapter;

    if-nez v3, :cond_3

    move v1, v2

    .line 435
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 438
    check-cast v0, Lcom/dsi/ant/adapter/Adapter;

    .line 440
    .local v0, "other":Lcom/dsi/ant/adapter/Adapter;
    iget v3, p0, Lcom/dsi/ant/adapter/Adapter;->mId:I

    iget v4, v0, Lcom/dsi/ant/adapter/Adapter;->mId:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 441
    goto :goto_0

    .line 444
    :cond_4
    invoke-direct {p0}, Lcom/dsi/ant/adapter/Adapter;->getHardwareType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0}, Lcom/dsi/ant/adapter/Adapter;->getHardwareType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 445
    goto :goto_0

    .line 448
    :cond_5
    invoke-direct {p0}, Lcom/dsi/ant/adapter/Adapter;->getChipName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0}, Lcom/dsi/ant/adapter/Adapter;->getChipName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 449
    goto :goto_0
.end method

.method public final getCapabilities()Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    return-object v0
.end method

.method public final getFirmwareVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersion:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    return-object v0
.end method

.method public final getFirmwareVersionCapabilities()Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    return-object v0
.end method

.method public final getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->getPerChannelHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v0

    return-object v0
.end method

.method public final getState()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method public final getWholeChipHandle()Lcom/dsi/ant/adapter/AdapterHandle;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mGateKeeper:Lcom/dsi/ant/adapter/AdapterGatekeeper;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterGatekeeper;->getWholeChipHandle()Lcom/dsi/ant/adapter/AdapterHandle;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 414
    iget v1, p0, Lcom/dsi/ant/adapter/Adapter;->mId:I

    add-int/lit16 v0, v1, 0xd9

    .line 418
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    invoke-direct {p0}, Lcom/dsi/ant/adapter/Adapter;->getHardwareType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 419
    mul-int/lit8 v1, v0, 0x1f

    invoke-direct {p0}, Lcom/dsi/ant/adapter/Adapter;->getChipName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    .line 421
    return v1
.end method

.method final onMessageReceived([B)V
    .locals 2
    .param p1, "message"    # [B

    .prologue
    .line 366
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 369
    :cond_0
    return-void
.end method

.method final onStateChanged(Lcom/dsi/ant/adapter/AntAdapterState;)V
    .locals 2
    .param p1, "newState"    # Lcom/dsi/ant/adapter/AntAdapterState;

    .prologue
    .line 382
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    if-eq p1, v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->interruptCommandTx()V

    .line 388
    :cond_0
    sget-object v0, Lcom/dsi/ant/adapter/AntAdapterState;->INITIALIZING:Lcom/dsi/ant/adapter/AntAdapterState;

    if-ne p1, v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChannelCloseController;->shutdown()V

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    if-eqz v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 395
    :cond_2
    return-void
.end method

.method public final reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->reinitialize()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v0

    return-object v0
.end method

.method final resetChannelStatus$1385ff()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelsStatus:Lcom/dsi/ant/adapter/ChannelsStatus;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/adapter/ChannelsStatus;->resetChannelStatus(Z)V

    .line 345
    return-void
.end method

.method final setCapabilities(Lcom/dsi/ant/message/fromant/CapabilitiesMessage;)V
    .locals 0
    .param p1, "capabilities"    # Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/dsi/ant/adapter/Adapter;->mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .line 349
    return-void
.end method

.method final setChannelStatusActive(I)V
    .locals 1
    .param p1, "channelNumber"    # I

    .prologue
    .line 332
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelsStatus:Lcom/dsi/ant/adapter/ChannelsStatus;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/ChannelsStatus;->setChannelActive(I)V

    .line 333
    return-void
.end method

.method final setChannelStatusInactive(I)V
    .locals 1
    .param p1, "channelNumber"    # I

    .prologue
    .line 328
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelsStatus:Lcom/dsi/ant/adapter/ChannelsStatus;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/ChannelsStatus;->setChannelInactive(I)V

    .line 329
    return-void
.end method

.method final setFirmwareVersion(Lcom/dsi/ant/message/fromant/AntVersionMessage;)V
    .locals 2
    .param p1, "firmwareVersion"    # Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .prologue
    .line 352
    iput-object p1, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersion:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 354
    new-instance v0, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    iget-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersion:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-direct {v0, v1}, Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;-><init>(Lcom/dsi/ant/message/fromant/AntVersionMessage;)V

    iput-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersionCapabilities:Lcom/dsi/ant/adapter/FirmwareVersionCapabilities;

    .line 355
    return-void
.end method

.method final setupChannelClosedController()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

    iget-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/adapter/ChannelCloseController;->initializeController(Lcom/dsi/ant/message/fromant/CapabilitiesMessage;)V

    .line 341
    :cond_0
    return-void
.end method

.method public final shutdown(Z)V
    .locals 2
    .param p1, "doDisable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 236
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/PowerControl;->shutdown(Z)V

    .line 238
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelsStatus:Lcom/dsi/ant/adapter/ChannelsStatus;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChannelsStatus;->destroy()V

    .line 239
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mChannelCloseController:Lcom/dsi/ant/adapter/ChannelCloseController;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChannelCloseController;->shutdown()V

    .line 241
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->chipReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipReceiver;->shutdown()V

    .line 242
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/MessageTrafficControl;->shutdown()V

    .line 243
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->shutdown()V

    .line 245
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;->destroy()V

    .line 247
    iput-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mCallbackHandler:Lcom/dsi/ant/adapter/Adapter$ChipCallbackHandler;

    .line 248
    iput-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mCapabilities:Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .line 249
    iput-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->mFirmwareVersion:Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .line 250
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 251
    return-void
.end method

.method public final startup()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 217
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->chipReceiver:Lcom/dsi/ant/adapter/ChipReceiver;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/ChipReceiver;->init()V

    .line 218
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/AdapterStateMachine;->init()V

    .line 219
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->powerControl:Lcom/dsi/ant/adapter/PowerControl;

    invoke-virtual {v0}, Lcom/dsi/ant/adapter/PowerControl;->init()V

    .line 220
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 221
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/dsi/ant/adapter/Adapter;->getHardwareType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/dsi/ant/adapter/Adapter;->getChipName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dsi/ant/adapter/Adapter;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final txBurst(I[B)Z
    .locals 1
    .param p1, "channel"    # I
    .param p2, "data"    # [B

    .prologue
    .line 295
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txBurst(I[B)Z

    move-result v0

    return v0
.end method

.method public final txCommand([B)[B
    .locals 3
    .param p1, "message"    # [B

    .prologue
    .line 273
    const/4 v0, 0x0

    .line 279
    .local v0, "response":[B
    sget-object v1, Lcom/dsi/ant/adapter/AntAdapterState;->ENABLED:Lcom/dsi/ant/adapter/AntAdapterState;

    iget-object v2, p0, Lcom/dsi/ant/adapter/Adapter;->stateMachine:Lcom/dsi/ant/adapter/AdapterStateMachine;

    invoke-virtual {v2}, Lcom/dsi/ant/adapter/AdapterStateMachine;->getState()Lcom/dsi/ant/adapter/AntAdapterState;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 280
    iget-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txCommand([B)[B

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    .line 282
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/adapter/Adapter;->TAG:Ljava/lang/String;

    const-string v2, "Adapter state is not ENABLED, not sending command."

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final txData([B)Z
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 290
    iget-object v0, p0, Lcom/dsi/ant/adapter/Adapter;->messageTrafficControl:Lcom/dsi/ant/adapter/MessageTrafficControl;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/adapter/MessageTrafficControl;->txData([B)Z

    move-result v0

    return v0
.end method
