.class public Lcom/dsi/ant/AntMessageBuilder;
.super Ljava/lang/Object;
.source "AntMessageBuilder.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/dsi/ant/AntMessageBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getANTAddChannelId(BIBBB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "deviceNumber"    # I
    .param p2, "deviceType"    # B
    .param p3, "txType"    # B
    .param p4, "listIndex"    # B

    .prologue
    const/4 v3, 0x6

    .line 214
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTAddChannelId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 216
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x59

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    const/4 v1, 0x3

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    const v2, 0xff00

    and-int/2addr v2, p1

    ushr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    aput-byte p2, v0, v1

    aput-byte p3, v0, v3

    const/4 v1, 0x7

    aput-byte p4, v0, v1

    .line 221
    return-object v0
.end method

.method public static getANTAdvanceBurstConfigurationMessage$480d76b0()[B
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 308
    const/16 v0, 0xb

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 314
    return-object v0

    .line 308
    nop

    :array_0
    .array-data 1
        0x9t
        0x78t
        0x0t
        0x1t
        0x3t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method

.method public static getANTAssignChannel(BBB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "channelType"    # B
    .param p2, "networkNumber"    # B

    .prologue
    const/4 v3, 0x3

    .line 58
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTAssignChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 60
    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x42

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    aput-byte p1, v0, v3

    const/4 v1, 0x4

    aput-byte p2, v0, v1

    .line 61
    return-object v0
.end method

.method public static getANTCloseChannel(B)[B
    .locals 3
    .param p0, "channelNumber"    # B

    .prologue
    const/4 v2, 0x1

    .line 242
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTCloseChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 244
    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    const/16 v1, 0x4c

    aput-byte v1, v0, v2

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    .line 245
    return-object v0
.end method

.method public static getANTConfigList(BBB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "listSize"    # B
    .param p2, "exclude"    # B

    .prologue
    const/4 v3, 0x3

    .line 226
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTConfigList: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 228
    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x5a

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    aput-byte p1, v0, v3

    const/4 v1, 0x4

    aput-byte p2, v0, v1

    .line 229
    return-object v0
.end method

.method public static getANTOpenChannel(B)[B
    .locals 3
    .param p0, "channelNumber"    # B

    .prologue
    const/4 v2, 0x1

    .line 234
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTOpenChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 236
    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    const/16 v1, 0x4b

    aput-byte v1, v0, v2

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    .line 237
    return-object v0
.end method

.method public static getANTRequestMessage(BB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "messageID"    # B

    .prologue
    const/4 v3, 0x2

    .line 250
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTRequestMessage: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 252
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x4d

    aput-byte v2, v0, v1

    aput-byte p0, v0, v3

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 253
    return-object v0
.end method

.method public static getANTResetSystem()[B
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 1
        0x1t
        0x4at
        0x0t
    .end array-data
.end method

.method public static getANTSendAcknowledgedData(B[B)[B
    .locals 7
    .param p0, "channelNumber"    # B
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSendAcknowledgedData: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 272
    const/16 v0, 0xb

    new-array v0, v0, [B

    const/16 v1, 0x9

    aput-byte v1, v0, v2

    const/16 v1, 0x4f

    aput-byte v1, v0, v3

    aput-byte p0, v0, v4

    aget-byte v1, p1, v2

    aput-byte v1, v0, v5

    aget-byte v1, p1, v3

    aput-byte v1, v0, v6

    const/4 v1, 0x5

    aget-byte v2, p1, v4

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    aget-byte v2, p1, v5

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    aget-byte v2, p1, v6

    aput-byte v2, v0, v1

    const/16 v1, 0x8

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xa

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    .line 277
    return-object v0
.end method

.method public static getANTSendBroadcastData(B[B)[B
    .locals 7
    .param p0, "channelNumber"    # B
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 258
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSendBroadcastData: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 260
    const/16 v0, 0xb

    new-array v0, v0, [B

    const/16 v1, 0x9

    aput-byte v1, v0, v2

    const/16 v1, 0x4e

    aput-byte v1, v0, v3

    aput-byte p0, v0, v4

    aget-byte v1, p1, v2

    aput-byte v1, v0, v5

    aget-byte v1, p1, v3

    aput-byte v1, v0, v6

    const/4 v1, 0x5

    aget-byte v2, p1, v4

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    aget-byte v2, p1, v5

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    aget-byte v2, p1, v6

    aput-byte v2, v0, v1

    const/16 v1, 0x8

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xa

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    .line 265
    return-object v0
.end method

.method public static getANTSendBurstTransferPacket(B[B)[B
    .locals 5
    .param p0, "control"    # B
    .param p1, "txBuffer"    # [B

    .prologue
    const/16 v2, 0xb

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 282
    sget-object v1, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 284
    new-array v0, v2, [B

    fill-array-data v0, :array_0

    .line 286
    .local v0, "burstAntPacket":[B
    const/4 v1, 0x2

    aput-byte p0, v0, v1

    .line 287
    invoke-static {v0, v4, v2, v3}, Ljava/util/Arrays;->fill([BIIB)V

    .line 288
    const/16 v1, 0x8

    array-length v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 290
    return-object v0

    .line 284
    nop

    :array_0
    .array-data 1
        0x9t
        0x50t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public static getANTSetChannelId(BIBB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "deviceNumber"    # I
    .param p2, "deviceType"    # B
    .param p3, "txType"    # B

    .prologue
    const/4 v3, 0x5

    .line 66
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 68
    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x51

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    const/4 v1, 0x3

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    const v2, 0xff00

    and-int/2addr v2, p1

    ushr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    aput-byte p2, v0, v3

    const/4 v1, 0x6

    aput-byte p3, v0, v1

    .line 73
    return-object v0
.end method

.method public static getANTSetChannelPeriod(BI)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "channelPeriod"    # I

    .prologue
    const/4 v3, 0x3

    .line 90
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelPeriod: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 92
    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x43

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    const/4 v1, 0x4

    const v2, 0xff00

    and-int/2addr v2, p1

    ushr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 95
    return-object v0
.end method

.method public static getANTSetChannelRFFreq(BB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "radioFrequency"    # B

    .prologue
    const/4 v3, 0x2

    .line 100
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelRFFreq: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 102
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x45

    aput-byte v2, v0, v1

    aput-byte p0, v0, v3

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 103
    return-object v0
.end method

.method public static getANTSetChannelSearchTimeout(BB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "searchTimeout"    # B

    .prologue
    const/4 v3, 0x2

    .line 108
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelSearchTimeout: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 110
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x44

    aput-byte v2, v0, v1

    aput-byte p0, v0, v3

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 111
    return-object v0
.end method

.method public static getANTSetChannelTxPower(BB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "txPower"    # B

    .prologue
    const/4 v3, 0x2

    .line 170
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetChannelTxPower: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 172
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x60

    aput-byte v2, v0, v1

    aput-byte p0, v0, v3

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 173
    return-object v0
.end method

.method public static getANTSetEventBuffering(SS)[B
    .locals 5
    .param p0, "timerInterval"    # S
    .param p1, "maxBufferLength"    # S

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 131
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 133
    const/16 v0, 0x8

    new-array v0, v0, [B

    aput-byte v4, v0, v3

    const/4 v1, 0x1

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte v3, v0, v1

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    const/4 v1, 0x4

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    const/4 v1, 0x7

    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 142
    return-object v0
.end method

.method public static getANTSetLowPriorityChannelSearchTimeout(BB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "searchTimeout"    # B

    .prologue
    const/4 v3, 0x2

    .line 116
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetLowPriorityChannelSearchTimeout: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 118
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x63

    aput-byte v2, v0, v1

    aput-byte p0, v0, v3

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 119
    return-object v0
.end method

.method public static getANTSetProximitySearch(BB)[B
    .locals 4
    .param p0, "channelNumber"    # B
    .param p1, "searchThreshold"    # B

    .prologue
    const/4 v3, 0x2

    .line 162
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTSetProximitySearch: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 164
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x71

    aput-byte v2, v0, v1

    aput-byte p0, v0, v3

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 165
    return-object v0
.end method

.method public static getANTSetTxPower$25251d6()[B
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 180
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 181
    return-object v0

    .line 180
    :array_0
    .array-data 1
        0x2t
        0x47t
        0x0t
        0x3t
    .end array-data
.end method

.method public static getANTStartupMessage(B)[B
    .locals 3
    .param p0, "resetType"    # B

    .prologue
    const/4 v2, 0x1

    .line 36
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    const/16 v1, 0x6f

    aput-byte v1, v0, v2

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    return-object v0
.end method

.method public static getANTUnassignChannel(B)[B
    .locals 3
    .param p0, "channelNumber"    # B

    .prologue
    const/4 v2, 0x1

    .line 50
    sget-object v0, Lcom/dsi/ant/AntMessageBuilder;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANTUnassignChannel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 52
    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    const/16 v1, 0x41

    aput-byte v1, v0, v2

    const/4 v1, 0x2

    aput-byte p0, v0, v1

    .line 53
    return-object v0
.end method
