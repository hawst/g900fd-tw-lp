.class public abstract Lcom/dsi/ant/IAnt_6$Stub;
.super Landroid/os/Binder;
.source "IAnt_6.java"

# interfaces
.implements Lcom/dsi/ant/IAnt_6;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/IAnt_6;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p0, p0, v0}, Lcom/dsi/ant/IAnt_6$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 410
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 42
    :sswitch_0
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->enable()Z

    move-result v6

    .line 49
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    if-eqz v6, :cond_0

    move v0, v7

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    move v0, v8

    goto :goto_1

    .line 55
    .end local v6    # "_result":Z
    :sswitch_2
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->disable()Z

    move-result v6

    .line 57
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 58
    if-eqz v6, :cond_1

    move v8, v7

    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 63
    .end local v6    # "_result":Z
    :sswitch_3
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->isEnabled()Z

    move-result v6

    .line 65
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    if-eqz v6, :cond_2

    move v8, v7

    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 71
    .end local v6    # "_result":Z
    :sswitch_4
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 74
    .local v1, "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt_6$Stub;->ANTTxMessage([B)Z

    move-result v6

    .line 75
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    if-eqz v6, :cond_3

    move v8, v7

    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 81
    .end local v1    # "_arg0":[B
    .end local v6    # "_result":Z
    :sswitch_5
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->ANTResetSystem()Z

    move-result v6

    .line 83
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    if-eqz v6, :cond_4

    move v8, v7

    :cond_4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 89
    .end local v6    # "_result":Z
    :sswitch_6
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 92
    .local v1, "_arg0":B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt_6$Stub;->ANTUnassignChannel(B)Z

    move-result v6

    .line 93
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 94
    if-eqz v6, :cond_5

    move v8, v7

    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 99
    .end local v1    # "_arg0":B
    .end local v6    # "_result":Z
    :sswitch_7
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 103
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 105
    .local v2, "_arg1":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 106
    .local v3, "_arg2":B
    invoke-virtual {p0, v1, v2, v3}, Lcom/dsi/ant/IAnt_6$Stub;->ANTAssignChannel(BBB)Z

    move-result v6

    .line 107
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 108
    if-eqz v6, :cond_6

    move v8, v7

    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 113
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v3    # "_arg2":B
    .end local v6    # "_result":Z
    :sswitch_8
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 117
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 119
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 121
    .restart local v3    # "_arg2":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v4

    .line 122
    .local v4, "_arg3":B
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetChannelId(BIBB)Z

    move-result v6

    .line 123
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 124
    if-eqz v6, :cond_7

    move v8, v7

    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 129
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":B
    .end local v4    # "_arg3":B
    .end local v6    # "_result":Z
    :sswitch_9
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 133
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 134
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetChannelPeriod(BI)Z

    move-result v6

    .line 135
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 136
    if-eqz v6, :cond_8

    move v8, v7

    :cond_8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 141
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":I
    .end local v6    # "_result":Z
    :sswitch_a
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 145
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 146
    .local v2, "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetChannelRFFreq(BB)Z

    move-result v6

    .line 147
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    if-eqz v6, :cond_9

    move v8, v7

    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 153
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_b
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 157
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 158
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetChannelSearchTimeout(BB)Z

    move-result v6

    .line 159
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    if-eqz v6, :cond_a

    move v8, v7

    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 165
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_c
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 169
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 170
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetLowPriorityChannelSearchTimeout(BB)Z

    move-result v6

    .line 171
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 172
    if-eqz v6, :cond_b

    move v8, v7

    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 177
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_d
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 181
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 182
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetProximitySearch(BB)Z

    move-result v6

    .line 183
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    if-eqz v6, :cond_c

    move v8, v7

    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 189
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_e
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 193
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 194
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSetChannelTxPower(BB)Z

    move-result v6

    .line 195
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 196
    if-eqz v6, :cond_d

    move v8, v7

    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 201
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_f
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 205
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 207
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 209
    .restart local v3    # "_arg2":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v4

    .line 211
    .restart local v4    # "_arg3":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v5

    .local v5, "_arg4":B
    move-object v0, p0

    .line 212
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/IAnt_6$Stub;->ANTAddChannelId(BIBBB)Z

    move-result v6

    .line 213
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 214
    if-eqz v6, :cond_e

    move v8, v7

    :cond_e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 219
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":B
    .end local v4    # "_arg3":B
    .end local v5    # "_arg4":B
    .end local v6    # "_result":Z
    :sswitch_10
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 223
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 225
    .local v2, "_arg1":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 226
    .restart local v3    # "_arg2":B
    invoke-virtual {p0, v1, v2, v3}, Lcom/dsi/ant/IAnt_6$Stub;->ANTConfigList(BBB)Z

    move-result v6

    .line 227
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 228
    if-eqz v6, :cond_f

    move v8, v7

    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 233
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v3    # "_arg2":B
    .end local v6    # "_result":Z
    :sswitch_11
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 235
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 236
    .restart local v1    # "_arg0":B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt_6$Stub;->ANTOpenChannel(B)Z

    move-result v6

    .line 237
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 238
    if-eqz v6, :cond_10

    move v8, v7

    :cond_10
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 243
    .end local v1    # "_arg0":B
    .end local v6    # "_result":Z
    :sswitch_12
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 246
    .restart local v1    # "_arg0":B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt_6$Stub;->ANTCloseChannel(B)Z

    move-result v6

    .line 247
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 248
    if-eqz v6, :cond_11

    move v8, v7

    :cond_11
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 253
    .end local v1    # "_arg0":B
    .end local v6    # "_result":Z
    :sswitch_13
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 257
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 258
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTRequestMessage(BB)Z

    move-result v6

    .line 259
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v6, :cond_12

    move v8, v7

    :cond_12
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 265
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_14
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 269
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 270
    .local v2, "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSendBroadcastData(B[B)Z

    move-result v6

    .line 271
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 272
    if-eqz v6, :cond_13

    move v8, v7

    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 277
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":Z
    :sswitch_15
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 279
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 281
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 282
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSendAcknowledgedData(B[B)Z

    move-result v6

    .line 283
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 284
    if-eqz v6, :cond_14

    move v8, v7

    :cond_14
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 289
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":Z
    :sswitch_16
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 291
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 293
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 294
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSendBurstTransferPacket(B[B)Z

    move-result v6

    .line 295
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 296
    if-eqz v6, :cond_15

    move v8, v7

    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 301
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":Z
    :sswitch_17
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 305
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 306
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt_6$Stub;->ANTSendBurstTransfer(B[B)I

    move-result v6

    .line 307
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 308
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 313
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":I
    :sswitch_18
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 315
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 317
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 319
    .restart local v2    # "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 321
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    move v4, v7

    .line 322
    .local v4, "_arg3":Z
    :goto_2
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/dsi/ant/IAnt_6$Stub;->ANTTransmitBurst(B[BIZ)I

    move-result v6

    .line 323
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 324
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v4    # "_arg3":Z
    .end local v6    # "_result":I
    :cond_16
    move v4, v8

    .line 321
    goto :goto_2

    .line 329
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v3    # "_arg2":I
    :sswitch_19
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 331
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 333
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 335
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 337
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 338
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/dsi/ant/IAnt_6$Stub;->ANTConfigEventBuffering(IIII)Z

    move-result v6

    .line 339
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 340
    if-eqz v6, :cond_17

    move v8, v7

    :cond_17
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 345
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v6    # "_result":Z
    :sswitch_1a
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->ANTDisableEventBuffering()Z

    move-result v6

    .line 347
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 348
    if-eqz v6, :cond_18

    move v8, v7

    :cond_18
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 353
    .end local v6    # "_result":Z
    :sswitch_1b
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->getServiceLibraryVersionCode()I

    move-result v6

    .line 355
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 356
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 361
    .end local v6    # "_result":I
    :sswitch_1c
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->getServiceLibraryVersionName()Ljava/lang/String;

    move-result-object v6

    .line 363
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 364
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 369
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_1d
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 370
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->claimInterface()Z

    move-result v6

    .line 371
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 372
    if-eqz v6, :cond_19

    move v8, v7

    :cond_19
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 377
    .end local v6    # "_result":Z
    :sswitch_1e
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 379
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 380
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt_6$Stub;->requestForceClaimInterface(Ljava/lang/String;)Z

    move-result v6

    .line 381
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 382
    if-eqz v6, :cond_1a

    move v8, v7

    :cond_1a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 387
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_1f
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 388
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->stopRequestForceClaimInterface()Z

    move-result v6

    .line 389
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 390
    if-eqz v6, :cond_1b

    move v8, v7

    :cond_1b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 395
    .end local v6    # "_result":Z
    :sswitch_20
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 396
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->releaseInterface()Z

    move-result v6

    .line 397
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 398
    if-eqz v6, :cond_1c

    move v8, v7

    :cond_1c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 403
    .end local v6    # "_result":Z
    :sswitch_21
    const-string v0, "com.dsi.ant.IAnt_6"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 404
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt_6$Stub;->hasClaimedInterface()Z

    move-result v6

    .line 405
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 406
    if-eqz v6, :cond_1d

    move v8, v7

    :cond_1d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
