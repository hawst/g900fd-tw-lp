.class public Lcom/dsi/ant/AntService;
.super Ljava/lang/Object;
.source "AntService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/AntService$Component;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sServiceVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-class v0, Lcom/dsi/ant/AntService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/AntService;->TAG:Ljava/lang/String;

    .line 124
    const/4 v0, 0x0

    sput v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    return-void
.end method

.method public static requiresBundle()Z
    .locals 2

    .prologue
    .line 150
    sget v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    const v1, 0x9c40

    if-gt v0, v1, :cond_0

    sget v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
