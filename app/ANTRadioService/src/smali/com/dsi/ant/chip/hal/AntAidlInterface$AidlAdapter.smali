.class final Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
.super Ljava/lang/Object;
.source "AntAidlInterface.java"

# interfaces
.implements Lcom/dsi/ant/chip/IAntChip;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/hal/AntAidlInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AidlAdapter"
.end annotation


# instance fields
.field public volatile mCallback:Landroid/os/Messenger;

.field volatile mInvalid:Z

.field final synthetic this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V
    .locals 1

    .prologue
    .line 51
    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;B)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V

    return-void
.end method


# virtual methods
.method public final disable()I
    .locals 2

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "ANTAidlInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/4 v0, 0x5

    .line 128
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # invokes: Lcom/dsi/ant/chip/hal/AntAidlInterface;->disable()I
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$500(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I

    move-result v0

    goto :goto_0
.end method

.method public final enable()I
    .locals 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 112
    const-string v0, "ANTAidlInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x5

    .line 116
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # invokes: Lcom/dsi/ant/chip/hal/AntAidlInterface;->enable()I
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I

    move-result v0

    goto :goto_0
.end method

.method public final getChipName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getChipState()I
    .locals 2

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "ANTAidlInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # invokes: Lcom/dsi/ant/chip/hal/AntAidlInterface;->getEnabledState()I
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$300(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I

    move-result v0

    goto :goto_0
.end method

.method public final getHardwareType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const-string v0, "built-in"

    return-object v0
.end method

.method public final setCallback(Landroid/os/Messenger;)V
    .locals 2
    .param p1, "callback"    # Landroid/os/Messenger;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 67
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mCallback:Landroid/os/Messenger;

    .line 68
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final txBurst(I[B)Z
    .locals 2
    .param p1, "channel"    # I
    .param p2, "data"    # [B

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "ANTAidlInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x0

    .line 92
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-static {}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$200$47bd09e8()Z

    move-result v0

    goto :goto_0
.end method

.method public final txMessage([B)Z
    .locals 2
    .param p1, "message"    # [B

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "ANTAidlInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # invokes: Lcom/dsi/ant/chip/hal/AntAidlInterface;->txMessage([B)Z
    invoke-static {v0, p1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$100(Lcom/dsi/ant/chip/hal/AntAidlInterface;[B)Z

    move-result v0

    goto :goto_0
.end method
