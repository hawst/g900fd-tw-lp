.class final Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;
.super Ljava/lang/Object;
.source "TcpSocketConnection.java"

# interfaces
.implements Lcom/dsi/ant/chip/socket/ISocketConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SocketConnection"
.end annotation


# instance fields
.field private mSocket:Ljava/net/Socket;

.field final synthetic this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)V
    .locals 1

    .prologue
    .line 155
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    .line 157
    return-void
.end method


# virtual methods
.method public final connect()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$200(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Ljava/net/InetAddress;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I
    invoke-static {v2}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$000(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 164
    .local v0, "socketAddress":Ljava/net/InetSocketAddress;
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1}, Ljava/net/Socket;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    .line 165
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    const/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 166
    return-void
.end method

.method public final disconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v0, :cond_2

    .line 173
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    .line 185
    :cond_2
    return-void
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 232
    .local v0, "input":Ljava/io/InputStream;
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_0

    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 244
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 212
    .local v0, "output":Ljava/io/OutputStream;
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_0

    .line 216
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 224
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final isConnected()Z
    .locals 2

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 199
    .local v0, "connected":Z
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    .line 204
    :cond_0
    return v0
.end method

.method public final reconnect()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->disconnect()V

    .line 191
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->connect()V

    .line 192
    return-void
.end method

.method public final saveState()V
    .locals 4

    .prologue
    .line 254
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "TCPSocketPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 255
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 257
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$200(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Ljava/net/InetAddress;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 259
    const-string v1, "IP_ADDRESS"

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    invoke-static {v2}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$200(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 261
    :cond_0
    const-string v1, "PORT"

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I
    invoke-static {v2}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$000(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 263
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 264
    return-void
.end method
