.class public final Lcom/dsi/ant/chip/socket/AntSocketManagerThread;
.super Ljava/lang/Thread;
.source "AntSocketManagerThread.java"


# static fields
.field private static mRxThreadLock:Ljava/lang/Object;


# instance fields
.field private SOCKET_IO_LOCK:Ljava/lang/Object;

.field private SOCKET_RX_CHANGE_LOCK:Ljava/lang/Object;

.field private TX_COMPLETE_LOCK:Ljava/lang/Object;

.field private mConnected:Z

.field private mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

.field private mFlagRxThreadInUse:Z

.field private mInitialising:Z

.field private mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

.field private mSocketInput:Ljava/io/InputStream;

.field private mSocketOutput:Ljava/io/OutputStream;

.field private volatile mTxRetry:Z

.field private volatile mTxSuccess:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mRxThreadLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/chip/socket/AntSocketInterface;Lcom/dsi/ant/chip/socket/ISocketConnection;)V
    .locals 3
    .param p1, "connector"    # Lcom/dsi/ant/chip/socket/AntSocketInterface;
    .param p2, "socket"    # Lcom/dsi/ant/chip/socket/ISocketConnection;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_IO_LOCK:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_RX_CHANGE_LOCK:Ljava/lang/Object;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->TX_COMPLETE_LOCK:Ljava/lang/Object;

    .line 43
    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxSuccess:Z

    .line 44
    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxRetry:Z

    .line 49
    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mFlagRxThreadInUse:Z

    .line 58
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Connector is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    if-nez p2, :cond_1

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    const-string v0, "ANT Local Socket Rx Thread"

    invoke-virtual {p0, v0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->setName(Ljava/lang/String;)V

    .line 70
    iput-object p2, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    .line 71
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    .line 73
    iput-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketOutput:Ljava/io/OutputStream;

    .line 74
    iput-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    .line 76
    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    .line 77
    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnected:Z

    .line 78
    return-void
.end method

.method private connect()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 468
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 470
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    if-nez v0, :cond_0

    .line 472
    const-string v0, "ANTSocketManagerThread"

    const-string v1, "Socket Connection type not set"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    new-instance v0, Ljava/io/IOException;

    const-string v1, "SocketConnection not set"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_IO_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 480
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 484
    monitor-exit v1

    .line 512
    :goto_0
    return-void

    .line 487
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    .line 488
    const-string v0, "ANTSocketManagerThread"

    const-string v2, "ANT initialising..."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    invoke-interface {v0}, Lcom/dsi/ant/chip/socket/ISocketConnection;->connect()V

    .line 492
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 494
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    invoke-interface {v0}, Lcom/dsi/ant/chip/socket/ISocketConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketOutput:Ljava/io/OutputStream;

    .line 496
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_RX_CHANGE_LOCK:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    invoke-interface {v0}, Lcom/dsi/ant/chip/socket/ISocketConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    .line 498
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnected:Z

    .line 500
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 502
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 504
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/dsi/ant/chip/socket/AntSocketManagerThread$1;

    invoke-direct {v1, p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread$1;-><init>(Lcom/dsi/ant/chip/socket/AntSocketManagerThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 498
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 500
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private disconnect()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 516
    iget-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnected:Z

    if-nez v1, :cond_0

    .line 551
    :goto_0
    return-void

    .line 518
    :cond_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 520
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->setThreadReady(Z)V

    .line 522
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_IO_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 524
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnected:Z

    .line 526
    iget-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    if-eqz v1, :cond_1

    .line 528
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    .line 530
    const-string v1, "ANTSocketManagerThread"

    const-string v3, "ANT initialisation CANCELLED"

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    :cond_1
    :try_start_1
    const-string v1, "ANTSocketManagerThread"

    const-string v3, "disconnect: Closing socket"

    invoke-static {v1, v3}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    if-eqz v1, :cond_2

    .line 539
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    invoke-interface {v1}, Lcom/dsi/ant/chip/socket/ISocketConnection;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547
    :cond_2
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_RX_CHANGE_LOCK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 548
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    .line 549
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 550
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketOutput:Ljava/io/OutputStream;

    .line 551
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 542
    :catch_0
    move-exception v0

    .line 544
    .local v0, "e":Ljava/io/IOException;
    :try_start_5
    const-string v1, "ANTSocketManagerThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception thrown during disconnect ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 549
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v1

    monitor-exit v3

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private declared-synchronized reconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 556
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 558
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    invoke-interface {v0}, Lcom/dsi/ant/chip/socket/ISocketConnection;->reconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    :cond_0
    monitor-exit p0

    return-void

    .line 556
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final ANTTxMessage([B)Z
    .locals 10
    .param p1, "message"    # [B

    .prologue
    .line 365
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 367
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 370
    iget-object v6, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_IO_LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 372
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketOutput:Ljava/io/OutputStream;

    if-eqz v5, :cond_3

    .line 375
    array-length v5, p1

    int-to-short v0, v5

    .line 376
    .local v0, "antlen":S
    array-length v5, p1

    add-int/lit8 v5, v5, 0x2

    new-array v2, v5, [B

    .line 377
    .local v2, "hci_msg":[B
    const/4 v5, 0x1

    ushr-int/lit8 v7, v0, 0x8

    int-to-byte v7, v7

    aput-byte v7, v2, v5

    .line 378
    const/4 v5, 0x0

    int-to-byte v7, v0

    aput-byte v7, v2, v5

    .line 379
    const/4 v5, 0x0

    const/4 v7, 0x2

    array-length v8, p1

    invoke-static {p1, v5, v2, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 381
    const/4 v4, 0x0

    .line 385
    .local v4, "txFailRetryCount":I
    :cond_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 387
    iget-object v7, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->TX_COMPLETE_LOCK:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 389
    const/4 v5, 0x0

    :try_start_1
    iput-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxSuccess:Z

    .line 390
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxRetry:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394
    :try_start_2
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 396
    iget-object v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketOutput:Ljava/io/OutputStream;

    invoke-virtual {v5, v2}, Ljava/io/OutputStream;->write([B)V

    .line 397
    iget-object v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketOutput:Ljava/io/OutputStream;

    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    .line 399
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401
    const/4 v4, 0x0

    .line 405
    :try_start_3
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 407
    iget-object v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->TX_COMPLETE_LOCK:Ljava/lang/Object;

    const-wide/16 v8, 0x7d0

    invoke-virtual {v5, v8, v9}, Ljava/lang/Object;->wait(J)V

    .line 409
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 438
    :goto_0
    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 439
    :try_start_5
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 441
    iget-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxRetry:Z

    if-eqz v5, :cond_1

    .line 443
    const-string v5, "ANTSocketManagerThread"

    const-string v7, "ANTTxMessage: Retrying Tx"

    invoke-static {v5, v7}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_1
    iget-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxRetry:Z

    if-nez v5, :cond_0

    .line 453
    .end local v0    # "antlen":S
    .end local v2    # "hci_msg":[B
    .end local v4    # "txFailRetryCount":I
    :goto_1
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 454
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 456
    iget-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxSuccess:Z

    return v5

    .line 411
    .restart local v0    # "antlen":S
    .restart local v2    # "hci_msg":[B
    .restart local v4    # "txFailRetryCount":I
    :catch_0
    move-exception v1

    .line 413
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_6
    const-string v5, "ANTSocketManagerThread"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ANTTxMessage: Tx command complete timeout ("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 416
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 418
    .local v1, "e":Ljava/io/IOException;
    :try_start_7
    const-string v5, "ANTSocketManagerThread"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ANTTxMessage: Send HCI Command failed. "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 420
    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0xa

    if-gt v4, v5, :cond_2

    .line 424
    :try_start_8
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->reconnect()V

    .line 425
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxRetry:Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    .line 427
    :catch_2
    move-exception v3

    .line 429
    .local v3, "reconnectEx":Ljava/io/IOException;
    :try_start_9
    const-string v5, "ANTSocketManagerThread"

    const-string v8, "ANTTxMessage: Could not reconnect ANT socket, aborting transmit."

    invoke-static {v5, v8, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_0

    .line 438
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "reconnectEx":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :try_start_a
    monitor-exit v7

    throw v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 453
    .end local v0    # "antlen":S
    .end local v2    # "hci_msg":[B
    .end local v4    # "txFailRetryCount":I
    :catchall_1
    move-exception v5

    monitor-exit v6

    throw v5

    .line 434
    .restart local v0    # "antlen":S
    .restart local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "hci_msg":[B
    .restart local v4    # "txFailRetryCount":I
    :cond_2
    :try_start_b
    const-string v5, "ANTSocketManagerThread"

    const-string v8, "ANTTxMessage: Send HCI Command failed too many times, aborting transmit."

    invoke-static {v5, v8}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->disconnect()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_0

    .line 450
    .end local v0    # "antlen":S
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "hci_msg":[B
    .end local v4    # "txFailRetryCount":I
    :cond_3
    :try_start_c
    const-string v5, "ANTSocketManagerThread"

    const-string v7, "Tx aborted - socket was set to null."

    invoke-static {v5, v7}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxSuccess:Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_1
.end method

.method public final destroy()V
    .locals 3

    .prologue
    .line 100
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 102
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->interrupt()V

    .line 103
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->disconnect()V

    .line 107
    :try_start_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 109
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->join()V

    .line 111
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 119
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "ANTSocketManagerThread"

    const-string v2, " destroy: main thread was interrupted while waiting for rxThread to terminate"

    invoke-static {v1, v2, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final isConnected()Z
    .locals 2

    .prologue
    .line 128
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "retVal":Z
    iget-boolean v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnected:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocket:Lcom/dsi/ant/chip/socket/ISocketConnection;

    invoke-interface {v1}, Lcom/dsi/ant/chip/socket/ISocketConnection;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const/4 v0, 0x1

    .line 135
    :cond_0
    return v0
.end method

.method public final isInitialising()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    return v0
.end method

.method public final run()V
    .locals 15

    .prologue
    .line 141
    sget-object v10, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mRxThreadLock:Ljava/lang/Object;

    monitor-enter v10

    .line 143
    :try_start_0
    iget-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mFlagRxThreadInUse:Z

    if-eqz v9, :cond_3

    .line 145
    const-string v9, "ANTSocketManagerThread"

    const-string v11, "rxThread was found to be already open while starting this thread"

    invoke-static {v9, v11}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :goto_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 155
    const/4 v8, 0x0

    .line 156
    .local v8, "socketOpen":Z
    const/4 v4, 0x0

    .line 157
    .local v4, "bytesRead":I
    const/4 v9, 0x2

    new-array v6, v9, [B

    .line 158
    .local v6, "hciMessageSizeBuffer":[B
    const/16 v9, 0x20

    new-array v0, v9, [B

    .line 162
    .local v0, "antData":[B
    :try_start_1
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->connect()V

    .line 163
    const/4 v8, 0x1

    .line 165
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 178
    :cond_0
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->interrupted()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4

    move-result v9

    if-nez v9, :cond_4

    .line 182
    if-nez v8, :cond_1

    .line 184
    :try_start_3
    const-string v9, "ANTSocketManagerThread"

    const-string v10, "rxThread: Socket connection closed unnexpectedly.  Attempting reconnection (after 6 second pause)."

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->disconnect()V

    .line 187
    const-wide/16 v9, 0x1770

    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_4

    .line 190
    :try_start_4
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->connect()V

    .line 191
    const/4 v8, 0x1

    .line 193
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4

    .line 201
    :cond_1
    :goto_2
    :try_start_5
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 203
    const/4 v4, 0x0

    .line 204
    :goto_3
    if-eqz v8, :cond_7

    const/4 v9, 0x2

    if-ge v4, v9, :cond_7

    .line 207
    iget-object v10, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_RX_CHANGE_LOCK:Ljava/lang/Object;

    monitor-enter v10
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4

    .line 208
    :try_start_6
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    if-nez v9, :cond_5

    .line 210
    const/4 v8, 0x0

    .line 229
    :goto_4
    monitor-exit v10
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v9

    :try_start_7
    monitor-exit v10

    throw v9
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_4

    .line 336
    :catch_0
    move-exception v5

    .line 339
    .local v5, "e":Ljava/io/IOException;
    :try_start_8
    iget-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnected:Z

    if-eqz v9, :cond_2

    .line 340
    const-string v9, "ANTSocketManagerThread"

    const-string v10, "ANT Radio Service transport error - communication with system service failed"

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v9, "ANTSocketManagerThread"

    const-string v10, "rxThread: AntHalService socket communication failed"

    invoke-static {v9, v10, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 344
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->setThreadReady(Z)V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_4

    .line 347
    :cond_2
    const/4 v8, 0x0

    .line 348
    goto :goto_1

    .line 149
    .end local v0    # "antData":[B
    .end local v4    # "bytesRead":I
    .end local v5    # "e":Ljava/io/IOException;
    .end local v6    # "hciMessageSizeBuffer":[B
    .end local v8    # "socketOpen":Z
    :cond_3
    const/4 v9, 0x1

    :try_start_9
    iput-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mFlagRxThreadInUse:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_0

    .line 151
    :catchall_1
    move-exception v9

    monitor-exit v10

    throw v9

    .line 169
    .restart local v0    # "antData":[B
    .restart local v4    # "bytesRead":I
    .restart local v6    # "hciMessageSizeBuffer":[B
    .restart local v8    # "socketOpen":Z
    :catch_1
    move-exception v9

    const-string v9, "ANTSocketManagerThread"

    const-string v10, "rxThread: Initial connect timed out, trying in loop"

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 173
    :catch_2
    move-exception v9

    const-string v9, "ANTSocketManagerThread"

    const-string v10, "rxThread: Initial connect failed, trying in loop"

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 197
    :catch_3
    move-exception v9

    :try_start_a
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_2

    .line 353
    :catch_4
    move-exception v9

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 354
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->setThreadReady(Z)V

    .line 357
    :cond_4
    invoke-direct {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->disconnect()V

    .line 358
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mFlagRxThreadInUse:Z

    .line 360
    const-string v9, "ANTSocketManagerThread"

    const-string v10, "rxThread: Thread aborted"

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    return-void

    .line 214
    :cond_5
    :try_start_b
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    rsub-int/lit8 v11, v4, 0x2

    invoke-virtual {v9, v6, v4, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    .line 216
    .local v7, "len":I
    const/4 v9, -0x1

    if-eq v9, v7, :cond_6

    .line 218
    add-int/2addr v4, v7

    .line 220
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "rxThread: Read total "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " bytes of message size from socket"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    goto :goto_4

    .line 224
    :cond_6
    const-string v9, "ANTSocketManagerThread"

    const-string v11, "rxThread: Input stream closed while waiting for message size from socket"

    invoke-static {v9, v11}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 226
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 232
    .end local v7    # "len":I
    :cond_7
    if-eqz v8, :cond_0

    .line 234
    const/4 v9, 0x0

    :try_start_c
    aget-byte v9, v6, v9

    and-int/lit16 v9, v9, 0xff

    const/4 v10, 0x1

    aget-byte v10, v6, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    add-int v2, v9, v10

    .line 236
    .local v2, "antMessageSize":I
    array-length v9, v0

    if-le v2, v9, :cond_8

    .line 238
    new-array v0, v2, [B

    .line 239
    const-string v9, "ANTSocketManagerThread"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "rxThread: got ANT message larger than current buffer, making new buffer["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_8
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "rxThread: Message size = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 244
    const/4 v4, 0x0

    .line 245
    :goto_5
    if-eqz v8, :cond_b

    if-ge v4, v2, :cond_b

    .line 248
    iget-object v10, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->SOCKET_RX_CHANGE_LOCK:Ljava/lang/Object;

    monitor-enter v10
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_4

    .line 249
    :try_start_d
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    if-nez v9, :cond_9

    .line 251
    const/4 v8, 0x0

    .line 270
    :goto_6
    monitor-exit v10
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto :goto_5

    :catchall_2
    move-exception v9

    :try_start_e
    monitor-exit v10

    throw v9
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_4

    .line 255
    :cond_9
    :try_start_f
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mSocketInput:Ljava/io/InputStream;

    sub-int v11, v2, v4

    invoke-virtual {v9, v0, v4, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    .line 257
    .restart local v7    # "len":I
    const/4 v9, -0x1

    if-eq v9, v7, :cond_a

    .line 259
    add-int/2addr v4, v7

    .line 261
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "rxThread: Read total "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " bytes of message from socket"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    goto :goto_6

    .line 265
    :cond_a
    const-string v9, "ANTSocketManagerThread"

    const-string v11, "rxThread: Input stream closed while reading message data from socket"

    invoke-static {v9, v11}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 267
    const/4 v8, 0x0

    goto :goto_6

    .line 273
    .end local v7    # "len":I
    :cond_b
    if-eqz v8, :cond_0

    .line 275
    const/16 v9, -0x38

    const/4 v10, 0x1

    :try_start_10
    aget-byte v10, v0, v10

    if-ne v9, v10, :cond_f

    .line 277
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 279
    iget-object v10, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->TX_COMPLETE_LOCK:Ljava/lang/Object;

    monitor-enter v10
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_4

    .line 281
    const/4 v9, 0x2

    :try_start_11
    aget-byte v3, v0, v9

    .line 283
    .local v3, "antResponse":B
    if-nez v3, :cond_c

    const/4 v9, 0x1

    :goto_7
    iput-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxSuccess:Z

    .line 285
    iget-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxSuccess:Z

    if-eqz v9, :cond_d

    .line 287
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 303
    :goto_8
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->TX_COMPLETE_LOCK:Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/lang/Object;->notify()V

    .line 304
    monitor-exit v10
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 305
    :try_start_12
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_4

    goto/16 :goto_1

    .line 283
    :cond_c
    const/4 v9, 0x0

    goto :goto_7

    .line 291
    :cond_d
    const/16 v9, 0x1f

    if-ne v9, v3, :cond_e

    .line 293
    const/4 v9, 0x1

    :try_start_13
    iput-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mTxRetry:Z

    .line 295
    const-string v9, "ANTSocketManagerThread"

    const-string v11, "rxThread: Command complete: ANT buffer full, retry send command"

    invoke-static {v9, v11}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    goto :goto_8

    .line 304
    .end local v3    # "antResponse":B
    :catchall_3
    move-exception v9

    :try_start_14
    monitor-exit v10

    throw v9
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_4

    .line 299
    .restart local v3    # "antResponse":B
    :cond_e
    :try_start_15
    const-string v9, "ANTSocketManagerThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "rxThread: Command complete: Received ANT response = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    goto :goto_8

    .line 307
    .end local v3    # "antResponse":B
    :cond_f
    :try_start_16
    iget-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    if-eqz v9, :cond_11

    .line 309
    const/16 v9, 0x3e

    const/4 v10, 0x1

    aget-byte v10, v0, v10

    if-ne v9, v10, :cond_10

    .line 311
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 313
    const-string v9, "ANTSocketManagerThread"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "ANT Version: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v11, Ljava/lang/String;

    const/4 v12, 0x2

    const/4 v13, 0x0

    aget-byte v13, v0, v13

    const-string v14, "UTF-8"

    invoke-direct {v11, v0, v12, v13, v14}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mInitialising:Z

    .line 317
    const-string v9, "ANTSocketManagerThread"

    const-string v10, "...ANT initialised."

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->setThreadReady(Z)V

    goto/16 :goto_1

    .line 323
    :cond_10
    const-string v9, "ANTSocketManagerThread"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "rxThread: Received unexpected message while initializing. ID=0x"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x1

    aget-byte v11, v0, v11

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 328
    :cond_11
    new-array v1, v2, [B

    .line 329
    .local v1, "antDataMessage":[B
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v0, v9, v1, v10, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 331
    iget-object v9, p0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->mConnector:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    iget-object v10, v9, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v10
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_16} :catch_4

    :try_start_17
    iget-object v11, v9, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    if-eqz v11, :cond_12

    :try_start_18
    iget-object v9, v9, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    iget-object v9, v9, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mCallback:Landroid/os/Messenger;

    invoke-static {v9, v1}, Lcom/dsi/ant/chip/ChipCallbackHelper;->sendRxMessage(Landroid/os/Messenger;[B)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_5
    .catchall {:try_start_18 .. :try_end_18} :catchall_4

    :cond_12
    :goto_9
    :try_start_19
    monitor-exit v10
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_4

    goto/16 :goto_1

    :catchall_4
    move-exception v9

    :try_start_1a
    monitor-exit v10

    throw v9
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1a .. :try_end_1a} :catch_4

    :catch_5
    move-exception v9

    :try_start_1b
    const-string v11, "ANTSocketInterface"

    const-string v12, "Impossible Remote Exception."

    invoke-static {v11, v12, v9}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_4

    goto :goto_9
.end method

.method public final declared-synchronized start()V
    .locals 1

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 85
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 89
    invoke-super {p0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    monitor-exit p0

    return-void

    .line 93
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
