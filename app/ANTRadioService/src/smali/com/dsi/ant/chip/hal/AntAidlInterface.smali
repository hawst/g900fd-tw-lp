.class public final Lcom/dsi/ant/chip/hal/AntAidlInterface;
.super Ljava/lang/Object;
.source "AntAidlInterface.java"

# interfaces
.implements Lcom/dsi/ant/chip/IAntChipDetector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    }
.end annotation


# static fields
.field private static sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;


# instance fields
.field private mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

.field private final mAidlInterfaceLock:Ljava/lang/Object;

.field private final mAntHalCallback:Lcom/dsi/ant/server/IAntHalCallback$Stub;

.field private mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

.field private mAntHalServiceConnected:Z

.field private mAntHalServiceConnecting:Z

.field private mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

.field private final mDisablingExpiredEvent:Ljava/lang/Runnable;

.field private final mEnablingExpiredEvent:Ljava/lang/Runnable;

.field private mEnablingSent:Z

.field private mHandler:Landroid/os/Handler;

.field private mIAntHalConnection:Landroid/content/ServiceConnection;

.field mOverrideDisabling:Z

.field mOverrideEnabling:Z

.field private final mOverrideState_LOCK:Ljava/lang/Object;

.field private mPreviousState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;

    .line 179
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    .line 185
    iput v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mPreviousState:I

    .line 194
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    .line 278
    new-instance v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface$1;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingExpiredEvent:Ljava/lang/Runnable;

    .line 301
    new-instance v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface$2;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mDisablingExpiredEvent:Ljava/lang/Runnable;

    .line 324
    new-instance v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mIAntHalConnection:Landroid/content/ServiceConnection;

    .line 391
    new-instance v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalCallback:Lcom/dsi/ant/server/IAntHalCallback$Stub;

    .line 201
    iput-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    .line 202
    iput-boolean v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    .line 204
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    .line 208
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    iput-boolean v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingSent:Z

    .line 212
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 214
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 215
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 217
    sget-object v1, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 219
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnecting:Z

    .line 221
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 215
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 221
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/chip/hal/AntAidlInterface;[B)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;
    .param p1, "x1"    # [B

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->txMessage([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/dsi/ant/chip/hal/AntAidlInterface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    return p1
.end method

.method static synthetic access$1202$5ff94f9e(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnecting:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/server/IAntHalCallback$Stub;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalCallback:Lcom/dsi/ant/server/IAntHalCallback$Stub;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/dsi/ant/chip/hal/AntAidlInterface;Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;
    .param p1, "x1"    # Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/IAntChipDetectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mPreviousState:I

    return v0
.end method

.method static synthetic access$1800(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->clearStateOverrides()V

    return-void
.end method

.method static synthetic access$1900(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingSent:Z

    return v0
.end method

.method static synthetic access$1902$5ff94f9e(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingSent:Z

    return v0
.end method

.method static synthetic access$200$47bd09e8()Z
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use concatenated burst messages with txMessage instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic access$2000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->getEnabledState()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->enable()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->disable()I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/chip/hal/AntAidlInterface;I)V
    .locals 4
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    if-nez v0, :cond_0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    iget-object v0, v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mCallback:Landroid/os/Messenger;

    iget v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mPreviousState:I

    invoke-static {v0, v2, p1}, Lcom/dsi/ant/chip/ChipCallbackHelper;->sendStateUpdate(Landroid/os/Messenger;II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    iput p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mPreviousState:I

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "ANTAidlInterface"

    const-string v3, "Impossible remote exception."

    invoke-static {v2, v3, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method static synthetic access$900(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/server/IAntHal;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    return-object v0
.end method

.method static synthetic access$902(Lcom/dsi/ant/chip/hal/AntAidlInterface;Lcom/dsi/ant/server/IAntHal;)Lcom/dsi/ant/server/IAntHal;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/hal/AntAidlInterface;
    .param p1, "x1"    # Lcom/dsi/ant/server/IAntHal;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    return-object p1
.end method

.method private clearStateOverrides()V
    .locals 3

    .prologue
    .line 607
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 609
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    .line 610
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mDisablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 611
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    .line 612
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 613
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private disable()I
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x1

    .line 763
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 765
    const/4 v3, 0x0

    .line 767
    .local v3, "resultState":I
    sget-object v5, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 769
    :try_start_0
    iget-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    if-eqz v4, :cond_2

    .line 771
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->getEnabledState()I

    move-result v3

    .line 772
    const/4 v1, -0x1

    .line 775
    .local v1, "disableResult":I
    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 777
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    .line 778
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mDisablingExpiredEvent:Ljava/lang/Runnable;

    const-wide/16 v8, 0xbb8

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 779
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 782
    :try_start_2
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    const/4 v6, 0x4

    invoke-interface {v4, v6}, Lcom/dsi/ant/server/IAntHal;->setAntState(I)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    .line 789
    :goto_0
    if-ne v10, v1, :cond_1

    .line 790
    :try_start_3
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->getEnabledState()I

    move-result v0

    .line 792
    .local v0, "currentState":I
    if-ne v11, v0, :cond_0

    .line 793
    move v3, v0

    .line 796
    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 798
    const/4 v4, 0x0

    :try_start_4
    iput-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    .line 799
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mDisablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 800
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 818
    .end local v0    # "currentState":I
    .end local v1    # "disableResult":I
    :goto_1
    :try_start_5
    monitor-exit v5

    .line 820
    return v3

    .line 779
    .restart local v1    # "disableResult":I
    :catchall_0
    move-exception v4

    monitor-exit v6

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 818
    .end local v1    # "disableResult":I
    :catchall_1
    move-exception v4

    monitor-exit v5

    throw v4

    .line 783
    .restart local v1    # "disableResult":I
    :catch_0
    move-exception v2

    .line 784
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_6
    const-string v4, "ANTAidlInterface"

    const-string v6, "Ant Service Disable Failed"

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    const-string v4, "ANTAidlInterface"

    const-string v6, "Ant Service Disable failure reason"

    invoke-static {v4, v6, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 800
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "currentState":I
    :catchall_2
    move-exception v4

    monitor-exit v6

    throw v4

    .line 803
    :cond_0
    const/4 v3, 0x3

    .line 805
    goto :goto_1

    .line 807
    .end local v0    # "currentState":I
    :cond_1
    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 809
    const/4 v4, 0x0

    :try_start_7
    iput-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    .line 810
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mDisablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 811
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v4

    :try_start_8
    monitor-exit v6

    throw v4

    .line 813
    .end local v1    # "disableResult":I
    :cond_2
    const-string v4, "ANTAidlInterface"

    const-string v6, "ANT HAL Service was not connected"

    invoke-static {v4, v6}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1
.end method

.method private enable()I
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 704
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 706
    const/4 v3, 0x0

    .line 708
    .local v3, "resultState":I
    sget-object v5, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 710
    :try_start_0
    iget-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    if-eqz v4, :cond_0

    .line 712
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->getEnabledState()I

    move-result v3

    .line 713
    const/4 v2, -0x1

    .line 717
    .local v2, "enableResult":I
    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 719
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    .line 720
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingExpiredEvent:Ljava/lang/Runnable;

    const-wide/16 v8, 0xbb8

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 721
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 724
    :try_start_2
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Lcom/dsi/ant/server/IAntHal;->setAntState(I)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    .line 731
    :goto_0
    if-ne v10, v2, :cond_2

    .line 732
    :try_start_3
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->getEnabledState()I

    move-result v0

    .line 734
    .local v0, "currentState":I
    if-ne v11, v0, :cond_1

    .line 735
    move v3, v0

    .line 738
    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 740
    const/4 v4, 0x0

    :try_start_4
    iput-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    .line 741
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 742
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 756
    .end local v0    # "currentState":I
    .end local v2    # "enableResult":I
    :cond_0
    :goto_1
    :try_start_5
    monitor-exit v5

    .line 758
    return v3

    .line 721
    .restart local v2    # "enableResult":I
    :catchall_0
    move-exception v4

    monitor-exit v6

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 756
    .end local v2    # "enableResult":I
    :catchall_1
    move-exception v4

    monitor-exit v5

    throw v4

    .line 725
    .restart local v2    # "enableResult":I
    :catch_0
    move-exception v1

    .line 726
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_6
    const-string v4, "ANTAidlInterface"

    const-string v6, "Ant Service Enable Failed"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    const-string v4, "ANTAidlInterface"

    const-string v6, "Ant Service Enable failure reason: "

    invoke-static {v4, v6, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 742
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v0    # "currentState":I
    :catchall_2
    move-exception v4

    monitor-exit v6

    throw v4

    .line 745
    :cond_1
    const/4 v3, 0x1

    .line 747
    goto :goto_1

    .line 749
    .end local v0    # "currentState":I
    :cond_2
    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 751
    const/4 v4, 0x0

    :try_start_7
    iput-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    .line 752
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 753
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v4

    :try_start_8
    monitor-exit v6

    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method private getEnabledState()I
    .locals 7

    .prologue
    .line 618
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 620
    const/4 v1, 0x5

    .line 622
    .local v1, "result":I
    sget-object v4, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 624
    :try_start_0
    iget-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v3, :cond_2

    .line 627
    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    invoke-interface {v3}, Lcom/dsi/ant/server/IAntHal;->getAntState()I

    move-result v2

    .line 628
    .local v2, "state":I
    iget-object v5, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideState_LOCK:Ljava/lang/Object;

    monitor-enter v5
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 630
    packed-switch v2, :pswitch_data_0

    .line 683
    :pswitch_0
    const/4 v1, 0x5

    .line 686
    :goto_0
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 697
    .end local v2    # "state":I
    :goto_1
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 699
    return v1

    .line 633
    .restart local v2    # "state":I
    :pswitch_1
    :try_start_4
    iget-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    if-eqz v3, :cond_0

    .line 636
    const/4 v1, 0x3

    goto :goto_0

    .line 640
    :cond_0
    const/4 v1, 0x2

    .line 642
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    .line 643
    iget-object v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 686
    :catchall_0
    move-exception v3

    :try_start_5
    monitor-exit v5

    throw v3
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 691
    .end local v2    # "state":I
    :catch_0
    move-exception v0

    .line 688
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_6
    const-string v3, "ANTAidlInterface"

    const-string v5, "Ant Service State Poll Failed"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    const-string v3, "ANTAidlInterface"

    const-string v5, "Poll failure reason:"

    invoke-static {v3, v5, v0}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 697
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_1
    move-exception v3

    monitor-exit v4

    throw v3

    .line 649
    .restart local v2    # "state":I
    :pswitch_2
    const/4 v1, 0x1

    .line 650
    :try_start_7
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->clearStateOverrides()V

    goto :goto_0

    .line 655
    :pswitch_3
    const/4 v1, 0x3

    .line 656
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->clearStateOverrides()V

    goto :goto_0

    .line 661
    :pswitch_4
    iget-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideEnabling:Z

    if-eqz v3, :cond_1

    .line 664
    const/4 v1, 0x1

    goto :goto_0

    .line 668
    :cond_1
    const/4 v1, 0x0

    .line 670
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mOverrideDisabling:Z

    .line 671
    iget-object v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mDisablingExpiredEvent:Ljava/lang/Runnable;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 677
    :pswitch_5
    const/4 v1, 0x4

    .line 678
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->clearStateOverrides()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 695
    .end local v2    # "state":I
    :cond_2
    :try_start_8
    const-string v3, "ANTAidlInterface"

    const-string v5, "ANT HAL Service was not connected"

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    .line 630
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private initService()Z
    .locals 7

    .prologue
    .line 505
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 507
    const/4 v1, 0x0

    .line 509
    .local v1, "bound":Z
    sget-object v4, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 511
    :try_start_0
    iget-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    if-nez v3, :cond_2

    .line 513
    iget-boolean v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnecting:Z

    if-nez v3, :cond_1

    .line 515
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 518
    .local v2, "context":Landroid/content/Context;
    const-string v3, "AntService"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 519
    .local v0, "binder":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 521
    const/4 v1, 0x1

    .line 522
    const-string v3, "ANTAidlInterface"

    const-string v5, "initService: Got ANT Service"

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    iget-object v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mIAntHalConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x0

    invoke-interface {v3, v5, v0}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 549
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v2    # "context":Landroid/content/Context;
    :goto_0
    monitor-exit v4

    .line 551
    return v1

    .line 528
    .restart local v0    # "binder":Landroid/os/IBinder;
    .restart local v2    # "context":Landroid/content/Context;
    :cond_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 531
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/dsi/ant/server/IAntHal;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mIAntHalConnection:Landroid/content/ServiceConnection;

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 533
    iput-boolean v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnecting:Z

    .line 535
    const-string v3, "ANTAidlInterface"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "initService: Bound with ANT HAL Service: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 549
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v2    # "context":Landroid/content/Context;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 540
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 541
    const/4 v1, 0x1

    goto :goto_0

    .line 546
    :cond_2
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private txMessage([B)Z
    .locals 5
    .param p1, "message"    # [B

    .prologue
    const/4 v2, 0x1

    .line 825
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 827
    const/4 v1, 0x0

    .line 829
    .local v1, "result":Z
    sget-object v3, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 831
    :try_start_0
    iget-boolean v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    .line 834
    :try_start_1
    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    invoke-interface {v4, p1}, Lcom/dsi/ant/server/IAntHal;->ANTTxMessage([B)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-ne v2, v4, :cond_0

    move v1, v2

    .line 845
    :goto_0
    :try_start_2
    monitor-exit v3

    .line 847
    return v1

    .line 834
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 835
    :catch_0
    move-exception v0

    .line 836
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "ANTAidlInterface"

    const-string v4, "Ant Service Message Tx Failed"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    const-string v2, "ANTAidlInterface"

    const-string v4, "Ant Service Message Tx failure reason:"

    invoke-static {v2, v4, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 845
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 843
    :cond_1
    :try_start_3
    const-string v2, "ANTAidlInterface"

    const-string v4, "Tx message ignored, no connection to system service"

    invoke-static {v2, v4}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final bindDetectorSpecificInterface(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 275
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final finalize()V
    .locals 0

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->shutdown()V

    .line 228
    return-void
.end method

.method public final init(Lcom/dsi/ant/chip/IAntChipDetectedListener;)V
    .locals 2
    .param p1, "callback"    # Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 235
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 236
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    invoke-direct {p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->initService()Z

    .line 238
    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final shutdown()V
    .locals 5

    .prologue
    .line 242
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    iget-object v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    invoke-interface {v0, v2}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->adapterGone(Lcom/dsi/ant/chip/IAntChip;)V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    .line 253
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 255
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;

    iget-object v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalCallback:Lcom/dsi/ant/server/IAntHalCallback$Stub;

    invoke-interface {v0, v3}, Lcom/dsi/ant/server/IAntHal;->unregisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    :try_start_3
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mIAntHalConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z

    :goto_2
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 257
    return-void

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 256
    :catch_0
    move-exception v0

    :try_start_5
    const-string v3, "ANTAidlInterface"

    const-string v4, "Ant Service Unbind Failed"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ANTAidlInterface"

    const-string v4, "Unbind Failure reason:"

    invoke-static {v3, v4, v0}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_6
    iget-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnecting:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v0, :cond_3

    :try_start_7
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mIAntHalConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_3
    const/4 v0, 0x0

    :try_start_8
    iput-boolean v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnecting:Z

    goto :goto_2

    :cond_3
    const-string v0, "ANTAidlInterface"

    const-string v1, "ANT HAL Service was not connected"

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_1
.end method
