.class final Lcom/dsi/ant/chip/remote/RemoteAntChip$1;
.super Ljava/lang/Object;
.source "RemoteAntChip.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/remote/RemoteAntChip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/chip/remote/RemoteAntChip;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    sget-object v2, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Please Update the ANT Radio Service, unknown Remote ANT Chip version: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/dsi/ant/chip/remote/IAntChipAidl$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/chip/remote/IAntChipAidl;

    move-result-object v2

    new-instance v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    invoke-direct {v0, v2, v1}, Lcom/dsi/ant/chip/remote/RemoteAntChip;-><init>(Lcom/dsi/ant/chip/remote/IAntChipAidl;I)V

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-array v0, p1, [Lcom/dsi/ant/chip/remote/RemoteAntChip;

    return-object v0
.end method
