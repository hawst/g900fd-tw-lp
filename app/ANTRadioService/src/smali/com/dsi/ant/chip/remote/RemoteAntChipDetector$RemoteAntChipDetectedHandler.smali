.class final Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;
.super Landroid/os/Handler;
.source "RemoteAntChipDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RemoteAntChipDetectedHandler"
.end annotation


# instance fields
.field private final mDestroy_LOCK:Ljava/lang/Object;

.field private mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)V
    .locals 1
    .param p1, "detector"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 61
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDestroy_LOCK:Ljava/lang/Object;

    .line 62
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .line 63
    return-void
.end method

.method private static getChipFromMsg(Landroid/os/Message;)Lcom/dsi/ant/chip/remote/RemoteAntChip;
    .locals 2
    .param p0, "msg"    # Landroid/os/Message;

    .prologue
    .line 72
    invoke-virtual {p0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 74
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 77
    :goto_0
    return-object v1

    .line 76
    :cond_0
    const-class v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 77
    const-string v1, "com.dsi.ant.server.remote.key.ANT_CHIP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    goto :goto_0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 124
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 150
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mSignalChipAddedOrRemoved_LOCK:Ljava/lang/Object;
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$400(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :try_start_1
    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$3;->$SwitchMap$com$dsi$ant$chip$remote$ServiceCallbackMessageIds:[I

    sget-object v3, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->serviceCallbackMessages:[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    iget v4, p1, Landroid/os/Message;->what:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->ordinal()I

    move-result v3

    aget v0, v0, v3
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    packed-switch v0, :pswitch_data_0

    .line 149
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 150
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 137
    :pswitch_0
    :try_start_4
    invoke-static {p1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->getChipFromMsg(Landroid/os/Message;)Lcom/dsi/ant/chip/remote/RemoteAntChip;

    move-result-object v0

    if-eqz v0, :cond_1

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "chipAdded: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    iget-object v3, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    invoke-static {v3, v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$100(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/RemoteAntChip;)V
    :try_end_4
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 146
    :catch_0
    move-exception v0

    :try_start_5
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unknown message type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Remote Service is newer than ANT Radio Service"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 149
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v2

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 140
    :pswitch_1
    :try_start_7
    invoke-static {p1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->getChipFromMsg(Landroid/os/Message;)Lcom/dsi/ant/chip/remote/RemoteAntChip;

    move-result-object v0

    if-eqz v0, :cond_1

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "chipRemoved: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    iget-object v3, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$200(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;->mDetector:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mIAntHardwareCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;
    invoke-static {v3}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$300(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Lcom/dsi/ant/chip/IAntChipDetectedListener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->adapterGone(Lcom/dsi/ant/chip/IAntChip;)V

    goto :goto_1

    :cond_2
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ANT chip detached which is not in the list: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_1

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
