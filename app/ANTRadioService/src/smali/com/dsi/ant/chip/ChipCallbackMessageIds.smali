.class public final enum Lcom/dsi/ant/chip/ChipCallbackMessageIds;
.super Ljava/lang/Enum;
.source "ChipCallbackMessageIds.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/chip/ChipCallbackMessageIds;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CHIP_STATE_CHANGED:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

.field private static final synthetic ENUM$VALUES:[Lcom/dsi/ant/chip/ChipCallbackMessageIds;

.field public static final enum RX_DATA:Lcom/dsi/ant/chip/ChipCallbackMessageIds;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    const-string v1, "RX_DATA"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/chip/ChipCallbackMessageIds;-><init>(Ljava/lang/String;I)V

    .line 37
    sput-object v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->RX_DATA:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    .line 39
    new-instance v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    const-string v1, "CHIP_STATE_CHANGED"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/chip/ChipCallbackMessageIds;-><init>(Ljava/lang/String;I)V

    .line 44
    sput-object v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->CHIP_STATE_CHANGED:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    sget-object v1, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->RX_DATA:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->CHIP_STATE_CHANGED:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->ENUM$VALUES:[Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/chip/ChipCallbackMessageIds;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/chip/ChipCallbackMessageIds;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->ENUM$VALUES:[Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    array-length v1, v0

    new-array v2, v1, [Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
