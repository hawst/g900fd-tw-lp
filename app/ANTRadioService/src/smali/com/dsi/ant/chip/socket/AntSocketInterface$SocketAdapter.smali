.class final Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;
.super Ljava/lang/Object;
.source "AntSocketInterface.java"

# interfaces
.implements Lcom/dsi/ant/chip/IAntChip;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/socket/AntSocketInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SocketAdapter"
.end annotation


# instance fields
.field public mCallback:Landroid/os/Messenger;

.field private final mChipName:Ljava/lang/String;

.field private final mHardwareType:Ljava/lang/String;

.field volatile mInvalid:Z

.field final synthetic this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/chip/socket/AntSocketInterface;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "chipName"    # Ljava/lang/String;
    .param p3, "hardwareType"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    .line 48
    iput-object p2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mChipName:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mHardwareType:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public final disable()I
    .locals 2

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "ANTSocketInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x5

    .line 125
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->disable()I

    move-result v0

    goto :goto_0
.end method

.method public final enable()I
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "ANTSocketInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/4 v0, 0x5

    .line 113
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->enable()I

    move-result v0

    goto :goto_0
.end method

.method public final getChipName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mChipName:Ljava/lang/String;

    return-object v0
.end method

.method public final getChipState()I
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "ANTSocketInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const/4 v0, 0x5

    .line 101
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->getEnabledState()I

    move-result v0

    goto :goto_0
.end method

.method public final getHardwareType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mHardwareType:Ljava/lang/String;

    return-object v0
.end method

.method public final setCallback(Landroid/os/Messenger;)V
    .locals 2
    .param p1, "callback"    # Landroid/os/Messenger;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    iget-object v1, v0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mCallback:Landroid/os/Messenger;

    .line 65
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final txBurst(I[B)Z
    .locals 2
    .param p1, "channel"    # I
    .param p2, "data"    # [B

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "ANTSocketInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const/4 v0, 0x0

    return v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use concatenated burst messages with txMessage instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final txMessage([B)Z
    .locals 2
    .param p1, "message"    # [B

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "ANTSocketInterface"

    const-string v1, "This adapter is invalid."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/4 v0, 0x0

    .line 77
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->this$0:Lcom/dsi/ant/chip/socket/AntSocketInterface;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->txMessage([B)Z

    move-result v0

    goto :goto_0
.end method
