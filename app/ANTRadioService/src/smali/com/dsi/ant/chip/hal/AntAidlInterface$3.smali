.class final Lcom/dsi/ant/chip/hal/AntAidlInterface$3;
.super Ljava/lang/Object;
.source "AntAidlInterface.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/hal/AntAidlInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "pClassName"    # Landroid/content/ComponentName;
    .param p2, "pService"    # Landroid/os/IBinder;

    .prologue
    .line 333
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 335
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-static {p2}, Lcom/dsi/ant/server/IAntHal$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/server/IAntHal;

    move-result-object v2

    # setter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;
    invoke-static {v1, v2}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$902(Lcom/dsi/ant/chip/hal/AntAidlInterface;Lcom/dsi/ant/server/IAntHal;)Lcom/dsi/ant/server/IAntHal;

    .line 337
    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1000()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 339
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    const/4 v3, 0x1

    # setter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z
    invoke-static {v1, v3}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1102(Lcom/dsi/ant/chip/hal/AntAidlInterface;Z)Z

    .line 340
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1202$5ff94f9e(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Z

    .line 341
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$900(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/server/IAntHal;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalCallback:Lcom/dsi/ant/server/IAntHalCallback$Stub;
    invoke-static {v2}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1300(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/server/IAntHalCallback$Stub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/dsi/ant/server/IAntHal;->registerAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I

    .line 347
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 349
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 351
    :try_start_2
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    new-instance v3, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    iget-object v4, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;B)V

    # setter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v1, v3}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1402(Lcom/dsi/ant/chip/hal/AntAidlInterface;Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    .line 353
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1600(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/IAntChipDetectedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1600(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/IAntChipDetectedListener;

    move-result-object v1

    iget-object v3, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v3}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->newAdapter(Lcom/dsi/ant/chip/IAntChip;)V

    .line 355
    :cond_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 363
    :goto_0
    return-void

    .line 341
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 355
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2

    throw v1
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 362
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ANTAidlInterface"

    const-string v2, "Ant Service Bind Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string v1, "ANTAidlInterface"

    const-string v2, "Failed Bind exception: "

    invoke-static {v1, v2, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "pClassName"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 369
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 371
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 373
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1600(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/IAntChipDetectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1600(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/IAntChipDetectedListener;

    move-result-object v0

    iget-object v2, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v2}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->adapterGone(Lcom/dsi/ant/chip/IAntChip;)V

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    move-result-object v0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mInvalid:Z

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    const/4 v2, 0x0

    # setter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v0, v2}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1402(Lcom/dsi/ant/chip/hal/AntAidlInterface;Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    .line 380
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # setter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalReceiver:Lcom/dsi/ant/server/IAntHal;
    invoke-static {v0, v3}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$902(Lcom/dsi/ant/chip/hal/AntAidlInterface;Lcom/dsi/ant/server/IAntHal;)Lcom/dsi/ant/server/IAntHal;

    .line 384
    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->sAntHalServiceConnectionState_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 386
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$3;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    const/4 v2, 0x0

    # setter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAntHalServiceConnected:Z
    invoke-static {v0, v2}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1102(Lcom/dsi/ant/chip/hal/AntAidlInterface;Z)Z

    .line 387
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 387
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
