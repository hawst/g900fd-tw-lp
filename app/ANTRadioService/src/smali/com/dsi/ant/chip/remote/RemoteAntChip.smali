.class public Lcom/dsi/ant/chip/remote/RemoteAntChip;
.super Ljava/lang/Object;
.source "RemoteAntChip.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/dsi/ant/chip/IAntChip;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/chip/remote/RemoteAntChip;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String;


# instance fields
.field private final mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

.field private final mVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 162
    new-instance v0, Lcom/dsi/ant/chip/remote/RemoteAntChip$1;

    invoke-direct {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChip$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 189
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/chip/remote/IAntChipAidl;I)V
    .locals 6
    .param p1, "chip"    # Lcom/dsi/ant/chip/remote/IAntChipAidl;
    .param p2, "version"    # I

    .prologue
    const/4 v5, 0x2

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 97
    if-nez p1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given IRemoteAntChip is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    if-le p2, v5, :cond_1

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given version (%d) is larger than max (%d)"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 105
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 104
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    .line 108
    iput p2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mVersion:I

    .line 109
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method public final disable()I
    .locals 3

    .prologue
    .line 311
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 312
    const/4 v0, 0x5

    .line 316
    .local v0, "chipState":I
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->disable()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 324
    :goto_0
    return v0

    .line 320
    :catch_0
    move-exception v1

    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    const-string v2, "Could not disable, remote service appears to have crashed."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final enable()I
    .locals 3

    .prologue
    .line 291
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 293
    const/4 v0, 0x5

    .line 297
    .local v0, "chipState":I
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->enable()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 305
    :goto_0
    return v0

    .line 301
    :catch_0
    move-exception v1

    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    const-string v2, "Could not enable, remote service appears to have crashed."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    if-ne p0, p1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v1

    .line 123
    :cond_1
    instance-of v3, p1, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    if-nez v3, :cond_2

    move v1, v2

    .line 125
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 130
    check-cast v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    .line 133
    .local v0, "lhs":Lcom/dsi/ant/chip/remote/RemoteAntChip;
    iget-object v3, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v3}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    iget-object v4, v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v4}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public final getChipName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 347
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 349
    const-string v0, ""

    .line 353
    .local v0, "chipName":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->getChipName()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 360
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getChipState()I
    .locals 2

    .prologue
    .line 273
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 274
    const/4 v0, 0x5

    .line 278
    .local v0, "chipState":I
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->getChipState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 285
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHardwareType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 365
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 367
    const-string v0, ""

    .line 371
    .local v0, "hardwareType":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->getHardwareType()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 378
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getRemoteAntChip()Lcom/dsi/ant/chip/remote/IAntChipAidl;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v0}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final setCallback(Landroid/os/Messenger;)V
    .locals 2
    .param p1, "callback"    # Landroid/os/Messenger;

    .prologue
    .line 212
    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 216
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v0, p1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->setCallback(Landroid/os/Messenger;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-void

    .line 220
    :catch_0
    move-exception v0

    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    const-string v1, "Could not setCallback, remote service appears to have crashed."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "[target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final txBurst(I[B)Z
    .locals 3
    .param p1, "channel"    # I
    .param p2, "data"    # [B

    .prologue
    .line 248
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 249
    const/4 v0, 0x0

    .line 251
    .local v0, "txWritten":Z
    const/4 v1, 0x1

    iget v2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mVersion:I

    if-ne v1, v2, :cond_0

    .line 253
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Use concatenated burst messages with txMessage instead"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 259
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1, p1, p2}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->txBurst(I[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 267
    :goto_0
    return v0

    .line 263
    :catch_0
    move-exception v1

    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    const-string v2, "Could not txBurst, remote service appears to have crashed."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final txMessage([B)Z
    .locals 3
    .param p1, "message"    # [B

    .prologue
    .line 229
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    .line 230
    const/4 v0, 0x0

    .line 234
    .local v0, "txWritten":Z
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v1, p1}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->txMessage([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 242
    :goto_0
    return v0

    .line 238
    :catch_0
    move-exception v1

    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChip;->TAG:Ljava/lang/String;

    const-string v2, "Could not txMessage, remote service appears to have crashed."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 205
    iget v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChip;->mRemoteAntChip:Lcom/dsi/ant/chip/remote/IAntChipAidl;

    invoke-interface {v0}, Lcom/dsi/ant/chip/remote/IAntChipAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 207
    return-void
.end method
