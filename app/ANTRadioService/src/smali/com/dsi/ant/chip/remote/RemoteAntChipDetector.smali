.class public Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;
.super Ljava/lang/Object;
.source "RemoteAntChipDetector.java"

# interfaces
.implements Lcom/dsi/ant/chip/IAntChipDetector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$3;,
        Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field static serviceCallbackMessages:[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;


# instance fields
.field private final mExecutorAccessPermits:Ljava/util/concurrent/Semaphore;

.field private mIAntHardwareCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

.field private mInitialized:Z

.field private final mRemoteAntChipConnectionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/chip/remote/RemoteAntChip;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoteAntChipDetectedListener:Landroid/os/Messenger;

.field private mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

.field private mRemoteServiceConnection:Landroid/content/ServiceConnection;

.field private final mSignalChipAddedOrRemoved_LOCK:Ljava/lang/Object;

.field private final mTargetClass:Ljava/lang/String;

.field private final mTargetPackage:Ljava/lang/String;

.field private mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    const-class v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    .line 489
    invoke-static {}, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->values()[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->serviceCallbackMessages:[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mInitialized:Z

    .line 179
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mSignalChipAddedOrRemoved_LOCK:Ljava/lang/Object;

    .line 210
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mExecutorAccessPermits:Ljava/util/concurrent/Semaphore;

    .line 429
    new-instance v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;-><init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteServiceConnection:Landroid/content/ServiceConnection;

    .line 495
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;

    invoke-direct {v1, p0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$RemoteAntChipDetectedHandler;-><init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipDetectedListener:Landroid/os/Messenger;

    .line 222
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mTargetPackage:Ljava/lang/String;

    .line 223
    iput-object p2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mTargetClass:Ljava/lang/String;

    .line 224
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/RemoteAntChip;)V
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;
    .param p1, "x1"    # Lcom/dsi/ant/chip/remote/RemoteAntChip;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mIAntHardwareCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    invoke-interface {v0, p1}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->newAdapter(Lcom/dsi/ant/chip/IAntChip;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Lcom/dsi/ant/chip/IAntChipDetectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mIAntHardwareCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mSignalChipAddedOrRemoved_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mInitialized:Z

    return v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    return-object v0
.end method

.method static synthetic access$602(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;
    .param p1, "x1"    # Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    return-object p1
.end method

.method static synthetic access$700(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Landroid/os/Messenger;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipDetectedListener:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$802(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;
    .param p1, "x1"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    return-object p1
.end method

.method static synthetic access$900(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->removeAllChips()V

    return-void
.end method

.method private removeAllChips()V
    .locals 3

    .prologue
    .line 417
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipConnectionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/chip/remote/RemoteAntChip;

    .line 423
    .local v0, "chip":Lcom/dsi/ant/chip/remote/RemoteAntChip;
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing chip "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChip;->getRemoteAntChip()Lcom/dsi/ant/chip/remote/IAntChipAidl;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 425
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mIAntHardwareCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    invoke-interface {v1, v0}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->adapterGone(Lcom/dsi/ant/chip/IAntChip;)V

    goto :goto_0

    .line 427
    .end local v0    # "chip":Lcom/dsi/ant/chip/remote/RemoteAntChip;
    :cond_0
    return-void
.end method


# virtual methods
.method public final bindDetectorSpecificInterface(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 409
    const/4 v0, 0x0

    return-object v0
.end method

.method public final init(Lcom/dsi/ant/chip/IAntChipDetectedListener;)V
    .locals 4
    .param p1, "callback"    # Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .prologue
    const/4 v3, 0x1

    .line 229
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 231
    iget-boolean v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mInitialized:Z

    if-eqz v1, :cond_0

    .line 234
    sget-object v1, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    const-string v2, "Already initialized, ignoring extra call."

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :goto_0
    return-void

    .line 237
    :cond_0
    iput-boolean v3, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mInitialized:Z

    .line 239
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mIAntHardwareCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 242
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 243
    .local v0, "bindIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mTargetPackage:Ljava/lang/String;

    iget-object v2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v1, "com.dsi.ant.action.BIND_REMOTE_INTERFACE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method public final scanForNewChips()Z
    .locals 7

    .prologue
    .line 290
    const/4 v3, 0x0

    .line 294
    .local v3, "scanResult":Z
    iget-object v4, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mExecutorAccessPermits:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 300
    const/4 v2, 0x0

    .line 305
    .local v2, "scanForNewChipsFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Boolean;>;"
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    .line 307
    .local v1, "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v4

    if-nez v4, :cond_1

    .line 311
    iget-object v4, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;

    invoke-direct {v5, p0, v1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;-><init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 356
    const-wide/16 v4, 0x1388

    :try_start_1
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v6}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    .line 375
    :goto_0
    if-nez v3, :cond_0

    .line 381
    :try_start_2
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getAdapterProvider()Lcom/dsi/ant/adapter/AdapterProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/adapter/AdapterProvider;->hasBuiltInChipDetector()Z

    move-result v4

    if-nez v4, :cond_0

    .line 382
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProviderImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannelProviderImpl;->setServiceInitialised()V

    .line 389
    :cond_0
    iget-object v4, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;
    :try_end_2
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 399
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mExecutorAccessPermits:Ljava/util/concurrent/Semaphore;

    .end local v1    # "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    :goto_1
    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->release()V

    .line 402
    .end local v2    # "scanForNewChipsFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Boolean;>;"
    :cond_2
    return v3

    .line 359
    .restart local v1    # "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    .restart local v2    # "scanForNewChipsFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Boolean;>;"
    :catch_0
    move-exception v4

    const/4 v4, 0x1

    :try_start_3
    invoke-interface {v2, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 360
    sget-object v4, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V
    :try_end_3
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 391
    .end local v1    # "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    :catch_1
    move-exception v0

    .line 397
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    :try_start_4
    sget-object v4, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    const-string v5, "ScanForNewChips cannot be started."

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 399
    iget-object v4, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mExecutorAccessPermits:Ljava/util/concurrent/Semaphore;

    goto :goto_1

    .line 361
    .end local v0    # "e":Ljava/util/concurrent/RejectedExecutionException;
    .restart local v1    # "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    :catch_2
    move-exception v0

    .line 364
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_5
    sget-object v4, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ScanForNewChips failed: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 399
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v1    # "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mExecutorAccessPermits:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v5}, Ljava/util/concurrent/Semaphore;->release()V

    throw v4

    .line 367
    .restart local v1    # "remoteService":Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    :catch_3
    move-exception v4

    const/4 v4, 0x1

    :try_start_6
    invoke-interface {v2, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 368
    sget-object v4, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    const-string v5, "Getting chip list with scan timed out as it is taking longer than usual to complete."

    invoke-static {v4, v5}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 372
    :catch_4
    move-exception v4

    sget-object v4, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_6
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public final shutdown()V
    .locals 2

    .prologue
    .line 252
    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mInitialized:Z

    .line 259
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 265
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;->setCallback(Landroid/os/Messenger;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->removeAllChips()V

    .line 277
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 278
    return-void

    .line 272
    :catch_0
    move-exception v0

    sget-object v0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;

    const-string v1, "Error clearing callback to remote service, proceed with chips shutdown."

    invoke-static {v0, v1}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
