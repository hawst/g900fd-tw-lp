.class public Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;
.super Lcom/dsi/ant/chip/socket/AntSocketInterface;
.source "LocalSocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;
    }
.end annotation


# static fields
.field private static final SETTINGS_BINDER_NAME:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mSettingsBinder:Lcom/dsi/ant/service/socket/local/ILocalSocketSettings$Stub;

.field private mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

.field private mSocketName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->TAG:Ljava/lang/String;

    .line 39
    const-class v0, Lcom/dsi/ant/service/socket/local/ILocalSocketSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->SETTINGS_BINDER_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 51
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "built-in"

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/chip/socket/AntSocketInterface;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;-><init>(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSettingsBinder:Lcom/dsi/ant/service/socket/local/ILocalSocketSettings$Stub;

    .line 55
    new-instance v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;-><init>(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    .line 56
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "LocalSocketPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v0, v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    const-string v2, "SOCKET_NAME"

    const-string v3, "antradio.socket"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;-><init>(Lcom/dsi/ant/chip/socket/AntSocketInterface;Lcom/dsi/ant/chip/socket/ISocketConnection;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    return-object v0
.end method


# virtual methods
.method public final bindDetectorSpecificInterface(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 74
    .local v0, "binder":Landroid/os/IBinder;
    sget-object v1, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->SETTINGS_BINDER_NAME:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    sget-object v1, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->TAG:Ljava/lang/String;

    const-string v2, "Get Binder: Settings"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSettingsBinder:Lcom/dsi/ant/service/socket/local/ILocalSocketSettings$Stub;

    .line 81
    :cond_0
    return-object v0
.end method
