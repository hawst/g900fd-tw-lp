.class final Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;
.super Ljava/lang/Object;
.source "RemoteAntChipDetector.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 434
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 437
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mInitialized:Z
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$500(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    :goto_0
    return-void

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    invoke-static {p2}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    move-result-object v1

    # setter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    invoke-static {v0, v1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$602(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    .line 442
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$600(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteAntChipDetectedListener:Landroid/os/Messenger;
    invoke-static {v1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$700(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Landroid/os/Messenger;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;->setCallback(Landroid/os/Messenger;)V

    .line 444
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    # setter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0, v1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$802(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;

    .line 446
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$800(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2$1;

    invoke-direct {v1, p0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2$1;-><init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 454
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 463
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 465
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$800(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mWorkerThreadPool:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$800(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # invokes: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->removeAllChips()V
    invoke-static {v0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$900(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)V

    .line 471
    iget-object v0, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$2;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    const/4 v1, 0x0

    # setter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mRemoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    invoke-static {v0, v1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$602(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    .line 472
    return-void
.end method
