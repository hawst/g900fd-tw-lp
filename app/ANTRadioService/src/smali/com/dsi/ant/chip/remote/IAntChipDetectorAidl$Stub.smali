.class public abstract Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub;
.super Landroid/os/Binder;
.source "IAntChipDetectorAidl.java"

# interfaces
.implements Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub$Proxy;
    }
.end annotation


# direct methods
.method public static asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.dsi.ant.chip.remote.IAntChipDetectorAidl"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 72
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 46
    :sswitch_0
    const-string v3, "com.dsi.ant.chip.remote.IAntChipDetectorAidl"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v3, "com.dsi.ant.chip.remote.IAntChipDetectorAidl"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 54
    sget-object v3, Landroid/os/Messenger;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 59
    .local v0, "_arg0":Landroid/os/Messenger;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub;->setCallback(Landroid/os/Messenger;)V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":Landroid/os/Messenger;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Messenger;
    goto :goto_1

    .line 65
    .end local v0    # "_arg0":Landroid/os/Messenger;
    :sswitch_2
    const-string v3, "com.dsi.ant.chip.remote.IAntChipDetectorAidl"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub;->scanForNewChips()[Lcom/dsi/ant/chip/remote/RemoteAntChip;

    move-result-object v1

    .line 67
    .local v1, "_result":[Lcom/dsi/ant/chip/remote/RemoteAntChip;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    invoke-virtual {p3, v1, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
