.class final Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;
.super Ljava/lang/Object;
.source "LocalSocketConnection.java"

# interfaces
.implements Lcom/dsi/ant/chip/socket/ISocketConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SocketConnection"
.end annotation


# instance fields
.field private mSocket:Landroid/net/LocalSocket;

.field final synthetic this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)V
    .locals 1

    .prologue
    .line 123
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    .line 125
    return-void
.end method


# virtual methods
.method public final connect()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    .line 131
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    new-instance v1, Landroid/net/LocalSocketAddress;

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;
    invoke-static {v2}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->access$000(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 132
    return-void
.end method

.method public final disconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->shutdownOutput()V

    .line 140
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->shutdownInput()V

    .line 141
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    .line 145
    :cond_0
    return-void
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 192
    .local v0, "input":Ljava/io/InputStream;
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    .line 196
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 204
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 172
    .local v0, "output":Ljava/io/OutputStream;
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 184
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final isConnected()Z
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    .line 159
    .local v0, "connected":Z
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v0

    .line 164
    :cond_0
    return v0
.end method

.method public final reconnect()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->disconnect()V

    .line 151
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->connect()V

    .line 152
    return-void
.end method
