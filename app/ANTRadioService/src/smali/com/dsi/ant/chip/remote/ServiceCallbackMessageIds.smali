.class public final enum Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;
.super Ljava/lang/Enum;
.source "ServiceCallbackMessageIds.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CHIP_ADDED:Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

.field public static final enum CHIP_REMOVED:Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

.field private static final synthetic ENUM$VALUES:[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    const-string v1, "CHIP_ADDED"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;-><init>(Ljava/lang/String;I)V

    .line 36
    sput-object v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->CHIP_ADDED:Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    .line 38
    new-instance v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    const-string v1, "CHIP_REMOVED"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;-><init>(Ljava/lang/String;I)V

    .line 43
    sput-object v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->CHIP_REMOVED:Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    sget-object v1, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->CHIP_ADDED:Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->CHIP_REMOVED:Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->ENUM$VALUES:[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;->ENUM$VALUES:[Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    array-length v1, v0

    new-array v2, v1, [Lcom/dsi/ant/chip/remote/ServiceCallbackMessageIds;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
