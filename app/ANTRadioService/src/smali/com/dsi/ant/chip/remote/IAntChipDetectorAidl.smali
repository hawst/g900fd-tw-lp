.class public interface abstract Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;
.super Ljava/lang/Object;
.source "IAntChipDetectorAidl.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl$Stub;
    }
.end annotation


# virtual methods
.method public abstract scanForNewChips()[Lcom/dsi/ant/chip/remote/RemoteAntChip;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setCallback(Landroid/os/Messenger;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
