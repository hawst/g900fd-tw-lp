.class public interface abstract Lcom/dsi/ant/chip/IAntChip;
.super Ljava/lang/Object;
.source "IAntChip.java"


# virtual methods
.method public abstract disable()I
.end method

.method public abstract enable()I
.end method

.method public abstract getChipName()Ljava/lang/String;
.end method

.method public abstract getChipState()I
.end method

.method public abstract getHardwareType()Ljava/lang/String;
.end method

.method public abstract setCallback(Landroid/os/Messenger;)V
.end method

.method public abstract txBurst(I[B)Z
.end method

.method public abstract txMessage([B)Z
.end method
