.class public Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;
.super Lcom/dsi/ant/chip/socket/AntSocketInterface;
.source "TcpSocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;
    }
.end annotation


# static fields
.field private static final SETTINGS_BINDER_NAME:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mHost:Ljava/net/InetAddress;

.field private mPort:I

.field private final mSettingsBinder:Lcom/dsi/ant/service/socket/tcp/ITcpSocketSettings$Stub;

.field private mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->TAG:Ljava/lang/String;

    .line 41
    const-class v0, Lcom/dsi/ant/service/socket/tcp/ITcpSocketSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->SETTINGS_BINDER_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 56
    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "built-in"

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/chip/socket/AntSocketInterface;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    new-instance v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;-><init>(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSettingsBinder:Lcom/dsi/ant/service/socket/tcp/ITcpSocketSettings$Stub;

    .line 60
    new-instance v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    invoke-direct {v0, p0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;-><init>(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    .line 61
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "TCPSocketPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    :try_start_0
    iget-object v2, v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    const-string v3, "IP_ADDRESS"

    const-string v4, "10.0.2.2"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    iput-object v3, v2, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    const-string v2, "PORT"

    const/16 v3, 0x235a

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I

    .line 65
    new-instance v0, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;-><init>(Lcom/dsi/ant/chip/socket/AntSocketInterface;Lcom/dsi/ant/chip/socket/ISocketConnection;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    .line 66
    return-void

    .line 61
    :catch_0
    move-exception v2

    iget-object v2, v0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    .prologue
    .line 34
    iget v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I

    return v0
.end method

.method static synthetic access$002(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I

    return p1
.end method

.method static synthetic access$100(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Ljava/net/InetAddress;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;

    return-object v0
.end method

.method static synthetic access$202(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;
    .param p1, "x1"    # Ljava/net/InetAddress;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;

    return-object p1
.end method


# virtual methods
.method public final bindDetectorSpecificInterface(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 78
    .local v0, "binder":Landroid/os/IBinder;
    sget-object v1, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->SETTINGS_BINDER_NAME:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    sget-object v1, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->TAG:Ljava/lang/String;

    const-string v2, "Get Binder: Settings"

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSettingsBinder:Lcom/dsi/ant/service/socket/tcp/ITcpSocketSettings$Stub;

    .line 85
    :cond_0
    return-object v0
.end method
