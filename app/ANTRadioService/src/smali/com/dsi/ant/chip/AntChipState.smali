.class public final Lcom/dsi/ant/chip/AntChipState;
.super Ljava/lang/Object;
.source "AntChipState.java"


# direct methods
.method public static final toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 82
    packed-switch p0, :pswitch_data_0

    .line 97
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 85
    :pswitch_0
    const-string v0, "DISABLED"

    goto :goto_0

    .line 87
    :pswitch_1
    const-string v0, "DISABLING"

    goto :goto_0

    .line 89
    :pswitch_2
    const-string v0, "ENABLED"

    goto :goto_0

    .line 91
    :pswitch_3
    const-string v0, "ENABLING"

    goto :goto_0

    .line 93
    :pswitch_4
    const-string v0, "HARD RESETTING"

    goto :goto_0

    .line 95
    :pswitch_5
    const-string v0, "ERROR"

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
