.class final Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;
.super Ljava/lang/Object;
.source "RemoteAntChipDetector.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->scanForNewChips()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

.field final synthetic val$remoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    iput-object p2, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;->val$remoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private call()Ljava/lang/Boolean;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 319
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 323
    .local v5, "result":Ljava/lang/Boolean;
    iget-object v6, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->mSignalChipAddedOrRemoved_LOCK:Ljava/lang/Object;
    invoke-static {v6}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$400(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 330
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;->val$remoteService:Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;

    invoke-interface {v6}, Lcom/dsi/ant/chip/remote/IAntChipDetectorAidl;->scanForNewChips()[Lcom/dsi/ant/chip/remote/RemoteAntChip;

    move-result-object v2

    .line 331
    .local v2, "chips":[Lcom/dsi/ant/chip/remote/RemoteAntChip;
    if-nez v2, :cond_1

    .line 333
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 348
    :cond_0
    :goto_0
    monitor-exit v7

    .line 350
    return-object v5

    .line 335
    :cond_1
    array-length v6, v2

    if-nez v6, :cond_2

    .line 337
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 348
    .end local v2    # "chips":[Lcom/dsi/ant/chip/remote/RemoteAntChip;
    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    .line 339
    .restart local v2    # "chips":[Lcom/dsi/ant/chip/remote/RemoteAntChip;
    :cond_2
    :try_start_1
    array-length v6, v2

    if-lez v6, :cond_0

    .line 341
    # getter for: Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$000()Ljava/lang/String;

    const-string v6, "Got a list of %d chips from scan"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    array-length v10, v2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 342
    move-object v0, v2

    .local v0, "arr$":[Lcom/dsi/ant/chip/remote/RemoteAntChip;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 344
    .local v1, "chip":Lcom/dsi/ant/chip/remote/RemoteAntChip;
    iget-object v6, p0, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;->this$0:Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;

    invoke-static {v6, v1}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;->access$100(Lcom/dsi/ant/chip/remote/RemoteAntChipDetector;Lcom/dsi/ant/chip/remote/RemoteAntChip;)V

    .line 342
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 346
    .end local v1    # "chip":Lcom/dsi/ant/chip/remote/RemoteAntChip;
    :cond_3
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/dsi/ant/chip/remote/RemoteAntChipDetector$1;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
