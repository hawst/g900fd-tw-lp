.class final Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;
.super Lcom/dsi/ant/service/socket/tcp/ITcpSocketSettings$Stub;
.source "TcpSocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    invoke-direct {p0}, Lcom/dsi/ant/service/socket/tcp/ITcpSocketSettings$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final getIpAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    const-string v0, ""

    .line 126
    .local v0, "ipAddress":Ljava/lang/String;
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$200(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Ljava/net/InetAddress;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$200(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 131
    :cond_0
    return-object v0
.end method

.method public final getPort()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I
    invoke-static {v0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$000(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)I

    move-result v0

    return v0
.end method

.method public final reconnectSocket()Z
    .locals 2

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 139
    .local v0, "result":Z
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$100(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->reconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final setIpAddress(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 111
    .local v0, "result":Z
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    # setter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mHost:Ljava/net/InetAddress;
    invoke-static {v1, v2}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$202(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;Ljava/net/InetAddress;)Ljava/net/InetAddress;

    .line 112
    const/4 v0, 0x1

    .line 113
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$100(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->saveState()V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final setPort(I)Z
    .locals 1
    .param p1, "port"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # setter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mPort:I
    invoke-static {v0, p1}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$002(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;I)I

    .line 96
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;
    invoke-static {v0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;->access$100(Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection;)Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/tcp/TcpSocketConnection$SocketConnection;->saveState()V

    .line 98
    const/4 v0, 0x1

    return v0
.end method
