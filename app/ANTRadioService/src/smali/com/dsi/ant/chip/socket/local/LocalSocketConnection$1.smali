.class final Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;
.super Lcom/dsi/ant/service/socket/local/ILocalSocketSettings$Stub;
.source "LocalSocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    invoke-direct {p0}, Lcom/dsi/ant/service/socket/local/ILocalSocketSettings$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;
    invoke-static {v0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->access$000(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final reconnectSocket()Z
    .locals 2

    .prologue
    .line 104
    const/4 v0, 0x0

    .line 107
    .local v0, "result":Z
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;
    invoke-static {v1}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->access$100(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->reconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final setName(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    # setter for: Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->access$002(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;Ljava/lang/String;)Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$1;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketConnection:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;
    invoke-static {v0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->access$100(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;

    move-result-object v0

    invoke-static {}, Lcom/dsi/ant/service/AntRadioService;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "LocalSocketPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "SOCKET_NAME"

    iget-object v0, v0, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection$SocketConnection;->this$0:Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;

    # getter for: Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->mSocketName:Ljava/lang/String;
    invoke-static {v0}, Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;->access$000(Lcom/dsi/ant/chip/socket/local/LocalSocketConnection;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 94
    const/4 v0, 0x1

    return v0
.end method
