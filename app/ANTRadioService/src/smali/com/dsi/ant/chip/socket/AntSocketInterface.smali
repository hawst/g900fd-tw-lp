.class public abstract Lcom/dsi/ant/chip/socket/AntSocketInterface;
.super Ljava/lang/Object;
.source "AntSocketInterface.java"

# interfaces
.implements Lcom/dsi/ant/chip/IAntChipDetector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;
    }
.end annotation


# instance fields
.field mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

.field private mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

.field private final mChipName:Ljava/lang/String;

.field private mEnabledState:I

.field private final mHardwareType:Ljava/lang/String;

.field final mSocketInterfaceLock:Ljava/lang/Object;

.field protected mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

.field private mTxSuccess:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "chipName"    # Ljava/lang/String;
    .param p2, "hardwareType"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    .line 178
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipName:Ljava/lang/String;

    .line 179
    iput-object p2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mHardwareType:Ljava/lang/String;

    .line 181
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 185
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    iput-object v3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    .line 188
    iput-boolean v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mTxSuccess:Z

    .line 189
    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private updateState(I)V
    .locals 5
    .param p1, "newState"    # I

    .prologue
    .line 302
    iget-object v3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v3

    .line 304
    :try_start_0
    iget v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    .line 305
    .local v1, "oldState":I
    iput p1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    .line 307
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 310
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    iget-object v2, v2, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mCallback:Landroid/os/Messenger;

    iget v4, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    invoke-static {v2, v1, v4}, Lcom/dsi/ant/chip/ChipCallbackHelper;->sendStateUpdate(Landroid/os/Messenger;II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v3

    return-void

    .line 311
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "ANTSocketInterface"

    const-string v4, "Impossible Remote Exception."

    invoke-static {v2, v4, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 316
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "oldState":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method final disable()I
    .locals 4

    .prologue
    .line 384
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 386
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 388
    :try_start_0
    iget v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    packed-switch v0, :pswitch_data_0

    .line 416
    const-string v0, "ANTSocketInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "disable: Unknown current state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423
    iget v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    return v0

    .line 393
    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->updateState(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 399
    :pswitch_1
    :try_start_2
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_0

    .line 404
    :pswitch_2
    const-string v0, "ANTSocketInterface"

    const-string v2, "disable: Chip already enabling"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 410
    :pswitch_3
    const-string v0, "ANTSocketInterface"

    const-string v2, "disable: Chip already resetting"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method final enable()I
    .locals 4

    .prologue
    .line 321
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 323
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 327
    :cond_0
    const-string v0, "ANTSocketInterface"

    const-string v2, "enable: Socket not connected"

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const/4 v0, 0x5

    monitor-exit v1

    .line 360
    :goto_0
    return v0

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->isInitialising()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    const-string v0, "ANTSocketInterface"

    const-string v2, "enable: Socket still initialising."

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 358
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 337
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    packed-switch v0, :pswitch_data_0

    .line 353
    :pswitch_0
    const-string v0, "ANTSocketInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "enable: ANT in invalid state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    invoke-static {v3}, Lcom/dsi/ant/chip/AntChipState;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    iget v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    goto :goto_0

    .line 340
    :pswitch_1
    :try_start_2
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    goto :goto_1

    .line 344
    :pswitch_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->updateState(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 337
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final getEnabledState()I
    .locals 3

    .prologue
    .line 193
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "getEnabledState ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 197
    iget v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final init(Lcom/dsi/ant/chip/IAntChipDetectedListener;)V
    .locals 5
    .param p1, "callback"    # Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .prologue
    .line 203
    iget-object v3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v3

    .line 205
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 206
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 210
    const/4 v0, 0x0

    .line 212
    .local v0, "createdOk":Z
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    if-nez v2, :cond_1

    .line 214
    const-string v2, "ANTSocketInterface"

    const-string v3, "Socket Thread was not initialised"

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 206
    .end local v0    # "createdOk":Z
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 218
    .restart local v0    # "createdOk":Z
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v2}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 220
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 222
    const/4 v0, 0x1

    .line 243
    :goto_1
    if-nez v0, :cond_0

    .line 245
    const-string v2, "ANTSocketInterface"

    const-string v3, "create: FAILED"

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->shutdown()V

    goto :goto_0

    .line 228
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 230
    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v2}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->start()V

    .line 233
    const/4 v0, 0x1

    .line 235
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 237
    :catch_0
    move-exception v1

    .line 239
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ANTSocketInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception thrown during socket enable ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final setThreadReady(Z)V
    .locals 4
    .param p1, "isReady"    # Z

    .prologue
    .line 271
    if-eqz p1, :cond_2

    .line 273
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    if-eqz v0, :cond_0

    monitor-exit v1

    .line 298
    :goto_0
    return-void

    .line 277
    :cond_0
    new-instance v0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipName:Ljava/lang/String;

    iget-object v3, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mHardwareType:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v3}, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;-><init>(Lcom/dsi/ant/chip/socket/AntSocketInterface;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    .line 279
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    invoke-interface {v0, v2}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->newAdapter(Lcom/dsi/ant/chip/IAntChip;)V

    .line 281
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 285
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 287
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    if-eqz v0, :cond_3

    .line 288
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    iget-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    invoke-interface {v0, v2}, Lcom/dsi/ant/chip/IAntChipDetectedListener;->adapterGone(Lcom/dsi/ant/chip/IAntChip;)V

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    if-eqz v0, :cond_4

    .line 291
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;->mInvalid:Z

    .line 293
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mAdapter:Lcom/dsi/ant/chip/socket/AntSocketInterface$SocketAdapter;

    .line 294
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 296
    invoke-virtual {p0}, Lcom/dsi/ant/chip/socket/AntSocketInterface;->disable()I

    goto :goto_0

    .line 294
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final shutdown()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 366
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 368
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 370
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mChipDetectorCallback:Lcom/dsi/ant/chip/IAntChipDetectedListener;

    .line 371
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v0}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->destroy()V

    .line 376
    iput-object v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    .line 379
    :cond_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 380
    return-void

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final declared-synchronized txMessage([B)Z
    .locals 4
    .param p1, "message"    # [B

    .prologue
    const/4 v0, 0x0

    .line 429
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 431
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketInterfaceLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    :try_start_1
    iget v2, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mEnabledState:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 435
    const-string v2, "ANTSocketInterface"

    const-string v3, "ANTTxMessage: failed, Ant not enabled"

    invoke-static {v2, v3}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 457
    :goto_0
    monitor-exit p0

    return v0

    .line 438
    :cond_0
    :try_start_2
    monitor-exit v1

    .line 440
    if-eqz p1, :cond_1

    array-length v1, p1

    if-nez v1, :cond_2

    .line 442
    :cond_1
    const-string v1, "ANTSocketInterface"

    const-string v2, "ANTTxMessage: No message provided, skipping request."

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 429
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 438
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0

    .line 446
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v1}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->isConnected()Z

    move-result v1

    if-nez v1, :cond_3

    .line 448
    const-string v1, "ANTSocketInterface"

    const-string v2, "ANTTxMessage: No connection to ANT chip."

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 453
    :cond_3
    iget-object v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mSocketThread:Lcom/dsi/ant/chip/socket/AntSocketManagerThread;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/chip/socket/AntSocketManagerThread;->ANTTxMessage([B)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mTxSuccess:Z

    .line 455
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->d$16da05f7()V

    .line 457
    iget-boolean v0, p0, Lcom/dsi/ant/chip/socket/AntSocketInterface;->mTxSuccess:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
