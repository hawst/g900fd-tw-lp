.class final Lcom/dsi/ant/chip/hal/AntAidlInterface$4;
.super Lcom/dsi/ant/server/IAntHalCallback$Stub;
.source "AntAidlInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/chip/hal/AntAidlInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;


# direct methods
.method constructor <init>(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-direct {p0}, Lcom/dsi/ant/server/IAntHalCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final antHalRxMessage([B)V
    .locals 4
    .param p1, "message"    # [B

    .prologue
    .line 480
    invoke-static {}, Lcom/dsi/ant/util/LogAnt;->v$16da05f7()V

    .line 482
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 484
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    .line 487
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAdapter:Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1400(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;

    move-result-object v1

    iget-object v1, v1, Lcom/dsi/ant/chip/hal/AntAidlInterface$AidlAdapter;->mCallback:Landroid/os/Messenger;

    invoke-static {v1, p1}, Lcom/dsi/ant/chip/ChipCallbackHelper;->sendRxMessage(Landroid/os/Messenger;[B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 493
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    return-void

    .line 488
    :catch_0
    move-exception v0

    .line 490
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ANTAidlInterface"

    const-string v3, "Impossible Remote Exception."

    invoke-static {v1, v3, v0}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 493
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final antHalStateChanged(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    .line 395
    const-string v2, "ANTAidlInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "ANT HAL State changed to "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "STATE: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/dsi/ant/util/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mAidlInterfaceLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 402
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mPreviousState:I
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1700(Lcom/dsi/ant/chip/hal/AntAidlInterface;)I

    move-result v0

    .line 403
    .local v0, "newState":I
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # invokes: Lcom/dsi/ant/chip/hal/AntAidlInterface;->clearStateOverrides()V
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1800(Lcom/dsi/ant/chip/hal/AntAidlInterface;)V

    .line 410
    packed-switch p1, :pswitch_data_1

    .line 466
    :pswitch_0
    const-string v1, "ANTAidlInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HAL state changed to unknown state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/util/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const/4 v0, 0x0

    .line 472
    :goto_1
    const/16 v1, 0x9

    if-eq p1, v1, :cond_0

    .line 474
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-static {v1, v0}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$800(Lcom/dsi/ant/chip/hal/AntAidlInterface;I)V

    .line 476
    :cond_0
    return-void

    .line 395
    .end local v0    # "newState":I
    :pswitch_1
    const-string v1, "STATE UNKNOWN"

    goto :goto_0

    :pswitch_2
    const-string v1, "ENABLING"

    goto :goto_0

    :pswitch_3
    const-string v1, "ENABLED"

    goto :goto_0

    :pswitch_4
    const-string v1, "DISABLING"

    goto :goto_0

    :pswitch_5
    const-string v1, "DISABLED"

    goto :goto_0

    :pswitch_6
    const-string v1, "ANT NOT SUPPORTED"

    goto :goto_0

    :pswitch_7
    const-string v1, "ANT HAL SERVICE NOT INSTALLED"

    goto :goto_0

    :pswitch_8
    const-string v1, "ANT HAL SERVICE NOT CONNECTED"

    goto :goto_0

    :pswitch_9
    const-string v1, "RESETTING"

    goto :goto_0

    :pswitch_a
    const-string v1, "RESET"

    goto :goto_0

    .line 403
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 414
    .restart local v0    # "newState":I
    :pswitch_b
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mEnablingSent:Z
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1900(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 416
    const/4 v0, 0x1

    goto :goto_1

    .line 420
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$1902$5ff94f9e(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Z

    goto :goto_1

    .line 426
    :pswitch_c
    const/4 v0, 0x2

    .line 427
    goto :goto_1

    .line 431
    :pswitch_d
    const/4 v0, 0x3

    .line 432
    goto :goto_1

    .line 436
    :pswitch_e
    const/4 v0, 0x0

    .line 437
    goto :goto_1

    .line 441
    :pswitch_f
    const/4 v0, 0x4

    .line 442
    goto :goto_1

    .line 454
    :pswitch_10
    iget-object v1, p0, Lcom/dsi/ant/chip/hal/AntAidlInterface$4;->this$0:Lcom/dsi/ant/chip/hal/AntAidlInterface;

    # getter for: Lcom/dsi/ant/chip/hal/AntAidlInterface;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/dsi/ant/chip/hal/AntAidlInterface;->access$2000(Lcom/dsi/ant/chip/hal/AntAidlInterface;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/dsi/ant/chip/hal/AntAidlInterface$4$1;

    invoke-direct {v2, p0}, Lcom/dsi/ant/chip/hal/AntAidlInterface$4$1;-><init>(Lcom/dsi/ant/chip/hal/AntAidlInterface$4;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 410
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method
