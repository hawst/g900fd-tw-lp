.class public final Lcom/dsi/ant/chip/ChipCallbackHelper;
.super Ljava/lang/Object;
.source "ChipCallbackHelper.java"


# direct methods
.method public static sendRxMessage(Landroid/os/Messenger;[B)V
    .locals 4
    .param p0, "messenger"    # Landroid/os/Messenger;
    .param p1, "message"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 39
    if-nez p0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 42
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "com.dsi.ant.service.hardwareinterface.chip.key.DATA_BYTE_ARRAY"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 43
    const/4 v2, 0x0

    sget-object v3, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->RX_DATA:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    invoke-virtual {v3}, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->ordinal()I

    move-result v3

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 44
    .local v1, "ipcMessage":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public static sendStateUpdate(Landroid/os/Messenger;II)V
    .locals 2
    .param p0, "messenger"    # Landroid/os/Messenger;
    .param p1, "oldChipState"    # I
    .param p2, "newChipState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 24
    if-nez p0, :cond_0

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->CHIP_STATE_CHANGED:Lcom/dsi/ant/chip/ChipCallbackMessageIds;

    invoke-virtual {v1}, Lcom/dsi/ant/chip/ChipCallbackMessageIds;->ordinal()I

    move-result v1

    invoke-static {v0, v1, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    goto :goto_0
.end method
