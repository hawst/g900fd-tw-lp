.class public abstract Lcom/dsi/ant/IAnt$Stub;
.super Landroid/os/Binder;
.source "IAnt.java"

# interfaces
.implements Lcom/dsi/ant/IAnt;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/IAnt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 25
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p0, p0, v0}, Lcom/dsi/ant/IAnt$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 44
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 48
    sparse-switch p1, :sswitch_data_0

    .line 378
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 52
    :sswitch_0
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :sswitch_1
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->enable()Z

    move-result v6

    .line 59
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 60
    if-eqz v6, :cond_0

    move v0, v7

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    move v0, v8

    goto :goto_1

    .line 65
    .end local v6    # "_result":Z
    :sswitch_2
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->disable()Z

    move-result v6

    .line 67
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v6, :cond_1

    move v8, v7

    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 73
    .end local v6    # "_result":Z
    :sswitch_3
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->isEnabled()Z

    move-result v6

    .line 75
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    if-eqz v6, :cond_2

    move v8, v7

    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 81
    .end local v6    # "_result":Z
    :sswitch_4
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 84
    .local v1, "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt$Stub;->ANTTxMessage([B)Z

    move-result v6

    .line 85
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    if-eqz v6, :cond_3

    move v8, v7

    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 91
    .end local v1    # "_arg0":[B
    .end local v6    # "_result":Z
    :sswitch_5
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->ANTResetSystem()Z

    move-result v6

    .line 93
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 94
    if-eqz v6, :cond_4

    move v8, v7

    :cond_4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 99
    .end local v6    # "_result":Z
    :sswitch_6
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 102
    .local v1, "_arg0":B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt$Stub;->ANTUnassignChannel(B)Z

    move-result v6

    .line 103
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 104
    if-eqz v6, :cond_5

    move v8, v7

    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 109
    .end local v1    # "_arg0":B
    .end local v6    # "_result":Z
    :sswitch_7
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 113
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 115
    .local v2, "_arg1":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 116
    .local v3, "_arg2":B
    invoke-virtual {p0, v1, v2, v3}, Lcom/dsi/ant/IAnt$Stub;->ANTAssignChannel(BBB)Z

    move-result v6

    .line 117
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 118
    if-eqz v6, :cond_6

    move v8, v7

    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 123
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v3    # "_arg2":B
    .end local v6    # "_result":Z
    :sswitch_8
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 127
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 129
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 131
    .restart local v3    # "_arg2":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v4

    .line 132
    .local v4, "_arg3":B
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/dsi/ant/IAnt$Stub;->ANTSetChannelId(BIBB)Z

    move-result v6

    .line 133
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    if-eqz v6, :cond_7

    move v8, v7

    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 139
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":B
    .end local v4    # "_arg3":B
    .end local v6    # "_result":Z
    :sswitch_9
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 143
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 144
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSetChannelPeriod(BI)Z

    move-result v6

    .line 145
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 146
    if-eqz v6, :cond_8

    move v8, v7

    :cond_8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 151
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":I
    .end local v6    # "_result":Z
    :sswitch_a
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 155
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 156
    .local v2, "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSetChannelRFFreq(BB)Z

    move-result v6

    .line 157
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 158
    if-eqz v6, :cond_9

    move v8, v7

    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 163
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_b
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 167
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 168
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSetChannelSearchTimeout(BB)Z

    move-result v6

    .line 169
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 170
    if-eqz v6, :cond_a

    move v8, v7

    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 175
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_c
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 179
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 180
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSetLowPriorityChannelSearchTimeout(BB)Z

    move-result v6

    .line 181
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 182
    if-eqz v6, :cond_b

    move v8, v7

    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 187
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_d
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 191
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 192
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSetProximitySearch(BB)Z

    move-result v6

    .line 193
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    if-eqz v6, :cond_c

    move v8, v7

    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 199
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_e
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 203
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 204
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSetChannelTxPower(BB)Z

    move-result v6

    .line 205
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 206
    if-eqz v6, :cond_d

    move v8, v7

    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 211
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_f
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 215
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 217
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 219
    .restart local v3    # "_arg2":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v4

    .line 221
    .restart local v4    # "_arg3":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v5

    .local v5, "_arg4":B
    move-object v0, p0

    .line 222
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/IAnt$Stub;->ANTAddChannelId(BIBBB)Z

    move-result v6

    .line 223
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    if-eqz v6, :cond_e

    move v8, v7

    :cond_e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 229
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":B
    .end local v4    # "_arg3":B
    .end local v5    # "_arg4":B
    .end local v6    # "_result":Z
    :sswitch_10
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 233
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 235
    .local v2, "_arg1":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 236
    .restart local v3    # "_arg2":B
    invoke-virtual {p0, v1, v2, v3}, Lcom/dsi/ant/IAnt$Stub;->ANTConfigList(BBB)Z

    move-result v6

    .line 237
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 238
    if-eqz v6, :cond_f

    move v8, v7

    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 243
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v3    # "_arg2":B
    .end local v6    # "_result":Z
    :sswitch_11
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 246
    .restart local v1    # "_arg0":B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt$Stub;->ANTOpenChannel(B)Z

    move-result v6

    .line 247
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 248
    if-eqz v6, :cond_10

    move v8, v7

    :cond_10
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 253
    .end local v1    # "_arg0":B
    .end local v6    # "_result":Z
    :sswitch_12
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 256
    .restart local v1    # "_arg0":B
    invoke-virtual {p0, v1}, Lcom/dsi/ant/IAnt$Stub;->ANTCloseChannel(B)Z

    move-result v6

    .line 257
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 258
    if-eqz v6, :cond_11

    move v8, v7

    :cond_11
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 263
    .end local v1    # "_arg0":B
    .end local v6    # "_result":Z
    :sswitch_13
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 267
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 268
    .restart local v2    # "_arg1":B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTRequestMessage(BB)Z

    move-result v6

    .line 269
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 270
    if-eqz v6, :cond_12

    move v8, v7

    :cond_12
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 275
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":B
    .end local v6    # "_result":Z
    :sswitch_14
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 279
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 280
    .local v2, "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSendBroadcastData(B[B)Z

    move-result v6

    .line 281
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 282
    if-eqz v6, :cond_13

    move v8, v7

    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 287
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":Z
    :sswitch_15
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 291
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 292
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSendAcknowledgedData(B[B)Z

    move-result v6

    .line 293
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 294
    if-eqz v6, :cond_14

    move v8, v7

    :cond_14
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 299
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":Z
    :sswitch_16
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 301
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 303
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 304
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSendBurstTransferPacket(B[B)Z

    move-result v6

    .line 305
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 306
    if-eqz v6, :cond_15

    move v8, v7

    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 311
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":Z
    :sswitch_17
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 315
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 316
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/dsi/ant/IAnt$Stub;->ANTSendBurstTransfer(B[B)I

    move-result v6

    .line 317
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 318
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 323
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v6    # "_result":I
    :sswitch_18
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 327
    .restart local v1    # "_arg0":B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 329
    .restart local v2    # "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 331
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    move v4, v7

    .line 332
    .local v4, "_arg3":Z
    :goto_2
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/dsi/ant/IAnt$Stub;->ANTTransmitBurst(B[BIZ)I

    move-result v6

    .line 333
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 334
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v4    # "_arg3":Z
    .end local v6    # "_result":I
    :cond_16
    move v4, v8

    .line 331
    goto :goto_2

    .line 339
    .end local v1    # "_arg0":B
    .end local v2    # "_arg1":[B
    .end local v3    # "_arg2":I
    :sswitch_19
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 343
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 345
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 347
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 348
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/dsi/ant/IAnt$Stub;->ANTConfigEventBuffering(IIII)Z

    move-result v6

    .line 349
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 350
    if-eqz v6, :cond_17

    move v8, v7

    :cond_17
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 355
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v6    # "_result":Z
    :sswitch_1a
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 356
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->ANTDisableEventBuffering()Z

    move-result v6

    .line 357
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 358
    if-eqz v6, :cond_18

    move v8, v7

    :cond_18
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 363
    .end local v6    # "_result":Z
    :sswitch_1b
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->getServiceLibraryVersionCode()I

    move-result v6

    .line 365
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 366
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 371
    .end local v6    # "_result":I
    :sswitch_1c
    const-string v0, "com.dsi.ant.IAnt"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p0}, Lcom/dsi/ant/IAnt$Stub;->getServiceLibraryVersionName()Ljava/lang/String;

    move-result-object v6

    .line 373
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 374
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
