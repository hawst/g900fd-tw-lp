.class public final Lcom/dsi/ant/util/LogAnt;
.super Ljava/lang/Object;
.source "LogAnt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/util/LogAnt$DebugLevels;
    }
.end annotation


# static fields
.field public static final DEBUG_LEVEL_DEFAULT:I

.field private static sDebugLevel:I

.field private static sSerialDebug:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dsi/ant/util/LogAnt;->sSerialDebug:Z

    .line 41
    sget-object v0, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ERROR:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v0}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    move-result v0

    .line 42
    sput v0, Lcom/dsi/ant/util/LogAnt;->DEBUG_LEVEL_DEFAULT:I

    sput v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    return-void
.end method

.method public static final d$16da05f7()V
    .locals 1

    .prologue
    .line 86
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v0, Lcom/dsi/ant/util/LogAnt$DebugLevels;->DEBUG:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v0}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    .line 87
    return-void
.end method

.method public static final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 56
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ERROR:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v1}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 57
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    return-void
.end method

.method public static final e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "th"    # Ljava/lang/Throwable;

    .prologue
    .line 62
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ERROR:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v1}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 63
    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 64
    :cond_0
    return-void
.end method

.method public static getDebugLevel()I
    .locals 1

    .prologue
    .line 51
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    return v0
.end method

.method public static getHexString([B)Ljava/lang/String;
    .locals 7
    .param p0, "data"    # [B

    .prologue
    .line 130
    if-nez p0, :cond_0

    .line 132
    const-string v2, ""

    .line 141
    :goto_0
    return-object v2

    .line 135
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 136
    .local v0, "hexString":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 138
    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "%02X"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aget-byte v6, p0, v1

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 141
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getSerialDebug()Z
    .locals 1

    .prologue
    .line 28
    sget-boolean v0, Lcom/dsi/ant/util/LogAnt;->sSerialDebug:Z

    return v0
.end method

.method public static final i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 80
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/util/LogAnt$DebugLevels;->INFO:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v1}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 81
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    return-void
.end method

.method public static setDebugLevel(I)V
    .locals 0
    .param p0, "level"    # I

    .prologue
    .line 46
    sput p0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    .line 47
    return-void
.end method

.method public static setSerialDebug(Z)V
    .locals 0
    .param p0, "on"    # Z

    .prologue
    .line 23
    sput-boolean p0, Lcom/dsi/ant/util/LogAnt;->sSerialDebug:Z

    .line 24
    return-void
.end method

.method public static final v$16da05f7()V
    .locals 1

    .prologue
    .line 92
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v0, Lcom/dsi/ant/util/LogAnt$DebugLevels;->VERBOSE:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v0}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    .line 93
    return-void
.end method

.method public static final w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 68
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/util/LogAnt$DebugLevels;->WARNING:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v1}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 69
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    return-void
.end method

.method public static final w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "th"    # Ljava/lang/Throwable;

    .prologue
    .line 74
    sget v0, Lcom/dsi/ant/util/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/util/LogAnt$DebugLevels;->WARNING:Lcom/dsi/ant/util/LogAnt$DebugLevels;

    invoke-virtual {v1}, Lcom/dsi/ant/util/LogAnt$DebugLevels;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 75
    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    :cond_0
    return-void
.end method
