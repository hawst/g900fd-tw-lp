.class public Landroid/provider/Telephony$Sms;
.super Ljava/lang/Object;
.source "Telephony.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sms"
.end annotation


# static fields
.field public static ADDRESS:Ljava/lang/String; = null

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final PRIORITY:Ljava/lang/String; = "pri"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static SUBJECT_CHARSET:Ljava/lang/String; = null

.field public static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "sub_cs"

    sput-object v0, Landroid/provider/Telephony$Sms;->SUBJECT_CHARSET:Ljava/lang/String;

    .line 18
    const-string/jumbo v0, "address"

    sput-object v0, Landroid/provider/Telephony$Sms;->ADDRESS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
