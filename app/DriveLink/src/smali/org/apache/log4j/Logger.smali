.class public Lorg/apache/log4j/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/log4j/Logger$1;,
        Lorg/apache/log4j/Logger$Level;
    }
.end annotation


# static fields
.field private static final configMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/log4j/Logger$Level;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_ClassName:Ljava/lang/String;

.field private m_isEnabled:Z

.field private m_level:Lorg/apache/log4j/Logger$Level;

.field private m_prefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11
    new-instance v0, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    const-string/jumbo v1, "java.lang.Object"

    sget-object v2, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo"

    sget-object v2, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.dialog"

    sget-object v2, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.dialog.nlg"

    sget-object v2, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "org.hibernate"

    sget-object v2, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lorg/apache/log4j/Logger;->configMap:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/log4j/Logger$Level;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "level"    # Lorg/apache/log4j/Logger$Level;
    .param p4, "enabled"    # Z

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/log4j/Logger;->m_isEnabled:Z

    .line 111
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 112
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/log4j/Logger;->m_ClassName:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lorg/apache/log4j/Logger;->m_prefix:Ljava/lang/String;

    .line 114
    iput-boolean p4, p0, Lorg/apache/log4j/Logger;->m_isEnabled:Z

    .line 115
    iput-object p3, p0, Lorg/apache/log4j/Logger;->m_level:Lorg/apache/log4j/Logger$Level;

    .line 116
    return-void
.end method

.method protected static getLevel(Ljava/lang/Class;)Lorg/apache/log4j/Logger$Level;
    .locals 6
    .param p0, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 85
    move-object v0, p0

    .local v0, "c":Ljava/lang/Class;
    :goto_0
    if-eqz v0, :cond_4

    .line 86
    sget-object v4, Lorg/apache/log4j/Logger;->configMap:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/log4j/Logger$Level;

    .line 87
    .local v1, "level":Lorg/apache/log4j/Logger$Level;
    if-eqz v1, :cond_0

    move-object v2, v1

    .line 94
    .end local v1    # "level":Lorg/apache/log4j/Logger$Level;
    .local v2, "level":Lorg/apache/log4j/Logger$Level;
    :goto_1
    return-object v2

    .line 91
    .end local v2    # "level":Lorg/apache/log4j/Logger$Level;
    .restart local v1    # "level":Lorg/apache/log4j/Logger$Level;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    .local v3, "p":Ljava/lang/String;
    :goto_2
    if-eqz v3, :cond_2

    .line 92
    sget-object v4, Lorg/apache/log4j/Logger;->configMap:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "level":Lorg/apache/log4j/Logger$Level;
    check-cast v1, Lorg/apache/log4j/Logger$Level;

    .line 93
    .restart local v1    # "level":Lorg/apache/log4j/Logger$Level;
    if-eqz v1, :cond_1

    move-object v2, v1

    .line 94
    .end local v1    # "level":Lorg/apache/log4j/Logger$Level;
    .restart local v2    # "level":Lorg/apache/log4j/Logger$Level;
    goto :goto_1

    .line 96
    .end local v2    # "level":Lorg/apache/log4j/Logger$Level;
    .restart local v1    # "level":Lorg/apache/log4j/Logger$Level;
    :cond_1
    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 85
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_3
    const-string/jumbo v4, "\\.[^.]*$"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 102
    .end local v1    # "level":Lorg/apache/log4j/Logger$Level;
    .end local v3    # "p":Ljava/lang/String;
    :cond_4
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string/jumbo v5, "no logger found"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;
    .locals 1
    .param p0, "class1"    # Ljava/lang/Class;

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;)Lorg/apache/log4j/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Ljava/lang/String;)Lorg/apache/log4j/Logger;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/log4j/Logger;"
        }
    .end annotation

    .prologue
    .line 135
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lorg/apache/log4j/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lorg/apache/log4j/Logger;
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lorg/apache/log4j/Logger;"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p0, :cond_0

    .line 140
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 141
    :cond_0
    if-nez p1, :cond_1

    .line 142
    const-string/jumbo p1, "EDM_"

    .line 144
    :cond_1
    new-instance v0, Lorg/apache/log4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lorg/apache/log4j/Logger;->getLevel(Ljava/lang/Class;)Lorg/apache/log4j/Logger$Level;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2, p2}, Lorg/apache/log4j/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/log4j/Logger$Level;Z)V

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Z)Lorg/apache/log4j/Logger;
    .locals 2
    .param p1, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)",
            "Lorg/apache/log4j/Logger;"
        }
    .end annotation

    .prologue
    .line 131
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lorg/apache/log4j/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/String;)Lorg/apache/log4j/Logger;
    .locals 3
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 120
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;)Lorg/apache/log4j/Logger;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getThread()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isEnabled(Lorg/apache/log4j/Logger$Level;)Z
    .locals 1
    .param p1, "threshold"    # Lorg/apache/log4j/Logger$Level;

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/log4j/Logger;->m_level:Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0, p1}, Lorg/apache/log4j/Logger$Level;->compare(Lorg/apache/log4j/Logger$Level;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/log4j/Logger;->m_isEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "threshold"    # Lorg/apache/log4j/Logger$Level;
    .param p2, "object"    # Ljava/lang/Object;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 279
    return-void
.end method

.method private log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "threshold"    # Lorg/apache/log4j/Logger$Level;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 291
    return-void
.end method


# virtual methods
.method public assertLog(ZLjava/lang/String;)V
    .locals 0
    .param p1, "b"    # Z
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 253
    if-nez p1, :cond_0

    .line 254
    invoke-virtual {p0, p2}, Lorg/apache/log4j/Logger;->error(Ljava/lang/String;)V

    .line 256
    :cond_0
    return-void
.end method

.method public debug(Ljava/lang/Object;)V
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 208
    sget-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 209
    return-void
.end method

.method public debug(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 212
    sget-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 213
    return-void
.end method

.method public debug(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 148
    sget-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 149
    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 152
    sget-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    return-void
.end method

.method public error(Ljava/lang/Object;)V
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 244
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 245
    return-void
.end method

.method public error(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 248
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 249
    return-void
.end method

.method public error(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 172
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 173
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 168
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 169
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 164
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p3, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 176
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    return-void
.end method

.method public getResourceBundle()Ljava/util/ResourceBundle;
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return-object v0
.end method

.method public info(Ljava/lang/Object;)V
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 220
    sget-object v0, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 221
    return-void
.end method

.method public info(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 224
    sget-object v0, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 225
    return-void
.end method

.method public info(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 184
    sget-object v0, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 180
    sget-object v0, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    return-void
.end method

.method public isDebugEnabled()Z
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0}, Lorg/apache/log4j/Logger;->isEnabled(Lorg/apache/log4j/Logger$Level;)Z

    move-result v0

    return v0
.end method

.method public isErrorEnabled()Z
    .locals 1

    .prologue
    .line 240
    sget-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0}, Lorg/apache/log4j/Logger;->isEnabled(Lorg/apache/log4j/Logger$Level;)Z

    move-result v0

    return v0
.end method

.method public isInfoEnabled()Z
    .locals 1

    .prologue
    .line 216
    sget-object v0, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0}, Lorg/apache/log4j/Logger;->isEnabled(Lorg/apache/log4j/Logger$Level;)Z

    move-result v0

    return v0
.end method

.method public isTraceEnabled()Z
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lorg/apache/log4j/Logger;->isDebugEnabled()Z

    move-result v0

    return v0
.end method

.method public isWarnEnabled()Z
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lorg/apache/log4j/Logger$Level;->WARN:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0}, Lorg/apache/log4j/Logger;->isEnabled(Lorg/apache/log4j/Logger$Level;)Z

    move-result v0

    return v0
.end method

.method public setResourceBundle(Ljava/util/ResourceBundle;)V
    .locals 0
    .param p1, "resourceBundle"    # Ljava/util/ResourceBundle;

    .prologue
    .line 263
    return-void
.end method

.method public trace(Ljava/lang/Object;)V
    .locals 0
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;)V

    .line 197
    return-void
.end method

.method public trace(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 200
    invoke-virtual {p0, p1, p2}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 201
    return-void
.end method

.method public warn(Ljava/lang/Object;)V
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 232
    sget-object v0, Lorg/apache/log4j/Logger$Level;->WARN:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 233
    return-void
.end method

.method public warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 236
    sget-object v0, Lorg/apache/log4j/Logger$Level;->WARN:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 237
    return-void
.end method

.method public warn(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 156
    sget-object v0, Lorg/apache/log4j/Logger$Level;->WARN:Lorg/apache/log4j/Logger$Level;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 157
    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 160
    sget-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Logger$Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    return-void
.end method
