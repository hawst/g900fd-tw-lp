.class public Lorg/apache/commons/jexl2/parser/Token;
.super Ljava/lang/Object;
.source "Token.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public beginColumn:I

.field public beginLine:I

.field public endColumn:I

.field public endLine:I

.field public image:Ljava/lang/String;

.field public kind:I

.field public next:Lorg/apache/commons/jexl2/parser/Token;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "kind"    # I
    .param p2, "image"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput p1, p0, Lorg/apache/commons/jexl2/parser/Token;->kind:I

    .line 94
    iput-object p2, p0, Lorg/apache/commons/jexl2/parser/Token;->image:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public static newToken(ILjava/lang/String;)Lorg/apache/commons/jexl2/parser/Token;
    .locals 1
    .param p0, "ofKind"    # I
    .param p1, "image"    # Ljava/lang/String;

    .prologue
    .line 119
    .line 121
    new-instance v0, Lorg/apache/commons/jexl2/parser/Token;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/jexl2/parser/Token;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/Token;->image:Ljava/lang/String;

    return-object v0
.end method
