.class public final Lorg/apache/commons/jexl2/internal/ListGetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
.source "ListGetExecutor.java"


# static fields
.field private static final ARRAY_GET:Ljava/lang/reflect/Method;

.field private static final LIST_GET:Ljava/lang/reflect/Method;


# instance fields
.field private final property:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    const-class v0, Ljava/lang/reflect/Array;

    const-string/jumbo v1, "get"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/Object;

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->initMarker(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->ARRAY_GET:Ljava/lang/reflect/Method;

    .line 30
    const-class v0, Ljava/util/List;

    const-string/jumbo v1, "get"

    new-array v2, v5, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->initMarker(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->LIST_GET:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "key"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p2}, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->discover(Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 43
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->property:Ljava/lang/Integer;

    .line 44
    return-void
.end method

.method static discover(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->ARRAY_GET:Ljava/lang/reflect/Method;

    .line 95
    :goto_0
    return-object v0

    .line 92
    :cond_0
    const-class v0, Ljava/util/List;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    sget-object v0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->LIST_GET:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->method:Ljava/lang/reflect/Method;

    sget-object v1, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->ARRAY_GET:Ljava/lang/reflect/Method;

    if-ne v0, v1, :cond_0

    .line 60
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->property:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 62
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    check-cast p1, Ljava/util/List;

    .end local p1    # "obj":Ljava/lang/Object;
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->property:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->property:Ljava/lang/Integer;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 69
    if-eqz p1, :cond_1

    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->method:Ljava/lang/reflect/Method;

    sget-object v1, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->ARRAY_GET:Ljava/lang/reflect/Method;

    if-ne v0, v1, :cond_0

    .line 73
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "key":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 78
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 75
    .restart local p1    # "obj":Ljava/lang/Object;
    .restart local p2    # "key":Ljava/lang/Object;
    :cond_0
    check-cast p1, Ljava/util/List;

    .end local p1    # "obj":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "key":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 78
    .restart local p1    # "obj":Ljava/lang/Object;
    .restart local p2    # "key":Ljava/lang/Object;
    :cond_1
    sget-object v0, Lorg/apache/commons/jexl2/internal/ListGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
