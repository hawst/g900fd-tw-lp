.class final Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
.super Ljava/lang/Object;
.source "ClassMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;
    }
.end annotation


# instance fields
.field private final fieldCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field

.field private final methodCache:Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;


# direct methods
.method constructor <init>(Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V
    .locals 1
    .param p2, "log"    # Lorg/apache/commons/logging/Log;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/logging/Log;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->createMethodCache(Ljava/lang/Class;Lorg/apache/commons/logging/Log;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->methodCache:Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;

    .line 61
    invoke-static {p1}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->createFieldCache(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->fieldCache:Ljava/util/Map;

    .line 62
    return-void
.end method

.method private static createFieldCache(Ljava/lang/Class;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 90
    .local v3, "fields":[Ljava/lang/reflect/Field;
    array-length v6, v3

    if-lez v6, :cond_0

    .line 91
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 92
    .local v1, "cache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 93
    .local v2, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 97
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v1    # "cache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private static createMethodCache(Ljava/lang/Class;Lorg/apache/commons/logging/Log;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;
    .locals 4
    .param p1, "log"    # Lorg/apache/commons/logging/Log;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/logging/Log;",
            ")",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "classToReflect":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;-><init>()V

    .line 145
    .local v0, "cache":Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;
    :goto_0
    if-eqz p0, :cond_2

    .line 146
    invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I

    move-result v3

    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 147
    invoke-static {v0, p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->populateMethodCacheWith(Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V

    .line 149
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v2

    .line 150
    .local v2, "interfaces":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 151
    aget-object v3, v2, v1

    invoke-static {v0, v3, p1}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->populateMethodCacheWithInterface(Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V

    .line 150
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 145
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_0

    .line 154
    .end local v1    # "i":I
    .end local v2    # "interfaces":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_2
    return-object v0
.end method

.method private static populateMethodCacheWith(Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V
    .locals 6
    .param p0, "cache"    # Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;
    .param p2, "log"    # Lorg/apache/commons/logging/Log;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/logging/Log;",
            ")V"
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 182
    .local v1, "methods":[Ljava/lang/reflect/Method;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 183
    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v2

    .line 184
    .local v2, "modifiers":I
    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 185
    aget-object v4, v1, v0

    invoke-virtual {p0, v4}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->put(Ljava/lang/reflect/Method;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "i":I
    .end local v1    # "methods":[Ljava/lang/reflect/Method;
    .end local v2    # "modifiers":I
    :catch_0
    move-exception v3

    .line 190
    .local v3, "se":Ljava/lang/SecurityException;
    invoke-interface {p2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 191
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "While accessing methods of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 194
    .end local v3    # "se":Ljava/lang/SecurityException;
    :cond_1
    return-void
.end method

.method private static populateMethodCacheWithInterface(Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V
    .locals 3
    .param p0, "cache"    # Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;
    .param p2, "log"    # Lorg/apache/commons/logging/Log;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/logging/Log;",
            ")V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "iface":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    invoke-static {p0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->populateMethodCacheWith(Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V

    .line 167
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v1

    .line 168
    .local v1, "supers":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 169
    aget-object v2, v1, v0

    invoke-static {p0, v2, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->populateMethodCacheWithInterface(Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_1
    return-void
.end method


# virtual methods
.method findField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 1
    .param p2, "fname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->fieldCache:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    return-object v0
.end method

.method findMethod(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    .locals 1
    .param p1, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->methodCache:Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->get(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method
