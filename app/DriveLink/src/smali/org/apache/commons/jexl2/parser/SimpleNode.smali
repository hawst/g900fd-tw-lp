.class public Lorg/apache/commons/jexl2/parser/SimpleNode;
.super Ljava/lang/Object;
.source "SimpleNode.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/Node;


# instance fields
.field protected children:[Lorg/apache/commons/jexl2/parser/JexlNode;

.field protected final id:I

.field protected parent:Lorg/apache/commons/jexl2/parser/JexlNode;

.field protected volatile value:Ljava/lang/Object;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->id:I

    .line 50
    return-void
.end method


# virtual methods
.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 138
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/SimpleNode;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public jjtAddChild(Lorg/apache/commons/jexl2/parser/Node;I)V
    .locals 4
    .param p1, "n"    # Lorg/apache/commons/jexl2/parser/Node;
    .param p2, "i"    # I

    .prologue
    const/4 v3, 0x0

    .line 90
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    if-nez v1, :cond_1

    .line 91
    add-int/lit8 v1, p2, 0x1

    new-array v1, v1, [Lorg/apache/commons/jexl2/parser/JexlNode;

    iput-object v1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 97
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    check-cast p1, Lorg/apache/commons/jexl2/parser/JexlNode;

    .end local p1    # "n":Lorg/apache/commons/jexl2/parser/Node;
    aput-object p1, v1, p2

    .line 98
    return-void

    .line 92
    .restart local p1    # "n":Lorg/apache/commons/jexl2/parser/Node;
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    array-length v1, v1

    if-lt p2, v1, :cond_0

    .line 93
    add-int/lit8 v1, p2, 0x1

    new-array v0, v1, [Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 94
    .local v0, "c":[Lorg/apache/commons/jexl2/parser/JexlNode;
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    goto :goto_0
.end method

.method public jjtClose()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public jjtGetNumChildren()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    array-length v0, v0

    goto :goto_0
.end method

.method public jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->parent:Lorg/apache/commons/jexl2/parser/JexlNode;

    return-object v0
.end method

.method public jjtGetValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public jjtOpen()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public jjtSetParent(Lorg/apache/commons/jexl2/parser/Node;)V
    .locals 0
    .param p1, "n"    # Lorg/apache/commons/jexl2/parser/Node;

    .prologue
    .line 74
    check-cast p1, Lorg/apache/commons/jexl2/parser/JexlNode;

    .end local p1    # "n":Lorg/apache/commons/jexl2/parser/Node;
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->parent:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 75
    return-void
.end method

.method public jjtSetValue(Ljava/lang/Object;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 121
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->value:Ljava/lang/Object;

    .line 122
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 163
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTreeConstants;->jjtNodeName:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/commons/jexl2/parser/SimpleNode;->id:I

    aget-object v0, v0, v1

    return-object v0
.end method
