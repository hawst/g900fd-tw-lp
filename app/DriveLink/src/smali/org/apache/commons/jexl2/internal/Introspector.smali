.class public Lorg/apache/commons/jexl2/internal/Introspector;
.super Ljava/lang/Object;
.source "Introspector.java"


# instance fields
.field private volatile ref:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;",
            ">;"
        }
    .end annotation
.end field

.field protected final rlog:Lorg/apache/commons/logging/Log;


# direct methods
.method protected constructor <init>(Lorg/apache/commons/logging/Log;)V
    .locals 2
    .param p1, "log"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/Introspector;->rlog:Lorg/apache/commons/logging/Log;

    .line 46
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/Introspector;->ref:Ljava/lang/ref/SoftReference;

    .line 47
    return-void
.end method


# virtual methods
.method protected final base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    .locals 4

    .prologue
    .line 84
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/Introspector;->ref:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    .line 85
    .local v1, "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    if-nez v1, :cond_1

    .line 87
    monitor-enter p0

    .line 88
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/Introspector;->ref:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-object v1, v0

    .line 89
    if-nez v1, :cond_0

    .line 90
    new-instance v2, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/Introspector;->rlog:Lorg/apache/commons/logging/Log;

    invoke-direct {v2, v3}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;-><init>(Lorg/apache/commons/logging/Log;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    .end local v1    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    .local v2, "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    :try_start_1
    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, p0, Lorg/apache/commons/jexl2/internal/Introspector;->ref:Ljava/lang/ref/SoftReference;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 93
    .end local v2    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    .restart local v1    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    :cond_0
    :try_start_2
    monitor-exit p0

    .line 95
    :cond_1
    return-object v1

    .line 93
    :catchall_0
    move-exception v3

    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .end local v1    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    .restart local v2    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    .restart local v1    # "intro":Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
    goto :goto_0
.end method

.method public final getConstructor(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/reflect/Constructor;
    .locals 4
    .param p1, "ctorHandle"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "className":Ljava/lang/String;
    const/4 v1, 0x0

    .line 178
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    instance-of v2, p1, Ljava/lang/Class;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 179
    check-cast v1, Ljava/lang/Class;

    .line 180
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 186
    :goto_0
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/Introspector;->base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-result-object v2

    new-instance v3, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-direct {v3, v0, p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2, v1, v3}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getConstructor(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    :goto_1
    return-object v2

    .line 181
    :cond_0
    if-eqz p1, :cond_1

    .line 182
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected final getField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 1
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/Introspector;->base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    return-object v0
.end method

.method public final getGetExecutor(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;

    .prologue
    .line 208
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 209
    .local v0, "claz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/internal/Introspector;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 212
    .local v3, "property":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 213
    new-instance v1, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;

    invoke-direct {v1, p0, v0, v3}, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)V

    .line 214
    .local v1, "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v4, v1

    .line 244
    :goto_0
    return-object v4

    .line 219
    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    :cond_0
    if-eqz v3, :cond_1

    .line 220
    new-instance v1, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;

    invoke-direct {v1, p0, v0, v3}, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)V

    .line 221
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v1

    .line 222
    goto :goto_0

    .line 226
    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    :cond_1
    new-instance v1, Lorg/apache/commons/jexl2/internal/MapGetExecutor;

    invoke-direct {v1, p0, v0, p2}, Lorg/apache/commons/jexl2/internal/MapGetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 227
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, v1

    .line 228
    goto :goto_0

    .line 232
    :cond_2
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/internal/Introspector;->toInteger(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    .line 233
    .local v2, "index":Ljava/lang/Integer;
    if-eqz v2, :cond_3

    .line 234
    new-instance v1, Lorg/apache/commons/jexl2/internal/ListGetExecutor;

    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-direct {v1, p0, v0, v2}, Lorg/apache/commons/jexl2/internal/ListGetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Integer;)V

    .line 235
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v4, v1

    .line 236
    goto :goto_0

    .line 240
    :cond_3
    new-instance v1, Lorg/apache/commons/jexl2/internal/DuckGetExecutor;

    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-direct {v1, p0, v0, p2}, Lorg/apache/commons/jexl2/internal/DuckGetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 241
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v4, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected final getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "params"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/Introspector;->base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-result-object v0

    new-instance v1, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-direct {v1, p2, p3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method protected final getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    .locals 1
    .param p2, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/Introspector;->base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method public final getMethodExecutor(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 197
    new-instance v0, Lorg/apache/commons/jexl2/internal/MethodExecutor;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/commons/jexl2/internal/MethodExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    .local v0, "me":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .end local v0    # "me":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    :goto_0
    return-object v0

    .restart local v0    # "me":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSetExecutor(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "arg"    # Ljava/lang/Object;

    .prologue
    .line 255
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 256
    .local v0, "claz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/internal/Introspector;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 259
    .local v3, "property":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 260
    new-instance v1, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;

    invoke-direct {v1, p0, v0, v3, p3}, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 261
    .local v1, "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v4, v1

    .line 284
    :goto_0
    return-object v4

    .line 266
    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    :cond_0
    new-instance v1, Lorg/apache/commons/jexl2/internal/MapSetExecutor;

    invoke-direct {v1, p0, v0, p2, p3}, Lorg/apache/commons/jexl2/internal/MapSetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 267
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v1

    .line 268
    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/internal/Introspector;->toInteger(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    .line 273
    .local v2, "index":Ljava/lang/Integer;
    if-eqz v2, :cond_2

    .line 274
    new-instance v1, Lorg/apache/commons/jexl2/internal/ListSetExecutor;

    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    invoke-direct {v1, p0, v0, v2, p3}, Lorg/apache/commons/jexl2/internal/ListSetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Integer;Ljava/lang/Object;)V

    .line 275
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, v1

    .line 276
    goto :goto_0

    .line 280
    :cond_2
    new-instance v1, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;

    .end local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    invoke-direct {v1, p0, v0, v3, p3}, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;-><init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 281
    .restart local v1    # "executor":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v4, v1

    .line 282
    goto :goto_0

    .line 284
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected toInteger(Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 3
    .param p1, "arg"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 55
    if-nez p1, :cond_0

    .line 64
    .end local p1    # "arg":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 58
    .restart local p1    # "arg":Ljava/lang/Object;
    :cond_0
    instance-of v2, p1, Ljava/lang/Number;

    if-eqz v2, :cond_1

    .line 59
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "arg":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 62
    .restart local p1    # "arg":Ljava/lang/Object;
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "xnumber":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method protected toString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "arg"    # Ljava/lang/Object;

    .prologue
    .line 74
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
