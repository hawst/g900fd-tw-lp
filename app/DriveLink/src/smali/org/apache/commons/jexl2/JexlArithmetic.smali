.class public Lorg/apache/commons/jexl2/JexlArithmetic;
.super Ljava/lang/Object;
.source "JexlArithmetic.java"


# static fields
.field protected static final BIGD_DOUBLE_MAX_VALUE:Ljava/math/BigDecimal;

.field protected static final BIGD_DOUBLE_MIN_VALUE:Ljava/math/BigDecimal;

.field protected static final BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

.field protected static final BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;


# instance fields
.field private strict:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGD_DOUBLE_MAX_VALUE:Ljava/math/BigDecimal;

    .line 48
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGD_DOUBLE_MIN_VALUE:Ljava/math/BigDecimal;

    .line 50
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

    .line 52
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "lenient"    # Z

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    .line 62
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 276
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 277
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v6

    .line 309
    :goto_0
    return-object v6

    .line 282
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 283
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 284
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v3

    .line 285
    .local v3, "r":D
    new-instance v6, Ljava/lang/Double;

    add-double v7, v0, v3

    invoke-direct {v6, v7, v8}, Ljava/lang/Double;-><init>(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 307
    .end local v0    # "l":D
    .end local v3    # "r":D
    :catch_0
    move-exception v2

    .line 309
    .local v2, "nfe":Ljava/lang/NumberFormatException;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 289
    .end local v2    # "nfe":Ljava/lang/NumberFormatException;
    :cond_2
    :try_start_1
    instance-of v6, p1, Ljava/math/BigInteger;

    if-eqz v6, :cond_3

    instance-of v6, p2, Ljava/math/BigInteger;

    if-eqz v6, :cond_3

    .line 290
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 291
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v3

    .line 292
    .local v3, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    goto :goto_0

    .line 296
    .end local v0    # "l":Ljava/math/BigInteger;
    .end local v3    # "r":Ljava/math/BigInteger;
    :cond_3
    instance-of v6, p1, Ljava/math/BigDecimal;

    if-nez v6, :cond_4

    instance-of v6, p2, Ljava/math/BigDecimal;

    if-eqz v6, :cond_5

    .line 297
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 298
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 299
    .local v3, "r":Ljava/math/BigDecimal;
    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v6

    goto :goto_0

    .line 303
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v3    # "r":Ljava/math/BigDecimal;
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 304
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v3

    .line 305
    .local v3, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 306
    .local v5, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    goto :goto_0
.end method

.method protected controlNullNullOperands()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 92
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "jexl.null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected controlNullOperand()V
    .locals 2

    .prologue
    .line 103
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "jexl.null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    return-void
.end method

.method public divide(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 321
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 322
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v0

    .line 354
    :goto_0
    return-object v0

    .line 326
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 327
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v1

    .line 328
    .local v1, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v3

    .line 329
    .local v3, "r":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v3, v6

    if-nez v6, :cond_2

    .line 330
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string/jumbo v7, "/"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 332
    :cond_2
    new-instance v0, Ljava/lang/Double;

    div-double v6, v1, v3

    invoke-direct {v0, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 336
    .end local v1    # "l":D
    .end local v3    # "r":D
    :cond_3
    instance-of v6, p1, Ljava/math/BigInteger;

    if-eqz v6, :cond_4

    instance-of v6, p2, Ljava/math/BigInteger;

    if-eqz v6, :cond_4

    .line 337
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v1

    .line 338
    .local v1, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v3

    .line 339
    .local v3, "r":Ljava/math/BigInteger;
    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0

    .line 343
    .end local v1    # "l":Ljava/math/BigInteger;
    .end local v3    # "r":Ljava/math/BigInteger;
    :cond_4
    instance-of v6, p1, Ljava/math/BigDecimal;

    if-nez v6, :cond_5

    instance-of v6, p2, Ljava/math/BigDecimal;

    if-eqz v6, :cond_6

    .line 344
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 345
    .local v1, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 346
    .local v3, "r":Ljava/math/BigDecimal;
    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 347
    .local v0, "d":Ljava/math/BigDecimal;
    goto :goto_0

    .line 351
    .end local v0    # "d":Ljava/math/BigDecimal;
    .end local v1    # "l":Ljava/math/BigDecimal;
    .end local v3    # "r":Ljava/math/BigDecimal;
    :cond_6
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v1

    .line 352
    .local v1, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v3

    .line 353
    .local v3, "r":Ljava/math/BigInteger;
    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 354
    .local v5, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 511
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 536
    :cond_0
    :goto_0
    return v0

    .line 516
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v0, v1

    .line 520
    goto :goto_0

    .line 521
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 522
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 523
    :cond_4
    instance-of v2, p1, Ljava/math/BigDecimal;

    if-nez v2, :cond_5

    instance-of v2, p2, Ljava/math/BigDecimal;

    if-eqz v2, :cond_6

    .line 524
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 525
    :cond_6
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointType(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 526
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 527
    :cond_7
    instance-of v2, p1, Ljava/lang/Number;

    if-nez v2, :cond_8

    instance-of v2, p2, Ljava/lang/Number;

    if-nez v2, :cond_8

    instance-of v2, p1, Ljava/lang/Character;

    if-nez v2, :cond_8

    instance-of v2, p2, Ljava/lang/Character;

    if-eqz v2, :cond_9

    .line 529
    :cond_8
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v2

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 530
    :cond_9
    instance-of v2, p1, Ljava/lang/Boolean;

    if-nez v2, :cond_a

    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    .line 531
    :cond_a
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 532
    :cond_b
    instance-of v0, p1, Ljava/lang/String;

    if-nez v0, :cond_c

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 533
    :cond_c
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 536
    :cond_d
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public greaterThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 589
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 592
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public greaterThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 614
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->greaterThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isFloatingPoint(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 143
    instance-of v0, p1, Ljava/lang/Float;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isFloatingPointNumber(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 126
    instance-of v3, p1, Ljava/lang/Float;

    if-nez v3, :cond_0

    instance-of v3, p1, Ljava/lang/Double;

    if-eqz v3, :cond_2

    :cond_0
    move v1, v2

    .line 133
    :cond_1
    :goto_0
    return v1

    .line 129
    :cond_2
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_1

    move-object v0, p1

    .line 130
    check-cast v0, Ljava/lang/String;

    .line 131
    .local v0, "string":Ljava/lang/String;
    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ne v3, v4, :cond_3

    const/16 v3, 0x65

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ne v3, v4, :cond_3

    const/16 v3, 0x45

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v4, :cond_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method protected isFloatingPointType(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 115
    instance-of v0, p1, Ljava/lang/Float;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Double;

    if-nez v0, :cond_0

    instance-of v0, p2, Ljava/lang/Float;

    if-nez v0, :cond_0

    instance-of v0, p2, Ljava/lang/Double;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLenient()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isNumberable(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 153
    instance-of v0, p1, Ljava/lang/Integer;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Long;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Byte;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Short;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 18
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 548
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 549
    :cond_0
    const/4 v15, 0x0

    .line 573
    :goto_0
    return v15

    .line 550
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPoint(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPoint(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 551
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v4

    .line 552
    .local v4, "leftDouble":D
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v10

    .line 553
    .local v10, "rightDouble":D
    cmpg-double v15, v4, v10

    if-gez v15, :cond_3

    const/4 v15, 0x1

    goto :goto_0

    :cond_3
    const/4 v15, 0x0

    goto :goto_0

    .line 554
    .end local v4    # "leftDouble":D
    .end local v10    # "rightDouble":D
    :cond_4
    move-object/from16 v0, p1

    instance-of v15, v0, Ljava/math/BigDecimal;

    if-nez v15, :cond_5

    move-object/from16 v0, p2

    instance-of v15, v0, Ljava/math/BigDecimal;

    if-eqz v15, :cond_7

    .line 555
    :cond_5
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 556
    .local v3, "l":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v9

    .line 557
    .local v9, "r":Ljava/math/BigDecimal;
    invoke-virtual {v3, v9}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v15

    if-gez v15, :cond_6

    const/4 v15, 0x1

    goto :goto_0

    :cond_6
    const/4 v15, 0x0

    goto :goto_0

    .line 558
    .end local v3    # "l":Ljava/math/BigDecimal;
    .end local v9    # "r":Ljava/math/BigDecimal;
    :cond_7
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isNumberable(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_8

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isNumberable(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 559
    :cond_8
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v6

    .line 560
    .local v6, "leftLong":J
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v12

    .line 561
    .local v12, "rightLong":J
    cmp-long v15, v6, v12

    if-gez v15, :cond_9

    const/4 v15, 0x1

    goto :goto_0

    :cond_9
    const/4 v15, 0x0

    goto :goto_0

    .line 562
    .end local v6    # "leftLong":J
    .end local v12    # "rightLong":J
    :cond_a
    move-object/from16 v0, p1

    instance-of v15, v0, Ljava/lang/String;

    if-nez v15, :cond_b

    move-object/from16 v0, p2

    instance-of v15, v0, Ljava/lang/String;

    if-eqz v15, :cond_d

    .line 563
    :cond_b
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 564
    .local v8, "leftString":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    .line 565
    .local v14, "rightString":Ljava/lang/String;
    invoke-virtual {v8, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-gez v15, :cond_c

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 566
    .end local v8    # "leftString":Ljava/lang/String;
    .end local v14    # "rightString":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p1

    instance-of v15, v0, Ljava/lang/Comparable;

    if-eqz v15, :cond_f

    move-object/from16 v2, p1

    .line 568
    check-cast v2, Ljava/lang/Comparable;

    .line 569
    .local v2, "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v15

    if-gez v15, :cond_e

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_e
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 570
    .end local v2    # "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    :cond_f
    move-object/from16 v0, p2

    instance-of v15, v0, Ljava/lang/Comparable;

    if-eqz v15, :cond_11

    move-object/from16 v2, p2

    .line 572
    check-cast v2, Ljava/lang/Comparable;

    .line 573
    .restart local v2    # "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v15

    if-lez v15, :cond_10

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_10
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 576
    .end local v2    # "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    :cond_11
    new-instance v15, Ljava/lang/IllegalArgumentException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Invalid comparison : comparing cardinality for left: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, " and right: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15
.end method

.method public lessThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 603
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public matches(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 487
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 489
    const/4 v1, 0x1

    .line 499
    .end local p2    # "right":Ljava/lang/Object;
    :goto_0
    return v1

    .line 491
    .restart local p2    # "right":Ljava/lang/Object;
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 493
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 495
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 496
    .local v0, "arg":Ljava/lang/String;
    instance-of v1, p2, Ljava/util/regex/Pattern;

    if-eqz v1, :cond_3

    .line 497
    check-cast p2, Ljava/util/regex/Pattern;

    .end local p2    # "right":Ljava/lang/Object;
    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0

    .line 499
    .restart local p2    # "right":Ljava/lang/Object;
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public mod(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 365
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 366
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v4

    .line 398
    :goto_0
    return-object v4

    .line 370
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 371
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 372
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 373
    .local v2, "r":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v2, v6

    if-nez v6, :cond_2

    .line 374
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string/jumbo v7, "%"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 376
    :cond_2
    new-instance v4, Ljava/lang/Double;

    rem-double v6, v0, v2

    invoke-direct {v4, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 380
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_3
    instance-of v6, p1, Ljava/math/BigInteger;

    if-eqz v6, :cond_4

    instance-of v6, p2, Ljava/math/BigInteger;

    if-eqz v6, :cond_4

    .line 381
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 382
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 383
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    goto :goto_0

    .line 387
    .end local v0    # "l":Ljava/math/BigInteger;
    .end local v2    # "r":Ljava/math/BigInteger;
    :cond_4
    instance-of v6, p1, Ljava/math/BigDecimal;

    if-nez v6, :cond_5

    instance-of v6, p2, Ljava/math/BigDecimal;

    if-eqz v6, :cond_6

    .line 388
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 389
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 390
    .local v2, "r":Ljava/math/BigDecimal;
    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->remainder(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 391
    .local v4, "remainder":Ljava/math/BigDecimal;
    goto :goto_0

    .line 395
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    .end local v4    # "remainder":Ljava/math/BigDecimal;
    :cond_6
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 396
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 397
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 398
    .local v5, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v4

    goto :goto_0
.end method

.method public multiply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 408
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 409
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v5

    .line 437
    :goto_0
    return-object v5

    .line 413
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 414
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 415
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 416
    .local v2, "r":D
    new-instance v5, Ljava/lang/Double;

    mul-double v6, v0, v2

    invoke-direct {v5, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 420
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_2
    instance-of v5, p1, Ljava/math/BigInteger;

    if-eqz v5, :cond_3

    instance-of v5, p2, Ljava/math/BigInteger;

    if-eqz v5, :cond_3

    .line 421
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 422
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 423
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    goto :goto_0

    .line 427
    .end local v0    # "l":Ljava/math/BigInteger;
    .end local v2    # "r":Ljava/math/BigInteger;
    :cond_3
    instance-of v5, p1, Ljava/math/BigDecimal;

    if-nez v5, :cond_4

    instance-of v5, p2, Ljava/math/BigDecimal;

    if-eqz v5, :cond_5

    .line 428
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 429
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 430
    .local v2, "r":Ljava/math/BigDecimal;
    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    goto :goto_0

    .line 434
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 435
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 436
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 437
    .local v4, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0
.end method

.method public narrow(Ljava/lang/Number;)Ljava/lang/Number;
    .locals 7
    .param p1, "original"    # Ljava/lang/Number;

    .prologue
    .line 813
    if-nez p1, :cond_1

    .line 850
    .end local p1    # "original":Ljava/lang/Number;
    :cond_0
    :goto_0
    return-object p1

    .line 816
    .restart local p1    # "original":Ljava/lang/Number;
    :cond_1
    move-object v2, p1

    .line 817
    .local v2, "result":Ljava/lang/Number;
    instance-of v5, p1, Ljava/math/BigDecimal;

    if-eqz v5, :cond_2

    move-object v0, p1

    .line 818
    check-cast v0, Ljava/math/BigDecimal;

    .line 820
    .local v0, "bigd":Ljava/math/BigDecimal;
    sget-object v5, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGD_DOUBLE_MAX_VALUE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v5

    if-gtz v5, :cond_0

    .line 824
    .end local v0    # "bigd":Ljava/math/BigDecimal;
    :cond_2
    instance-of v5, p1, Ljava/lang/Double;

    if-nez v5, :cond_3

    instance-of v5, p1, Ljava/lang/Float;

    if-nez v5, :cond_3

    instance-of v5, p1, Ljava/math/BigDecimal;

    if-eqz v5, :cond_5

    .line 825
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v3

    .line 826
    .local v3, "value":D
    const-wide v5, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpg-double v5, v3, v5

    if-gtz v5, :cond_4

    const-wide/high16 v5, 0x36a0000000000000L    # 1.401298464324817E-45

    cmpl-double v5, v3, v5

    if-ltz v5, :cond_4

    .line 827
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .end local v3    # "value":D
    :cond_4
    :goto_1
    move-object p1, v2

    .line 850
    goto :goto_0

    .line 831
    :cond_5
    instance-of v5, p1, Ljava/math/BigInteger;

    if-eqz v5, :cond_6

    move-object v1, p1

    .line 832
    check-cast v1, Ljava/math/BigInteger;

    .line 834
    .local v1, "bigi":Ljava/math/BigInteger;
    sget-object v5, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

    invoke-virtual {v1, v5}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v5

    if-gtz v5, :cond_0

    sget-object v5, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;

    invoke-virtual {v1, v5}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v5

    if-ltz v5, :cond_0

    .line 839
    .end local v1    # "bigi":Ljava/math/BigInteger;
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    .line 840
    .local v3, "value":J
    const-wide/16 v5, 0x7f

    cmp-long v5, v3, v5

    if-gtz v5, :cond_7

    const-wide/16 v5, -0x80

    cmp-long v5, v3, v5

    if-ltz v5, :cond_7

    .line 842
    long-to-int v5, v3

    int-to-byte v5, v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    goto :goto_1

    .line 843
    :cond_7
    const-wide/16 v5, 0x7fff

    cmp-long v5, v3, v5

    if-gtz v5, :cond_8

    const-wide/16 v5, -0x8000

    cmp-long v5, v3, v5

    if-ltz v5, :cond_8

    .line 844
    long-to-int v5, v3

    int-to-short v5, v5

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    goto :goto_1

    .line 845
    :cond_8
    const-wide/32 v5, 0x7fffffff

    cmp-long v5, v3, v5

    if-gtz v5, :cond_4

    const-wide/32 v5, -0x80000000

    cmp-long v5, v3, v5

    if-ltz v5, :cond_4

    .line 846
    long-to-int v5, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1
.end method

.method protected narrowArguments([Ljava/lang/Object;)Z
    .locals 5
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 251
    const/4 v3, 0x0

    .line 252
    .local v3, "narrowed":Z
    const/4 v0, 0x0

    .local v0, "a":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_2

    .line 253
    aget-object v1, p1, v0

    .line 254
    .local v1, "arg":Ljava/lang/Object;
    instance-of v4, v1, Ljava/lang/Number;

    if-eqz v4, :cond_1

    move-object v4, v1

    .line 255
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {p0, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrow(Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    .line 256
    .local v2, "narg":Ljava/lang/Number;
    if-eq v2, v1, :cond_0

    .line 257
    const/4 v3, 0x1

    .line 259
    :cond_0
    aput-object v2, p1, v0

    .line 252
    .end local v2    # "narg":Ljava/lang/Number;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    .end local v1    # "arg":Ljava/lang/Object;
    :cond_2
    return v3
.end method

.method protected narrowArrayType([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1, "untyped"    # [Ljava/lang/Object;

    .prologue
    .line 203
    array-length v6, p1

    .line 204
    .local v6, "size":I
    const/4 v2, 0x0

    .line 205
    .local v2, "commonClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-lez v6, :cond_4

    .line 207
    const/4 v8, 0x0

    aget-object v8, p1, v8

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 208
    const-class v8, Ljava/lang/Number;

    invoke-virtual {v8, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    .line 210
    .local v5, "isNumber":Z
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_2

    .line 211
    aget-object v8, p1, v4

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 213
    .local v3, "eclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v8, Ljava/lang/Object;

    invoke-virtual {v8, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 215
    if-eqz v5, :cond_1

    const-class v8, Ljava/lang/Number;

    invoke-virtual {v8, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 216
    const-class v2, Ljava/lang/Number;

    .line 210
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 218
    :cond_1
    const-class v2, Ljava/lang/Object;

    goto :goto_1

    .line 223
    .end local v3    # "eclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    const-class v8, Ljava/lang/Object;

    invoke-virtual {v8, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 225
    if-eqz v5, :cond_3

    .line 227
    :try_start_0
    const-string/jumbo v8, "TYPE"

    invoke-virtual {v2, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 228
    .local v1, "TYPE":Ljava/lang/reflect/Field;
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/Class;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    .end local v1    # "TYPE":Ljava/lang/reflect/Field;
    :cond_3
    :goto_2
    invoke-static {v2, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7

    .line 235
    .local v7, "typed":Ljava/lang/Object;
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v6, :cond_5

    .line 236
    aget-object v8, p1, v4

    invoke-static {v7, v4, v8}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 235
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .end local v4    # "i":I
    .end local v5    # "isNumber":Z
    .end local v7    # "typed":Ljava/lang/Object;
    :cond_4
    move-object v7, p1

    .line 241
    :cond_5
    return-object v7

    .line 229
    .restart local v4    # "i":I
    .restart local v5    # "isNumber":Z
    :catch_0
    move-exception v8

    goto :goto_2
.end method

.method protected narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;
    .locals 4
    .param p1, "lhs"    # Ljava/lang/Object;
    .param p2, "rhs"    # Ljava/lang/Object;
    .param p3, "bigi"    # Ljava/math/BigInteger;

    .prologue
    .line 175
    instance-of v2, p1, Ljava/math/BigInteger;

    if-nez v2, :cond_0

    instance-of v2, p2, Ljava/math/BigInteger;

    if-nez v2, :cond_0

    sget-object v2, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

    invoke-virtual {p3, v2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-gtz v2, :cond_0

    sget-object v2, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;

    invoke-virtual {p3, v2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-ltz v2, :cond_0

    .line 179
    invoke-virtual {p3}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    .line 181
    .local v0, "l":J
    instance-of v2, p1, Ljava/lang/Long;

    if-nez v2, :cond_1

    instance-of v2, p2, Ljava/lang/Long;

    if-nez v2, :cond_1

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 184
    long-to-int v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 188
    .end local v0    # "l":J
    .end local p3    # "bigi":Ljava/math/BigInteger;
    :cond_0
    :goto_0
    return-object p3

    .line 186
    .restart local v0    # "l":J
    .restart local p3    # "bigi":Ljava/math/BigInteger;
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    goto :goto_0
.end method

.method setLenient(Z)V
    .locals 1
    .param p1, "lenient"    # Z

    .prologue
    .line 74
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    .line 75
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 447
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 448
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v5

    .line 476
    :goto_0
    return-object v5

    .line 452
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 453
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 454
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 455
    .local v2, "r":D
    new-instance v5, Ljava/lang/Double;

    sub-double v6, v0, v2

    invoke-direct {v5, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 459
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_2
    instance-of v5, p1, Ljava/math/BigInteger;

    if-eqz v5, :cond_3

    instance-of v5, p2, Ljava/math/BigInteger;

    if-eqz v5, :cond_3

    .line 460
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 461
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 462
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    goto :goto_0

    .line 466
    .end local v0    # "l":Ljava/math/BigInteger;
    .end local v2    # "r":Ljava/math/BigInteger;
    :cond_3
    instance-of v5, p1, Ljava/math/BigDecimal;

    if-nez v5, :cond_4

    instance-of v5, p2, Ljava/math/BigDecimal;

    if-eqz v5, :cond_5

    .line 467
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 468
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 469
    .local v2, "r":Ljava/math/BigDecimal;
    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    goto :goto_0

    .line 473
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 474
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 475
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 476
    .local v4, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0
.end method

.method public toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 728
    instance-of v2, p1, Ljava/math/BigDecimal;

    if-eqz v2, :cond_0

    .line 729
    check-cast p1, Ljava/math/BigDecimal;

    .line 743
    .end local p1    # "val":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 730
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_0
    if-nez p1, :cond_1

    .line 731
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 732
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    goto :goto_0

    .line 733
    :cond_1
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object v1, p1

    .line 734
    check-cast v1, Ljava/lang/String;

    .line 735
    .local v1, "string":Ljava/lang/String;
    const-string/jumbo v2, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 736
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    goto :goto_0

    .line 738
    :cond_2
    new-instance p1, Ljava/math/BigDecimal;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p1, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 739
    .end local v1    # "string":Ljava/lang/String;
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v2, p1, Ljava/lang/Number;

    if-eqz v2, :cond_4

    .line 740
    new-instance v2, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object p1, v2

    goto :goto_0

    .line 741
    :cond_4
    instance-of v2, p1, Ljava/lang/Character;

    if-eqz v2, :cond_5

    .line 742
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 743
    .local v0, "i":I
    new-instance p1, Ljava/math/BigDecimal;

    invoke-direct {p1, v0}, Ljava/math/BigDecimal;-><init>(I)V

    goto :goto_0

    .line 746
    .end local v0    # "i":I
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "BigDecimal coercion exception. Can\'t coerce type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 698
    instance-of v2, p1, Ljava/math/BigInteger;

    if-eqz v2, :cond_0

    .line 699
    check-cast p1, Ljava/math/BigInteger;

    .line 713
    .end local p1    # "val":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 700
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_0
    if-nez p1, :cond_1

    .line 701
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 702
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object p1

    goto :goto_0

    .line 703
    :cond_1
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object v1, p1

    .line 704
    check-cast v1, Ljava/lang/String;

    .line 705
    .local v1, "string":Ljava/lang/String;
    const-string/jumbo v2, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 706
    sget-object p1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    goto :goto_0

    .line 708
    :cond_2
    new-instance p1, Ljava/math/BigInteger;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p1, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 709
    .end local v1    # "string":Ljava/lang/String;
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v2, p1, Ljava/lang/Number;

    if-eqz v2, :cond_4

    .line 710
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    move-object p1, v2

    goto :goto_0

    .line 711
    :cond_4
    instance-of v2, p1, Ljava/lang/Character;

    if-eqz v2, :cond_5

    .line 712
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 713
    .local v0, "i":I
    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object p1

    goto :goto_0

    .line 716
    .end local v0    # "i":I
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "BigInteger coercion exception. Can\'t coerce type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toBoolean(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 624
    if-nez p1, :cond_1

    .line 625
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 633
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 627
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 628
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 629
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 630
    check-cast p1, Ljava/lang/String;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public toDouble(Ljava/lang/Object;)D
    .locals 6
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v2, 0x0

    .line 758
    if-nez p1, :cond_1

    .line 759
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 777
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-wide v2

    .line 761
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v4, p1, Ljava/lang/String;

    if-eqz v4, :cond_2

    move-object v1, p1

    .line 762
    check-cast v1, Ljava/lang/String;

    .line 763
    .local v1, "string":Ljava/lang/String;
    const-string/jumbo v4, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 767
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    goto :goto_0

    .line 768
    .end local v1    # "string":Ljava/lang/String;
    :cond_2
    instance-of v2, p1, Ljava/lang/Character;

    if-eqz v2, :cond_3

    .line 769
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 771
    .local v0, "i":I
    int-to-double v2, v0

    goto :goto_0

    .line 772
    .end local v0    # "i":I
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v2, p1, Ljava/lang/Double;

    if-eqz v2, :cond_4

    .line 773
    check-cast p1, Ljava/lang/Double;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto :goto_0

    .line 774
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_4
    instance-of v2, p1, Ljava/lang/Number;

    if-eqz v2, :cond_5

    .line 777
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    goto :goto_0

    .line 778
    :cond_5
    instance-of v2, p1, Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 779
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Boolean->Double coercion exception"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 782
    :cond_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Double coercion exception. Can\'t coerce type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toLong(Ljava/lang/Object;)J
    .locals 3
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v0, 0x0

    .line 671
    if-nez p1, :cond_1

    .line 672
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 684
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-wide v0

    .line 674
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 675
    const-string/jumbo v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 678
    check-cast p1, Ljava/lang/String;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 679
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_3

    .line 680
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 681
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 682
    new-instance v0, Ljava/lang/NumberFormatException;

    const-string/jumbo v1, "Boolean->Long coercion exception"

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 683
    :cond_4
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_5

    .line 684
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 687
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_5
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Long coercion exception. Can\'t coerce type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 795
    if-nez p1, :cond_0

    .line 796
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 797
    const-string/jumbo p1, ""

    .line 799
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
