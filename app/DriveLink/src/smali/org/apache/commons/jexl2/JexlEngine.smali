.class public Lorg/apache/commons/jexl2/JexlEngine;
.super Ljava/lang/Object;
.source "JexlEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/JexlEngine$SoftCache;,
        Lorg/apache/commons/jexl2/JexlEngine$UberspectHolder;
    }
.end annotation


# static fields
.field public static final EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;


# instance fields
.field protected final arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

.field protected cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/jexl2/JexlEngine$SoftCache",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/commons/jexl2/parser/ASTJexlScript;",
            ">;"
        }
    .end annotation
.end field

.field protected debug:Z

.field protected functions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected final logger:Lorg/apache/commons/logging/Log;

.field protected final parser:Lorg/apache/commons/jexl2/parser/Parser;

.field protected silent:Z

.field protected final uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$1;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/JexlEngine$1;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 170
    invoke-direct {p0, v0, v0, v0, v0}, Lorg/apache/commons/jexl2/JexlEngine;-><init>(Lorg/apache/commons/jexl2/introspection/Uberspect;Lorg/apache/commons/jexl2/JexlArithmetic;Ljava/util/Map;Lorg/apache/commons/logging/Log;)V

    .line 171
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/introspection/Uberspect;Lorg/apache/commons/jexl2/JexlArithmetic;Ljava/util/Map;Lorg/apache/commons/logging/Log;)V
    .locals 4
    .param p1, "anUberspect"    # Lorg/apache/commons/jexl2/introspection/Uberspect;
    .param p2, "anArithmetic"    # Lorg/apache/commons/jexl2/JexlArithmetic;
    .param p4, "log"    # Lorg/apache/commons/logging/Log;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/introspection/Uberspect;",
            "Lorg/apache/commons/jexl2/JexlArithmetic;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/apache/commons/logging/Log;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "theFunctions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v3, 0x1

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    new-instance v0, Lorg/apache/commons/jexl2/parser/Parser;

    new-instance v1, Ljava/io/StringReader;

    const-string/jumbo v2, ";"

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/commons/jexl2/parser/Parser;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    .line 152
    iput-boolean v3, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    .line 156
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    .line 182
    if-nez p1, :cond_0

    invoke-static {p4}, Lorg/apache/commons/jexl2/JexlEngine;->getUberspect(Lorg/apache/commons/logging/Log;)Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object p1

    .end local p1    # "anUberspect":Lorg/apache/commons/jexl2/introspection/Uberspect;
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    .line 183
    if-nez p4, :cond_1

    .line 184
    const-class v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object p4

    .line 186
    :cond_1
    iput-object p4, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    .line 187
    if-nez p2, :cond_2

    new-instance p2, Lorg/apache/commons/jexl2/JexlArithmetic;

    .end local p2    # "anArithmetic":Lorg/apache/commons/jexl2/JexlArithmetic;
    invoke-direct {p2, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;-><init>(Z)V

    :cond_2
    iput-object p2, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    .line 188
    if-eqz p3, :cond_3

    .line 189
    iput-object p3, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    .line 191
    :cond_3
    return-void
.end method

.method public static final cleanExpression(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v3, 0x20

    .line 872
    if-eqz p0, :cond_3

    .line 873
    const/4 v1, 0x0

    .line 874
    .local v1, "start":I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 875
    .local v0, "end":I
    if-lez v0, :cond_2

    .line 877
    :goto_0
    if-ge v1, v0, :cond_0

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    .line 878
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 881
    :cond_0
    :goto_1
    if-lez v0, :cond_1

    add-int/lit8 v2, v0, -0x1

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_1

    .line 882
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 884
    :cond_1
    invoke-interface {p0, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 888
    .end local v0    # "end":I
    .end local v1    # "start":I
    :goto_2
    return-object v2

    .line 886
    .restart local v0    # "end":I
    .restart local v1    # "start":I
    :cond_2
    const-string/jumbo v2, ""

    goto :goto_2

    .line 888
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static getUberspect(Lorg/apache/commons/logging/Log;)Lorg/apache/commons/jexl2/introspection/Uberspect;
    .locals 1
    .param p0, "logger"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 204
    if-eqz p0, :cond_0

    const-class v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    :cond_0
    # getter for: Lorg/apache/commons/jexl2/JexlEngine$UberspectHolder;->UBERSPECT:Lorg/apache/commons/jexl2/introspection/Uberspect;
    invoke-static {}, Lorg/apache/commons/jexl2/JexlEngine$UberspectHolder;->access$000()Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object v0

    .line 207
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;

    invoke-direct {v0, p0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;-><init>(Lorg/apache/commons/logging/Log;)V

    goto :goto_0
.end method


# virtual methods
.method protected createCache(I)Ljava/util/Map;
    .locals 6
    .param p1, "cacheSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 773
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$2;

    const/high16 v3, 0x3f400000    # 0.75f

    const/4 v4, 0x1

    move-object v1, p0

    move v2, p1

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/jexl2/JexlEngine$2;-><init>(Lorg/apache/commons/jexl2/JexlEngine;IFZI)V

    return-object v0
.end method

.method public createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;
    .locals 1
    .param p1, "expression"    # Ljava/lang/String;

    .prologue
    .line 368
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v0

    return-object v0
.end method

.method public createExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/Expression;
    .locals 4
    .param p1, "expression"    # Ljava/lang/String;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 384
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v0

    .line 385
    .local v0, "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 386
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "The JEXL Expression created will be a reference to the first expression from the supplied script: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\" "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 389
    :cond_0
    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v1

    return-object v1
.end method

.method protected createExpression(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;
    .locals 1
    .param p1, "tree"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 354
    new-instance v0, Lorg/apache/commons/jexl2/ExpressionImpl;

    invoke-direct {v0, p0, p2, p1}, Lorg/apache/commons/jexl2/ExpressionImpl;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Ljava/lang/String;Lorg/apache/commons/jexl2/parser/ASTJexlScript;)V

    return-object v0
.end method

.method protected createInfo(Ljava/lang/String;II)Lorg/apache/commons/jexl2/JexlInfo;
    .locals 1
    .param p1, "fn"    # Ljava/lang/String;
    .param p2, "l"    # I
    .param p3, "c"    # I

    .prologue
    .line 828
    new-instance v0, Lorg/apache/commons/jexl2/DebugInfo;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/commons/jexl2/DebugInfo;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method protected createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 692
    if-nez p1, :cond_0

    .line 693
    sget-object p1, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    .line 695
    :cond_0
    new-instance v0, Lorg/apache/commons/jexl2/Interpreter;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/jexl2/Interpreter;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;)V

    return-object v0
.end method

.method protected debugInfo()Lorg/apache/commons/jexl2/JexlInfo;
    .locals 10

    .prologue
    .line 838
    const/4 v2, 0x0

    .line 839
    .local v2, "info":Lorg/apache/commons/jexl2/JexlInfo;
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    if-eqz v7, :cond_3

    .line 840
    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    .line 841
    .local v6, "xinfo":Ljava/lang/Throwable;
    invoke-virtual {v6}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    .line 842
    invoke-virtual {v6}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    .line 843
    .local v5, "stack":[Ljava/lang/StackTraceElement;
    const/4 v4, 0x0

    .line 844
    .local v4, "se":Ljava/lang/StackTraceElement;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 845
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v3, 0x1

    .local v3, "s":I
    :goto_0
    array-length v7, v5

    if-ge v3, v7, :cond_2

    .line 846
    aget-object v4, v5, v3

    .line 847
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 848
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 850
    const-class v7, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 851
    const-class v1, Lorg/apache/commons/jexl2/JexlEngine;

    .line 845
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    const/4 v4, 0x0

    goto :goto_0

    .line 852
    :cond_1
    const-class v7, Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 853
    const-class v1, Lorg/apache/commons/jexl2/UnifiedJEXL;

    goto :goto_1

    .line 859
    .end local v0    # "className":Ljava/lang/String;
    :cond_2
    if-eqz v4, :cond_3

    .line 860
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {p0, v7, v8, v9}, Lorg/apache/commons/jexl2/JexlEngine;->createInfo(Ljava/lang/String;II)Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v2

    .line 863
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "s":I
    .end local v4    # "se":Ljava/lang/StackTraceElement;
    .end local v5    # "stack":[Ljava/lang/StackTraceElement;
    .end local v6    # "xinfo":Ljava/lang/Throwable;
    :cond_3
    return-object v2
.end method

.method protected parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .locals 10
    .param p1, "expression"    # Ljava/lang/CharSequence;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 792
    invoke-static {p1}, Lorg/apache/commons/jexl2/JexlEngine;->cleanExpression(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 793
    .local v1, "expr":Ljava/lang/String;
    const/4 v3, 0x0

    .line 794
    .local v3, "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    iget-object v8, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    monitor-enter v8

    .line 795
    :try_start_0
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v7, :cond_0

    .line 796
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v7, v1}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-object v3, v0

    .line 797
    if-eqz v3, :cond_0

    .line 798
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v3

    .line 817
    .end local v3    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .local v4, "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :goto_0
    return-object v4

    .line 802
    .end local v4    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .restart local v3    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 804
    .local v2, "reader":Ljava/io/Reader;
    if-nez p2, :cond_1

    .line 805
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->debugInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object p2

    .line 807
    :cond_1
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    invoke-virtual {v7, v2, p2}, Lorg/apache/commons/jexl2/parser/Parser;->parse(Ljava/io/Reader;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v3

    .line 808
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v7, :cond_2

    .line 809
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v7, v1, v3}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/apache/commons/jexl2/parser/TokenMgrError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/commons/jexl2/parser/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 816
    :cond_2
    :try_start_2
    monitor-exit v8

    move-object v4, v3

    .line 817
    .end local v3    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .restart local v4    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    goto :goto_0

    .line 811
    .end local v2    # "reader":Ljava/io/Reader;
    .end local v4    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .restart local v3    # "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :catch_0
    move-exception v6

    .line 812
    .local v6, "xtme":Lorg/apache/commons/jexl2/parser/TokenMgrError;
    new-instance v7, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v9, "tokenization failed"

    invoke-direct {v7, p2, v9, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 816
    .end local v6    # "xtme":Lorg/apache/commons/jexl2/parser/TokenMgrError;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v7

    .line 813
    :catch_1
    move-exception v5

    .line 814
    .local v5, "xparse":Lorg/apache/commons/jexl2/parser/ParseException;
    :try_start_3
    new-instance v7, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v9, "parsing failed"

    invoke-direct {v7, p2, v9, v5}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public setCache(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 298
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    monitor-enter v1

    .line 299
    if-gtz p1, :cond_1

    .line 300
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    .line 304
    :cond_0
    :goto_0
    monitor-exit v1

    .line 305
    return-void

    .line 301
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->size()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 302
    :cond_2
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;-><init>(Lorg/apache/commons/jexl2/JexlEngine;I)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    goto :goto_0

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setFunctions(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "funcs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .end local p1    # "funcs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    .line 335
    return-void

    .line 334
    .restart local p1    # "funcs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    goto :goto_0
.end method

.method public setLenient(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 268
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->setLenient(Z)V

    .line 269
    return-void
.end method

.method public setSilent(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 247
    iput-boolean p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    .line 248
    return-void
.end method
