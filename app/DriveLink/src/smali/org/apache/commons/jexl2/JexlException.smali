.class public Lorg/apache/commons/jexl2/JexlException;
.super Ljava/lang/RuntimeException;
.source "JexlException.java"


# instance fields
.field protected final info:Lorg/apache/commons/jexl2/JexlInfo;

.field protected final mark:Lorg/apache/commons/jexl2/parser/JexlNode;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "dbg"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 76
    invoke-direct {p0, p2, p3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 78
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 42
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->getInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 44
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 53
    invoke-direct {p0, p2, p3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 55
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->getInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 56
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 114
    new-instance v1, Lorg/apache/commons/jexl2/Debugger;

    invoke-direct {v1}, Lorg/apache/commons/jexl2/Debugger;-><init>()V

    .line 115
    .local v1, "dbg":Lorg/apache/commons/jexl2/Debugger;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .local v2, "msg":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    if-eqz v3, :cond_0

    .line 117
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    invoke-interface {v3}, Lorg/apache/commons/jexl2/JexlInfo;->debugString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    invoke-virtual {v1, v3}, Lorg/apache/commons/jexl2/Debugger;->debug(Lorg/apache/commons/jexl2/parser/JexlNode;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 120
    const-string/jumbo v3, "!["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->start()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->end()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 124
    const-string/jumbo v3, "]: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->data()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_1
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    invoke-super {p0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 131
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_2

    const-string/jumbo v3, "jexl.null"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-ne v3, v4, :cond_2

    .line 132
    const-string/jumbo v3, " caused by null operand"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
