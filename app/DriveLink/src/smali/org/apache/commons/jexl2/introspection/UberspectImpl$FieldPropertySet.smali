.class public final Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;
.super Ljava/lang/Object;
.source "UberspectImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/JexlPropertySet;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FieldPropertySet"
.end annotation


# instance fields
.field private final field:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;)V
    .locals 0
    .param p1, "theField"    # Ljava/lang/reflect/Field;

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;->field:Ljava/lang/reflect/Field;

    .line 201
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "arg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 208
    return-object p2
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x1

    return v0
.end method

.method public tryFailed(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "rval"    # Ljava/lang/Object;

    .prologue
    .line 232
    sget-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tryInvoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 215
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_0

    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1, p3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    .end local p3    # "value":Ljava/lang/Object;
    :goto_0
    return-object p3

    .line 221
    .restart local p3    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 222
    .local v0, "xill":Ljava/lang/IllegalAccessException;
    sget-object p3, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 225
    .end local v0    # "xill":Ljava/lang/IllegalAccessException;
    :cond_1
    sget-object p3, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
