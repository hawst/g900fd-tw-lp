.class public abstract Lorg/apache/commons/jexl2/internal/AbstractExecutor;
.super Ljava/lang/Object;
.source "AbstractExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;,
        Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;,
        Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
    }
.end annotation


# static fields
.field public static final TRY_FAILED:Ljava/lang/Object;


# instance fields
.field protected final method:Ljava/lang/reflect/Method;

.field protected final objectClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$1;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$1;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->TRY_FAILED:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V
    .locals 0
    .param p2, "theMethod"    # Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Method;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "theClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->objectClass:Ljava/lang/Class;

    .line 79
    iput-object p2, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->method:Ljava/lang/reflect/Method;

    .line 80
    return-void
.end method

.method static varargs initMarker(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "parms":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "xnever":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static varargs makeArgs([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 0
    .param p0, "args"    # [Ljava/lang/Object;

    .prologue
    .line 64
    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 85
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lorg/apache/commons/jexl2/internal/AbstractExecutor;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/apache/commons/jexl2/internal/AbstractExecutor;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->equals(Lorg/apache/commons/jexl2/internal/AbstractExecutor;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Lorg/apache/commons/jexl2/internal/AbstractExecutor;)Z
    .locals 5
    .param p1, "arg"    # Lorg/apache/commons/jexl2/internal/AbstractExecutor;

    .prologue
    const/4 v2, 0x0

    .line 101
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v2

    .line 104
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->getMethod()Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->getMethod()Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->getTargetClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->getTargetClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->getTargetProperty()Ljava/lang/Object;

    move-result-object v0

    .line 112
    .local v0, "lhsp":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->getTargetProperty()Ljava/lang/Object;

    move-result-object v1

    .line 113
    .local v1, "rhsp":Ljava/lang/Object;
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    .line 114
    const/4 v2, 0x1

    goto :goto_0

    .line 116
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public final getMethod()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->method:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public final getTargetClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->objectClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isAlive()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tryFailed(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "exec"    # Ljava/lang/Object;

    .prologue
    .line 181
    sget-object v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->TRY_FAILED:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
