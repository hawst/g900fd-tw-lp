.class public final Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
.super Ljava/lang/Object;
.source "MethodKey.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;,
        Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    }
.end annotation


# static fields
.field private static final CONSTRUCTORS:Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters",
            "<",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final METHODS:Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field

.field private static final NOARGS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final hashCode:I

.field private final method:Ljava/lang/String;

.field private final params:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Class;

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->NOARGS:[Ljava/lang/Class;

    .line 631
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$1;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$1;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->METHODS:Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;

    .line 642
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$2;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$2;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->CONSTRUCTORS:Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;[Ljava/lang/Class;)V
    .locals 6
    .param p1, "aMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p2, "args":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    .line 108
    iget-object v4, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 111
    .local v0, "hash":I
    if-eqz p2, :cond_0

    array-length v3, p2

    .local v3, "size":I
    if-lez v3, :cond_0

    .line 112
    new-array v4, v3, [Ljava/lang/Class;

    iput-object v4, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    .line 113
    const/4 v1, 0x0

    .local v1, "p":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 114
    aget-object v4, p2, v1

    invoke-static {v4}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->primitiveClass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v2

    .line 115
    .local v2, "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    mul-int/lit8 v4, v0, 0x25

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v5

    add-int v0, v4, v5

    .line 116
    iget-object v4, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    aput-object v2, v4, v1

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "p":I
    .end local v2    # "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "size":I
    :cond_0
    sget-object v4, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->NOARGS:[Ljava/lang/Class;

    iput-object v4, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    .line 121
    :cond_1
    iput v0, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->hashCode:I

    .line 122
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 7
    .param p1, "aMethod"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    .line 73
    iget-object v5, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 76
    .local v1, "hash":I
    if-eqz p2, :cond_1

    array-length v4, p2

    .local v4, "size":I
    if-lez v4, :cond_1

    .line 77
    new-array v5, v4, [Ljava/lang/Class;

    iput-object v5, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    .line 78
    const/4 v2, 0x0

    .local v2, "p":I
    :goto_0
    if-ge v2, v4, :cond_2

    .line 79
    aget-object v0, p2, v2

    .line 81
    .local v0, "arg":Ljava/lang/Object;
    if-nez v0, :cond_0

    const-class v3, Ljava/lang/Void;

    .line 82
    .local v3, "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    mul-int/lit8 v5, v1, 0x25

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v6

    add-int v1, v5, v6

    .line 83
    iget-object v5, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    aput-object v3, v5, v2

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    .end local v3    # "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    goto :goto_1

    .line 86
    .end local v0    # "arg":Ljava/lang/Object;
    .end local v2    # "p":I
    .end local v4    # "size":I
    :cond_1
    sget-object v5, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->NOARGS:[Ljava/lang/Class;

    iput-object v5, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    .line 88
    :cond_2
    iput v1, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->hashCode:I

    .line 89
    return-void
.end method

.method constructor <init>(Ljava/lang/reflect/Method;)V
    .locals 2
    .param p1, "aMethod"    # Ljava/lang/reflect/Method;

    .prologue
    .line 96
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Class;)V

    .line 97
    return-void
.end method

.method public static isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z
    .locals 3
    .param p2, "possibleVarArg"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;Z)Z"
        }
    .end annotation

    .prologue
    .local p0, "formal":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p1, "actual":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 227
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 278
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 239
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 240
    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_3

    const-class v2, Ljava/lang/Boolean;

    if-eq p1, v2, :cond_0

    .line 242
    :cond_3
    sget-object v2, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_4

    const-class v2, Ljava/lang/Character;

    if-eq p1, v2, :cond_0

    .line 244
    :cond_4
    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_5

    const-class v2, Ljava/lang/Byte;

    if-eq p1, v2, :cond_0

    .line 246
    :cond_5
    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_6

    const-class v2, Ljava/lang/Short;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Byte;

    if-eq p1, v2, :cond_0

    .line 249
    :cond_6
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_7

    const-class v2, Ljava/lang/Integer;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Short;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Byte;

    if-eq p1, v2, :cond_0

    .line 253
    :cond_7
    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_8

    const-class v2, Ljava/lang/Long;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Integer;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Short;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Byte;

    if-eq p1, v2, :cond_0

    .line 257
    :cond_8
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_9

    const-class v2, Ljava/lang/Float;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Long;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Integer;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Short;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Byte;

    if-eq p1, v2, :cond_0

    .line 262
    :cond_9
    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_a

    const-class v2, Ljava/lang/Double;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Float;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Long;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Integer;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Short;

    if-eq p1, v2, :cond_0

    const-class v2, Ljava/lang/Byte;

    if-eq p1, v2, :cond_0

    .line 271
    :cond_a
    if-eqz p2, :cond_c

    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 272
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 273
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object p1

    .line 275
    :cond_b
    invoke-virtual {p0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 278
    goto/16 :goto_0
.end method

.method public static isStrictInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z
    .locals 3
    .param p2, "possibleVarArg"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;Z)Z"
        }
    .end annotation

    .prologue
    .local p0, "formal":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p1, "actual":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 301
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 342
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 312
    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 313
    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_2

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    .line 315
    :cond_2
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_3

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    .line 318
    :cond_3
    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_4

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    .line 322
    :cond_4
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_5

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    .line 326
    :cond_5
    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne p0, v2, :cond_6

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-eq p1, v2, :cond_0

    .line 335
    :cond_6
    if-eqz p2, :cond_8

    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 336
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 337
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object p1

    .line 339
    :cond_7
    invoke-virtual {p0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isStrictInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v0

    goto :goto_0

    :cond_8
    move v0, v1

    .line 342
    goto :goto_0
.end method


# virtual methods
.method public debugString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 172
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/16 v2, 0x28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 173
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 174
    if-lez v1, :cond_0

    .line 175
    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_0
    const-class v2, Ljava/lang/Void;

    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    aget-object v3, v3, v1

    if-ne v2, v3, :cond_1

    const-string/jumbo v2, "null"

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 179
    :cond_2
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 149
    instance-of v2, p1, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 150
    check-cast v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    .line 151
    .local v0, "key":Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    iget-object v3, v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 153
    .end local v0    # "key":Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    :cond_0
    return v1
.end method

.method getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getMostSpecificConstructor(Ljava/util/List;)Ljava/lang/reflect/Constructor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;)",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "methods":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->CONSTRUCTORS:Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    # invokes: Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getMostSpecific(Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;
    invoke-static {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->access$000(Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Constructor;

    return-object v0
.end method

.method public getMostSpecificMethod(Ljava/util/List;)Ljava/lang/reflect/Method;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "methods":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->METHODS:Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    # invokes: Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getMostSpecific(Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;
    invoke-static {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->access$000(Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->method:Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 160
    .local v1, "builder":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->params:[Ljava/lang/Class;

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 161
    .local v2, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Ljava/lang/Void;

    if-ne v2, v5, :cond_0

    const-string/jumbo v5, "null"

    :goto_1
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 163
    .end local v2    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
