.class public final Lorg/apache/commons/jexl2/internal/MethodExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
.source "MethodExecutor.java"


# instance fields
.field private final isVarArgs:Z


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "args"    # [Ljava/lang/Object;

    .prologue
    .line 38
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, p2, p3, p4}, Lorg/apache/commons/jexl2/internal/MethodExecutor;->discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;-><init>(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;)V

    .line 39
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->method:Ljava/lang/reflect/Method;

    invoke-static {v0}, Lorg/apache/commons/jexl2/internal/MethodExecutor;->isVarArgMethod(Ljava/lang/reflect/Method;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->isVarArgs:Z

    .line 40
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;
    .locals 4
    .param p0, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 105
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 106
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v1, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-direct {v1, p2, p3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    .local v1, "key":Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 108
    .local v2, "m":Ljava/lang/reflect/Method;
    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    const-class v3, Lorg/apache/commons/jexl2/internal/ArrayListWrapper;

    invoke-virtual {p0, v3, v1}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 112
    :cond_0
    if-nez v2, :cond_1

    instance-of v3, p1, Ljava/lang/Class;

    if-eqz v3, :cond_1

    .line 113
    check-cast p1, Ljava/lang/Class;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1, v1}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 115
    :cond_1
    new-instance v3, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;

    invoke-direct {v3, v2, v1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;-><init>(Ljava/lang/reflect/Method;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)V

    return-object v3
.end method

.method private static isVarArgMethod(Ljava/lang/reflect/Method;)Z
    .locals 3
    .param p0, "m"    # Ljava/lang/reflect/Method;

    .prologue
    .line 173
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    .line 174
    .local v0, "formal":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    if-eqz v0, :cond_0

    array-length v2, v0

    if-nez v2, :cond_1

    .line 175
    :cond_0
    const/4 v2, 0x0

    .line 180
    :goto_0
    return v2

    .line 177
    :cond_1
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v0, v2

    .line 180
    .local v1, "last":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 53
    iget-boolean v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->isVarArgs:Z

    if-eqz v3, :cond_0

    .line 54
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    .line 55
    .local v0, "formal":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    array-length v3, v0

    add-int/lit8 v1, v3, -0x1

    .line 56
    .local v1, "index":I
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    .line 57
    .local v2, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    array-length v3, p2

    if-lt v3, v1, :cond_0

    .line 58
    invoke-virtual {p0, v2, v1, p2}, Lorg/apache/commons/jexl2/internal/MethodExecutor;->handleVarArg(Ljava/lang/Class;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    .line 61
    .end local v0    # "formal":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v1    # "index":I
    .end local v2    # "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lorg/apache/commons/jexl2/internal/ArrayListWrapper;

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->method:Ljava/lang/reflect/Method;

    new-instance v4, Lorg/apache/commons/jexl2/internal/ArrayListWrapper;

    invoke-direct {v4, p1}, Lorg/apache/commons/jexl2/internal/ArrayListWrapper;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 64
    :goto_0
    return-object v3

    :cond_1
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v3, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0
.end method

.method protected handleVarArg(Ljava/lang/Class;I[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 8
    .param p2, "index"    # I
    .param p3, "actual"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;I[",
            "Ljava/lang/Object;",
            ")[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 131
    array-length v4, p3

    if-ne v4, p2, :cond_1

    .line 133
    new-array p3, v7, [Ljava/lang/Object;

    .end local p3    # "actual":[Ljava/lang/Object;
    invoke-static {p1, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, p3, v6

    .line 164
    .restart local p3    # "actual":[Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object p3

    .line 134
    :cond_1
    array-length v4, p3

    add-int/lit8 v5, p2, 0x1

    if-ne v4, v5, :cond_2

    .line 137
    aget-object v4, p3, p2

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {p1, v4, v6}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    invoke-static {p1, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    .line 142
    .local v1, "lastActual":Ljava/lang/Object;
    aget-object v4, p3, p2

    invoke-static {v1, v6, v4}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 143
    aput-object v1, p3, p2

    goto :goto_0

    .line 145
    .end local v1    # "lastActual":Ljava/lang/Object;
    :cond_2
    array-length v4, p3

    add-int/lit8 v5, p2, 0x1

    if-le v4, v5, :cond_0

    .line 148
    array-length v4, p3

    sub-int v3, v4, p2

    .line 149
    .local v3, "size":I
    invoke-static {p1, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    .line 150
    .restart local v1    # "lastActual":Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_3

    .line 151
    add-int v4, p2, v0

    aget-object v4, p3, v4

    invoke-static {v1, v0, v4}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155
    :cond_3
    add-int/lit8 v4, p2, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    .line 156
    .local v2, "newActual":[Ljava/lang/Object;
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p2, :cond_4

    .line 157
    aget-object v4, p3, v0

    aput-object v4, v2, v0

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 159
    :cond_4
    aput-object v1, v2, p2

    .line 162
    move-object p3, v2

    goto :goto_0
.end method

.method public tryExecute(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "o"    # Ljava/lang/Object;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 71
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-direct {v0, p1, p3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    .local v0, "tkey":Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/MethodExecutor;->key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-virtual {v0, v3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 76
    :try_start_0
    invoke-virtual {p0, p2, p3}, Lorg/apache/commons/jexl2/internal/MethodExecutor;->execute(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 83
    :goto_0
    return-object v3

    .line 77
    :catch_0
    move-exception v2

    .line 78
    .local v2, "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    sget-object v3, Lorg/apache/commons/jexl2/internal/MethodExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 79
    .end local v2    # "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v1

    .line 80
    .local v1, "xill":Ljava/lang/IllegalAccessException;
    sget-object v3, Lorg/apache/commons/jexl2/internal/MethodExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 83
    .end local v1    # "xill":Ljava/lang/IllegalAccessException;
    :cond_0
    sget-object v3, Lorg/apache/commons/jexl2/internal/MethodExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
