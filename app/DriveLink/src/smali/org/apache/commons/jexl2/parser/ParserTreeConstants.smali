.class public interface abstract Lorg/apache/commons/jexl2/parser/ParserTreeConstants;
.super Ljava/lang/Object;
.source "ParserTreeConstants.java"


# static fields
.field public static final jjtNodeName:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56
    const/16 v0, 0x30

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "JexlScript"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "void"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "Block"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "Ambiguous"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "IfStatement"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "WhileStatement"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "ForeachStatement"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Assignment"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "TernaryNode"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "OrNode"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "AndNode"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "BitwiseOrNode"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "BitwiseXorNode"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "BitwiseAndNode"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "EQNode"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "NENode"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "LTNode"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "GTNode"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "LENode"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "GENode"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "ERNode"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "NRNode"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "AdditiveNode"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "AdditiveOperator"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "MulNode"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "DivNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "ModNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "UnaryMinusNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "BitwiseComplNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "NotNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "Identifier"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "NullLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "TrueNode"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "FalseNode"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "IntegerLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "FloatLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "StringLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "ArrayLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "MapLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "MapEntry"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "EmptyFunction"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "SizeFunction"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "FunctionNode"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "MethodNode"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "SizeMethod"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "ConstructorNode"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "ArrayAccess"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "Reference"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTreeConstants;->jjtNodeName:[Ljava/lang/String;

    return-void
.end method
