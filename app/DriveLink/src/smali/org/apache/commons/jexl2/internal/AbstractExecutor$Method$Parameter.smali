.class public final Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;
.super Ljava/lang/Object;
.source "AbstractExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "Parameter"
.end annotation


# instance fields
.field private final key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

.field private final method:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)V
    .locals 0
    .param p1, "m"    # Ljava/lang/reflect/Method;
    .param p2, "k"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->method:Ljava/lang/reflect/Method;

    .line 313
    iput-object p2, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    .line 314
    return-void
.end method

.method static synthetic access$000(Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;)Ljava/lang/reflect/Method;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;

    .prologue
    .line 302
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->method:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;)Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;

    .prologue
    .line 302
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    return-object v0
.end method
