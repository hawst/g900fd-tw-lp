.class final Lorg/apache/commons/jexl2/Debugger;
.super Ljava/lang/Object;
.source "Debugger.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/ParserVisitor;


# instance fields
.field private final builder:Ljava/lang/StringBuilder;

.field private cause:Lorg/apache/commons/jexl2/parser/JexlNode;

.field private end:I

.field private start:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 97
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 98
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 99
    return-void
.end method

.method private accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 151
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v1, :cond_0

    .line 152
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 154
    :cond_0
    invoke-virtual {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 155
    .local v0, "value":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v1, :cond_1

    .line 156
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 158
    :cond_1
    return-object v0
.end method

.method private acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "child"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 170
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTBlock;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTIfStatement;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;

    if-eqz v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-object v0

    .line 176
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "image"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v0, :cond_0

    .line 190
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 192
    :cond_0
    if-eqz p2, :cond_2

    .line 193
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v0, :cond_1

    .line 198
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 200
    :cond_1
    return-object p3

    .line 195
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "infix"    # Ljava/lang/String;
    .param p3, "paren"    # Z
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 213
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v1

    .line 214
    .local v1, "num":I
    if-eqz p3, :cond_0

    .line 215
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 218
    if-lez v0, :cond_1

    .line 219
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p4}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_2
    if-eqz p3, :cond_3

    .line 224
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_3
    return-object p4
.end method

.method private prefixChild(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 238
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v2

    if-le v2, v0, :cond_2

    .line 239
    .local v0, "paren":Z
    :goto_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    if-eqz v0, :cond_0

    .line 241
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_0
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-direct {p0, v1, p3}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    if-eqz v0, :cond_1

    .line 245
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_1
    return-object p3

    .end local v0    # "paren":Z
    :cond_2
    move v0, v1

    .line 238
    goto :goto_0
.end method


# virtual methods
.method public data()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public debug(Lorg/apache/commons/jexl2/parser/JexlNode;)Z
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    const/4 v1, 0x0

    .line 107
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 108
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 109
    if-eqz p1, :cond_1

    .line 110
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 111
    iput-object p1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 113
    move-object v0, p1

    .line 114
    .local v0, "root":Lorg/apache/commons/jexl2/parser/JexlNode;
    :goto_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 115
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    .end local v0    # "root":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_1
    iget v2, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    if-lez v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public end()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    return v0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    return v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 253
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTMulNode;

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTDivNode;

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTModNode;

    if-eqz v4, :cond_2

    :cond_0
    const/4 v2, 0x1

    .line 256
    .local v2, "paren":Z
    :goto_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetNumChildren()I

    move-result v1

    .line 257
    .local v1, "num":I
    if-eqz v2, :cond_1

    .line 258
    iget-object v4, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_1
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 262
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "num":I
    .end local v2    # "paren":Z
    :cond_2
    move v2, v3

    .line 253
    goto :goto_0

    .line 264
    .restart local v0    # "i":I
    .restart local v1    # "num":I
    .restart local v2    # "paren":Z
    :cond_3
    if-eqz v2, :cond_4

    .line 265
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_4
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/16 v2, 0x20

    .line 272
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 273
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    iget-object v1, p1, Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;->image:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 275
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAmbiguous;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAmbiguous;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 639
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unexpected type of node"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 280
    const-string/jumbo v0, " && "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayAccess;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 285
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetNumChildren()I

    move-result v1

    .line 287
    .local v1, "num":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 288
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 292
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 297
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetNumChildren()I

    move-result v1

    .line 298
    .local v1, "num":I
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 301
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAssignment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAssignment;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 310
    const-string/jumbo v0, " = "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 315
    const-string/jumbo v0, " & "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 320
    const-string/jumbo v0, "~"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->prefixChild(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 325
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    instance-of v0, v1, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;

    .line 326
    .local v0, "paren":Z
    const-string/jumbo v1, " | "

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 331
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    instance-of v0, v1, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;

    .line 332
    .local v0, "paren":Z
    const-string/jumbo v1, " ^ "

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBlock;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBlock;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 337
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v4, "{ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetNumChildren()I

    move-result v2

    .line 339
    .local v2, "num":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 340
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 341
    .local v0, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 343
    .end local v0    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v4, " }"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTConstructorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTConstructorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 477
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetNumChildren()I

    move-result v1

    .line 478
    .local v1, "num":I
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "new "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 482
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTDivNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTDivNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 349
    const-string/jumbo v0, " / "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEQNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEQNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 362
    const-string/jumbo v0, " == "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTERNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTERNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 367
    const-string/jumbo v0, " =~ "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "empty("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFalseNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFalseNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 372
    const-string/jumbo v0, "false"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 377
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;->image:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTForeachStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTForeachStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x2

    .line 382
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "for("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 388
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    :goto_0
    return-object p2

    .line 390
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFunctionNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFunctionNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 491
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetNumChildren()I

    move-result v1

    .line 492
    .local v1, "num":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 497
    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    .line 498
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 502
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 397
    const-string/jumbo v0, " >= "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 402
    const-string/jumbo v0, " > "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 407
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIfStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIfStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/16 v4, 0x3b

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 412
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "if ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 416
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 418
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " else "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    :goto_0
    return-object p2

    .line 421
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 424
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 431
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;->image:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 436
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v2

    .line 437
    .local v2, "num":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 438
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 439
    .local v0, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 441
    .end local v0    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 446
    const-string/jumbo v0, " <= "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 451
    const-string/jumbo v0, " < "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapEntry;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 456
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 464
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetNumChildren()I

    move-result v1

    .line 465
    .local v1, "num":I
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "{ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 468
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 471
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, " }"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMethodNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMethodNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 508
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetNumChildren()I

    move-result v1

    .line 509
    .local v1, "num":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 512
    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 513
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 517
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTModNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTModNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 523
    const-string/jumbo v0, " % "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMulNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMulNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 528
    const-string/jumbo v0, " * "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 533
    const-string/jumbo v0, " != "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNRNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNRNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 538
    const-string/jumbo v0, " !~ "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNotNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNotNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 543
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTNotNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNullLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNullLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 550
    const-string/jumbo v0, "null"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 557
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    instance-of v0, v1, Lorg/apache/commons/jexl2/parser/ASTAndNode;

    .line 558
    .local v0, "paren":Z
    const-string/jumbo v1, " || "

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReference;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReference;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 563
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetNumChildren()I

    move-result v1

    .line 564
    .local v1, "num":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 566
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 569
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 574
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "size("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTSizeFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeMethod;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeMethod;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 582
    const-string/jumbo v0, "size()"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTStringLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTStringLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 588
    iget-object v1, p1, Lorg/apache/commons/jexl2/parser/ASTStringLiteral;->image:Ljava/lang/String;

    const-string/jumbo v2, "\'"

    const-string/jumbo v3, "\\\'"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 589
    .local v0, "img":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTernaryNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTernaryNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 594
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 596
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 605
    :goto_0
    return-object p2

    .line 601
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTrueNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTrueNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 610
    const-string/jumbo v0, "true"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 616
    const-string/jumbo v0, "-"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->prefixChild(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTWhileStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTWhileStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 621
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "while ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 625
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    :goto_0
    return-object p2

    .line 627
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/SimpleNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/SimpleNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unexpected type of node"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
