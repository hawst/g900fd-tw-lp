.class public final Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
.source "PropertyGetExecutor.java"


# static fields
.field private static final EMPTY_PARAMS:[Ljava/lang/Object;


# instance fields
.field private final property:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->EMPTY_PARAMS:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "identifier"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1, p2, p3}, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 39
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->property:Ljava/lang/String;

    .line 40
    return-void
.end method

.method static discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 1
    .param p0, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p2, "property"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v0, "get"

    invoke-static {p0, v0, p1, p2}, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->discoverGet(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method static discoverGet(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 6
    .param p0, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p1, "which"    # Ljava/lang/String;
    .param p3, "property"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    .line 98
    .local v1, "method":Ljava/lang/reflect/Method;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 100
    .local v3, "start":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 101
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 104
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 105
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->EMPTY_PARAMS:[Ljava/lang/Object;

    invoke-virtual {p0, p2, v4, v5}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 107
    if-nez v1, :cond_0

    .line 108
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 109
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->EMPTY_PARAMS:[Ljava/lang/Object;

    invoke-virtual {p0, p2, v4, v5}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 111
    :cond_0
    return-object v1
.end method


# virtual methods
.method public execute(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 52
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->method:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->method:Ljava/lang/reflect/Method;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->property:Ljava/lang/String;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;

    .prologue
    .line 58
    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->property:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->method:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 69
    :goto_0
    return-object v2

    .line 63
    :catch_0
    move-exception v1

    .line 64
    .local v1, "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    sget-object v2, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 65
    .end local v1    # "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v0

    .line 66
    .local v0, "xill":Ljava/lang/IllegalAccessException;
    sget-object v2, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 69
    .end local v0    # "xill":Ljava/lang/IllegalAccessException;
    :cond_0
    sget-object v2, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
