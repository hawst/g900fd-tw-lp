.class public Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
.super Ljava/lang/Object;
.source "IntrospectorBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase$CacheMiss;
    }
.end annotation


# static fields
.field private static final CTOR_MISS:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final classMethodMaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap;",
            ">;"
        }
    .end annotation
.end field

.field private final constructibleClasses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final constructorsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;"
        }
    .end annotation
.end field

.field private loader:Ljava/lang/ClassLoader;

.field protected final rlog:Lorg/apache/commons/logging/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 162
    const-class v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase$CacheMiss;

    invoke-virtual {v0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->CTOR_MISS:Ljava/lang/reflect/Constructor;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/logging/Log;)V
    .locals 1
    .param p1, "log"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    .line 83
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    .line 84
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    .line 85
    return-void
.end method

.method private getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap;"
        }
    .end annotation

    .prologue
    .line 266
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    monitor-enter v2

    .line 267
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    .line 268
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    if-nez v0, :cond_0

    .line 269
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    .end local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    invoke-direct {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;-><init>(Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V

    .line 270
    .restart local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    :cond_0
    monitor-exit v2

    return-object v0

    .line 273
    .end local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public getConstructor(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Constructor;
    .locals 16
    .param p2, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 203
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    .line 204
    .local v5, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    monitor-enter v13
    :try_end_0
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    move-object/from16 v0, p2

    invoke-interface {v12, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Ljava/lang/reflect/Constructor;

    move-object v5, v0

    .line 207
    sget-object v12, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->CTOR_MISS:Ljava/lang/reflect/Constructor;

    invoke-virtual {v12, v5}, Ljava/lang/reflect/Constructor;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 208
    const/4 v12, 0x0

    monitor-exit v13

    move-object v5, v12

    .line 256
    .end local v5    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_0
    return-object v5

    .line 211
    .restart local v5    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :cond_0
    if-nez v5, :cond_4

    .line 212
    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMethod()Ljava/lang/String;

    move-result-object v4

    .line 214
    .local v4, "cname":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    invoke-interface {v12, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    .local v3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez v3, :cond_1

    .line 218
    if-eqz p1, :cond_2

    :try_start_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMethod()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 219
    move-object/from16 v3, p1

    .line 224
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    invoke-interface {v12, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_1
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 227
    .local v8, "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    invoke-virtual {v3}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v2

    .local v2, "arr$":[Ljava/lang/reflect/Constructor;
    array-length v9, v2

    .local v9, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_2
    if-ge v6, v9, :cond_3

    aget-object v7, v2, v6

    .line 228
    .local v7, "ictor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 221
    .end local v2    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v6    # "i$":I
    .end local v7    # "ictor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .end local v8    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .end local v9    # "len$":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {v12, v4}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    goto :goto_1

    .line 231
    .restart local v2    # "arr$":[Ljava/lang/reflect/Constructor;
    .restart local v6    # "i$":I
    .restart local v8    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .restart local v9    # "len$":I
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMostSpecificConstructor(Ljava/util/List;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    .line 232
    if-eqz v5, :cond_6

    .line 233
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    move-object/from16 v0, p2

    invoke-interface {v12, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 247
    .end local v2    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v3    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "cname":Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .end local v9    # "len$":I
    :cond_4
    :goto_3
    :try_start_3
    monitor-exit v13

    goto :goto_0

    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v12
    :try_end_4
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_4 .. :try_end_4} :catch_0

    .line 249
    :catch_0
    move-exception v1

    .line 251
    .local v1, "ae":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    if-eqz v12, :cond_5

    .line 252
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "ambiguous constructor invocation: new "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->debugString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lorg/apache/commons/logging/Log;->error(Ljava/lang/Object;)V

    .line 256
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 235
    .end local v1    # "ae":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    .restart local v2    # "arr$":[Ljava/lang/reflect/Constructor;
    .restart local v3    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v4    # "cname":Ljava/lang/String;
    .restart local v6    # "i$":I
    .restart local v8    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .restart local v9    # "len$":I
    :cond_6
    :try_start_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    sget-object v14, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->CTOR_MISS:Ljava/lang/reflect/Constructor;

    move-object/from16 v0, p2

    invoke-interface {v12, v0, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 237
    .end local v2    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v6    # "i$":I
    .end local v8    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .end local v9    # "len$":I
    :catch_1
    move-exception v11

    .line 238
    .local v11, "xnotfound":Ljava/lang/ClassNotFoundException;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    invoke-interface {v12}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 239
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "could not load class "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v12, v14, v11}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 241
    :cond_7
    const/4 v5, 0x0

    .line 245
    goto :goto_3

    .line 242
    .end local v11    # "xnotfound":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v10

    .line 243
    .local v10, "xambiguous":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "ambiguous ctor detected for "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v12, v14, v10}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 244
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public getField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 2
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v0

    .line 124
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->findField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    return-object v1
.end method

.method public getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    .locals 5
    .param p2, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v1

    .line 100
    .local v1, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->findMethod(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 109
    .end local v1    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    :goto_0
    return-object v2

    .line 101
    :catch_0
    move-exception v0

    .line 103
    .local v0, "ae":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ambiguous method invocation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->debugString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->error(Ljava/lang/Object;)V

    .line 109
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
