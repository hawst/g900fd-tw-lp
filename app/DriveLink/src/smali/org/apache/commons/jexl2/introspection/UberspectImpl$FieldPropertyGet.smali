.class public final Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;
.super Ljava/lang/Object;
.source "UberspectImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FieldPropertyGet"
.end annotation


# instance fields
.field private final field:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;)V
    .locals 0
    .param p1, "theField"    # Ljava/lang/reflect/Field;

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;->field:Ljava/lang/reflect/Field;

    .line 134
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public tryFailed(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "rval"    # Ljava/lang/Object;

    .prologue
    .line 161
    sget-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tryInvoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 147
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 154
    :goto_0
    return-object v1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "xill":Ljava/lang/IllegalAccessException;
    sget-object v1, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 154
    .end local v0    # "xill":Ljava/lang/IllegalAccessException;
    :cond_0
    sget-object v1, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
