.class public final Lorg/apache/commons/jexl2/internal/DuckSetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
.source "DuckSetExecutor.java"


# instance fields
.field private final property:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "key"    # Ljava/lang/Object;
    .param p4, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1, p2, p3, p4}, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 45
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->property:Ljava/lang/Object;

    .line 46
    return-void
.end method

.method private static discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/reflect/Method;
    .locals 3
    .param p0, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v0, "set"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v1}, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->makeArgs([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public execute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->property:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 59
    .local v0, "pargs":[Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_0
    return-object p2
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->property:Ljava/lang/Object;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 68
    if-eqz p1, :cond_0

    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->property:Ljava/lang/Object;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    const/4 v3, 0x2

    :try_start_0
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->property:Ljava/lang/Object;

    aput-object v4, v0, v3

    const/4 v3, 0x1

    aput-object p3, v0, v3

    .line 74
    .local v0, "args":[Ljava/lang/Object;
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v3, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 82
    .end local v0    # "args":[Ljava/lang/Object;
    .end local p3    # "value":Ljava/lang/Object;
    :goto_0
    return-object p3

    .line 76
    .restart local p3    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 77
    .local v2, "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    sget-object p3, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 78
    .end local v2    # "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v1

    .line 79
    .local v1, "xill":Ljava/lang/IllegalAccessException;
    sget-object p3, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 82
    .end local v1    # "xill":Ljava/lang/IllegalAccessException;
    :cond_0
    sget-object p3, Lorg/apache/commons/jexl2/internal/DuckSetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
