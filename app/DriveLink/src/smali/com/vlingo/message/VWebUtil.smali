.class public Lcom/vlingo/message/VWebUtil;
.super Ljava/lang/Object;
.source "VWebUtil.java"


# static fields
.field private static final FREE_FORM_HEADERS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final ivNonVLogger:Lorg/apache/log4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "x-vlconfiguration"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "x-vlenvironment"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "x-vlrequest"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/vlingo/message/VWebUtil;->FREE_FORM_HEADERS:Ljava/util/Set;

    .line 46
    const-class v0, Lcom/vlingo/message/VWebUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/String;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/message/VWebUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 47
    const-class v0, Lcom/vlingo/message/VWebUtil;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/message/VWebUtil;->ivNonVLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHeaderIgnoreCase(Lcom/vlingo/message/util/HeaderProvider;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "request"    # Lcom/vlingo/message/util/HeaderProvider;
    .param p1, "headerName"    # Ljava/lang/String;

    .prologue
    .line 200
    invoke-interface {p0}, Lcom/vlingo/message/util/HeaderProvider;->getHeaderNames()Ljava/util/Enumeration;

    move-result-object v0

    .line 201
    .local v0, "iter":Ljava/util/Enumeration;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 202
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 203
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    invoke-interface {p0, v1}, Lcom/vlingo/message/util/HeaderProvider;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 208
    .end local v1    # "key":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
