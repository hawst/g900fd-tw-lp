.class public Lcom/vlingo/dialog/manager/model/TaskFormManager;
.super Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;
.source "TaskFormManager.java"


# static fields
.field protected static final KEY_RESTORE_FIELDID:Ljava/lang/String; = "RestoreFieldId"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;-><init>()V

    return-void
.end method

.method private clearAndUpdateCompleteness(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 111
    :goto_0
    if-eqz p1, :cond_1

    .line 112
    sget-object v0, Lcom/vlingo/dialog/manager/model/TaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/manager/model/TaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "clearing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 113
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 114
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 115
    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->isRequired()Z

    move-result v0

    if-nez v0, :cond_2

    .line 119
    :cond_1
    return-void

    .line 111
    :cond_2
    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object p1

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object p2

    goto :goto_0
.end method

.method private processActionCancelledEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionCancelledEvent;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionCancelledEvent;

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->doCancelTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 63
    return-void
.end method

.method private processActionCompletedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionCompletedEvent;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionCompletedEvent;

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getActionCompletedTemplate()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "template":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 68
    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1, v2}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 70
    :cond_0
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 71
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->exitSetFieldId(Lcom/vlingo/dialog/DMContext;)V

    .line 72
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->removeTask(Lcom/vlingo/dialog/model/IForm;)V

    .line 73
    invoke-virtual {p1, v2}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 74
    return-void
.end method

.method private processActionConfirmedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionConfirmedEvent;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionConfirmedEvent;

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->doExecuteTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 58
    return-void
.end method

.method private processActionFailedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionFailedEvent;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionFailedEvent;

    .prologue
    const/4 v5, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getTop()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/manager/model/TopFormManager;

    .line 78
    .local v3, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getRestoreFieldId()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getTopPromptAfterFailure()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v4, 0x1

    .line 79
    .local v4, "topPromptAfterFailure":Z
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getActionFailedTemplate()Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "template":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 81
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionFailedEvent;->getReason()Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "reason":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {p1, v2, p2, v6, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 85
    .end local v1    # "reason":Ljava/lang/String;
    :cond_0
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 86
    .local v0, "parentForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->removeTask(Lcom/vlingo/dialog/model/IForm;)V

    .line 87
    if-eqz v4, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v6

    invoke-interface {v6, p1, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 90
    :cond_1
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 91
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->exitSetFieldId(Lcom/vlingo/dialog/DMContext;)V

    .line 92
    invoke-virtual {p1, v5}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 93
    return-void

    .end local v0    # "parentForm":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "template":Ljava/lang/String;
    .end local v4    # "topPromptAfterFailure":Z
    :cond_2
    move v4, v5

    .line 78
    goto :goto_0
.end method

.method private processContentChangedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContentChangedEvent;)V
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ContentChangedEvent;

    .prologue
    .line 96
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ContentChangedEvent;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 97
    .local v0, "slot":Lcom/vlingo/dialog/model/IForm;
    if-nez v0, :cond_0

    .line 98
    sget-object v3, Lcom/vlingo/dialog/manager/model/TaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "cannot find slot for ContentChangedEvent Key=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ContentChangedEvent;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\' -- ignoring"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 108
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ContentChangedEvent;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 102
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ContentChangedEvent;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 103
    .local v1, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-direct {p0, v1, v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->clearAndUpdateCompleteness(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 105
    .end local v1    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_1
    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 29
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    const-string/jumbo v1, "RestoreFieldId"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method protected convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "newTaskName"    # Ljava/lang/String;

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TopFormManager;

    .line 151
    .local v0, "topConfig":Lcom/vlingo/dialog/manager/model/TopFormManager;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method protected executeSetFieldId(Lcom/vlingo/dialog/DMContext;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getRestoreFieldId()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "fieldId":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 139
    .end local v0    # "fieldId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected exitSetFieldId(Lcom/vlingo/dialog/DMContext;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getRestoreFieldId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    const-string/jumbo v2, "RestoreFieldId"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "fieldId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    const-string/jumbo v2, "RestoreFieldId"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 130
    :goto_0
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 131
    return-void

    .line 128
    .end local v0    # "fieldId":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "fieldId":Ljava/lang/String;
    goto :goto_0
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 38
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionConfirmedEvent;

    if-eqz v0, :cond_0

    .line 39
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionConfirmedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processActionConfirmedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionConfirmedEvent;)V

    .line 53
    :goto_0
    return-void

    .line 40
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionCancelledEvent;

    if-eqz v0, :cond_1

    .line 41
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionCancelledEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processActionCancelledEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionCancelledEvent;)V

    goto :goto_0

    .line 42
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionCompletedEvent;

    if-eqz v0, :cond_2

    .line 43
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionCompletedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processActionCompletedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionCompletedEvent;)V

    goto :goto_0

    .line 44
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_2
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionFailedEvent;

    if-eqz v0, :cond_3

    .line 45
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionFailedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processActionFailedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionFailedEvent;)V

    goto :goto_0

    .line 46
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_3
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ContentChangedEvent;

    if-eqz v0, :cond_4

    .line 47
    check-cast p3, Lcom/vlingo/dialog/event/model/ContentChangedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processContentChangedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContentChangedEvent;)V

    goto :goto_0

    .line 48
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_4
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionNluEvent;

    if-eqz v0, :cond_5

    .line 49
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionNluEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processActionNluEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionNluEvent;)V

    goto :goto_0

    .line 51
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_5
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method protected removeTask(Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 33
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->removeSlot(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method protected sendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1, v1, p2}, Lcom/vlingo/dialog/DMContext;->addTaskGoal(Ljava/lang/String;ZZLcom/vlingo/dialog/model/IForm;)V

    .line 147
    return-void
.end method

.method public startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 18
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setContactClearCache(Z)V

    .line 19
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->storeRestoreFieldId(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public storeRestoreFieldId(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getRestoreFieldId()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 24
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    const-string/jumbo v1, "RestoreFieldId"

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    :cond_0
    return-void
.end method
