.class public Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManagerBase;
.source "TaskAddTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 37
    new-instance v0, Lcom/vlingo/dialog/event/model/Task;

    invoke-direct {v0}, Lcom/vlingo/dialog/event/model/Task;-><init>()V

    .line 38
    .local v0, "task":Lcom/vlingo/dialog/event/model/Task;
    const-string/jumbo v2, "title"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Task;->setTitle(Ljava/lang/String;)V

    .line 39
    const-string/jumbo v2, "date"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Task;->setDate(Ljava/lang/String;)V

    .line 40
    const-string/jumbo v2, "reminder_date"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Task;->setReminderDate(Ljava/lang/String;)V

    .line 41
    const-string/jumbo v2, "reminder_time"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Task;->setReminderTime(Ljava/lang/String;)V

    .line 42
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Task;->setComplete(Ljava/lang/Boolean;)V

    .line 44
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 45
    .local v1, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManager;->addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 47
    return-object v1
.end method

.method protected defaultDueDate(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 21
    const-string/jumbo v2, "date"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 22
    .local v0, "dateSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 23
    const-string/jumbo v2, "reminder_date"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 24
    .local v1, "reminderDateSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 26
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 32
    .end local v1    # "reminderDateSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    :goto_0
    return-void

    .line 29
    .restart local v1    # "reminderDateSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 13
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManager;->reminderFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskAddTaskFormManager;->defaultDueDate(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 18
    return-void
.end method
