.class public Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;
.super Ljava/lang/Object;
.source "SlotManager.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/manager/model/SlotManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ParseTypeTag"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x60884f7686bfe122L


# instance fields
.field tag:Ljava/lang/String;

.field typePattern:Ljava/util/regex/Pattern;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "tagSpec"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string/jumbo v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "items":[Ljava/lang/String;
    array-length v1, v0

    if-ne v1, v4, :cond_0

    .line 41
    aget-object v1, v0, v3

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->tag:Ljava/lang/String;

    .line 48
    :goto_0
    return-void

    .line 42
    :cond_0
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 43
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->typePattern:Ljava/util/regex/Pattern;

    .line 44
    aget-object v1, v0, v4

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->tag:Ljava/lang/String;

    goto :goto_0

    .line 46
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "bad tag spec: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method typeMatches(Ljava/lang/String;)Z
    .locals 1
    .param p1, "parseType"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->typePattern:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->typePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
