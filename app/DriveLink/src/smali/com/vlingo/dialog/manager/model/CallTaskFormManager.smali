.class public Lcom/vlingo/dialog/manager/model/CallTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CallTaskFormManagerBase;
.source "CallTaskFormManager.java"


# static fields
.field private static final SLOT_MODE:Ljava/lang/String; = "mode"

.field private static final SLOT_RECIPIENT:Ljava/lang/String; = "recipient"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CallTaskFormManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v6, 0x0

    .line 13
    const-string/jumbo v5, "mode"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 14
    .local v1, "modeSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_0

    .line 15
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "mode":Ljava/lang/String;
    const-string/jumbo v5, "videophone"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 17
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CallTaskFormManager;->getVideoCallCapability()Ljava/lang/String;

    move-result-object v2

    .line 18
    .local v2, "videoCallCapability":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 19
    invoke-virtual {p1, v2}, Lcom/vlingo/dialog/DMContext;->getCapabilityValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 20
    .local v3, "videoCallCapabilityValue":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string/jumbo v5, "DISABLED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CallTaskFormManager;->getVideoDisabledTemplate()Ljava/lang/String;

    move-result-object v4

    .line 22
    .local v4, "videoDisabledTemplate":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 23
    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v6, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 24
    invoke-interface {v1, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 30
    .end local v0    # "mode":Ljava/lang/String;
    .end local v2    # "videoCallCapability":Ljava/lang/String;
    .end local v3    # "videoCallCapabilityValue":Ljava/lang/String;
    .end local v4    # "videoDisabledTemplate":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CallTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 31
    return-void
.end method

.method protected issueConfirmation(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v4, 0x1

    .line 35
    const/4 v2, 0x0

    .line 36
    .local v2, "doConfirm":Z
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CallTaskFormManager;->getConfirmTemplate()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "confirmTemplate":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 38
    const-string/jumbo v5, "recipient"

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/manager/model/CallTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/ContactFormManager;

    .line 40
    .local v1, "contactFormManager":Lcom/vlingo/dialog/manager/model/ContactFormManager;
    const-string/jumbo v5, "recipient"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    invoke-virtual {v1, p1, v5}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->isConfident(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v5

    if-nez v5, :cond_2

    move v2, v4

    .line 42
    .end local v1    # "contactFormManager":Lcom/vlingo/dialog/manager/model/ContactFormManager;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 43
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CallTaskFormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 44
    .local v3, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CallTaskFormManager;->getConfirmFieldId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v0, v3, v5, v4}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 45
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 46
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->setConfirmExecute()V

    .line 48
    .end local v3    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    return v2

    .line 40
    .restart local v1    # "contactFormManager":Lcom/vlingo/dialog/manager/model/ContactFormManager;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
