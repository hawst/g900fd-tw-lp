.class public Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManagerBase;
.source "CalendarEditTaskFormManager.java"


# static fields
.field private static final SLOT_INVITEE_LIST:Ljava/lang/String; = "invitee_list"

.field private static final SLOT_NEW_BEGIN:Ljava/lang/String; = "newbegin"

.field private static final SLOT_NEW_DATE:Ljava/lang/String; = "newdate"

.field private static final SLOT_NEW_DURATION:Ljava/lang/String; = "newduration"

.field private static final SLOT_NEW_END_TIME:Ljava/lang/String; = "newendtime"

.field private static final SLOT_NEW_LOCATION:Ljava/lang/String; = "newlocation"

.field private static final SLOT_NEW_TITLE:Ljava/lang/String; = "newtitle"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private computeNewDuration(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V
    .locals 12
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 68
    const-string/jumbo v11, "newduration"

    invoke-interface {p2, v11}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 69
    .local v5, "newDurationSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v11, "newendtime"

    invoke-interface {p2, v11}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 70
    .local v7, "newEndTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 72
    .local v6, "newEndTime":Ljava/lang/String;
    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v11

    if-nez v11, :cond_1

    if-eqz v6, :cond_1

    .line 73
    const-string/jumbo v11, "newdate"

    invoke-interface {p2, v11}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v11

    invoke-interface {v11}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "newDate":Ljava/lang/String;
    const-string/jumbo v11, "newtime"

    invoke-interface {p2, v11}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v11

    invoke-interface {v11}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 75
    .local v8, "newTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    move-object v1, v4

    .line 76
    .local v1, "date":Ljava/lang/String;
    :goto_0
    if-eqz v8, :cond_3

    move-object v10, v8

    .line 77
    .local v10, "time":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 78
    .local v0, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-static {v0, v1, v10}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v9

    .line 79
    .local v9, "startDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-static {v0, v1, v6}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    .line 80
    .local v2, "endDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v2, v9}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 81
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    .line 83
    :cond_0
    invoke-virtual {v9, v2}, Lcom/vlingo/dialog/util/DateTime;->minutesUntil(Lcom/vlingo/dialog/util/DateTime;)I

    move-result v3

    .line 84
    .local v3, "minutes":I
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v11}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 86
    .end local v0    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v1    # "date":Ljava/lang/String;
    .end local v2    # "endDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v3    # "minutes":I
    .end local v4    # "newDate":Ljava/lang/String;
    .end local v8    # "newTime":Ljava/lang/String;
    .end local v9    # "startDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v10    # "time":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    invoke-interface {v7, v11}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 87
    return-void

    .line 75
    .restart local v4    # "newDate":Ljava/lang/String;
    .restart local v8    # "newTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 76
    .restart local v1    # "date":Ljava/lang/String;
    :cond_3
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v10

    goto :goto_1
.end method

.method private confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Lcom/vlingo/dialog/model/IForm;
    .locals 15
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointmentIn"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 194
    const-string/jumbo v13, "newtitle"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 195
    .local v12, "title":Ljava/lang/String;
    const-string/jumbo v13, "newdate"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 196
    .local v4, "date":Ljava/lang/String;
    const-string/jumbo v13, "newtime"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 197
    .local v11, "time":Ljava/lang/String;
    const-string/jumbo v13, "newduration"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 198
    .local v5, "duration":Ljava/lang/String;
    const-string/jumbo v13, "newlocation"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 200
    .local v9, "location":Ljava/lang/String;
    if-nez v11, :cond_5

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getAllDay()Z

    move-result v1

    .line 202
    .local v1, "allDay":Z
    :goto_0
    if-nez v12, :cond_0

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTitle()Ljava/lang/String;

    move-result-object v12

    .line 203
    :cond_0
    if-nez v4, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v4

    .line 204
    :cond_1
    if-nez v11, :cond_2

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v11

    .line 205
    :cond_2
    if-nez v5, :cond_3

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDuration()Ljava/lang/String;

    move-result-object v5

    .line 206
    :cond_3
    if-nez v9, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getLocation()Ljava/lang/String;

    move-result-object v9

    .line 209
    :cond_4
    new-instance v2, Lcom/vlingo/dialog/event/model/Appointment;

    invoke-direct {v2}, Lcom/vlingo/dialog/event/model/Appointment;-><init>()V

    .line 210
    .local v2, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v2, v12}, Lcom/vlingo/dialog/event/model/Appointment;->setTitle(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/event/model/Appointment;->setDate(Ljava/lang/String;)V

    .line 212
    invoke-virtual {v2, v11}, Lcom/vlingo/dialog/event/model/Appointment;->setTime(Ljava/lang/String;)V

    .line 213
    invoke-virtual {v2, v5}, Lcom/vlingo/dialog/event/model/Appointment;->setDuration(Ljava/lang/String;)V

    .line 214
    invoke-virtual {v2, v9}, Lcom/vlingo/dialog/event/model/Appointment;->setLocation(Ljava/lang/String;)V

    .line 215
    invoke-virtual {v2, v1}, Lcom/vlingo/dialog/event/model/Appointment;->setAllDay(Z)V

    .line 217
    invoke-virtual {v2}, Lcom/vlingo/dialog/event/model/Appointment;->getInvitees()Ljava/util/List;

    move-result-object v13

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getInvitees()Ljava/util/List;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 218
    const-string/jumbo v13, "invitee_list"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 219
    .local v8, "inviteeListForm":Lcom/vlingo/dialog/model/IForm;
    const/4 v6, 0x0

    .line 220
    .local v6, "i":I
    :goto_1
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v8, v13}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 221
    .local v7, "invitee":Lcom/vlingo/dialog/model/IForm;
    if-nez v7, :cond_6

    .line 229
    new-instance v10, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v10}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 230
    .local v10, "templateForm":Lcom/vlingo/dialog/model/Form;
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 232
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v10, v13}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 234
    return-object v10

    .line 200
    .end local v1    # "allDay":Z
    .end local v2    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v6    # "i":I
    .end local v7    # "invitee":Lcom/vlingo/dialog/model/IForm;
    .end local v8    # "inviteeListForm":Lcom/vlingo/dialog/model/IForm;
    .end local v10    # "templateForm":Lcom/vlingo/dialog/model/Form;
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 224
    .restart local v1    # "allDay":Z
    .restart local v2    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .restart local v6    # "i":I
    .restart local v7    # "invitee":Lcom/vlingo/dialog/model/IForm;
    .restart local v8    # "inviteeListForm":Lcom/vlingo/dialog/model/IForm;
    :cond_6
    new-instance v3, Lcom/vlingo/dialog/event/model/Contact;

    invoke-direct {v3}, Lcom/vlingo/dialog/event/model/Contact;-><init>()V

    .line 225
    .local v3, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/vlingo/dialog/event/model/Contact;->setName(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v2}, Lcom/vlingo/dialog/event/model/Appointment;->getInvitees()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method private resolveAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 31
    const-string/jumbo v10, "newdate"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "newDate":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v8

    .line 34
    .local v8, "savedDate":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v9

    .line 36
    .local v9, "savedTime":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 37
    .local v1, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "clientDate":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "clientTime":Ljava/lang/String;
    const-string/jumbo v10, "newtime"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 41
    .local v7, "newTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 42
    const-string/jumbo v10, "newtime"

    invoke-virtual {p0, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v6

    .line 43
    .local v6, "newTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    if-nez v3, :cond_5

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 45
    :cond_0
    invoke-static {v6, v7, v2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 50
    :goto_0
    invoke-static {v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 53
    .end local v6    # "newTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_1
    const-string/jumbo v10, "newendtime"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 54
    .local v5, "newEndTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 55
    const-string/jumbo v10, "newendtime"

    invoke-virtual {p0, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    .line 56
    .local v4, "newEndTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v5, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 57
    invoke-static {v4, v5, v9}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 58
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    if-nez v3, :cond_3

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 60
    :cond_2
    invoke-static {v4, v5, v2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 62
    :cond_3
    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 65
    .end local v4    # "newEndTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_4
    return-void

    .line 48
    .end local v5    # "newEndTimeSlot":Lcom/vlingo/dialog/model/IForm;
    .restart local v6    # "newTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_5
    invoke-static {v6, v7, v9}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPmNear(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 179
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 180
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 181
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v1

    .line 182
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .line 183
    .local v4, "results":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 184
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 185
    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 189
    .end local v0    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v1    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "results":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    goto :goto_0
.end method

.method protected foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V
    .locals 13
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p3}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->abortReadOnly(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    const-string/jumbo v10, "id"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 105
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 107
    const-string/jumbo v10, "newtitle"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    .line 108
    .local v9, "newTitleSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v10, "newdate"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 109
    .local v4, "newDateSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v10, "newtime"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 110
    .local v8, "newTimeSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v10, "newduration"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 111
    .local v5, "newDurationSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v10, "newlocation"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v6

    .line 112
    .local v6, "newLocationSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v10, "invitee_list"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 114
    .local v1, "inviteeListSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-direct/range {p0 .. p3}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->resolveAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    .line 116
    invoke-direct/range {p0 .. p3}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->computeNewDuration(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    .line 120
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 121
    .local v3, "newDate":Ljava/lang/String;
    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 122
    .local v7, "newTime":Ljava/lang/String;
    if-nez v3, :cond_3

    if-eqz v7, :cond_3

    .line 123
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v3

    .line 124
    invoke-interface {v4, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 134
    :cond_2
    :goto_1
    const/4 v0, 0x0

    .line 139
    .local v0, "anyChanges":Z
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v10

    or-int/2addr v0, v10

    .line 140
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v4, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v10

    or-int/2addr v0, v10

    .line 141
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v8, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v10

    or-int/2addr v0, v10

    .line 142
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getDuration()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v5, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v10

    or-int/2addr v0, v10

    .line 143
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getLocation()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v6, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v10

    or-int/2addr v0, v10

    .line 145
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_4

    const/4 v10, 0x1

    :goto_2
    or-int/2addr v0, v10

    .line 147
    if-eqz v0, :cond_6

    .line 150
    if-eqz v3, :cond_5

    if-eqz v7, :cond_5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "newBegin":Ljava/lang/String;
    :goto_3
    const-string/jumbo v10, "newbegin"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v10

    invoke-interface {v10, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 154
    invoke-super/range {p0 .. p3}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManagerBase;->foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    goto/16 :goto_0

    .line 126
    .end local v0    # "anyChanges":Z
    .end local v2    # "newBegin":Ljava/lang/String;
    :cond_3
    if-nez v7, :cond_2

    if-eqz v3, :cond_2

    .line 127
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v7

    .line 128
    invoke-interface {v8, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_1

    .line 145
    .restart local v0    # "anyChanges":Z
    :cond_4
    const/4 v10, 0x0

    goto :goto_2

    .line 150
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 158
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->getNoChangesTemplate()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {p1, v10, p2, v11, v12}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 159
    invoke-static/range {p3 .. p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-static {p1, v10}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->addAppointmentShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 167
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 169
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManager;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 170
    const/4 v0, 0x0

    .line 172
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarEditTaskFormManagerBase;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    goto :goto_0
.end method
