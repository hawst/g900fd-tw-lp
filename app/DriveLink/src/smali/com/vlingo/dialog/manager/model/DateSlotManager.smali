.class public Lcom/vlingo/dialog/manager/model/DateSlotManager;
.super Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;
.source "DateSlotManager.java"


# static fields
.field private static final MONTH_PATTERN:Ljava/util/regex/Pattern;

.field private static final RANGE_PATTERN:Ljava/util/regex/Pattern;

.field private static final WEEKEND_PATTERN:Ljava/util/regex/Pattern;

.field private static final WEEK_PATTERN:Ljava/util/regex/Pattern;

.field private static final YEAR_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string/jumbo v0, "\\d{4}-\\d{1,2}-\\d{1,2},\\d{4}-\\d{1,2}-\\d{1,2}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DateSlotManager;->RANGE_PATTERN:Ljava/util/regex/Pattern;

    .line 16
    const-string/jumbo v0, "w=(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DateSlotManager;->WEEK_PATTERN:Ljava/util/regex/Pattern;

    .line 18
    const-string/jumbo v0, "weekend=(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DateSlotManager;->WEEKEND_PATTERN:Ljava/util/regex/Pattern;

    .line 23
    const-string/jumbo v0, "(?:month=(january|february|march|april|may|june|july|august|september|october|november|december)\\s*|m=(\\d+)\\s*)+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DateSlotManager;->MONTH_PATTERN:Ljava/util/regex/Pattern;

    .line 27
    const-string/jumbo v0, "(?:year=(\\d+)\\s*|y=(\\d+)\\s*)+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DateSlotManager;->YEAR_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;-><init>()V

    return-void
.end method

.method private static getMatch(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    .line 117
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 118
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 121
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getMatcher(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    .line 126
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 127
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    .end local v0    # "matcher":Ljava/util/regex/Matcher;
    :goto_0
    return-object v0

    .restart local v0    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRange(Ljava/lang/String;)Z
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 109
    if-eqz p0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/manager/model/DateSlotManager;->RANGE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static splitRange(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 113
    const-string/jumbo v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toString(Lcom/vlingo/dialog/util/DateTime$Range;)Ljava/lang/String;
    .locals 2
    .param p0, "range"    # Lcom/vlingo/dialog/util/DateTime$Range;

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime$Range;->begin:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime$Range;->end:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 26
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 31
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v18

    .line 32
    .local v18, "oldValue":Ljava/lang/String;
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v17

    .line 33
    .local v17, "oldComplete":Z
    invoke-super/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 34
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v16

    .line 35
    .local v16, "newValue":Ljava/lang/String;
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v23

    if-eqz v23, :cond_1

    .line 37
    move-object/from16 v6, v16

    .line 38
    .local v6, "date":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v5

    .line 39
    .local v5, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v5}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "clientDate":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 41
    const/4 v14, 0x0

    .line 42
    .local v14, "newDate":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->getAllowRanges()Z

    move-result v3

    .line 45
    .local v3, "allowRanges":Z
    sget-object v23, Lcom/vlingo/dialog/manager/model/DateSlotManager;->WEEK_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->getMatch(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v10

    .local v10, "match":Ljava/lang/String;
    if-eqz v10, :cond_3

    .line 46
    if-eqz v3, :cond_2

    .line 47
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 48
    .local v13, "n":I
    const/16 v23, 0x1

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v5, v0, v13, v1}, Lcom/vlingo/dialog/util/DateTime;->getWeek(IIZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v19

    .line 49
    .local v19, "range":Lcom/vlingo/dialog/util/DateTime$Range;
    invoke-static/range {v19 .. v19}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->toString(Lcom/vlingo/dialog/util/DateTime$Range;)Ljava/lang/String;

    move-result-object v14

    .line 91
    .end local v13    # "n":I
    .end local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    :cond_0
    :goto_0
    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 92
    if-nez v14, :cond_c

    .line 93
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 94
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 106
    .end local v3    # "allowRanges":Z
    .end local v4    # "clientDate":Ljava/lang/String;
    .end local v5    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v6    # "date":Ljava/lang/String;
    .end local v10    # "match":Ljava/lang/String;
    .end local v14    # "newDate":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 51
    .restart local v3    # "allowRanges":Z
    .restart local v4    # "clientDate":Ljava/lang/String;
    .restart local v5    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .restart local v6    # "date":Ljava/lang/String;
    .restart local v10    # "match":Ljava/lang/String;
    .restart local v14    # "newDate":Ljava/lang/String;
    :cond_2
    const/4 v14, 0x0

    goto :goto_0

    .line 53
    :cond_3
    sget-object v23, Lcom/vlingo/dialog/manager/model/DateSlotManager;->WEEKEND_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->getMatch(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 54
    if-eqz v3, :cond_4

    .line 55
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 56
    .restart local v13    # "n":I
    const/16 v23, 0x7

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v5, v0, v13, v1}, Lcom/vlingo/dialog/util/DateTime;->getWeekend(IIZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v19

    .line 57
    .restart local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    invoke-static/range {v19 .. v19}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->toString(Lcom/vlingo/dialog/util/DateTime$Range;)Ljava/lang/String;

    move-result-object v14

    .line 58
    goto :goto_0

    .line 59
    .end local v13    # "n":I
    .end local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    :cond_4
    const/4 v14, 0x0

    goto :goto_0

    .line 61
    :cond_5
    sget-object v23, Lcom/vlingo/dialog/manager/model/DateSlotManager;->MONTH_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->getMatcher(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v11

    .local v11, "matcher":Ljava/util/regex/Matcher;
    if-eqz v11, :cond_8

    .line 62
    if-eqz v3, :cond_7

    .line 63
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 64
    .local v12, "month":Ljava/lang/String;
    const/16 v23, 0x2

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 65
    .local v9, "mString":Ljava/lang/String;
    if-eqz v9, :cond_6

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 66
    .local v8, "m":I
    :goto_2
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v5, v12, v8, v0}, Lcom/vlingo/dialog/util/DateTime;->getMonth(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v19

    .line 67
    .restart local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    invoke-static/range {v19 .. v19}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->toString(Lcom/vlingo/dialog/util/DateTime$Range;)Ljava/lang/String;

    move-result-object v14

    .line 68
    goto :goto_0

    .line 65
    .end local v8    # "m":I
    .end local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    :cond_6
    const/4 v8, 0x0

    goto :goto_2

    .line 69
    .end local v9    # "mString":Ljava/lang/String;
    .end local v12    # "month":Ljava/lang/String;
    :cond_7
    const/4 v14, 0x0

    goto :goto_0

    .line 71
    :cond_8
    sget-object v23, Lcom/vlingo/dialog/manager/model/DateSlotManager;->YEAR_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->getMatcher(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v11

    if-eqz v11, :cond_b

    .line 72
    if-eqz v3, :cond_a

    .line 73
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v22

    .line 74
    .local v22, "year":Ljava/lang/String;
    const/16 v23, 0x2

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v21

    .line 75
    .local v21, "yString":Ljava/lang/String;
    if-eqz v21, :cond_9

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 76
    .local v20, "y":I
    :goto_3
    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v20

    move/from16 v2, v23

    invoke-virtual {v5, v0, v1, v2}, Lcom/vlingo/dialog/util/DateTime;->getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v19

    .line 77
    .restart local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    invoke-static/range {v19 .. v19}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->toString(Lcom/vlingo/dialog/util/DateTime$Range;)Ljava/lang/String;

    move-result-object v14

    .line 78
    goto/16 :goto_0

    .line 75
    .end local v19    # "range":Lcom/vlingo/dialog/util/DateTime$Range;
    .end local v20    # "y":I
    :cond_9
    const/16 v20, 0x0

    goto :goto_3

    .line 79
    .end local v21    # "yString":Ljava/lang/String;
    .end local v22    # "year":Ljava/lang/String;
    :cond_a
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 82
    :cond_b
    invoke-static {v4, v6}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->modifyDate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 83
    const-string/jumbo v23, "dayNum="

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_0

    const-string/jumbo v23, "month="

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_0

    .line 84
    const/16 v23, 0x0

    move-object/from16 v0, v23

    invoke-static {v5, v14, v0}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v15

    .line 85
    .local v15, "newDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v15, v5}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 86
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/vlingo/dialog/util/DateTime;->addMonths(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v15

    .line 87
    invoke-virtual {v15}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 96
    .end local v11    # "matcher":Ljava/util/regex/Matcher;
    .end local v15    # "newDateTime":Lcom/vlingo/dialog/util/DateTime;
    :cond_c
    const/16 v23, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 99
    .end local v3    # "allowRanges":Z
    .end local v4    # "clientDate":Ljava/lang/String;
    .end local v5    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v10    # "match":Ljava/lang/String;
    .end local v14    # "newDate":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 100
    .local v7, "e":Ljava/lang/Exception;
    sget-object v23, Lcom/vlingo/dialog/manager/model/DateSlotManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "ignoring bad date cf: \'"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string/jumbo v25, "\'"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 101
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 102
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 103
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    goto/16 :goto_1
.end method
