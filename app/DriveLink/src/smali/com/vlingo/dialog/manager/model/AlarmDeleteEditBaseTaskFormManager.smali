.class public Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;
.source "AlarmDeleteEditBaseTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private processAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 115
    const-class v0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    invoke-virtual {p1, p3, v0}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 116
    invoke-static {p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->filterAlarms(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    .line 117
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 118
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    .line 119
    return-void
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    .line 122
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v3

    .line 123
    .local v3, "id":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->fetchAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    move-result-object v1

    .line 124
    .local v1, "are":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    const/4 v4, 0x0

    .line 125
    .local v4, "matchFound":Z
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 126
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 127
    const/4 v4, 0x1

    .line 128
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 129
    invoke-static {p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->resolveTimeAmPmFromAlarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    .line 130
    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    goto :goto_0

    .line 133
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    :cond_1
    if-nez v4, :cond_2

    .line 134
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "no matching alarm id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 136
    :cond_2
    return-void
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v1

    .line 26
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .line 27
    .local v5, "results":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    if-nez v5, :cond_1

    .line 29
    const-string/jumbo v6, "id"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 30
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v6

    invoke-static {p1, p2, v6}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 49
    :cond_0
    :goto_0
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 50
    return-void

    .line 32
    :cond_1
    const-string/jumbo v6, "id"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 34
    const/4 v4, 0x0

    .line 35
    .local v4, "matchFound":Z
    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 36
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 37
    const/4 v4, 0x1

    .line 38
    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    .line 42
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    :cond_3
    if-nez v4, :cond_0

    .line 43
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unmatched id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 46
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "matchFound":Z
    :cond_4
    invoke-virtual {p0, p1, p2, v5}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    goto :goto_0
.end method

.method protected deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 155
    sget-object v0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 156
    .local v4, "slotName":Ljava/lang/String;
    invoke-interface {p1, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 157
    .local v3, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v3, :cond_0

    .line 158
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/vlingo/dialog/model/IForm;->setDisabled(Ljava/lang/Boolean;)V

    .line 155
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    .end local v3    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "slotName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 142
    const-string/jumbo v1, "newdays"

    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 143
    .local v0, "newDaysSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 144
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Alarm;->getDays()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/util/Day;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 149
    :cond_0
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 150
    const/4 v1, 0x1

    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 151
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 152
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 104
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    if-eqz v0, :cond_0

    .line 105
    check-cast p3, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->processAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    .line 111
    :goto_0
    return-void

    .line 106
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 107
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 109
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method protected processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V
    .locals 19
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "results"    # Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .prologue
    .line 53
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getNumMatches()I

    move-result v7

    .line 54
    .local v7, "numMatches":I
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v11

    .line 55
    .local v11, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    if-nez v7, :cond_2

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getFullBackoff()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->anyConstraints(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 59
    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V

    .line 60
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 65
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 66
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->removeTask(Lcom/vlingo/dialog/model/IForm;)V

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 69
    :cond_2
    const/4 v3, 0x1

    if-ne v7, v3, :cond_3

    .line 70
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/dialog/event/model/Alarm;

    .line 71
    .local v10, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    const-string/jumbo v3, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-virtual {v10}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 72
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v10}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    goto :goto_0

    .line 74
    .end local v10    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    :cond_3
    const/16 v16, 0x1

    .line 75
    .local v16, "needChoose":Z
    const-string/jumbo v3, "choice"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v14

    .line 76
    .local v14, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v14, :cond_4

    invoke-interface {v14}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 77
    invoke-interface {v14}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v12

    .line 78
    .local v12, "choice":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 79
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 80
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v3

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6, v12}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v13

    .line 81
    .local v13, "choiceIndex":I
    if-ltz v13, :cond_4

    .line 82
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/dialog/event/model/Alarm;

    .line 83
    .restart local v10    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    const-string/jumbo v3, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-virtual {v10}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 84
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v10}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    .line 85
    const/16 v16, 0x0

    .line 88
    .end local v10    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v12    # "choice":Ljava/lang/String;
    .end local v13    # "choiceIndex":I
    :cond_4
    if-eqz v16, :cond_0

    .line 89
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getChooseFieldId()Ljava/lang/String;

    move-result-object v15

    .line 90
    .local v15, "fieldId":Ljava/lang/String;
    if-nez v15, :cond_5

    .line 91
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v15

    .line 93
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    .line 94
    .local v18, "visibleChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    const/4 v6, 0x0

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-virtual/range {v3 .. v9}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v17

    .line 95
    .local v17, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static/range {v17 .. v18}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->getChooseTemplate()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1, v15, v4}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 97
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;->addAlarmChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    goto/16 :goto_0
.end method
