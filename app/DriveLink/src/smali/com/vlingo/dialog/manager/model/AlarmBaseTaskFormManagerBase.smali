.class public Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/TaskFormManager;
.source "AlarmBaseTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Limit:Ljava/lang/String; = "Limit"

.field public static final PROP_NoneFoundTemplate:Ljava/lang/String; = "NoneFoundTemplate"


# instance fields
.field private ID:J

.field private Limit:I

.field private NoneFoundTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->Limit:I

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->Limit:I

    return v0
.end method

.method public getNoneFoundTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->NoneFoundTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->ID:J

    .line 33
    return-void
.end method

.method public setLimit(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->Limit:I

    .line 26
    return-void
.end method

.method public setNoneFoundTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->NoneFoundTemplate:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
