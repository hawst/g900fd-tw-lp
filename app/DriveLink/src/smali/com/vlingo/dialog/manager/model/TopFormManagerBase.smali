.class public Lcom/vlingo/dialog/manager/model/TopFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/FormManager;
.source "TopFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_DisabledTemplate:Ljava/lang/String; = "DisabledTemplate"

.field public static final PROP_EyesFreeModeTemplateSuffix:Ljava/lang/String; = "EyesFreeModeTemplateSuffix"

.field public static final PROP_Hijacks:Ljava/lang/String; = "Hijacks"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Passthroughs:Ljava/lang/String; = "Passthroughs"

.field public static final PROP_SkipOtherParseTypes:Ljava/lang/String; = "SkipOtherParseTypes"

.field public static final PROP_TopLevelFieldIds:Ljava/lang/String; = "TopLevelFieldIds"

.field public static final PROP_TopPromptAfterFailure:Ljava/lang/String; = "TopPromptAfterFailure"


# instance fields
.field private DisabledTemplate:Ljava/lang/String;

.field private EyesFreeModeTemplateSuffix:Ljava/lang/String;

.field private Hijacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/Hijack;",
            ">;"
        }
    .end annotation
.end field

.field private ID:J

.field private Passthroughs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/Passthrough;",
            ">;"
        }
    .end annotation
.end field

.field private SkipOtherParseTypes:Z

.field private TopLevelFieldIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/TopLevelFieldId;",
            ">;"
        }
    .end annotation
.end field

.field private TopPromptAfterFailure:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/FormManager;-><init>()V

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->SkipOtherParseTypes:Z

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->TopPromptAfterFailure:Z

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->TopLevelFieldIds:Ljava/util/List;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->Hijacks:Ljava/util/List;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->Passthroughs:Ljava/util/List;

    .line 23
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 67
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TopFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 71
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TopFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDisabledTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->DisabledTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getEyesFreeModeTemplateSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->EyesFreeModeTemplateSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public getHijacks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/Hijack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->Hijacks:Ljava/util/List;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getPassthroughs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/Passthrough;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->Passthroughs:Ljava/util/List;

    return-object v0
.end method

.method public getSkipOtherParseTypes()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->SkipOtherParseTypes:Z

    return v0
.end method

.method public getTopLevelFieldIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/TopLevelFieldId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->TopLevelFieldIds:Ljava/util/List;

    return-object v0
.end method

.method public getTopPromptAfterFailure()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->TopPromptAfterFailure:Z

    return v0
.end method

.method public setDisabledTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->DisabledTemplate:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setEyesFreeModeTemplateSuffix(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->EyesFreeModeTemplateSuffix:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->ID:J

    .line 63
    return-void
.end method

.method public setSkipOtherParseTypes(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->SkipOtherParseTypes:Z

    .line 29
    return-void
.end method

.method public setTopPromptAfterFailure(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->TopPromptAfterFailure:Z

    .line 50
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
