.class public Lcom/vlingo/dialog/manager/model/TopFormManager;
.super Lcom/vlingo/dialog/manager/model/TopFormManagerBase;
.source "TopFormManager.java"


# static fields
.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private hijackEventMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hijackMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/manager/model/Hijack;",
            ">;>;"
        }
    .end annotation
.end field

.field private topLevelFieldIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/manager/model/TopLevelFieldId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/dialog/manager/model/TopFormManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->topLevelFieldIdMap:Ljava/util/Map;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackMap:Ljava/util/Map;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackEventMap:Ljava/util/Map;

    .line 35
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->setName(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private addFailedCapabilityPrompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "taskManager"    # Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 223
    invoke-virtual {p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    .line 224
    .local v3, "taskName":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getCapability()Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "capabilityName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 226
    move-object v0, v3

    .line 228
    :cond_0
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->getCapabilityValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "capabilityValue":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v2}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 230
    .local v2, "taskForm":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 231
    invoke-static {p3, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getDisabledTemplate(Lcom/vlingo/dialog/manager/model/TaskFormManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "template":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 234
    invoke-virtual {p1, v4, v2, v6, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getDisabledTemplate()Ljava/lang/String;

    move-result-object v4

    .line 238
    invoke-virtual {p1, v4, v2, v6, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private checkCapability(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "taskManager"    # Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .prologue
    .line 209
    invoke-virtual {p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    .line 210
    .local v2, "taskName":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getCapability()Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "capabilityName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 212
    move-object v0, v2

    .line 214
    :cond_0
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->getCapabilityValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 219
    .local v1, "capabilityValue":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string/jumbo v3, "DISABLED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "DISABLED:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static convertForm(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p0, "srcFormManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "srcForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "dstForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 306
    invoke-interface {p0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getSlots()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 307
    .local v5, "srcSlotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v5}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    .line 308
    .local v3, "slotName":Ljava/lang/String;
    invoke-interface {p1, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 309
    .local v4, "srcSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v4, :cond_0

    .line 310
    invoke-interface {v5}, Lcom/vlingo/dialog/manager/model/IFormManager;->getTransferPath()Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "dstPath":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 312
    move-object v0, v3

    .line 314
    :cond_1
    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 315
    .local v1, "dstSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_0

    .line 316
    invoke-static {v5, v4, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->convertForm(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 320
    .end local v0    # "dstPath":Ljava/lang/String;
    .end local v1    # "dstSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v3    # "slotName":Ljava/lang/String;
    .end local v4    # "srcSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v5    # "srcSlotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 321
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v6

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 322
    return-void
.end method

.method private static getDisabledTemplate(Lcom/vlingo/dialog/manager/model/TaskFormManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "manager"    # Lcom/vlingo/dialog/manager/model/TaskFormManager;
    .param p1, "capabilityValue"    # Ljava/lang/String;

    .prologue
    .line 243
    const/4 v3, 0x0

    .line 244
    .local v3, "template":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getDisabledTemplates()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/DisabledTemplate;

    .line 245
    .local v0, "dt":Lcom/vlingo/dialog/manager/model/DisabledTemplate;
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DisabledTemplate;->getMatches()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "matchesRegex":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 247
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DisabledTemplate;->getTemplate()Ljava/lang/String;

    move-result-object v3

    .line 251
    .end local v0    # "dt":Lcom/vlingo/dialog/manager/model/DisabledTemplate;
    .end local v2    # "matchesRegex":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method private getHijack(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/Hijack;
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "parseType"    # Ljava/lang/String;

    .prologue
    .line 356
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 357
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/Hijack;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/Hijack;

    goto :goto_0
.end method

.method private getHijackManagerForParse(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/TaskFormManager;
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 362
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "fieldId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v2

    .line 365
    .local v2, "parseType":Ljava/lang/String;
    invoke-direct {p0, v0, v2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getHijack(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/Hijack;

    move-result-object v1

    .line 366
    .local v1, "hijack":Lcom/vlingo/dialog/manager/model/Hijack;
    if-nez v1, :cond_1

    const/4 v4, 0x0

    .line 374
    :cond_0
    :goto_0
    return-object v4

    .line 368
    :cond_1
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/Hijack;->getTask()Ljava/lang/String;

    move-result-object v3

    .line 369
    .local v3, "task":Ljava/lang/String;
    invoke-virtual {p1, v3}, Lcom/vlingo/dialog/DMContext;->appendDialogSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 370
    .local v4, "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    if-nez v4, :cond_0

    .line 371
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    .end local v4    # "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    check-cast v4, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .restart local v4    # "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    goto :goto_0
.end method

.method private softwareVersionValid(Ljava/lang/String;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z
    .locals 5
    .param p1, "softwareVersion"    # Ljava/lang/String;
    .param p2, "taskManager"    # Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 407
    invoke-virtual {p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v1

    .line 408
    .local v1, "minSoftwareVersion":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getMaxSoftwareVersion()Ljava/lang/String;

    move-result-object v0

    .line 410
    .local v0, "maxSoftwareVersion":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v2

    .line 412
    :cond_1
    if-eqz v1, :cond_2

    invoke-static {p1, v1}, Lcom/vlingo/voicepad/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_2

    move v2, v3

    .line 413
    goto :goto_0

    .line 414
    :cond_2
    if-eqz v0, :cond_0

    invoke-static {p1, v0}, Lcom/vlingo/voicepad/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_0

    move v2, v3

    .line 415
    goto :goto_0
.end method

.method private startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "taskManager"    # Lcom/vlingo/dialog/manager/model/TaskFormManager;
    .param p4, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 181
    invoke-virtual {p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getName()Ljava/lang/String;

    move-result-object v1

    .line 183
    .local v1, "taskName":Ljava/lang/String;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->removeTasks(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 185
    invoke-direct {p0, p1, p3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->checkCapability(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    invoke-virtual {p3}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->createForm()Lcom/vlingo/dialog/model/Form;

    move-result-object v0

    .line 190
    .local v0, "taskForm":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 191
    invoke-virtual {p3, p1, v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 192
    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lcom/vlingo/dialog/DMContext;->startTask(Ljava/lang/String;Z)V

    .line 193
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 195
    if-nez p4, :cond_0

    .line 196
    invoke-virtual {p3, p1, v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 206
    .end local v0    # "taskForm":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    return-void

    .line 198
    .restart local v0    # "taskForm":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    invoke-virtual {p3, p1, v0, p4}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0

    .line 203
    .end local v0    # "taskForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->addFailedCapabilityPrompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;)V

    goto :goto_0
.end method


# virtual methods
.method public bumpUpParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 270
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    .line 271
    .local v0, "childManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getSeamlessEscape()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 273
    const/4 v2, 0x1

    .line 278
    :goto_0
    return v2

    .line 275
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v1

    .line 276
    .local v1, "parse":Lcom/vlingo/dialog/util/Parse;
    sget-object v2, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unhandled parse: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 277
    :cond_1
    invoke-static {}, Lcom/vlingo/dialog/util/FailFast;->failure()V

    .line 278
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 256
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "should not be called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "oldTask"    # Lcom/vlingo/dialog/model/IForm;
    .param p4, "newTaskName"    # Ljava/lang/String;

    .prologue
    .line 283
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v3

    .line 284
    .local v3, "oldTaskName":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .line 285
    .local v2, "oldTaskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-virtual {p1, p4}, Lcom/vlingo/dialog/DMContext;->appendDialogSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 286
    .local v1, "newTaskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    if-nez v1, :cond_0

    .line 287
    invoke-virtual {p0, p4}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .end local v1    # "newTaskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    check-cast v1, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 290
    .restart local v1    # "newTaskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->checkCapability(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 292
    sget-object v4, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "converting task "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 293
    :cond_1
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->createForm()Lcom/vlingo/dialog/model/Form;

    move-result-object v0

    .line 294
    .local v0, "newTask":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v2, p3, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->convertForm(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 295
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->removeTasks(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 296
    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 297
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 298
    invoke-virtual {v1, p1, v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 303
    .end local v0    # "newTask":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    return-void

    .line 300
    :cond_2
    sget-object v4, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "converting task "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " failed due to capabilities"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 301
    :cond_3
    invoke-direct {p0, p1, p2, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->addFailedCapabilityPrompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;)V

    goto :goto_0
.end method

.method public createForm()Lcom/vlingo/dialog/model/Form;
    .locals 6

    .prologue
    .line 75
    new-instance v0, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 76
    .local v0, "f":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/model/Form;->setValue(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v1

    .line 79
    .local v1, "fSlots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 80
    .local v4, "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    instance-of v5, v4, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-nez v5, :cond_0

    .line 81
    invoke-interface {v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->createForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 82
    .local v3, "s":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v3, v0}, Lcom/vlingo/dialog/model/IForm;->setParent(Lcom/vlingo/dialog/model/IForm;)V

    .line 83
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    .end local v3    # "s":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/model/Form;->setModified(Z)V

    .line 87
    return-object v0
.end method

.method public bridge synthetic createForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->createForm()Lcom/vlingo/dialog/model/Form;

    move-result-object v0

    return-object v0
.end method

.method public doPassthrough(Lcom/vlingo/dialog/DMContext;)Z
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getPassthroughs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/Passthrough;

    .line 423
    .local v1, "passthrough":Lcom/vlingo/dialog/manager/model/Passthrough;
    invoke-virtual {v1, p1}, Lcom/vlingo/dialog/manager/model/Passthrough;->matches(Lcom/vlingo/dialog/DMContext;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 424
    const/4 v2, 0x1

    .line 427
    .end local v1    # "passthrough":Lcom/vlingo/dialog/manager/model/Passthrough;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public hijackEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)Z
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    const/4 v3, 0x0

    .line 389
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "eventName":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackEventMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 391
    .local v1, "task":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 403
    :cond_0
    :goto_0
    return v3

    .line 393
    :cond_1
    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/DMContext;->appendDialogSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 394
    .local v2, "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    if-nez v2, :cond_2

    .line 395
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .end local v2    # "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    check-cast v2, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 397
    .restart local v2    # "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    :cond_2
    if-eqz v2, :cond_0

    .line 401
    invoke-direct {p0, p1, p2, v2, p3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;Lcom/vlingo/dialog/event/model/Event;)V

    .line 403
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public hijackParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getHijackManagerForParse(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/TaskFormManager;

    move-result-object v0

    .line 380
    .local v0, "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    if-nez v0, :cond_0

    .line 381
    const/4 v1, 0x0

    .line 384
    :goto_0
    return v1

    .line 383
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;Lcom/vlingo/dialog/event/model/Event;)V

    .line 384
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSkipParseType(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "parseType"    # Ljava/lang/String;

    .prologue
    .line 342
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->topLevelFieldIdMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;

    .line 343
    .local v0, "topLevelFieldId":Lcom/vlingo/dialog/manager/model/TopLevelFieldId;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->isSkipParseType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTopLevelFieldId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->topLevelFieldIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public mightHijack(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 347
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 348
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/Hijack;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 12

    .prologue
    .line 40
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->postDeserialize()V

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getTopLevelFieldIds()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;

    .line 42
    .local v9, "topLevelFieldId":Lcom/vlingo/dialog/manager/model/TopLevelFieldId;
    invoke-virtual {v9}, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->postDeserialize()V

    .line 43
    iget-object v10, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->topLevelFieldIdMap:Ljava/util/Map;

    invoke-virtual {v9}, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->getFieldId()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 45
    .end local v9    # "topLevelFieldId":Lcom/vlingo/dialog/manager/model/TopLevelFieldId;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getHijacks()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/Hijack;

    .line 46
    .local v2, "hijack":Lcom/vlingo/dialog/manager/model/Hijack;
    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/Hijack;->getFieldIds()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/vlingo/dialog/manager/model/TopFormManager;->splitList(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 47
    .local v1, "fieldId":Ljava/lang/String;
    iget-object v10, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackMap:Ljava/util/Map;

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 48
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/Hijack;>;"
    if-nez v6, :cond_3

    .line 49
    iget-object v10, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackMap:Ljava/util/Map;

    new-instance v6, Ljava/util/HashMap;

    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/Hijack;>;"
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .restart local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/Hijack;>;"
    invoke-interface {v10, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_3
    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/Hijack;->getParseTypes()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/vlingo/dialog/manager/model/TopFormManager;->splitList(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 52
    .local v7, "parseType":Ljava/lang/String;
    invoke-interface {v6, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 55
    .end local v1    # "fieldId":Ljava/lang/String;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/Hijack;>;"
    .end local v7    # "parseType":Ljava/lang/String;
    :cond_4
    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/Hijack;->getEvents()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/vlingo/dialog/manager/model/TopFormManager;->splitList(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    .local v0, "event":Ljava/lang/String;
    iget-object v10, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackEventMap:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/Hijack;->getTask()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v0, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 59
    .end local v0    # "event":Ljava/lang/String;
    .end local v2    # "hijack":Lcom/vlingo/dialog/manager/model/Hijack;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getPassthroughs()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/dialog/manager/model/Passthrough;

    .line 60
    .local v8, "passthrough":Lcom/vlingo/dialog/manager/model/Passthrough;
    invoke-virtual {v8}, Lcom/vlingo/dialog/manager/model/Passthrough;->postDeserialize()V

    goto :goto_3

    .line 62
    .end local v8    # "passthrough":Lcom/vlingo/dialog/manager/model/Passthrough;
    :cond_6
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 103
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :goto_0
    return-void

    .line 104
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionNluEvent;

    if-eqz v0, :cond_1

    .line 105
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionNluEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->processActionNluEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionNluEvent;)V

    goto :goto_0

    .line 107
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    sget-object v0, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unhandled event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 108
    :cond_2
    invoke-static {}, Lcom/vlingo/dialog/util/FailFast;->failure()V

    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v10, 0x0

    .line 116
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->setupTopLevelState()V

    .line 118
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v2

    .line 120
    .local v2, "parse":Lcom/vlingo/dialog/util/Parse;
    iget-object v7, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->cancelParseTypeSet:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 121
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->doCancelTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 178
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 127
    .local v4, "taskList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/manager/model/IFormManager;>;"
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getSoftwareVersion()Ljava/lang/String;

    move-result-object v3

    .line 131
    .local v3, "softwareVersion":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "dialogSuffix":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 135
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlots()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 136
    .local v5, "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v5, p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->hasParseType(Lcom/vlingo/dialog/DMContext;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v7, v5

    .line 137
    check-cast v7, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    invoke-direct {p0, v3, v7}, Lcom/vlingo/dialog/manager/model/TopFormManager;->softwareVersionValid(Ljava/lang/String;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 138
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 142
    .end local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_6

    .line 143
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlots()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 144
    .restart local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v5, p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->hasParseType(Lcom/vlingo/dialog/DMContext;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v5}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    move-object v7, v5

    .line 145
    check-cast v7, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    invoke-direct {p0, v3, v7}, Lcom/vlingo/dialog/manager/model/TopFormManager;->softwareVersionValid(Ljava/lang/String;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 146
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 153
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlots()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 154
    .restart local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v5, p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->hasParseType(Lcom/vlingo/dialog/DMContext;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v5}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    move-object v7, v5

    .line 155
    check-cast v7, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    invoke-direct {p0, v3, v7}, Lcom/vlingo/dialog/manager/model/TopFormManager;->softwareVersionValid(Ljava/lang/String;Lcom/vlingo/dialog/manager/model/TaskFormManager;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 156
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 162
    .end local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_6
    sget-object v7, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v7

    if-eqz v7, :cond_7

    sget-object v7, Lcom/vlingo/dialog/manager/model/TopFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Possible tasks: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 164
    :cond_7
    const/4 v5, 0x0

    .line 165
    .local v5, "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_8

    .line 167
    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    check-cast v5, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 170
    .restart local v5    # "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    :cond_8
    if-nez v5, :cond_9

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSkipOtherParseTypes()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 171
    invoke-virtual {p1, v2}, Lcom/vlingo/dialog/DMContext;->startOutsideTask(Lcom/vlingo/dialog/util/Parse;)V

    goto/16 :goto_0

    .line 172
    :cond_9
    if-eqz v5, :cond_a

    .line 173
    const/4 v7, 0x0

    invoke-direct {p0, p1, p2, v5, v7}, Lcom/vlingo/dialog/manager/model/TopFormManager;->startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TaskFormManager;Lcom/vlingo/dialog/event/model/Event;)V

    goto/16 :goto_0

    .line 175
    :cond_a
    iget-object v7, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->promptTemplateList:Ljava/util/List;

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 176
    .local v6, "template":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p1, v6, p2, v7, v8}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method public prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 261
    iget-object v3, p0, Lcom/vlingo/dialog/manager/model/TopFormManager;->promptTemplateList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 262
    .local v2, "promptTemplate":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "fieldId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getPromptAutoListen()Z

    move-result v0

    .line 264
    .local v0, "autoListen":Z
    invoke-virtual {p1, v2, p2, v1, v0}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 265
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 266
    return-void
.end method

.method public removeTasks(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlots()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 92
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    instance-of v4, v2, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-eqz v4, :cond_0

    move-object v3, v2

    .line 93
    check-cast v3, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 94
    .local v3, "task":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "name":Ljava/lang/String;
    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 96
    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->removeSlot(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    .end local v3    # "task":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    :cond_1
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-string/jumbo v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "name must be \"\""

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/dialog/manager/model/TopFormManagerBase;->setName(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public unprune(Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/dialog/manager/model/TopFormManager;->equal(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 327
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "name mismatch: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " != "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 329
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 330
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .line 331
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    instance-of v3, v2, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-nez v3, :cond_1

    .line 332
    invoke-interface {v2, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->unprune(Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 335
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    return-void
.end method

.method public willHijackParse(Lcom/vlingo/dialog/DMContext;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 352
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getHijackManagerForParse(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/TaskFormManager;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
