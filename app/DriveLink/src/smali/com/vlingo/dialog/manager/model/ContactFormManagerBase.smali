.class public Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/FormManager;
.source "ContactFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_AnaphoraFieldIds:Ljava/lang/String; = "AnaphoraFieldIds"

.field public static final PROP_ChooseContactTemplate:Ljava/lang/String; = "ChooseContactTemplate"

.field public static final PROP_ChooseTypeTemplate:Ljava/lang/String; = "ChooseTypeTemplate"

.field public static final PROP_ConfidenceThreshold:Ljava/lang/String; = "ConfidenceThreshold"

.field public static final PROP_ContactLimit:Ljava/lang/String; = "ContactLimit"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_NoAnaphoraTemplate:Ljava/lang/String; = "NoAnaphoraTemplate"

.field public static final PROP_NoContactFoundTemplate:Ljava/lang/String; = "NoContactFoundTemplate"

.field public static final PROP_NoTypeFoundTemplate:Ljava/lang/String; = "NoTypeFoundTemplate"

.field public static final PROP_TypeLimit:Ljava/lang/String; = "TypeLimit"

.field public static final PROP_TypeMismatchTemplate:Ljava/lang/String; = "TypeMismatchTemplate"


# instance fields
.field private AnaphoraFieldIds:Ljava/lang/String;

.field private ChooseContactTemplate:Ljava/lang/String;

.field private ChooseTypeTemplate:Ljava/lang/String;

.field private ConfidenceThreshold:Ljava/lang/Float;

.field private ContactLimit:Ljava/lang/Integer;

.field private ID:J

.field private NoAnaphoraTemplate:Ljava/lang/String;

.field private NoContactFoundTemplate:Ljava/lang/String;

.field private NoTypeFoundTemplate:Ljava/lang/String;

.field private TypeLimit:Ljava/lang/Integer;

.field private TypeMismatchTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/FormManager;-><init>()V

    .line 29
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/ContactFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAnaphoraFieldIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->AnaphoraFieldIds:Ljava/lang/String;

    return-object v0
.end method

.method public getChooseContactTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ChooseContactTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getChooseTypeTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ChooseTypeTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 113
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/ContactFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getConfidenceThreshold()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ConfidenceThreshold:Ljava/lang/Float;

    return-object v0
.end method

.method public getContactLimit()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ContactLimit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getNoAnaphoraTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->NoAnaphoraTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getNoContactFoundTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->NoContactFoundTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getNoTypeFoundTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->NoTypeFoundTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeLimit()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->TypeLimit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getTypeMismatchTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->TypeMismatchTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public setAnaphoraFieldIds(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->AnaphoraFieldIds:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setChooseContactTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ChooseContactTemplate:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setChooseTypeTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ChooseTypeTemplate:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setConfidenceThreshold(Ljava/lang/Float;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Float;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ConfidenceThreshold:Ljava/lang/Float;

    .line 98
    return-void
.end method

.method public setContactLimit(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ContactLimit:Ljava/lang/Integer;

    .line 84
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 104
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->ID:J

    .line 105
    return-void
.end method

.method public setNoAnaphoraTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->NoAnaphoraTemplate:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setNoContactFoundTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->NoContactFoundTemplate:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setNoTypeFoundTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->NoTypeFoundTemplate:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setTypeLimit(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->TypeLimit:Ljava/lang/Integer;

    .line 91
    return-void
.end method

.method public setTypeMismatchTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->TypeMismatchTemplate:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
