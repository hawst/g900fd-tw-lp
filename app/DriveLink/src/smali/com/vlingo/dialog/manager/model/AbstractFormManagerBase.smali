.class public abstract Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;
.super Ljava/lang/Object;
.source "AbstractFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/dialog/manager/model/IFormManager;
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_CancelFieldId:Ljava/lang/String; = "CancelFieldId"

.field public static final PROP_CancelNoParseTypes:Ljava/lang/String; = "CancelNoParseTypes"

.field public static final PROP_CancelParseTypes:Ljava/lang/String; = "CancelParseTypes"

.field public static final PROP_CancelTemplate:Ljava/lang/String; = "CancelTemplate"

.field public static final PROP_CancelYesParseTypes:Ljava/lang/String; = "CancelYesParseTypes"

.field public static final PROP_ChooseFieldId:Ljava/lang/String; = "ChooseFieldId"

.field public static final PROP_ConfirmFieldId:Ljava/lang/String; = "ConfirmFieldId"

.field public static final PROP_ConfirmNoParseTypes:Ljava/lang/String; = "ConfirmNoParseTypes"

.field public static final PROP_ConfirmNoTemplate:Ljava/lang/String; = "ConfirmNoTemplate"

.field public static final PROP_ConfirmTemplate:Ljava/lang/String; = "ConfirmTemplate"

.field public static final PROP_ConfirmYesParseTypes:Ljava/lang/String; = "ConfirmYesParseTypes"

.field public static final PROP_ExceptParseTypes:Ljava/lang/String; = "ExceptParseTypes"

.field public static final PROP_ExecuteTemplate:Ljava/lang/String; = "ExecuteTemplate"

.field public static final PROP_FieldId:Ljava/lang/String; = "FieldId"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_ParseTypes:Ljava/lang/String; = "ParseTypes"

.field public static final PROP_PromptAutoListen:Ljava/lang/String; = "PromptAutoListen"

.field public static final PROP_PromptTemplates:Ljava/lang/String; = "PromptTemplates"

.field public static final PROP_Required:Ljava/lang/String; = "Required"

.field public static final PROP_Scrolling:Ljava/lang/String; = "Scrolling"

.field public static final PROP_Sticky:Ljava/lang/String; = "Sticky"

.field public static final PROP_TransferPath:Ljava/lang/String; = "TransferPath"

.field public static final PROP_Value:Ljava/lang/String; = "Value"


# instance fields
.field private CancelFieldId:Ljava/lang/String;

.field private CancelNoParseTypes:Ljava/lang/String;

.field private CancelParseTypes:Ljava/lang/String;

.field private CancelTemplate:Ljava/lang/String;

.field private CancelYesParseTypes:Ljava/lang/String;

.field private ChooseFieldId:Ljava/lang/String;

.field private ConfirmFieldId:Ljava/lang/String;

.field private ConfirmNoParseTypes:Ljava/lang/String;

.field private ConfirmNoTemplate:Ljava/lang/String;

.field private ConfirmTemplate:Ljava/lang/String;

.field private ConfirmYesParseTypes:Ljava/lang/String;

.field private ExceptParseTypes:Ljava/lang/String;

.field private ExecuteTemplate:Ljava/lang/String;

.field private FieldId:Ljava/lang/String;

.field private ID:J

.field private Name:Ljava/lang/String;

.field private ParseTypes:Ljava/lang/String;

.field private PromptAutoListen:Z

.field private PromptTemplates:Ljava/lang/String;

.field private Required:Z

.field private Scrolling:Z

.field private Sticky:Z

.field private TransferPath:Ljava/lang/String;

.field private Value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Sticky:Z

    .line 27
    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->PromptAutoListen:Z

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Scrolling:Z

    .line 55
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 226
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AbstractFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCancelFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelFieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getCancelNoParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelNoParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getCancelParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getCancelTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getCancelYesParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelYesParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getChooseFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ChooseFieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 230
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AbstractFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getConfirmFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmFieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getConfirmNoParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmNoParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getConfirmNoTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmNoTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getConfirmTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getConfirmYesParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmYesParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getExceptParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ExceptParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getExecuteTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ExecuteTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->FieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 218
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptAutoListen()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->PromptAutoListen:Z

    return v0
.end method

.method public getPromptTemplates()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->PromptTemplates:Ljava/lang/String;

    return-object v0
.end method

.method public getRequired()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Required:Z

    return v0
.end method

.method public getScrolling()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Scrolling:Z

    return v0
.end method

.method public getSticky()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Sticky:Z

    return v0
.end method

.method public getTransferPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->TransferPath:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Value:Ljava/lang/String;

    return-object v0
.end method

.method public setCancelFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelFieldId:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public setCancelNoParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelNoParseTypes:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setCancelParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelParseTypes:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setCancelTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelTemplate:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public setCancelYesParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->CancelYesParseTypes:Ljava/lang/String;

    .line 194
    return-void
.end method

.method public setChooseFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ChooseFieldId:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setConfirmFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmFieldId:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setConfirmNoParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmNoParseTypes:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setConfirmNoTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmNoTemplate:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public setConfirmTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmTemplate:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setConfirmYesParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ConfirmYesParseTypes:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public setExceptParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ExceptParseTypes:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setExecuteTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ExecuteTemplate:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->FieldId:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 221
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ID:J

    .line 222
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Name:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->ParseTypes:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setPromptAutoListen(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->PromptAutoListen:Z

    .line 138
    return-void
.end method

.method public setPromptTemplates(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->PromptTemplates:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setRequired(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Required:Z

    .line 75
    return-void
.end method

.method public setScrolling(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 214
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Scrolling:Z

    .line 215
    return-void
.end method

.method public setSticky(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Sticky:Z

    .line 82
    return-void
.end method

.method public setTransferPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->TransferPath:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;->Value:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
