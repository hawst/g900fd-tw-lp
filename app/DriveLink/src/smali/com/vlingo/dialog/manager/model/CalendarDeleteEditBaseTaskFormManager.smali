.class public Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;
.source "CalendarDeleteEditBaseTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private processAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    .line 125
    invoke-static {p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->filterAppointments(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 126
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 127
    const-class v0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    invoke-virtual {p1, p3, v0}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 128
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 129
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 130
    return-void
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    .line 133
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "id":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->fetchAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    move-result-object v1

    .line 135
    .local v1, "are":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    const/4 v4, 0x0

    .line 136
    .local v4, "matchFound":Z
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 137
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 138
    const/4 v4, 0x1

    .line 140
    invoke-static {p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->resolveTimeAmPmFromAppointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    .line 141
    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    goto :goto_0

    .line 144
    .end local v0    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    :cond_1
    if-nez v4, :cond_2

    .line 145
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "no matching appointment id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 147
    :cond_2
    return-void
.end method


# virtual methods
.method protected abortReadOnly(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Z
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getReadOnlyTemplate()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Appointment;->getReadOnly()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getReadOnlyTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, p2, v2, v0}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 163
    invoke-virtual {p1, p0, p2}, Lcom/vlingo/dialog/DMContext;->resetToTop(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V

    .line 164
    const/4 v0, 0x1

    .line 166
    :cond_0
    return v0
.end method

.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v1

    .line 26
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .line 27
    .local v5, "results":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    if-nez v5, :cond_0

    .line 29
    const-string/jumbo v6, "id"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 30
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v6

    invoke-static {p1, p2, v6}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 31
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 51
    :goto_0
    return-void

    .line 33
    :cond_0
    const-string/jumbo v6, "id"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 35
    const/4 v4, 0x0

    .line 36
    .local v4, "matchFound":Z
    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 37
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 38
    const/4 v4, 0x1

    .line 39
    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    .line 43
    .end local v0    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    :cond_2
    if-nez v4, :cond_3

    .line 44
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unmatched id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 46
    :cond_3
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 48
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "matchFound":Z
    :cond_4
    invoke-virtual {p0, p1, p2, v5}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    goto :goto_0
.end method

.method protected deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 171
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 172
    .local v4, "slotName":Ljava/lang/String;
    invoke-interface {p1, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 173
    .local v3, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v3, :cond_0

    .line 174
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/vlingo/dialog/model/IForm;->setDisabled(Ljava/lang/Boolean;)V

    .line 171
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v3    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "slotName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 151
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->abortReadOnly(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 153
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 154
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 155
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 157
    :cond_0
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 115
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    if-eqz v0, :cond_0

    .line 116
    check-cast p3, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->processAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 122
    :goto_0
    return-void

    .line 117
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 118
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 120
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method protected processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V
    .locals 21
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "results"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    .line 55
    const-string/jumbo v3, "true"

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v4

    const-string/jumbo v5, "event_no_match_issued"

    invoke-virtual {v4, v5}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    .line 56
    .local v17, "noMatchIssued":Z
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v3

    const-string/jumbo v4, "event_no_match_issued"

    invoke-virtual {v3, v4}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 58
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getNumMatches()I

    move-result v7

    .line 59
    .local v7, "numMatches":I
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v11

    .line 60
    .local v11, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    if-nez v7, :cond_4

    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getFullBackoff()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->anyConstraintsSet(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 62
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getNoMatchFoundTemplate()Ljava/lang/String;

    move-result-object v18

    .line 63
    .local v18, "noMatchTemplate":Ljava/lang/String;
    if-eqz v18, :cond_0

    .line 64
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 65
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v3

    const-string/jumbo v4, "event_no_match_issued"

    const-string/jumbo v5, "true"

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V

    .line 70
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 111
    .end local v18    # "noMatchTemplate":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 73
    :cond_2
    if-nez v17, :cond_3

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 77
    :cond_3
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 78
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->removeTask(Lcom/vlingo/dialog/model/IForm;)V

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 81
    :cond_4
    const/4 v3, 0x1

    if-ne v7, v3, :cond_5

    .line 82
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/dialog/event/model/Appointment;

    .line 84
    .local v10, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v10}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    goto :goto_0

    .line 86
    .end local v10    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    :cond_5
    const/16 v16, 0x1

    .line 87
    .local v16, "needChoose":Z
    const-string/jumbo v3, "choice"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v14

    .line 88
    .local v14, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v14, :cond_7

    invoke-interface {v14}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 89
    invoke-interface {v14}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v12

    .line 90
    .local v12, "choice":Ljava/lang/String;
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v3

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6, v12}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v13

    .line 91
    .local v13, "choiceIndex":I
    if-ltz v13, :cond_6

    .line 92
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/dialog/event/model/Appointment;

    .line 93
    .restart local v10    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v10}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->foundUniqueAppointment(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    .line 94
    const/16 v16, 0x0

    .line 96
    .end local v10    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    :cond_6
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 97
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 99
    .end local v12    # "choice":Ljava/lang/String;
    .end local v13    # "choiceIndex":I
    :cond_7
    if-eqz v16, :cond_1

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getChooseFieldId()Ljava/lang/String;

    move-result-object v15

    .line 101
    .local v15, "fieldId":Ljava/lang/String;
    if-nez v15, :cond_8

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v15

    .line 104
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v20

    .line 105
    .local v20, "visibleChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    const/4 v6, 0x0

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-virtual/range {v3 .. v9}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v19

    .line 106
    .local v19, "templateForm":Lcom/vlingo/dialog/model/IForm;
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->getChooseTemplate()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1, v15, v4}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 108
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;->addAppointmentChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    goto/16 :goto_0
.end method
