.class public Lcom/vlingo/dialog/manager/model/SlotManager;
.super Lcom/vlingo/dialog/manager/model/SlotManagerBase;
.source "SlotManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;
    }
.end annotation


# static fields
.field public static final CANONICAL_FORM:Ljava/lang/String; = ":cf"

.field public static final CANONICAL_SPOKEN_FORM:Ljava/lang/String; = ":csf"

.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field protected ignorePattern:Ljava/util/regex/Pattern;

.field protected parseTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected parseTypeTagList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/dialog/manager/model/SlotManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/SlotManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/SlotManagerBase;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeTagList:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeMap:Ljava/util/Map;

    .line 34
    return-void
.end method


# virtual methods
.method public bridge synthetic createForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->createForm()Lcom/vlingo/dialog/model/Slot;

    move-result-object v0

    return-object v0
.end method

.method public createForm()Lcom/vlingo/dialog/model/Slot;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/vlingo/dialog/model/Slot;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Slot;-><init>()V

    .line 81
    .local v0, "s":Lcom/vlingo/dialog/model/Slot;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Slot;->setName(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Slot;->setValue(Ljava/lang/String;)V

    .line 83
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Slot;->setModified(Z)V

    .line 84
    return-object v0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v7, 0x1

    .line 104
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->isDisabled()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v6

    if-nez v6, :cond_4

    .line 105
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v1

    .line 106
    .local v1, "parse":Lcom/vlingo/dialog/util/Parse;
    if-eqz v1, :cond_4

    .line 107
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getSticky()Z

    move-result v6

    if-nez v6, :cond_0

    .line 108
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/SlotManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 110
    :cond_0
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "parseType":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->exceptParseTypeSet:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 112
    const/4 v2, 0x0

    .line 113
    .local v2, "parseTagMatched":Z
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeTagList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;

    .line 114
    .local v4, "tagSpec":Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;
    invoke-virtual {v4, v3}, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->typeMatches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 115
    iget-object v6, v4, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 116
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_1

    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->ignorePattern:Ljava/util/regex/Pattern;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->ignorePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_1

    .line 117
    :cond_2
    const/4 v2, 0x1

    .line 118
    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 119
    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 120
    iget-object v6, v4, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lcom/vlingo/dialog/util/Parse;->consume(Ljava/lang/String;)V

    .line 125
    .end local v4    # "tagSpec":Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;
    .end local v5    # "value":Ljava/lang/String;
    :cond_3
    if-nez v2, :cond_4

    .line 126
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 127
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 128
    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 129
    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 135
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "parse":Lcom/vlingo/dialog/util/Parse;
    .end local v2    # "parseTagMatched":Z
    .end local v3    # "parseType":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public postDeserialize()V
    .locals 8

    .prologue
    .line 56
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/SlotManagerBase;->postDeserialize()V

    .line 58
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeTagList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getParseTags()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/SlotManager;->splitList(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 60
    .local v3, "tag":Ljava/lang/String;
    const-string/jumbo v4, ":cf"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 61
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeTagList:Ljava/util/List;

    new-instance v5, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ":csf"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeTagList:Ljava/util/List;

    new-instance v5, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;

    invoke-direct {v5, v3}, Lcom/vlingo/dialog/manager/model/SlotManager$ParseTypeTag;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    .end local v3    # "tag":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getMappings()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/Mapping;

    .line 67
    .local v1, "mapping":Lcom/vlingo/dialog/manager/model/Mapping;
    instance-of v4, v1, Lcom/vlingo/dialog/manager/model/ParseTypeMapping;

    if-eqz v4, :cond_2

    move-object v2, v1

    .line 68
    check-cast v2, Lcom/vlingo/dialog/manager/model/ParseTypeMapping;

    .line 69
    .local v2, "parseTypeMapping":Lcom/vlingo/dialog/manager/model/ParseTypeMapping;
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->parseTypeMap:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/ParseTypeMapping;->getParseType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/ParseTypeMapping;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 73
    .end local v1    # "mapping":Lcom/vlingo/dialog/manager/model/Mapping;
    .end local v2    # "parseTypeMapping":Lcom/vlingo/dialog/manager/model/ParseTypeMapping;
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getIgnoreValue()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 74
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getIgnoreValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->ignorePattern:Ljava/util/regex/Pattern;

    .line 76
    :cond_4
    return-void
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 100
    return-void
.end method

.method public prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 139
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->promptTemplateList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 140
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "no prompts: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 142
    :cond_0
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getPromptCountAsInt()I

    move-result v1

    .line 143
    .local v1, "count":I
    add-int/lit8 v4, v1, 0x1

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->setPromptCount(I)V

    .line 144
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->promptTemplateList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 145
    .local v2, "index":I
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/SlotManager;->promptTemplateList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 146
    .local v3, "template":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getPromptAutoListen()Z

    move-result v0

    .line 147
    .local v0, "autoListen":Z
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/SlotManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v3, v4, v5, v0}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 148
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 149
    return-void
.end method
