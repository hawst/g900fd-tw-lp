.class public Lcom/vlingo/dialog/manager/model/CalendarDeleteTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CalendarDeleteTaskFormManagerBase;
.source "CalendarDeleteTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Lcom/vlingo/dialog/model/IForm;
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 32
    new-instance v0, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 33
    .local v0, "templateForm":Lcom/vlingo/dialog/model/Form;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 35
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/vlingo/dialog/manager/model/CalendarDeleteTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 37
    return-object v0
.end method


# virtual methods
.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 17
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 18
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarDeleteTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v1

    .line 20
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .line 21
    .local v4, "results":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 22
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 23
    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarDeleteTaskFormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 27
    .end local v0    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v1    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "results":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    goto :goto_0
.end method
