.class public final enum Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;
.super Ljava/lang/Enum;
.source "TimeSlotManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/manager/model/TimeSlotManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "AmPmMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

.field public static final enum AFTER:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

.field public static final enum AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

.field public static final enum FORCE_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

.field public static final enum FORCE_DEFAULT:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

.field public static final enum NEAR:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const-string/jumbo v1, "FORCE_DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_DEFAULT:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    .line 25
    new-instance v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const-string/jumbo v1, "FORCE_AM"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    .line 26
    new-instance v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const-string/jumbo v1, "NEAR"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->NEAR:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    .line 27
    new-instance v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const-string/jumbo v1, "AFTER"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    .line 28
    new-instance v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const-string/jumbo v1, "AFTER_OR_AM"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_DEFAULT:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->NEAR:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->$VALUES:[Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->$VALUES:[Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    invoke-virtual {v0}, [Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    return-object v0
.end method
