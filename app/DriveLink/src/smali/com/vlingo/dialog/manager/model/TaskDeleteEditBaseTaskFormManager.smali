.class public Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManagerBase;
.source "TaskDeleteEditBaseTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    .line 117
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "id":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->fetchTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    move-result-object v0

    .line 119
    .local v0, "are":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    const/4 v3, 0x0

    .line 120
    .local v3, "matchFound":Z
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/Task;

    .line 121
    .local v4, "task":Lcom/vlingo/dialog/event/model/Task;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 122
    const/4 v3, 0x1

    .line 123
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0, p1, p2, v4}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V

    goto :goto_0

    .line 127
    .end local v4    # "task":Lcom/vlingo/dialog/event/model/Task;
    :cond_1
    if-nez v3, :cond_2

    .line 128
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "no matching task id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 130
    :cond_2
    return-void
.end method

.method private processTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 111
    const-class v0, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    invoke-virtual {p1, p3, v0}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 112
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 113
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V

    .line 114
    return-void
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 22
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .line 23
    .local v4, "results":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    if-nez v4, :cond_1

    .line 25
    const-string/jumbo v6, "id"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 26
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v6

    invoke-static {p1, p2, v6}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 45
    :cond_0
    :goto_0
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 46
    return-void

    .line 28
    :cond_1
    const-string/jumbo v6, "id"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 30
    const/4 v3, 0x0

    .line 31
    .local v3, "matchFound":Z
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/event/model/Task;

    .line 32
    .local v5, "task":Lcom/vlingo/dialog/event/model/Task;
    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 33
    const/4 v3, 0x1

    .line 34
    invoke-virtual {p0, p1, p2, v5}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V

    .line 38
    .end local v5    # "task":Lcom/vlingo/dialog/event/model/Task;
    :cond_3
    if-nez v3, :cond_0

    .line 39
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unmatched id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 42
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "matchFound":Z
    :cond_4
    invoke-virtual {p0, p1, p2, v4}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V

    goto :goto_0
.end method

.method protected deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 140
    sget-object v0, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 141
    .local v4, "slotName":Ljava/lang/String;
    invoke-interface {p1, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 142
    .local v3, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v3, :cond_0

    .line 143
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/vlingo/dialog/model/IForm;->setDisabled(Ljava/lang/Boolean;)V

    .line 140
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    .end local v3    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "slotName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "task"    # Lcom/vlingo/dialog/event/model/Task;

    .prologue
    .line 134
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 135
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 136
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 137
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 100
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    if-eqz v0, :cond_0

    .line 101
    check-cast p3, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->processTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V

    .line 107
    :goto_0
    return-void

    .line 102
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 103
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 105
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method protected processResults(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V
    .locals 19
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "results"    # Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .prologue
    .line 49
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getNumMatches()I

    move-result v7

    .line 50
    .local v7, "numMatches":I
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v16

    .line 51
    .local v16, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    if-nez v7, :cond_2

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getFullBackoff()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->anyConstraints(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 55
    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V

    .line 56
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 61
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 62
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->removeTask(Lcom/vlingo/dialog/model/IForm;)V

    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 65
    :cond_2
    const/4 v3, 0x1

    if-ne v7, v3, :cond_3

    .line 66
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/dialog/event/model/Task;

    .line 67
    .local v15, "task":Lcom/vlingo/dialog/event/model/Task;
    const-string/jumbo v3, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-virtual {v15}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 68
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v15}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V

    goto :goto_0

    .line 70
    .end local v15    # "task":Lcom/vlingo/dialog/event/model/Task;
    :cond_3
    const/4 v14, 0x1

    .line 71
    .local v14, "needChoose":Z
    const-string/jumbo v3, "choice"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v12

    .line 72
    .local v12, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v12, :cond_4

    invoke-interface {v12}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 73
    invoke-interface {v12}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v10

    .line 74
    .local v10, "choice":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 75
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 76
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v3

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6, v10}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v11

    .line 77
    .local v11, "choiceIndex":I
    if-ltz v11, :cond_4

    .line 78
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/dialog/event/model/Task;

    .line 79
    .restart local v15    # "task":Lcom/vlingo/dialog/event/model/Task;
    const-string/jumbo v3, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-virtual {v15}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 80
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v15}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V

    .line 81
    const/4 v14, 0x0

    .line 84
    .end local v10    # "choice":Ljava/lang/String;
    .end local v11    # "choiceIndex":I
    .end local v15    # "task":Lcom/vlingo/dialog/event/model/Task;
    :cond_4
    if-eqz v14, :cond_0

    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getChooseFieldId()Ljava/lang/String;

    move-result-object v13

    .line 86
    .local v13, "fieldId":Ljava/lang/String;
    if-nez v13, :cond_5

    .line 87
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v13

    .line 89
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    .line 90
    .local v18, "visibleChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    const/4 v6, 0x0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v9

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-virtual/range {v3 .. v9}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v17

    .line 91
    .local v17, "templateForm":Lcom/vlingo/dialog/model/IForm;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->getChooseTemplate()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1, v13, v4}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 93
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/TaskDeleteEditBaseTaskFormManager;->addTaskChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    goto/16 :goto_0
.end method
