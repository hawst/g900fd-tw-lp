.class public interface abstract Lcom/vlingo/dialog/manager/model/IFormManager;
.super Ljava/lang/Object;
.source "IFormManager.java"


# virtual methods
.method public abstract bumpUpEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
.end method

.method public abstract bumpUpParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
.end method

.method public abstract childClearedCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract compatibleFieldId(Ljava/lang/String;)Z
.end method

.method public abstract configure()V
.end method

.method public abstract createForm()Lcom/vlingo/dialog/model/IForm;
.end method

.method public abstract dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;
.end method

.method public abstract fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract getCancelTemplate()Ljava/lang/String;
.end method

.method public abstract getChooseFieldId()Ljava/lang/String;
.end method

.method public abstract getConfirmFieldId()Ljava/lang/String;
.end method

.method public abstract getConfirmTemplate()Ljava/lang/String;
.end method

.method public abstract getFieldId()Ljava/lang/String;
.end method

.method public abstract getFieldIdRecursive()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getParent()Lcom/vlingo/dialog/manager/model/IFormManager;
.end method

.method public abstract getPromptTemplates()Ljava/lang/String;
.end method

.method public abstract getReferencedTemplates()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSeamlessEscape()Z
.end method

.method public abstract getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;
.end method

.method public abstract getSlots()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/IFormManager;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTop()Lcom/vlingo/dialog/manager/model/IFormManager;
.end method

.method public abstract getTransferPath()Ljava/lang/String;
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public abstract hasParseType(Lcom/vlingo/dialog/DMContext;)Z
.end method

.method public abstract isComplete(Lcom/vlingo/dialog/model/IForm;)Z
.end method

.method public abstract isRequired()Z
.end method

.method public abstract maybeSendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract postDeserialize()V
.end method

.method public abstract processCancel(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
.end method

.method public abstract processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
.end method

.method public abstract processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
.end method

.method public abstract prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract removeTemporarySlots(Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract reset(Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract setComplete(ZLcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract setParent(Lcom/vlingo/dialog/manager/model/IFormManager;)V
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method

.method public abstract unprune(Lcom/vlingo/dialog/model/IForm;)V
.end method
