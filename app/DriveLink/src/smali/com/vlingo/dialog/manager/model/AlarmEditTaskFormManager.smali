.class public Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManagerBase;
.source "AlarmEditTaskFormManager.java"


# static fields
.field private static final SLOT_NEW_ENABLE:Ljava/lang/String; = "newenable"

.field private static final SLOT_NEW_TIME:Ljava/lang/String; = "newtime"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private confirmationTemplateForm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)Lcom/vlingo/dialog/model/IForm;
    .locals 2
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 77
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 78
    .local v0, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 80
    return-object v0
.end method

.method private resolveNewTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 17
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v1

    .line 18
    .local v1, "clientTime":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v0

    .line 19
    .local v0, "alarmTime":Ljava/lang/String;
    const-string/jumbo v4, "newtime"

    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .line 20
    .local v2, "newTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    const-string/jumbo v4, "newtime"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 21
    .local v3, "newTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v2, v3, v0}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->resolveAmPmNear(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 22
    invoke-static {v2, v3, v1}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->resolveAmPmAfterOrAm(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 23
    return-void
.end method


# virtual methods
.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 62
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 64
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v1

    .line 65
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .line 66
    .local v4, "results":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 67
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 68
    invoke-direct {p0, p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 72
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v1    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "results":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    goto :goto_0
.end method

.method protected foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 29
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 31
    const-string/jumbo v5, "newdays"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 32
    .local v1, "newDaysSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "newtime"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 33
    .local v4, "newTimeSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "newenable"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 34
    .local v2, "newEnableSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 38
    .local v3, "newRepeatSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->resolveNewTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    .line 40
    const/4 v0, 0x0

    .line 45
    .local v0, "anyChanges":Z
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Alarm;->getDays()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v1, v5}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->isChange(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v5

    or-int/2addr v0, v5

    .line 46
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v5

    or-int/2addr v0, v5

    .line 47
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Alarm;->getSet()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p0, v2, v5}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Boolean;)Z

    move-result v5

    or-int/2addr v0, v5

    .line 48
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Alarm;->getRepeat()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Boolean;)Z

    move-result v5

    or-int/2addr v0, v5

    .line 50
    if-eqz v0, :cond_0

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManagerBase;->foundUniqueAlarm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    .line 57
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->getNoChangesTemplate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {p1, v5, p2, v6, v7}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 54
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/vlingo/dialog/manager/model/AlarmEditTaskFormManager;->addAlarmShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    goto :goto_0
.end method
