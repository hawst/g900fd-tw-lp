.class public Lcom/vlingo/dialog/manager/model/Passthrough;
.super Lcom/vlingo/dialog/manager/model/PassthroughBase;
.source "Passthrough.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/manager/model/Passthrough$Capability;
    }
.end annotation


# static fields
.field private static final COMMA_SPLITTER:Lcom/google/common/base/Splitter;

.field private static final EQUALS_SPLITTER:Lcom/google/common/base/Splitter;

.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private capabilityList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/Passthrough$Capability;",
            ">;"
        }
    .end annotation
.end field

.field private fieldIdSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private parseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/dialog/manager/model/Passthrough;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/Passthrough;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 19
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/Passthrough;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    .line 20
    const/16 v0, 0x3d

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/Passthrough;->EQUALS_SPLITTER:Lcom/google/common/base/Splitter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/PassthroughBase;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->fieldIdSet:Ljava/util/Set;

    .line 23
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->parseTypeSet:Ljava/util/Set;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->capabilityList:Ljava/util/List;

    .line 26
    return-void
.end method


# virtual methods
.method public matches(Lcom/vlingo/dialog/DMContext;)Z
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    const/4 v5, 0x1

    .line 66
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "fieldId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "parseType":Ljava/lang/String;
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->fieldIdSet:Ljava/util/Set;

    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->parseTypeSet:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 69
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->capabilityList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 79
    :goto_0
    return v5

    .line 72
    :cond_0
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->capabilityList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/Passthrough$Capability;

    .line 73
    .local v0, "capability":Lcom/vlingo/dialog/manager/model/Passthrough$Capability;
    iget-object v6, v0, Lcom/vlingo/dialog/manager/model/Passthrough$Capability;->key:Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/vlingo/dialog/DMContext;->getCapabilityValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/vlingo/dialog/manager/model/Passthrough$Capability;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_0

    .line 79
    .end local v0    # "capability":Lcom/vlingo/dialog/manager/model/Passthrough$Capability;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "value":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 10

    .prologue
    .line 45
    sget-object v6, Lcom/vlingo/dialog/manager/model/Passthrough;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/Passthrough;->getFieldIds()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 46
    .local v2, "fieldId":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->fieldIdSet:Ljava/util/Set;

    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    .end local v2    # "fieldId":Ljava/lang/String;
    :cond_0
    sget-object v6, Lcom/vlingo/dialog/manager/model/Passthrough;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/Passthrough;->getParseTypes()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 49
    .local v5, "parseType":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->parseTypeSet:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 51
    .end local v5    # "parseType":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/Passthrough;->getCapabilities()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "capabilities":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 53
    sget-object v6, Lcom/vlingo/dialog/manager/model/Passthrough;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v6, v0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 54
    .local v1, "expression":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/dialog/manager/model/Passthrough;->EQUALS_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v6, v1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    const-class v7, Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/common/collect/Iterables;->toArray(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 55
    .local v4, "items":[Ljava/lang/String;
    array-length v6, v4

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 56
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/Passthrough;->capabilityList:Ljava/util/List;

    new-instance v7, Lcom/vlingo/dialog/manager/model/Passthrough$Capability;

    const/4 v8, 0x0

    aget-object v8, v4, v8

    const/4 v9, 0x1

    aget-object v9, v4, v9

    invoke-direct {v7, v8, v9}, Lcom/vlingo/dialog/manager/model/Passthrough$Capability;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 58
    :cond_2
    sget-object v6, Lcom/vlingo/dialog/manager/model/Passthrough;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "bad Passthrough capability expression \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\' -- ignoring"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_2

    .line 63
    .end local v1    # "expression":Ljava/lang/String;
    .end local v4    # "items":[Ljava/lang/String;
    :cond_3
    return-void
.end method
