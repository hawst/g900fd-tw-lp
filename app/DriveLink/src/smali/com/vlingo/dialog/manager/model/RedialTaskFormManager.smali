.class public Lcom/vlingo/dialog/manager/model/RedialTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/RedialTaskFormManagerBase;
.source "RedialTaskFormManager.java"


# instance fields
.field private final SLOT_RECIPIENT:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/RedialTaskFormManagerBase;-><init>()V

    .line 8
    const-string/jumbo v0, "recipient"

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/RedialTaskFormManager;->SLOT_RECIPIENT:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 12
    const-string/jumbo v0, "recipient"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 16
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/RedialTaskFormManager;->getNoneTemplate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/RedialTaskFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v1, v2}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 17
    invoke-virtual {p1, p0, p2}, Lcom/vlingo/dialog/DMContext;->resetToTop(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V

    .line 21
    :goto_0
    return-void

    .line 19
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/RedialTaskFormManagerBase;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method
