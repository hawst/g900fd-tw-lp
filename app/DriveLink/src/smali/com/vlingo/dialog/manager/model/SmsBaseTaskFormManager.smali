.class public Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManagerBase;
.source "SmsBaseTaskFormManager.java"


# static fields
.field public static final OP_ADD:Ljava/lang/String; = "add"

.field public static final OP_REPLACE:Ljava/lang/String; = "replace"

.field public static final PT_SET_ADD:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SLOT_CHOICE:Ljava/lang/String; = "choice"

.field private static final SLOT_ID:Ljava/lang/String; = "id"

.field public static final SLOT_MESSAGE:Ljava/lang/String; = "message"

.field public static final SLOT_OP:Ljava/lang/String; = "op"

.field private static final SLOT_SENDER:Ljava/lang/String; = "sender"

.field public static final SLOT_TEXT:Ljava/lang/String; = "text"

.field public static final TAG_CONTACT:Ljava/lang/String; = "contact"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "sms:add"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "cmd:add"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "sms:contactadd"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sms:msgappend"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->PT_SET_ADD:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private addMessageQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 46
    const-string/jumbo v3, "choice"

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "which":Ljava/lang/String;
    new-instance v1, Lcom/vlingo/dialog/goal/model/MessageQueryGoal;

    invoke-direct {v1}, Lcom/vlingo/dialog/goal/model/MessageQueryGoal;-><init>()V

    .line 48
    .local v1, "goal":Lcom/vlingo/dialog/goal/model/MessageQueryGoal;
    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/goal/model/MessageQueryGoal;->setWhich(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    .line 50
    .local v0, "clearCache":Z
    invoke-virtual {p1, v1, v0}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 51
    return-void
.end method

.method protected static resolveAddReplace(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "addTemplate"    # Ljava/lang/String;
    .param p3, "messageFieldId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v10, 0x0

    .line 140
    const-string/jumbo v8, "message"

    invoke-interface {p1, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 141
    .local v1, "messageSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v8, "text"

    invoke-interface {p1, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 142
    .local v5, "textSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v8, "op"

    invoke-interface {p1, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 144
    .local v3, "opSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_1

    if-eqz v5, :cond_1

    if-eqz v3, :cond_1

    .line 146
    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 147
    .local v4, "text":Ljava/lang/String;
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "op":Ljava/lang/String;
    const-string/jumbo v8, "add"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 151
    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 152
    if-eqz p2, :cond_1

    .line 153
    invoke-virtual {p0, p2, p1, p3, v7}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 185
    .end local v2    # "op":Ljava/lang/String;
    .end local v4    # "text":Ljava/lang/String;
    :goto_0
    return v6

    .line 157
    .restart local v2    # "op":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    :cond_0
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    const-string/jumbo v6, " "

    aput-object v6, v8, v7

    const/4 v6, 0x2

    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v8}, Lcom/vlingo/dialog/util/StringUtil;->concatenate([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "message":Ljava/lang/String;
    invoke-interface {v1, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 159
    invoke-interface {v5, v10}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 160
    invoke-interface {v3, v10}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .end local v0    # "message":Ljava/lang/String;
    .end local v2    # "op":Ljava/lang/String;
    .end local v4    # "text":Ljava/lang/String;
    :cond_1
    :goto_1
    move v6, v7

    .line 185
    goto :goto_0

    .line 163
    .restart local v2    # "op":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    :cond_2
    const-string/jumbo v6, "replace"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 165
    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 166
    invoke-interface {v1, v10}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_1

    .line 169
    :cond_3
    invoke-interface {v1, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 170
    invoke-interface {v5, v10}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 171
    invoke-interface {v3, v10}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_1

    .line 176
    :cond_4
    if-eqz v4, :cond_1

    .line 177
    invoke-interface {v1, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 178
    invoke-interface {v5, v10}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setSender(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "phone"    # Ljava/lang/String;

    .prologue
    .line 114
    const-string/jumbo v1, "sender"

    invoke-interface {p1, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 115
    .local v0, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 116
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 117
    invoke-interface {v0, p2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 119
    invoke-interface {v0, p3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected processActionCallEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionCallEvent;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionCallEvent;

    .prologue
    .line 69
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionCallEvent;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionCallEvent;->getSenderName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionCallEvent;->getSenderPhone()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->setSender(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionCallEvent;->getFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->storeRestoreFieldId(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processEventContinue(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 73
    return-void
.end method

.method protected processActionForwardEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionForwardEvent;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionForwardEvent;

    .prologue
    .line 76
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionForwardEvent;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionForwardEvent;->getSenderName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionForwardEvent;->getSenderPhone()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->setSender(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionForwardEvent;->getFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->storeRestoreFieldId(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processEventContinue(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 80
    return-void
.end method

.method protected processActionReplyEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionReplyEvent;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionReplyEvent;

    .prologue
    .line 83
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionReplyEvent;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionReplyEvent;->getSenderName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionReplyEvent;->getSenderPhone()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->setSender(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionReplyEvent;->getFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->storeRestoreFieldId(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processEventContinue(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 87
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 55
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/MessageResolvedEvent;

    if-eqz v0, :cond_0

    .line 56
    check-cast p3, Lcom/vlingo/dialog/event/model/MessageResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processMessageResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/MessageResolvedEvent;)V

    .line 66
    :goto_0
    return-void

    .line 57
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionCallEvent;

    if-eqz v0, :cond_1

    .line 58
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionCallEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processActionCallEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionCallEvent;)V

    goto :goto_0

    .line 59
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionForwardEvent;

    if-eqz v0, :cond_2

    .line 60
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionForwardEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processActionForwardEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionForwardEvent;)V

    goto :goto_0

    .line 61
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_2
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ActionReplyEvent;

    if-eqz v0, :cond_3

    .line 62
    check-cast p3, Lcom/vlingo/dialog/event/model/ActionReplyEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processActionReplyEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionReplyEvent;)V

    goto :goto_0

    .line 64
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method protected processEventContinue(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 125
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->firstRequiredIncompleteSlot(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    .line 130
    .local v0, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-eqz v0, :cond_1

    .line 131
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method

.method protected processMessageResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/MessageResolvedEvent;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/MessageResolvedEvent;

    .prologue
    const/4 v4, 0x0

    .line 90
    const-class v3, Lcom/vlingo/dialog/goal/model/MessageQueryGoal;

    invoke-virtual {p1, p3, v3}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 91
    invoke-virtual {p1, v4}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 92
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 94
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/MessageResolvedEvent;->getNumMatches()I

    move-result v3

    if-nez v3, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "template":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3, v4}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 98
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->exitSetFieldId(Lcom/vlingo/dialog/DMContext;)V

    .line 99
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 100
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearQueries()V

    .line 111
    .end local v2    # "template":Ljava/lang/String;
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/MessageResolvedEvent;->getMessages()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/Message;

    .line 105
    .local v1, "message":Lcom/vlingo/dialog/event/model/Message;
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Message;->getId()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "id":Ljava/lang/String;
    const-string/jumbo v3, "id"

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Message;->getSenderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Message;->getSenderPhone()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p2, v3, v4}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->setSender(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->processEventContinue(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 38
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->addMessageQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 40
    const/4 v0, 0x0

    .line 42
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManagerBase;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    goto :goto_0
.end method
