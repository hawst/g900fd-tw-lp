.class Lcom/vlingo/dialog/manager/model/ContactFormManager$1;
.super Ljava/lang/Object;
.source "ContactFormManager.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/dialog/manager/model/ContactFormManager;->processContactResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/vlingo/dialog/event/model/Address;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/dialog/manager/model/ContactFormManager;


# direct methods
.method constructor <init>(Lcom/vlingo/dialog/manager/model/ContactFormManager;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/ContactFormManager$1;->this$0:Lcom/vlingo/dialog/manager/model/ContactFormManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/vlingo/dialog/event/model/Address;)Z
    .locals 3
    .param p1, "address"    # Lcom/vlingo/dialog/event/model/Address;

    .prologue
    .line 161
    # invokes: Lcom/vlingo/dialog/manager/model/ContactFormManager;->hasInvalidType(Lcom/vlingo/dialog/event/model/Address;)Z
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->access$000(Lcom/vlingo/dialog/event/model/Address;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    sget-object v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    sget-object v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removing invalid type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 165
    :cond_0
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 159
    check-cast p1, Lcom/vlingo/dialog/event/model/Address;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager$1;->apply(Lcom/vlingo/dialog/event/model/Address;)Z

    move-result v0

    return v0
.end method
