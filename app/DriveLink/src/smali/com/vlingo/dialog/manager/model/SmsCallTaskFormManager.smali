.class public Lcom/vlingo/dialog/manager/model/SmsCallTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/SmsCallTaskFormManagerBase;
.source "SmsCallTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/SmsCallTaskFormManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 12
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v1

    .line 13
    .local v1, "parse":Lcom/vlingo/dialog/util/Parse;
    const-string/jumbo v3, "contact:cf"

    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 14
    .local v0, "contact":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 15
    const-string/jumbo v3, "contact"

    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    :cond_0
    const-string/jumbo v3, "pn"

    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 18
    .local v2, "pn":Ljava/lang/String;
    if-nez v0, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    const-string/jumbo v3, "anaphora"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 20
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsCallTaskFormManagerBase;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 24
    :goto_0
    return-void

    .line 22
    :cond_3
    const-string/jumbo v3, "call"

    invoke-virtual {p0, p1, p2, v3}, Lcom/vlingo/dialog/manager/model/SmsCallTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    goto :goto_0
.end method
