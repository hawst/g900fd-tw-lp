.class public Lcom/vlingo/dialog/manager/model/ContactFormManager;
.super Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;
.source "ContactFormManager.java"


# static fields
.field public static final CONTACT_ANNOTATE_SEPARATOR:Ljava/lang/String; = "_id_"

.field private static final DISAMBIG_CONTACT:Ljava/lang/String; = "contact"

.field private static final DISAMBIG_TYPE:Ljava/lang/String; = "type"

.field private static final EDIT_DISTANCE:Lcom/vlingo/dialog/util/EditDistance;

.field private static final MATCH_THRESHOLD:F = 0.2f

.field private static final NAME_ANAPHORA:Ljava/lang/String; = "ANAPHORA"

.field private static final NO_CONTACT:Ljava/lang/String; = "NO_CONTACT"

.field public static final REDIAL:Ljava/lang/String; = "REDIAL"

.field private static final SLOT_ADDRESS:Ljava/lang/String; = "address"

.field private static final SLOT_CANDIDATES:Ljava/lang/String; = "candidates"

.field private static final SLOT_CAPABILITY:Ljava/lang/String; = "capability"

.field private static final SLOT_CHOICE:Ljava/lang/String; = "choice"

.field public static final SLOT_CONFIDENT:Ljava/lang/String; = "confident"

.field private static final SLOT_CONTACT_ID:Ljava/lang/String; = "contactId"

.field private static final SLOT_NAME:Ljava/lang/String; = "name"

.field private static final SLOT_TYPE:Ljava/lang/String; = "type"

.field private static final SLOT_TYPE_ID:Ljava/lang/String; = "typeId"

.field private static final TYPE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

.field private static final VALID_TYPE_PATTERN:Ljava/util/regex/Pattern;

.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field protected anaphoraFieldIdSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 42
    const-class v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 65
    new-instance v0, Lcom/vlingo/dialog/util/EditDistance;

    new-instance v1, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v3, v3, v2}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;-><init>(IIII)V

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/util/EditDistance;-><init>(Lcom/vlingo/dialog/util/EditDistance$Cost;)V

    sput-object v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->EDIT_DISTANCE:Lcom/vlingo/dialog/util/EditDistance;

    .line 72
    const-string/jumbo v0, "home|mobile|work|other"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->VALID_TYPE_PATTERN:Ljava/util/regex/Pattern;

    .line 584
    const-string/jumbo v0, "^([^0-9]+)([0-9]+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->TYPE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;-><init>()V

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->anaphoraFieldIdSet:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/dialog/event/model/Address;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/dialog/event/model/Address;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->hasInvalidType(Lcom/vlingo/dialog/event/model/Address;)Z

    move-result v0

    return v0
.end method

.method private addCandidatesQueryGoal(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/goal/model/ContactQueryGoal;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "candidates"    # Ljava/lang/String;
    .param p3, "capability"    # Ljava/lang/String;

    .prologue
    .line 772
    new-instance v0, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;-><init>()V

    .line 773
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/ContactQueryGoal;
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setAsynchronous(Ljava/lang/Boolean;)V

    .line 774
    invoke-virtual {v0, p2}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setName(Ljava/lang/String;)V

    .line 775
    invoke-virtual {v0, p3}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setCapability(Ljava/lang/String;)V

    .line 777
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getContactLimit()Ljava/lang/Integer;

    move-result-object v1

    .line 778
    .local v1, "limit":Ljava/lang/Integer;
    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setLimit(Ljava/lang/Integer;)V

    .line 780
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getContactClearCache()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 782
    return-object v0
.end method

.method private static addContactNameChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 790
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/ContactNameChooseGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/ContactNameChooseGoal;-><init>()V

    .line 791
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/ContactNameChooseGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/ContactNameChooseGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 792
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 793
    return-void
.end method

.method private addContactQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/goal/model/ContactQueryGoal;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "capability"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;

    .prologue
    .line 756
    new-instance v0, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;-><init>()V

    .line 757
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/ContactQueryGoal;
    invoke-virtual {v0, p3}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setName(Ljava/lang/String;)V

    .line 758
    invoke-virtual {v0, p5}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setType(Ljava/lang/String;)V

    .line 759
    invoke-virtual {v0, p4}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setCapability(Ljava/lang/String;)V

    .line 761
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getContactLimit()Ljava/lang/Integer;

    move-result-object v1

    .line 762
    .local v1, "limit":Ljava/lang/Integer;
    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;->setLimit(Ljava/lang/Integer;)V

    .line 764
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getContactClearCache()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 766
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 768
    return-object v0
.end method

.method private static addContactTypeChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 796
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/ContactTypeChooseGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/ContactTypeChooseGoal;-><init>()V

    .line 797
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/ContactTypeChooseGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/ContactTypeChooseGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 798
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 799
    return-void
.end method

.method private static addressFromId(Lcom/vlingo/dialog/event/model/Contact;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Address;
    .locals 3
    .param p0, "contact"    # Lcom/vlingo/dialog/event/model/Contact;
    .param p1, "typeId"    # Ljava/lang/String;

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/vlingo/dialog/event/model/Contact;->getAddresses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Address;

    .line 421
    .local v0, "address":Lcom/vlingo/dialog/event/model/Address;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 425
    .end local v0    # "address":Lcom/vlingo/dialog/event/model/Address;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static annotateContact(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 614
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "contactId"

    invoke-interface {p1, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static annotateContact(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 618
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 619
    :cond_0
    const/4 p1, 0x0

    .line 623
    .end local p1    # "name":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 620
    .restart local p1    # "name":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getAnnotateContacts()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 621
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_id_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static buildNamePipeList(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 628
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 629
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/Contact;

    .line 630
    .local v1, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 631
    .local v0, "annotatedName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 632
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 635
    .end local v0    # "annotatedName":Ljava/lang/String;
    .end local v1    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    :cond_1
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->pipeJoin(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static buildSubtypesPipeList(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 639
    .local p0, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 640
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Address;

    .line 641
    .local v0, "address":Lcom/vlingo/dialog/event/model/Address;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 643
    .end local v0    # "address":Lcom/vlingo/dialog/event/model/Address;
    :cond_0
    invoke-static {v2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->pipeJoin(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private clearCandidates(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v2, 0x0

    .line 119
    const-string/jumbo v1, "candidates"

    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 120
    .local v0, "candidatesSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 121
    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->setCandidates(Lcom/vlingo/dialog/state/model/CompletedQuery;)V

    .line 124
    :cond_0
    return-void
.end method

.method private static contactFromId(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Contact;
    .locals 3
    .param p0, "cre"    # Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    .param p1, "contactId"    # Ljava/lang/String;

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Contact;

    .line 412
    .local v0, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 416
    .end local v0    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static equivalent(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 5
    .param p0, "x"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "y"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 742
    const-string/jumbo v4, "typeId"

    invoke-interface {p0, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 743
    .local v1, "xId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 744
    const-string/jumbo v4, "typeId"

    invoke-interface {p1, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 745
    .local v3, "yId":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 752
    .end local v3    # "yId":Ljava/lang/String;
    :goto_0
    return v4

    .line 747
    :cond_0
    const-string/jumbo v4, "address"

    invoke-interface {p0, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 748
    .local v0, "xAddress":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 749
    const-string/jumbo v4, "address"

    invoke-interface {p1, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 750
    .local v2, "yAddress":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_0

    .line 752
    .end local v2    # "yAddress":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static fetchContactResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 786
    const-class v0, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->fetchQueryEvent(Ljava/lang/Class;)Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    return-object v0
.end method

.method private static filterAddressesByType(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587
    .local p0, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    if-nez p1, :cond_1

    move-object v4, p0

    .line 610
    :cond_0
    :goto_0
    return-object v4

    .line 590
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 591
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    sget-object v5, Lcom/vlingo/dialog/manager/model/ContactFormManager;->TYPE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 592
    .local v3, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 593
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    .line 594
    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 595
    .local v2, "index":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Address;

    .line 596
    .local v0, "address":Lcom/vlingo/dialog/event/model/Address;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 597
    add-int/lit8 v2, v2, -0x1

    if-nez v2, :cond_2

    .line 598
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 603
    .end local v0    # "address":Lcom/vlingo/dialog/event/model/Address;
    :cond_3
    sget-object v5, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v5}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v5, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "didn\'t find type for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " -- backing off to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 605
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "index":I
    :cond_4
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Address;

    .line 606
    .restart local v0    # "address":Lcom/vlingo/dialog/event/model/Address;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 607
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static hasInvalidType(Lcom/vlingo/dialog/event/model/Address;)Z
    .locals 2
    .param p0, "address"    # Lcom/vlingo/dialog/event/model/Address;

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "type":Ljava/lang/String;
    if-eqz v0, :cond_0

    sget-object v1, Lcom/vlingo/dialog/manager/model/ContactFormManager;->VALID_TYPE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private markConfident(Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 806
    const-string/jumbo v1, "confident"

    invoke-interface {p1, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 807
    .local v0, "confidentSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 808
    const-string/jumbo v1, "true"

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 810
    :cond_0
    return-void
.end method

.method private static matchName(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p0, "cre"    # Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    .param p1, "oldName"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/event/model/ContactResolvedEvent;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x20

    .line 430
    if-nez p0, :cond_1

    .line 431
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 451
    :cond_0
    return-object v6

    .line 434
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v0

    .line 436
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    const/4 v7, 0x3

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v5, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    .line 438
    .local v5, "refs":[Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    new-array v1, v7, [Ljava/lang/String;

    .line 439
    .local v1, "hyps":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 440
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/dialog/event/model/Contact;

    invoke-virtual {v7}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v2

    .line 439
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 443
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 444
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    sget-object v7, Lcom/vlingo/dialog/manager/model/ContactFormManager;->EDIT_DISTANCE:Lcom/vlingo/dialog/util/EditDistance;

    invoke-virtual {v7, v5, v1}, Lcom/vlingo/dialog/util/EditDistance;->bestMatches([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/util/EditDistance$Match;

    .line 445
    .local v4, "match":Lcom/vlingo/dialog/util/EditDistance$Match;
    iget v7, v4, Lcom/vlingo/dialog/util/EditDistance$Match;->normalizedDistance:F

    const v8, 0x3e4ccccd    # 0.2f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_3

    .line 446
    sget-object v7, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v7, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "match: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v4, Lcom/vlingo/dialog/util/EditDistance$Match;->refIndex:I

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v4, Lcom/vlingo/dialog/util/EditDistance$Match;->hypIndex:I

    aget-object v9, v1, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v4, Lcom/vlingo/dialog/util/EditDistance$Match;->normalizedDistance:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 447
    :cond_4
    iget v7, v4, Lcom/vlingo/dialog/util/EditDistance$Match;->hypIndex:I

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static pipeJoin(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p0, "strings":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/16 v4, 0x7c

    .line 647
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 648
    const-string/jumbo v3, ""

    .line 655
    :goto_0
    return-object v3

    .line 650
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 651
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 652
    .local v2, "string":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 653
    const/16 v3, 0x20

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 655
    .end local v2    # "string":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 227
    invoke-virtual {p1, v10}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 228
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->fetchContactResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    move-result-object v3

    .line 229
    .local v3, "cre":Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v4

    .line 230
    .local v4, "id":Ljava/lang/String;
    const-string/jumbo v7, "\\."

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 231
    .local v5, "idParts":[Ljava/lang/String;
    array-length v7, v5

    if-ne v7, v9, :cond_2

    .line 233
    move-object v2, v4

    .line 234
    .local v2, "contactId":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->contactFromId(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Contact;

    move-result-object v1

    .line 235
    .local v1, "contact":Lcom/vlingo/dialog/event/model/Contact;
    if-nez v1, :cond_0

    .line 236
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unmatched contact id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 238
    :cond_0
    invoke-direct {p0, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->markConfident(Lcom/vlingo/dialog/model/IForm;)V

    .line 239
    const-string/jumbo v7, "name"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 240
    const-string/jumbo v7, "contactId"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-interface {v7, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 241
    invoke-direct {p0, p1, p2, v3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->resolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 243
    invoke-static {p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 244
    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 245
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v7

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-interface {v7, p1, v8, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 271
    :cond_1
    :goto_0
    return-void

    .line 247
    .end local v1    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    .end local v2    # "contactId":Ljava/lang/String;
    :cond_2
    array-length v7, v5

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    .line 249
    aget-object v2, v5, v10

    .line 250
    .restart local v2    # "contactId":Ljava/lang/String;
    move-object v6, v4

    .line 251
    .local v6, "typeId":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->contactFromId(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Contact;

    move-result-object v1

    .line 252
    .restart local v1    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    if-nez v1, :cond_3

    .line 253
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unmatched contact id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 255
    :cond_3
    invoke-static {v1, v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->addressFromId(Lcom/vlingo/dialog/event/model/Contact;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Address;

    move-result-object v0

    .line 256
    .local v0, "address":Lcom/vlingo/dialog/event/model/Address;
    if-nez v0, :cond_4

    .line 257
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unmatched type id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 259
    :cond_4
    invoke-direct {p0, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->markConfident(Lcom/vlingo/dialog/model/IForm;)V

    .line 260
    const-string/jumbo v7, "contactId"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-interface {v7, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 261
    const-string/jumbo v7, "type"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 262
    const-string/jumbo v7, "typeId"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 263
    const-string/jumbo v7, "address"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getDetail()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 264
    invoke-static {p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 265
    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 266
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v7

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-interface {v7, p1, v8, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 269
    .end local v0    # "address":Lcom/vlingo/dialog/event/model/Address;
    .end local v1    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    .end local v2    # "contactId":Ljava/lang/String;
    .end local v6    # "typeId":Ljava/lang/String;
    :cond_5
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "bad choice selected uid: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method private processContactResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)V
    .locals 14
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    .prologue
    .line 143
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 146
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/Contact;

    .line 147
    .local v4, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Contact;->getAddresses()Ljava/util/List;

    move-result-object v2

    .line 148
    .local v2, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 150
    const/4 v11, 0x0

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/Address;

    .line 151
    .local v1, "address":Lcom/vlingo/dialog/event/model/Address;
    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->hasInvalidType(Lcom/vlingo/dialog/event/model/Address;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 152
    sget-object v11, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v11}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 153
    sget-object v11, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "mapping invalid type to other: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 155
    :cond_1
    const-string/jumbo v11, "other"

    invoke-virtual {v1, v11}, Lcom/vlingo/dialog/event/model/Address;->setType(Ljava/lang/String;)V

    goto :goto_0

    .line 159
    .end local v1    # "address":Lcom/vlingo/dialog/event/model/Address;
    :cond_2
    new-instance v11, Lcom/vlingo/dialog/manager/model/ContactFormManager$1;

    invoke-direct {v11, p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager$1;-><init>(Lcom/vlingo/dialog/manager/model/ContactFormManager;)V

    invoke-static {v2, v11}, Lcom/google/common/collect/Iterables;->removeIf(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Z

    goto :goto_0

    .line 174
    .end local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    .end local v4    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v7

    .line 175
    .local v7, "nContactsBefore":I
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v11

    new-instance v12, Lcom/vlingo/dialog/manager/model/ContactFormManager$2;

    invoke-direct {v12, p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager$2;-><init>(Lcom/vlingo/dialog/manager/model/ContactFormManager;)V

    invoke-static {v11, v12}, Lcom/google/common/collect/Iterables;->removeIf(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Z

    .line 187
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v6

    .line 188
    .local v6, "nContactsAfter":I
    sub-int v8, v7, v6

    .line 189
    .local v8, "nRemoved":I
    if-lez v8, :cond_4

    .line 190
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getNumMatches()I

    move-result v11

    sub-int/2addr v11, v8

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->setNumMatches(I)V

    .line 203
    :cond_4
    const-class v11, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    move-object/from16 v0, p3

    invoke-virtual {p1, v0, v11}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v3

    .line 204
    .local v3, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v9

    .line 205
    .local v9, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v9}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getAsynchronous()Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 206
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v11

    invoke-virtual {v11, v3}, Lcom/vlingo/dialog/state/model/State;->setCandidates(Lcom/vlingo/dialog/state/model/CompletedQuery;)V

    .line 224
    :cond_5
    :goto_1
    return-void

    .line 208
    :cond_6
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_7

    const-string/jumbo v11, "REDIAL"

    const-string/jumbo v12, "name"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 210
    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 211
    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 212
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v11

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-interface {v11, p1, v12, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1

    .line 213
    :cond_7
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_8

    const-string/jumbo v11, "ANAPHORA"

    const-string/jumbo v12, "name"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 215
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTop()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v10

    .line 216
    .local v10, "topManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getNoAnaphoraTemplate()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v11, v0, v12, v13}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 217
    move-object/from16 v0, p2

    invoke-virtual {p1, p0, v0}, Lcom/vlingo/dialog/DMContext;->resetToTop(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1

    .line 218
    .end local v10    # "topManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_8
    invoke-direct/range {p0 .. p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->resolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 220
    invoke-static/range {p1 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 221
    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 222
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v11

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-interface {v11, p1, v12, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_1
.end method

.method private resolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Z
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "cre"    # Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 456
    const-string/jumbo v4, "contactId"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "contactId":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 458
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->resolveName(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Ljava/lang/String;

    move-result-object v0

    .line 459
    const-string/jumbo v4, "NO_CONTACT"

    if-ne v0, v4, :cond_2

    .line 460
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getRequired()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 462
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 483
    :cond_0
    :goto_0
    return v2

    .line 466
    :cond_1
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    move v2, v3

    .line 467
    goto :goto_0

    .line 470
    :cond_2
    if-eqz v0, :cond_0

    .line 474
    :cond_3
    const-string/jumbo v4, "typeId"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 475
    .local v1, "typeId":Ljava/lang/String;
    if-nez v1, :cond_4

    .line 476
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->resolveType(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 477
    if-eqz v1, :cond_0

    .line 481
    :cond_4
    invoke-static {p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 482
    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    move v2, v3

    .line 483
    goto :goto_0
.end method

.method private resolveName(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Ljava/lang/String;
    .locals 13
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "cre"    # Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    .prologue
    .line 487
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getNumMatches()I

    move-result v4

    .line 488
    .local v4, "totalMatches":I
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v10

    .line 489
    .local v10, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    .line 490
    .local v5, "numMatches":I
    if-nez v5, :cond_1

    .line 491
    const-string/jumbo v0, "name"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 492
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 493
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 494
    const/4 v7, 0x0

    .line 516
    :goto_0
    return-object v7

    .line 496
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getNoContactFoundTemplate()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 497
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 498
    const-string/jumbo v7, "NO_CONTACT"

    goto :goto_0

    .line 500
    :cond_1
    const/4 v0, 0x1

    if-ne v5, v0, :cond_2

    .line 501
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/dialog/event/model/Contact;

    .line 502
    .local v9, "match":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v7

    .line 503
    .local v7, "contactId":Ljava/lang/String;
    const-string/jumbo v0, "name"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 504
    const-string/jumbo v0, "contactId"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 505
    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/Contact;->getScore()Ljava/lang/Float;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setConfidence(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Float;)V

    goto :goto_0

    .line 508
    .end local v7    # "contactId":Ljava/lang/String;
    .end local v9    # "match":Lcom/vlingo/dialog/event/model/Contact;
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setListPosition(Lcom/vlingo/dialog/manager/model/ListPosition;)V

    .line 509
    invoke-virtual {p0, p1, v10}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 510
    .local v12, "visibleMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    invoke-static {p1, v12}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->buildNamePipeList(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v11

    .line 511
    .local v11, "templateForm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getChooseFieldId()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 512
    .local v8, "fieldId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getChooseContactTemplate()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v11, v8, v1}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 513
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 514
    invoke-static {p1, v10}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->addContactNameChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 515
    const-string/jumbo v0, "contact"

    invoke-static {p1, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setDisambig(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 516
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method private resolveType(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "cre"    # Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    .param p4, "contactId"    # Ljava/lang/String;

    .prologue
    .line 521
    invoke-static/range {p3 .. p4}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->contactFromId(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Contact;

    move-result-object v11

    .line 522
    .local v11, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v11}, Lcom/vlingo/dialog/event/model/Contact;->getAddresses()Ljava/util/List;

    move-result-object v10

    .line 523
    .local v10, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 526
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getNoTypeFoundTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    const-string/jumbo v4, "type"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 527
    invoke-virtual/range {p1 .. p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 528
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 529
    const/4 v15, 0x0

    .line 580
    :goto_0
    return-object v15

    .line 532
    :cond_0
    const-string/jumbo v2, "type"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v17

    .line 533
    .local v17, "typeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface/range {v17 .. v17}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v18

    .line 535
    .local v18, "typeValue":Ljava/lang/String;
    if-nez v18, :cond_4

    move-object v14, v10

    .line 539
    .local v14, "typeAddresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    :goto_1
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 542
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 543
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 544
    invoke-interface {v14, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 547
    :cond_1
    const/4 v9, 0x0

    .line 549
    .local v9, "address":Lcom/vlingo/dialog/event/model/Address;
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 550
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "address":Lcom/vlingo/dialog/event/model/Address;
    check-cast v9, Lcom/vlingo/dialog/event/model/Address;

    .line 556
    .restart local v9    # "address":Lcom/vlingo/dialog/event/model/Address;
    :cond_2
    :goto_2
    if-nez v9, :cond_6

    .line 558
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v6

    .line 560
    .local v6, "numTypeAddresses":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTypeLimit()Ljava/lang/Integer;

    move-result-object v16

    .line 561
    .local v16, "typeLimit":Ljava/lang/Integer;
    if-eqz v16, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v6, :cond_3

    .line 562
    const/4 v2, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v14, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v14

    .line 565
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/DMContext;->setListPosition(Lcom/vlingo/dialog/manager/model/ListPosition;)V

    .line 566
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v19

    .line 567
    .local v19, "visibleMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    invoke-static/range {v19 .. v19}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->buildSubtypesPipeList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v8

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move v7, v6

    invoke-virtual/range {v2 .. v8}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v13

    .line 568
    .local v13, "templateForm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v2, "type"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getChooseFieldId()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "type"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 569
    .local v12, "fieldId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getChooseTypeTemplate()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v12, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 570
    invoke-virtual/range {p1 .. p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 571
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->addContactTypeChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 572
    const-string/jumbo v2, "type"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setDisambig(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 573
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 535
    .end local v6    # "numTypeAddresses":I
    .end local v9    # "address":Lcom/vlingo/dialog/event/model/Address;
    .end local v12    # "fieldId":Ljava/lang/String;
    .end local v13    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    .end local v14    # "typeAddresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    .end local v16    # "typeLimit":Ljava/lang/Integer;
    .end local v19    # "visibleMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    :cond_4
    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->filterAddressesByType(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    goto/16 :goto_1

    .line 551
    .restart local v9    # "address":Lcom/vlingo/dialog/event/model/Address;
    .restart local v14    # "typeAddresses":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    :cond_5
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 553
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "address":Lcom/vlingo/dialog/event/model/Address;
    check-cast v9, Lcom/vlingo/dialog/event/model/Address;

    .restart local v9    # "address":Lcom/vlingo/dialog/event/model/Address;
    goto/16 :goto_2

    .line 576
    :cond_6
    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/Address;->getId()Ljava/lang/String;

    move-result-object v15

    .line 577
    .local v15, "typeId":Ljava/lang/String;
    const-string/jumbo v2, "type"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 578
    const-string/jumbo v2, "typeId"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-interface {v2, v15}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 579
    const-string/jumbo v2, "address"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/Address;->getDetail()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setConfidence(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Float;)V
    .locals 4
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "score"    # Ljava/lang/Float;

    .prologue
    .line 813
    const-string/jumbo v2, "confident"

    invoke-interface {p1, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 814
    .local v1, "confidentSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_1

    .line 815
    const/4 v0, 0x0

    .line 816
    .local v0, "confident":Z
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getConfidenceThreshold()Ljava/lang/Float;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 817
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getConfidenceThreshold()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    const/4 v0, 0x1

    .line 819
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 821
    .end local v0    # "confident":Z
    :cond_1
    return-void

    .line 817
    .restart local v0    # "confident":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private withinCompleteList(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 824
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    instance-of v0, v0, Lcom/vlingo/dialog/manager/model/ContactListFormManager;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 13
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 85
    iget-object v8, p0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->anaphoraFieldIdSet:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 86
    const-string/jumbo v8, "name"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    const-string/jumbo v9, "ANAPHORA"

    invoke-interface {v8, v9}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 89
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 91
    const-string/jumbo v8, "address"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 92
    .local v0, "addressSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 93
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 94
    invoke-interface {p2, v11}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 95
    invoke-direct {p0, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->markConfident(Lcom/vlingo/dialog/model/IForm;)V

    .line 97
    const/4 v8, 0x5

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v8, "name"

    aput-object v8, v1, v10

    const-string/jumbo v8, "type"

    aput-object v8, v1, v11

    const/4 v8, 0x2

    const-string/jumbo v9, "contactId"

    aput-object v9, v1, v8

    const/4 v8, 0x3

    const-string/jumbo v9, "typeId"

    aput-object v9, v1, v8

    const/4 v8, 0x4

    const-string/jumbo v9, "candidates"

    aput-object v9, v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_5

    aget-object v6, v1, v2

    .line 98
    .local v6, "slotName":Ljava/lang/String;
    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 99
    .local v5, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v5, :cond_1

    .line 100
    invoke-interface {v5, v12}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 101
    invoke-interface {v5, v10}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 97
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v6    # "slotName":Ljava/lang/String;
    :cond_2
    const-string/jumbo v8, "name"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 106
    .local v4, "nameSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v8, "type"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 107
    .local v7, "typeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 108
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->clearCandidates(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 110
    :cond_3
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 111
    :cond_4
    invoke-interface {p2, v12}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 112
    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 113
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v8

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v8, p1, v9, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childClearedCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 116
    .end local v4    # "nameSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v7    # "typeSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_5
    return-void
.end method

.method public isConfident(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getConfidenceThreshold()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "confident"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManagerBase;->postDeserialize()V

    .line 78
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/ContactFormManager;->anaphoraFieldIdSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getAnaphoraFieldIds()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 127
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    if-eqz v0, :cond_0

    .line 128
    check-cast p3, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->processContactResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)V

    .line 135
    :goto_0
    return-void

    .line 129
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 130
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 133
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-interface {v0, p1, v1, p3}, Lcom/vlingo/dialog/manager/model/IFormManager;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 36
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 275
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->processCancel(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v31

    if-eqz v31, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->processListScroll(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v31

    if-nez v31, :cond_0

    .line 283
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getDisambig(Lcom/vlingo/dialog/DMContext;)Ljava/lang/String;

    move-result-object v15

    .line 284
    .local v15, "disambig":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->clearDisambig(Lcom/vlingo/dialog/DMContext;)V

    .line 286
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->hasParseType(Lcom/vlingo/dialog/DMContext;)Z

    move-result v31

    if-nez v31, :cond_2

    .line 287
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v31

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    move-object/from16 v3, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->bumpUpParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z

    goto :goto_0

    .line 291
    :cond_2
    const-string/jumbo v31, "name"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v22

    .line 292
    .local v22, "nameSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface/range {v22 .. v22}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v25

    .line 293
    .local v25, "oldName":Ljava/lang/String;
    const-string/jumbo v31, "type"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v30

    .line 294
    .local v30, "typeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface/range {v30 .. v30}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v26

    .line 295
    .local v26, "oldType":Ljava/lang/String;
    const-string/jumbo v31, "candidates"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 298
    .local v24, "oldCandidates":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTaskManager(Lcom/vlingo/dialog/manager/model/IFormManager;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v28

    .line 299
    .local v28, "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-static/range {p2 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v27

    .line 300
    .local v27, "taskForm":Lcom/vlingo/dialog/model/IForm;
    move-object/from16 v0, v28

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-interface {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 302
    const-string/jumbo v31, "address"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v31

    if-eqz v31, :cond_3

    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v31

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    move-object/from16 v3, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 307
    :cond_3
    invoke-interface/range {v22 .. v22}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v31

    if-nez v31, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->withinCompleteList(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 309
    const/16 v31, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 310
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v31

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    move-object/from16 v3, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 314
    :cond_4
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->fetchContactResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    move-result-object v14

    .line 316
    .local v14, "cre":Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    const-string/jumbo v31, "choice"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 317
    .local v8, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getLastPrompt()Ljava/lang/String;

    move-result-object v19

    .line 318
    .local v19, "lastPrompt":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v31

    const-string/jumbo v32, "name"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/vlingo/dialog/state/model/State;->getCandidates()Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v31

    if-eqz v31, :cond_9

    const/16 v16, 0x1

    .line 319
    .local v16, "haveCandidates":Z
    :goto_1
    const-string/jumbo v31, "contact"

    move-object/from16 v0, v31

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_5

    if-eqz v16, :cond_a

    :cond_5
    const/16 v17, 0x1

    .line 320
    .local v17, "isChooseContact":Z
    :goto_2
    if-nez v17, :cond_b

    const-string/jumbo v31, "type"

    move-object/from16 v0, v31

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    const/16 v18, 0x1

    .line 326
    .local v18, "isChooseType":Z
    :goto_3
    if-eqz v8, :cond_7

    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v31

    if-eqz v31, :cond_7

    .line 327
    if-eqz v17, :cond_c

    .line 328
    invoke-virtual {v14}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v10

    .line 329
    .local v10, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v31

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v32

    const/16 v33, 0x1

    const/16 v34, 0x0

    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v31 .. v35}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v7

    .line 330
    .local v7, "choice":I
    if-ltz v7, :cond_6

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v31

    move/from16 v0, v31

    if-ge v7, v0, :cond_6

    .line 332
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/event/model/Contact;

    .line 333
    .local v5, "c":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 334
    const/16 v31, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 335
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->markConfident(Lcom/vlingo/dialog/model/IForm;)V

    .line 336
    const-string/jumbo v31, "contactId"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v31

    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 337
    const-string/jumbo v31, "typeId"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v31

    const/16 v32, 0x0

    invoke-interface/range {v31 .. v32}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 361
    .end local v5    # "c":Lcom/vlingo/dialog/event/model/Contact;
    .end local v7    # "choice":I
    .end local v10    # "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    :cond_6
    :goto_4
    const/16 v31, 0x0

    move-object/from16 v0, v31

    invoke-interface {v8, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 362
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-interface {v8, v0}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 365
    :cond_7
    invoke-interface/range {v22 .. v22}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v31

    if-eqz v31, :cond_8

    if-eqz v14, :cond_8

    .line 366
    const-string/jumbo v31, "contactId"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v13

    .line 367
    .local v13, "contactIdSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface/range {v22 .. v22}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v23

    .line 368
    .local v23, "newName":Ljava/lang/String;
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-static {v14, v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->matchName(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    .line 369
    .local v21, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_e

    .line 371
    const/16 v31, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vlingo/dialog/event/model/Contact;

    .line 372
    .local v20, "match":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 373
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-interface {v13, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 374
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/dialog/event/model/Contact;->getScore()Ljava/lang/Float;

    move-result-object v31

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setConfidence(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Float;)V

    .line 389
    .end local v13    # "contactIdSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v20    # "match":Lcom/vlingo/dialog/event/model/Contact;
    .end local v21    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    .end local v23    # "newName":Ljava/lang/String;
    :cond_8
    :goto_5
    const-string/jumbo v31, "candidates"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 390
    .local v6, "candidates":Ljava/lang/String;
    invoke-interface/range {v22 .. v22}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v31

    if-nez v31, :cond_11

    move-object/from16 v0, v24

    invoke-static {v0, v6}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_11

    .line 392
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 318
    .end local v6    # "candidates":Ljava/lang/String;
    .end local v16    # "haveCandidates":Z
    .end local v17    # "isChooseContact":Z
    .end local v18    # "isChooseType":Z
    :cond_9
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 319
    .restart local v16    # "haveCandidates":Z
    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 320
    .restart local v17    # "isChooseContact":Z
    :cond_b
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 339
    .restart local v18    # "isChooseType":Z
    :cond_c
    if-eqz v18, :cond_6

    .line 340
    const-string/jumbo v31, "contactId"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 341
    .local v12, "contactId":Ljava/lang/String;
    invoke-static {v14, v12}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->contactFromId(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Contact;

    move-result-object v11

    .line 342
    .local v11, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v11}, Lcom/vlingo/dialog/event/model/Contact;->getAddresses()Ljava/util/List;

    move-result-object v31

    invoke-interface/range {v30 .. v30}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->filterAddressesByType(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 343
    .local v9, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getTypeLimit()Ljava/lang/Integer;

    move-result-object v29

    .line 344
    .local v29, "typeLimit":Ljava/lang/Integer;
    if-eqz v29, :cond_d

    .line 345
    const/16 v31, 0x0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v32

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Integer;->intValue()I

    move-result v33

    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->min(II)I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-interface {v9, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    .line 347
    :cond_d
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v31

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v32

    const/16 v33, 0x1

    const/16 v34, 0x0

    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v31 .. v35}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v7

    .line 348
    .restart local v7    # "choice":I
    if-ltz v7, :cond_6

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v31

    move/from16 v0, v31

    if-ge v7, v0, :cond_6

    .line 350
    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 351
    const/16 v31, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 352
    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 353
    const/16 v31, 0x0

    invoke-interface/range {v30 .. v31}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 354
    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/Address;

    .line 355
    .local v4, "a":Lcom/vlingo/dialog/event/model/Address;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->markConfident(Lcom/vlingo/dialog/model/IForm;)V

    .line 356
    const-string/jumbo v31, "typeId"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v31

    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Address;->getId()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 357
    const-string/jumbo v31, "type"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v31

    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Address;->getType()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 358
    const-string/jumbo v31, "address"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v31

    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Address;->getDetail()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 375
    .end local v4    # "a":Lcom/vlingo/dialog/event/model/Address;
    .end local v7    # "choice":I
    .end local v9    # "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Address;>;"
    .end local v11    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    .end local v12    # "contactId":Ljava/lang/String;
    .end local v29    # "typeLimit":Ljava/lang/Integer;
    .restart local v13    # "contactIdSlot":Lcom/vlingo/dialog/model/IForm;
    .restart local v21    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    .restart local v23    # "newName":Ljava/lang/String;
    :cond_e
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_f

    .line 377
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v31

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->setNumMatches(I)V

    .line 378
    invoke-virtual {v14}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->clear()V

    .line 379
    invoke-virtual {v14}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    .line 382
    :cond_f
    sget-object v31, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v31 .. v31}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v31

    if-eqz v31, :cond_10

    sget-object v31, Lcom/vlingo/dialog/manager/model/ContactFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v32, "no name match: bumping upstairs"

    invoke-virtual/range {v31 .. v32}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 383
    :cond_10
    const/16 v31, 0x0

    move-object/from16 v0, v31

    invoke-interface {v13, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 384
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v31

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-interface {v0, v1, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 396
    .end local v13    # "contactIdSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v21    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Contact;>;"
    .end local v23    # "newName":Ljava/lang/String;
    .restart local v6    # "candidates":Ljava/lang/String;
    :cond_11
    if-nez v14, :cond_12

    .line 399
    invoke-virtual/range {p0 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 402
    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->resolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Z

    move-result v31

    if-eqz v31, :cond_0

    .line 403
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v31

    if-nez v31, :cond_13

    .line 404
    invoke-static/range {p1 .. p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->annotateContact(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 406
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v31

    invoke-interface/range {p2 .. p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    move-object/from16 v3, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 17
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 688
    const-string/jumbo v3, "address"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    .line 689
    .local v9, "addressSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v3, "name"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v12

    .line 691
    .local v12, "nameSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v12}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 692
    invoke-interface {v9}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 693
    invoke-interface {v9}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 694
    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 697
    :cond_0
    const/4 v3, 0x1

    .line 738
    :goto_0
    return v3

    .line 700
    :cond_1
    const-string/jumbo v3, "type"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v15

    .line 701
    .local v15, "typeSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v3, "contactId"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v10

    .line 702
    .local v10, "contactIdSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v3, "typeId"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v14

    .line 704
    .local v14, "typeIdSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v12}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v13

    .line 705
    .local v13, "nameWasFilled":Z
    invoke-interface {v15}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v16

    .line 708
    .local v16, "typeWasFilled":Z
    if-eqz v13, :cond_2

    .line 709
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 710
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 711
    const/4 v3, 0x0

    invoke-interface {v9, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 712
    if-nez v16, :cond_2

    .line 713
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 716
    :cond_2
    if-eqz v16, :cond_3

    .line 717
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 720
    :cond_3
    invoke-interface {v14}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 722
    const/4 v3, 0x1

    goto :goto_0

    .line 725
    :cond_4
    const/4 v11, 0x0

    .line 726
    .local v11, "cre":Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    if-nez v13, :cond_5

    .line 727
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->fetchContactResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    move-result-object v11

    .line 730
    :cond_5
    if-nez v11, :cond_6

    .line 733
    invoke-interface {v12}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v3, "capability"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v3, "type"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v8}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->addContactQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    .line 735
    const/4 v3, 0x0

    goto :goto_0

    .line 738
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v11}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->resolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ContactResolvedEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method public prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 663
    const-string/jumbo v0, "name"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 664
    .local v7, "name":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 666
    .local v3, "nameValue":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 668
    const-string/jumbo v0, "candidates"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 669
    .local v6, "candidates":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 670
    const-string/jumbo v0, "capability"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->addCandidatesQueryGoal(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    .line 671
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->maybeSendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 673
    :cond_0
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v8

    .line 674
    .local v8, "nameManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lcom/vlingo/dialog/model/IForm;->setPromptCount(I)V

    .line 675
    invoke-interface {v8, p1, v7}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 676
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 682
    .end local v6    # "candidates":Ljava/lang/String;
    .end local v8    # "nameManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :goto_0
    return-void

    .line 679
    :cond_1
    const-string/jumbo v0, "capability"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "type"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->addContactQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    goto :goto_0
.end method

.method protected scrollingBuildTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 851
    .local p3, "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .local p4, "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getDisambig(Lcom/vlingo/dialog/DMContext;)Ljava/lang/String;

    move-result-object v7

    .line 852
    .local v7, "disambig":Ljava/lang/String;
    const-string/jumbo v0, "contact"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    invoke-static {p1, p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->buildNamePipeList(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 855
    .local v8, "templateForm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v0, "contact"

    invoke-static {v8, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setScrollType(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    .line 863
    .end local v8    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    return-object v8

    .line 857
    :cond_0
    const-string/jumbo v0, "type"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 858
    invoke-static {p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->buildSubtypesPipeList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 860
    .restart local v8    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v0, "type"

    invoke-static {v8, v0}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setScrollType(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    goto :goto_0

    .line 863
    .end local v8    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method protected scrollingFetchList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 829
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->fetchContactResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    move-result-object v2

    .line 830
    .local v2, "cre":Lcom/vlingo/dialog/event/model/ContactResolvedEvent;
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->getDisambig(Lcom/vlingo/dialog/DMContext;)Ljava/lang/String;

    move-result-object v3

    .line 831
    .local v3, "disambig":Ljava/lang/String;
    const-string/jumbo v6, "contact"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 832
    .local v4, "isChooseContact":Z
    const-string/jumbo v6, "type"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 833
    .local v5, "isChooseType":Z
    if-eqz v4, :cond_0

    .line 834
    const-string/jumbo v6, "contact"

    invoke-static {p1, v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setDisambig(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 835
    invoke-virtual {v2}, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;->getMatches()Ljava/util/List;

    move-result-object v6

    .line 843
    :goto_0
    return-object v6

    .line 836
    :cond_0
    if-eqz v5, :cond_1

    .line 837
    const-string/jumbo v6, "type"

    invoke-static {p1, v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->setDisambig(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 838
    const-string/jumbo v6, "contactId"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 839
    .local v1, "contactId":Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->contactFromId(Lcom/vlingo/dialog/event/model/ContactResolvedEvent;Ljava/lang/String;)Lcom/vlingo/dialog/event/model/Contact;

    move-result-object v0

    .line 840
    .local v0, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Contact;->getAddresses()Ljava/util/List;

    move-result-object v6

    const-string/jumbo v7, "type"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->filterAddressesByType(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    goto :goto_0

    .line 842
    .end local v0    # "contact":Lcom/vlingo/dialog/event/model/Contact;
    .end local v1    # "contactId":Ljava/lang/String;
    :cond_1
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->clearDisambig(Lcom/vlingo/dialog/DMContext;)V

    .line 843
    const/4 v6, 0x0

    goto :goto_0
.end method
