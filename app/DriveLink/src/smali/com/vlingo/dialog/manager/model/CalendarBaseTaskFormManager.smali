.class public Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;
.source "CalendarBaseTaskFormManager.java"


# static fields
.field protected static final CONSTRAINT_SLOTS:[Ljava/lang/String;

.field protected static final MATCH_TYPE_BEGIN:Ljava/lang/String; = "begin"

.field protected static final MATCH_TYPE_END:Ljava/lang/String; = "end"

.field protected static final MATCH_TYPE_INTERSECT:Ljava/lang/String; = "intersect"

.field protected static final PIPE_JOINER:Lcom/google/common/base/Joiner;

.field protected static final QUERY_KEY_CONFLICT_CACHE:Ljava/lang/String; = "ConflictQuery"

.field protected static final SLOT_APT:Ljava/lang/String; = "appointment"

.field protected static final SLOT_APT_ALL_DAY:Ljava/lang/String; = "all_day"

.field protected static final SLOT_APT_DATE:Ljava/lang/String; = "date"

.field protected static final SLOT_APT_DATE_ALT:Ljava/lang/String; = "date_alt"

.field protected static final SLOT_APT_DURATION:Ljava/lang/String; = "duration"

.field protected static final SLOT_APT_INVITEES:Ljava/lang/String; = "invitees"

.field protected static final SLOT_APT_LIST:Ljava/lang/String; = "appointment_list"

.field protected static final SLOT_APT_LOCATION:Ljava/lang/String; = "location"

.field protected static final SLOT_APT_TIME:Ljava/lang/String; = "time"

.field protected static final SLOT_APT_TITLE:Ljava/lang/String; = "title"

.field protected static final SLOT_CHOICE:Ljava/lang/String; = "choice"

.field protected static final SLOT_DATE:Ljava/lang/String; = "date"

.field protected static final SLOT_DURATION:Ljava/lang/String; = "duration"

.field protected static final SLOT_END_TIME:Ljava/lang/String; = "endtime"

.field protected static final SLOT_ID:Ljava/lang/String; = "id"

.field protected static final SLOT_INVITEE:Ljava/lang/String; = "invitee"

.field protected static final SLOT_INVITEE_LIST:Ljava/lang/String; = "invitee_list"

.field protected static final SLOT_LOCATION:Ljava/lang/String; = "location"

.field protected static final SLOT_NEW_TIME:Ljava/lang/String; = "newtime"

.field protected static final SLOT_TIME:Ljava/lang/String; = "time"

.field protected static final SLOT_TITLE:Ljava/lang/String; = "title"

.field protected static final SLOT_WHICH:Ljava/lang/String; = "which"

.field protected static final STATE_NO_MATCH_ISSUED:Ljava/lang/String; = "event_no_match_issued"

.field protected static final TIME_MIDNIGHT:Ljava/lang/String; = "00:00"

.field protected static final WHICH_FIRST:Ljava/lang/String; = "first"

.field protected static final WHICH_LAST:Ljava/lang/String; = "last"

.field protected static final WHICH_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static final WHICH_NEXT:Ljava/lang/String; = "next"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    new-instance v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->WHICH_MAP:Ljava/util/Map;

    .line 82
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "time"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "location"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "invitee"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "which"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .line 91
    const/16 v0, 0x7c

    invoke-static {v0}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Joiner;->skipNulls()Lcom/google/common/base/Joiner;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->PIPE_JOINER:Lcom/google/common/base/Joiner;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method protected static addAppointmentChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Appointment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 553
    .local p1, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/AppointmentChooseGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/AppointmentChooseGoal;-><init>()V

    .line 554
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/AppointmentChooseGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentChooseGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 555
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 556
    return-void
.end method

.method protected static addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 376
    invoke-virtual {p2}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 377
    return-void
.end method

.method protected static addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V
    .locals 0
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;
    .param p3, "clearCache"    # Z

    .prologue
    .line 380
    invoke-virtual {p0, p2, p3}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 381
    invoke-static {p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 382
    return-void
.end method

.method protected static addAppointmentShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Appointment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559
    .local p1, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/AppointmentShowGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/AppointmentShowGoal;-><init>()V

    .line 560
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/AppointmentShowGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentShowGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 561
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 562
    return-void
.end method

.method static addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V
    .locals 7
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Appointment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    .line 98
    .local v2, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    new-instance v5, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v5}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 99
    .local v5, "listForm":Lcom/vlingo/dialog/model/Form;
    const-string/jumbo v6, "appointment_list"

    invoke-virtual {v5, v6}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 101
    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 103
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 104
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    new-instance v1, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v1}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 105
    .local v1, "aptForm":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v5}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    .line 106
    .local v3, "i":I
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v5, v1}, Lcom/vlingo/dialog/model/Form;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 108
    const/4 v6, 0x1

    invoke-static {v2, v1, v0, v6}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->populateAppointment(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;Z)V

    goto :goto_0

    .line 110
    .end local v0    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v1    # "aptForm":Lcom/vlingo/dialog/model/Form;
    .end local v3    # "i":I
    :cond_0
    return-void
.end method

.method protected static anyConstraintsFilled(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 474
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 475
    .local v3, "slotName":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->slotIsFilled(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 476
    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "slot "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " is set"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 477
    :cond_0
    const/4 v4, 0x1

    .line 480
    .end local v3    # "slotName":Ljava/lang/String;
    :goto_1
    return v4

    .line 474
    .restart local v3    # "slotName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 480
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected static anyConstraintsFilledOtherThanWhich(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 484
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 485
    .local v3, "slotName":Ljava/lang/String;
    const-string/jumbo v4, "which"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p0, v3}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->slotIsFilled(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 486
    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "slot "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " is set"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 487
    :cond_0
    const/4 v4, 0x1

    .line 490
    .end local v3    # "slotName":Ljava/lang/String;
    :goto_1
    return v4

    .line 484
    .restart local v3    # "slotName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 490
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected static anyConstraintsSet(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 464
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 465
    .local v3, "slotName":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->slotIsSet(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 466
    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "slot "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " is set"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 467
    :cond_0
    const/4 v4, 0x1

    .line 470
    .end local v3    # "slotName":Ljava/lang/String;
    :goto_1
    return v4

    .line 464
    .restart local v3    # "slotName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 470
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected static clearDateIfRange(Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 616
    const-string/jumbo v1, "date"

    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/model/Slot;

    .line 617
    .local v0, "dateSlot":Lcom/vlingo/dialog/model/Slot;
    if-eqz v0, :cond_0

    .line 618
    invoke-virtual {v0}, Lcom/vlingo/dialog/model/Slot;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->isRange(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 619
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Slot;->setValue(Ljava/lang/String;)V

    .line 620
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Slot;->setComplete(Z)V

    .line 623
    :cond_0
    return-void
.end method

.method protected static clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 424
    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    const-string/jumbo v1, "id"

    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 426
    .local v0, "idSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 427
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 430
    .end local v0    # "idSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    return-void
.end method

.method protected static fetchAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 549
    const-class v0, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->fetchQueryEvent(Ljava/lang/Class;)Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    return-object v0
.end method

.method protected static filterAppointments(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V
    .locals 12
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "event"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v3

    .line 202
    .local v3, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 203
    const-string/jumbo v9, "time"

    invoke-interface {p0, v9}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 204
    .local v8, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 205
    .local v5, "time":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 206
    move-object v6, v5

    .line 207
    .local v6, "timeAm":Ljava/lang/String;
    move-object v7, v5

    .line 208
    .local v7, "timePm":Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 209
    invoke-static {v5}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToAm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 210
    invoke-static {v5}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToPm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 212
    :cond_0
    const/4 v0, 0x0

    .line 213
    .local v0, "anyRemoved":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/event/model/Appointment;>;"
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 214
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/Appointment;

    .line 215
    .local v1, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v2

    .line 216
    .local v2, "appointmentTime":Ljava/lang/String;
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 217
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 218
    const/4 v0, 0x1

    goto :goto_0

    .line 221
    .end local v1    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v2    # "appointmentTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {p1, v9}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->setNumMatches(I)V

    .line 222
    if-eqz v0, :cond_3

    .line 223
    sget-object v9, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v9}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v9

    if-eqz v9, :cond_3

    sget-object v9, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "filtered appointments by time: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 227
    .end local v0    # "anyRemoved":Z
    .end local v4    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/event/model/Appointment;>;"
    .end local v5    # "time":Ljava/lang/String;
    .end local v6    # "timeAm":Ljava/lang/String;
    .end local v7    # "timePm":Ljava/lang/String;
    .end local v8    # "timeSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_3
    return-void
.end method

.method protected static filterForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 409
    .local p1, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object p0

    .line 410
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 411
    .local v2, "constraintSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 412
    .local v1, "constraint":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 413
    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 411
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 416
    .end local v1    # "constraint":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method protected static getConstraints(Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 6
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 400
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 401
    .local v1, "constraint":Ljava/lang/String;
    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 402
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 405
    .end local v1    # "constraint":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method private havePriorResults(Lcom/vlingo/dialog/DMContext;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 185
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->fetchAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static populateAppointment(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;Z)V
    .locals 8
    .param p0, "clientDateTime"    # Lcom/vlingo/dialog/util/DateTime;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;
    .param p3, "detailed"    # Z

    .prologue
    .line 584
    const/4 v0, 0x0

    .line 585
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 586
    const-string/jumbo v0, "today"

    .line 591
    :cond_0
    :goto_0
    const-string/jumbo v6, "title"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string/jumbo v6, "date"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string/jumbo v6, "date_alt"

    invoke-static {p1, v6, v0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    const-string/jumbo v6, "time"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string/jumbo v6, "duration"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getDuration()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const-string/jumbo v6, "location"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string/jumbo v6, "all_day"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getAllDay()Z

    move-result v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 599
    if-eqz p3, :cond_4

    .line 600
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 601
    .local v4, "inviteeSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getInvitees()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/event/model/Contact;

    .line 602
    .local v3, "invitee":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v3}, Lcom/vlingo/dialog/event/model/Contact;->getName()Ljava/lang/String;

    move-result-object v5

    .line 603
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v2

    .line 604
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 605
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_id_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 607
    :cond_1
    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 587
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "invitee":Lcom/vlingo/dialog/event/model/Contact;
    .end local v4    # "inviteeSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "name":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Appointment;->getDate()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 588
    const-string/jumbo v0, "tomorrow"

    goto/16 :goto_0

    .line 609
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "inviteeSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 610
    const-string/jumbo v6, "invitees"

    sget-object v7, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->PIPE_JOINER:Lcom/google/common/base/Joiner;

    invoke-virtual {v7, v4}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "inviteeSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_4
    return-void
.end method

.method protected static relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v6, 0x0

    .line 504
    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "relaxing all constraints"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 505
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 506
    .local v3, "slotName":Ljava/lang/String;
    invoke-static {p0, v3, v6}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 508
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_1
    const-string/jumbo v4, "which"

    invoke-static {p0, v4, v6}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    return-void
.end method

.method protected static resolveTimeAmPmFromAppointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V
    .locals 4
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 230
    const-string/jumbo v1, "time"

    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 231
    .local v0, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Appointment;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 233
    sget-object v1, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "resolved time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 235
    :cond_0
    return-void
.end method

.method private static slotIsFilled(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 499
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 500
    .local v0, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static slotIsSet(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 494
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 495
    .local v0, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addAppointmentConflictQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;I)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "count"    # I

    .prologue
    .line 436
    const/4 v0, 0x1

    .line 438
    .local v0, "clearCache":Z
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 440
    .local v1, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    const-string/jumbo v10, "date"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 441
    .local v2, "date":Ljava/lang/String;
    const-string/jumbo v10, "time"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 442
    .local v9, "time":Ljava/lang/String;
    const-string/jumbo v10, "duration"

    invoke-interface {p2, v10}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 443
    .local v3, "duration":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 445
    .local v4, "durationInt":I
    invoke-static {v1, v2, v9}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v8

    .line 446
    .local v8, "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v8, v4}, Lcom/vlingo/dialog/util/DateTime;->addMinutes(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v7

    .line 448
    .local v7, "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    const-string/jumbo v6, "intersect"

    .line 450
    .local v6, "matchType":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    invoke-direct {v5}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;-><init>()V

    .line 452
    .local v5, "goal":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    invoke-virtual {v8}, Lcom/vlingo/dialog/util/DateTime;->getDateTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setRangeStart(Ljava/lang/String;)V

    .line 453
    invoke-virtual {v7}, Lcom/vlingo/dialog/util/DateTime;->getDateTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setRangeEnd(Ljava/lang/String;)V

    .line 454
    invoke-virtual {v5, v6}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setMatchType(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getConflictIncludeAllDay()Z

    move-result v10

    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setIncludeAllDay(Z)V

    .line 456
    if-eqz p3, :cond_0

    .line 457
    new-instance v10, Ljava/lang/Integer;

    invoke-direct {v10, p3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setCount(Ljava/lang/Integer;)V

    .line 460
    :cond_0
    const/4 v10, 0x1

    invoke-static {p1, p2, v5, v10}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 461
    return-void
.end method

.method protected addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 385
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->buildAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    move-result-object v0

    .line 386
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getClearCache()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 387
    invoke-static {p2, v0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 388
    return-void
.end method

.method protected buildAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    .locals 22
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 261
    const/4 v2, 0x1

    .line 263
    .local v2, "clearCache":Z
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 265
    .local v3, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    const-string/jumbo v20, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 266
    .local v17, "title":Ljava/lang/String;
    const-string/jumbo v20, "location"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 267
    .local v10, "location":Ljava/lang/String;
    const-string/jumbo v20, "invitee"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 268
    .local v9, "invitee":Ljava/lang/String;
    const-string/jumbo v20, "which"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 269
    .local v18, "which":Ljava/lang/String;
    const-string/jumbo v20, "date"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 270
    .local v5, "date":Ljava/lang/String;
    const-string/jumbo v20, "time"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 271
    .local v14, "time":Ljava/lang/String;
    const/4 v4, 0x0

    .line 274
    .local v4, "count":I
    if-nez v14, :cond_5

    const/4 v8, 0x1

    .line 276
    .local v8, "includeAllDay":Z
    :goto_0
    const-string/jumbo v11, "begin"

    .line 278
    .local v11, "matchType":Ljava/lang/String;
    const-string/jumbo v20, "last"

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    if-nez v5, :cond_0

    .line 279
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v5

    .line 282
    :cond_0
    if-nez v5, :cond_6

    if-nez v14, :cond_6

    .line 284
    move-object v13, v3

    .line 285
    .local v13, "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->endOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getRangeDays()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v12

    .line 286
    .local v12, "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    if-nez v4, :cond_1

    .line 287
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getRangeCount()I

    move-result v4

    .line 347
    :cond_1
    :goto_1
    if-nez v14, :cond_2

    if-eqz v18, :cond_2

    .line 349
    sget-object v20, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->WHICH_MAP:Ljava/util/Map;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    .line 350
    .local v19, "whichCount":Ljava/lang/Integer;
    if-eqz v19, :cond_2

    .line 351
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 355
    .end local v19    # "whichCount":Ljava/lang/Integer;
    :cond_2
    if-nez v4, :cond_3

    .line 356
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getLimit()I

    move-result v4

    .line 359
    :cond_3
    new-instance v7, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    invoke-direct {v7}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;-><init>()V

    .line 360
    .local v7, "goal":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setClearCache(Z)V

    .line 361
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setTitle(Ljava/lang/String;)V

    .line 362
    invoke-virtual {v7, v10}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setLocation(Ljava/lang/String;)V

    .line 363
    invoke-virtual {v7, v9}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setInvitee(Ljava/lang/String;)V

    .line 364
    invoke-virtual {v13}, Lcom/vlingo/dialog/util/DateTime;->getDateTime()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setRangeStart(Ljava/lang/String;)V

    .line 365
    invoke-virtual {v12}, Lcom/vlingo/dialog/util/DateTime;->getDateTime()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setRangeEnd(Ljava/lang/String;)V

    .line 366
    invoke-virtual {v7, v11}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setMatchType(Ljava/lang/String;)V

    .line 367
    invoke-virtual {v7, v8}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setIncludeAllDay(Z)V

    .line 368
    if-eqz v4, :cond_4

    .line 369
    new-instance v20, Ljava/lang/Integer;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->setCount(Ljava/lang/Integer;)V

    .line 372
    :cond_4
    return-object v7

    .line 274
    .end local v7    # "goal":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    .end local v8    # "includeAllDay":Z
    .end local v11    # "matchType":Ljava/lang/String;
    .end local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 289
    .restart local v8    # "includeAllDay":Z
    .restart local v11    # "matchType":Ljava/lang/String;
    :cond_6
    if-nez v5, :cond_b

    .line 292
    const/4 v8, 0x0

    .line 293
    invoke-static {v14}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 294
    invoke-static {v14}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToAm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 295
    .local v15, "timeAm":Ljava/lang/String;
    invoke-static {v14}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToPm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 296
    .local v16, "timePm":Ljava/lang/String;
    invoke-static {v3, v5, v15}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 297
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v13, v3}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 298
    move-object/from16 v0, v16

    invoke-static {v3, v5, v0}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 300
    :cond_7
    invoke-virtual {v13, v3}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 301
    invoke-static {v3, v5, v15}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 302
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 304
    :cond_8
    const/16 v20, 0x2d0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/vlingo/dialog/util/DateTime;->addMinutes(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v12

    .line 305
    .restart local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    goto/16 :goto_1

    .line 306
    .end local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    .end local v15    # "timeAm":Ljava/lang/String;
    .end local v16    # "timePm":Ljava/lang/String;
    :cond_9
    invoke-static {v3, v5, v14}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 307
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v13, v3}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 308
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 310
    :cond_a
    move-object v12, v13

    .restart local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    goto/16 :goto_1

    .line 312
    .end local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_b
    invoke-static {v5}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->isRange(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 313
    const/4 v14, 0x0

    .line 314
    invoke-static {v5}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->splitRange(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 315
    .local v6, "dates":[Ljava/lang/String;
    const/16 v20, 0x0

    aget-object v20, v6, v20

    const-string/jumbo v21, "00:00"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 316
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v13, v3}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 317
    move-object v13, v3

    .line 319
    :cond_c
    const/16 v20, 0x1

    aget-object v20, v6, v20

    const-string/jumbo v21, "00:00"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/dialog/util/DateTime;->endOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v12

    .line 320
    .restart local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    goto/16 :goto_1

    .end local v6    # "dates":[Ljava/lang/String;
    .end local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_d
    if-nez v14, :cond_10

    .line 322
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 323
    move-object v13, v3

    .line 330
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_e
    :goto_2
    invoke-virtual {v13}, Lcom/vlingo/dialog/util/DateTime;->endOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v12

    .restart local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    goto/16 :goto_1

    .line 325
    .end local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_f
    const-string/jumbo v20, "00:00"

    move-object/from16 v0, v20

    invoke-static {v3, v5, v0}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 326
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->startOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 327
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/vlingo/dialog/util/DateTime;->addYears(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    goto :goto_2

    .line 333
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_10
    invoke-static {v14}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_11

    .line 334
    invoke-static {v14}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToAm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 335
    .restart local v15    # "timeAm":Ljava/lang/String;
    invoke-static {v14}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToPm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 336
    .restart local v16    # "timePm":Ljava/lang/String;
    invoke-static {v3, v5, v15}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 337
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    move-object/from16 v0, v16

    invoke-static {v3, v5, v0}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v12

    .line 338
    .restart local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v13, v3}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-virtual {v12, v3}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 339
    move-object v13, v12

    goto/16 :goto_1

    .line 342
    .end local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    .end local v15    # "timeAm":Ljava/lang/String;
    .end local v16    # "timePm":Ljava/lang/String;
    :cond_11
    invoke-static {v3, v5, v14}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v13

    .line 343
    .restart local v13    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    move-object v12, v13

    .restart local v12    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    goto/16 :goto_1
.end method

.method public cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 196
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ConflictQuery"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method protected convertEndTimeToDuration(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "dateName"    # Ljava/lang/String;
    .param p4, "timeName"    # Ljava/lang/String;
    .param p5, "durationName"    # Ljava/lang/String;
    .param p6, "endTimeName"    # Ljava/lang/String;

    .prologue
    .line 514
    invoke-interface {p2, p6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 515
    .local v3, "endTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 516
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 517
    .local v0, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-interface {p2, p3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 518
    .local v1, "dateSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {p2, p4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 519
    .local v4, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {p2, p5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 520
    .local v2, "durationSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v0, v1, v4, v2, v3}, Lcom/vlingo/dialog/util/TimeRange;->convertEndTimeToDuration(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 522
    .end local v0    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v1    # "dateSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "durationSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "timeSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    return-void
.end method

.method protected fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 565
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->buildAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    move-result-object v2

    .line 566
    .local v2, "queryGoal":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    invoke-virtual {p1, v2}, Lcom/vlingo/dialog/DMContext;->fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 567
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    if-nez v0, :cond_0

    .line 568
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 569
    .local v4, "time":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isResolved(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 570
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 571
    .local v1, "formUnresolvedAmPm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "time"

    invoke-interface {v1, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->markUnresolved(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 572
    invoke-virtual {p0, p1, v1}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->buildAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    move-result-object v3

    .line 573
    .local v3, "queryGoalUnresolvedAmPm":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    invoke-virtual {p1, v3}, Lcom/vlingo/dialog/DMContext;->fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 576
    .end local v1    # "formUnresolvedAmPm":Lcom/vlingo/dialog/model/IForm;
    .end local v3    # "queryGoalUnresolvedAmPm":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    .end local v4    # "time":Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 577
    new-instance v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    const/4 v5, 0x0

    invoke-direct {v0, v2, v5}, Lcom/vlingo/dialog/state/model/CompletedQuery;-><init>(Lcom/vlingo/dialog/goal/model/QueryGoal;Lcom/vlingo/dialog/event/model/QueryEvent;)V

    .line 579
    .restart local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :cond_1
    return-object v0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 115
    const-string/jumbo v6, "which"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "prevWhich":Ljava/lang/String;
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 121
    const-string/jumbo v6, "which"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 122
    .local v3, "whichSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v6

    if-eqz v6, :cond_3

    move v2, v4

    .line 124
    .local v2, "whichFilled":Z
    :goto_0
    const-string/jumbo v6, "choice"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 125
    .local v0, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 126
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 127
    invoke-interface {v0, v5}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 130
    :cond_0
    if-eqz v2, :cond_4

    const-string/jumbo v6, "next"

    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 132
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->isLookup()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 134
    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "clearing slots because which=next"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 135
    :cond_1
    const-string/jumbo v4, "title"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 136
    const-string/jumbo v4, "date"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 137
    const-string/jumbo v4, "time"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 138
    const-string/jumbo v4, "invitee"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 139
    const-string/jumbo v4, "location"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 182
    :cond_2
    :goto_1
    return-void

    .end local v0    # "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "whichFilled":Z
    :cond_3
    move v2, v5

    .line 122
    goto :goto_0

    .line 142
    .restart local v0    # "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    .restart local v2    # "whichFilled":Z
    :cond_4
    if-eqz v2, :cond_6

    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->anyConstraintsFilledOtherThanWhich(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v6

    if-nez v6, :cond_6

    if-eqz v0, :cond_6

    invoke-direct {p0, p1}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->havePriorResults(Lcom/vlingo/dialog/DMContext;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 149
    sget-object v6, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v6}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v6, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v7, "transforming which to choice"

    invoke-virtual {v6, v7}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 150
    :cond_5
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 151
    invoke-interface {v0, v4}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 152
    invoke-interface {v3, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 153
    invoke-interface {v3, v5}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    goto :goto_1

    .line 155
    :cond_6
    if-eqz v2, :cond_7

    const-string/jumbo v4, "1st"

    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 160
    const-string/jumbo v4, "first"

    invoke-interface {v3, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :cond_7
    const-string/jumbo v4, "id"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->anySlotsFilled(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 164
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->isLookup()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 173
    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_8

    sget-object v4, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "clearing slots because no slots filled"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 174
    :cond_8
    const-string/jumbo v4, "title"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 175
    const-string/jumbo v4, "date"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 176
    const-string/jumbo v4, "time"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 177
    const-string/jumbo v4, "invitee"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 178
    const-string/jumbo v4, "location"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_1
.end method

.method public getConflictTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isLookup()Z
    .locals 1

    .prologue
    .line 626
    const/4 v0, 0x0

    return v0
.end method

.method public issueConflictDetectionQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 10
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v7, 0x0

    .line 529
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getConflictTemplate()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 530
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v5

    .line 531
    .local v5, "state":Lcom/vlingo/dialog/state/model/State;
    const-string/jumbo v8, "date"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 532
    .local v1, "date":Ljava/lang/String;
    const-string/jumbo v8, "time"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 533
    .local v6, "time":Ljava/lang/String;
    const-string/jumbo v8, "duration"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-interface {v8}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 534
    .local v2, "duration":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "ConflictQuery"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535
    .local v0, "conflictKey":Ljava/lang/String;
    invoke-virtual {v5, v0}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 536
    .local v4, "oldConflictQuery":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 537
    .local v3, "newConflictQuery":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 539
    invoke-virtual {v5, v0, v3}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    invoke-virtual {p0, p1, p2, v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->addAppointmentConflictQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;I)V

    .line 541
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 542
    const/4 v7, 0x1

    .line 545
    .end local v0    # "conflictKey":Ljava/lang/String;
    .end local v1    # "date":Ljava/lang/String;
    .end local v2    # "duration":Ljava/lang/String;
    .end local v3    # "newConflictQuery":Ljava/lang/String;
    .end local v4    # "oldConflictQuery":Ljava/lang/String;
    .end local v5    # "state":Lcom/vlingo/dialog/state/model/State;
    .end local v6    # "time":Ljava/lang/String;
    :cond_0
    return v7
.end method

.method protected resolveTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 239
    const-string/jumbo v6, "time"

    invoke-virtual {p0, v6}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    .line 240
    .local v4, "timeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    const-string/jumbo v6, "time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 242
    .local v5, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 243
    .local v1, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "clientDate":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "clientTime":Ljava/lang/String;
    const-string/jumbo v6, "date"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 248
    .local v3, "date":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 249
    :cond_0
    invoke-static {v4, v5, v2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 251
    :cond_1
    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 252
    return-void
.end method

.method protected scrollingBuildTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 645
    .local p3, "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .local p4, "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    const/4 v3, 0x0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 646
    .local v7, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p1, v7, p3}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 647
    invoke-static {v7}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->clearDateIfRange(Lcom/vlingo/dialog/model/IForm;)V

    .line 648
    return-object v7
.end method

.method protected scrollingFetchList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 631
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 632
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    if-eqz v0, :cond_0

    .line 633
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .line 634
    .local v1, "event":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    if-eqz v1, :cond_0

    .line 635
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v2

    .line 638
    .end local v1    # "event":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 190
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->startTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 191
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ConflictQuery"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 192
    return-void
.end method
