.class public Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;
.super Ljava/lang/Object;
.source "DialogManagerConfigBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ContactAnnotateMinSoftwareVersion:Ljava/lang/String; = "ContactAnnotateMinSoftwareVersion"

.field public static final PROP_DialogManagerConfigMaps:Ljava/lang/String; = "DialogManagerConfigMaps"

.field public static final PROP_FieldIdPattern:Ljava/lang/String; = "FieldIdPattern"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Imports:Ljava/lang/String; = "Imports"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_ScrollAutoListen:Ljava/lang/String; = "ScrollAutoListen"

.field public static final PROP_ScrollDefaultListSize:Ljava/lang/String; = "ScrollDefaultListSize"

.field public static final PROP_ScrollErrorAutoListen:Ljava/lang/String; = "ScrollErrorAutoListen"

.field public static final PROP_ScrollShowUserTurn:Ljava/lang/String; = "ScrollShowUserTurn"

.field public static final PROP_Top:Ljava/lang/String; = "Top"


# instance fields
.field private ContactAnnotateMinSoftwareVersion:Ljava/lang/String;

.field private DialogManagerConfigMaps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;",
            ">;"
        }
    .end annotation
.end field

.field private FieldIdPattern:Ljava/lang/String;

.field private ID:J

.field private Imports:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/manager/model/Import;",
            ">;"
        }
    .end annotation
.end field

.field private Name:Ljava/lang/String;

.field private ScrollAutoListen:Z

.field private ScrollDefaultListSize:I

.field private ScrollErrorAutoListen:Z

.field private ScrollShowUserTurn:Z

.field private Top:Lcom/vlingo/dialog/manager/model/TopFormManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string/jumbo v0, "dm_.*"

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->FieldIdPattern:Ljava/lang/String;

    .line 11
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollDefaultListSize:I

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollAutoListen:Z

    .line 15
    iput-boolean v1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollErrorAutoListen:Z

    .line 17
    iput-boolean v1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollShowUserTurn:Z

    .line 21
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->Imports:Ljava/util/Set;

    .line 23
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->DialogManagerConfigMaps:Ljava/util/Set;

    .line 29
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 99
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 103
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getContactAnnotateMinSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ContactAnnotateMinSoftwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogManagerConfigMaps()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->DialogManagerConfigMaps:Ljava/util/Set;

    return-object v0
.end method

.method public getFieldIdPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->FieldIdPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ID:J

    return-wide v0
.end method

.method public getImports()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/manager/model/Import;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->Imports:Ljava/util/Set;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollAutoListen()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollAutoListen:Z

    return v0
.end method

.method public getScrollDefaultListSize()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollDefaultListSize:I

    return v0
.end method

.method public getScrollErrorAutoListen()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollErrorAutoListen:Z

    return v0
.end method

.method public getScrollShowUserTurn()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollShowUserTurn:Z

    return v0
.end method

.method public getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->Top:Lcom/vlingo/dialog/manager/model/TopFormManager;

    return-object v0
.end method

.method public setContactAnnotateMinSoftwareVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ContactAnnotateMinSoftwareVersion:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setFieldIdPattern(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->FieldIdPattern:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ID:J

    .line 95
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->Name:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setScrollAutoListen(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollAutoListen:Z

    .line 63
    return-void
.end method

.method public setScrollDefaultListSize(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollDefaultListSize:I

    .line 56
    return-void
.end method

.method public setScrollErrorAutoListen(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollErrorAutoListen:Z

    .line 70
    return-void
.end method

.method public setScrollShowUserTurn(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->ScrollShowUserTurn:Z

    .line 77
    return-void
.end method

.method public setTop(Lcom/vlingo/dialog/manager/model/TopFormManager;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/dialog/manager/model/TopFormManager;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;->Top:Lcom/vlingo/dialog/manager/model/TopFormManager;

    .line 84
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
