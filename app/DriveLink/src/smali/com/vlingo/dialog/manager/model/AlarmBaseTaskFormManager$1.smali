.class final Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;
.super Ljava/util/HashMap;
.source "AlarmBaseTaskFormManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/EnumSet",
        "<",
        "Lcom/vlingo/dialog/util/Day;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 7

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 51
    invoke-static {}, Lcom/vlingo/dialog/util/Day;->values()[Lcom/vlingo/dialog/util/Day;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/dialog/util/Day;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 52
    .local v1, "day":Lcom/vlingo/dialog/util/Day;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/Day;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 54
    .end local v1    # "day":Lcom/vlingo/dialog/util/Day;
    :cond_0
    const-string/jumbo v4, "daily"

    const-class v5, Lcom/vlingo/dialog/util/Day;

    invoke-static {v5}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string/jumbo v4, "weekend"

    sget-object v5, Lcom/vlingo/dialog/util/Day;->SAT:Lcom/vlingo/dialog/util/Day;

    sget-object v6, Lcom/vlingo/dialog/util/Day;->SUN:Lcom/vlingo/dialog/util/Day;

    invoke-static {v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string/jumbo v4, "weekday"

    sget-object v5, Lcom/vlingo/dialog/util/Day;->MON:Lcom/vlingo/dialog/util/Day;

    sget-object v6, Lcom/vlingo/dialog/util/Day;->FRI:Lcom/vlingo/dialog/util/Day;

    invoke-static {v5, v6}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method
