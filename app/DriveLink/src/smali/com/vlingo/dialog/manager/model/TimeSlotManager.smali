.class public Lcom/vlingo/dialog/manager/model/TimeSlotManager;
.super Lcom/vlingo/dialog/manager/model/TimeSlotManagerBase;
.source "TimeSlotManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;
    }
.end annotation


# static fields
.field private static final ABSOLUTE_TIME_FORMAT:Ljava/lang/String; = "{0,number,00}:{1,number,00}"

.field private static final ABSOLUTE_TIME_PATTERN:Ljava/util/regex/Pattern;

.field private static final RELATIVE_TIME_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "([01][0-9]|2[0-3])[?:]([0-5][0-9])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->ABSOLUTE_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 18
    const-string/jumbo v0, "\\+(\\d{2}):(\\d{2})(?::\\d{2})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->RELATIVE_TIME_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TimeSlotManagerBase;-><init>()V

    .line 23
    return-void
.end method

.method public static isResolved(Ljava/lang/String;)Z
    .locals 2
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 157
    if-eqz p0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUnresolved(Ljava/lang/String;)Z
    .locals 2
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 161
    if-eqz p0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static markResolved(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 165
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x3f

    const/16 v1, 0x3a

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static markUnresolved(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 169
    if-nez p0, :cond_0

    const/4 v1, 0x0

    .line 174
    :goto_0
    return-object v1

    .line 170
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 171
    .local v0, "h":I
    const/16 v1, 0xc

    if-lt v0, v1, :cond_1

    .line 172
    add-int/lit8 v0, v0, -0xc

    .line 174
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "{0,number,00}?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static resolveToAm(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 178
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static resolveToPm(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 182
    if-nez p0, :cond_0

    const/4 v1, 0x0

    .line 184
    :goto_0
    return-object v1

    .line 183
    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v0, v1, 0xc

    .line 184
    .local v0, "h":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v9, 0x1

    .line 33
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 34
    .local v5, "originalValue":Ljava/lang/String;
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TimeSlotManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 35
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 36
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 37
    .local v7, "time":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 38
    sget-object v8, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->ABSOLUTE_TIME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 39
    .local v0, "absoluteMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 40
    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 41
    invoke-static {p0, p2, v5}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveAmPmNear(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 60
    .end local v0    # "absoluteMatcher":Ljava/util/regex/Matcher;
    .end local v7    # "time":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 43
    .restart local v0    # "absoluteMatcher":Ljava/util/regex/Matcher;
    .restart local v7    # "time":Ljava/lang/String;
    :cond_1
    sget-object v8, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->RELATIVE_TIME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 44
    .local v6, "relativeMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 46
    invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 47
    .local v2, "h":I
    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 48
    .local v3, "m":I
    mul-int/lit8 v8, v2, 0x3c

    add-int v4, v8, v3

    .line 49
    .local v4, "minutes":I
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/vlingo/dialog/util/DateTime;->addMinutes(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 50
    .local v1, "dateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 51
    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    goto :goto_0

    .line 53
    .end local v1    # "dateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v2    # "h":I
    .end local v3    # "m":I
    .end local v4    # "minutes":I
    :cond_2
    sget-object v8, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "ignoring bad time cf: \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 54
    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 55
    const/4 v8, 0x0

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    goto :goto_0
.end method

.method public resolveAmPm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;Ljava/lang/String;)Z
    .locals 16
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "mode"    # Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;
    .param p3, "timeRef"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-interface/range {p1 .. p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 66
    .local v9, "time":Ljava/lang/String;
    if-eqz v9, :cond_0

    invoke-static {v9}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isResolved(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 68
    :cond_0
    const/4 v8, 0x1

    .line 154
    :goto_0
    return v8

    .line 71
    :cond_1
    const/4 v8, 0x0

    .line 73
    .local v8, "resolved":Z
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->ABSOLUTE_TIME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v12, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    .line 74
    .local v10, "timeMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-nez v12, :cond_2

    .line 75
    new-instance v12, Ljava/lang/RuntimeException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "bad time: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 77
    :cond_2
    const/4 v12, 0x1

    invoke-virtual {v10, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 78
    .local v2, "h":I
    const/4 v12, 0x2

    invoke-virtual {v10, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 80
    .local v4, "m":I
    const/16 v12, 0xc

    if-ne v2, v12, :cond_3

    .line 83
    const/4 v2, 0x0

    .line 84
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "00?"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x3

    invoke-virtual {v9, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 87
    :cond_3
    const/16 v12, 0xc

    if-le v2, v12, :cond_6

    .line 90
    const/4 v8, 0x1

    .line 150
    :cond_4
    :goto_1
    if-eqz v8, :cond_5

    .line 151
    const-string/jumbo v12, "{0,number,00}:{1,number,00}"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 153
    :cond_5
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_6
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_DEFAULT:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_8

    .line 94
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->getMinimumDefaultAM()I

    move-result v12

    if-ge v2, v12, :cond_7

    .line 95
    add-int/lit8 v2, v2, 0xc

    .line 96
    const-string/jumbo v12, "{0,number,00}:{1,number,00}"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 100
    :goto_2
    const/4 v8, 0x1

    goto :goto_1

    .line 98
    :cond_7
    invoke-static {v9}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->markResolved(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 102
    :cond_8
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_9

    .line 104
    invoke-static {v9}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->markResolved(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 105
    const/4 v8, 0x1

    goto :goto_1

    .line 107
    :cond_9
    invoke-static/range {p3 .. p3}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isResolved(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 109
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->ABSOLUTE_TIME_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .line 110
    .local v11, "timeRefMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-nez v12, :cond_a

    .line 111
    new-instance v12, Ljava/lang/RuntimeException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "bad time: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 113
    :cond_a
    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 114
    .local v3, "hRef":I
    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 116
    .local v5, "mRef":I
    mul-int/lit8 v12, v2, 0x3c

    add-int v1, v12, v4

    .line 117
    .local v1, "amMinutes":I
    add-int/lit16 v6, v1, 0x2d0

    .line 118
    .local v6, "pmMinutes":I
    mul-int/lit8 v12, v3, 0x3c

    add-int v7, v12, v5

    .line 127
    .local v7, "refMinutes":I
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->NEAR:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_c

    .line 128
    sub-int v12, v6, v7

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    sub-int v13, v1, v7

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    if-ge v12, v13, :cond_b

    .line 129
    add-int/lit8 v2, v2, 0xc

    .line 131
    :cond_b
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 132
    :cond_c
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-eq v0, v12, :cond_d

    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_4

    .line 133
    :cond_d
    if-lt v1, v7, :cond_e

    .line 135
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 136
    :cond_e
    if-lt v6, v7, :cond_f

    .line 138
    add-int/lit8 v2, v2, 0xc

    .line 139
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 140
    :cond_f
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_4

    .line 141
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 146
    .end local v1    # "amMinutes":I
    .end local v3    # "hRef":I
    .end local v5    # "mRef":I
    .end local v6    # "pmMinutes":I
    .end local v7    # "refMinutes":I
    .end local v11    # "timeRefMatcher":Ljava/util/regex/Matcher;
    :cond_10
    sget-object v12, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_4

    .line 148
    const/4 v8, 0x1

    goto/16 :goto_1
.end method
