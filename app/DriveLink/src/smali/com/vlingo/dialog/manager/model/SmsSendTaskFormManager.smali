.class public Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManagerBase;
.source "SmsSendTaskFormManager.java"


# static fields
.field public static final PT_SET_ADD:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SLOT_MESSAGE:Ljava/lang/String; = "message"

.field public static final SLOT_OP:Ljava/lang/String; = "op"

.field public static final SLOT_TEXT:Ljava/lang/String; = "text"

.field public static final TAG_CONTACT:Ljava/lang/String; = "contact"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "sms:add"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "cmd:add"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "sms:contactadd"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sms:msgappend"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManager;->PT_SET_ADD:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 21
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v3

    .line 22
    .local v3, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v4

    .line 24
    .local v4, "parseType":Ljava/lang/String;
    const-string/jumbo v7, "op"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 25
    .local v2, "opSlot":Lcom/vlingo/dialog/model/IForm;
    if-nez v2, :cond_2

    move-object v1, v6

    .line 27
    .local v1, "opSave":Ljava/lang/String;
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 29
    sget-object v7, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManager;->PT_SET_ADD:Ljava/util/Set;

    invoke-interface {v7, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string/jumbo v7, "contact"

    invoke-virtual {v3, v7, v8}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 33
    if-eqz v2, :cond_0

    .line 34
    invoke-interface {v2, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 38
    :cond_0
    const-string/jumbo v7, "text"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 39
    .local v5, "textSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v7, "message"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 41
    .local v0, "messageSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    if-eqz v5, :cond_1

    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    .line 43
    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 44
    invoke-interface {v0, v8}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 45
    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 46
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 49
    :cond_1
    return-void

    .line 25
    .end local v0    # "messageSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v1    # "opSave":Ljava/lang/String;
    .end local v5    # "textSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_2
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManagerBase;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    const/4 v1, 0x0

    .line 57
    :goto_0
    return v1

    .line 56
    :cond_0
    const-string/jumbo v1, "message"

    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "messageFieldId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SmsSendTaskFormManager;->getAppendTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, v1, v0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->resolveAddReplace(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
