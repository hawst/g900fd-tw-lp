.class public Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;
.super Lcom/vlingo/dialog/event/model/QueryEvent;
.source "ContactResolvedEventBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Matches:Ljava/lang/String; = "Matches"

.field public static final PROP_NumMatches:Ljava/lang/String; = "NumMatches"

.field public static final PROP_Query:Ljava/lang/String; = "Query"


# instance fields
.field private ID:J

.field private Matches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private NumMatches:I

.field private Query:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/dialog/event/model/QueryEvent;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->Matches:Ljava/util/List;

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/ContactResolvedEvent;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->ID:J

    return-wide v0
.end method

.method public getMatches()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->Matches:Ljava/util/List;

    return-object v0
.end method

.method public getNumMatches()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->NumMatches:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->Query:Ljava/lang/String;

    return-object v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->ID:J

    .line 37
    return-void
.end method

.method public setNumMatches(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->NumMatches:I

    .line 28
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ContactResolvedEventBase;->Query:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
