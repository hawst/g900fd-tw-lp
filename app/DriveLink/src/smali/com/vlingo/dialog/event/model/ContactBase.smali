.class public Lcom/vlingo/dialog/event/model/ContactBase;
.super Ljava/lang/Object;
.source "ContactBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Addresses:Ljava/lang/String; = "Addresses"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Id:Ljava/lang/String; = "Id"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_Score:Ljava/lang/String; = "Score"


# instance fields
.field private Addresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;"
        }
    .end annotation
.end field

.field private ID:J

.field private Id:Ljava/lang/String;

.field private Name:Ljava/lang/String;

.field private Score:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Addresses:Ljava/util/List;

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Contact;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAddresses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Addresses:Ljava/util/List;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Contact;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/ContactBase;->ID:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Score:Ljava/lang/Float;

    return-object v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/ContactBase;->ID:J

    .line 46
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Id:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Name:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setScore(Ljava/lang/Float;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Float;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ContactBase;->Score:Ljava/lang/Float;

    .line 37
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
