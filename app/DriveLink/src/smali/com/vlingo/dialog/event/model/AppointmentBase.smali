.class public Lcom/vlingo/dialog/event/model/AppointmentBase;
.super Ljava/lang/Object;
.source "AppointmentBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_AllDay:Ljava/lang/String; = "AllDay"

.field public static final PROP_Date:Ljava/lang/String; = "Date"

.field public static final PROP_Duration:Ljava/lang/String; = "Duration"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Id:Ljava/lang/String; = "Id"

.field public static final PROP_Invitees:Ljava/lang/String; = "Invitees"

.field public static final PROP_Location:Ljava/lang/String; = "Location"

.field public static final PROP_ReadOnly:Ljava/lang/String; = "ReadOnly"

.field public static final PROP_Time:Ljava/lang/String; = "Time"

.field public static final PROP_Title:Ljava/lang/String; = "Title"


# instance fields
.field private AllDay:Z

.field private Date:Ljava/lang/String;

.field private Duration:Ljava/lang/String;

.field private ID:J

.field private Id:Ljava/lang/String;

.field private Invitees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private Location:Ljava/lang/String;

.field private ReadOnly:Z

.field private Time:Ljava/lang/String;

.field private Title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->AllDay:Z

    .line 19
    iput-boolean v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->ReadOnly:Z

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Invitees:Ljava/util/List;

    .line 27
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Appointment;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAllDay()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->AllDay:Z

    return v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 99
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Appointment;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Date:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Duration:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->ID:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getInvitees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Invitees:Ljava/util/List;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Location:Ljava/lang/String;

    return-object v0
.end method

.method public getReadOnly()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->ReadOnly:Z

    return v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Time:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public setAllDay(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->AllDay:Z

    .line 68
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Date:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setDuration(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Duration:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->ID:J

    .line 91
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Id:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Location:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setReadOnly(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->ReadOnly:Z

    .line 82
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Time:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AppointmentBase;->Title:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
