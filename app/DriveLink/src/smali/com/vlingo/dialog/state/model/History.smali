.class public Lcom/vlingo/dialog/state/model/History;
.super Lcom/vlingo/dialog/state/model/HistoryBase;
.source "History.java"


# static fields
.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private completedQueries:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;",
            ">;"
        }
    .end annotation
.end field

.field private pendingQueries:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/dialog/state/model/History;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/HistoryBase;-><init>()V

    .line 18
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    .line 22
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p1, "queryGoal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/state/model/History;->clearQueriesByType(Ljava/lang/Class;)V

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public clearPendingQueries()V
    .locals 5

    .prologue
    .line 66
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 68
    .local v1, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "clearing pending query: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 72
    return-void
.end method

.method public clearQueriesByType(Ljava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    iget-object v4, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 76
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 77
    .local v3, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 79
    sget-object v4, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "clearing pending query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    .end local v3    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_1
    iget-object v4, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/state/model/CompletedQuery;>;"
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 83
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 84
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v3

    .line 85
    .restart local v3    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 86
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 89
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v3    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_3
    return-void
.end method

.method public completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 5
    .param p1, "queryEvent"    # Lcom/vlingo/dialog/event/model/QueryEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/event/model/QueryEvent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;"
        }
    .end annotation

    .prologue
    .line 50
    .local p2, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .local v1, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne p2, v2, :cond_1

    .line 52
    new-instance v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    invoke-direct {v0, v1, p1}, Lcom/vlingo/dialog/state/model/CompletedQuery;-><init>(Lcom/vlingo/dialog/goal/model/QueryGoal;Lcom/vlingo/dialog/event/model/QueryEvent;)V

    .line 53
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 62
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :goto_1
    return-object v0

    .line 56
    :cond_1
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "removing unmatched query of type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getCompletedQueries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPendingQueries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public peekQuery()Lcom/vlingo/dialog/goal/model/QueryGoal;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 35
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 100
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/HistoryItem;->postDeserialize()V

    goto :goto_0

    .line 102
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_0
    return-void
.end method

.method public preSerialize()V
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 94
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/HistoryItem;->preSerialize()V

    goto :goto_0

    .line 96
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 106
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 107
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 108
    return-void
.end method
