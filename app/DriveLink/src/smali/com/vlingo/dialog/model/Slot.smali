.class public Lcom/vlingo/dialog/model/Slot;
.super Lcom/vlingo/dialog/model/SlotBase;
.source "Slot.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/dialog/model/SlotBase;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/vlingo/dialog/model/Slot;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Slot;-><init>()V

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/model/Slot;->copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;

    move-result-object v0

    return-object v0
.end method

.method protected copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;
    .locals 2
    .param p1, "d"    # Lcom/vlingo/dialog/model/AbstractForm;

    .prologue
    .line 26
    move-object v0, p1

    check-cast v0, Lcom/vlingo/dialog/model/Slot;

    .line 27
    .local v0, "dst":Lcom/vlingo/dialog/model/Slot;
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Slot;->getPromptCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/model/Slot;->setPromptCount(Ljava/lang/Integer;)V

    .line 28
    invoke-super {p0, v0}, Lcom/vlingo/dialog/model/SlotBase;->copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;

    move-result-object v1

    return-object v1
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Slot;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setComplete(Z)V
    .locals 0
    .param p1, "complete"    # Z

    .prologue
    .line 12
    return-void
.end method
