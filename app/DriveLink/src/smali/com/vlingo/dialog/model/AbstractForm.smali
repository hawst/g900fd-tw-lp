.class public abstract Lcom/vlingo/dialog/model/AbstractForm;
.super Lcom/vlingo/dialog/model/AbstractFormBase;
.source "AbstractForm.java"


# instance fields
.field private transient filled:Z

.field private transient modified:Z

.field private transient parent:Lcom/vlingo/dialog/model/IForm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/model/AbstractFormBase;-><init>()V

    .line 15
    iput-boolean v0, p0, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    .line 16
    iput-boolean v0, p0, Lcom/vlingo/dialog/model/AbstractForm;->filled:Z

    return-void
.end method


# virtual methods
.method public addSlot(Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public anyModifications()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 105
    iget-boolean v3, p0, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    if-ne v3, v2, :cond_0

    .line 113
    :goto_0
    return v2

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 109
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->anyModifications()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 113
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public copy()Lcom/vlingo/dialog/model/IForm;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "must override"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;
    .locals 1
    .param p1, "dst"    # Lcom/vlingo/dialog/model/AbstractForm;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/model/AbstractForm;->setName(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/model/AbstractForm;->setValue(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->isComplete()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/model/AbstractForm;->setComplete(Z)V

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getDisabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/model/AbstractForm;->setDisabled(Ljava/lang/Boolean;)V

    .line 183
    iget-boolean v0, p0, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    iput-boolean v0, p1, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    .line 184
    return-object p1
.end method

.method public dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 145
    if-nez p1, :cond_1

    .line 146
    const/4 p0, 0x0

    .line 158
    .end local p0    # "this":Lcom/vlingo/dialog/model/AbstractForm;
    :cond_0
    :goto_0
    return-object p0

    .line 148
    .restart local p0    # "this":Lcom/vlingo/dialog/model/AbstractForm;
    :cond_1
    const-string/jumbo v5, ""

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 151
    move-object v1, p0

    .line 152
    .local v1, "form":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "\\."

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 153
    .local v4, "name":Ljava/lang/String;
    invoke-interface {v1, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 154
    if-nez v1, :cond_3

    .end local v4    # "name":Ljava/lang/String;
    :cond_2
    move-object p0, v1

    .line 158
    goto :goto_0

    .line 152
    .restart local v4    # "name":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/model/AbstractForm;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 47
    .local v0, "f":Lcom/vlingo/dialog/model/IForm;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getFilled()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 84
    iget-boolean v3, p0, Lcom/vlingo/dialog/model/AbstractForm;->filled:Z

    if-eqz v3, :cond_0

    .line 93
    :goto_0
    return v2

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 88
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 89
    iput-boolean v2, p0, Lcom/vlingo/dialog/model/AbstractForm;->filled:Z

    .line 93
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_2
    iget-boolean v2, p0, Lcom/vlingo/dialog/model/AbstractForm;->filled:Z

    goto :goto_0
.end method

.method public getModified()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    return v0
.end method

.method public getParent()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/dialog/model/AbstractForm;->parent:Lcom/vlingo/dialog/model/IForm;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getPathList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 126
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 129
    .end local v2    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 130
    .local v1, "length":I
    if-lez v1, :cond_1

    .line 131
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 133
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getPathList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 138
    .local v1, "path":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "form":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 139
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 138
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_0
    return-object v1
.end method

.method public getPromptCountAsInt()I
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getPromptCount()Ljava/lang/Integer;

    move-result-object v0

    .line 42
    .local v0, "i":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getSlots()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 56
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSlots()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/model/IForm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTop()Lcom/vlingo/dialog/model/IForm;
    .locals 2

    .prologue
    .line 188
    move-object v0, p0

    .line 189
    .local v0, "form":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    goto :goto_0

    .line 192
    :cond_0
    return-object v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isDisabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getDisabled()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getDisabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getSlots()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 169
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1, p0}, Lcom/vlingo/dialog/model/IForm;->setParent(Lcom/vlingo/dialog/model/IForm;)V

    .line 170
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->postDeserialize()V

    goto :goto_0

    .line 172
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    return-void
.end method

.method public preSerialize()V
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/AbstractForm;->getSlots()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 163
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->preSerialize()V

    goto :goto_0

    .line 165
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    return-void
.end method

.method public prune()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public removeSlot(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/model/AbstractForm;->setValue(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/model/AbstractForm;->setComplete(Z)V

    .line 119
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/model/AbstractForm;->setFilled(Z)V

    .line 120
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/model/AbstractForm;->setModified(Z)V

    .line 121
    return-void
.end method

.method public setComplete(Z)V
    .locals 1
    .param p1, "complete"    # Z

    .prologue
    .line 25
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setFilled(Z)V
    .locals 0
    .param p1, "filled"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/vlingo/dialog/model/AbstractForm;->filled:Z

    .line 81
    return-void
.end method

.method public setModified(Z)V
    .locals 0
    .param p1, "modified"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    .line 98
    return-void
.end method

.method public setParent(Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "parent"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vlingo/dialog/model/AbstractForm;->parent:Lcom/vlingo/dialog/model/IForm;

    .line 77
    return-void
.end method

.method public setPromptCount(I)V
    .locals 1
    .param p1, "promptCount"    # I

    .prologue
    .line 37
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/model/AbstractForm;->setPromptCount(Ljava/lang/Integer;)V

    .line 38
    return-void

    .line 37
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/vlingo/dialog/model/AbstractFormBase;->setValue(Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/model/AbstractForm;->modified:Z

    .line 22
    return-void
.end method
