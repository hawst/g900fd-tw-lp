.class public abstract Lcom/vlingo/dialog/model/AbstractFormBase;
.super Ljava/lang/Object;
.source "AbstractFormBase.java"

# interfaces
.implements Lcom/vlingo/dialog/model/IForm;
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Disabled:Ljava/lang/String; = "Disabled"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_PromptCount:Ljava/lang/String; = "PromptCount"

.field public static final PROP_Value:Ljava/lang/String; = "Value"


# instance fields
.field private Disabled:Ljava/lang/Boolean;

.field private ID:J

.field private Name:Ljava/lang/String;

.field private PromptCount:Ljava/lang/Integer;

.field private Value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/model/AbstractForm;

    const-string/jumbo v2, "/DialogModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/model/AbstractForm;

    const-string/jumbo v2, "/DialogModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDisabled()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->Disabled:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->ID:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->PromptCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->Value:Ljava/lang/String;

    return-object v0
.end method

.method public setDisabled(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->Disabled:Ljava/lang/Boolean;

    .line 37
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->ID:J

    .line 51
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->Name:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setPromptCount(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->PromptCount:Ljava/lang/Integer;

    .line 44
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/model/AbstractFormBase;->Value:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
