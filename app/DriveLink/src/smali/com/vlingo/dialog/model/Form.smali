.class public Lcom/vlingo/dialog/model/Form;
.super Lcom/vlingo/dialog/model/FormBase;
.source "Form.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/dialog/model/FormBase;-><init>()V

    return-void
.end method


# virtual methods
.method public addSlot(Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 24
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/model/Form;->removeSlot(Ljava/lang/String;)V

    .line 25
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-interface {p1, p0}, Lcom/vlingo/dialog/model/IForm;->setParent(Lcom/vlingo/dialog/model/IForm;)V

    .line 27
    return-void
.end method

.method public copy()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Form;-><init>()V

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/model/Form;->copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;

    move-result-object v0

    return-object v0
.end method

.method protected copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;
    .locals 6
    .param p1, "d"    # Lcom/vlingo/dialog/model/AbstractForm;

    .prologue
    .line 46
    move-object v0, p1

    check-cast v0, Lcom/vlingo/dialog/model/Form;

    .line 47
    .local v0, "dst":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v2

    .line 48
    .local v2, "dstSlots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 49
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/model/IForm;

    .line 50
    .local v4, "srcSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 51
    .local v1, "dstSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1, v0}, Lcom/vlingo/dialog/model/IForm;->setParent(Lcom/vlingo/dialog/model/IForm;)V

    .line 52
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 54
    .end local v1    # "dstSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "srcSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    invoke-super {p0, v0}, Lcom/vlingo/dialog/model/FormBase;->copyInto(Lcom/vlingo/dialog/model/AbstractForm;)Lcom/vlingo/dialog/model/AbstractForm;

    move-result-object v5

    return-object v5
.end method

.method public isComplete()Z
    .locals 2

    .prologue
    .line 19
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Form;->getComplete()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public prune()V
    .locals 3

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/model/IForm;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 61
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->anyModifications()Z

    move-result v2

    if-nez v2, :cond_0

    .line 62
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 65
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    return-void
.end method

.method public removeSlot(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/model/IForm;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 32
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 33
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 37
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    return-void
.end method

.method public setComplete(Z)V
    .locals 1
    .param p1, "complete"    # Z

    .prologue
    .line 14
    if-eqz p1, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/model/Form;->setComplete(Ljava/lang/Boolean;)V

    .line 15
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
